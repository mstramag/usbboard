####################### Instructions for different config files######################################
#############                    Snow HAN @ EPFL                               ######################
#############                      2012.09.04                                  ######################
#####################################################################################################

For DAQ system, there are two types of config file: usbBoard cfg file and vata64 chip cfg file.
-----The usbBoard cfg file is used to set path to usbBoard firmware,registers,uplinks and path to vata64 chip cfg files. Examples for new version usbBoard is board01.cfg_newUSBBoard, while the one for old version usbBoard is board05.cfg_oldUSBBoard
!!!Attention: the usbBoard cfg files should strictly follow the format:boardXX.cfg, XX is the usbBoard ID which is written on the usbBoard box.for example: board09.cfg, or board15.cfg.


-----The vata64 chip cfg file is used to configure the vata64 chip and its corresponding FrontEnd board (trigger,LEDs).
!!!Attention: this file shoudl also strictly follow the format:VATA64V2_uplinkXX.cfg. XX stands for the uplink ID which can be set in the usbBoard cfgFile. Example can be found as VATA64V2_uplink10.cfg



For Analysis:
------setup.txt is used for lect_uplinks to analyze the ECAL prototype.
