# Simple method to manually select debug/release build for the whole usbBoard project
CONFIG += debug
#CONFIG += release

# Qt is disabled, per default. We're only using the Qt make system in usbBoard
CONFIG -= qt

# Mac-specific trick, to allow to build for (Snow)Leopard
macx {
    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.5

    message("NOTE: There is no use the QuickUSB drivers on mac. Forcing NO_QUSB mode.")
    CONFIG += NO_QUSB
    CONFIG -= app_bundle
}

# Handle "qmake CONFIG+=NO_ROOT"
NO_ROOT {
    QMAKE_CXXFLAGS += -DNO_ROOT
}

# Handle "qmake CONFIG+=NO_QUSB"
NO_QUSB {
    QMAKE_CXXFLAGS += -DNO_QUSB
}

# Handle "qmake CONFIG+=NEWUSBBOARD_NO_QUSB"
NEWUSBBOARD_NO_QUSB {
    QMAKE_CXXFLAGS += -DNO_QUSB
    QMAKE_CXXFLAGS += -DNEW_USBBOARD
}

# Handle "qmke CONFIG+=NEWUSBBOARD"
NEWUSBBOARD {
    QMAKE_CXXFLAGS += -DNEW_USBBOARD
}


# Set default include path, to the current directory
INCLUDEPATH += $${PWD}

# Seperate source & build dirs
DESTDIR = $${PWD}$${QMAKE_DIR_SEP}Builds
OBJECTS_DIR = $${DESTDIR}$${QMAKE_DIR_SEP}Temp
MOC_DIR = $${DESTDIR}$${QMAKE_DIR_SEP}Temp
UI_DIR = $${DESTDIR}$${QMAKE_DIR_SEP}Temp
RCC_DIR = $${DESTDIR}$${QMAKE_DIR_SEP}Temp
