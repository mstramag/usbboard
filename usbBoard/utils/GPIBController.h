#ifndef GPIBController_h
#define GPIBController_h

#include <time.h>

#include <string>
#include <sstream>

const int amplitudeMin       = 50;  //mV
const int amplitudeMax       = 175;  //mV
const int amplitudeStep      = 25;  //mV

const int nChannelGroups     = 2;    // we have odd and even channels for chipTest
const int nEventsPerSetting  = 200; 

const int triggerDelay       = 40;   // ns
const int signalDelay        = 0;    // ns

inline std::string pulseGeneratorSettings()
{
    std::stringstream s;
    s
        << "amplitudeMin " << amplitudeMin << ", "
        << "amplitudeMax " << amplitudeMax << ", "
        << "amplitudeStep " << amplitudeStep << ", "
        << "nEventsPerSetting " << nEventsPerSetting << ", "
        << "triggerDelay " << triggerDelay << ", "
        << "signalDelay " << signalDelay;
    return s.str();
}


class GPIBController {
public:
    GPIBController();

    void initialize();
    void initializeSignalGenerator();
    void releaseDevices();

    void setAmplitude(int amplitude);

    // Global constants
    static const unsigned int s_maximumBufferSize = 52000;
    static const unsigned int s_maximumMessageBufferSize = 240;
    static const char s_deviceName[8];
    static const int s_primaryAddress;
    static const int s_secondaryAddress;

private:
    void armSignalGenerator();

    void readData();
    void writeData(char* data);

    void failWithGPIBError(char* msg);

    int m_board;
    int m_device;
    char m_buffer[s_maximumBufferSize];

    time_t m_startTime;
};

#endif
