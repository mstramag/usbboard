#ifndef ChannelSwitchController_h
#define ChannelSwitchController_h

#include <bitset>
#include <map>

class CQuickUsb;

class ChannelSwitchController {
public:
    ChannelSwitchController();
    ~ChannelSwitchController();

    // Call once to initialize board, returns a boolean indicating the success of the QuickUSB communication
    bool initialize(int boardSerialNumber = 6147);

    // Switch to channel (range: 0 .. 255)
    bool selectChannel(unsigned int channel);
private:
    CQuickUsb* m_interface;
};

#endif // ChannelSwitchController_h
