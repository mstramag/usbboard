#include "ChannelSwitchController.h"

// Hacks to include CQuickUsb
#include "CQuickUsb.h"
#include <stdlib.h>

#undef TRUE
#undef FALSE
#undef HANDLE

// All other includes start here
#include <assert.h>
#include <bitset>
#include <iostream>

// Maximum number of channels
static const unsigned int s_channels = 256;

ChannelSwitchController::ChannelSwitchController()
    : m_interface(0)
{
}

ChannelSwitchController::~ChannelSwitchController()
{
    delete m_interface;
}

bool ChannelSwitchController::initialize(int boardSerialNumber)
{
    char availableModules[256];
    int result = CQuickUsb::FindModules(availableModules, 256);
    if (!result) {
        fprintf(stderr, "ERROR: Couldn't enumerate QuickUSB modules!\n");
        return false;
    }

    // Parsing QuickUSB module list and adding them to the USB board vector.
    // Modules are separated through '\0'. The module list ends with "\0\0".
    char* namePtr = availableModules;
    while (namePtr[0] != '\0' && namePtr[1] != '\0') {
        const char* moduleString = strdup(namePtr);
        namePtr = strchr(namePtr, '\0') + 1;

        m_interface = new CQuickUsb(moduleString);
        if (!m_interface->Open()) {
            fprintf(stderr, "ERROR: Unable to open QuickUSB device '%s', advancing to next module!\n", moduleString);
            m_interface->Close();
            delete m_interface;
            m_interface = 0;
            continue;
        }

        char deviceDescriptor[256];
        if (!m_interface->GetStringDescriptor(QUICKUSB_SERIAL, deviceDescriptor, sizeof(deviceDescriptor))) {
            fprintf(stderr, "ERROR: Unable to query QuickUSB device '%s' serial number, advancing to next module!\n", moduleString);
            m_interface->Close();
            delete m_interface;
            m_interface = 0;
            continue;
        }

        int serialNumber = atoi(deviceDescriptor);
        if (serialNumber != boardSerialNumber) {
            m_interface->Close();
            delete m_interface;
            m_interface = 0;
            continue;
        }

        // fprintf(stderr, "Found valid device '%s' with serial '%i'\n", moduleString, serialNumber);
        delete[] moduleString;
        break;
    }

    if (!m_interface) {
        fprintf(stderr, "ERROR: Couldn't find any suitable device!\n");
        return false;
    }

	if (!m_interface->WritePortDir(1, 0xFF)) {
        fprintf(stderr, "ERROR: Unable to set port direction\n");
        m_interface->Close();
        delete m_interface;
        return false;
    }

    // Close board so that any following selectChannel() call automatically opens & closes the device (see CQuickUsb class)
    m_interface->Close();
    return true;
}

#define BINARY_OCTET(x) \
    (((0##x & 01)) | \
     ((0##x & 010) >> 2) | \
     ((0##x & 0100) >> 4) | \
     ((0##x & 01000) >> 6) | \
     ((0##x & 010000) >> 8) | \
     ((0##x & 0100000) >> 10) | \
     ((0##x & 01000000) >> 12) | \
     ((0##x & 010000000) >> 14))



static unsigned char s_channelMap[s_channels] = {
    // 0..31
    BINARY_OCTET(01100000),
    BINARY_OCTET(01100001),
    BINARY_OCTET(01100010),
    BINARY_OCTET(01100011),
    BINARY_OCTET(01100100),
    BINARY_OCTET(01100101),
    BINARY_OCTET(01100110),
    BINARY_OCTET(01100111),
    BINARY_OCTET(01101000),
    BINARY_OCTET(01101001),
    BINARY_OCTET(01101010),
    BINARY_OCTET(01101011),
    BINARY_OCTET(01101100),
    BINARY_OCTET(01101101),
    BINARY_OCTET(01101110),
    BINARY_OCTET(01101111),
    BINARY_OCTET(01110000),
    BINARY_OCTET(01110001),
    BINARY_OCTET(01110010),
    BINARY_OCTET(01110011),
    BINARY_OCTET(01110100),
    BINARY_OCTET(01110101),
    BINARY_OCTET(01110110),
    BINARY_OCTET(01110111),
    BINARY_OCTET(01111000),
    BINARY_OCTET(01111001),
    BINARY_OCTET(01111010),
    BINARY_OCTET(01111011),
    BINARY_OCTET(01111100),
    BINARY_OCTET(01111101),
    BINARY_OCTET(01111110),
    BINARY_OCTET(01111111),
    //32..63
    BINARY_OCTET(01000000),
    BINARY_OCTET(01000001),
    BINARY_OCTET(01000010),
    BINARY_OCTET(01000011),
    BINARY_OCTET(01000100),
    BINARY_OCTET(01000101),
    BINARY_OCTET(01000110),
    BINARY_OCTET(01000111),
    BINARY_OCTET(01001000),
    BINARY_OCTET(01001001),
    BINARY_OCTET(01001010),
    BINARY_OCTET(01001011),
    BINARY_OCTET(01001100),
    BINARY_OCTET(01001101),
    BINARY_OCTET(01001110),
    BINARY_OCTET(01001111),
    BINARY_OCTET(01010000),
    BINARY_OCTET(01010001),
    BINARY_OCTET(01010010),
    BINARY_OCTET(01010011),
    BINARY_OCTET(01010100),
    BINARY_OCTET(01010101),
    BINARY_OCTET(01010110),
    BINARY_OCTET(01010111),
    BINARY_OCTET(01011000),
    BINARY_OCTET(01011001),
    BINARY_OCTET(01011010),
    BINARY_OCTET(01011011),
    BINARY_OCTET(01011100),
    BINARY_OCTET(01011101),
    BINARY_OCTET(01011110),
    BINARY_OCTET(01011111),
    //64..95
    BINARY_OCTET(11100000),
    BINARY_OCTET(11100001),
    BINARY_OCTET(11100010),
    BINARY_OCTET(11100011),
    BINARY_OCTET(11100100),
    BINARY_OCTET(11100101),
    BINARY_OCTET(11100110),
    BINARY_OCTET(11100111),
    BINARY_OCTET(11101000),
    BINARY_OCTET(11101001),
    BINARY_OCTET(11101010),
    BINARY_OCTET(11101011),
    BINARY_OCTET(11101100),
    BINARY_OCTET(11101101),
    BINARY_OCTET(11101110),
    BINARY_OCTET(11101111),
    BINARY_OCTET(11110000),
    BINARY_OCTET(11110001),
    BINARY_OCTET(11110010),
    BINARY_OCTET(11110011),
    BINARY_OCTET(11110100),
    BINARY_OCTET(11110101),
    BINARY_OCTET(11110110),
    BINARY_OCTET(11110111),
    BINARY_OCTET(11111000),
    BINARY_OCTET(11111001),
    BINARY_OCTET(11111010),
    BINARY_OCTET(11111011),
    BINARY_OCTET(11111100),
    BINARY_OCTET(11111101),
    BINARY_OCTET(11111110),
    BINARY_OCTET(11111111),
    //96..127
    BINARY_OCTET(11000000),
    BINARY_OCTET(11000001),
    BINARY_OCTET(11000010),
    BINARY_OCTET(11000011),
    BINARY_OCTET(11000100),
    BINARY_OCTET(11000101),
    BINARY_OCTET(11000110),
    BINARY_OCTET(11000111),
    BINARY_OCTET(11001000),
    BINARY_OCTET(11001001),
    BINARY_OCTET(11001010),
    BINARY_OCTET(11001011),
    BINARY_OCTET(11001100),
    BINARY_OCTET(11001101),
    BINARY_OCTET(11001110),
    BINARY_OCTET(11001111),
    BINARY_OCTET(11010000),
    BINARY_OCTET(11010001),
    BINARY_OCTET(11010010),
    BINARY_OCTET(11010011),
    BINARY_OCTET(11010100),
    BINARY_OCTET(11010101),
    BINARY_OCTET(11010110),
    BINARY_OCTET(11010111),
    BINARY_OCTET(11011000),
    BINARY_OCTET(11011001),
    BINARY_OCTET(11011010),
    BINARY_OCTET(11011011),
    BINARY_OCTET(11011100),
    BINARY_OCTET(11011101),
    BINARY_OCTET(11011110),
    BINARY_OCTET(11011111),
    //128..159
    BINARY_OCTET(00100000),
    BINARY_OCTET(00100001),
    BINARY_OCTET(00100010),
    BINARY_OCTET(00100011),
    BINARY_OCTET(00100100),
    BINARY_OCTET(00100101),
    BINARY_OCTET(00100110),
    BINARY_OCTET(00100111),
    BINARY_OCTET(00101000),
    BINARY_OCTET(00101001),
    BINARY_OCTET(00101010),
    BINARY_OCTET(00101011),
    BINARY_OCTET(00101100),
    BINARY_OCTET(00101101),
    BINARY_OCTET(00101110),
    BINARY_OCTET(00101111),
    BINARY_OCTET(00110000),
    BINARY_OCTET(00110001),
    BINARY_OCTET(00110010),
    BINARY_OCTET(00110011),
    BINARY_OCTET(00110100),
    BINARY_OCTET(00110101),
    BINARY_OCTET(00110110),
    BINARY_OCTET(00110111),
    BINARY_OCTET(00111000),
    BINARY_OCTET(00111001),
    BINARY_OCTET(00111010),
    BINARY_OCTET(00111011),
    BINARY_OCTET(00111100),
    BINARY_OCTET(00111101),
    BINARY_OCTET(00111110),
    BINARY_OCTET(00111111),
    //160..191
    BINARY_OCTET(00000000),
    BINARY_OCTET(00000001),
    BINARY_OCTET(00000010),
    BINARY_OCTET(00000011),
    BINARY_OCTET(00000100),
    BINARY_OCTET(00000101),
    BINARY_OCTET(00000110),
    BINARY_OCTET(00000111),
    BINARY_OCTET(00001000),
    BINARY_OCTET(00001001),
    BINARY_OCTET(00001010),
    BINARY_OCTET(00001011),
    BINARY_OCTET(00001100),
    BINARY_OCTET(00001101),
    BINARY_OCTET(00001110),
    BINARY_OCTET(00001111),
    BINARY_OCTET(00010000),
    BINARY_OCTET(00010001),
    BINARY_OCTET(00010010),
    BINARY_OCTET(00010011),
    BINARY_OCTET(00010100),
    BINARY_OCTET(00010101),
    BINARY_OCTET(00010110),
    BINARY_OCTET(00010111),
    BINARY_OCTET(00011000),
    BINARY_OCTET(00011001),
    BINARY_OCTET(00011010),
    BINARY_OCTET(00011011),
    BINARY_OCTET(00011100),
    BINARY_OCTET(00011101),
    BINARY_OCTET(00011110),
    BINARY_OCTET(00011111),
    //192..223
    BINARY_OCTET(10100000),
    BINARY_OCTET(10100001),
    BINARY_OCTET(10100010),
    BINARY_OCTET(10100011),
    BINARY_OCTET(10100100),
    BINARY_OCTET(10100101),
    BINARY_OCTET(10100110),
    BINARY_OCTET(10100111),
    BINARY_OCTET(10101000),
    BINARY_OCTET(10101001),
    BINARY_OCTET(10101010),
    BINARY_OCTET(10101011),
    BINARY_OCTET(10101100),
    BINARY_OCTET(10101101),
    BINARY_OCTET(10101110),
    BINARY_OCTET(10101111),
    BINARY_OCTET(10110000),
    BINARY_OCTET(10110001),
    BINARY_OCTET(10110010),
    BINARY_OCTET(10110011),
    BINARY_OCTET(10110100),
    BINARY_OCTET(10110101),
    BINARY_OCTET(10110110),
    BINARY_OCTET(10110111),
    BINARY_OCTET(10111000),
    BINARY_OCTET(10111001),
    BINARY_OCTET(10111010),
    BINARY_OCTET(10111011),
    BINARY_OCTET(10111100),
    BINARY_OCTET(10111101),
    BINARY_OCTET(10111110),
    BINARY_OCTET(10111111),
    //224..255
    BINARY_OCTET(10000000),
    BINARY_OCTET(10000001),
    BINARY_OCTET(10000010),
    BINARY_OCTET(10000011),
    BINARY_OCTET(10000100),
    BINARY_OCTET(10000101),
    BINARY_OCTET(10000110),
    BINARY_OCTET(10000111),
    BINARY_OCTET(10001000),
    BINARY_OCTET(10001001),
    BINARY_OCTET(10001010),
    BINARY_OCTET(10001011),
    BINARY_OCTET(10001100),
    BINARY_OCTET(10001101),
    BINARY_OCTET(10001110),
    BINARY_OCTET(10001111),
    BINARY_OCTET(10010000),
    BINARY_OCTET(10010001),
    BINARY_OCTET(10010010),
    BINARY_OCTET(10010011),
    BINARY_OCTET(10010100),
    BINARY_OCTET(10010101),
    BINARY_OCTET(10010110),
    BINARY_OCTET(10010111),
    BINARY_OCTET(10011000),
    BINARY_OCTET(10011001),
    BINARY_OCTET(10011010),
    BINARY_OCTET(10011011),
    BINARY_OCTET(10011100),
    BINARY_OCTET(10011101),
    BINARY_OCTET(10011110),
    BINARY_OCTET(10011111)
};
int s_channelPermutation[] = {
	156, 144, 157, 145, 158, 146, 159, 147,
	139, 148, 138, 149, 137, 150, 136, 151,
	135, 152, 134, 153, 133, 154, 132, 155,
	131, 143, 130, 142, 129, 141, 128, 140,
	188, 176, 189, 177, 190, 178, 191, 179,
	171, 180, 170, 181, 169, 182, 168, 183,
	167, 184, 166, 185, 165, 186, 164, 187,
	163, 175, 162, 174, 161, 173, 160, 172,
	220, 208, 221, 209, 222, 210, 223, 211,
	203, 212, 202, 213, 201, 214, 200, 215,
	199, 216, 198, 217, 197, 218, 196, 219,
	195, 207, 194, 206, 193, 205, 192, 204,
	252, 240, 253, 241, 254, 242, 255, 243,
	235, 244, 234, 245, 233, 246, 232, 247,
	231, 248, 230, 249, 229, 250, 228, 251,
	227, 239, 226, 238, 225, 237, 224, 236,
	28, 16, 29, 17, 30, 18, 31, 19,
	11, 20, 10, 21, 9, 22, 8, 23,
	7, 24, 6, 25, 5, 26, 4, 27,
	3, 15, 2, 14, 1, 13, 0, 12,
	60, 48, 61, 49, 62, 50, 63, 51,
	43, 52, 42, 53, 41, 54, 40, 55,
	39, 56, 38, 57, 37, 58, 36, 59,
	35, 47, 34, 46, 33, 45, 32, 44,
	92, 80, 93, 81, 94, 82, 95, 83,
	75, 84, 74, 85, 73, 86, 72, 87,
	71, 88, 70, 89, 69, 90, 68, 91,
	67, 79, 66, 78, 65, 77, 64, 76,
	124, 112, 125, 113, 126, 114, 127, 115,
	107, 116, 106, 117, 105, 118, 104, 119,
	103, 120, 102, 121, 101, 122, 100, 123,
	99, 111, 98, 110, 97, 109, 96, 108
};

bool ChannelSwitchController::selectChannel(unsigned int channel)
{
    unsigned char data = s_channelMap[s_channelPermutation[channel]];
    if (!m_interface->WritePort(1, &data, 1)) {
        fprintf(stderr, "ERROR: Unable to switch to channel '%i'\n", s_channelPermutation[channel]);
        m_interface->Close();
        delete m_interface;
        return false;
    }
    return true; 
}

