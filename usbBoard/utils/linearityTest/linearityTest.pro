#CONFIG += release
CONFIG += debug

CONFIG -= qt

SOURCES += \
    ChannelSwitchController.cpp \
    ../GPIBController.cpp \
    linearityTest.cpp \
    ../../support/CQuickUsb.cpp

HEADERS += \
    ChannelSwitchController.h \
    ../GPIBController.h \
    ../../support/CQuickUsb.h

exists($(USBBOARDPATH)) {
    include($(USBBOARDPATH)/usbBoard.pri)
    include($(USBBOARDPATH)/RootCInt.pri)
    USBBOARDPATH=$(USBBOARDPATH)
} else {
    include(../../usbBoard.pri)
    include(../../RootCInt.pri)
    USBBOARDPATH=../..
}

DEPENDPATH += \
    $$USBBOARDPATH/readout \
    $$USBBOARDPATH/utils/linearityAnalysis/shared \
    $$USBBOARDPATH/event \
    $$USBBOARDPATH/support \
    support

INCLUDEPATH += \
    $$DEPENDPATH

win* {
    LIBS += -L$$USBBOARDPATH/support -L..$${QMAKE_DIR_SEP}..$${QMAKE_DIR_SEP}Builds -llinearityTestShared -lusbBoardEvent -lusbBoardReadout
    LIBS += -lQuickUsb support$${QMAKE_DIR_SEP}gpib-32.obj
    CONFIG += console embed_manifest_exe
}
