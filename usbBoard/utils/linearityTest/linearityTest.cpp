#include <vector>
#include <iostream>
#include <cstdlib>
#include <time.h>

#include <Settings.h>
#include <UsbBoardManager.h>
#include <RunDescription.h>
#include <MergedEvent.h>

#include "../GPIBController.h"

#include <conio.h>
#include <TROOT.h>
#include <TFile.h>
#include <TH2.h>
#include <TAxis.h>
#include "ChannelSwitchController.h"

// Helper structure for the callback
typedef MergedEvent* MergedEventPtr;

static const int s_uplinkId = 1;

int main(int argc, char* argv[])
{
	if (argc != 10) {
        std::cerr << "usage: " << argv[0] << "<board number> <chipID 1> <chipID 2> ... <chipID 8>" << std::endl;
        exit(1);
    }
	
	int boardNumber = atoi(argv[1]);
	
	std::vector<int> chipIds;
	for (int i = 2; i<argc ;i++) {
		chipIds.push_back(atoi(argv[i]));
	}
	
	char rootFileName[256];
	sprintf(rootFileName, "board%02d_a%03d_b%03d_c%03d_d%03d_e%03d_f%03d_g%03d_h%03d.root", boardNumber, chipIds[0], chipIds[1], chipIds[2], chipIds[3], chipIds[4], chipIds[5], chipIds[6],chipIds[7]);
	
    //std::string outputFileName(rootFileName);

    GPIBController* gpibController = new GPIBController;
    gpibController->initialize();
    gpibController->initializeSignalGenerator();

    ChannelSwitchController switchController;
    switchController.initialize();

    // Read usbBoard configuration file
    Settings* settings = Settings::instance();
    settings->setConfigPath("C:\\usbBoardReadoutSettings");

    RunDescription* runDescription = Settings::instance()->createRunDescription(0, "");
    runDescription->setComment(pulseGeneratorSettings());

    UsbBoardManager usbBoardManager;
    usbBoardManager.initUsbBoards();
    // Setup MergedEvent storage buffer
    MergedEventPtr* event = new MergedEventPtr[Settings::instance()->eventsPerAccess()];
    for (unsigned int i = 0; i < Settings::instance()->eventsPerAccess(); ++i)
        event[i] = settings->createMergedEvent();

    // Create result data histogram
    std::vector<TH2*> histograms;
    MergedEvent* eventPointer = 0;
    bool abort = false;
    int amplitude = amplitudeMin;
    int amplitudeIndex = 1;

	char histoName[256];
    sprintf(histoName, "pedestal");
    TH2* pedestalHistogram = new TH2I(histoName, histoName, 256, 0, 256, 3000, 0, 3000);
	pedestalHistogram->GetXaxis()->SetTitle("channel");
    pedestalHistogram->GetYaxis()->SetTitle("amplitude / ADC counts");

    while (amplitude <= amplitudeMax && !abort) {
		gpibController->setAmplitude(amplitude);
		std::cerr << amplitude << "mV\n" << std::flush;
        char rootName[256];
        sprintf(rootName, "%04d mV", amplitude);
        TH2* histogram = new TH2I(rootName, rootName, 256, 0, 256, 3000, 0, 3000);
        histogram->GetXaxis()->SetTitle("channel");
        histogram->GetYaxis()->SetTitle("amplitude / ADC counts");
        histograms.push_back(histogram);
        for (int channel = 0; channel < 256; ++channel) {
			switchController.selectChannel(channel);
			usbBoardManager.setBusy();
            usbBoardManager.resetDaq();
            usbBoardManager.setBusy(false);
			for (unsigned long eventCounter = 0; eventCounter < nEventsPerSetting && !abort; eventCounter+= Settings::instance()->eventsPerAccess()) {
                usbBoardManager.takeEvents(event);
                for (unsigned int i = 0; i < Settings::instance()->eventsPerAccess(); ++i) {
                    eventPointer = event[i];
					if (Settings::instance()->eventsPerAccess()==2 && s_uplinkId==1) {//to avoid wrong readout channels for in current firmware
						if (i==1 && (channel==14 || channel==15)) continue;
						if (i==1 && (channel==46 || channel==47)) continue;
						if (i==1 && (channel==78 || channel==79)) continue;
						if (i==1 && (channel==110 || channel==111)) continue;
						if (i==1 && (channel==142 || channel==143)) continue;
						if (i==1 && (channel==174 || channel==175)) continue;
						if (i==1 && (channel==206 || channel==207)) continue;
						if (i==1 && (channel==238 || channel==239)) continue;

						if (i==0 && (channel==30 || channel==31)) continue;
						if (i==0 && (channel==62 || channel==63)) continue;
						if (i==0 && (channel==94 || channel==95)) continue;
						if (i==0 && (channel==126 || channel==127)) continue;
						if (i==0 && (channel==158 || channel==159)) continue;
						if (i==0 && (channel==190 || channel==191)) continue;
						if (i==0 && (channel==222 || channel==223)) continue;
						if (i==0 && (channel==254 || channel==255)) continue;
					}

                    histogram->Fill(channel, (eventPointer->uplinkData(s_uplinkId).adcValue(channel)));

					for (int ch = 0; ch < 256; ++ch) {
						if(ch!=channel) {
							if (Settings::instance()->eventsPerAccess()==2 && s_uplinkId==1) {//to avoid wrong readout channels for in current firmware
								if (i==1 && (ch==14 || ch==15)) continue;
								if (i==1 && (ch==46 || ch==47)) continue;
								if (i==1 && (ch==78 || ch==79)) continue;
								if (i==1 && (ch==110 || ch==111)) continue;
								if (i==1 && (ch==142 || ch==143)) continue;
								if (i==1 && (ch==174 || ch==175)) continue;
								if (i==1 && (ch==206 || ch==207)) continue;
								if (i==1 && (ch==238 || ch==239)) continue;


								if (i==0 && (ch==30 || ch==31)) continue;
								if (i==0 && (ch==62 || ch==63)) continue;
								if (i==0 && (ch==94 || ch==95)) continue;
								if (i==0 && (ch==126 || ch==127)) continue;
								if (i==0 && (ch==158 || ch==159)) continue;
								if (i==0 && (ch==190 || ch==191)) continue;
								if (i==0 && (ch==222 || ch==223)) continue;
								if (i==0 && (ch==254 || ch==255)) continue;
							}
						pedestalHistogram->Fill(ch, (eventPointer->uplinkData(s_uplinkId).adcValue(ch)));
						}
					}
					
					/*
					for (int ch = 0; ch < 256; ++ch)
					if (eventPointer->uplinkData(s_uplinkId).adcValue(ch) > 1500)
						std::cout << i << '\t' << channel << '\t' << ch << (ch != channel ? "<---" : "") << std::endl;
					*/
				}
                if (_kbhit())
                    if (_getch() == 0x1B /* ESC */)
                        abort = true;
            }   
        }
		amplitude = amplitudeMin + amplitudeIndex * amplitudeStep;
        ++amplitudeIndex;
    }
    TFile file(rootFileName, "RECREATE");
    for (std::vector<TH2*>::iterator it = histograms.begin(); it!= histograms.end(); ++it)
        (*it)->Write();
		pedestalHistogram->Write();
    file.Close();

	std::cout<<"histograms saved to file: "<< rootFileName <<std::endl;

    for (unsigned int i = 0; i < Settings::instance()->eventsPerAccess(); ++i)
        delete event[i];

    delete[] event;
    return 0;
}
