//
// Linearity of selected Channel (1..128)
// Real-Time Mode
//

#include <conio.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <windows.h>
#include "decl-32.h"

#define ND      3                            //Number of GPIB devices

#define DG      0                            //Device index DG2020
#define LC      1                            //Device index LC574AM
#define HP      2                            //Device index HP8110A

#define DGPAD   1                            //GPIB primary address DG2020
#define LCPAD   4                            //GPIB primary address LC574AM
#define HPPAD   5                            //GPIB primary address HP8110A

#define MAX     52000
#define PTS     50000                        //50000pts = 100us

#define ASPECT  2.0                          //2ns/pt
#define STB     0.6                          //Data strobe = STB * clock cycle

#define SMAX    64                           //Max number of setup files
#define INITRUN -2                           //Initializing runs

#define AMPL    0                            //Option Index in alphabetic order
#define CHAN    1
#define DELAY   2
#define RUNS    3
#define STEP    4
#define NOPT    5                            //Number of options

struct { char str[ 8 ];
         int  min;
         int  max;
         int  def;
       } option[ NOPT ] = { { "--ampl" , 50, 800, 250 },
                            { "--chan" ,  1, 128,   1 },
                            { "--delay",  0, 150,  47 },
                            { "--runs" ,  1,   0,  10 },
                            { "--step" ,  1, 100,  50 } };

int  board;                                  //Board unit descriptor
int  device[ ND ];                           //Device unit descriptor

char devName[ ND ][ 8 ] = { "DG2020", "LC574AM", "HP8110A" };

int  inverted = 0;                           //Amplitude polarity
int  sptn[ 2 ][ 2048 ];                      //Sample points of negative transitions
int  sptp[ 2 ][ 2048 ];                      //Sample points of positive transitions
int  tstb[ 2 ];                              //Data strobe (in pts)

char dat[ MAX ];                             //Data buffer
char msg[ 240 ] = { 0 };                     //Message buffer
char timing[ ] = "va128rm";                  //DG2020 timing

FILE *fdata, *fstat;

void dgGetReady( );
void dgSetup( );
void hpGetReady( );
void hpSetup( );
void lcGetReady( );
void lcTrgReady( );
void lcSetup( );

void gpibInit( );
void gpibError( char* msg );
void gpibGetData( int devIndex );
void gpibQuery( int devIndex, char* msg );
void gpibSendMsg( int devIndex, char* msg );
void gpibRelease( );

void digitizeData( );
void evalClk( int slice );
int  isNumber( char* str );

int main( int argc, char* argv[ ] )
{
  char   cc, outfile[ 256 ] = "va128rlin.txt";
  int    arg, i, isOption, param[ NOPT ];
  int    cn[ 128 ], clock[ 2 ] = { 0 }, slice;
  int    ampl, chn, run, runs;
  int    bit, delay, mask;
  int    *adc[ 128 ], pedestal;
  int    adcSum, pedSum;
  int    adcMin, runMin = 1;
  int    adcMax, runMax = 1;
  int    aborted = 0, button, dt;
  int    cms = 0, vaChip;
  double adcMean, dsum;
  time_t tStart, tStop;

  for( i = 0; i < NOPT; i++ )
    param[ i ] = option[ i ].def;

  for( arg = 1; arg < argc; arg++ ) {
    isOption = 0;

    for( i = 0; i < NOPT; i++ )
      if( !strcmp( argv[ arg ], option[ i ].str ) )
        if( arg == ( argc - 1 ) ) {
          fprintf( stderr, "%s: %s: Missing Operand\n", argv[ 0 ], argv[ arg ] );
          exit( 1 );
        }
        else {
          if( isNumber( argv[ arg + 1 ] ) ) {
            param[ i ] = atoi( argv[ arg + 1 ] );
            isOption = 1;
          }
          else {
            fprintf( stderr, "%s: %s %s: Bad Operand\n", argv[ 0 ], argv[ arg ], argv[ arg + 1 ] );
            exit( 1 );
          }

          if( param[ i ] < option[ i ].min ) {
              fprintf( stderr, "%s: %s %s: Operand out of range\n", argv[ 0 ], argv[ arg ], argv[ arg + 1 ] );
              exit( 1 );
            }

          if( option[ i ].max > 0 && param[ i ] > option[ i ].max ) {
              fprintf( stderr, "%s: %s %s: Operand out of range\n", argv[ 0 ], argv[ arg ], argv[ arg + 1 ] );
              exit( 1 );
            }

          break;
        }

    if( isOption ) {
      arg++;                                 //option has argument, skip next
      continue;
    }

    if( !strcmp( argv[ arg ], "--cms" ) ) {
      cms = 1;
      continue;
    }

    if( !strcmp( argv[ arg ], "--file" ) ) {
        if( arg == ( argc - 1 ) ) {
          fprintf( stderr, "%s: %s: Missing Operand\n", argv[ 0 ], argv[ arg ] );
          exit( 1 );
        }
        else {
          strcpy( outfile, argv[ arg + 1 ] );
          strcat( outfile, ".txt" );

          arg++;
          continue;
        }
    }

    if( !strcmp( argv[ arg ], "--inv" ) ) {
      inverted = 1;
      continue;
    }

    if( !strcmp( argv[ arg ], "--help" ) ) {
      printf( "%s [OPTION]\n", argv[ 0 ] );

      printf( " --ampl      amplitude range in mV (%d..%d)\n", option[ AMPL ].min, option[ AMPL ].max );
      printf( "             default: %d\n", option[ AMPL ].def );
      printf( " --chan      channel number (%d..%d)\n", option[ CHAN ].min, option[ CHAN ].max );
      printf( "             default: %d\n", option[ CHAN ].def );
      printf( " --cms       common mode subtraction\n" );
      printf( " --delay     trigger delay in ns (%d..%d)\n", option[ DELAY ].min, option[ DELAY ].max );
      printf( "             default: %d\n", option[ DELAY ].def );
      printf( " --file      output filename\n" );
      printf( "             default: va128rlin.txt\n" );
      printf( " --inv       amplitude polarity\n" );
      printf( "             default: normal\n" );
      printf( " --runs      number of runs (1..INTMAX)\n", option[ RUNS ].min );
      printf( "             default: %d\n", option[ RUNS ].def );
      printf( " --step      amplitude step size in mV (%d..%d)\n", option[ STEP ].min, option[ STEP ].max );
      printf( "             default: %d\n\n", option[ STEP ].def );

      printf( " --help      display this help and exit\n" );
      printf( " --version   output version information and exit\n" );
      exit( 0 );
    }

    if( !strcmp( argv[ arg ], "--version" ) ) {
      printf( "%s (2009) Release 1.0\n", argv[ 0 ] );
      printf( "VA-Chip Linearity Measurement (Real-Time Mode)\n" );
      exit( 0 );
    }

    fprintf( stderr, "%s: Unrecognized option: %s\n", argv[ 0 ], argv[ arg ] );
    fprintf( stderr, "Try '%s --help' for more information.\n", argv[ 0 ] );
    exit( 1 );
  }

  if( ( fdata = fopen( "delay.dat", "rt" ) ) == NULL ) {
    fprintf( stderr, "%s: delay.dat: No such file. Recalibrate system!\n", argv[ 0 ] );
    exit( 1 );
  }

  if( fscanf( fdata, "%d", &delay ) != 1 ) {
    fprintf( stderr, "%s: delay.dat: Data format corrupted\n", argv[ 0 ] );
    exit( 1 );
  }

  fclose( fdata );

  if( delay <= 0 ) {
    fprintf( stderr, "%s: Delay error. Recalibrate system!\n", argv[ 0 ] );
    exit( 1 );
  }

  delay = ( int )( delay / ASPECT );         //Time delay in memory points

  if( ( fstat = fopen( outfile, "rt" ) ) != NULL ) {
      sprintf( msg, "%s exists. Overwrite?", outfile );

      button = MessageBox( NULL, msg, "Warning",
                           MB_YESNO | MB_DEFBUTTON2 |
                           MB_ICONWARNING | MB_SETFOREGROUND );

      if( button == IDNO ) {
        printf( "\nEnter new filename (without extension): " );
        Beep( 2400, 60 );
        Beep( 2000, 100 );

        outfile[ i = 0 ] = '\0';

        while( ( cc = getchar( ) ) != '\n' )
          outfile[ i++ ] = cc;

        outfile[ i ] = '\0';

        if( !strcmp( outfile, "" ) )
          exit( 2 );
      }

      fclose( fstat );
  }

  if( ( fstat = fopen( outfile, "wt" ) ) == NULL ) {
    fprintf( stderr, "%s: %s: Permission denied\n", argv[ 0 ], outfile );
    exit( 1 );
  }

  for( chn = 0; chn < 128; chn++ )
    if( ( adc[ chn ] = new int[ param[ RUNS ] ] ) == NULL ) {
      fprintf( stderr, "Memory allocation error (adc values)\n" );
      exit( 1 );
    }

  vaChip = param[ CHAN ] / 33;
  fprintf( fstat, "//VA-Chip Linearity Measurement (Real-Time Mode)\n" );
  fprintf( fstat, "//Selected Channel: %d (Chip %c)\n", param[ CHAN ], 'A' + vaChip );
  fprintf( fdata, "//Trigger Delay   : %dns\n", param[ DELAY ] );

  if( cms ) {
    fprintf( fstat, "//Common Mode Substraction\n" );
    fprintf( fstat, "\nVIn[mV]  Pedestal     Mean    Min    Max  StdDev  Runs\n" );
  }
  else
    fprintf( fstat, "\nVIn[mV]     Mean    Min    Max  StdDev  Runs\n" );
    
  for( chn = 0; chn < 64; chn++ )
    cn[ chn ] = chn;

  for( chn = 64; chn < 96; chn++ )
    cn[ chn ] = 159 - chn;

  for( chn = 96; chn < 128; chn++ )
    cn[ chn ] = 223 - chn;

  gpibInit( );
  dgSetup( );
  lcSetup( );
  hpSetup( );

  printf( "\nPress <ESC> to abort measurement (data will be saved)\n\n" );

  time( &tStart );
  gpibSendMsg( DG, ":START" );
  dgGetReady( );

  if( param[ DELAY ] != option[ DELAY ].def ) {
    sprintf( msg, ":PULS:DEL2 %dNS", param[ DELAY ] );
    gpibSendMsg( HP, msg );
  }

  for( ampl = 0; ampl <= param[ AMPL ]; ampl += param[ STEP ] ) {
    if( ampl > 0 && ampl < 50 )
      continue;

    if( ampl != 0 ) {
      sprintf( msg, ":VOLT1:HIGH %dMV", ampl );
      gpibSendMsg( HP, msg );
      gpibSendMsg( HP, ":OUTP1 ON" );
      hpGetReady( );
    }

    adcMin =  4096;
    adcMax = -4096;
    adcSum =  0;
    pedSum =  0;

    for( run = INITRUN; run < param[ RUNS ]; run++ ) {
      for( slice = 0; slice < 2; slice++ ) {
        gpibSendMsg( LC, "ARM" );
        lcTrgReady( );

        gpibSendMsg( DG, "*TRG" );
        gpibSendMsg( LC, "WAIT" );

        if( run == INITRUN )
          evalClk( slice );
        else {
          gpibSendMsg( LC, "C3:WF?" );       //ADC values of 64 VA channels
          gpibGetData( LC );
          digitizeData( );
        }

        if( run < 0 ) {
          printf( "\rInitializing..." );
          continue;
        }

        for( chn = 0; chn < 64; chn++ ) {
          i = ( slice * 64 ) + chn;
          adc[ cn[ i ] ][ run ] = 0;

          for( mask = 1, bit = 11; bit >= 0; bit--, mask <<= 1 )
            adc[ cn[ i ] ][ run ] +=
              dat[ sptp[ slice ][ ( chn * 16 ) + bit ] + delay + tstb[ slice ] ] * mask;
        }
      }

      if( run < 0 )
        continue;

      if( cms ) {
        pedestal = 0;

        for( chn = vaChip * 32; chn < ( vaChip + 1 ) * 32; chn++ )
          pedestal += adc[ chn ][ run ];

        if( ampl == 0 )
          pedestal = ( int )( ( double ) pedestal / 32 + 0.5 );
        else {
          pedestal -= adc[ param[ CHAN ] - 1 ][ run ];
          pedestal = ( int )( ( double ) pedestal / 31 + 0.5 );
        }

        for( chn = vaChip * 32; chn < ( vaChip + 1 ) * 32; chn++ )
          adc[ chn ][ run ] -= pedestal;

        pedSum += pedestal;
      }

      adcSum += adc[ param[ CHAN ] - 1 ][ run ];

      if( adc[ param[ CHAN ] - 1 ][ run ] < adcMin ) {
        adcMin = adc[ param[ CHAN ] - 1 ][ run ];
        runMin = run + 1;
      }

      if( adc[ param[ CHAN ] - 1 ][ run ] > adcMax ) {
        adcMax = adc[ param[ CHAN ] - 1 ][ run ];
        runMax = run + 1;
      }

      printf( "\rChn: %d, VIN: %3dmV, adcValue: %4d, min(%d): %d, max(%d): %d, Run: %d     ",
              param[ CHAN ], ampl, adc[ param[ CHAN ] - 1 ][ run ], runMin, adcMin, runMax, adcMax, run + 1 );

      if( _kbhit( ) ) {
        cc = _getch( );

        if( cc == 0x1B ) {
          aborted = 1;
          break;
        }
      }
    }

    printf( "\n" );

    runs = ( aborted ) ? run + 1 : param[ RUNS ];
    adcMean = ( double )( adcSum ) / runs;

    if( cms )
      fprintf( fstat, "%6d   %8.1f %8.1f  %5d  %5d",
               ampl, ( double )( pedSum ) / runs, adcMean, adcMin, adcMax );
    else
      fprintf( fstat, "%6d  %8.1f  %5d  %5d", ampl, adcMean, adcMin, adcMax );

    if( runs > 1 ) {
      dsum = 0.0;

      for( run = 0; run < runs; run++ )
        dsum += ( adc[ param[ CHAN ] - 1 ][ run ] - adcMean ) * ( adc[ param[ CHAN ] - 1 ][ run ] - adcMean );

      fprintf( fstat, "  %6.1f", sqrt( dsum / ( runs - 1 ) ) );
    }
    else
      fprintf( fstat, "  %6s", "--" );

    fprintf( fstat, "   %d\n", runs );

    if( aborted )
      break;
  }

  fclose( fstat );

  gpibSendMsg( DG, ":STOP" );
  gpibRelease( );

  for( chn = 0; chn < 128; chn++ )
    delete[ ] adc[ chn ];

  time( &tStop );
  dt = ( int ) difftime( tStop, tStart );

  printf( "\nElapsed time: %02d:%02d:%02d\n",
          dt / 3600, ( dt % 3600 ) / 60, ( dt % 3600 ) % 60 );

  return 0;
}


////////////////////////////////////////
//
// DG2020 Functions and Timing Blocks
//
////////////////////////////////////////
//
// 0  TRIGGER
// 1  HOLD_CLK
// 2  VA_RD_B
// 3  RESET
//
////////////////////////////////////////

void dgGetReady( )
{
  do {
    gpibSendMsg( DG, "*OPC?" );              //Query waits until all pending operations are finished
    gpibGetData( DG );
    dat[ ibcntl ] = '\0';
  } while( !( atoi( dat ) ) );
}

void dgSetup( )
{
  int  fid = 0, qw = 0;
  int  flist[ SMAX ], length = 0, i, k;
  char file[ SMAX ][ 16 ], cc;

  gpibSendMsg( DG, ":STOP" );
  gpibSendMsg( DG, "*ESE 1" );               //Enable OPC (Operation complete) event for
                                             //ESB (Event Status Bit)
  gpibSendMsg( DG, "MMEM:CAT?" );            //Get all files and directories
                                             //Data format: "File name",Size,"Time Stamp"
  gpibGetData( DG );

  dat[ ibcntl ] = '\0';
  strlwr( dat );

  for( k = 0, i = 0; i < ( int ) strlen( dat ); i++ )
    if( dat[ i ] == '\"' ) {
      qw++;

      if( qw == 4 ) {
        file[ fid ][ k ] = '\0';
        fid++;

        if( fid == SMAX ) {
          fprintf( stderr, "Too many setup files\n" );
          gpibRelease( );
          exit( 1 );
        }

        qw = k = 0;
      }
    }
    else if( qw == 1 )
           file[ fid ][ k++ ] = dat[ i ];

                                             //Constraints: *va128rm*.pda
  for( i = 0; i < fid; i++ )
    if( strstr( file[ i ], timing ) && strstr( file[ i ], ".pda" ) )
      flist[ length++ ] = i;

  if( length == 0 ) {
    fprintf( stderr, "No DG2020 setup file(s)\n" );
    gpibRelease( );
    exit( 1 );
  }

  if( length == 1 ) {
    printf( "Loading '%s'...", file[ flist[ 0 ] ] );
    sprintf( msg, "MMEM:LOAD \"%s\"", file[ flist[ 0 ] ] );
  }
  else {
    printf( "Enter your DG2020 setup selection:\n\n" );

    for( i = 0; i < length; i++ ) {
      file[ flist [ i ] ][ strlen( file[ flist[ i ] ] ) - 4 ] = '\0';
      printf( " (%c)  %s\n", 'a' + i, file[ flist[ i ] ] );
    }

    printf( "\n" );

    while( _kbhit( ) )
      getch( );                              //Flush keyboard buffer

    do {
      while( !_kbhit( ) );
      cc = getch( );
    } while( cc < 'a' || cc >= 'a' + length );

    printf( "Loading '%s.pda'...", file[ flist[ cc - 'a' ] ] );
    sprintf( msg, "MMEM:LOAD \"%s.pda\"", file[ flist[ cc - 'a' ] ] );
  }

  gpibSendMsg( DG, msg );
  dgGetReady( );

  printf( " ok.\n" );
}


////////////////////////////////////////
//
// HP8110A Functions
//
////////////////////////////////////////

void hpGetReady( )
{
  do {
    gpibSendMsg( HP, "*OPC?" );              //Query waits until all pending operations are finished
    gpibGetData( HP );
    dat[ ibcntl ] = '\0';
  } while( !( atoi( dat ) ) );
}

void hpSetup( )
{
  gpibSendMsg( HP, ":ARM:SOUR EXT1" );
  gpibSendMsg( HP, ":ARM:SENS EDGE" );
  gpibSendMsg( HP, ":ARM:SLOP NEG" );
  gpibSendMsg( HP, ":TRIG:COUN 1" );
  gpibSendMsg( HP, ":TRIG:SOUR INT1" );

  gpibSendMsg( HP, ":PULS:WIDT1 10US" );
  gpibSendMsg( HP, ":PULS:DEL1 0NS" );
  gpibSendMsg( HP, ":PULS:WIDT2 50NS" );

  sprintf( msg, ":PULS:DEL2 %dNS", option[ DELAY ].def );
  gpibSendMsg( HP, msg );

  gpibSendMsg( HP, ":HOLD VOLT" );
  gpibSendMsg( HP, ":VOLT1:HIGH 50MV" );
  gpibSendMsg( HP, ":VOLT1:LOW 0V" );

  if( !inverted )
    gpibSendMsg( HP, ":OUTP1:POL NORM" );
  else
    gpibSendMsg( HP, ":OUTP1:POL INV" );

  gpibSendMsg( HP, ":OUTP1 OFF" );
  gpibSendMsg( HP, ":VOLT2:HIGH 0V" );
  gpibSendMsg( HP, ":VOLT2:LOW -800MV" );
  gpibSendMsg( HP, ":OUTP2:POL INV" );
  gpibSendMsg( HP, ":OUTP2 ON" );

  hpGetReady( );
}


////////////////////////////////////////
//
// LC574AM Functions
//
////////////////////////////////////////

void lcGetReady( )
{
  do {
    gpibSendMsg( LC, "*OPC?" );              //Query waits until all pending operations are finished
    gpibGetData( LC );
    dat[ ibcntl ] = '\0';
  } while( !( atoi( dat ) ) );
}

void lcTrgReady( )
{
  int mask = 8192;

  do {
    gpibSendMsg( LC, "INR?" );               //Trigger ready?
    gpibGetData( LC );
    dat[ ibcntl ] = '\0';
  } while( !( atoi( dat ) & mask ) );
}

void lcSetup( )
{
  gpibSendMsg( LC, "STOP" );                 //Stop acquisition
  gpibSendMsg( LC, "CFMT DEF9,BYTE,BIN" );   //Transmission format
  gpibSendMsg( LC, "CHDR OFF" );             //Response format (Numbers only)
  gpibSendMsg( LC, "COMB 2" );               //Combine Channels
  gpibSendMsg( LC, "TRSE SNG,SR,C2" );
  gpibSendMsg( LC, "C2:TRA ON" );
  gpibSendMsg( LC, "C2:VDIV 1V" );
  gpibSendMsg( LC, "C2:OFST 0V" );
  gpibSendMsg( LC, "C2:TRCP DC" );
  gpibSendMsg( LC, "C2:TRSL NEG" );
  gpibSendMsg( LC, "C2:TRLV 1.2V" );
  gpibSendMsg( LC, "C3:TRA ON" );
  gpibSendMsg( LC, "C3:VDIV 1V" );
  gpibSendMsg( LC, "C3:OFST -3V" );
  gpibSendMsg( LC, "TDIV 10US" );
  gpibSendMsg( LC, "TRDL 10PCT" );

  sprintf( msg, "MSIZ %d", PTS );
  gpibSendMsg( LC, msg );
                                             //Send all data points
  sprintf( msg, "WFSU SP,0,NP,%d,FP,0,SN,0", PTS );
  gpibSendMsg( LC, msg );

  lcGetReady( );

  printf( "Scope channel 2: Clock\n" );
  printf( "Scope channel 3: ADC values\n" );
}


////////////////////////////////////////
//
// GPIB Commands
//
////////////////////////////////////////





////////////////////////////////////////
//
// Auxiliary Functions
//
////////////////////////////////////////

void digitizeData( )
{
  int  i;
  char avg, min, max;

  min = max = dat[ 367 ];                    //First data byte

  for( i = 368; i < ( ibcntl - 1 ); i++ ) {
    if( dat[ i ] < min )
      min = dat[ i ];

    if( dat[ i ] > max )
      max = dat[ i ];
  }

  avg = ( char )( ( min + max ) / 2 );

  for( i = 367; i < ( ibcntl - 1 ); i++ )
    if( dat[ i ] > avg && ( max - min ) > 32 )
      dat[ i ] = ( char )( 1 );              //Minimal output voltage = 32pts (1V)
    else
      dat[ i ] = ( char )( 0 );
}

void evalClk( int slice )
{
  int  i, neg = 0, pos = 0;
  char level;

  gpibSendMsg( LC, "C2:WF?" );
  gpibGetData( LC );
  digitizeData( );

  level = dat[ 367 ];                        //First data point

  for( i = 368; i < ( ibcntl - 1 ); i++ )
    if( dat[ i ] != level ) {
      if( dat[ i - 1 ] )
        sptn[ slice ][ neg++ ] = i;
      else
        sptp[ slice ][ pos++ ] = i;

      level = dat[ i ];
    }

  if( neg != 1024 || pos != 1024 ) {         //1024 Transitions
    fprintf( stderr, "\nClock error in slice %d\n", slice );
    gpibRelease( );
    exit( 1 );
  }

  tstb[ slice ] = ( int )( ( sptp[ slice ][ 15 ] - sptp[ slice ][ 0 ] ) / 15.0 * STB + 0.5 );

  gpibSendMsg( LC, "C2:TRA OFF" );
  gpibSendMsg( LC, "C3:WF?" );               //ADC value of selected VA channel
  gpibGetData( LC );
  digitizeData( );
}

int isNumber( char* str )
{
  for( int i = 0; i < ( int ) strlen( str ); i++ )
    if( str[ i ] < '0' || str[ i ] > '9' )
      return 0;

  return 1;
}
