#include "DataManager.h"
#include <Settings.h>

#include <assert.h>
#include <iostream>

#include <MergedEvent.h>
#include <UsbBoardConfiguration.h>

#include <TH1.h>
#include <TH2.h>
#include <TH2F.h>
#include <TF1.h>
#include <TH3.h>
#include <TProfile.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TMath.h>
#include <TObjArray.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TGraphSmooth.h>
#include "TVector.h"

#include <TRandom3.h>
#include <TSpectrum.h>
#include <TVirtualFitter.h>
#include <TROOT.h>
#include <TStyle.h>

#define CANVAS_X  1400
#define CANVAS_Y  1000

void DataManager::ConfigHistoTH1(TH1* Histo, TString Title, TString axeXname, TString axeYname, Double_t axeXmin, Double_t axeXmax, double axeYmin, double axeYmax, double LineWidth)
{
    if(LineWidth) Histo->SetLineWidth(LineWidth); // 1 or 2
    if(Title != "") Histo->SetTitle(Title);
    if(Title != "") Histo->GetXaxis()->SetTitle(axeXname);
    if(Title != "") Histo->GetYaxis()->SetTitle(axeYname);
    if(axeXmax) Histo->SetAxisRange(axeXmin, axeXmax, "x");
    if(axeYmax) Histo->SetAxisRange(axeYmin, axeYmax, "y");

    Histo->SetTitleSize(0.06, "x");
    Histo->SetTitleSize(0.06, "y");
    Histo->SetTitleOffset(1, "x");
    Histo->SetTitleOffset(1, "y");
}

void DataManager::ConfigHistoTH2(TH2* Histo, TString Title, TString axeXname, TString axeYname, Double_t axeXmin, Double_t axeXmax, double axeYmin, double axeYmax)
{
    if(Title != "") Histo->SetTitle(Title);
    if(Title != "") Histo->GetXaxis()->SetTitle(axeXname);
    if(Title != "") Histo->GetYaxis()->SetTitle(axeYname);
    if(axeXmax) Histo->SetAxisRange(axeXmin, axeXmax, "x");
    if(axeYmax) Histo->SetAxisRange(axeYmin, axeYmax, "y");

    Histo->SetTitleSize(0.06, "x");
    Histo->SetTitleSize(0.06, "y");
    Histo->SetTitleOffset(1, "x");
    Histo->SetTitleOffset(1, "y");
}

// Cluster results histogram
void DataManager::Cluster_DrawHisto(bool noise_cluster_option)
{
	// displays clustering plots for seed threshold = 2.5 p.e.
   
  std::cout << "*****>>> DataManager::" << __func__ << "() *****" << std::endl;
  char tmp[256];
  int Xmax;

  sprintf(tmp,"Cluster number per event");
  TCanvas* canvas1 = new TCanvas(tmp,tmp,0,0,CANVAS_X, CANVAS_Y);
  canvas1->Divide(2,1);
  canvas1->cd(1);
  m_NbCluster_Tot->Draw("");
  Xmax = m_NbCluster_Tot->FindLastBinAbove(0, 1);
  ConfigHistoTH1(m_NbCluster_Tot, "Number of cluster per event", "Number of cluster","Number of Entries", 0, Xmax+5, 0, 0, 2);
  canvas1->cd(2);
  m_ClusterSize_Tot->Draw("");
  Xmax = m_ClusterSize_Tot->FindLastBinAbove(0, 1);
  ConfigHistoTH1(m_ClusterSize_Tot, "Cluster size all SiPm", "Cluster size", "Number of Entries", 0, Xmax+1, 0, 0, 2);

  ofstream myfile;
  myfile.open("light_yield_file.txt", std::ios_base::app); 

  for( unsigned int plot_index = 0; plot_index <  m_NumberOfSipm;plot_index++){
    std::string ctitle = "Clusterization Histogram SiPM " + m_SipmNames[plot_index];
    TCanvas* canvas = new TCanvas(ctitle.c_str(),ctitle.c_str(),0,0,CANVAS_X, CANVAS_Y);
    canvas->Divide(2,2);
    
    canvas->cd(1);
    m_NbCluster[plot_index]->Draw("");
    Xmax = m_NbCluster[plot_index]->FindLastBinAbove(0, 1); // (thrsd, 1=Xaxis)
    ConfigHistoTH1(m_NbCluster[plot_index], "Number of cluster", "Number of cluster","Number of Entries", 0, Xmax+5, 0, 0, 2);
    
    canvas->cd(2);
    m_ClusterSize[plot_index]->Draw("");
    Xmax = m_ClusterSize[plot_index]->FindLastBinAbove(0, 1);
    ConfigHistoTH1(m_ClusterSize[plot_index], "Cluster size", "Cluster size","Number of Entries", 0, Xmax+1, 0, 0, 2);
    
    canvas->cd(3);
    m_ClusterSum[plot_index]->Draw("");
    Xmax = m_ClusterSum[plot_index]->FindLastBinAbove(0, 1);
    ConfigHistoTH1(m_ClusterSum[plot_index], "Cluster sum", "Cluster Sum","Number of Entries", 0, Xmax, 0, 0, 1);
    
    double mean=m_ClusterSum[plot_index]->GetMean();
    TF1 *f1 = new TF1("f1","gaus",mean-15,mean+15); //Change range for fit of MPV
    m_ClusterSum[plot_index]->Fit("f1","R");
    mean= f1->GetParameter(1);
    int sipm = plot_index +1;
    myfile << sipm << " " << mean << "\n"; 
    sprintf(tmp,"Light Yield of %.1f",mean);
    TText *t = new TText(70,mean,tmp);
    t->Draw();
    //m_ClusterSum[plot_index]->SaveAs("clustersum.root");
    
    canvas->cd(4);
    m_ClusterChannel[plot_index]->Draw("");
    Xmax = m_ClusterChannel[plot_index]->FindLastBinAbove(0, 1);
    ConfigHistoTH1(m_ClusterChannel[plot_index], "Cluster channel", "Cluster channel","Number of Entries", 0, Xmax, 0, 0, 1);
  }
  //Get Data to txt file (decoment also myfile= open before loop)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
    myfile.close();


    //canvas->SaveAs("clusterhistos.eps");

      
	unsigned int plot_index = 0;
        // displays clustering plots for seed threshold = 2.5 p.e.  
	if(noise_cluster_option) {
	  plot_index = 4;
        } 
    TH1D* m_ClusterDisplay_mean;
    if(1) {
		
		sprintf(tmp,"Cluster Display 1");
		TCanvas* cDis1 = new TCanvas(tmp,tmp,0,0,1000,600);
		m_ClusterDisplay[plot_index]->Draw("COLZ");
		m_ClusterDisplay[plot_index]->GetXaxis()->SetTitle("Channel");
		m_ClusterDisplay[plot_index]->GetYaxis()->SetTitle("P.E. per channel");
		cDis1->SetLogz();
		
		sprintf(tmp,"Cluster Display 2");
		TCanvas* cDis2 = new TCanvas(tmp,tmp,0,0,1000,600);
		m_ClusterDisplay_mean = new TH1D("Cluster Display Mean","", 15, -7.5, 7.5);
		for(unsigned int bin(0); bin < 15; ++bin) {
			m_ClusterDisplay_mean->SetBinContent(bin+1,m_ClusterDisplay[plot_index]->ProjectionY("m_ClusterDisplay_PY", bin+1, bin+1)->GetMean());
		}
		m_ClusterDisplay_mean->SetYTitle("Average P.E. per channel");
		m_ClusterDisplay_mean->SetXTitle("Channel");
		m_ClusterDisplay_mean->SetTitle("Average number of P.E. per channel");
		m_ClusterDisplay_mean->SetLineColor(4);
		m_ClusterDisplay_mean->SetStats(0);
		m_ClusterDisplay_mean->DrawCopy();
		
		//m_ClusterDisplay[plot_index]->SaveAs("ClusterDisplay_all.root");
		//m_ClusterDisplay_mean->SaveAs("ClusterDisplay_mean.root");
	}
	
	TGraph *NCR;
	if(noise_cluster_option) {
		// Analysis of noise cluster rate
		sprintf(tmp,"Noise Cluster Rate");
		TCanvas* cCluNoi = new TCanvas(tmp,tmp,0,0,1000,600);
		double seed_thrs[17];
		double noise_cluster_rate[17];
		for(unsigned int step_nb(0); step_nb<17; ++step_nb) {
			seed_thrs[step_nb] = 1.5 + 0.25*step_nb;
			noise_cluster_rate[step_nb] = 0.0;
			for(unsigned int bin(2); bin<=10; ++bin) {
				// adds up all clusters
				noise_cluster_rate[step_nb] += (bin-1)*m_NbCluster[step_nb]->GetBinContent(bin);
			}
			noise_cluster_rate[step_nb] = 40*noise_cluster_rate[step_nb]/m_NbCluster[step_nb]->GetEntries();
		}
		NCR = new TGraph(17,&seed_thrs[0],&noise_cluster_rate[0]);
		NCR->Draw("AP");
		NCR->GetXaxis()->SetTitle("Seed threshold [p.e.]");
		NCR->GetYaxis()->SetTitle("Noise cluster rate [MHz]");
		//NCR->SaveAs("Noise_Cluster_Rate.root");
	}
	
	TGraph* DCR_prob;
    TGraph* Xtalk_15;
    TGraph* Xtalk_25;
    double _dcrprob[128], _xtalk15[128], _xtalk25[128], _channel[128];
    double proba_sum(0.0), proba_ind(0), mean(0.0);
	if(0) { // calculates DCR probability and X-talk
		for(int channel=0; channel<128; channel++) { // only first uplink
			//cout << "channel " << channel << ": " << (double)m_Nabove05[channel]/m_NbCluster[plot_index]->GetEntries() << " " << (double)m_Nabove15[channel]/m_Nabove05[channel] << " " << (double)m_Nabove25[channel]/m_Nabove15[channel] << endl;
			TH1D* hist = m_PS_adcHistogram[0]->ProjectionY("m_PS_adcHistogram_PY", channel+1, channel+1);
			//cout << "channel " << channel << ": " << hist->GetBinContent(hist->FindBin(-1)) << " " << hist->GetBinContent(hist->FindBin(0)) << " " << hist->GetBinContent(hist->FindBin(1)) << " " << (int)(hist->GetBinContent(hist->FindBin(1)) + hist->GetBinContent(hist->FindBin(-1)))/2 << endl;
			_channel[channel] = channel;
			int bug_correction = hist->GetBinContent(hist->FindBin(0)) - (int)((hist->GetBinContent(hist->FindBin(1)) + hist->GetBinContent(hist->FindBin(-1)))/2);
			if(bug_correction<0) bug_correction=0;
			// modified to take into account the bug at 0
			_dcrprob[channel] = (double)m_Nabove05[channel]/(m_NbCluster[plot_index]->GetEntries()-bug_correction);
			if(channel == 125) _dcrprob[channel] = 0;	// noisy channel
			if(_dcrprob[channel] == 0) {
				_dcrprob[channel] = NAN;
				_xtalk15[channel] = NAN;
				_xtalk25[channel] = NAN;
			} else {
				_xtalk15[channel] = 100.0*(double)m_Nabove15[channel]/m_Nabove05[channel];	// f(>1.5pe)/f(>0.5pe)
				_xtalk25[channel] = 100.0*(double)m_Nabove25[channel]/m_Nabove15[channel];	// f(>2.5pe)/f(>1.5pe)	which should be the same as f(>1.5pe)/f(>0.5pe) if no random overlap
				double only_xtalk = 10.5164;
				_dcrprob[channel] = _dcrprob[channel]/(1.0 - 0.01*(_xtalk15[channel]-only_xtalk));	// Corrects DCR for random overlap
				//_dcrprob[channel] = _dcrprob[channel]/(1.0 - 0.01*_xtalk15[channel]);
				//_dcrprob[channel] = _dcrprob[channel]/(0.9 - 0.01*_xtalk15[channel]);
				//proba_sum += _dcrprob[channel];
				proba_sum += _dcrprob[channel]/(1.0 - 0.01*_xtalk15[channel]);
				proba_ind +=1;
				mean += _xtalk15[channel];
			}
		}
		sprintf(tmp,"DCR and Xtalk");
		TCanvas* cXtalk = new TCanvas(tmp,tmp,0,0,1400,600);
		cXtalk->Divide(2,1);
		DCR_prob = new TGraph(128,&_channel[0],&_dcrprob[0]);
		DCR_prob->GetXaxis()->SetTitle("Channel");
		DCR_prob->GetYaxis()->SetTitle("Dark count probability");
		DCR_prob->SetMarkerColor(kBlue);
		DCR_prob->SetTitle("Dark count rate");
		DCR_prob->GetXaxis()->SetRangeUser(0,127);
		DCR_prob->SetName("DCR_prob");
		Xtalk_15 = new TGraph(128,&_channel[0],&_xtalk15[0]);
		Xtalk_15->GetXaxis()->SetTitle("Channel");
		Xtalk_15->GetYaxis()->SetTitle("X-talk [%]");
		Xtalk_15->SetMarkerColor(kRed);
		Xtalk_15->SetTitle("Cross-talk");
		Xtalk_15->GetXaxis()->SetRangeUser(0,127);
		Xtalk_15->SetName("Xtalk_15");
		Xtalk_25 = new TGraph(128,&_channel[0],&_xtalk25[0]);
		Xtalk_25->GetXaxis()->SetTitle("Channel");
		Xtalk_25->GetYaxis()->SetTitle("X-talk [%]");
		Xtalk_25->SetMarkerColor(kGreen);
		Xtalk_25->SetTitle("Cross-talk");
		Xtalk_25->GetXaxis()->SetRangeUser(0,128);
		Xtalk_25->SetName("Xtalk_25");
		cXtalk->cd(1);
		DCR_prob->Draw("AP");
		cXtalk->cd(2);
		Xtalk_15->Draw("AP");
		Xtalk_25->Draw("P+");
		TLegend* leg = new TLegend(0.7,0.8,0.9,0.9);
		leg->AddEntry(Xtalk_15,"f_{>1.5 p.e.}/f_{>0.5 p.e.}","p");
		leg->AddEntry(Xtalk_25,"f_{>2.5 p.e.}/f_{>1.5 p.e.}","p");
		leg->SetFillColor(0);
		leg->Draw();
		cout << endl << "******  probability sum for DCR and x-talk check = " << proba_sum*(128.0/proba_ind) << "  ******" << endl;
		cout << endl << "******  mean R15 = " << mean/proba_ind << "  ******" << endl;
	}
	
	/*if(1) {
		
		sprintf(tmp,"Cluster Distance");
		TCanvas* cDist = new TCanvas(tmp,tmp,0,0,1000,600);
		m_ClusterDistance[plot_index]->DrawCopy();
		m_ClusterDistance[plot_index]->GetXaxis()->SetTitle("Position");
		m_ClusterDistance[plot_index]->GetYaxis()->SetTitle("Entries");
		m_ClusterDistance[plot_index]->SetTitle("Position of secondary clusters with respect to main cluster");
	}*/
	
	if(0) {//save cluster histos
		
		TFile* recordFile =  new TFile("cluster_histograms.root","RECREATE");
		//TFile* recordFile =  new TFile("dcr_haistograms.root","RECREATE");
		
		if(1) {
			m_NbCluster[plot_index]->Write();
			m_ClusterSize[plot_index]->Write();
			m_ClusterSum[plot_index]->Write();
			m_ClusterChannel[plot_index]->Write();
			m_ClusterDisplay[plot_index]->Write();
			m_ClusterDisplay_mean->Write();
		}
		if(noise_cluster_option) NCR->Write();
		
		// DCR and X-talk
		if(0) {
			DCR_prob->Write();
			Xtalk_15->Write();
			Xtalk_25->Write();
			TVectorD v(1);
			v[0] =  proba_sum*(128.0/proba_ind);
			v.Write("correction_factor");
		}
			
	    recordFile->Close();
	    //recordFile->SaveAs("cluster_histograms.root");
	}
}

    void DataManager::Cluster_FillTree(){m_TrackingTree->Fill();}

    void DataManager::Cluster_InitBranches(){    
    ClusterPosition.clear();
    SiPMNum.clear();
    EventNumber=-1;}


// Search Cluster Algorithm (3 Thresholds: Seed, Neighb, Sum)
void DataManager::Cluster_Search(std::vector<std::vector<int> > &eventADC, float threshold[3], int id, unsigned int thrs_id){


    bool    EndOfCluster = false;
    int     clusterSize = 0;
    int     clusterSizeMAX = 15;
    int     nbCluster = 0;
    int     first_ch_in_cluster = 0;


	EventNumber=id;
    int     NumberChannels = 0;
    double   sum = 0;

    vector<int> clusterSize_fin;
    vector<int> first_ch_in_cluster_vec;
    vector<double> sum_vec;
    vector<double> mean_position_vec;
    vector<double> mean_position_write;

    double   eventPE[SIPM_READOUT_CHANNELS];
    double   eventPE_copy[SIPM_READOUT_CHANNELS];
    double   SeedCutPE = threshold[0];
    double   NeighborCutPE = threshold[1];
    double   SumCutPE = threshold[2];

    double mean_position = 0.0;

    double mean_position_num = 0.0;
    double mean_position_den = 0.0;
    
    double cluster_display_tmp[15] = {0};
    double cluster_display[15] = {0};

    //std::cout << "NumberChannels = " << m_NumberChannels << std::endl;
    //std::cout << "Event_n = " << EventNr << std::endl;

    //unsigned short numberOfReadoutChips = eventADC.size();
    unsigned int chosen_chip = thrs_id;
    unsigned short numberOfReadoutChips = 1;


    ofstream myfile;
    // myfile.open("example_file.txt", std::ios_base::app);
    bool cleandata = true;

    //std::vector<TH1*> EventCluster;
    //m_EventCluster.push_back(EventCluster);
    char tmp[128];

    for (unsigned int chip(chosen_chip); chip < chosen_chip+numberOfReadoutChips; ++chip) {
     
      /* //For single event graphing 
      std::vector<int> uplinkId;
      uplinkId = m_Sipm_uplinkId.at(chip);
      sprintf(tmp, "Single Event, uplink %d %d", uplinkId.at(0), uplinkId.at(1));
      TH1D* EventCluster_S = new TH1D(tmp, "Event Cluster", SIPM_READOUT_CHANNELS, 1, SIPM_READOUT_CHANNELS);
      EventCluster.push_back(EventCluster_S);
      */
      int chipclusters=0;
      SiPMNum.push_back(chip);
      double positiontemp;

      int clusters = 0;
        NumberChannels = SIPM_READOUT_CHANNELS;
        
        // rescale data from ADC to PE
        for (int channel=0; channel<NumberChannels; channel++){
            eventPE[channel] = eventADC.at(0)[channel] / m_SipmGains[chip];
            eventPE_copy[channel] = eventPE[channel];
        }
	
	//Creation of event histograms                                                                                                
	//for (int channel=0; channel<NumberChannels; ++channel){
	//EventCluster_S->Fill(channel+1,eventPE[channel]);
	//}
 
        // For the computation of DCR and X-talk:
        if(1) {
			for (int channel=0; channel<NumberChannels; channel++){
	            if(eventPE[channel] > 0.5) {
					++m_Nabove05[channel];
				}
				if(eventPE[channel] > 1.5) {
					++m_Nabove15[channel];
				}
				if(eventPE[channel] > 2.5) {
					++m_Nabove25[channel];
				}
	        }
		}
        
        //cout << chip << endl;
        //--------------------------------------------------------------
        // Generates a random array of noise signals
        //--------------------------------------------------------------
        /*TRandom3* rnd_gen = new TRandom3(0);
		double data[128];
        rnd_gen->RndmArray(128, data);
        for (int channel=0; channel<128; channel++){
			eventPE[channel] = m_Rndm_graph->Eval(data[channel]) / m_SipmGains[chip];
		}*/
		//--------------------------------------------------------------
        
        // Killing two channels atrificially
        /*if(chip==1) {
            eventPE[42] = 0;
            eventPE[41] = 0;
        }*/

        // Print the active channels
        //std::cout << "signalPE raw: ["<< chip <<"][Channel]Value :";
        //for (int channel=0; channel<NumberChannels; channel++)
        //  if (eventPE[channel] >= 1.5)
        //      std::cout << "["<< channel << "]" << eventPE[channel] <<" ";
        //std::cout << std::endl;
        
        // Kills artificially channel 125 which doesn't work for spiroc
        //eventPE[125] = 0;
        

        // cluster search, start to find channels passing the seed cut
        for (int channel=0; channel<NumberChannels; ++channel){

            if (eventPE[channel] >= SeedCutPE){                                             // Search for seed threshold
                ++clusterSize;
            }
            else if ((clusterSize >= 1 )){                                                  // no more seeds in cluster
                EndOfCluster=true;
                first_ch_in_cluster = channel - clusterSize;
            }

            if ((clusterSize == 1) && (channel == NumberChannels - 1)){                     // last channel reached (Check for single channel Cluster)
                if(eventPE[channel] >= SeedCutPE) {
					first_ch_in_cluster = channel;
				}
                EndOfCluster=true;
            }

            if ((clusterSize > 1 ) && (channel == NumberChannels - 1)){                     // Looking for Cluster with the last channel included
                EndOfCluster=true;
                if(eventPE[channel] >= SeedCutPE) {
					first_ch_in_cluster = channel - clusterSize + 1;
				}
            }

            if ((clusterSize == clusterSizeMAX - 2) && (!EndOfCluster))  {                  // maximum size reached
                ++channel;                                                                  // go to next channel to search for neighbour threshold
                first_ch_in_cluster = channel - clusterSize;                                // NOTE: Cluster with the size greater than clusterSizeMAX will be breaked
                EndOfCluster=true;
            }

            // cluster finnished so we need to add the neighbors
            if (EndOfCluster){

                if(first_ch_in_cluster > 0){                                                // check left neighbour
                    if (eventPE[first_ch_in_cluster - 1] >= NeighborCutPE){
                        ++clusterSize;
                        first_ch_in_cluster = first_ch_in_cluster - 1;
                    }
                }
                if(channel == NumberChannels - 1){  
					if(channel == first_ch_in_cluster+clusterSize+1) {
						if (eventPE[channel] >= NeighborCutPE){
							++clusterSize;
						}
					}
				} else if(channel < NumberChannels - 1){                                           // check right neighbour
                    if (eventPE[channel] >= NeighborCutPE){
                        ++clusterSize;
                    }
                }

                // calculate and check the sum threshold
                for (int cluster_ch = 0; cluster_ch < clusterSize; cluster_ch++){
                    sum += eventPE[first_ch_in_cluster + cluster_ch];
                }

                if(sum >= SumCutPE){
		  chipclusters++;
                    nbCluster++;
                    clusterSize_fin.push_back(clusterSize);
                    first_ch_in_cluster_vec.push_back(first_ch_in_cluster);
                    sum_vec.push_back(sum);
                    
                    int middle(0);
                    double maxpe(0);
                    // In order to displays the clusters
                    for(int itt=0; itt < clusterSize; itt++) {
                        if(eventPE[first_ch_in_cluster+itt]>maxpe) {
							middle = itt;
							maxpe = eventPE[first_ch_in_cluster+itt];
						}
                    }
                    if(0) {
						// Fills cluster display vector (applying the neighbouring cut)
						bool below_thrs = false;
						for(int itt=0; itt < clusterSize; itt++) {
							if((7-middle+itt)>=0 && (7-middle+itt)<15 ) {//&& first_ch_in_cluster+middle==cha) {
								cluster_display_tmp[7-middle+itt] = eventPE[first_ch_in_cluster+itt];
								if(cluster_display_tmp[7-middle+itt]<1.5) {
									below_thrs = true;
								}
							}
						}
						if(below_thrs) { // WARNING if some cluster signal is below the neighbouring threshold
							cout << "Cluster includes signal below the threshold!" << endl << clusterSize << "  "  << sum << "  " << first_ch_in_cluster << "   ";
		                    for(int itt=0; itt < 15; itt++) {
								cout << cluster_display_tmp[itt] << " ";
							} cout << endl;
						}
					}
					// looks if cluster is not at one of the edge of the SiPM
					int it_start(0), it_stop(15);
					if(1) {
						// Fills cluster display vector (displays the data of all neigbouring channels even the ones which are below the thresholds)
						for(int itt=0; itt < 15; itt++) {
							if(first_ch_in_cluster+middle-7+itt==0) {
								it_start = itt;	//cannot be below 0
							}
							if(first_ch_in_cluster+middle-7+itt==128) {
								it_stop = itt;	//cannot be above 127
							}
							if((first_ch_in_cluster+middle-7+itt>=0) && (first_ch_in_cluster+middle-7+itt<=127)) {
								//if((nbCluster>1) && 
								cluster_display_tmp[itt] = eventPE_copy[first_ch_in_cluster+middle-7+itt];
							}
							/*if(eventPE[first_ch_in_cluster+middle-7+itt]==0) {
								cout<< first_ch_in_cluster << " " << middle<< " "<<first_ch_in_cluster+middle-7+itt<<endl;
							}*/
						}
					}
					for(int itt(0); itt<15; ++itt) {
						cluster_display[itt] = cluster_display_tmp[itt];
						cluster_display_tmp[itt] = 0;
					}
					
                    // Weighted Mean calculation
                    for(int itt=0; itt < clusterSize; itt++) {
                        mean_position_num += eventPE[first_ch_in_cluster+itt]*(double)itt;
                        mean_position_den += eventPE[first_ch_in_cluster+itt];
                        eventPE[first_ch_in_cluster+itt] = 0;   // Clean values already evaluated to avoid duplicate channel in two clusters
                    }
					
                    mean_position = mean_position_num / mean_position_den;
                    mean_position_vec.push_back((chip*SIPM_READOUT_CHANNELS) + first_ch_in_cluster + mean_position);
		    positiontemp=first_ch_in_cluster + mean_position;
		    //Get the cluster positions to a file
		    //mean_position = (mean_position + first_ch_in_cluster) * 0.25 + 0.25/2;
		    //if(mean_position > 64.5*0.25) mean_position = mean_position + 0.25;
		    //mean_position_write.push_back(mean_position);
		   	

                    if(0) {
                        std::cout << std::endl;
                        std::cout << "CHIP = " << chip << std::endl;
                        //std::cout << "signalPE raw: [Channel]Value :";
                        //for (int show=0; show<NumberChannels; show++){
                        //if (eventPE[show] > 1.5)  std::cout << "["<< show << "]" << eventPE[show] <<" ";    }
                        std::cout << std::endl;
                        std::cout << "nbCluster\t : "       << nbCluster            << std::endl;
                        std::cout << "Cluster POS\t : "     << first_ch_in_cluster  << std::endl;
                        std::cout << "POS Mean\t : "        << mean_position        << std::endl;
                        std::cout << "Cluster Size\t : "    << clusterSize          << std::endl;
                        std::cout << "Cluster Sum\t : "     << sum                  << std::endl;
                        std::cout << endl;
                    }

                    // Filling the Cluster Histograms: showing all events
                    //m_ClusterChannel[thrs_id]->Fill((chip*SIPM_READOUT_CHANNELS)+ first_ch_in_cluster + mean_position);
                    m_ClusterChannel[thrs_id]->Fill( first_ch_in_cluster + mean_position);
                    m_ClusterSum[thrs_id]->Fill(sum);
                    m_ClusterSize[thrs_id]->Fill(clusterSize);
		    m_ClusterSize_Tot->Fill(clusterSize);
					for(int itt(it_start); itt<it_stop; ++itt) {
						m_ClusterDisplay[thrs_id]->Fill(itt-7,cluster_display[itt]);
					}
                }
				
                EndOfCluster=false;
                clusterSize=0;
                mean_position = 0.0;
                mean_position_num = 0.0;
                mean_position_den = 0.0;
                sum = 0.0;
                first_ch_in_cluster = 0;
                clusters++;
            }
        }//End of channels loop
	if(clusters>1) cleandata = false;
	if(chipclusters==1)ClusterPosition.push_back(positiontemp);
	else ClusterPosition.push_back(-1);



    }//End of chips loop

    //Get Data to txt file (to use decoment also myfile= open before loop)
    /* if(cleandata == true && mean_position_write.size()==numberOfReadoutChips){
      myfile << id << " ";
      for (int chip = 0 ; chip < numberOfReadoutChips ; chip++){
	myfile << chip << " " << mean_position_write.at(chip) << " ";
      }
      myfile << "\n";
    }
    myfile.close();*/


    m_NbCluster[thrs_id]->Fill(nbCluster);

    if(thrs_id==0) m_NbCluster_Tot_buff = 0;
    m_NbCluster_Tot_buff+=nbCluster;
    if(thrs_id==m_NumberOfSipm-1) m_NbCluster_Tot->Fill(m_NbCluster_Tot_buff);

    // Analyze only events with "select_nb_cluster" number of clusters
    // Filling the Cluster Histograms
    /*int select_nb_cluster(1);
    //double spot_position(45); && abs(mean_position_vec[0]-spot_position)<15.
	//double sum_select(30); && abs(sum_vec[0]-sum_select)<3
    if(nbCluster == select_nb_cluster) {
        for(unsigned int j(0); j<select_nb_cluster; ++j) {
            m_ClusterChannel[thrs_id]->Fill(mean_position_vec[j]);
            m_ClusterSum[thrs_id]->Fill(sum_vec[j]);
            m_ClusterSize[thrs_id]->Fill(clusterSize_fin[j]);
            for(int itt(0); itt<15; ++itt) {
				m_ClusterDisplay[thrs_id]->Fill(itt-7,cluster_display[itt]);
			}
        }
    }*/
    
    // Selects only electron signal clusters
    /*double sum_select(11);
    for(unsigned int j(0); j<sum_vec.size(); ++j) {
        if(abs(sum_vec[j]-sum_select)<0.5 && (abs(mean_position_vec[j]-32)<29 || abs(mean_position_vec[j]-96)<29)) {
            m_ClusterChannel[thrs_id]->Fill(mean_position_vec[j]);
            m_ClusterSum[thrs_id]->Fill(sum_vec[j]);
            m_ClusterSize[thrs_id]->Fill(clusterSize_fin[j]);
            if(nbCluster == 1) {
	            for(int itt(0); itt<15; ++itt) {
					m_ClusterDisplay[thrs_id]->Fill(itt-7,cluster_display[itt]);
				}
			}
        }
    }*/
    
    // Fills clusterDisplay histogram
    /*if(nbCluster == 1) {
        for(int itt(0); itt<15; ++itt) {
			m_ClusterDisplay[thrs_id]->Fill(itt-7,cluster_display[itt]);
		}
    }*/
    /*if(nbCluster>1) {
		int ind_maxi(0);
		double maxi(0.0);
		for(int j(0); j<nbCluster; ++j) {
			if(sum_vec[j]>maxi) {
				maxi = sum_vec[j];
				ind_maxi = j;
			}
		}
		for(int j(0); j<nbCluster; ++j) {
			if(j != ind_maxi && (mean_position_vec[ind_maxi]-63.5) < 5) {//
				m_ClusterDistance[thrs_id]->Fill(mean_position_vec[j]-mean_position_vec[ind_maxi]);
			}
		}
	}*/


}



// Plot Cluster Algorithm (3 Thresholds: Seed, Neighb, Sum)                                                                                                        
void DataManager::Cluster_Plot_Event(std::vector<std::vector<int> > &eventADC, float threshold[3], unsigned int ev_id){                      
 char tmp[256];
 sprintf(tmp,"Single Event %d",ev_id);
 int Xmax = 129;
 double eventPE[SIPM_READOUT_CHANNELS];
 TCanvas* canvas = new TCanvas(tmp,tmp,0,0,CANVAS_X, CANVAS_Y);                                                                                                                      TH1D *EventCluster[10]; 
 canvas->Divide(2,m_NumberOfSipm/2);  

 for( int i=0 ;i< m_NumberOfSipm;i++) {
   canvas->cd(i+1);
   sprintf(tmp,"Sipm %d", i);
   EventCluster[i] = new TH1D(tmp,tmp,128,1,128);
   std::vector<int> uplinkId;
   int NumberChannels = SIPM_READOUT_CHANNELS;
   // rescale data from ADC to PE                                                                                                                                            
   for (int channel=0; channel<NumberChannels; channel++){
     eventPE[channel] = eventADC.at(i)[channel] / m_SipmGains[i];
   }

   //Creation of event histograms                                                                                                                                            
   for (int channel=0; channel<NumberChannels; ++channel){
     EventCluster[i]->Fill(channel+1,eventPE[channel]);
   }
   EventCluster[i]->Draw("");                                                                                                                                                  
   // Xmax = EventCluster[2*ii]]->FindLastBinAbove(0, 1); // (thrsd, 1=Xaxis)                                                                                                     
   sprintf(tmp,"Sipm %d", i);
                                                                   
   ConfigHistoTH1(EventCluster[i], tmp, "Channel", "PE", 0, Xmax, -5, 50, 2);                                                                                                 
   TLine *l= new TLine(0,threshold[0],129,threshold[0]);                                                                                       
   l->SetLineColor(kRed); 
   l->SetLineStyle(2);  
   l->Draw();
 }
 sprintf(tmp,"SingleEvent%d.pdf",ev_id+1);
 canvas->SaveAs(tmp);
 /*
 std::vector<TH1*> EventCluster;
 EventCluster=m_EventCluster.at(ev_id);

 sprintf(tmp,"Single Event %d",ev_id);
 TCanvas* canvas = new TCanvas(tmp,tmp,0,0,CANVAS_X, CANVAS_Y);
 canvas->Divide(2,m_NumberOfSipm);

 TLine *l= new TLine(0,threshold[0],129,threshold[0]);
 l->SetLineColor(kRed);
 l->SetLineStyle(2);

 for (int ii = 0; ii < m_NumberOfSipm/2; ii++){
   std::vector<int> uplinkId;
   uplinkId = m_Sipm_uplinkId.at(ii*2);
   canvas->cd(2*ii+1);
   EventCluster[2*ii]->Draw("");
   // Xmax = EventCluster[2*ii]]->FindLastBinAbove(0, 1); // (thrsd, 1=Xaxis)
   sprintf(tmp,"UpLinks %d %d",uplinkId.at(0),uplinkId.at(1));                                                                                                   
   ConfigHistoTH1(EventCluster[2*ii], tmp, "Channel", "PE", 0, Xmax, 0, 15, 2);
   l->Draw();*/
}
