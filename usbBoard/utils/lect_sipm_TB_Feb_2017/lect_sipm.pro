CONFIG -= qt
CONFIG += debug
exists($(USBBOARDPATH)) { 
    include($(USBBOARDPATH)/usbBoard.pri)
    include($(USBBOARDPATH)/RootCInt.pri)
    USBBOARDPATH = $(USBBOARDPATH)
}
else { 
    include(../../usbBoard.pri)
    include(../../RootCInt.pri)
    USBBOARDPATH = ../..
}
DEPENDPATH += $$USBBOARDPATH/event \
    $$USBBOARDPATH/readout
INCLUDEPATH += $$DEPENDPATH
HEADERS += DataManager.h
SOURCES += main.cpp \
    DataManager.cpp \
    ../ClusteringTool/clustering_tool.cpp
!NO_QUSB:LIBS += -L$$USBBOARDPATH/support \
    -lquickusb \
    -L$$USBBOARDPATH/Builds \
    -lusbBoardEvent \
    -L$$USBBOARDPATH/Builds \
    -lusbBoardReadout \
    -L$$ROOTSYS/lib \
    -lSpectrum
NO_QUSB:LIBS += -L$$USBBOARDPATH/Builds \
    -lusbBoardEvent \
    -L$$USBBOARDPATH/Builds \
    -lusbBoardReadout \
    -L$$ROOTSYS/lib \
    -lSpectrum
