#include <iostream>
#include <sstream>
#include <cstdlib>
#include <vector>

#include <TH1.h>
#include <TH2.h>
#include <TF1.h>
#include <TApplication.h>
#include <TFile.h>
#include <TTree.h>
#include <TBrowser.h>
#include <TROOT.h>

#include <TColor.h>
#include <TStyle.h>

#include <RunDescription.h>
#include <MergedEvent.h>

#include "Settings.h"
#include "DataManager.h"


#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <assert.h>

void setRootStyle()
{
    gStyle->SetOptStat(1111);
    gStyle->SetOptFit(1111);
    gStyle->SetCanvasColor(10);
    gStyle->SetCanvasBorderMode(0);
    gStyle->SetFrameLineWidth(1);
    gStyle->SetFrameFillColor(10);
    gStyle->SetPadColor(10);
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    gStyle->SetPadTopMargin(0.15);
    gStyle->SetPadBottomMargin(0.15);
    gStyle->SetPadLeftMargin(0.15);
    gStyle->SetPadRightMargin(0.15);
    gStyle->SetPadGridX(true);
    gStyle->SetPadGridY(true);
    gStyle->SetStatColor(10);
    gStyle->SetStatX(.8);
    gStyle->SetStatY(.8);
    gStyle->SetStatH(.09);
    gStyle->SetHistLineWidth(1);
    gStyle->SetFuncWidth(1);
    gStyle->SetMarkerStyle(20);
    gStyle->SetTitleOffset(1.5, "y");

    gStyle->SetLineScalePS(3.0);
    gStyle->SetTextFont(42);
    gStyle->SetTitleFont(42, "X");
    gStyle->SetTitleFont(42, "Y");
    gStyle->SetTitleFont(42, "Z");
    gStyle->SetLabelFont(42, "X");
    gStyle->SetLabelFont(42, "Y");
    gStyle->SetLabelFont(42, "Z");
    gStyle->SetLabelSize(0.03, "X");
    gStyle->SetLabelSize(0.03, "Y");
    gStyle->SetLabelSize(0.03, "Z");

    int NRGBs = 5;
    int NCont = 255;
    double stops[] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    double red[]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    double green[] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    double blue[]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}

struct globalArgs_t
{
    const char * arg_pathToPedestal;                /* -p option */
    const char * arg_pathToSignal;                  /* -s option */
    bool arg_readInSetupFile;                       /* -S option */
    const char* arg_pathToSetupFile;
    bool arg_clusteringTest;                        /* -C option */
    int  arg_eventplot;                        /* -E option */
  bool arg_fastplot;        /* -F option*/
  bool arg_Tracking; /* -T option*/
  std::vector<std::string> arg_TelescopeLayers;	/*-X(12345) and -Y(12345) */
} globalArgs;

static const char *optString = "p:s:S:RCTE:Fh?X:Y:";

int main(int argc, char* argv[]) {

    // initialize globalArgs
    globalArgs.arg_pathToPedestal = " ";
    globalArgs.arg_pathToSignal = " ";
    globalArgs.arg_readInSetupFile = false;
    globalArgs.arg_pathToSetupFile = " ";

    globalArgs.arg_clusteringTest = false;
    globalArgs.arg_eventplot = -1;
    globalArgs.arg_fastplot = false;
    globalArgs.arg_TelescopeLayers.clear();
    globalArgs.arg_Tracking= false;
    // Get paremeter from the commond
    int opt =0;
    opt = getopt(argc, argv, optString);
    if(opt == -1){
        std::cerr <<  "There is no opption in the command! Type \"lect_sipm -h\" for help.[VATA64]" << std::endl;
        exit(EXIT_FAILURE);
    }
    
    while(opt != -1){
        switch(opt){
        case 'p':
            globalArgs.arg_pathToPedestal = optarg;
            //std::cout<<"-p option path= "<<globalArgs.arg_pathToPedestal<<std::endl;
            break;
        case 's':
            globalArgs.arg_pathToSignal = optarg;
            //std::cout<<"-s option path= "<<globalArgs.arg_pathToSignal<<std::endl;
            break;
        case 'S':
            globalArgs.arg_readInSetupFile =  true;
            globalArgs.arg_pathToSetupFile = optarg;
            break;
        case 'C':
            globalArgs.arg_clusteringTest = true;
            break;
        case 'T':
            globalArgs.arg_Tracking = true;
            break;
	case 'E':
	  int buff;
	  globalArgs.arg_eventplot = 0;
	  for(int aa = 0; aa< strlen(optarg);aa++){
	    buff=optarg[aa]-'0';
	    for(int bb = strlen(optarg); bb - 1 > aa; bb--){
	      buff= buff*10;
	      }
	    globalArgs.arg_eventplot += buff;
	    buff = 0;
	  }
	  break;
	case 'F':
	  globalArgs.arg_fastplot = true;
	  break;
        case 'h':
        case '?':
            std::cerr << "Usage: lect_sipm -p pathToPedestal -s pathToSignal [-S pathToSetupFile]" << std::endl;
            std::cerr << "----------------------------------------------------------------------------------------------------"<<std::endl;
            std::cerr << " either '-r' option or '-p'+'-s' options is necessary!"<<std::endl;
            std::cerr << " -r option: fill all the necessary histograms by an exist root file."<<std::endl;
            std::cerr << " -p and -s options: fill the histograms by the data getting from DAQ. In this case both pedestal root file and signal root file are needed! " <<std::endl;
            std::cerr << " -S option: read in setup file."<<std::endl;
            std::cerr << "-----------------------------------------------------------------------------------------------------"<<std::endl;
            std::cerr << " -C option: Set this option to cluster analysis"<<std::endl;
            std::cerr << "Such as:" << std::endl
                      << "./lect_sipm -p ../data/pedestals.root -s ../data/test.root -C" << std::endl
                      << "----------------------------------------------------------------------------------"<<std::endl;
	    std::cerr << " -E option: Set to plot an example event, give an event number"<<std::endl;
	    std::cerr << "Such as: -E 40000"<< std::endl
	              << "----------------------------------------------------------------------------------"<<std::endl;
	    std::cerr << " -F option: Set to do a quick plot without doing the clustering over all events, -E option must be active"<<std::endl;
	    std::cerr << " -T option: Set to do a quick tracking and residuals measurement, -F has to be inactive"<<std::endl;
	    exit(EXIT_FAILURE);
            break;
		// Cases added for testbeam analysis: select the SciFi telescope planes to analyse
		case 'X':
			globalArgs.arg_TelescopeLayers.push_back(std::string("X")+std::string(optarg));
			break;
		case 'Y':
			globalArgs.arg_TelescopeLayers.push_back(std::string("Y")+std::string(optarg));
			break;
		
        default:
            break;
        } 
        opt = getopt(argc, argv, optString);
    }

    setRootStyle();
    TApplication app("readout_calibration", &argc, argv);


    if((globalArgs.arg_pathToPedestal == " " || globalArgs.arg_pathToSignal == " ")){
        std::cerr << "ERROR: -p or -s option is not set! Both of them has to be set correctly!"<<std::endl;
        exit(EXIT_FAILURE);
    }

    /*if((globalArgs.arg_fastplot == true && globalArgs.arg_eventplot == -1)){
      std::cerr << "ERROR: -E option is not set! It needs to be set to know which event to plot!"<<std::endl;
      exit(EXIT_FAILURE);
    }*/

    if(globalArgs.arg_readInSetupFile){
        std::cout<< "-S option is set, uplinks to be analyzed will be defined by the setupfile: "<< globalArgs.arg_pathToSetupFile <<"\nuplinks set in the command line will be ommited!"<<std::endl;
    }

    if((globalArgs.arg_fastplot == true && globalArgs.arg_Tracking == true)){
      std::cerr << "ERROR: -F option is active! It is not possible to perform tracking without a saved tree!"<<std::endl;
      exit(EXIT_FAILURE);
    }

    if(globalArgs.arg_readInSetupFile) {

        printf("Begin to fill data\n");

        TFile pedestalFile(globalArgs.arg_pathToPedestal);
        TFile signalFile(globalArgs.arg_pathToSignal);

        gROOT->cd();

        TTree* pedestalTree = static_cast<TTree*>(pedestalFile.Get("ReadoutData"));
        TTree* signalTree = static_cast<TTree*>(signalFile.Get("ReadoutData"));

        RunDescription* runDescription = static_cast<RunDescription*>(pedestalTree->GetUserInfo()->First());

        runDescription->dump(std::cout);

        printf("\n------------------------finished dump runDescription()\n");

        std::stringstream settingsStream(runDescription->settings());
        Settings::instance()->loadFromStream(settingsStream);

        printf("\n------------------------finished loadFromStream()\n");

        /////////////////////////////////////////////////////////////////////


        std::cout << "***** >>> Start Clusterization part <<< *****" << std::endl;
        float threshold[3] = {2.5, 1.5, 4.5}; // SeedCutPE , NeigCutPE, SumCutPE

        DataManager dataManager(globalArgs.arg_pathToSetupFile,globalArgs.arg_TelescopeLayers);

        MergedEvent* event_pedestal = 0;
        MergedEvent* event_signal = 0;

        pedestalTree->SetBranchAddress("events", &event_pedestal);
        signalTree->SetBranchAddress("events", &event_signal);
        long pedestalEntries = pedestalTree->GetEntries();
        std::cout << "taking event_pedestal, pedestalEntries = "<<pedestalEntries<< std::endl;
        long signalEntries = signalTree->GetEntries();
        std::cout << "taking event_signals, signalEntries = "<<signalEntries<< std::endl;
        std::cout << std::endl;

        for (long it=0; it<pedestalEntries; ++it) {
            pedestalTree->GetEntry(it);
            dataManager.fill_raw_pedestals(event_pedestal);
        }

        std::cout << "Entries = " << dataManager.counter << std::endl;
        
        dataManager.pedestals_write_Mean_RMS();
		
        ////////////////////////

        int NumberChannels = dataManager.Get_NumberChannels();
        int NumberOfSipms = dataManager.Get_NumberSIPM();
		
        std::vector <int> signalADC(NumberChannels);
        std::vector <std::vector <int> > sipm_signalADC_raw(NumberOfSipms, signalADC);
        std::vector <std::vector <int> > sipm_signalADC_PS(NumberOfSipms, signalADC);
        
        dataManager.printSipmInfos();


	
        for (long it=0; it<signalEntries ; ++it)
        {
	  if (globalArgs.arg_fastplot==true && it!=globalArgs.arg_eventplot && globalArgs.arg_eventplot!=-1 ) continue;
	  if(it%10000==0) std::cout << "Event  = "<<it<< std::endl;
            signalTree->GetEntry(it);
			
            // Get ordered raw signalADC from root file and fill the signal histogram
            dataManager.GetRawsignalADCdata_1event(event_signal, sipm_signalADC_raw);
            //if(it==1){ std::cout << "signalADC raw: "; for(int i=0; i<NumberChannels; i++) std::cout << event_signalADC_raw[i] << " "; std::cout << std::endl << std::endl; }
			
            // Get pedestal subtracted signalADC from sipm_signalADC_raw, return to sipm_signalADC_PS and fill the pedestal subtracted histogram
            dataManager.GetsignalADC_PedSub(sipm_signalADC_raw, sipm_signalADC_PS);
			
            // Start clusterization process with a reordered subtracted signalADC, threshold {SeedCutPE , NeigCutPE, SumCutPE} and Gain ADC to PE
	    if(1) {
	      //dataManager.Cluster_Search(sipm_signalADC_PS, threshold, it);
		dataManager.Cluster_InitBranches();   

	      for(int jj=0; jj<NumberOfSipms; jj++){
		std::vector <std::vector <int> > sipm_signalADC_1_Sipm(1, sipm_signalADC_PS.at(jj));
		dataManager.Cluster_Search(sipm_signalADC_1_Sipm, threshold, it, jj);
	      }
		dataManager.Cluster_FillTree();

	    } else {	// scans over the seed threshold to analyze noise cluster rate

				for(unsigned int step_nb(0); step_nb<17; ++step_nb) {
					threshold[0] = 1.5 + step_nb*0.25;
					threshold[1] = threshold[0] - 1.0;
					threshold[2] = threshold[0] + 2.0;
			        	dataManager.Cluster_Search(sipm_signalADC_PS, threshold, it, step_nb);
					
				}
			}
	
	     if(it==globalArgs.arg_eventplot&&globalArgs.arg_eventplot!=-1){
	       std::cout << "Plotting event  = "<<globalArgs.arg_eventplot<< std::endl;
	       int ev_id = globalArgs.arg_eventplot;
	       dataManager.Cluster_Plot_Event(sipm_signalADC_PS, threshold, ev_id);
	      }
        }

	if (globalArgs.arg_fastplot==true)
        dataManager.draw();
        if(globalArgs.arg_clusteringTest)
            dataManager.Cluster_DrawHisto();
	if (globalArgs.arg_fastplot==false)
	  dataManager.SaveAllHistos();
        if(globalArgs.arg_Tracking && !globalArgs.arg_fastplot)
          dataManager.tracking(globalArgs.arg_eventplot);
	  std::cout << "End" << std::endl;
        app.Run();
        return 0;
    }
}
