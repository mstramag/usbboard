Following changes were made to the SiPM algorithm:

*New option for plotting a single event:

Set -E number_event (included instructions in -h)

*Fast plot option
To get only the plot of the event and not any of the rest plots type -F at the end 


*Cluster_Plot_Event macro added to clustering_tool.cpp
Plots the event, the plots are in order from left to right then from top to bottom in the order of the
uplinks mention on the setupfile.

*Get the light yield of the run
Text file is created with the -C option named light_yield_file.txt with the same order of the sipms in which
they appear on the setup file.


*Option to get data in txt format
Decoment manually on the clustering_tool.cpp in the macro Cluster_Search at the end of the chips loop
also a decoment is needed at the begining of the plot. The data will be saved in the same folder.

*Runs with up of 20 uplinks may be analyzed, for including more uplinks the DataManager.cpp file mey be edited.

*Plot of the general efficency of the telescope (histogram of clusters per event) as well as the cluster size histogram over all sipms.
