TEMPLATE = subdirs

!NO_QUSB {
    !NO_ROOT {
        win* {
            SUBDIRS += \
                linearityTest \
                chipTest
        }
    }

	SUBDIRS += \
	ecalTest \
	#analysis \
	#lect_uplinks
        lect_sipm_prod\
        #lect_sipm-marc\
	#lect_sipm_TB_Feb_2017\


}

NO_QUSB{
      SUBDIRS += \
      #analysis \
      #lect_uplinks
      lect_sipm_prod
      #lect_sipm-marc
      #lect_sipm_telescope\
}
CONFIG += ordered
