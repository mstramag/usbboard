#include "ADataManager.h"
#include <Settings.h>

#include <assert.h>
#include <iostream>
 
#include <MergedEvent.h>
#include <UsbBoardConfiguration.h>

#include <TH1.h>
#include <TH2.h>
#include <TH2F.h>
#include <TF1.h>
#include <TProfile.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TMath.h>
#include <TObjArray.h>
#include <TLegend.h>

DataManager::DataManager(const int uplinksToBeAnalyzed)
{
    char tmp[128];
    Settings* settings = Settings::instance();

    // for (unsigned int k = 0; k < uplinksToBeAnalyzed.size(); ++k) {
        int uplinkId = uplinksToBeAnalyzed;
        int usbBoardId = settings->usbBoardIdFromUplinkId(uplinkId);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
	std::cerr <<"DataManager::DataManager, sampleSize"<<sampleSize<<std::endl;

        m_uplinkId.push_back(uplinkId);

        sprintf(tmp, "adcHistogram_uplink_%03d", uplinkId);
        TH2* adcHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        adcHistogram->GetXaxis()->SetTitle("channel");
        adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_adcHistogram.push_back(adcHistogram);

        // sprintf(tmp, "commonModeCorrectedAdcHistogram_uplink_%03d", uplinkId);
        // TH2* commonModeCorrectedAdcHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        // commonModeCorrectedAdcHistogram->GetXaxis()->SetTitle("channel");
        // commonModeCorrectedAdcHistogram->GetYaxis()->SetTitle("adcValue");
        // m_commonModeCorrectedAdcHistogram.push_back(commonModeCorrectedAdcHistogram);
	
	//}
}

DataManager::~DataManager()
{}

unsigned int DataManager::uplinkIndex(int uplinkId)
{
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
    for (int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
        if (*uplinkIdIt == uplinkId)
            return i;
    }
    assert(false);
    return 0;
}

void DataManager::fillEvent(MergedEvent* event)
{ 
  Settings* settings = Settings::instance();
  std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
  std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
  for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
    int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
    unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
    unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
    unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;
    const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);
    int* commonMode = new int[channelsPerReadoutChip];
    for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip) {
      for (unsigned short channel = 0; channel < channelsPerReadoutChip; ++channel) {
	unsigned short sample = chip * channelsPerReadoutChip + channel;
	m_adcHistogram[i]->Fill(sample, uplinkData.adcValue(sample));
      }
    }

    delete[] commonMode;
  }
}

void DataManager::getEnergy(MergedEvent* event)
{
  Settings* settings = Settings::instance();
  std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
  std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
  int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
  unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
  // std::cerr<<"in DataManager::getEnergy, sampleSize="<< sampleSize <<std::endl;

  unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
  //std::cerr<<"in DataManager::getEnergy, channelsPerReadoutChip ="<< channelsPerReadoutChip <<std::endl; 
  unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;
  //std::cerr<<"in DataManager::getEnergy, numberOfReadoutChips ="<< numberOfReadoutChips <<std::endl;  

  const UplinkData& uplinkData0 = event->uplinkData(40);
  //std::cerr<<"in DataManager::finish event->uplinkData" <<std::endl; 
   
  double baseline=0;  //double baseline1=0;
  
  for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip) {   
    for (unsigned short channel = 0; channel < channelsPerReadoutChip; ++channel) {
      //std::cerr<<"in DataManager:channel="<< channel <<std::endl;   
      // std::cerr<<"in DataManager:channelsPerReadoutChip="<< channelsPerReadoutChip <<std::endl;  

      unsigned short sample = chip * channelsPerReadoutChip + channel;
      unsigned short row = ((sample+1)%3 ? (sample+1)%3 : (sample+1)%3 + 3); // row1 = bar0, 3, 6, 9, ...  

      if (sample!=1) 
	{
	   baseline = baseline + uplinkData0.adcValue(sample);
	}
      std::cerr<<std::endl;
    }
  }
  // std::cerr<<"in DataManager:finish for loop"<<std::endl;  

  baseline=baseline/channelsPerReadoutChip;
  

  for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip) {
    for (unsigned short channel = 0; channel < channelsPerReadoutChip; ++channel) {
      unsigned short sample = chip * channelsPerReadoutChip + channel;
      unsigned short row = ((sample+1)%3 ? (sample+1)%3 : (sample+1)%3 + 3); // row1 = bar0, 3, 6, 9, ...
      //  unsigned short sample1= 33 + 2*row - (sample+1) - 1;
      //std::cout<<"sample "<<sample<<"      row " <<row<<"      sample1 "<<sample1<<std::endl;
      //std::cout<<"sample="<<sample+1<<std::endl;
      double adc0=uplinkData0.adcValue(sample)-(int)(baseline)+600;
      // double adc1=uplinkData1.adcValue(sample1)-(int)(baseline1)+600;
      //std::cout<<"sample "<<sample<<" adc0 "<<adc0<<std::endl;
    }
  }

}

void DataManager::draw(int firstChannel, int lastChannel)
{
  //std::cout<<"finished\n";
  
  TCanvas* canvas = new TCanvas("c1","c1", 1);
  canvas->Divide(1, 2);
  canvas->cd(1);
  m_adcHistogram[0]->Draw("COLZ");
  canvas->cd(2);
  TH1D* proj= m_adcHistogram[0]-> ProjectionY("channel", firstChannel ,lastChannel ,"");
  proj -> GetXaxis()->SetRangeUser(400,650);
  proj-> Draw("");

  printf("mean\n");
  for (int i=0;i<0;i++)
    {
      float mean1,mean2;
      mean1 = m_adcHistogram[0]->ProjectionY("projection1",i+1,i+1,"")->GetMean();
      printf("channel = %d\t mean1 = %f\t",i,mean1);
    }
  
  /*
  file_data = new TFile ("root_file/Data2Read.root","RECREATE");
  m_adcHistogram[0]->Write();
  //m_commonModeCorrectedAdcHistogram[0]->Write();
  m_adcHistogram[1]->Write();
  //m_commonModeCorrectedAdcHistogram[1]->Write();
  //data_run->Write();
  m_uplink_41_vs_40[0]->Write();
  file_data->Close();
  */

  /*
  TH2* adcHistogram;
  adcHistogram = (TH2*)file->Get("adcHistogram_uplink_040;1");
  for (Int_t i=0; i<36; ++i)
    {
      Int_t j=i+1;
      sprintf(nameofhisto,"hE%d;*",i);
      file->Delete(nameofhisto);
      sprintf(nameofhisto,"hE%d",i);
      TH1D *hE;
      hE  = adcHistogram->ProjectionY(nameofhisto,j,j);
      hE->Draw();
      hE->Write();
      }
  */
}


