#include <iostream>
#include <sstream>
#include <cstdlib>
#include <vector>

#include <TH1.h>
#include <TApplication.h>
#include <TFile.h>
#include <TTree.h>
#include <TBrowser.h>
#include <TROOT.h>

#include <TColor.h>
#include <TStyle.h>

#include <RunDescription.h>
#include <MergedEvent.h>

#include <Settings.h>
#include "ADataManager.h"

void setRootStyle()
{
  gStyle->SetOptStat(1);
  gStyle->SetCanvasColor(10);
  gStyle->SetCanvasBorderMode(0);
  gStyle->SetFrameLineWidth(1);
  gStyle->SetFrameFillColor(10);
  gStyle->SetPadColor(10);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetPadTopMargin(0.15);
  gStyle->SetPadBottomMargin(0.15);
  gStyle->SetPadLeftMargin(0.15);
  gStyle->SetPadRightMargin(0.15);
  gStyle->SetPadGridX(true);
  gStyle->SetPadGridY(true);
  gStyle->SetStatColor(10);
  gStyle->SetStatX(.8);
  gStyle->SetStatY(.8);
  gStyle->SetStatH(.09);
  gStyle->SetHistLineWidth(1);
  gStyle->SetFuncWidth(1);
  gStyle->SetMarkerStyle(20);
  gStyle->SetTitleOffset(1.5, "y");
  
  gStyle->SetLineScalePS(3.0);
  gStyle->SetTextFont(42);
  gStyle->SetTitleFont(42, "X");
  gStyle->SetTitleFont(42, "Y");
  gStyle->SetTitleFont(42, "Z");
  gStyle->SetLabelFont(42, "X");
  gStyle->SetLabelFont(42, "Y");
  gStyle->SetLabelFont(42, "Z");
  gStyle->SetLabelSize(0.03, "X");
  gStyle->SetLabelSize(0.03, "Y");
  gStyle->SetLabelSize(0.03, "Z");
  
  int NRGBs = 5;
  int NCont = 255;
  double stops[] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
  double red[]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
  double green[] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
  double blue[]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);
}

int main(int argc, char* argv[])
{
    if (argc != 5) {
    std::cerr << "usage:   " << argv[0] << " <signal file name>" << " <uplink IDs to be analyzed>" << " <firstChannel to show> " <<" <lastChannel to show >" << std::endl;
    std::cerr << "example: " << argv[0] << " test.root " << "41 29 30" << std::endl;
    exit(1);
    }
 
  setRootStyle();
  TApplication app("readout_calibration", &argc, argv);
  
  TFile signalFile(app.Argv(1));
  gROOT->cd();
  TTree* signalTree = static_cast<TTree*>(signalFile.Get("ReadoutData"));
  
  RunDescription* runDescription = static_cast<RunDescription*>(signalTree->GetUserInfo()->First());
  runDescription->dump(std::cout);
  std::stringstream settingsStream(runDescription->settings());
  Settings::instance()->loadFromStream(settingsStream);

  //std::cerr<<"finish loadFromStream"<<std::endl;
  
  int uplinksToBeAnalyzed = atoi(app.Argv(2));
  int firstChannel = atoi(app.Argv(3))+1;
  int lastChannel = atoi(app.Argv(4))+1;
 
  DataManager dataManager(uplinksToBeAnalyzed);
  //std::cerr<<"build DataManager"<<std::endl; 
  
  MergedEvent* event = 0;
  signalTree->SetBranchAddress("events", &event);
  // std::cerr<<"finish SetBranchAddress"<<std::endl; 

  long signalEntries = signalTree->GetEntries();
  // std::cerr << "taking events"<<std::endl; 

  for (long it = 0; it < signalEntries; ++it) {
    signalTree->GetEntry(it);
    //std::cerr << "finish GetEntry"<<std::endl;

    dataManager.fillEvent(event);
    //std::cerr << "finish fillEvent"<<std::endl;

    dataManager.getEnergy(event) ;
    // std::cerr << "finish getEnergy"<<std::endl;

    if ((it+1) % 1000 == 0)
      std::cout << '.' << std::flush;
  }
  std::cout << std::endl;
  
  dataManager.draw(firstChannel,lastChannel);
  // std::cerr << "finish draw"<<std::endl;

  signalFile.Close();
  //std::cerr << "finish signalFile.Close()"<<std::endl;  

  app.Run();
  //std::cerr << "finish app.Run()"<<std::endl;  
    
  return 0;
}
