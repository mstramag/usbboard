#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <vector>


#include <time.h>
#include <stdio.h>
#include <cstring>
#include <sstream>



#include <TH1.h>
#include <TApplication.h>
#include <TFile.h>
#include <TTree.h>
#include <TBrowser.h>
#include <TROOT.h>
#include <TH2.h>
#include <TH2F.h>
#include <TF1.h>
#include <TProfile.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TMath.h>
#include <TObjArray.h>
#include <TLegend.h>


#include <TColor.h>
#include <TStyle.h>

#include <RunDescription.h>
#include <MergedEvent.h>

#include <Settings.h>
#include "A2DataManagerTime.h"


void setRootStyle()
{
  gStyle->SetOptStat(110);
  gStyle->SetCanvasColor(10);
  gStyle->SetCanvasBorderMode(0);
  gStyle->SetFrameLineWidth(1);
  gStyle->SetFrameFillColor(10);
  gStyle->SetPadColor(10);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetPadTopMargin(0.15);
  gStyle->SetPadBottomMargin(0.15);
  gStyle->SetPadLeftMargin(0.15);
  gStyle->SetPadRightMargin(0.15);
  gStyle->SetPadGridX(true);
  gStyle->SetPadGridY(true);
  gStyle->SetStatColor(10);
  gStyle->SetStatX(.8);
  gStyle->SetStatY(.8);
  gStyle->SetStatH(.09);
  gStyle->SetHistLineWidth(1);
  gStyle->SetFuncWidth(1);
  gStyle->SetMarkerStyle(20);
  gStyle->SetTitleOffset(1.5, "y");
  
  gStyle->SetLineScalePS(3.0);
  gStyle->SetTextFont(42);
  gStyle->SetTitleFont(42, "X");
  gStyle->SetTitleFont(42, "Y");
  gStyle->SetTitleFont(42, "Z");
  gStyle->SetLabelFont(42, "X");
  gStyle->SetLabelFont(42, "Y");
  gStyle->SetLabelFont(42, "Z");
  gStyle->SetLabelSize(0.03, "X");
  gStyle->SetLabelSize(0.03, "Y");
  gStyle->SetLabelSize(0.03, "Z");
  
  int NRGBs = 5;
  int NCont = 255;
  double stops[] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
  double red[]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
  double green[] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
  double blue[]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);

  
}



//------------------------------------------------------------------------------------------------

void PimpPierre1D(TH1F* Histo, TString axe_x, TString axe_y, int couleur) {

   // graphes avec lignes : noir 1 : rouge 2 : vert 3 : bleu 4 :
   Histo->SetLineColor(couleur);
   Histo->SetLineStyle(1);
   Histo->SetLineWidth(2);

   // graphes avec points
   Histo->SetMarkerColor(2);
   Histo->SetMarkerStyle(7);

   Histo->GetXaxis()->SetTitle(axe_x);
   Histo->GetXaxis()->SetTitleSize(0.06);
   Histo->GetXaxis()->SetTitleOffset(1);
   Histo->GetYaxis()->SetTitle(axe_y);
   Histo->GetYaxis()->SetTitleSize(0.06);
   Histo->GetYaxis()->SetTitleOffset(1.2);

}

//------------------------------------------------------------------------------------------------

void PimpPierre2D(TH2F* Histo, TString axe_x, TString axe_y, int couleur) {

   // graphes avec points : noir 1 : rouge 2 : vert 3 : bleu 4 :
   Histo->SetMarkerColor(couleur);
   Histo->SetMarkerStyle(6);

   Histo->GetXaxis()->SetTitle(axe_x);
   Histo->GetXaxis()->SetTitleSize(0.05);
   Histo->GetXaxis()->SetTitleOffset(1.3);
   Histo->GetYaxis()->SetTitle(axe_y);
   Histo->GetYaxis()->SetTitleSize(0.05);
   Histo->GetYaxis()->SetTitleOffset(1.1);

}

//------------------------------------------------------------------------------------------------

void PimpPierre3D(TH2F* Histo, TString axe_x, TString axe_y, TString axe_z) {

 
   Histo->SetMarkerStyle(6);

   Histo->GetXaxis()->SetTitle(axe_x);
   Histo->GetXaxis()->SetTitleSize(0.05);
   Histo->GetXaxis()->SetTitleOffset(1.3);
   Histo->GetYaxis()->SetTitle(axe_y);
   Histo->GetYaxis()->SetTitleSize(0.05);
   Histo->GetYaxis()->SetTitleOffset(1.5);
   Histo->GetZaxis()->SetTitle(axe_z);
   Histo->GetZaxis()->SetTitleSize(0.05);
   Histo->GetZaxis()->SetTitleOffset(1.3);

}

//------------------------------------------------------------------------------------------------

#define MAX_DATE 18

std::string get_date(void)
{
   time_t now;
   char the_date[MAX_DATE];

   the_date[0] = '\0';

   now = time(NULL);

   if (now != -1)
   {
      strftime(the_date, MAX_DATE, "%Y%m%d_%H%M%S", localtime(&now));
   }

   return std::string(the_date);
}




int main(int argc, char* argv[])
{

  // Loading of parameters

  if (argc != 6) {
    std::cerr << "usage:   " << argv[0]
	      << " <run mode: pedestal(0) or data(1)>"
	      << " <signal file name>"
	      << " <uplink1 IDs to be analyzed>"
	      << " <uplink2 IDs to be analyzed>"
	      << " <output mode: fill histograms(0) or fill text files(1)> " << std::endl;
    std::cerr << "example: " << argv[0] << " 0 or 1"<< " test.root " << "41 42 0" << std::endl;
    exit(1);
  }
  setRootStyle();
 

  TApplication app("readout_calibration", &argc, argv);
  //std::cout<<"1: "<< app.Argv(1)<<" 2: "<<app.Argv(2)<<" 3: "<<app.Argv(3)<<" 4: "<<app.Argv(4)<<" 5: "<<app.Argv(5)<<" 6: "<<app.Argv(6)<<std::endl;
 
  int mode=atoi(app.Argv(1));
  //std::cout<<"mode: "<< mode<<std::endl;
  
  if(mode!=0 && mode!=1){
    std::cerr<<" The running mode should be 0: pedestal mode, 1: data mode"<< std::endl;
    exit(1);
  }

  int uplink1ToBeAnalyzed = atoi(app.Argv(3));
  //int uplink1ToBeAnalyzed = 41;
  int uplink2ToBeAnalyzed = atoi(app.Argv(4));
  //int uplink2ToBeAnalyzed = 42;
  int uplinkTime = 47;
  int outputOption = atoi(app.Argv(5)); // outputOption =0 or =1
  if(outputOption!=0 && outputOption!=1) {
    std::cerr<<" The output option should be 0: plot histograms, 1: write text files"<< std::endl;
    exit(1);
  }
  //  int firstChannel = atoi(app.Argv(5))+1;
  //  int lastChannel = atoi(app.Argv(6))+1;

  // Histogram scales
  Int_t DispPedWidth=128;
  Int_t DispPedOffset=0;
  //  Long64_t PedN=1000;
  //  Long64_t N;

  // Root file data
  
  TFile signalFile(app.Argv(2)); //Data Root file to Analyze
  gROOT->cd();
  TTree* signalTree = static_cast<TTree*>(signalFile.Get("ReadoutData"));
  //
  MergedEvent* event = 0;
  signalTree->SetBranchAddress("events", &event);
  // std::cerr<<"finish SetBranchAddress"<<std::endl; 
  
  Long64_t signalEntries = signalTree->GetEntries();
  // std::cerr << "taking events"<<std::endl; 
  //  Long64_t PedNentries = signalTree->GetEntries();
  
  RunDescription* runDescription = static_cast<RunDescription*>(signalTree->GetUserInfo()->First());
  runDescription->dump(std::cout);
  std::stringstream settingsStream(runDescription->settings());
  Settings::instance()->loadFromStream(settingsStream);
  time_t timeStamp = runDescription->timeStamp(); // time of first trigger
  long int t0;
  t0 = static_cast<int> (timeStamp);

  //print time of first trigger
  char t0_date[MAX_DATE];
  t0_date[0] = '\0';
  strftime(t0_date, MAX_DATE, "%Y%m%d_%H%M%S", localtime(&timeStamp));
  std::cout<<" time of first trigger: "<< t0_date
	   <<" ...in seconds: "<<t0<<std::endl;

  // Data Manager initialization
  
  DataManager dataManager;
  dataManager.addUplink(uplink1ToBeAnalyzed);
  dataManager.addUplink(uplink2ToBeAnalyzed);
  dataManager.addUplink(uplinkTime);
  
  //std::cerr<<"build DataManager"<<std::endl; 

  // Histograms definitions 
  TH2* Raw_data = new TH2I("Raw_data", "Raw Data", 128, .0, 128, 1000, .0, 1000);
  TH2* ADCvsEvent = new TH2I("ADCvsEvent", "ADC versus Event", 1000, 0, 1000, 700, -50., 650.);
  TH2* ADCvsChannelvsEvent = new TH2I("ADCvsChannelvsEvent", "ADC versus Channel versus Event", 1000, 0, 1000, 128, 0, 128);
  TH2* Corrected_data = new TH2F("Corrected_data", "Corrected Data", 128, .0, 128, 640, 10., 650.);
  TH1F* ADC_projection= new TH1F("ADC_projection", "ADC counts", 700, -50., 650.);
  TH1F* ADC_projection_truncated= new TH1F("ADC_projection", "ADC counts", 700, -50., 650.);
  TH2D *Pedestal = new TH2D("Pedestal","",DispPedWidth,DispPedOffset+0.5,DispPedWidth+DispPedOffset+0.5,100,480,580);
  TH1F* Common_mode1= new TH1F("Common_mode1", "Common Mode 1st Chip", 100, -500., 500.);
  TH1F* Common_mode2= new TH1F("Common_mode2", "Common Mode 2nd Chip", 100, -500., 500.);
  TH1F* NbCluster = new TH1F("NbCluster", "", 10, -0.5, 9.5);
  TH1F* ClusterSize = new TH1F("ClusterSize", "", 11,-0.5,10.5);
  TH1F* ClusterADC = new TH1F("ClusterADC", "",600, 0., 60.);
  TH2F* Cluster3D = new TH2F("Cluster3D", "", 10, -0.5, 9.5, 150, 0., 75.);
  TH2F* Clustersize3D = new TH2F("Clustersize3D", "", 6, 0.5, 6.5, 150, 0., 75.);
 
  // Output Ascii files // useless
  //char outputFileName[80] = "SciFi_";
  //outputFileName.append(t0_date);
  //  std::string outputFileName = "SciFi_";
  //  outputFileName.append(t0_date);
  //  outputFileName.append(".txt");
  //  const char* outputFileNameChar = (const char*)outputFileName.c_str();
  //  ofstream fout(outputFileNameChar);
  ofstream fout;
  ofstream flac;

  // Useful data
  int peADC=37; //number of ADC per Photoelectron
  Float_t pcut=2.5*peADC;//cut used to seed the clusters
  Float_t seccut=0.5*peADC; //cut used to add neibourgh channel to clusters
  int vide=0;
  int data[128]; //Raw data vector
  Float_t Cdata[128];
  Long64_t Time[1]; //Time stamp from uplink ID 47
  Time[0] = 0.;
  Long64_t referenceTime = 0; // time of first event in each spill
  Long64_t timePreviousEvent = 0; // time of previous event
  std::string date;
  Float_t clusterX[128];//in channel number units
  Float_t clusterADC[128];//Total number of ADC in the cluster
  Int_t clusterSize[128];//Number of Channels in cluster
  Int_t clusterGaps[128];// to skip dead channels
  Int_t nbClusters;
  Double_t PedSquaredArray[128];
  Double_t  PedArray[128];
  Double_t  NoiseArray[128];
  for (UInt_t ich = 0; ich<128; ich++){
    PedArray[ich] = 0.;
    NoiseArray[ich] = 0.;
    PedSquaredArray[ich]=0.;
  }

  // Read in pedestals (if in data analysis mode)
  if(mode==1){
    ifstream pin("pedestal.txt");
    if (!pin) {
      std::cerr << "Can't open input file " << "pedestal.txt" <<std:: endl;
      exit(1);
    }
    /*
      int total = std::count(std::istreambuf_iterator<char>(pin),
      std::istreambuf_iterator<char>(),'\n');
      std::cout << "pedestal.txt has " << total << " lines." <<std::endl;
    */
    int k=0;
    while (!pin.eof() && k<128 ) {
      //      std::cout<<"Je suis a la ligne "<< k <<std::endl;
      //pin >> PedArray[k]>>PedSquaredArray[k]>>NoiseArray[k]; 
      float readValue;
      pin >> readValue;
      PedArray[k] = readValue;
      pin >> readValue;
      PedSquaredArray[k] = readValue;
      pin >> readValue;
      NoiseArray[k] = readValue;
      //std::cout<<PedArray[k]<<"  "<<PedSquaredArray[k]<<"  "<<NoiseArray[k]<<std::endl;
      k++;
    }
  }

  //*****************************************************
  // DATA ANALYSIS
  //*****************************************************

  // loop over events
  for (long it = 0; it < signalEntries; ++it) {

   
    signalTree->GetEntry(it);
   
    //std::cerr << "finish GetEntry"<<std::endl;
    
    
    dataManager.fillEvent(event, data, Time);// Filling the data vector and time
   

    // Close output file if time from last event is larger than 5 seconds
    Long64_t timeDiff = Time[0]-timePreviousEvent; // in clock cycles ~10ns
    //std::cout << "timeDiff : " << timeDiff << "  Time : "<< Time[0] << "  PreviousEvent : " << timePreviousEvent <<  std::endl;
    if ( mode==1 && outputOption==1 && timeDiff > 5e8 ) {  // 5 seconds
      if(fout.is_open()) fout.close(); //Check for the first trigger, if it comes less than 5sec after starting the prog
      if(flac.is_open()) flac.close(); //Check for the first trigger, if it comes less than 5sec after starting the prog

      referenceTime = Time[0];
      Long64_t absoluteSpillTimeFirstEvent = (Long64_t) Time[0]/96e6; // frequency 96Mhz 
      std::cout << "Time[0] : " << Time[0] << std::endl;
      std::cout << "t0 : " << t0 << "  Time of first event : " << absoluteSpillTimeFirstEvent << std::endl;
      time_t spillTime = (time_t)(t0+absoluteSpillTimeFirstEvent);
      t0_date[0] = '\0';
      strftime(t0_date, MAX_DATE, "%Y%m%d_%H%M%S", localtime(&spillTime));
      std::cout<<" New file; time of first trigger: "<< t0_date
	       <<" ...in seconds: "<<t0<<std::endl;

      std::string outputFileName = "SciFi_";
      outputFileName.append(t0_date);
      outputFileName.append(".txt");
      const char* outputFileNameChar = (const char*)outputFileName.c_str();
      fout.open(outputFileNameChar);
      flac.open("test.txt");
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////
    // Calculate the common mode// 
    /////////////////////////////
  
    Float_t CMN_1=0.;
    Float_t NbChannelsAtPedestal_1=0.;
    Float_t CMN_2=0.;
    Float_t NbChannelsAtPedestal_2=0.;



    for(int j=0;j<128; ++j){
      Float_t deltaADC = data[j]-PedArray[j];
      //      std::cout << "j="<<j<<"  deltaADC="<<deltaADC<<std::endl;

      /*
      CHANGE THE VALUE 100 TO MORE REASONABLE NUMBER WHEN GOOD
      PEDESTAL RUN IS TAKEN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

      if(deltaADC<5.*NoiseArray[j]) {
	if(j<64) {
	  //	  std::cout<<"Chip 1: "<< j <<  " " << deltaADC << std::endl;
	  CMN_1 += deltaADC;
	  NbChannelsAtPedestal_1+=1.;
	} else {
	  //	  std::cout<<"Chip 2: "<< j << " " << deltaADC << std::endl;
	  CMN_2 += deltaADC;
	  NbChannelsAtPedestal_2+=1.;
	}
      }
    }
    //    std::cout << "1 " << CMN_1 << " " << NbChannelsAtPedestal_1 << std::endl;
    if(NbChannelsAtPedestal_1!=0.)
      CMN_1/=NbChannelsAtPedestal_1;
    if(outputOption==0) Common_mode1->Fill(CMN_1);

    //    std::cout << "2 " << CMN_2 << " " << NbChannelsAtPedestal_2 << std::endl;
    if(NbChannelsAtPedestal_2!=0.)
      CMN_2/=NbChannelsAtPedestal_2;
    if(outputOption==0) Common_mode2->Fill(CMN_2);
////////////////////////////////////////////////////////////////////////////////



    // Find clusters
    // Float_t signalADC;
    // Float_t clusterX[20];//in channel number units
    // Float_t clusterADC[20];//Total number of ADC in the cluster
    // Int_t clusterSize[20];//Number of Channels in cluster
    // Int_t clusterGaps[20];// to skip dead channels
    // Int_t nbClusters=0;
    // Bool_t signalInPreviousChannel = false;
    // for(int j=0;j<20; ++j){
    //   clusterX[j]=0.;
    //   clusterADC[j]=0.;
    //   clusterSize[j]=0;
    //   clusterGaps[j]=0.;
    // }
    // for(int j=0;j<128; ++j){
    //   if(nbClusters>19) break;
    //   Float_t adc= float(data[j]);
    //   Float_t pedSubtractedAdc = adc-PedArray[j];
    //   //pedSubtractedAdc -= (j<64) ? CMN_1 : CMN_2;
    //   if(j<64) {
    // 	pedSubtractedAdc-=CMN_1;
    //   } else {
    // 	pedSubtractedAdc-=CMN_2;
    //   }
    //   signalADC= (pedSubtractedAdc>pcut) ? pedSubtractedAdc : 0.; //Threshold value =100 ie 2.5 pe
    //   if(signalADC>0.) {
    // 	clusterX[nbClusters]+=signalADC*j;// Somme des cannaux ponderee par les adc
    // 	clusterADC[nbClusters]+=signalADC;
    // 	++clusterSize[nbClusters];
    // 	signalInPreviousChannel = true;
    //   } else {
    // 	if(signalInPreviousChannel) {
    // 	  if(clusterADC[nbClusters]>0.){ //closing the cluster
    // 	    clusterX[nbClusters]/=clusterADC[nbClusters];
    // 	    ++nbClusters;
    // 	    signalInPreviousChannel = false;	
    // 	  } 
    // 	  else { // reset counters
    // 	    clusterX[nbClusters]=0.;
    // 	    clusterADC[nbClusters]=0.;
    // 	    clusterSize[nbClusters]=0;
    // 	    clusterGaps[nbClusters]=0.;
    // 	    signalInPreviousChannel = false;
    // 	  }
    // 	}
    //   }
    // }
    // //for the channel n°128
    // if(signalInPreviousChannel && clusterADC[nbClusters]>0.) {
    //   clusterX[nbClusters]/=clusterADC[nbClusters];
    //   ++nbClusters;
    //   signalInPreviousChannel = false;
    // }


    
    // if(mode==1 && outputOption==0){
    //   NbCluster->Fill(nbClusters);
    //   for (int g=0; g<nbClusters; ++g){
    // 	ClusterADC->Fill(clusterADC[g]/peADC);//In number of photo electron
    // 	Cluster3D->Fill(nbClusters,clusterADC[g]/peADC);
    // 	ClusterSize->Fill(clusterSize[g]);
    // 	Clustersize3D->Fill(clusterSize[g],clusterADC[g]/peADC);
    //   }
    // }
    


    /*
    // print clusters
    for(int j=0;j<nbClusters;++j){
      if(clusterADC[j]>120.) {
	std::cout << "Cluster nb " << j << " X= " << clusterX[j]
		<< " ADC = " << clusterADC[j]
		<< " size = " << clusterSize[j]
		<< std::endl;
      }
    }
    */

///////////////////////////////////////////////////////////////////


    std:: vector<int> selection;
  
    
    
    
    //Histogramms and data sorting
   
    for(int j=0;j<128; ++j){
      
      Float_t adc= float(data[j]);
      if(mode==1) {
	if(outputOption==0) Raw_data->Fill(j, data[j]);
	Float_t pedSubtractedAdc = adc-PedArray[j];
	if(j<64) {
	  pedSubtractedAdc-=CMN_1;
	} else {
	  pedSubtractedAdc-=CMN_2;
	}
	Cdata[j]=pedSubtractedAdc;
	//std::cout<< "channel and corrected data: "<<j<<" "<<pedSubtractedAdc<<std::endl;
	


	if(outputOption==0) Corrected_data->Fill(j,pedSubtractedAdc);
	
	Long64_t eventTime = Time[0]-referenceTime; // event time in clock cycles
	Long64_t eventTimeSec = (Long64_t)(eventTime/0.096); // event time in nanoseconds
	// fout << j << " " << pedSubtractedAdc << " " << eventTimeSec << std::endl;
	
	//	flac << "j=" << j << "   ADC=" << pedSubtractedAdc << std::endl;
	// Start the 0 suppression loop
	
	if(pedSubtractedAdc>= seccut){
	  selection.push_back(j);  
	  //flac <<" Selection: "<< j<<" "<< selection[selection.size()-1]<< std::endl;
	}
		
       	if(pedSubtractedAdc>seccut) ADC_projection_truncated->Fill(pedSubtractedAdc);
	if(outputOption==0) ADC_projection->Fill(pedSubtractedAdc);
	if(it<1000) {
	  if(outputOption==0) ADCvsEvent->Fill(it,pedSubtractedAdc);
	  if(outputOption==0) ADCvsChannelvsEvent->Fill(it,j,pedSubtractedAdc);
	}
      }
      else{
	Pedestal->Fill(j, data[j]);
	PedArray[j]+=adc;
	PedSquaredArray[j]+=adc*adc;
      }
    } // end for(int j=0;j<128; ++j) Histogram and selection
    
//////////////////////////////////////////////////////////////////////////////////////////// 
//                      TEXT FILES && CLUSTERING                                         //
//////////////////////////////////////////////////////////////////////////////////////////
 
    if(mode==1) { 

   
   
      Float_t pedSubtractedAdc_prev;
      Float_t pedSubtractedAdc_next;
      Float_t pedSubtractedAdc;
      Float_t adc_prev;
      Float_t adc_next;
      Float_t adc;

      
      Float_t signalADC;
      std:: vector<int> rawcluster;
      
      Bool_t signalInPreviousChannel = false;
      for(int j=0;j<selection.size(); ++j){
      	clusterX[j]=0;
      	clusterADC[j]=0;
      	clusterSize[j]=0;
      	clusterGaps[j]=0;
      }
      
    
  
      
      for(int i=0; i<selection.size(); i++){
	
  
	
      	int j=selection[i];
	
      	if (j>0) {adc_prev= float(data[j-1]);pedSubtractedAdc_prev = adc_prev-PedArray[j-1];}
      	if (j<127) {adc_next= float(data[j+1]);pedSubtractedAdc_next = adc_next-PedArray[j+1];}
      	adc= float(data[j]);pedSubtractedAdc = adc-PedArray[j];
      	if(j<64) {
      	  pedSubtractedAdc-=CMN_1;
      	} else {
      	  pedSubtractedAdc-=CMN_2;
      	}
      	if (j>0) {
      	  if(j-1<64) {
      	    pedSubtractedAdc_prev-=CMN_1;
      	  } else {
      	    pedSubtractedAdc_prev-=CMN_2;
      	  }
      	}
      	if (j+1<128) {
      	  if(j+1<64) {
      	    pedSubtractedAdc_next-=CMN_1;
      	  } else {
      	    pedSubtractedAdc_next-=CMN_2;
      	  }
      	}
      	// Write data to ascii output file
	
      	Long64_t eventTime = Time[0]-referenceTime; // event time in clock cycles
      	Long64_t eventTimeSec = (Long64_t)(eventTime/0.096); // event time in nanoseconds
	
      	if(outputOption==1){
      	  if (i==0) { flac << "Premiere entree! "<< std::endl;
      	    if (j-1>=0) {fout << j-1 << " " << pedSubtractedAdc_prev << " " << eventTimeSec << std::endl;}
      	    fout << j << " " << pedSubtractedAdc << " " << eventTimeSec << std::endl;
      	    if (j+1<128) {fout << j+1 << " " << pedSubtractedAdc_next << " " << eventTimeSec << std::endl;}
      	  }	  
      	  else {
      	    int k=selection[i]-selection[i-1];
      	    if (k==1) {
      	      flac << "k=1" << std::endl;
      	      if (j+1<128) {fout << j+1 << " " << pedSubtractedAdc_next << " " << eventTimeSec << std::endl;} 
      	    }
      	    else if (k==2) {
      	      flac << "k=2" << std::endl;
      	      fout << j << " " << pedSubtractedAdc << " " << eventTimeSec << std::endl;
      	      if (j+1<128) {fout << j+1 << " " << pedSubtractedAdc_next << " " << eventTimeSec << std::endl;}
      	    }
      	    else if (k>2){
      	      flac << "k>2" << std::endl;
      	      if (j-1>=0) {fout << j-1 << " " << pedSubtractedAdc_prev << " " << eventTimeSec << std::endl;}
      	      fout << j << " " << pedSubtractedAdc << " " << eventTimeSec << std::endl;
      	      if (j+1<128) {fout << j+1 << " " << pedSubtractedAdc_next << " " << eventTimeSec << std::endl;}
      	    }
      	    else {
      	      std::cout << "techniquement il n'est pas possible de se trouver ici!!!" << std::endl;
      	    }	    
      	  }
	  
      	  fout << "# " << std::endl;  
      	}
	
      
	
	
      	 if(outputOption==0){
      	  	if ( pedSubtractedAdc>=pcut ) {
      	  	  rawcluster.push_back(selection[i]);
      	  	}
      	  	else {
      	  	  bool mausole=false;
      	  	  if ( j>0 ) {
      	  	    if( pedSubtractedAdc_prev>=pcut ) mausole=true;
      	  	  }
      	  	  if ( j<127 ) {
      	  	    if( pedSubtractedAdc_next>=pcut ) mausole=true;
      	  	  }
      	  	  if ( mausole ) rawcluster.push_back(selection[i]);
	
      	  	}
      	  }
	
      	 // std::cout<<"Selection : " << i << "  : " << selection[i] << " ADC: "<< pedSubtractedAdc<<std::endl;     
	
  
      } // end  for(int i=0; i<selection.size(); i++)
	
      //       clusterGaps[j]=0.;

      nbClusters=0;
      //std::cout<<"Taille de rawcluster: "<<rawcluster.size()<<std::endl;
      //      if(rawcluster.size()){
      //for(int k=0; k<rawcluster.size(); ++k)std::cout<<rawcluster[k]<<std::endl;
      //}
     
      if(rawcluster.size()==0){
	++vide;	
      }
      if(rawcluster.size()>0)++nbClusters;      
      for(int i=0; i<rawcluster.size(); i++) {
	++clusterSize[nbClusters-1];
	clusterX[nbClusters-1]+=signalADC*rawcluster[i];
	clusterADC[nbClusters-1]+=Cdata[rawcluster[i]];
	if(i< rawcluster.size()-1){
	  if(rawcluster[i+1]-rawcluster[i]>1){ 
	     clusterX[nbClusters-1]/=clusterADC[nbClusters-1];
	    ++nbClusters;
	  }
	}

	//std::cout<< "Rawcluster : " << i << "  : " << rawcluster[i] << std::endl;     
      }
      // for(int k=0; k<nbClusters; ++k)std::cout<<"cluster Size: " << clusterSize[k] << std::endl;      

     
    }
    /////////////////////////////////////////////////////////////////
    
    timePreviousEvent = Time[0]; // save time for next event
    if(mode==1 && outputOption==0){
      NbCluster->Fill(nbClusters);
      for (int g=0; g<nbClusters; ++g){
    	ClusterADC->Fill(clusterADC[g]/peADC);//In number of photo electron
    	Cluster3D->Fill(nbClusters,clusterADC[g]/peADC);
    	ClusterSize->Fill(clusterSize[g]);
    	Clustersize3D->Fill(clusterSize[g],clusterADC[g]/peADC);
      }
    
    }
  
    //    std::cout<< "Nombre d'evt sans cluster: " <<vide << std::endl;     
  }// end loop over events

 std::cout << "On est plus la pour beurrer des sandwichs" << std::endl;
 std::cout << "pcut :"<<pcut<<" seccut: "<<seccut<< std::endl;

  //Pedestal storage in ASCII file
  if(mode==0){
    ofstream pout("pedestal.txt");
    for (UInt_t ich = 0; ich<128; ich++){
      PedArray[ich]=PedArray[ich]/double(signalEntries);
      PedSquaredArray[ich]=PedSquaredArray[ich]/double(signalEntries);
      NoiseArray[ich]=sqrt( PedSquaredArray[ich]- (PedArray[ich]* PedArray[ich]));
      //    pout<<PedArray[ich]<<" "<< PedSquaredArray[ich]<<" "<< signalEntries<<" "<< PedSquaredArray[ich]- (PedArray[ich]* PedArray[ich])<<" "<< NoiseArray[ich]<<std::endl;
      pout<<PedArray[ich]<<" "<< PedSquaredArray[ich]<<" "<< NoiseArray[ich]<<std::endl;
    }

    pout.close();
  
  } //end if

  if(outputOption==0) {
    TCanvas* canvas = new TCanvas("c2","c2", 1);  
    if (mode==0) {
      canvas->Divide(1, 1);
      canvas->cd(1);
      Pedestal->Draw("COLZ");
    } else {
      canvas->Divide(2, 2);
      canvas->cd(1);
      //Raw_data->Draw("COLZ");
      ADC_projection_truncated->Draw();
      ADC_projection->Draw("Same");
      canvas->cd(2);
      //    ADCvsEvent->Draw("COLZ");
      ADCvsChannelvsEvent->Draw("COLZ");
      canvas->cd(3);
      // Corrected_data->SetMinimum(18);
      Corrected_data->Draw("COLZ");
      canvas->cd(4);
      Common_mode1->Draw();
      Common_mode2->Draw("Same");
    }
  }
  TCanvas* canvas2 = new TCanvas("c3","c3", 1500,1500);
  canvas2->Divide(1, 3);
  canvas2->cd(1);
  PimpPierre1D(NbCluster,"Nb of clusters","",1);
  NbCluster->Draw();
  canvas2->cd(2);
  PimpPierre1D(ClusterADC,"Sum of photons per Cluster","",1);
  ClusterADC->Draw(); 
  canvas2->cd(3);
  //PimpPierre3D(Cluster3D,"Nb of clusters","Sum of pe","");
  //Cluster3D->Draw("LEGO1");
  //  canvas2->cd(4);  
  PimpPierre1D(ClusterSize,"Clusters size","",1);
  ClusterSize->Draw();
  // canvas2->cd(5);   
  //PimpPierre3D(Clustersize3D,"Clusters size","Sum of pe","");
  //Clustersize3D->Draw("LEGO1");

  TCanvas* canvas3 = new TCanvas("c4","c4", 1500,1500); 
  canvas3->Divide(1, 2);
  canvas3->cd(1);
  PimpPierre3D(Cluster3D,"Nb of clusters","Sum of pe","");
  Cluster3D->Draw("LEGO1");
  canvas3->cd(2);
  PimpPierre3D(Clustersize3D,"Clusters size","Sum of pe","");
  Clustersize3D->Draw("LEGO1");








  //  date=get_date();
  //  std::cout<<" Date du jour: "<< date<< std::endl;

  // Raw_data->Write();

  if(outputOption==1){ 
    fout.close();
    flac.close();
  }

  // std::cout << std::endl;
  
  //  dataManager.draw(firstChannel,lastChannel);
  // std::cerr << "finish draw"<<std::endl;

  signalFile.Close();
  //std::cerr << "finish signalFile.Close()"<<std::endl;  

  if(outputOption==0) app.Run();
  //std::cerr << "finish app.Run()"<<std::endl;

  return 0;
}





