CONFIG -= qt
CONFIG += debug

exists($(USBBOARDPATH)) {
    include($(USBBOARDPATH)/usbBoard.pri)
    include($(USBBOARDPATH)/RootCInt.pri)
    USBBOARDPATH=$(USBBOARDPATH)
} else {

    include(../../usbBoard.pri)
    include(../../RootCInt.pri)
    USBBOARDPATH=../..
}

DEPENDPATH+= \
    $$USBBOARDPATH/event \
    $$USBBOARDPATH/readout

INCLUDEPATH+= \
    $$DEPENDPATH

HEADERS+= \
    A2DataManagerTime.h

SOURCES+= \
    Analyze2uplinksTime.cpp \
    A2DataManagerTime.cpp

LIBS+=\
    -L$$USBBOARDPATH/support -lquickusb \
    -L$$USBBOARDPATH/Builds -lusbBoardEvent\
    -L$$USBBOARDPATH/Builds -lusbBoardReadout
