#include "DataManager.h"
#include <Settings.h>

#include <assert.h>
#include <iostream>

#include <MergedEvent.h>
#include <UsbBoardConfiguration.h>

#include <TH1.h>
#include <TH2.h>
#include <TH2F.h>
#include <TF1.h>
#include <TH3.h>
#include <TProfile.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TMath.h>
#include <TObjArray.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TGraphSmooth.h>

#include <TRandom.h>
#include <TSpectrum.h>
#include <TVirtualFitter.h>
#include <TROOT.h>
#include <TStyle.h>

#define CANVAS_X  1400
#define CANVAS_Y  1000

void DataManager::tracking(){}
