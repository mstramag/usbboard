#include "DataManager.h"
#include <Settings.h>

#include <assert.h>
#include <iostream>

#include <MergedEvent.h>
#include <UsbBoardConfiguration.h>

#include <TH1.h>
#include <TH2.h>
#include <TH2F.h>
#include <TF1.h>
#include <TH3.h>
#include <TProfile.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TMath.h>
#include <TObjArray.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TGraphSmooth.h>

#include <TRandom.h>
#include <TSpectrum.h>
#include <TVirtualFitter.h>
#include <TROOT.h>
#include <TStyle.h>

#define CANVAS_X  1400
#define CANVAS_Y  1000

void DataManager::ConfigHistoTH1(TH1* Histo, TString Title, TString axeXname, TString axeYname, Double_t axeXmin, Double_t axeXmax, double axeYmin, double axeYmax, double LineWidth)
{
    if(LineWidth) Histo->SetLineWidth(LineWidth); // 1 or 2
    if(Title != "") Histo->SetTitle(Title);
    if(Title != "") Histo->GetXaxis()->SetTitle(axeXname);
    if(Title != "") Histo->GetYaxis()->SetTitle(axeYname);
    if(axeXmax) Histo->SetAxisRange(axeXmin, axeXmax, "x");
    if(axeYmax) Histo->SetAxisRange(axeYmin, axeYmax, "y");

    Histo->SetTitleSize(0.06, "x");
    Histo->SetTitleSize(0.06, "y");
    Histo->SetTitleOffset(1, "x");
    Histo->SetTitleOffset(1, "y");
}

void DataManager::ConfigHistoTH2(TH2* Histo, TString Title, TString axeXname, TString axeYname, Double_t axeXmin, Double_t axeXmax, double axeYmin, double axeYmax)
{
    if(Title != "") Histo->SetTitle(Title);
    if(Title != "") Histo->GetXaxis()->SetTitle(axeXname);
    if(Title != "") Histo->GetYaxis()->SetTitle(axeYname);
    if(axeXmax) Histo->SetAxisRange(axeXmin, axeXmax, "x");
    if(axeYmax) Histo->SetAxisRange(axeYmin, axeYmax, "y");

    Histo->SetTitleSize(0.06, "x");
    Histo->SetTitleSize(0.06, "y");
    Histo->SetTitleOffset(1, "x");
    Histo->SetTitleOffset(1, "y");
}

// Cluster results histogram
void DataManager::Cluster_DrawHisto()
{
    std::cout << "*****>>> DataManager::" << __func__ << "() *****" << std::endl;
    char tmp[256];
    int Xmax;

    sprintf(tmp,"Clusterization Histogram");
    TCanvas* canvas = new TCanvas(tmp,tmp,0,0,CANVAS_X, CANVAS_Y);
    canvas->Divide(2,2);

    canvas->cd(1);
    m_NbCluster[0]->Draw("");
    Xmax = m_NbCluster[0]->FindLastBinAbove(0, 1); // (thrsd, 1=Xaxis)
    ConfigHistoTH1(m_NbCluster[0], "Number of cluster", "Number of cluster","Number of Entries", 0, Xmax+5, 0, 0, 2);

    canvas->cd(2);
    m_ClusterSize[0]->Draw("");
    Xmax = m_ClusterSize[0]->FindLastBinAbove(0, 1);
    ConfigHistoTH1(m_ClusterSize[0], "Cluster size", "Cluster size","Number of Entries", 0, Xmax+1, 0, 0, 2);

    canvas->cd(3);
    m_ClusterSum[0]->Draw("");
    Xmax = m_ClusterSum[0]->FindLastBinAbove(0, 1);
    ConfigHistoTH1(m_ClusterSum[0], "Cluster sum", "Cluster Sum","Number of Entries", 0, Xmax, 0, 0, 1);

    TF1 *f1 = new TF1("f1","gaus",10,30);
    m_ClusterSum[0]->Fit("f1","R");

    canvas->cd(4);
    m_ClusterChannel[0]->Draw("");
    Xmax = m_ClusterChannel[0]->FindLastBinAbove(0, 1);
    ConfigHistoTH1(m_ClusterChannel[0], "Cluster channel", "Cluster channel","Number of Entries", 0, Xmax, 0, 0, 1);

}

// Search Cluster Algorithm (3 Thresholds: Seed, Neighb, Sum)
void DataManager::Cluster_Search(std::vector<std::vector<int> > &eventADC, float threshold[3]){

    bool    EndOfCluster = false;
    int     clusterSize = 0;
    int     clusterSizeMAX = 15;
    int     nbCluster = 0;
    int     first_ch_in_cluster = 0;


    int     NumberChannels = 0;
    float   sum = 0;

    vector<int> clusterSize_fin;
    vector<int> first_ch_in_cluster_vec;
    vector<float> sum_vec;
    vector<double> mean_position_vec;

    float   eventPE[SIPM_READOUT_CHANNELS];
    float   SeedCutPE = threshold[0];
    float   NeighborCutPE = threshold[1];
    float   SumCutPE = threshold[2];

    double mean_position = 0.0;

    float mean_position_num = 0.0;
    float mean_position_den = 0.0;

    //std::cout << "NumberChannels = " << m_NumberChannels << std::endl;
    //std::cout << "Event_n = " << EventNr << std::endl;
    // EventNumber=EventNr;
    unsigned short numberOfReadoutChips = eventADC.size();
    for (unsigned int chip = 0; chip < numberOfReadoutChips; ++chip) {
      int chipclusters=0;
      SiPMNum.push_back(chip);
      double positiontemp;
        NumberChannels = SIPM_READOUT_CHANNELS;
	cout << NumberChannels;
        // rescale data from ADC to PE
        for (int channel=0; channel<NumberChannels; channel++){
            eventPE[channel] = eventADC.at(chip)[channel] / m_SipmGains[chip];


        }
        // Killing two channels atrificially
        /*if(chip==1) {
            eventPE[42] = 0;
            eventPE[41] = 0;
        }*/

        // Print the active channels
        //std::cout << "signalPE raw: ["<< chip <<"][Channel]Value :";
        //for (int channel=0; channel<NumberChannels; channel++)
        //  if (eventPE[channel] >= 1.5)
        //      std::cout << "["<< channel << "]" << eventPE[channel] <<" ";
        //std::cout << std::endl;

        // cluster search, start to find channels passing the seed cut
        for (int channel=0; channel<NumberChannels; ++channel){

            if (eventPE[channel] >= SeedCutPE){                                             // Search for seed threshold
                ++clusterSize;
            }
            else if ((clusterSize >= 1 )){                                                  // no more seeds in cluster
                EndOfCluster=true;
                first_ch_in_cluster = channel - clusterSize;
            }

            if ((clusterSize == 1) && (channel == NumberChannels - 1)){                     // last channel reached (Check for single channel Cluster)
                first_ch_in_cluster = channel;
                EndOfCluster=true;
            }

            if ((clusterSize > 1 ) && (channel == NumberChannels - 1)){                     // Looking for Cluster with the last channel included
                EndOfCluster=true;
                first_ch_in_cluster = channel - clusterSize + 1;
            }

            if ((clusterSize == clusterSizeMAX - 2) && (!EndOfCluster))  {                  // maximum size reached
                ++channel;                                                                  // go to next channel to search for neighbour threshold
                first_ch_in_cluster = channel - clusterSize;                                // NOTE: Cluster with the size greater than clusterSizeMAX will be breaked
                EndOfCluster=true;
            }

            // cluster finnished so we need to add the neighbors
            if (EndOfCluster){

                if(first_ch_in_cluster > 0){                                                // check left neighbour
                    if (eventPE[first_ch_in_cluster - 1] >= NeighborCutPE){
                        ++clusterSize;
                        first_ch_in_cluster = first_ch_in_cluster - 1;
                    }
                }
                if(channel < NumberChannels - 1){                                           // check right neighbour
                    if (eventPE[channel] >= NeighborCutPE){
                        ++clusterSize;
                    }
                }

                // calculate and check the sum threshold
                for (int cluster_ch = 0; cluster_ch < clusterSize; cluster_ch++){
                    sum += eventPE[first_ch_in_cluster + cluster_ch];
                }

                if(sum >= SumCutPE){
		  chipclusters++;
                    nbCluster++;
                    clusterSize_fin.push_back(clusterSize);
                    first_ch_in_cluster_vec.push_back(first_ch_in_cluster);
                    sum_vec.push_back(sum);

                    // Weighted Mean calculation
                    for(int itt=0; itt < clusterSize; itt++){
                        mean_position_num += eventPE[first_ch_in_cluster+itt]*(float)itt;
                        mean_position_den += eventPE[first_ch_in_cluster+itt];
                        eventPE[first_ch_in_cluster+itt] = 0;   // Clean values already evaluated to avoid duplicate channel in two clusters
                    }

                    mean_position = mean_position_num / mean_position_den;
                    mean_position_vec.push_back((chip*SIPM_READOUT_CHANNELS) + first_ch_in_cluster + mean_position);
                    positiontemp=first_ch_in_cluster + mean_position;

                    if(0) {
                        std::cout << std::endl;
                        std::cout << "CHIP = " << chip << std::endl;
                        //std::cout << "signalPE raw: [Channel]Value :";
                        //for (int show=0; show<NumberChannels; show++){
                        //if (eventPE[show] > 1.5)  std::cout << "["<< show << "]" << eventPE[show] <<" ";    }
                        std::cout << std::endl;
                        std::cout << "nbCluster\t : "       << nbCluster            << std::endl;
                        std::cout << "Cluster POS\t : "     << first_ch_in_cluster  << std::endl;
                        std::cout << "POS Mean\t : "        << mean_position        << std::endl;
                        std::cout << "Cluster Size\t : "    << clusterSize          << std::endl;
                        std::cout << "Cluster Sum\t : "     << sum                  << std::endl;
                        std::cout << endl;
                    }

		 

                    // Filling the Cluster Histograms: showing all events
                    /*m_ClusterChannel[0]->Fill((chip*SIPM_READOUT_CHANNELS)+ first_ch_in_cluster + mean_position);
                    m_ClusterSum[0]->Fill(sum);
                    m_ClusterSize[0]->Fill(clusterSize);*/

                }

                EndOfCluster=false;
                clusterSize=0;
                mean_position = 0.0;
                mean_position_num = 0.0;
                mean_position_den = 0.0;
                sum = 0.0;
                first_ch_in_cluster = 0;

            }
	    if(chipclusters==1)ClusterPosition.push_back(positiontemp);
	    else ClusterPosition.push_back(-1);
	    std::cout <<"chip: "<<chip <<" position: "<<positiontemp<<std::endl;
        }

    }
    
    m_NbCluster[0]->Fill(nbCluster);

    // Analyze only events with "select_nb_cluster" number of clusters
    // Filling the Cluster Histograms
    int select_nb_cluster(1);
    double spot_position(160);
    if(nbCluster == select_nb_cluster && abs((int)(mean_position_vec[0]-spot_position))<15.) {
        for(unsigned int j(0); j<select_nb_cluster; ++j) {
            m_ClusterChannel[0]->Fill(mean_position_vec[j]);
            m_ClusterSum[0]->Fill(sum_vec[j]);
            m_ClusterSize[0]->Fill(clusterSize_fin[j]);
        }
    }
}
