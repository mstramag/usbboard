#ifndef DataManager_h
#define DataManager_h

#include <vector>
#include <TH1.h>
#include <TF1.h>
#include <TH3.h>
#include <TGraphErrors.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>
#include <map>
#include <stdint.h>
#include <TTree.h>

//#define DUMP_DEBUG

using namespace std;

class MergedEvent;
class TH1;
class TH2;
class TH3;
class TH2F;
class TFile;
class TTree;
class TProfile;

typedef std::map<int,int> SuperLayerMap;    //map<positon,uplinkId>

enum SuperLayerPosition{
    P1_P = 0,
    P1_N = 1,
    P2_P = 2,
    P2_N = 3,
    P3_P = 4,
    P3_N = 5,
    P4_P = 6,
    P4_N = 7
};

class DataManager {
public:

    const static unsigned short maxAdcValue = 4096;
    const static unsigned short SIPM_READOUT_CHANNELS = 128;

    //static long counter = 0;

    DataManager(std::string pathToSetupFile);
    ~DataManager();

    void fill_raw_pedestals(MergedEvent* event);
    void pedestals_write_Mean_RMS();

    void draw();
    void SaveAllHistos();

    void GetRawsignalADCdata_1event(MergedEvent* event, std::vector<std::vector<int> > &eventADC);
    void GetsignalADC_PedSub(std::vector<std::vector<int> > &in_eventADC, std::vector<std::vector<int> > &out_eventADC_PS);

    // Clustering tools
    void ConfigHistoTH1(TH1* Histo, TString Title, TString axeXname, TString axeYname, double axeXmin, double axeXmax, double axeYmin, double axeYmax, double LineWidth);
    void ConfigHistoTH2(TH2* Histo, TString Title, TString axeXname, TString axeYname, Double_t axeXmin, Double_t axeXmax, double axeYmin, double axeYmax);
    void Cluster_Search(std::vector<std::vector<int> > &eventADCin, float threshold[3]);
    void Cluster_DrawHisto();


    uint16_t GetChannelIndexOfSipm128(uint16_t channelIndexOnVata64Chip);
    int Get_NumberChannels();
    int Get_NumberSIPM();


private:
    unsigned int uplinkIndex(int uplinkId);


    std::vector< std::vector<int> > m_Sipm_uplinkId;
    std::vector<float *> m_pedestal_Mean;
    std::vector<float *> m_pedestal_RMS;

    std::vector<TH2*> m_raw_pedestals;
    std::vector<TH2*> m_adcHistogram;
    std::vector<TH2*> m_PS_adcHistogram;

    /**********/

    int m_NumberChannels;
    int m_NumberOfSipm;
    std::vector<float> m_SipmGains;

    std::vector<TH1*> m_NbCluster;
    std::vector<TH1*> m_ClusterSize;
    std::vector<TH1*> m_ClusterADC;
    std::vector<TH1D*> m_ClusterChannel;
    std::vector<TH1D*> m_ClusterSum;

    TTree * m_TrackingTree;
    std::vector<double> ClusterPosition;
    std::vector<int> SiPMNum;
    int EventNumber;
    TFile* file_data;

    std::string m_pathToFormerCfgFile; // only the path to the folder, the file name is generated automatically .
    bool m_forceToChangeCfgFile;

    SuperLayerMap m_superLayerMap;
    bool m_readInSetupFile;

};

#endif
