#ifndef CONFIGFILE_H
#define CONFIGFILE_H

#include <string>
#include <map>
#include <vector>
// Boost
#include <boost/regex.hpp>
#include <boost/algorithm/string/regex.hpp>
#include <boost/range/algorithm.hpp>

#include "Helpers.h"

typedef std::vector<std::string> strings ;

class ConfigFile {
public:
    ConfigFile(std::string const& configFile);
    
    std::vector<std::string> getKeys() ;
    bool                     hasKey(std::string const&) const ;
    std::vector<std::string> getValue(std::string const&) const ;
private:
    std::map<std::string, strings>    m_content ;
    std::vector<std::string>          m_keys    ;

};

#endif 
