CONFIG -= qt
CONFIG += debug
exists($(USBBOARDPATH)) { 
    include($(USBBOARDPATH)/usbBoard.pri)
    include($(USBBOARDPATH)/RootCInt.pri)
    USBBOARDPATH = $(USBBOARDPATH)
}
else { 
    include(../../usbBoard.pri)
    include(../../RootCInt.pri)
    USBBOARDPATH = ../..
}
DEPENDPATH += $$USBBOARDPATH/event \
    $$USBBOARDPATH/readout
INCLUDEPATH += $$DEPENDPATH
HEADERS += RunData.h ConfigFile.h Geometry.h Helpers.h
SOURCES += main.cpp \
	   RunData.cpp \
	   ConfigFile.cpp \
           Geometry.cpp 
!NO_QUSB:LIBS += -L$$USBBOARDPATH/support \
    -lquickusb \
    -L$$USBBOARDPATH/Builds \
    -lusbBoardEvent \
    -L$$USBBOARDPATH/Builds \
    -lusbBoardReadout \
    -L$$ROOTSYS/lib \
    -lSpectrum \ 
    -lboost_regex \
    -lboost_program_options
NO_QUSB:LIBS += -L$$USBBOARDPATH/Builds \
    -lusbBoardEvent \
    -L$$USBBOARDPATH/Builds \
    -lusbBoardReadout \
    -L$$ROOTSYS/lib \
    -lSpectrum \
    -lboost_regex \
    -lboost_program_options
