CONFIG -= qt
CONFIG += debug

exists($(USBBOARDPATH)) {
    include($(USBBOARDPATH)/usbBoard.pri)
    include($(USBBOARDPATH)/RootCInt.pri)
    USBBOARDPATH=$(USBBOARDPATH)
} else {

    include(../../usbBoard.pri)
    include(../../RootCInt.pri)
    USBBOARDPATH=../..
}

DEPENDPATH+= \
    $$USBBOARDPATH/event \
    $$USBBOARDPATH/readout

INCLUDEPATH+= \
    $$DEPENDPATH

HEADERS+= \
    lect_uplinks_with_extented_features.h

SOURCES+= \
    main_lect_uplinks_with_extented_features.cpp \
    lect_uplinks_with_extented_features.cpp

!NO_QUSB{
   LIBS += \
     -L$$USBBOARDPATH/support -lquickusb \
     -L$$USBBOARDPATH/Builds -lusbBoardEvent \
     -L$$USBBOARDPATH/Builds -lusbBoardReadout \
     -L$$ROOTSYS/lib -lSpectrum
   
}

NO_QUSB{
  LIBS += \
      -L$$USBBOARDPATH/Builds -lusbBoardEvent \
      -L$$USBBOARDPATH/Builds -lusbBoardReadout \
      -L$$ROOTSYS/lib -lSpectrum
}
