#include <getopt.h>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <vector>
#include <TH1.h>
#include <TF1.h>
#include <TApplication.h>
#include <TFile.h>
#include <TTree.h>
#include <TBrowser.h>
#include <TROOT.h>
#include <TColor.h>
#include <TStyle.h>
#include <RunDescription.h>
#include <MergedEvent.h>
#include "Settings.h"
#include "lect_uplinks_with_extented_features.h"
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <assert.h>

void setRootStyle()
{
    gStyle->SetOptStat(1111);
    gStyle->SetOptFit(1111);
    gStyle->SetCanvasColor(10);
    gStyle->SetCanvasBorderMode(0);
    gStyle->SetFrameLineWidth(1);
    gStyle->SetFrameFillColor(10);
    gStyle->SetPadColor(10);
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    gStyle->SetPadTopMargin(0.15);
    gStyle->SetPadBottomMargin(0.15);
    gStyle->SetPadLeftMargin(0.15);
    gStyle->SetPadRightMargin(0.15);
    gStyle->SetPadGridX(true);
    gStyle->SetPadGridY(true);
    gStyle->SetStatColor(10);
    gStyle->SetStatX(.8);
    gStyle->SetStatY(.8);
    gStyle->SetStatH(.09);
    gStyle->SetHistLineWidth(1);
    gStyle->SetFuncWidth(1);
    gStyle->SetMarkerStyle(20);
    gStyle->SetTitleOffset(1.5, "y");

    gStyle->SetLineScalePS(3.0);
    gStyle->SetTextFont(42);
    gStyle->SetTitleFont(42, "X");
    gStyle->SetTitleFont(42, "Y");
    gStyle->SetTitleFont(42, "Z");
    gStyle->SetLabelFont(42, "X");
    gStyle->SetLabelFont(42, "Y");
    gStyle->SetLabelFont(42, "Z");
    gStyle->SetLabelSize(0.03, "X");
    gStyle->SetLabelSize(0.03, "Y");
    gStyle->SetLabelSize(0.03, "Z");

    int NRGBs = 5;
    int NCont = 255;
    double stops[] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    double red[]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    double green[] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    double blue[]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}

struct globalArgs_t
{
    const char * arg_pathToPedestal;	/* -p option */
    const char * arg_pathToSignal;	/* -s option */
    const char * arg_pathToAdjustFile;	/* -a option */
    const char * arg_pathToCfgFileToBeWritten;	/* -f option */
    const char * arg_pathToDACCfgFile;	/* -g option */
    bool arg_drawProjY;			/* -y option */
    bool arg_ProjYinLog;
    bool arg_analyzeLed;                  /* -l option */
    bool arg_analyzeCosmics;              /* -c option */
    bool arg_findMuonsTracks;              /* -t option */
    bool arg_convert;              /* -C option */
    bool arg_useAdjustFileSet;
    bool arg_useCfgFileSet;
    bool arg_forceToChangeCfgFile;
    int arg_aimGain;
    int arg_currentGain;
    bool arg_readFromExistRootFile;
    const char * arg_pathToExistRootFile;
    bool arg_readInSetupFile;
    const char * arg_pathToSetupFile;
    bool arg_findRatio;            /* -R option or --findRatio */
    const char * arg_pathToLedN;	/* --LedN option */
    const char * arg_pathToLedP;	/* --LedP option */
}globalArgs;

static const char *optString = "r:p:s:a:f:g:y:n:G:S:RltcCh?";

static const struct option longOpts[] = {
        { "findRatio", no_argument, NULL, 'R' },
        { "LedN", required_argument, NULL, 0 },
        { "LedP", required_argument, NULL, 0 },
        { "help", no_argument, NULL, 'h' },
        { NULL, no_argument, NULL, 0 }
};

int main(int argc, char* argv[])
{
    // initialize globalArgs
    globalArgs.arg_pathToPedestal = " ";
    globalArgs.arg_pathToSignal = " ";
    globalArgs.arg_pathToAdjustFile = " ";
    globalArgs.arg_pathToCfgFileToBeWritten = " ";
    globalArgs.arg_pathToDACCfgFile = " ";
    globalArgs.arg_drawProjY = false;
    globalArgs.arg_ProjYinLog = false;
    globalArgs.arg_aimGain = 12;
    globalArgs.arg_analyzeLed = false;
    globalArgs.arg_analyzeCosmics = false;
    globalArgs.arg_findMuonsTracks = false;
    globalArgs.arg_convert = false;
    globalArgs.arg_findRatio = false;
    globalArgs.arg_useAdjustFileSet =false;
    globalArgs.arg_useCfgFileSet = false;
    globalArgs.arg_forceToChangeCfgFile = false;
    globalArgs.arg_currentGain =0;
    globalArgs.arg_readFromExistRootFile =false;
    globalArgs.arg_pathToExistRootFile = " ";
    globalArgs.arg_readInSetupFile = false;
    globalArgs.arg_pathToSetupFile = " ";
    globalArgs.arg_pathToLedN = " ";
    globalArgs.arg_pathToLedP = " ";

    //read from the command line/////////////////////////////////////////////////////////////////
    //Get paremeter from the commond
    int opt =0;
    int longIndex = 0;
    opt = getopt_long( argc, argv, optString, longOpts, &longIndex );
    if(opt == -1){
        std::cerr <<  "There is no opption in the command! Type \"lect_uplinks -h\" for help." << std::endl;
        exit(EXIT_FAILURE);
    }

    while(opt != -1){
        switch(opt){
        case 'r':
            globalArgs.arg_readFromExistRootFile = true;
            globalArgs.arg_pathToExistRootFile = optarg;
            break;
        case 'p':
            globalArgs.arg_pathToPedestal =optarg;
            //std::cout<<"-p option path= "<<globalArgs.arg_pathToPedestal<<std::endl;
            break;
        case 's':
            globalArgs.arg_pathToSignal =optarg;
            //std::cout<<"-s option path= "<<globalArgs.arg_pathToSignal<<std::endl;
            break;
        case 'a':
            globalArgs.arg_useAdjustFileSet =true;
            globalArgs.arg_pathToAdjustFile =optarg;
            std::cout<<"-a option path= "<<globalArgs.arg_pathToAdjustFile<<std::endl;
            break;
        case 'f':
            globalArgs.arg_forceToChangeCfgFile =true;
            globalArgs.arg_pathToCfgFileToBeWritten =optarg;
            //std::cout<<"-f option path= "<<globalArgs.arg_pathToCfgFileToBeWritten<<std::endl;
            break;
        case 'g':
            globalArgs.arg_useCfgFileSet = true;
            globalArgs.arg_pathToDACCfgFile = optarg;
            //std::cout<<"-g option path= "<<globalArgs.arg_pathToDACCfgFile<<std::endl;
            break;
        case 'y':
            globalArgs.arg_drawProjY = true;
            if(atoi(optarg)) globalArgs.arg_ProjYinLog =true;
            else globalArgs.arg_ProjYinLog = false;
            break;
        case 'l':
            globalArgs.arg_analyzeLed = true;
            break;
        case 'c':
            globalArgs.arg_analyzeCosmics = true;
            break;
        case 't':
            globalArgs.arg_findMuonsTracks = true;
            break;
        case 'C':
            globalArgs.arg_convert = true;
            break;
        case 'R':
            globalArgs.arg_findRatio = true;
            break;
        case 'n':
            globalArgs.arg_aimGain = atoi(optarg);
            break;
        case 'G':
            globalArgs.arg_currentGain = atoi(optarg);
            break;
        case 'S':
            globalArgs.arg_readInSetupFile =  true;
            globalArgs.arg_pathToSetupFile = optarg;
            std::cout<<"-S option path= "<<globalArgs.arg_pathToSetupFile<<std::endl;
            break;
        case 0:		/* long option without a short arg */
            if( strcmp( "LedN", longOpts[longIndex].name ) == 0 ) {
                globalArgs.arg_pathToLedN = optarg;
                //std::cout<<"--LedN option path= "<<globalArgs.arg_pathToLedN<<std::endl;
            }
            if( strcmp( "LedP", longOpts[longIndex].name ) == 0 ) {
                globalArgs.arg_pathToLedP = optarg;
                //std::cout<<"--LedP option path= "<<globalArgs.arg_pathToLedP<<std::endl;
            }
            break;
        case 'h':
        case '?':
            std::cerr << "Usage: lect_uplinks -p pathToPedestal -s pathToSignal  UplinkToBeAnalyzed \n[-S pathToSetupFile] [-a(read in DAC from adjustDAC file) pathToDirOfAdjustDACFile] [-g(read in DAC from cfg file) pathToDirOfCfgFile] [-f(force to modify DAC in the cfg file) pathToDirOfCfgFileToBeWritten] [-n aimGain] [-y(draw projectionY) 0(normal)/1(in log scale)] [-l (analyse led data)] [-c (analyse cosmic data)]" << std::endl;
            std::cerr << "----------------------------------------------------------------------------------------------------"<<std::endl;
            std::cerr << " either '-r' option or '-p'+'-s' options is necessary!"<<std::endl;
            std::cerr << " -r option: fill all the necessary histograms by an exist root file."<<std::endl;
            std::cerr << " -p and -s options: fill the histograms by the data getting from DAQ. In this case both pedestal root file and signal root file are needed! " <<std::endl;
            std::cerr << " UplinkToBeAnalyzed : please list all the uplinks you want to analyse, otherwise all the uplinks will be analysed by default."<<std::endl;
            std::cerr << " -S option: read in setup file."<<std::endl;
            std::cerr << "-----------------------------------------------------------------------------------------------------"<<std::endl;
            std::cerr << " To get proposed DACs : either '-a' or '-g' option has to be set , in order to give an proper path to read in the DAC values.But they can not be set at the same time! "<<std::endl;
            std::cerr << " -a option: read in DACs from the appointed file, and based on fit results,write the proposed DACs in the same file." <<std::endl;
            std::cerr << " -g option: read in DACs from the appointed cfg file, if '-f' is not set, write the proposed DACs in the ../data/adjustDAC_uplinkXX.txt   XX stands for the uplinkId number."<<std::endl;
            std::cerr << " -f option: force to write the proposed DACs from the fit into the appointed cfg file."<<std::endl;
            std::cerr << " -n option: set the aim GAIN you want, the code will caculate the proposed DACs in order to achieve this gain. if this option is not set, the aim Gain will be set to 12 by default."<<std::endl;
            std::cerr << "-----------------------------------------------------------------------------------------------------"<<std::endl;
            std::cerr << " -y option: set this to draw projectionY for each channel. "<<std::endl;
            std::cerr << " -l option: set to analyse the led data. i.e. get the Gain in adc, mean in p.e, caculate proposed DACs if the above options are set. ..."<<std::endl;
            std::cerr << " -c option: set to analyse the cosmic data. i.e. get the Gain in adc, MIP in adc, MIP in p.e. Attention: in this mode, proposed DACs will not be caculated even certain options are set."<<std::endl;
            std::cerr << " -t option: set to find the muon track and fill a root tree with the data"<<std::endl;
            std::cerr << " -C option: fill a root tree"<<std::endl;
            std::cerr << " -G option: set the operation Gain of SiPM under which the data to be analyzed are obtained. This will help to fit the gain and MIP better. Once -l or -c option is set, it is obliged to set the current Gain value by '-G', if you are not sure about the Gain,just give any probably value except for 0!"<<std::endl;
            std::cerr << " -R or --findRatio option: find the MPPC/MAPD ratio, caculate proposed DACs if the option -a or -g is set."<<std::endl;
            std::cerr << "Such as:" << std::endl
                    << "./lect_uplinks -p ../data/pedestals.root -s ../data/test.root 44 45 46 47 -a ../data/ -n 11 -y 1 -l" << std::endl
                    << "----------------------------------------------------------------------------------"<<std::endl
                    << "To change DAC values based on fit result, write directly into the cfg files: "<<std::endl
                    << "./lect_uplinks -p /home/pebs_ecal/pedestal.root -s /home/pebs_ecal/test.root 46 -y 0 -l -G 12 -g ../cfgFiles/vata64V2/ -f ../cfgFiles/vata64V2/"<<std::endl
                    << "./lect_uplinks_devel -p pedestal.root --LedN ledN.root --LedP ledP.root -S setup.txt -R -g ../cfgFiles/fullSetup -f ../cfgFiles/fullSetup_adjust"<<std::endl;
            exit(EXIT_FAILURE);
            break;
        default:
            break;
        }
        opt = getopt_long( argc, argv, optString, longOpts, &longIndex );
    }
    

    if(globalArgs.arg_readFromExistRootFile){
        if((globalArgs.arg_pathToPedestal != " ") || (globalArgs.arg_pathToSignal != " ")){
            std::cerr<< "ERROR: -r option is set! All the analyse will base on an exist file. Neither pathToPedestal or pathToSignal can be set now!"<<std::endl;
            exit(EXIT_FAILURE);
        }
    }

    if(globalArgs.arg_analyzeCosmics || globalArgs.arg_analyzeLed){
        if(globalArgs.arg_currentGain == 0){
            std::cerr<<"ERROR: -l or -c option is set! Now it is obliged to set the current Gain by '-G' option, in order to fit the gain and MIP properly!"<<std::endl;
            exit(EXIT_FAILURE);
        }
    }

    if(globalArgs.arg_findRatio){
        if((globalArgs.arg_pathToPedestal == " ") ||(globalArgs.arg_pathToLedN == " ") || (globalArgs.arg_pathToLedP == " ") || (!globalArgs.arg_readInSetupFile)){
            std::cerr<< "ERROR: -R option is set! the 4 path to the pedestal, LedN, LedP, setup files are needed!"<<std::endl;
            exit(EXIT_FAILURE);
        }
    }

    if((globalArgs.arg_pathToPedestal == " " || globalArgs.arg_pathToSignal == " ")&&(!globalArgs.arg_readFromExistRootFile)&&(!globalArgs.arg_findRatio)){
        std::cerr << "ERROR: -p or -s option is not set! Both of them has to be set correctly!"<<std::endl;
        exit(EXIT_FAILURE);
    }

    if(globalArgs.arg_useAdjustFileSet && globalArgs.arg_useCfgFileSet ){
        std::cerr <<"ERROR: '-a' and '-g' options can not be set at the same time. The DAC values read in can only either from the adjust file or the cfg file."<<std::endl;
        exit(EXIT_FAILURE);
    }

    if(globalArgs.arg_analyzeCosmics && globalArgs.arg_analyzeLed) {
        std::cerr <<"ERROR: '-l' and '-c' options can not be set at the same time. Either analyse cosmics data or led injection data."<<std::endl;
        exit(EXIT_FAILURE);
    }

    if(globalArgs.arg_findMuonsTracks && globalArgs.arg_analyzeLed) {
        std::cerr <<"ERROR: '-l' and '-t' options can not be set at the same time. Either analyse cosmics data or led injection data."<<std::endl;
        exit(EXIT_FAILURE);
    }

    if(globalArgs.arg_findMuonsTracks && globalArgs.arg_readFromExistRootFile) {
        std::cerr <<"ERROR: '-t' options can not be set when we read an existing root file"<<std::endl;
        exit(EXIT_FAILURE);
    }

    if(globalArgs.arg_findMuonsTracks && (!globalArgs.arg_readInSetupFile)) {
        std::cerr <<"ERROR: '-t' option can not be set if the setup file is not given"<<std::endl;
        exit(EXIT_FAILURE);
    }

    if(globalArgs.arg_convert && (!globalArgs.arg_readInSetupFile)) {
        std::cerr <<"ERROR: '-C' option can not be set if the setup file is not given"<<std::endl;
        exit(EXIT_FAILURE);
    }

    if(globalArgs.arg_convert && globalArgs.arg_findMuonsTracks) {
        std::cerr <<"ERROR: '-C' and 't' options can not be set at the same time"<<std::endl;
        exit(EXIT_FAILURE);
    }
    
    int uplinkNumbers = argc-optind;
    std::cout << "uplinkNumbers = " << uplinkNumbers<<std::endl;

    int uplink[uplinkNumbers];
    for(int i=optind; i<argc;i++) { uplink[i-optind] = atoi(argv[i]); std::cout<<"uplink["<<i-optind<<"]="<<uplink[i-optind]<<std::endl;}

    std::vector<int> uplinksToBeAnalyzed;
    for (int i = 0; i < uplinkNumbers; ++i) {
        uplinksToBeAnalyzed.push_back(uplink[i]);
    }

    if (uplinksToBeAnalyzed.size() == 0) {
        Settings* settings = Settings::instance();
        for (unsigned int i = 0; i < settings->numberOfUsbBoards(); ++i) {
            //for (unsigned int j = 1; j < Settings::s_uplinksPerUsbBoard + 1; ++j) {
            for (unsigned int j = 1; j < 9; ++j) {
                int usbBoardId = settings->usbBoardId(i);
                int uplinkId = settings->uplinkId(usbBoardId, j);
                uplinksToBeAnalyzed.push_back(uplinkId);
            }
        }
    }

    printf("uplinksToBeAnalyzed.size() = %d\n",uplinksToBeAnalyzed.size());
    setRootStyle();
    TApplication app("readout_calibration", &argc, argv);

    if(globalArgs.arg_readInSetupFile){
        std::cout<< "-S option is set, uplinks to be analyzed will be defined by the setupfile: "<< globalArgs.arg_pathToSetupFile <<"\nuplinks set in the command line will be ommited!"<<std::endl;
    }


    if(!globalArgs.arg_readFromExistRootFile && !globalArgs.arg_readInSetupFile && !globalArgs.arg_findRatio){


        //////////////////Fill DATA PART/////////////////////////////////////////////////////////////////////////////////////////////////
        printf("begin to fill data\n");

        TFile pedestalFile(globalArgs.arg_pathToPedestal);
        TFile signalFile(globalArgs.arg_pathToSignal);


        gROOT->cd();
        TTree* pedestalTree = static_cast<TTree*>(pedestalFile.Get("ReadoutData"));
        TTree* signalTree = static_cast<TTree*>(signalFile.Get("ReadoutData"));

        RunDescription* runDescription = static_cast<RunDescription*>(pedestalTree->GetUserInfo()->First());
        runDescription->dump(std::cout);
        printf("------------------------finished dump runDescription()\n");


        std::stringstream settingsStream(runDescription->settings());
        Settings::instance()->loadFromStream(settingsStream);

        printf("------------------------finished loadFromStream()\n");

        /////////////////////////////////////////////////////////////////////


        DataManager dataManager(uplinksToBeAnalyzed);

        if(globalArgs.arg_aimGain != 12) dataManager.SetAimGain(globalArgs.arg_aimGain);
        else dataManager.SetAimGain(12);


        MergedEvent* event_pedestal = 0;
        pedestalTree->SetBranchAddress("events", &event_pedestal);
        long pedestalEntries = pedestalTree->GetEntries();
        std::cout << "taking event_pedestals"<< std::endl;
        std::cout << std::endl;

        for (long it = 1; (it < pedestalEntries); ++it) {
            pedestalTree->GetEntry(it);
            dataManager.fill_raw_pedestals(event_pedestal);
        }


        dataManager.find_working_channels();

        dataManager.pedestals_write_Mean_RMS();


        for (long it = 1; (it < pedestalEntries); ++it) {
            pedestalTree->GetEntry(it);
            dataManager.FillPsPedestal(event_pedestal);
        }

        MergedEvent* event_signal = 0;
        signalTree->SetBranchAddress("events", &event_signal);
        long signalEntries = signalTree->GetEntries();
        std::cout << "taking event_signals, signalEntries="<<signalEntries<< std::endl;
        std::cout << std::endl;

        for (long it = 1; (it < signalEntries); ++it) {
            signalTree->GetEntry(it);
            dataManager.getEnergy(event_signal);
        }

        std::cout<<"OK"<<std::endl;


        printf("before dataManager.draw()\n");
        dataManager.draw();



        if(globalArgs.arg_drawProjY){
            if(globalArgs.arg_ProjYinLog) dataManager.PlotProjectionY(true);
            else dataManager.PlotProjectionY(false);
        }

        if(globalArgs.arg_useAdjustFileSet){
            dataManager.ClearDacFromCfgFile();
            dataManager.SetDacFromAdjustFile();
            if(globalArgs.arg_pathToAdjustFile == " ") {
                std::cerr <<"ERROR: -a option is set, DACs are supposed to be read in from the adjustDAC files, but path to the Dir of these files is not set properly!\n";
                exit(EXIT_FAILURE);
            }
            dataManager.SetPathToDACFile(globalArgs.arg_pathToAdjustFile);
        }

        if(globalArgs.arg_useCfgFileSet){
            dataManager.SetDacFromCfgFile();
            dataManager.ClearDacFromAdjustFile();
            if(globalArgs.arg_pathToDACCfgFile == " ") {
                std::cerr <<"ERROR: -a option is set, DACs are supposed to be read in from the cfg files, but path to the Dir of these files is not set properly!\n";
                exit(EXIT_FAILURE);
            }
            dataManager.SetPathToDACFile(globalArgs.arg_pathToDACCfgFile);
        }

        if(globalArgs.arg_forceToChangeCfgFile){
            dataManager.SetFroceToChangeCfgFile();
            if(globalArgs.arg_pathToCfgFileToBeWritten == " ") {
                std::cerr <<"ERROR: -f option is set, but path to dir of cfg files to be modified is not set properly!\n";
                exit(EXIT_FAILURE);
            }
            dataManager.SetPathToFormerCfgFile(globalArgs.arg_pathToCfgFileToBeWritten);
        }

        if(globalArgs.arg_analyzeLed){
            if(globalArgs.arg_currentGain) dataManager.AnalyzeLedData(globalArgs.arg_currentGain);
            else{
                std::cerr<<"ERROR:current Gain can not be set to 0. To continue analyse led injection data, please set the current Gain properly by option '-G' !"<<std::endl;
                exit(EXIT_FAILURE);
            }
        }

        if(globalArgs.arg_analyzeCosmics){
            if(globalArgs.arg_currentGain) dataManager.AnalyzeCosmicData(globalArgs.arg_currentGain,globalArgs.arg_currentGain);
            else{
                std::cerr<<"ERROR:current Gain can not be set to 0. To continue analyse cosmic data, please set the current Gain properly by option '-G' !"<<std::endl;
                exit(EXIT_FAILURE);
            }
        }


        pedestalFile.Close();
        signalFile.Close();
        dataManager.SaveAllHistos(globalArgs.arg_analyzeLed,globalArgs.arg_analyzeCosmics,globalArgs.arg_convert,globalArgs.arg_findMuonsTracks);
    }

    else if(globalArgs.arg_readFromExistRootFile && !globalArgs.arg_readInSetupFile && !globalArgs.arg_findRatio){
        DataManager dataManager(uplinkNumbers,uplink,globalArgs.arg_pathToExistRootFile);

        if(globalArgs.arg_aimGain != 12) dataManager.SetAimGain(globalArgs.arg_aimGain);
        else dataManager.SetAimGain(12);
        printf("fill data from exit file\n");
        //dataManager.pedestals_write_Mean_RMS();

        //printf("before dataManager.draw()\n");
        dataManager.draw();



        if(globalArgs.arg_drawProjY){
            if(globalArgs.arg_ProjYinLog) dataManager.PlotProjectionY(true);
            else dataManager.PlotProjectionY(false);
        }

        if(globalArgs.arg_useAdjustFileSet){
            dataManager.ClearDacFromCfgFile();
            dataManager.SetDacFromAdjustFile();
            if(globalArgs.arg_pathToAdjustFile == " ") {
                std::cerr <<"ERROR: -a option is set, DACs are supposed to be read in from the adjustDAC files, but path to the Dir of these files is not set properly!\n";
                exit(EXIT_FAILURE);
            }
            dataManager.SetPathToDACFile(globalArgs.arg_pathToAdjustFile);
        }

        if(globalArgs.arg_useCfgFileSet){
            dataManager.SetDacFromCfgFile();
            dataManager.ClearDacFromAdjustFile();
            if(globalArgs.arg_pathToDACCfgFile == " ") {
                std::cerr <<"ERROR: -a option is set, DACs are supposed to be read in from the cfg files, but path to the Dir of these files is not set properly!\n";
                exit(EXIT_FAILURE);
            }
            dataManager.SetPathToDACFile(globalArgs.arg_pathToDACCfgFile);
        }

        if(globalArgs.arg_forceToChangeCfgFile){
            dataManager.SetFroceToChangeCfgFile();
            if(globalArgs.arg_pathToCfgFileToBeWritten == " ") {
                std::cerr <<"ERROR: -f option is set, but path to dir of cfg files to be modified is not set properly!\n";
                exit(EXIT_FAILURE);
            }
            dataManager.SetPathToFormerCfgFile(globalArgs.arg_pathToCfgFileToBeWritten);
        }

        if(globalArgs.arg_analyzeLed){
            if(globalArgs.arg_currentGain) dataManager.AnalyzeLedData(globalArgs.arg_currentGain);
            else{
                std::cerr<<"ERROR:current Gain can not be set to 0. To continue analyse led injection data, please set the current Gain properly by option '-G' !"<<std::endl;
                exit(EXIT_FAILURE);
            }
        }

        if(globalArgs.arg_analyzeCosmics){
            if(globalArgs.arg_currentGain) dataManager.AnalyzeCosmicData(globalArgs.arg_currentGain,globalArgs.arg_currentGain);
            else{
                std::cerr<<"ERROR:current Gain can not be set to 0. To continue analyse cosmic data, please set the current Gain properly by option '-G' !"<<std::endl;
                exit(EXIT_FAILURE);
            }
        }


    }

    // SetupFile + globalArgs.arg_convert + globalArgs.arg_findMuonsTracks
    else if(!globalArgs.arg_readFromExistRootFile && globalArgs.arg_readInSetupFile && !globalArgs.arg_findRatio){


        //////////////////Fill DATA PART/////////////////////////////////////////////////////////////////////////////////////////////////
        printf("begin to fill data\n");

        TFile pedestalFile(globalArgs.arg_pathToPedestal);
        TFile signalFile(globalArgs.arg_pathToSignal);

        gROOT->cd();
        TTree* pedestalTree = static_cast<TTree*>(pedestalFile.Get("ReadoutData"));
        TTree* signalTree = static_cast<TTree*>(signalFile.Get("ReadoutData"));

        RunDescription* runDescription = static_cast<RunDescription*>(pedestalTree->GetUserInfo()->First());
        runDescription->dump(std::cout);
        printf("------------------------finished dump runDescription()\n");

        std::stringstream settingsStream(runDescription->settings());
        Settings::instance()->loadFromStream(settingsStream);

        printf("------------------------finished loadFromStream()\n");

        DataManager dataManager(globalArgs.arg_pathToSetupFile, globalArgs.arg_convert, globalArgs.arg_findMuonsTracks);

        if(globalArgs.arg_aimGain != 12) dataManager.SetAimGain(globalArgs.arg_aimGain);
        else dataManager.SetAimGain(12);

        MergedEvent* event_pedestal = 0;
        pedestalTree->SetBranchAddress("events", &event_pedestal);
        long pedestalEntries = pedestalTree->GetEntries();
        std::cout << "taking event_pedestals"<< std::endl;
        std::cout << std::endl;

        for (long it = 1; (it < pedestalEntries); ++it) {
            pedestalTree->GetEntry(it);
            dataManager.fill_raw_pedestals(event_pedestal);
        }

        dataManager.find_working_channels();

        dataManager.pedestals_write_Mean_RMS();

        for (long it = 1; (it < pedestalEntries); ++it) {
        //for (long it = 1; (it < 1000); ++it) {
            pedestalTree->GetEntry(it);
            dataManager.FillPsPedestal(event_pedestal);
        }
	
        MergedEvent* event_signal = 0;
        signalTree->SetBranchAddress("events", &event_signal);
        long signalEntries = signalTree->GetEntries();
        std::cout << "taking event_signals, signalEntries="<<signalEntries<< std::endl;
        std::cout << std::endl;

        for (long it = 1; (it < signalEntries); ++it) {
        //for (long it = 1; (it < 2); ++it) {
            signalTree->GetEntry(it);
            dataManager.getEnergy(event_signal);
            if(globalArgs.arg_convert){
                 dataManager.convert(event_signal);
            }
            if(globalArgs.arg_findMuonsTracks){
                    dataManager.find_the_fired_strip_in_each_layer(event_signal);
                    dataManager.find_the_track(event_signal);
                }
            if (it%1000==0) std::cout<<"event="<<it<<"\n";
        }

        std::cout<<"OK"<<std::endl;

        printf("before dataManager.draw()\n");
        //dataManager.draw();

        if(globalArgs.arg_drawProjY){
            if(globalArgs.arg_ProjYinLog) dataManager.PlotProjectionY(true);
            else dataManager.PlotProjectionY(false);
        }

        if(globalArgs.arg_useAdjustFileSet){
            dataManager.ClearDacFromCfgFile();
            dataManager.SetDacFromAdjustFile();
            if(globalArgs.arg_pathToAdjustFile == " ") {
                std::cerr <<"ERROR: -a option is set, DACs are supposed to be read in from the adjustDAC files, but path to the Dir of these files is not set properly!\n";
                exit(EXIT_FAILURE);
            }
            dataManager.SetPathToDACFile(globalArgs.arg_pathToAdjustFile);
        }

        if(globalArgs.arg_useCfgFileSet){
            dataManager.SetDacFromCfgFile();
            dataManager.ClearDacFromAdjustFile();
            if(globalArgs.arg_pathToDACCfgFile == " ") {
                std::cerr <<"ERROR: -a option is set, DACs are supposed to be read in from the cfg files, but path to the Dir of these files is not set properly!\n";
                exit(EXIT_FAILURE);
            }
            dataManager.SetPathToDACFile(globalArgs.arg_pathToDACCfgFile);
        }

        if(globalArgs.arg_forceToChangeCfgFile){
            dataManager.SetFroceToChangeCfgFile();
            if(globalArgs.arg_pathToCfgFileToBeWritten == " ") {
                std::cerr <<"ERROR: -f option is set, but path to dir of cfg files to be modified is not set properly!\n";
                exit(EXIT_FAILURE);
            }
            dataManager.SetPathToFormerCfgFile(globalArgs.arg_pathToCfgFileToBeWritten);
        }

        if(globalArgs.arg_analyzeLed){
            if(globalArgs.arg_currentGain) {
                dataManager.AnalyzeLedData(globalArgs.arg_currentGain);
            }
            else{
                std::cerr<<"ERROR:current Gain can not be set to 0. To continue analyse led injection data, please set the current Gain properly by option '-G' !"<<std::endl;
                exit(EXIT_FAILURE);
            }
        }

        if(globalArgs.arg_analyzeCosmics){
            if(globalArgs.arg_currentGain) {
                dataManager.AnalyzeCosmicData(globalArgs.arg_currentGain,globalArgs.arg_currentGain);
            }
            else{
                std::cerr<<"ERROR:current Gain can not be set to 0. To continue analyse cosmic data, please set the current Gain properly by option '-G' !"<<std::endl;
                exit(EXIT_FAILURE);
            }
        }

        pedestalFile.Close();
        signalFile.Close();
        //dataManager.SaveAllHistos(globalArgs.arg_analyzeLed,globalArgs.arg_analyzeCosmics,globalArgs.arg_findRatio);
        dataManager.SaveAllHistos(globalArgs.arg_analyzeLed,globalArgs.arg_analyzeCosmics,globalArgs.arg_convert,globalArgs.arg_findMuonsTracks);
        std::cout<<"files closed"<<std::endl;
    }

    // RootFile && SetupFile
    else if(globalArgs.arg_readFromExistRootFile && globalArgs.arg_readInSetupFile && !globalArgs.arg_findRatio){
        DataManager dataManager(globalArgs.arg_pathToSetupFile,globalArgs.arg_pathToExistRootFile);

        if(globalArgs.arg_aimGain != 12) dataManager.SetAimGain(globalArgs.arg_aimGain);
        else dataManager.SetAimGain(12);
        printf("fill data from exit file\n");
        //dataManager.pedestals_write_Mean_RMS();

        //printf("before dataManager.draw()\n");
        dataManager.draw();

        if(globalArgs.arg_drawProjY){
            if(globalArgs.arg_ProjYinLog) dataManager.PlotProjectionY(true);
            else dataManager.PlotProjectionY(false);
        }

        if(globalArgs.arg_useAdjustFileSet){
            dataManager.ClearDacFromCfgFile();
            dataManager.SetDacFromAdjustFile();
            if(globalArgs.arg_pathToAdjustFile == " ") {
                std::cerr <<"ERROR: -a option is set, DACs are supposed to be read in from the adjustDAC files, but path to the Dir of these files is not set properly!\n";
                exit(EXIT_FAILURE);
            }
            dataManager.SetPathToDACFile(globalArgs.arg_pathToAdjustFile);
        }

        if(globalArgs.arg_useCfgFileSet){
            dataManager.SetDacFromCfgFile();
            dataManager.ClearDacFromAdjustFile();
            if(globalArgs.arg_pathToDACCfgFile == " ") {
                std::cerr <<"ERROR: -a option is set, DACs are supposed to be read in from the cfg files, but path to the Dir of these files is not set properly!\n";
                exit(EXIT_FAILURE);
            }
            dataManager.SetPathToDACFile(globalArgs.arg_pathToDACCfgFile);
        }

        if(globalArgs.arg_forceToChangeCfgFile){
            dataManager.SetFroceToChangeCfgFile();
            if(globalArgs.arg_pathToCfgFileToBeWritten == " ") {
                std::cerr <<"ERROR: -f option is set, but path to dir of cfg files to be modified is not set properly!\n";
                exit(EXIT_FAILURE);
            }
            dataManager.SetPathToFormerCfgFile(globalArgs.arg_pathToCfgFileToBeWritten);
        }

        if(globalArgs.arg_analyzeLed){
            if(globalArgs.arg_currentGain) dataManager.AnalyzeLedData(globalArgs.arg_currentGain);
            else{
                std::cerr<<"ERROR:current Gain can not be set to 0. To continue analyse led injection data, please set the current Gain properly by option '-G' !"<<std::endl;
                exit(EXIT_FAILURE);
            }
        }

        if(globalArgs.arg_analyzeCosmics){
            if(globalArgs.arg_currentGain) dataManager.AnalyzeCosmicData(globalArgs.arg_currentGain,globalArgs.arg_currentGain);
            else{
                std::cerr<<"ERROR:current Gain can not be set to 0. To continue analyse cosmic data, please set the current Gain properly by option '-G' !"<<std::endl;
                exit(EXIT_FAILURE);
            }
        }
    }

    // pedestal+LedP+LedN && SetupFile
    else if(globalArgs.arg_findRatio){
      
      TFile pedestalFile(globalArgs.arg_pathToPedestal);
      
      gROOT->cd();
      TTree* pedestalTree = static_cast<TTree*>(pedestalFile.Get("ReadoutData"));
      
      RunDescription* runDescription = static_cast<RunDescription*>(pedestalTree->GetUserInfo()->First());
      runDescription->dump(std::cout);
      printf("------------------------finished dump runDescription()\n");
      
      std::stringstream settingsStream(runDescription->settings());
      Settings::instance()->loadFromStream(settingsStream);
      
      printf("------------------------finished loadFromStream()\n");
      
      /////////////////////////////////////////////////////////////////////
      DataManager dataManager(globalArgs.arg_findRatio,globalArgs.arg_pathToSetupFile);
      
      MergedEvent* event_pedestal = 0;
      pedestalTree->SetBranchAddress("events", &event_pedestal);
      long pedestalEntries = pedestalTree->GetEntries();
      std::cout << "taking event_pedestals"<< std::endl;
      std::cout << std::endl;
      
      for (long it = 1; (it < pedestalEntries); ++it) {
        pedestalTree->GetEntry(it);
        dataManager.fill_raw_pedestals(event_pedestal);
    }

    dataManager.find_working_channels();
    dataManager.pedestals_write_Mean_RMS();

    for (long it = 1; (it < pedestalEntries); ++it) {
        //for (long it = 1; (it < 1000); ++it) {
        pedestalTree->GetEntry(it);
        dataManager.FillPsPedestal(event_pedestal);
    }


    TFile LedPFile(globalArgs.arg_pathToLedP,"UPDATE");
    TTree* LedPTree = static_cast<TTree*>(LedPFile.Get("ReadoutData"));
    //LedPFile->Delete("Led*;*");
    //file->mkdir("images");
    //file->cd("images");

    MergedEvent* event_LedP = 0;
    LedPTree->SetBranchAddress("events", &event_LedP);
    long LedPEntries = LedPTree->GetEntries();

    for (long it = 1; (it < LedPEntries); ++it) {
        LedPTree->GetEntry(it);
        dataManager.getCorrelationP(event_LedP);
        if (it%1000==0) std::cout<<"event="<<it<<"\n";
    }

    dataManager.findRatioLedP();

    TFile LedNFile(globalArgs.arg_pathToLedN,"UPDATE");
    TTree* LedNTree = static_cast<TTree*>(LedNFile.Get("ReadoutData"));
    //LedNFile->Delete("Led*");

    MergedEvent* event_LedN = 0;
    LedNTree->SetBranchAddress("events", &event_LedN);
    long LedNEntries = LedNTree->GetEntries();

    for (long it = 1; (it < LedNEntries); ++it) {
        LedNTree->GetEntry(it);
        dataManager.getCorrelationN(event_LedN);
        if (it%1000==0) std::cout<<"event="<<it<<"\n";
    }

    dataManager.findRatioLedN();

    if(globalArgs.arg_useAdjustFileSet){
        dataManager.ClearDacFromCfgFile();
        dataManager.SetDacFromAdjustFile();
        if(globalArgs.arg_pathToAdjustFile == " ") {
            std::cerr <<"ERROR: -a option is set, DACs are supposed to be read in from the adjustDAC files, but path to the Dir of these files is not set properly!\n";
            exit(EXIT_FAILURE);
        }
        dataManager.SetPathToDACFile(globalArgs.arg_pathToAdjustFile);
    }

    if(globalArgs.arg_useCfgFileSet){
        dataManager.SetDacFromCfgFile();
        dataManager.ClearDacFromAdjustFile();
        if(globalArgs.arg_pathToDACCfgFile == " ") {
            std::cerr <<"ERROR: -a option is set, DACs are supposed to be read in from the cfg files, but path to the Dir of these files is not set properly!\n";
            exit(EXIT_FAILURE);
        }
        dataManager.SetPathToDACFile(globalArgs.arg_pathToDACCfgFile);
    }

    if(globalArgs.arg_forceToChangeCfgFile){
        dataManager.SetFroceToChangeCfgFile();
        if(globalArgs.arg_pathToCfgFileToBeWritten == " ") {
            std::cerr <<"ERROR: -f option is set, but path to dir of cfg files to be modified is not set properly!\n";
            exit(EXIT_FAILURE);
        }
        dataManager.SetPathToFormerCfgFile(globalArgs.arg_pathToCfgFileToBeWritten);
    }

    dataManager.findRatio();

    pedestalFile.Close();
    LedPFile.Close();
    LedNFile.Close();
    //dataManager.SaveAllHistos(globalArgs.arg_analyzeLed,globalArgs.arg_analyzeCosmics,globalArgs.arg_findRatio);

    std::cout<<"files closed"<<std::endl;
      
    }

    app.Run();
    
    return 0;
}
