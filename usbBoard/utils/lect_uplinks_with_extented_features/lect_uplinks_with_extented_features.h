#ifndef DataManager_h
#define DataManager_h

#include <TMath.h>
#include <TGraph2D.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TF2.h>
#include <TH1.h>
#include <TVirtualFitter.h>
#include <TPolyLine3D.h>
#include <Math/Vector3D.h>
#include <vector>
#include <TF1.h>
#include <TH3.h>
#include <TGraphErrors.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>
#include <map>

//#define DUMP_DEBUG

class MergedEvent;
class TH1;
class TH2;
class TH3;
class TH2F;
class TFile;
class TTree;
class TProfile;

void line(double t, double *p, double &x, double &y, double &z);// define the parameteric line equation
double distance2(double x,double y,double z, double *p);// calculate distance line-point
void SumDistance2(int &, double *, double & sum, double * par, int );// function to be minimized 
Double_t fpeaks(Double_t *x, Double_t *par);
Double_t langaufun(Double_t *x, Double_t *par);
TF1 *langaufit(TH1D *his, Double_t *fitrange, Double_t *startvalues, Double_t *parlimitslo, Double_t *parlimitshi, Double_t *fitparams, Double_t *fiterrors, Double_t *ChiSqr, Int_t *NDF);

Double_t fconvo_func(Double_t *x, Double_t *par);

typedef std::map<int,int> SuperLayerMap; //map<positon,uplinkId>

enum SuperLayerPosition{
  P1_P = 0,
  P2_P = 1,
  P3_P = 2,
  P4_P = 3,
  P1_N = 4,
  P2_N = 5,
  P3_N = 6,
  P4_N = 7,
  P_N_DISTANCE = 4
  
};


class DataManager {
public:
  DataManager(int uplinksNumber,int* uplinkIdArray,std::string pathToReadInFile);
  DataManager(const std::vector<int>& uplinksToBeAnalyzed);
  DataManager(std::string pathToSetupFile, bool arg_convert, bool arg_findMuonsTracks);
  DataManager(std::string pathToSetupFile,std::string pathToReadInFile);
  DataManager(bool arg_findRatio, std::string pathToSetupFile);
  ~DataManager();
  void fill_raw_pedestals(MergedEvent* event);
  void pedestals_write_Mean_RMS();
  void find_working_channels();
  void draw();
  void getEnergy(MergedEvent* event);
  void getCorrelationP(MergedEvent* event);
  void getCorrelationN(MergedEvent* event);
  void findRatioLedP();
  void findRatioLedN();
  void findRatio();
  void ProjectionYWithMean(int uplinkId,bool forceInLog = false);
  void ADCPerPhoton(int uplinkId,Float_t gain,bool plot=false,bool forMIP = false);
  void MPVinADC(int uplinkId,Int_t rebin,bool plot=false);
  void MPVinPE(int uplinkId); // It assumes that MPV and ADC values in ADC has already counted.
  void PlotAnalysisResult_1uplink(int uplinkId, std::string pedestalFilePath ="", std::string signalFilePath="");
  void PlotAnalysisResult(std::string pedestalFilePath = "", std::string signalFilePath ="");
  void WriteResults();
  void PlotProjectionY(bool forceInlog);
  void AnalyzeLedData(Float_t gain);
  void AnalyzeCosmicData(Float_t gain,Int_t rebin_mpv);
  void PlotAnalysisResult_LED();
  void PlotAnalysisResult_LED_InStrip();
  void PlotAnalysisResult_InStrip(std::string pedestalFilePath = "", std::string signalFilePath ="");
  void convert(MergedEvent* event);
  void find_the_fired_strip_in_each_layer(MergedEvent* event);
  void find_the_track(MergedEvent* event);
  void Fill2LayerCorrelation(MergedEvent* event);
  void Fill_tree_for_2_super_layer_with_cosmics(MergedEvent* event);
  void Plot2LayerCorr();
  void SaveAllHistos(bool arg_analyzeLed, bool arg_analyzeCosmics, bool arg_convert, bool arg_findMuonsTracks); // write all the generated histos and graphs in to a root file
  //void SaveAllHistos(bool arg_analyzeLed, bool arg_analyzeCosmics, bool arg_findRatio); // write all the generated histos and graphs into a root file
  void FillPsPedestal(MergedEvent* event);
  
  TH2* GetPS_pedestals(int uplinkId);
  
  //void FillAllByExistFiles(std::string analyzeFilePath = "../data/lect_uplinks_result.root");
  
  void SetPathToDACFile(std::string pathToDACFile){m_pathToDACFile = pathToDACFile;}
  std::string& GetPathToDACFile() { return m_pathToDACFile;}
  
  void SetPathToFormerCfgFile(std::string pathToFormerCfgFile){m_pathToFormerCfgFile = pathToFormerCfgFile;}
  std::string& GetPathToFormerCfgFile() { return m_pathToFormerCfgFile ;}
  
  void SetDacFromCfgFile(){m_dacFromCfgFile = true;}
  void ClearDacFromCfgFile(){m_dacFromCfgFile = false;}
  bool IsDacFromCfgFile(){ return m_dacFromCfgFile;}
  
  void SetDacFromAdjustFile(){m_dacFromAdjustFile = true;}
  void ClearDacFromAdjustFile(){m_dacFromAdjustFile = false;}
  bool IsDacFromAdjustFile(){ return m_dacFromAdjustFile;}
  
  void SetFroceToChangeCfgFile(){m_forceToChangeCfgFile = true;}
  void ClearFroceToChangeCfgFile(){m_forceToChangeCfgFile = false;}
  bool IsForceToChangeCfgFile(){return m_forceToChangeCfgFile;}
  
  void SetAimGain(int setGain){m_aimGain = setGain;}
  int GetAimGain(){return m_aimGain;}
  
  const static unsigned short maxAdcValue = 4096;
  //const static unsigned short maxNumberPhotons = 50000;
  const static unsigned short HEAD_OF_EFFECT_CHANNELS = 0;   // count as channel0,1,2....
  const static unsigned short NUMBER_OF_EFFECT_CHANNELS = 64;
  const static unsigned short ADC_PER_PHOTON_UPPER_THRESHOLD = 25;
  const static unsigned short MEAN_IN_PHOTON_UNDER_THRESHOLD =0.5;
  const static unsigned short DAC_NOMINAL = 128;
  const static unsigned short STRIP_BASE_TRIGGERLAYER_PART1 = 1;
  const static unsigned short STRIP_BASE_TRIGGERLAYER_PART2 = 1+NUMBER_OF_EFFECT_CHANNELS;
  const static unsigned short NUMBER_OF_STRIPS_TRIGGERLAYER = 2*NUMBER_OF_EFFECT_CHANNELS;
  const static unsigned short STRIP_BASE_SIGNALLAYER = 1+NUMBER_OF_STRIPS_TRIGGERLAYER;
  const static unsigned short COSMIC_THRESHOLD = 30 ;  // threshold to be treated as an cosmic event
  const static unsigned short VATA64_READOUT_CHANNELS = 64;
  const static float threshold_P = 2;
  const static float threshold_N = 2;
  const static unsigned short NUMBER_OF_GROUP=4;//number of row per SiPM board
  const static unsigned short CHANNELS_PER_GROUP=16;
  const static unsigned short STRIPS_PER_GROUP=12;
  const static unsigned short STRIPS_PER_LAYER=24;
  const static unsigned short STRIPS_PER_SUPERLAYER = 192;
  const static unsigned short STRIPS_PER_MODULE = 48;
  const static unsigned short BOARDS_PER_SUPERLAYER = 8;
  const static unsigned short NUMBER_OF_SUPERLAYER = 2;
  const static unsigned short NUMBER_OF_LAYER_PER_SUPERLAYER = 8;
  //const static unsigned short NUMBER_OF_LAYER = NUMBER_OF_SUPERLAYER*NUMBER_OF_LAYER_PER_SUPERLAYER;
  const static unsigned short NUMBER_OF_LAYER = 16;
  const static unsigned short NUMBER_OF_MAX_POINTS = 15 ;
  const static unsigned short NUMBER_OF_POSITION_PER_SUPERLAYER = 8;
  const static unsigned short NUMBER_OF_POSITIONS = 16;
  const static float overvoltage_ideal_MAPD = 1.8; // overvoltage_ideal_MAPD = 1 to be verified
  const static float ratio_ideal = 5; // ratio_ideal = 10
  const static float X_PITCH = 15.33; //unit= millimeter
  const static float Y_PITCH = 15.33; //unit= millimeter
  const static float Z_PITCH = 11.25; //unit= millimeter
  
private:
  unsigned int uplinkIndex(int uplinkId);
  int coordinate_layer[NUMBER_OF_LAYER]; // coordinate of the maximum (unit is strip width)
  float X[NUMBER_OF_LAYER]; // coordinate X of the track at layer i (unit is mm)
  float Y[NUMBER_OF_LAYER]; // coordinate Y of the track at layer i (unit is mm)
  float Z[NUMBER_OF_LAYER]; // coordinate Z of the track at layer i (unit is mm)
  float X_max[NUMBER_OF_MAX_POINTS]; // coordinate X of the max (unit is mm)
  float Y_max[NUMBER_OF_MAX_POINTS]; // coordinate Y of the max (unit is mm)
  float Z_max[NUMBER_OF_MAX_POINTS]; // coordinate Z of the max (unit is mm)
  int strip_max[NUMBER_OF_LAYER];
  float E_P_max[NUMBER_OF_LAYER];
  float E_N_max[NUMBER_OF_LAYER];
  int strip_muon[NUMBER_OF_LAYER]; // strip which is crossed by the track
  float E_P_muon[NUMBER_OF_LAYER]; // muon signal in layer i (P side)
  float E_N_muon[NUMBER_OF_LAYER]; // muon signal in layer i (N side)
  float theta, phi;
  float E_P[NUMBER_OF_LAYER*STRIPS_PER_LAYER];
  float E_N[NUMBER_OF_LAYER*STRIPS_PER_LAYER];
  float x0,y0,Ax,Ay;
  float PIN_P[NUMBER_OF_LAYER];
  float PIN_N[NUMBER_OF_LAYER];
  float ratioLedfitP[NUMBER_OF_LAYER*STRIPS_PER_LAYER]; // ratio MPPC/MAPD from a linear fit of the correlation plot, measured whith light injection side P
  float ratioLedfitN[NUMBER_OF_LAYER*STRIPS_PER_LAYER]; // ratio MPPC/MAPD from a linear fit of the correlation plot, measured whith light injection side P
  float ratioLedP[NUMBER_OF_LAYER*STRIPS_PER_LAYER]; // ratio MPPC/MAPD, measured whith light injection side P
  float ratioLedN[NUMBER_OF_LAYER*STRIPS_PER_LAYER]; // ratio MPPC/MAPD, measured whith light injection side P
  // create a tree
  TTree *tree1;
  
  //PEBS09_TestbeamSetup* m_setup;
  std::vector<int> m_uplinkId;
  std::vector<unsigned short *> m_state;

  std::vector<float *> m_pedestal_Mean;
  std::vector<float *> m_pedestal_RMS;
  std::vector<TH2*> m_raw_pedestals;
  std::vector<TH2*> m_adcHistogram;
  std::vector<TH2*> m_PS_adcHistogram;
  std::vector<TH2*> m_uplink_correlation;
  std::vector<TH2*> m_LedP_uplink_correlation;
  std::vector<TH2*> m_LedN_uplink_correlation;
  std::vector<TGraphErrors*> m_adcPerPhoton;
  std::vector<TGraphErrors*> m_mpvInAdc;
  std::vector<TGraphErrors*> m_lSigma;
  std::vector<TGraphErrors*> m_gSigma;
  std::vector<TGraphErrors*> m_resultCorr;
  std::vector<TH1F*> m_area;
  std::vector<TGraphErrors*> m_meanInPhoton;
  std::vector<TH2*> m_PS_pedestals; // pedestal which substracted by the meanvalue of m_raw_jpedestals.
  
  TH3I* m_TH3_spectra_P[NUMBER_OF_LAYER];
  TH3I* m_TH3_spectra_N[NUMBER_OF_LAYER];
  TH3I* m_2layerCorrHisto1;
  TH3I* m_2layerCorrHisto2;
  TH2*  m_signalLayerSide1_strip;
  TH2*  m_signalLayerSide2_strip;
  TH2*  m_signalLayerSide1_chan;
  TH2*  m_signalLayerSide2_chan;
  TH2*  m_s1Histo_strip;
  TH2*  m_s2Histo_strip;
  TH2*  m_s1Histo_chan;
  TH2*  m_s2Histo_chan;

  std::string m_pathToDACFile;  // to save the path to DAC source file, either from chip config file or from adjustDAC file.
  bool m_dacFromCfgFile;
  bool m_dacFromAdjustFile;
  std::string m_pathToFormerCfgFile; // only the path to the folder, the file name is generated automatically .
  bool m_forceToChangeCfgFile;
  
  int m_aimGain;
  
  SuperLayerMap m_superLayerMap[2];
  bool m_readInSetupFile;
  std::vector<int> m_corrUplinkPairs;
  std::map<int,TH2*> m_strip_correlation; //<stripIndex,correlationHisto>
  std::map<int,TH2*> m_LedP_strip_correlation; //<stripIndex,correlationHisto>
  std::map<int,TH2*> m_LedN_strip_correlation; //<stripIndex,correlationHisto>
  std::map<int,int> m_map_key_strip_value_N_uplink; //<stripIndex,uplinkID>
  std::map<int,int> m_map_key_strip_value_N_ch; //<stripIndex,ch>
  std::map<int,int> m_map_key_strip_value_P_uplink; //<stripIndex,uplinkID>
  std::map<int,int> m_map_key_strip_value_P_ch; //<stripIndex,ch>
  std::map<int,int> m_map_key_uplinkID_value_N_or_P; //<uplinkID,0 for N and 1 for P>
  std::map<int,double> m_map_key_strip_value_N_MPVadc; //<stripIndex,MPV in ADC for N>
  std::map<int,double> m_map_key_strip_value_P_MPVadc;//<stripIndex,MPV in ADC for P>
  std::map<int,double> m_map_key_strip_value_N_ADCPerPE; //<stripIndex,Gain per PE (for N)>
  std::map<int,double> m_map_key_strip_value_N_MPVinPE; //<stripIndex,MPV in PE (for N)>
  std::map<int,double> m_map_key_strip_value_N_area; //<stripIndex,area for N>
  std::map<int,double> m_map_key_strip_value_P_area; //<stripIndex,area for P>
  std::map<int,double> m_map_key_strip_value_N_chi2; //<stripIndex,chi2/n for N>
  std::map<int,double> m_map_key_strip_value_P_chi2; //<stripIndex,chi2/n for P>
  std::map<std::pair<int,int>,int> m_key_N_uplink_ch_value_strip; //<pair(uplinkID,ch) for N,strip>
  std::map<std::pair<int,int>,int> m_key_P_uplink_ch_value_strip; //<pair(uplinkID,ch) for P,strip>
};

#endif
