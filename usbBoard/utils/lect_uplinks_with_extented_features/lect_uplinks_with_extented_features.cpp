#include "lect_uplinks_with_extented_features.h"
#include <Settings.h>

#include <assert.h>
#include <iostream>

#include <MergedEvent.h>
#include <UsbBoardConfiguration.h>

#include <TH1.h>
#include <TH2.h>
#include <TH2F.h>
#include <TF1.h>
#include <TH3.h>
#include <TProfile.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TMath.h>
#include <TObjArray.h>
#include <TLegend.h>
#include <TGraph.h>

#include <TRandom.h>
#include <TSpectrum.h>
#include <TVirtualFitter.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TVector3.h>

//#include "PEBS09_TestbeamSetup.h"

Int_t npeaks;
TH1 *background = NULL;

void DataManager::PlotProjectionY(bool forceInLog){
    printf("m_uplinkId.size() = %d\n",m_uplinkId.size());
    for(unsigned int i=0; i<m_uplinkId.size();i++)  ProjectionYWithMean(m_uplinkId.at(i),forceInLog);
}

void DataManager::AnalyzeLedData(Float_t gain){
    for(unsigned int i=0; i<m_uplinkId.size();i++) {
        ADCPerPhoton(m_uplinkId.at(i),gain,false,false);
    }
    PlotAnalysisResult_LED();
    PlotAnalysisResult_LED_InStrip();
}

void DataManager::AnalyzeCosmicData(Float_t gain,Int_t rebin_mpv){
    for(unsigned int i=0; i<m_uplinkId.size();i++) {
        MPVinADC(m_uplinkId.at(i),rebin_mpv);
        ADCPerPhoton(m_uplinkId.at(i),gain,false,true);
        MPVinPE(m_uplinkId.at(i));
    }

    PlotAnalysisResult();
    PlotAnalysisResult_InStrip();
    WriteResults();
}

void DataManager::convert(MergedEvent* event)
{
    const int CHANNEL_MIN_LINE[4]={0,16,32,48};
    const int CHANNEL_MAX_LINE[4]={11,27,43,59};

    //begin searching the maximum within each rows of 12 channels
    for (int i=0;i<NUMBER_OF_SUPERLAYER;i++){
        for (int j=0;j<NUMBER_OF_POSITION_PER_SUPERLAYER/2;j++){
            for (int k=0;k<NUMBER_OF_GROUP;k++){
                // get energy for each SiPM
                int uplinkId_P=m_superLayerMap[i][j]; //uplinkID
                int uplinkId_N=m_superLayerMap[i][j+P_N_DISTANCE]; //uplinkID
                const UplinkData& uplinkData_P=event->uplinkData(uplinkId_P);
                const UplinkData& uplinkData_N=event->uplinkData(uplinkId_N);

                for (int chan_P=CHANNEL_MIN_LINE[k];chan_P<CHANNEL_MAX_LINE[k]+1;chan_P++) {
                    float EP, EN;
                    int chan_N = CHANNEL_MAX_LINE[k] - (chan_P - CHANNEL_MIN_LINE[k]);
                    EP = uplinkData_P.adcValue(chan_P)-m_pedestal_Mean.at(uplinkIndex(uplinkId_P))[chan_P];
                    EN = uplinkData_N.adcValue(chan_N)-m_pedestal_Mean.at(uplinkIndex(uplinkId_N))[chan_N];

                    int strip = k*4*12 + j*12 + (chan_P-CHANNEL_MIN_LINE[k]) + i*192;
                    E_P[strip] = EP;
                    E_N[strip] = EN;
                }

                // get energy for each PIN diode
                int strip = k*4*12 + j*12 + (CHANNEL_MAX_LINE[k]-CHANNEL_MIN_LINE[k]) + i*192;
                int layer = floor(strip/STRIPS_PER_LAYER);
                if ((j==0) ||(j==2)){
                    int uplinkId_P=m_superLayerMap[i][j]; //uplinkID
                    int uplinkId_N=m_superLayerMap[i][j+P_N_DISTANCE+1]; //uplinkID
                    const UplinkData& uplinkData_P=event->uplinkData(uplinkId_P);
                    const UplinkData& uplinkData_N=event->uplinkData(uplinkId_N);
                    PIN_P[layer] = uplinkData_P.adcValue(CHANNEL_MAX_LINE[k]+1)-m_pedestal_Mean.at(uplinkIndex(uplinkId_P))[CHANNEL_MAX_LINE[k]+1];
                    PIN_N[layer] = uplinkData_N.adcValue(CHANNEL_MAX_LINE[k]+1)-m_pedestal_Mean.at(uplinkIndex(uplinkId_N))[CHANNEL_MAX_LINE[k]+1];
                    std::cout<<layer<<"\t"<<PIN_P[layer]<<"\t"<<PIN_N[layer]<<"\n";
                }
            }
        }
    }
    tree1->Fill();
    //finish to fill the Ttree
}


void DataManager::find_the_fired_strip_in_each_layer(MergedEvent* event)
{
    const int CHANNEL_MIN_LINE[4]={0,16,32,48};
    const int CHANNEL_MAX_LINE[4]={11,27,43,59};
    float Emax[NUMBER_OF_SUPERLAYER][NUMBER_OF_POSITION_PER_SUPERLAYER][NUMBER_OF_GROUP];
    int stripmax[NUMBER_OF_SUPERLAYER][NUMBER_OF_POSITION_PER_SUPERLAYER][NUMBER_OF_GROUP];

    //begin searching the maximum within each rows of 12 channels
    for (int i=0;i<NUMBER_OF_SUPERLAYER;i++){
        for (int j=0;j<NUMBER_OF_POSITION_PER_SUPERLAYER/2;j++){

            //begin initialitation
            for (int k=0;k<NUMBER_OF_GROUP;k++){
                Emax[i][j][k] = 0;
                stripmax[i][j][k] = NUMBER_OF_LAYER*STRIPS_PER_LAYER;
                Emax[i][j+P_N_DISTANCE][k] = 0;
                stripmax[i][j+P_N_DISTANCE][k] = NUMBER_OF_LAYER*STRIPS_PER_LAYER;
                //std::cout<<"Emax["<<i<<"]["<<j<<"]["<<k<<"]="<<Emax[i][j][k]<<"\t stripmax["<<i<<"]["<<j<<"]["<<k<<"]="<<stripmax[i][j][k]<<"\n";
                //std::cout<<"Emax["<<i<<"]["<<j+P_N_DISTANCE<<"]["<<k<<"]="<<Emax[i][j+P_N_DISTANCE][k]<<"\t stripmax["<<i<<"]["<<j+P_N_DISTANCE<<"]["<<k<<"]="<<stripmax[i][j+P_N_DISTANCE][k]<<"\n";
            }// finish initialization

            for (int k=0;k<NUMBER_OF_GROUP;k++){
                // find highest signal on superlayer i, position j and j+P_N_DISTANCE, line k
                int uplinkId_P=m_superLayerMap[i][j]; //uplinkID
                int uplinkId_N=m_superLayerMap[i][j+P_N_DISTANCE]; //uplinkID
                const UplinkData& uplinkData_P=event->uplinkData(uplinkId_P);
                const UplinkData& uplinkData_N=event->uplinkData(uplinkId_N);

                for (int chan_P=CHANNEL_MIN_LINE[k];chan_P<CHANNEL_MAX_LINE[k]+1;chan_P++) {
                    float EP, EN;
                    int chan_N = CHANNEL_MAX_LINE[k] - (chan_P - CHANNEL_MIN_LINE[k]);
                    EP = uplinkData_P.adcValue(chan_P)-m_pedestal_Mean.at(uplinkIndex(uplinkId_P))[chan_P];
                    EN = uplinkData_N.adcValue(chan_N)-m_pedestal_Mean.at(uplinkIndex(uplinkId_N))[chan_N];

                    if ( (EP>threshold_P*m_pedestal_RMS.at(uplinkIndex(uplinkId_P))[chan_P]) && (EP > Emax[i][j][k]) ) {
                        Emax[i][j][k] = EP;
                        stripmax[i][j][k] = k*4*12 + j*12 + (chan_P-CHANNEL_MIN_LINE[k]) + i*192;
                        //std::cout<<"stripmax["<<i<<"]["<<j<<"]["<<k<<"]="<<stripmax[i][j][k]<<"\t Emax["<<i<<"]["<<j<<"]["<<k<<"]="<<Emax[i][j][k]<<"\n";
                    }

                    if ( (EN>threshold_N*m_pedestal_RMS.at(uplinkIndex(uplinkId_N))[chan_N]) && (EN > Emax[i][j+P_N_DISTANCE][k]) ) {
                        Emax[i][j+P_N_DISTANCE][k] = EN;
                        stripmax[i][j+P_N_DISTANCE][k] = k*4*12 + j*12 + (chan_P-CHANNEL_MIN_LINE[k]) + i*192;
                        //std::cout<<"stripmax["<<i<<"]["<<j+P_N_DISTANCE<<"]["<<k<<"]="<<stripmax[i][j+P_N_DISTANCE][k]<<"\t Emax["<<i<<"]["<<j+P_N_DISTANCE<<"]["<<k<<"]="<<Emax[i][j+P_N_DISTANCE][k]<<"\n";
                    }

                    int strip = k*4*12 + j*12 + (chan_P-CHANNEL_MIN_LINE[k]) + i*192;
                    E_P[strip] = EP;
                    E_N[strip] = EN;
                    //std::cout<<"uplinkId_P="<<uplinkId_P<<"\t uplinkId_N="<<uplinkId_N<<"\t E_P["<<strip<<"]="<<E_P[strip]<<"\t E_N["<<strip<<"]="<<E_N[strip]<<"\n";
                }

                if (stripmax[i][j][k]!=stripmax[i][j+P_N_DISTANCE][k]) {
                    stripmax[i][j][k]=NUMBER_OF_LAYER*STRIPS_PER_LAYER;
                    stripmax[i][j+P_N_DISTANCE][k]=NUMBER_OF_LAYER*STRIPS_PER_LAYER;
                }

                // get energy for each PIN diode
                int strip = k*4*12 + j*12 + (CHANNEL_MAX_LINE[k]-CHANNEL_MIN_LINE[k]) + i*192;
                int layer = floor(strip/STRIPS_PER_LAYER);
                if ((j==0) ||(j==2)){
                    int uplinkId_P=m_superLayerMap[i][j]; //uplinkID
                    int uplinkId_N=m_superLayerMap[i][j+P_N_DISTANCE+1]; //uplinkID
                    const UplinkData& uplinkData_P=event->uplinkData(uplinkId_P);
                    const UplinkData& uplinkData_N=event->uplinkData(uplinkId_N);
                    PIN_P[layer] = uplinkData_P.adcValue(CHANNEL_MAX_LINE[k]+1)-m_pedestal_Mean.at(uplinkIndex(uplinkId_P))[CHANNEL_MAX_LINE[k]+1];
                    PIN_N[layer] = uplinkData_N.adcValue(CHANNEL_MAX_LINE[k]+1)-m_pedestal_Mean.at(uplinkIndex(uplinkId_N))[CHANNEL_MAX_LINE[k]+1];
                }

                ////////////////////////////////////////////////////
                /*
	if ((stripmax[i][j][k]==stripmax[i][j+P_N_DISTANCE][k]) && (stripmax[i][j][k]!=NUMBER_OF_LAYER*STRIPS_PER_LAYER)) {
	  std::cout<<"k="<<k<<"\t j="<<j;
	  std::cout<<"\t stripmax["<<i<<"]["<<j<<"]["<<k<<"]="<<stripmax[i][j][k]<<"\t stripmax["<<i<<"]["<<j+P_N_DISTANCE<<"]["<<k<<"]="<<stripmax[i][j+P_N_DISTANCE][k];
	  std::cout<<"\t Emax["<<i<<"]["<<j<<"]["<<k<<"]="<<Emax[i][j][k]<<"\t Emax["<<i<<"]["<<j+P_N_DISTANCE<<"]["<<k<<"]="<<Emax[i][j+P_N_DISTANCE][k]<<"\n";
	}
	*/
            }
        }
    }
    //finish searching the maximum within each rows of 12 channels


    // begin finding the maximum for each layer

    // initialization
    for (unsigned short int layer=0;layer<NUMBER_OF_LAYER;layer++) {
        E_P_max[layer] = 0;
        E_N_max[layer] = 0;
        strip_max[layer] = NUMBER_OF_LAYER*STRIPS_PER_LAYER;
        coordinate_layer[layer] = STRIPS_PER_LAYER;
    }

    for (unsigned short int i=0;i<NUMBER_OF_SUPERLAYER;i++) {
        for (unsigned short int j=0;j<NUMBER_OF_POSITION_PER_SUPERLAYER/2;j++){
            if (j%2 == 0) {
                for (unsigned short int k=0;k<NUMBER_OF_GROUP;k++){
                    if ( (stripmax[i][j][k]!=NUMBER_OF_LAYER*STRIPS_PER_LAYER) || (stripmax[i][j+1][k]!=NUMBER_OF_LAYER*STRIPS_PER_LAYER) ) {
                        unsigned short int layer;
                        if (stripmax[i][j][k]!=NUMBER_OF_LAYER*STRIPS_PER_LAYER) layer = floor(stripmax[i][j][k]/STRIPS_PER_LAYER);
                        else if (stripmax[i][j+1][k]!=NUMBER_OF_LAYER*STRIPS_PER_LAYER) layer = floor(stripmax[i][j+1][k]/STRIPS_PER_LAYER);
                        //std::cout<<"layer="<<layer<<"\n";
                        if ( (stripmax[i][j+1][k]!=NUMBER_OF_LAYER*STRIPS_PER_LAYER)&&(Emax[i][j][k]<=Emax[i][j+1][k]) && (Emax[i][j+P_N_DISTANCE][k]<=Emax[i][j+P_N_DISTANCE+1][k]) ) {
                            //std::cout<<"(Emax[i][j][k]<Emax[i][j+1][k]) && (Emax[i][j+P_N_DISTANCE][k]<Emax[i][j+P_N_DISTANCE+1][k]) \n";
                            E_P_max[layer] = Emax[i][j+1][k];
                            E_N_max[layer] = Emax[i][j+P_N_DISTANCE+1][k];
                            strip_max[layer] = stripmax[i][j+1][k];
                            if (j<2) coordinate_layer[layer] = strip_max[layer] - 48*k - i*192;
                            else coordinate_layer[layer] = strip_max[layer] - 24 - 48*k - i*192;
                            /*
	      if (coordinate_layer[layer]>24) {
		std::cout<<"strip_max["<<layer<<"]="<<strip_max[layer]<<"\t i="<<i<<"\t j="<<j<<"\t k="<<k<<"\t coordinate_layer["<<layer<<"]="<<coordinate_layer[layer]<<"\n";
		getchar();}
	      //std::cout<<"\t E_P_max["<<layer<<"]="<<E_P_max[layer]<<"\t E_N_max["<<layer<<"]="<<E_N_max[layer]<<"\n\n";
	      */
                        }
                        if ( (stripmax[i][j][k]!=NUMBER_OF_LAYER*STRIPS_PER_LAYER)&&(Emax[i][j][k]>Emax[i][j+1][k]) && (Emax[i][j+P_N_DISTANCE][k]>Emax[i][j+P_N_DISTANCE+1][k]) ) {
                            //std::cout<<"(Emax[i][j][k]>Emax[i][j+1][k]) && (Emax[i][j+P_N_DISTANCE][k]>Emax[i][j+P_N_DISTANCE+1][k]) \n";
                            E_P_max[layer] = Emax[i][j][k];
                            E_N_max[layer] = Emax[i][j+P_N_DISTANCE][k];
                            strip_max[layer] = stripmax[i][j][k];
                            if (j<2) coordinate_layer[layer] = strip_max[layer] - 48*k - i*192;
                            else coordinate_layer[layer] = strip_max[layer] - 24 - 48*k - i*192;
                            /*
	      if (coordinate_layer[layer]>24) {
		std::cout<<"strip_max["<<layer<<"]="<<strip_max[layer]<<"\t i="<<i<<"\t j="<<j<<"\t k="<<k<<"\t coordinate_layer["<<layer<<"]="<<coordinate_layer[layer]<<"\n";
		getchar();}
	      //std::cout<<"\t E_P_max["<<layer<<"]="<<E_P_max[layer]<<"\t E_N_max["<<layer<<"]="<<E_N_max[layer]<<"\n\n";
	      */
                        }
                    }
                }
            }
        }
    }
    // finish finding the maximum for each layer
}


// define the parameteric line equation
void line(double t, double *p, double &x, double &y, double &z) {
    // a parameteric line is define from 6 parameters but 4 are independent
    // x0,y0,z0,z1,y1,z1 which are the coordinates of two points on the line
    // can choose z0 = 0 if line not parallel to x-y plane and z1 = 1;
    x = p[0] + p[1]*t;
    y = p[2] + p[3]*t;
    z = t;
}

// calculate distance line-point
double distance2(double x,double y,double z, double *p) {
    // distance line point is D= | (xp-x0) cross  ux |
    // where ux is direction of line and x0 is a point in the line (like t = 0)
    TVector3 xp(x,y,z);
    TVector3 x0(p[0], p[2], 0. );
    TVector3 x1(p[0] + p[1], p[2] + p[3], 1. );
    TVector3 u = (x1-x0).Unit();
    double d2 = ((xp-x0).Cross(u)) .Mag2();
    return d2;
}
bool first = true;


// function to be minimized
void SumDistance2(int &, double *, double & sum, double * par, int ) {
    // the TGraph must be a global variable
    TGraph2D * gr = dynamic_cast<TGraph2D*>( (TVirtualFitter::GetFitter())->GetObjectFit() );
    assert(gr != 0);
    double * x = gr->GetX();
    double * y = gr->GetY();
    double * z = gr->GetZ();
    int npoints = gr->GetN();
    sum = 0;
    for (int i  = 0; i < npoints; ++i) {
        double d = distance2(x[i],y[i],z[i],par);
        sum += d;
#ifdef DEBUG
        if (first) std::cout << "point " << i << "\t"
                << x[i] << "\t"
                << y[i] << "\t"
                << z[i] << "\t"
                << std::sqrt(d) << std::endl;
#endif
    }
    if (first)
        std::cout << "Total sum2 = " << sum << std::endl;
    first = false;
}

void DataManager::find_the_track(MergedEvent* event)
{
    // create the coordonates of the points (maximum 15) which give the track and fill a TGraph2D()
    TGraph2D * gr = new TGraph2D();
    TGraph * grx = new TGraph();
    TGraph * gry = new TGraph();
    unsigned short int point=0;
    for (unsigned short int i=0;i<NUMBER_OF_MAX_POINTS;i++){
        //std::cout<<"i="<<i<<"\n";
        if ((coordinate_layer[i]!=24)&&(coordinate_layer[i+1]!=24)) {
            if (i%2==0) {
                X_max[i]=(float)(coordinate_layer[i]+0.5)*X_PITCH;
                Y_max[i]=(float)(coordinate_layer[i+1]+0.5)*Y_PITCH;
                Z_max[i]=0.5*(i+i+1)*Z_PITCH;
            }
            else {
                X_max[i]=(float)(coordinate_layer[i+1]+0.5)*X_PITCH;
                Y_max[i]=(float)(coordinate_layer[i]+0.5)*Y_PITCH;
                Z_max[i]=0.5*(i+i+1)*Z_PITCH;
            }
            gr->SetPoint(point,X_max[i],Y_max[i],Z_max[i]);
            grx->SetPoint(point,Z_max[i],X_max[i]);
            gry->SetPoint(point,Z_max[i],Y_max[i]);
            //std::cout<<"point="<<point<<"\t layers = "<<i<<" and "<<i+1<<"\t coordinate_layer["<<i<<"]="<<coordinate_layer[i]<<"\t coordinate_layer["<<i+1<<"]="<<coordinate_layer[i+1]<<"\n";
            if (X_max[i]>370) {std::cout<<"X_max["<<i<<"]="<<X_max[i]<<"\n";getchar();}
            if (Y_max[i]>370) {std::cout<<"Y_max["<<i<<"]="<<Y_max[i]<<"\n";getchar();}
            point=point+1;
        }
        else {
            X_max[i]=-1;Y_max[i]=-1;Z_max[i]=-1;
        }
    }

    //std::cout<<"number of points="<<point<<"\n";

    if (point>4) {

        // fit the projections x=x(z) and y=y(z)
        TF1 * fx = new TF1("fx","[0]+x*[1]",0,90);
        TF1 * fy = new TF1("fy","[0]+x*[1]",0,90);
        grx->Fit("fx","QNO");
        gry->Fit("fy","QNO");
        /*
    TCanvas * cx = new TCanvas("cx","cx",200,10,700,500);
    cx->SetGridx();
    cx->SetGridy();
    TH2F * hx = new TH2F("hx","hx",2,0,90,2,0,400); // axis range
    hx->SetStats(kFALSE); // no statistics
    hx->Draw();
    grx->SetMarkerSize(2.);
    grx->SetMarkerStyle(24);
    grx->Draw("PSAME");
    grx->Fit("fx","SAME");
    
    TCanvas * cy = new TCanvas("cy","cy",200,10,700,500);
    cy->SetGridx();
    cy->SetGridy();
    TH2F * hy = new TH2F("hy","hy",2,0,90,2,0,400); // axis range
    hy->SetStats(kFALSE); // no statistics
    hy->Draw();
    gry->SetMarkerSize(2.);
    gry->SetMarkerStyle(24);
    gry->Draw("PSAME");
    gry->Fit("fy","SAME");
    */    

        /*
    // fit the graph with a 3Dline 
    TVirtualFitter *min = TVirtualFitter::Fitter(0,4);
    min->SetObjectFit(gr);
    min->SetFCN(SumDistance2);
    
    Double_t arglist[10];
    arglist[0] = 3;
    min->ExecuteCommand("SET PRINT",arglist,1);
    
    double pStart[4] = {fx->GetParameter(0),fx->GetParameter(1),fy->GetParameter(0),fy->GetParameter(1)};
    min->SetParameter(0,"x0",pStart[0],0.1,0,0);
    min->SetParameter(1,"Ax",pStart[1],0.1,0,0);
    min->SetParameter(2,"y0",pStart[2],0.1,0,0);
    min->SetParameter(3,"Ay",pStart[3],0.1,0,0);
    
    arglist[0] = 100; // number of function calls 
    arglist[1] = 0.01; // tolerance 
    min->ExecuteCommand("MIGRAD",arglist,2);
    
    int nvpar,nparx; 
    double amin,edm, errdef;
    min->GetStats(amin,edm,errdef,nvpar,nparx);
    min->PrintResults(1,amin);

    // get fit parameters
    for (unsigned short int i = 0; i <4; ++i) 
      parFit[i] = min->GetParameter(i);
    TVector3 V(parFit[1],parFit[3],1); // V is a vector which gives the direction of the line
    
    c1 = new TCanvas("c1","c1",200,10,700,500);
    c1->SetGridx();
    hpx = new TH3F("graph","graph",10,0,384,10,0,384,10,0,90); // axis range
    hpx->SetStats(kFALSE); // no statistics
    hpx->Draw();
    gr->Draw("SAMEP0");
    */

        // find the polar angle theta and the azimutal angle phi
        TVector3 V(fx->GetParameter(1),fy->GetParameter(1),1); // V is a vector which gives the direction of the line
        theta = V.Theta();
        phi = V.Phi();
        x0=fx->GetParameter(0);
        Ax=fx->GetParameter(1);
        y0=fy->GetParameter(0);
        Ay=fy->GetParameter(1);

        // find coordonates of the crossing point of the track with the layers
        // get fit parameters
        double parFit[4];
        for (unsigned short int i = 0; i <4; ++i) {
            if (i<2) parFit[i] = fx->GetParameter(i);
            if (i>1) parFit[i] = fy->GetParameter(i-2);
        }
        for (unsigned short int i=0;i<NUMBER_OF_LAYER;i++){
            double x,y,z;
            z=(float)(i+0.5)*Z_PITCH;
            line(z,parFit,x,y,z);
            X[i]=x;
            Y[i]=y;
            Z[i]=z;
        }
        /*
    // draw the fitted line
    TCanvas *c1 = new TCanvas("c1","c1",200,10,700,500);
    c1->SetGridx();
    TH3 * hpx = new TH3F("graph","graph",10,0,384,10,0,384,10,0,90); // axis range
    hpx->SetStats(kFALSE); // no statistics
    hpx->Draw();
    gr->Draw("SAMEP0");
    int n = 1000;
    double t0 = 0;
    double dt = (NUMBER_OF_LAYER-0.5)*Z_PITCH;
    TPolyLine3D *l = new TPolyLine3D(n);
    for (int i = 0; i <n;++i) {
      double t = t0 + dt*i/n;
      double x,y,z;
      line(t,parFit,x,y,z);
      l->SetPoint(i,x,y,z);
    }
    l->SetLineColor(kRed);
    l->Draw("same");
    */

        // find E_P_muon and E_N_muon for each layer
        for (unsigned short int i=0;i<NUMBER_OF_LAYER;i++){
            // find the strip which is crossed by the track

            if (i%2 == 0) {
                if ((floor(X[i]/X_PITCH)<24)&&(floor(X[i]/X_PITCH)>=0)) strip_muon[i] = floor(X[i]/X_PITCH) + i*STRIPS_PER_LAYER;
                else strip_muon[i] = -3;
            }

            else {
                if ((floor(Y[i]/Y_PITCH)<24)&&(floor(Y[i]/Y_PITCH)>=0)) strip_muon[i] = floor(Y[i]/Y_PITCH) + i*STRIPS_PER_LAYER;
                else strip_muon[i] = -3;
            }

            E_P_muon[i] = E_P[strip_muon[i]];
            E_N_muon[i] = E_N[strip_muon[i]];

            m_TH3_spectra_P[i]->Fill(X[i]/X_PITCH,Y[i]/Y_PITCH,E_P_muon[i]);
            m_TH3_spectra_N[i]->Fill(X[i]/X_PITCH,Y[i]/Y_PITCH,E_N_muon[i]);
        }

        fx->Delete();
        fy->Delete();

        tree1->Fill();

        /*
      for (unsigned short int i=0;i<NUMBER_OF_LAYER;i++){
      std::cout<<"coordinate_layer["<<i<<"]="<<coordinate_layer[i]<<"\t"<<X[i]<<"\t"<<Y[i]<<"\t"<<E_P_max[i]<<"\t"<<strip_muon[i]<<"\t"<<E_P_muon[i]<<"\n";
      }
      for (unsigned short int i=0;i<NUMBER_OF_LAYER*STRIPS_PER_LAYER;i++){
      std::cout<<"E_P["<<i<<"]="<<E_P[i]<<"\n\n";
      }
    */
    }
    /*
    else {
        theta = -3.;
        phi = -3.;
        for (unsigned short int i=0;i<NUMBER_OF_LAYER;i++){
            X[i]=-50;
            Y[i]=-50;
            Z[i]=-50;
            E_P_muon[i]=-3;
            E_N_muon[i]=-3;
            strip_muon[i]=-3;
            x0=-3;
            Ax=-3;
            y0=-3;
            Ay=-3;
        }
    }
*/

    gr->Delete();
    grx->Delete();
    gry->Delete();
}

DataManager::DataManager(std::string pathToSetupFile,std::string pathToReadInFile){
    m_pathToDACFile = "";
    m_pathToFormerCfgFile = "";
    m_dacFromCfgFile = false;
    m_dacFromAdjustFile = false;
    m_forceToChangeCfgFile = false;
    m_aimGain = 12;

    m_readInSetupFile = true;


    // read set up from the setupFile
    std::ifstream setupFile(pathToSetupFile.c_str());
    if(!setupFile){
        std::cerr << "Failure: could not open file: \"" << pathToSetupFile << "\"." << std::endl;
        std::cerr << "Please check if the path is correct or not!" << std::endl;
        assert(false);
    }

    std::string s;
    int temp;
    while (true) {
        std::getline(setupFile, s);
        if (setupFile.eof()) break;
        const char* searchString = s.c_str();

        if (s.find("#") == 0 || s=="") {
            continue; // Skip commented lines
        }

        if(sscanf(searchString, "L1_P1_P || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P1_P,temp));
        }

        if(sscanf(searchString, "L1_P1_N || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P1_N,temp));
        }

        if(sscanf(searchString, "L1_P2_P || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P2_P,temp));
        }

        if(sscanf(searchString, "L1_P2_N || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P2_N,temp));
        }

        if(sscanf(searchString, "L1_P3_P || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P3_P,temp));
        }

        if(sscanf(searchString, "L1_P3_N || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P3_N,temp));
        }

        if(sscanf(searchString, "L1_P4_P || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P4_P,temp));
        }

        if(sscanf(searchString, "L1_P4_N || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P4_N,temp));
        }

        if(sscanf(searchString, "L2_P1_P || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P1_P,temp));
        }

        if(sscanf(searchString, "L2_P1_N || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P1_N,temp));
        }

        if(sscanf(searchString, "L2_P2_P || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P2_P,temp));
        }

        if(sscanf(searchString, "L2_P2_N || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P2_N,temp));
        }

        if(sscanf(searchString, "L2_P3_P || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P3_P,temp));
        }

        if(sscanf(searchString, "L2_P3_N || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P3_N,temp));
        }

        if(sscanf(searchString, "L2_P4_P || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P4_P,temp));
        }

        if(sscanf(searchString, "L2_P4_N || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P4_N,temp));
        }

    }

    SuperLayerMap::iterator superLayer_iter;
    for(int superLayerIndex = 0;superLayerIndex<2;superLayerIndex++){
        for(superLayer_iter= m_superLayerMap[superLayerIndex].begin();superLayer_iter!=m_superLayerMap[superLayerIndex].end();superLayer_iter++){
            if(superLayer_iter->second!=0) m_uplinkId.push_back(superLayer_iter->second);
            //printf("m_superLayerMap[%d][%d] = %d \n",superLayerIndex,superLayer_iter->first,superLayer_iter->second);
            //getchar();
        }
    }

    TFile *file = new TFile(pathToReadInFile.c_str(),"READ");
    printf("readInFile from %s \n",pathToReadInFile.c_str());

    char tmp[256];

    TH2* histo;

    //Fill the raw pedestals
    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        sprintf(tmp,"raw_pedestals_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
        histo = (TH2*)file->Get(tmp);
        m_raw_pedestals.push_back(histo);
    }

    //Fill the PS_pedestal
    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        sprintf(tmp,"PS_pedestals_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
        histo = (TH2*)file->Get(tmp);
        m_PS_pedestals.push_back(histo);
    }

    //Fill the adcHistogram
    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        sprintf(tmp,"adcHistogram_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
        histo = (TH2*)file->Get(tmp);
        m_adcHistogram.push_back(histo);
    }

    //fill the PS_adcHistogram
    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        sprintf(tmp,"PS_adcHistogram_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
        histo = (TH2*)file->Get(tmp);
        m_PS_adcHistogram.push_back(histo);
    }

    //std::vector<int> corrUplinkPairs;
    SuperLayerMap::iterator corrIter;
    for(int superLayerIndex = 0; superLayerIndex<2;superLayerIndex++){
        for(superLayer_iter= m_superLayerMap[superLayerIndex].begin();superLayer_iter->first<4;superLayer_iter++){
            if(superLayer_iter->second == 0) continue;
            corrIter = m_superLayerMap[superLayerIndex].find(superLayer_iter->first+P_N_DISTANCE);
            if(corrIter == m_superLayerMap[superLayerIndex].end()){
                printf("Can not find its corresponding part of %d, check the setup file!\n",superLayer_iter->first);
                assert(false);
            }else{
                if(corrIter->second != 0){
                    m_corrUplinkPairs.push_back(superLayer_iter->second);
                    m_corrUplinkPairs.push_back(corrIter->second);
                }
            }
        }
    }

    if(m_corrUplinkPairs.size()!=0 ){

        for(int i=0; i< (m_corrUplinkPairs.size()/2); i++){
            int Xuplink = m_corrUplinkPairs.at(2*i);
            int Yuplink = m_corrUplinkPairs.at(2*i+1);

            int corrChan = 0;
            int stripIndex = 0;

            for (Int_t i=HEAD_OF_EFFECT_CHANNELS;i<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;i++){
                if((i%CHANNELS_PER_GROUP)>(STRIPS_PER_GROUP-1)) continue; // only 0~11,16~27,32~43,48~59 are SiPM output
                corrChan = (i/CHANNELS_PER_GROUP)*CHANNELS_PER_GROUP+STRIPS_PER_GROUP-1-i%CHANNELS_PER_GROUP;

                int SuperLayerIndex = 0;
                int positionIndex = 0;


                for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                    for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                        if(superLayer_iter->second==Xuplink) {
                            SuperLayerIndex = LayerIndex;
                            positionIndex = superLayer_iter->first;
                            break;
                        }
                    }
                }

                stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+(i/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+i%CHANNELS_PER_GROUP;
                //std::cout<<"chan= "<<i <<" & corrChan = "<<corrChan <<" are correlated!"<<" stripIndex = "<<stripIndex<<std::endl;
                //getchar();
                sprintf(tmp, "uplink_%d_vs_%d_____uplink%d_channel%03d_with_uplink%d_channel%03d",Xuplink,Yuplink,Xuplink,i,Yuplink,corrChan);
                histo = (TH2*)file->Get(tmp);
                m_strip_correlation.insert(std::map<int,TH2*>::value_type(stripIndex,histo));
                m_map_key_strip_value_N_uplink.insert(std::map<int,int>::value_type(stripIndex,Yuplink));
                m_map_key_strip_value_N_ch.insert(std::map<int,int>::value_type(stripIndex,corrChan));
                m_map_key_strip_value_P_uplink.insert(std::map<int,int>::value_type(stripIndex,Xuplink));
                m_map_key_strip_value_P_ch.insert(std::map<int,int>::value_type(stripIndex,i));
                m_map_key_uplinkID_value_N_or_P.insert(std::map<int,int>::value_type(Yuplink,0));
                m_map_key_uplinkID_value_N_or_P.insert(std::map<int,int>::value_type(Xuplink,1));
                std::pair<int,int> pair_N_uplink_ch(Yuplink,corrChan); //for N<uplinkID,ch>
                //std::cout<<"pair_N_uplink_ch.first="<<pair_N_uplink_ch.first<<"\t pair_N_uplink_ch.second="<<pair_N_uplink_ch.second<<"\n";
                std::pair<int,int> pair_P_uplink_ch(Xuplink,i); //for N<uplinkID,ch>
                m_key_N_uplink_ch_value_strip.insert(std::map<std::pair<int,int>,int>::value_type(pair_N_uplink_ch,stripIndex));
                //std::cout<<"m_key_N_uplink_ch_value_strip[pair_N_uplink_ch]="<<m_key_N_uplink_ch_value_strip[pair_N_uplink_ch]<<"\n";
                m_key_P_uplink_ch_value_strip.insert(std::map<std::pair<int,int>,int>::value_type(pair_P_uplink_ch,stripIndex));
                //std::cout<<"m_key_P_uplink_ch_value_strip[pair_P_uplink_ch]="<<m_key_P_uplink_ch_value_strip[pair_P_uplink_ch]<<"\n";
            }
        }
    }

    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        TGraphErrors* adcPerPhoton = new TGraphErrors(1);
        m_adcPerPhoton.push_back(adcPerPhoton);

        TGraphErrors* mpvInAdc = new TGraphErrors(1);
        m_mpvInAdc.push_back(mpvInAdc);

        TGraphErrors* resultCorr = new TGraphErrors(1);
        m_resultCorr.push_back(resultCorr);

        sprintf(tmp, "area_uplink%d", m_uplinkId.at(uplinkIndex));
        TH1F* area = new TH1F(tmp,"area_for_mean_Of_MIP_Distribution;Channel;Area",NUMBER_OF_EFFECT_CHANNELS,HEAD_OF_EFFECT_CHANNELS,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS);
        m_area.push_back(area);

        TGraphErrors* meanPhoton = new TGraphErrors(1);
        m_meanInPhoton.push_back(meanPhoton);

    }

}

DataManager::~DataManager(){
    //fclose(m_resultLog);
}

unsigned int DataManager::uplinkIndex(int uplinkId)
{
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
    for (int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
        if (*uplinkIdIt == uplinkId)
            return i;
    }
    assert(false);
    return 0;
}


void DataManager::fill_raw_pedestals(MergedEvent* event)
{
    Settings* settings = Settings::instance();
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
    for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
        int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
        unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
        unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;
        const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);
        for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip) {
            for (unsigned short channel = 0; channel < (channelsPerReadoutChip); ++channel) {
                unsigned short sample = chip * channelsPerReadoutChip + channel;
                m_raw_pedestals[i]->Fill(sample , uplinkData.adcValue(sample));
            }
        }
    }
}


void DataManager::find_working_channels()
{
    Settings* settings = Settings::instance();
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
    for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
        int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
        unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
        unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;
        unsigned short * ch_state;
        ch_state = new unsigned short[numberOfReadoutChips*channelsPerReadoutChip];
        for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip) {
            for (unsigned short channel = 0; channel < (channelsPerReadoutChip); ++channel) {
                unsigned short sample = chip * channelsPerReadoutChip + channel;
                float mean,rms;
                mean = m_raw_pedestals[i]->ProjectionY("proj1",sample+1,sample+1,"")->GetMean();
                rms = m_raw_pedestals[i]->ProjectionY("proj2",sample+1,sample+1,"")->GetRMS();
                unsigned short state=1;
                if (rms<1) state = 0;
                ch_state[sample]=state;
                //std::cout<< "i = " << i << "     sample = "<< sample << "   rms=" << rms << "   mean=" << mean << "    state = " << ch_state[sample] << std::endl;
            }
        }
        m_state.push_back(ch_state);
        //delete ch_state; //state must not be deleted, otherwise, the content of m_state is also deleted.
    }
}


void DataManager::pedestals_write_Mean_RMS()
{
    Settings* settings = Settings::instance();
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
    for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
        int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
        unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
        unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;

        float * pedestal_Mean;
        pedestal_Mean = new float[sampleSize];
        float * pedestal_RMS;
        pedestal_RMS = new float[sampleSize];

        for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip) {
            for (unsigned short channel = 0; channel < (channelsPerReadoutChip); ++channel) {

                unsigned short sample = chip * channelsPerReadoutChip + channel;
                pedestal_Mean[sample] = m_raw_pedestals[i]->ProjectionY("p1",sample+1,sample+1,"")->GetMean();
                pedestal_RMS[sample] = m_raw_pedestals[i]->ProjectionY("p2",sample+1,sample+1,"")->GetRMS();
            }
        }

        m_pedestal_Mean.push_back(pedestal_Mean);
        m_pedestal_RMS.push_back(pedestal_RMS);
    }
}

void DataManager::getEnergy(MergedEvent* event)
{
    Settings* settings = Settings::instance();
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();

    if(m_readInSetupFile){
        int SuperLayerIndex = 0;
        int positionIndex = 0;
        int stripIndex = 0;
        int XuplinkID = 0;
        int YuplinkID = 0;
        int XuplinkIndex  = 0;
        int YuplinkIndex = 0;
        std::map<int,TH2*>::iterator stripCorrHisto;
        SuperLayerMap::iterator superLayer_iter;
        int corrChan = 0;

        for(int corrIndex = 0; corrIndex< m_corrUplinkPairs.size()/2; corrIndex++){
            XuplinkID = m_corrUplinkPairs.at(2*corrIndex);
            XuplinkIndex = uplinkIndex(XuplinkID);
            YuplinkID = m_corrUplinkPairs.at(2*corrIndex+1);
            YuplinkIndex = uplinkIndex(YuplinkID);
            const UplinkData& XuplinkData = event->uplinkData(XuplinkID);
            const UplinkData& YuplinkData = event->uplinkData(YuplinkID);
            //std::cout<<"correalation for XuplinkID = "<<XuplinkID<<" & YuplinkID = "<<YuplinkID <<"m_corrUplinkPairs.size() = "<<m_corrUplinkPairs.size()<<std::endl;
            //getchar();
            //generate stripIndex
            for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                    if(superLayer_iter->second==XuplinkID) {
                        SuperLayerIndex = LayerIndex;
                        positionIndex = superLayer_iter->first;
                        break;
                    }
                }
            }

            for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS;chan++) {
                if((chan%CHANNELS_PER_GROUP)>(STRIPS_PER_GROUP-1)) continue; // only 0~11,16~27,32~43,48~59 are SiPM output
                corrChan = (chan/CHANNELS_PER_GROUP)*CHANNELS_PER_GROUP+STRIPS_PER_GROUP-1-chan%CHANNELS_PER_GROUP;
                stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+(chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+chan%CHANNELS_PER_GROUP;
                //std::cout<<"for chan = "<<chan <<" on uplink"<<XuplinkID<<" & corrChan = "<<corrChan <<" on uplink" <<YuplinkID<<" ,stripIndex = "<<stripIndex<<std::endl;
                //getchar();
                stripCorrHisto = m_strip_correlation.find(stripIndex);
                if(stripCorrHisto!=m_strip_correlation.end()){
                    stripCorrHisto->second->Fill(XuplinkData.adcValue(chan) - m_pedestal_Mean.at(XuplinkIndex)[chan] , YuplinkData.adcValue(corrChan) - m_pedestal_Mean.at(YuplinkIndex)[corrChan]);
                }else{
                    std::cerr<< "Cannot find corrHisto for uplink_P= "<<XuplinkID <<" & uplink_N= "<<YuplinkID<<std::endl;
                    assert(false);
                }
            }
            //std::cout<<"finish 1 correlation!\n";

        }
        //std::cout<<"finish All Correlation\n";

    }else{
        int corrNum = 2*HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1;
        //printf("corrNum = %d\n",corrNum);
        //printf("m_uplink_correlation.size() = %d\n",m_uplink_correlation.size());

        int corrSum = m_uplink_correlation.size()/NUMBER_OF_EFFECT_CHANNELS;
        //printf("corrSum = %d\n",corrSum);

        for(int corrIndex=0; corrIndex< corrSum;corrIndex++){
            const UplinkData& XuplinkData = event->uplinkData(m_uplinkId.at(2*corrIndex));
            const UplinkData& YuplinkData = event->uplinkData(m_uplinkId.at(2*corrIndex+1));
            // printf("before for loop \n");
            for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS;chan++) {
                m_uplink_correlation[corrIndex*NUMBER_OF_EFFECT_CHANNELS+chan-HEAD_OF_EFFECT_CHANNELS]->Fill(XuplinkData.adcValue(chan) - m_pedestal_Mean.at(2*corrIndex)[chan] , YuplinkData.adcValue(corrNum-chan) - m_pedestal_Mean.at(2*corrIndex+1)[corrNum-chan]);
            }
        }
    }

    //printf("finish correalation\n");
    // getchar();

    for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
        int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
        unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
        unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;
        const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);
        //if (i==0) const UplinkData& uplinkData41 = event->uplinkData(*uplinkIdIt);
        //if (i==1) const UplinkData& uplinkData42 = event->uplinkData(*uplinkIdIt);

        for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip) {
            for (unsigned short channel = 0; channel < channelsPerReadoutChip; ++channel) {
                unsigned short sample = chip * channelsPerReadoutChip + channel;
                m_adcHistogram[i]->Fill(sample , uplinkData.adcValue(sample));
                m_PS_adcHistogram[i]->Fill(sample , uplinkData.adcValue(sample) - m_pedestal_Mean.at(i)[sample]);
            }
        }
    }
}

void DataManager::getCorrelationP(MergedEvent* event)
{
    Settings* settings = Settings::instance();
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();

    if(m_readInSetupFile){
        int SuperLayerIndex = 0;
        int positionIndex = 0;
        int stripIndex = 0;
        int XuplinkID = 0;
        int YuplinkID = 0;
        int XuplinkIndex  = 0;
        int YuplinkIndex = 0;
        std::map<int,TH2*>::iterator stripCorrHistoP;
        SuperLayerMap::iterator superLayer_iter;
        int corrChan = 0;

        for(int corrIndex = 0; corrIndex< m_corrUplinkPairs.size()/2; corrIndex++){
            XuplinkID = m_corrUplinkPairs.at(2*corrIndex);
            XuplinkIndex = uplinkIndex(XuplinkID);
            YuplinkID = m_corrUplinkPairs.at(2*corrIndex+1);
            YuplinkIndex = uplinkIndex(YuplinkID);
            const UplinkData& XuplinkData = event->uplinkData(XuplinkID);
            const UplinkData& YuplinkData = event->uplinkData(YuplinkID);
            //std::cout<<"correalation for XuplinkID = "<<XuplinkID<<" & YuplinkID = "<<YuplinkID <<"m_corrUplinkPairs.size() = "<<m_corrUplinkPairs.size()<<std::endl;
            //getchar();
            //generate stripIndex
            for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                    if(superLayer_iter->second==XuplinkID) {
                        SuperLayerIndex = LayerIndex;
                        positionIndex = superLayer_iter->first;
                        break;
                    }
                }
            }

            for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS;chan++) {
                if((chan%CHANNELS_PER_GROUP)>(STRIPS_PER_GROUP-1)) continue; // only 0~11,16~27,32~43,48~59 are SiPM output
                corrChan = (chan/CHANNELS_PER_GROUP)*CHANNELS_PER_GROUP+STRIPS_PER_GROUP-1-chan%CHANNELS_PER_GROUP;
                stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+(chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+chan%CHANNELS_PER_GROUP;
                //std::cout<<"for chan = "<<chan <<" on uplink"<<XuplinkID<<" & corrChan = "<<corrChan <<" on uplink" <<YuplinkID<<" ,stripIndex = "<<stripIndex<<std::endl;
                //getchar();
                stripCorrHistoP = m_LedP_strip_correlation.find(stripIndex);
                if(stripCorrHistoP!=m_LedP_strip_correlation.end()){
                    stripCorrHistoP->second->Fill(XuplinkData.adcValue(chan) - m_pedestal_Mean.at(XuplinkIndex)[chan] , YuplinkData.adcValue(corrChan) - m_pedestal_Mean.at(YuplinkIndex)[corrChan]);
                }else{
                    std::cerr<< "Cannot find corrHisto for uplink_P= "<<XuplinkID <<" & uplink_N= "<<YuplinkID<<std::endl;
                    assert(false);
                }
            }
        }
        //std::cout<<"finish All Correlation\n";

    }else{
        int corrNum = 2*HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1;
        //printf("corrNum = %d\n",corrNum);
        //printf("m_uplink_correlation.size() = %d\n",m_uplink_correlation.size());

        int corrSum = m_uplink_correlation.size()/NUMBER_OF_EFFECT_CHANNELS;
        //printf("corrSum = %d\n",corrSum);

        for(int corrIndex=0; corrIndex< corrSum;corrIndex++){
            const UplinkData& XuplinkData = event->uplinkData(m_uplinkId.at(2*corrIndex));
            const UplinkData& YuplinkData = event->uplinkData(m_uplinkId.at(2*corrIndex+1));
            // printf("before for loop \n");
            for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS;chan++) {
                m_LedP_uplink_correlation[corrIndex*NUMBER_OF_EFFECT_CHANNELS+chan-HEAD_OF_EFFECT_CHANNELS]->Fill(XuplinkData.adcValue(chan) - m_pedestal_Mean.at(2*corrIndex)[chan] , YuplinkData.adcValue(corrNum-chan) - m_pedestal_Mean.at(2*corrIndex+1)[corrNum-chan]);
            }
        }
    }
    //printf("finish correalation\n");
    // getchar();
}

void DataManager::getCorrelationN(MergedEvent* event)
{
    Settings* settings = Settings::instance();
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();

    if(m_readInSetupFile){
        int SuperLayerIndex = 0;
        int positionIndex = 0;
        int stripIndex = 0;
        int XuplinkID = 0;
        int YuplinkID = 0;
        int XuplinkIndex  = 0;
        int YuplinkIndex = 0;
        std::map<int,TH2*>::iterator stripCorrHistoN;
        SuperLayerMap::iterator superLayer_iter;
        int corrChan = 0;

        for(int corrIndex = 0; corrIndex< m_corrUplinkPairs.size()/2; corrIndex++){
            XuplinkID = m_corrUplinkPairs.at(2*corrIndex);
            XuplinkIndex = uplinkIndex(XuplinkID);
            YuplinkID = m_corrUplinkPairs.at(2*corrIndex+1);
            YuplinkIndex = uplinkIndex(YuplinkID);
            const UplinkData& XuplinkData = event->uplinkData(XuplinkID);
            const UplinkData& YuplinkData = event->uplinkData(YuplinkID);
            //std::cout<<"correalation for XuplinkID = "<<XuplinkID<<" & YuplinkID = "<<YuplinkID <<"m_corrUplinkPairs.size() = "<<m_corrUplinkPairs.size()<<std::endl;
            //getchar();
            //generate stripIndex
            for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                    if(superLayer_iter->second==XuplinkID) {
                        SuperLayerIndex = LayerIndex;
                        positionIndex = superLayer_iter->first;
                        break;
                    }
                }
            }

            for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS;chan++) {
                if((chan%CHANNELS_PER_GROUP)>(STRIPS_PER_GROUP-1)) continue; // only 0~11,16~27,32~43,48~59 are SiPM output
                corrChan = (chan/CHANNELS_PER_GROUP)*CHANNELS_PER_GROUP+STRIPS_PER_GROUP-1-chan%CHANNELS_PER_GROUP;
                stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+(chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+chan%CHANNELS_PER_GROUP;
                //std::cout<<"for chan = "<<chan <<" on uplink"<<XuplinkID<<" & corrChan = "<<corrChan <<" on uplink" <<YuplinkID<<" ,stripIndex = "<<stripIndex<<std::endl;
                //getchar();
                stripCorrHistoN = m_LedN_strip_correlation.find(stripIndex);
                if(stripCorrHistoN!=m_LedN_strip_correlation.end()){
                    stripCorrHistoN->second->Fill(XuplinkData.adcValue(chan) - m_pedestal_Mean.at(XuplinkIndex)[chan] , YuplinkData.adcValue(corrChan) - m_pedestal_Mean.at(YuplinkIndex)[corrChan]);
                }else{
                    std::cerr<< "Cannot find corrHisto for uplink_P= "<<XuplinkID <<" & uplink_N= "<<YuplinkID<<std::endl;
                    assert(false);
                }
            }
        }
        //std::cout<<"finish All Correlation\n";

    }else{
        int corrNum = 2*HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1;
        //printf("corrNum = %d\n",corrNum);
        //printf("m_uplink_correlation.size() = %d\n",m_uplink_correlation.size());

        int corrSum = m_uplink_correlation.size()/NUMBER_OF_EFFECT_CHANNELS;
        //printf("corrSum = %d\n",corrSum);

        for(int corrIndex=0; corrIndex< corrSum;corrIndex++){
            const UplinkData& XuplinkData = event->uplinkData(m_uplinkId.at(2*corrIndex));
            const UplinkData& YuplinkData = event->uplinkData(m_uplinkId.at(2*corrIndex+1));
            // printf("before for loop \n");
            for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS;chan++) {
                m_LedN_uplink_correlation[corrIndex*NUMBER_OF_EFFECT_CHANNELS+chan-HEAD_OF_EFFECT_CHANNELS]->Fill(XuplinkData.adcValue(chan) - m_pedestal_Mean.at(2*corrIndex)[chan] , YuplinkData.adcValue(corrNum-chan) - m_pedestal_Mean.at(2*corrIndex+1)[corrNum-chan]);
            }
        }
    }
    //printf("finish correalation\n");
    // getchar();
}

void DataManager::draw()
{
    char tmp[256];
    TCanvas* canvas = NULL;

    double mean = 0;
    double rms = 0;
    TH1 *hE = NULL;

    for(unsigned int i=0;i<m_uplinkId.size();++i){
        sprintf(tmp,"uplink%d_histogram",m_uplinkId.at(i));
        canvas = new TCanvas(tmp,tmp,0,0,1200,900);
        canvas->Divide(1,3);
        canvas->cd(1);
        m_raw_pedestals[i]->Draw("COLZ");
        mean = m_raw_pedestals[i]->GetMean(2);
        rms = m_raw_pedestals[i]->GetRMS(2);
        m_raw_pedestals[i]->GetYaxis()->SetRangeUser(mean - 10*rms, mean + 10*rms);

        canvas->GetPad(1)->SetLogz();

        canvas->cd(2);
        m_adcHistogram[i]->Draw("COLZ");
        mean = m_adcHistogram[i]->GetMean(2);
        rms = m_adcHistogram[i]->GetRMS(2);
        m_adcHistogram[i]->GetYaxis()->SetRangeUser(300, mean + 10*rms);
        canvas->GetPad(2)->SetLogz();

        canvas->cd(3);
        m_PS_adcHistogram[i]->Draw("COLZ");
        mean = m_PS_adcHistogram[i]->GetMean(2);
        rms = m_PS_adcHistogram[i]->GetRMS(2);
        // m_PS_adcHistogram[i]->GetYaxis()->SetRangeUser(mean - 10*rms, mean + 10*rms);
        // m_PS_adcHistogram[i]->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
        canvas->GetPad(3)->SetLogz();
    }

    //open a file
    FILE* resultLog = NULL;
    resultLog = fopen("../data/rmsNpedestal_rmsPpedestal_ratio_N_over_P.txt","w");
    fprintf(resultLog,"strip N_uplink N_ch P_uplink P_ch rms_N_pedestal rms_P_pedestal <N>/<P>\n");
    int corrSum = 0;
    if(m_readInSetupFile){
        corrSum = m_strip_correlation.size();
        //printf("corrSum = %d\n", corrSum);
        int xPadNum = 4;
        int yPadNum = 3;
        int padPerCanvas = xPadNum*yPadNum;
        int canvasNum = 0;
        if(corrSum%padPerCanvas == 0)    canvasNum = corrSum/padPerCanvas;
        else   canvasNum = corrSum/padPerCanvas+1;
        int canvasIndex = 0;

        std::map<int,TH2*>::iterator stripCorrelation_iter;
        TH2* currHisto = NULL;
        int picIndex = 0;
        for(stripCorrelation_iter = m_strip_correlation.begin();stripCorrelation_iter!=m_strip_correlation.end();picIndex++,stripCorrelation_iter++){
            if(picIndex%padPerCanvas == 0){
                if(canvasNum==1) sprintf(tmp,"strip correlation");
                else sprintf(tmp,"strip correlation (%d)",picIndex/padPerCanvas);
                canvas = new TCanvas(tmp,tmp,0,0,1200,900);
                canvas->Divide(xPadNum,yPadNum);
                canvasIndex++;
            }

            canvas->cd(picIndex%padPerCanvas+1);
            currHisto = stripCorrelation_iter->second;
            currHisto->Draw("COLZ");

            canvas->GetPad(picIndex%padPerCanvas+1)->SetLogz();

            //std::cout<<"strip="<<stripCorrelation_iter->first;
            //std::cout<<"\t N uplinkID="<<m_map_key_strip_value_N_uplink[stripCorrelation_iter->first]<<"\t N ch="<<m_map_key_strip_value_N_ch[stripCorrelation_iter->first];
            //std::cout<<"\t P uplinkID="<<m_map_key_strip_value_P_uplink[stripCorrelation_iter->first]<<"\t P ch="<<m_map_key_strip_value_P_ch[stripCorrelation_iter->first];
            TH2 *raw_pedestal_N = m_raw_pedestals.at(uplinkIndex(m_map_key_strip_value_N_uplink[stripCorrelation_iter->first]));
            sprintf(tmp,"N_U%d_chan%d",m_map_key_strip_value_N_uplink[stripCorrelation_iter->first],m_map_key_strip_value_N_ch[stripCorrelation_iter->first]);
            hE = raw_pedestal_N->ProjectionY(tmp,m_map_key_strip_value_N_ch[stripCorrelation_iter->first]+1,m_map_key_strip_value_N_ch[stripCorrelation_iter->first]+1,"");
            Float_t rms_N=hE->GetRMS();
            TH2 *raw_pedestal_P = m_raw_pedestals.at(uplinkIndex(m_map_key_strip_value_P_uplink[stripCorrelation_iter->first]));
            sprintf(tmp,"P_U%d_chan%d",m_map_key_strip_value_P_uplink[stripCorrelation_iter->first],m_map_key_strip_value_P_ch[stripCorrelation_iter->first]);
            hE = raw_pedestal_P->ProjectionY(tmp,m_map_key_strip_value_P_ch[stripCorrelation_iter->first]+1,m_map_key_strip_value_P_ch[stripCorrelation_iter->first]+1,"");
            Float_t rms_P=hE->GetRMS();

            currHisto->GetYaxis()->SetRangeUser(0+4*rms_P,3500);
            currHisto->GetXaxis()->SetRangeUser(0+4*rms_N,3500);
            TF1 *func = new TF1("func","[0]*x",0+4*rms_N,3500);
            currHisto->Fit("func","RQ");
            //Float_t ratio_N_P = currHisto->GetMean(2) / currHisto->GetMean(1);
            Float_t ratio_N_P = func->GetParameter(0);
            //std::cout<<"\t <N>/<P>="<<ratio_N_P<<"\n";
            fprintf(resultLog,"%d %d %d ",stripCorrelation_iter->first,m_map_key_strip_value_N_uplink[stripCorrelation_iter->first],m_map_key_strip_value_N_ch[stripCorrelation_iter->first]);
            fprintf(resultLog,"%d %d %f %f %f\n",m_map_key_strip_value_P_uplink[stripCorrelation_iter->first],m_map_key_strip_value_P_ch[stripCorrelation_iter->first],rms_N,rms_P,ratio_N_P);
            //              TProfile *corrProf=currHisto->ProfileY();
            //              corrProf->Fit("pol1");
            //              corrProf->Draw("SAME");

        }
    }
    hE = NULL;
    fclose(resultLog);
}



Double_t fpeaks(Double_t *x, Double_t *par){
    Double_t result = par[0] + par[1]*x[0];
    for (Int_t p=0;p<npeaks;p++) {
        Double_t norm  = par[3*p+2];
        Double_t mean  = par[3*p+3];
        Double_t sigma = par[3*p+4];
        result += norm*TMath::Gaus(x[0],mean,sigma);
    }
    return result;
}

void DataManager::ProjectionYWithMean(int uplinkId,bool forceInLog){

    TH2 *PS_adcHistogram_uplink = m_PS_adcHistogram.at(uplinkIndex(uplinkId));

    printf("analyze m_PS_adcHistogram.at(%d)_uplinkId%d \n",uplinkIndex(uplinkId),uplinkId);
    //getchar();


    char tmp[256];
    TCanvas *c1 = NULL;
    TH1 *hE = NULL;


    printf("for UplinkId = %d, draw projection and save mean values\n",uplinkId);

    //open a file
    FILE* resultLog = NULL;
    sprintf(tmp,"../data/uplink%d_mean_rms.txt",uplinkId);
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----find mean and RMS for all channels -measurement with LED and high light-----\n\n");
    fprintf(resultLog,"uplink channel mean rms\n");

    printf("onpen file\n");


    int xPadNum = 4;
    int yPadNum = 4;
    int padPerCanvas = xPadNum*yPadNum;
    printf("padPerCanvas = %d\n",padPerCanvas);


    int canvasNum = 0;
    if((NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS)%padPerCanvas == 0)    canvasNum = (NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS)/padPerCanvas;
    else  canvasNum = (NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS)/padPerCanvas+1;
    int canvasIndex = 0;
    printf("canvasNum = %d\n",canvasNum);

    for (Int_t chan=0;chan<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;chan++){
        // measurement with LED and high light
        if(chan%padPerCanvas ==0){      // create a new canvas for padPerCanvas channels
            if(canvasNum == 1){
                if(forceInLog) sprintf(tmp,"uplink%d_projectionY_in_Log",uplinkId);
                else sprintf(tmp,"uplink%d_projectionY",uplinkId);

                printf("only 1 canvas needed \n");
            }else{
                if(forceInLog) sprintf(tmp,"uplink%d_projectionY_in_Log (%d)",uplinkId,canvasIndex+1);
                else sprintf(tmp,"uplink%d_projectionY (%d)",uplinkId,canvasIndex+1);

                printf("canvas %d created\n",canvasIndex+1);
            }

            c1 = new TCanvas(tmp,tmp,10,10,1000,900);
            c1->Divide(xPadNum,yPadNum);
            canvasIndex++;
        }
        // find mean and RMS for all channels

        //for (Int_t chan=0;chan<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;chan++){
        Double_t mean,rms;
        c1->cd(chan%padPerCanvas+1);
        printf("c1->cd(%d)\n",chan%padPerCanvas+1);
        if(forceInLog) gPad->SetLogy();
        sprintf(tmp,"U%d_chan%d",uplinkId,chan);
        hE = PS_adcHistogram_uplink->ProjectionY(tmp,chan+1,chan+1,"");
        mean = hE->GetMean();
        rms = hE->GetRMS();

        if(forceInLog) hE->SetAxisRange(-50,200,"X");
        else hE->SetAxisRange(mean-3*rms,mean+10*rms,"X");

        hE->DrawCopy();

        /*
       *        if (chan==0) {
       *              std::cout<<"uplink=" <<uplinkId<<"\t ch="<<chan<<"\t mean="<<mean<<"\t rms="<<rms<<"\n";
       *              fprintf(resultLog,"%d %d %f %f\n",uplinkId,chan,mean,rms);
  }
  */
        if (chan>(HEAD_OF_EFFECT_CHANNELS-1)){
            //std::cout<<"uplink="<< uplinkId <<"\t ch="<<chan<<"\t mean="<<mean<<"\t rms="<<rms<<"\n";
            fprintf(resultLog,"%d %d %f %f\n",uplinkId,chan,mean,rms);
        }
        c1->Update();
    }


    fclose(resultLog);

    hE = NULL;
}

void DataManager::ADCPerPhoton(int uplinkId,Float_t gain,bool plot,bool forMIP){

    TH2 *PS_adcHistogram_uplink = m_PS_adcHistogram.at(uplinkIndex(uplinkId));

    printf("analyze m_PS_adcHistogram.at(%d)_uplinkId%d \n",uplinkIndex(uplinkId),uplinkId);

    char tmp[256];
    TCanvas *c1 = NULL;   //TCanvas for ADCperPhoton
    TH1 *hE = NULL;

    //set TCanvas
    gStyle->SetOptFit(1000);

    printf("for UplinkId = %d, draw projection and save mean values\n",uplinkId);

    FILE* resultLog = NULL;

    TGraphErrors* gains = m_adcPerPhoton.at(uplinkIndex(uplinkId));
    TGraphErrors* meanInPhoton = m_meanInPhoton.at(uplinkIndex(uplinkId));    // only for ligth injection
    Int_t gains_index = 0;

    TGraphErrors* mpvInAdc = m_mpvInAdc.at(uplinkIndex(uplinkId));


    //open a file to log the result
    if(forMIP) sprintf(tmp,"../data/uplink%d_ADCperPhoton_MIP.txt",uplinkId);
    else sprintf(tmp,"../data/uplink%d_ADCperPhoton.txt",uplinkId);
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----find ADC/photon --------\n\n");
    if(forMIP) fprintf(resultLog,"uplink\t channel\t ADC/photonPeak\t\n");
    else   fprintf(resultLog,"uplink\t channel\t ADC/photonPeak\t meanPhoton\t DAC_adjust\t formerDAC_values\t\n");


    int setDAC[64]; // array to instore the DAC values
    for(int i=0; i<64;i++) {
        if(i==0) setDAC[i] = 0;
        else setDAC[i]=DAC_NOMINAL;
    }

    //////////////// read in the DAC values from file///////////////////////////////////////////////////////////////////
    if(m_dacFromCfgFile || m_dacFromAdjustFile){
        std::ostringstream pathToUsedDAC;
        if(m_dacFromCfgFile) pathToUsedDAC << m_pathToDACFile <<"/VATA64V2_uplink"<<uplinkId<<".cfg";
        else pathToUsedDAC << m_pathToDACFile <<"/adjustDAC_uplink" << uplinkId <<".txt";

        std::fstream dacFile(pathToUsedDAC.str().c_str(),std::fstream::in|std::fstream::out);

        if(!dacFile) {
            std::cerr<<"dacFile =  "<<pathToUsedDAC.str()<<" doesn't exist! exit abnormal!\n"<<std::endl;
            assert(false);
        }else{
            //std::cout<<"dacFile = "<<pathToUsedDAC.str()<<std::endl;
        }

        std::string s;

        while(true){
            std::getline(dacFile,s);
            if(dacFile.eof()) break;

            const char* searchString = s.c_str();

#ifdef DUMP_DEBUG
            //std::cout<<"s = "<< s <<std::endl;
            getchar();
#endif

            if(s.find("#") ==0)  continue; //skip the comment line

            if(sscanf(searchString, "Preamplifier_input_potential_DAC  || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d ||",
                      &setDAC[0], &setDAC[1], &setDAC[2], &setDAC[3], &setDAC[4],&setDAC[5], &setDAC[6],&setDAC[7], &setDAC[8],&setDAC[9], &setDAC[10],&setDAC[11], &setDAC[12],
                      &setDAC[13], &setDAC[14],&setDAC[15], &setDAC[16], &setDAC[17], &setDAC[18], &setDAC[19],&setDAC[20], &setDAC[21], &setDAC[22],&setDAC[23], &setDAC[24],
                      &setDAC[25], &setDAC[26],&setDAC[27], &setDAC[28], &setDAC[29], &setDAC[30], &setDAC[31],&setDAC[32], &setDAC[33],&setDAC[34],&setDAC[35], &setDAC[36],
                      &setDAC[37],&setDAC[38], &setDAC[39], &setDAC[40], &setDAC[41], &setDAC[42],&setDAC[43],&setDAC[44],&setDAC[45], &setDAC[46],&setDAC[47], &setDAC[48],
                      &setDAC[49], &setDAC[50],&setDAC[51], &setDAC[52],&setDAC[53], &setDAC[54], &setDAC[55],&setDAC[56],&setDAC[57], &setDAC[58],&setDAC[59],&setDAC[60],
                      &setDAC[61], &setDAC[62],&setDAC[63])==64){
                printf("read in setDACs from %s\n",pathToUsedDAC.str().c_str());
                for(int i = 0; i<64; i++) {printf("setDAC[%d] = %d ",i,setDAC[i]);if(i%6 ==0 &&i!=0) printf("\n");}
                printf("\n");
            }
        }

        dacFile.close();
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    int xPadNum = 4;
    int yPadNum = 3;
    int padPerCanvas = xPadNum*yPadNum;


    int canvasNum = 0;
    if(NUMBER_OF_EFFECT_CHANNELS%padPerCanvas == 0)    canvasNum = NUMBER_OF_EFFECT_CHANNELS/padPerCanvas;
    else   canvasNum = NUMBER_OF_EFFECT_CHANNELS/padPerCanvas+1;
    int canvasIndex = 0;
    // printf("canvasNum = %d\n",canvasNum);

    int picIndex = 0;
    for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;chan++){

        if(m_readInSetupFile){
            if((chan%CHANNELS_PER_GROUP)>(STRIPS_PER_GROUP-1)) {
                //  printf("ADCperPhoton: chan= %d, will be ommited!\n",chan);
                // getchar();
                continue;  //only fit for SiPM output
            }
        }

        //if((chan-HEAD_OF_EFFECT_CHANNELS)%padPerCanvas == 0){
        if((picIndex%padPerCanvas) == 0){
            if(m_readInSetupFile){
                sprintf(tmp,"uplink%d_ADC/photon  (%d)",uplinkId,picIndex/padPerCanvas);
            }else{
                if(canvasNum == 1) sprintf(tmp,"uplink%d_ADC/photon",uplinkId);
                else sprintf(tmp,"uplink%d_ADC/photon  (%d)",uplinkId,canvasIndex+1);
            }
            c1 = new TCanvas(tmp,tmp,10,10,1000,900);
            c1->Divide(xPadNum,yPadNum);
            canvasIndex++;
            //std::cout<<"create a new canvas: now picIndex = "<<picIndex<<" chan = "<<chan<<std::endl;
            //getchar();
        }
        Double_t meanValue = 0;
        c1->cd(picIndex%padPerCanvas+1);
        picIndex++;
        sprintf(tmp,"U%d_chan%d",uplinkId,chan);
        hE = PS_adcHistogram_uplink->ProjectionY(tmp,chan+1,chan+1,"");

        if(!forMIP) meanValue = hE->GetMean();    // save the mean value for calculating meanPhoton.

        Double_t mean_full = 0;
        Float_t preThreshold = 0;
        Double_t rms_full = 0;

        double mip = 0;
        if(!forMIP){  //for the led injection, find the fit range automaticlly
            // hE->SetAxisRange(20,400,"X");
        }else {
            double corrChan=0;
            double temp = 70;
            for(int i=0; i<mpvInAdc->GetN();i++){
                mpvInAdc->GetPoint(i,corrChan,temp);
                if(corrChan == chan) {
                    mip = temp;
                    printf("mip = %f\n",mip);
                    break;
                }
            }
            hE->SetAxisRange(mip-3*gain,mip+6*gain,"X");
            //hE->SetAxisRange(35,400,"X");// since the mip begins from ~35, below is the noise
        }
        mean_full = hE->GetMean();
        //printf("mean_full= %f\n",mean_full);
        Int_t meanBin = hE->GetXaxis()->FindBin(mean_full);
        //printf("meanBin = %d\n",meanBin);
        if(forMIP) {
            Int_t maxBin = hE->GetMaximumBin();
            preThreshold = hE->GetBinContent(maxBin);
            // printf("maxBin = %d, preThreshold = %.0f\n",maxBin,preThreshold);
            // getchar();
        }else    preThreshold = hE->GetBinContent(meanBin);


        rms_full = hE->GetRMS();
        //printf("mean_full = %f, rms_full = %f, preThreshold = %f\n",mean_full,rms_full,preThreshold);
        //getchar();

        Double_t xmin_fitRange = 0;
        if(!forMIP) xmin_fitRange =5;
        else xmin_fitRange =30;


        Double_t fitRange_L ;
        Double_t fitRange_R;

        if(forMIP) {
            //             fitRange_L = mean_full-1.5*rms_full;
            //             fitRange_R = mean_full+4*rms_full;z
            if(mip-4*rms_full<30) fitRange_L = 30;
            else fitRange_L = mip-4*gain;

            fitRange_R = mip+5*gain;
            fitRange_R = mip+3*gain;

            //temper
            fitRange_L = 40;//temp for Layer24_cosmics_20120404.root
            fitRange_R = 120;//temp for Layer24_cosmics_20120404.root

        }else{
            fitRange_L = mean_full-3*rms_full;
            fitRange_R = mean_full+rms_full;
        }
        if(fitRange_L< xmin_fitRange) fitRange_L = xmin_fitRange;

        //printf("before seeking peaks, set AxisRange: fitRange_L = %f, fitRange_R = %f. \n",fitRange_L,fitRange_R);
        //getchar();


        hE->SetAxisRange(fitRange_L,fitRange_R,"X");


        Int_t max_findPeak = (fitRange_R-fitRange_L)/gain+1;



        TSpectrum *s = new TSpectrum(max_findPeak);



        Int_t nfound = 0;
        if(forMIP)  nfound = s->Search(hE,1.0,"nobackground",0.001);
        else        nfound = s->Search(hE,1.0,"nobackground",0.01);
        //printf("Found %d candidate peaks to fit\n",nfound);

        // fit with a Gaussian
        Double_t mean=hE->GetMean();
        Double_t rms=hE->GetRMS();
        //std::cout<<"chan="<<chan<<"\t mean="<<mean<<"\t rms="<<rms<<"\n";

        if(rms<2) continue; //will not fit for Gain

        Float_t xmin_gaus=mean-4*rms;
        Float_t xmax_gaus=mean+4*rms;
        TF1 *fgaus = new TF1("fgaus","[2]*TMath::Gaus(x,[0],[1])",xmin_gaus,xmax_gaus);
        Int_t bin = hE->GetXaxis()->FindBin(mean);
        Float_t A = hE->GetBinContent(bin);
        fgaus->SetParameters(mean,rms,A);
        hE->Fit("fgaus","RQ");

        Double_t xp_max = -100;
        Double_t xp_min = 4400;

        Double_t par[3000];
        par[0] = 0.;
        par[1] = 0.;
        npeaks = 0;
        Float_t *xpeaks = s->GetPositionX();

        float setThreshold = 0;
        setThreshold = preThreshold;
        //printf("setThreshold = %f\n",setThreshold);

        int npeaks_lowThreshold;
        //if(forMIP) npeaks_lowThreshold =  (fitRange_R-fitRange_L)/gain-1;
        if(forMIP) npeaks_lowThreshold =  5;
        else npeaks_lowThreshold = 6;

        if(uplinkId == 44 && chan == 6) npeaks_lowThreshold = 8; //temp for layer20_cosmic_2012_4_17.root
        if(uplinkId == 45 && chan == 8) npeaks_lowThreshold = 8; //temp for layer20_cosmic_2012_4_17.root

        while(npeaks<npeaks_lowThreshold){
            // printf("threshold = %f\n",setThreshold);
            // getchar();
            npeaks = 0;
            xp_max = -100;
            xp_min = 4400;
            for (Int_t p=0;p<nfound;p++) {
                Float_t xp = xpeaks[p];
                Int_t bin = hE->GetXaxis()->FindBin(xp);
                Float_t yp = hE->GetBinContent(bin);

                //std::cout<<"p = "<<p<<",xp = "<<xp<<",yp = "<<yp<<std::endl;

                if(forMIP) {
                    //printf("forMIP \n");
                    if (yp<setThreshold ) continue;
                }else{
                    if (((yp-TMath::Sqrt(yp)) < 1.0*fgaus->Eval(xp))||(yp<setThreshold)) continue;
                }

                if(xp_min>xp) xp_min=xp;
                if(xp_max<xp) xp_max=xp;


                par[3*npeaks+2] = yp;
                par[3*npeaks+3] = xp;
                par[3*npeaks+4] = 3;
                //std::cout<<"npeaks="<<npeaks<<"\t yp="<<yp<<"\t xp="<<xp<<"\n";
                //std::cout<<"xp_min="<<xp_min<<"\t xp_max="<<xp_max<<"\t\n";
                npeaks++;
            }
            setThreshold = setThreshold*0.8;

            if(setThreshold <10) break;
        };

        //printf("Found %d useful peaks to fit\n",npeaks);
        //printf("Now fitting: Be patient\n");

        Double_t xmin_fit=xp_min-5;
        Double_t xmax_fit=xp_max+5;
        TF1 *fit = new TF1("fit",fpeaks,xmin_fit,xmax_fit,2+3*npeaks);
        TVirtualFitter::Fitter(hE,10+3*npeaks); //we may have more than the default 25 parameters
        fit->SetParameters(par);
        fit->FixParameter(0,0.);
        fit->FixParameter(1,0.);
        fit->SetNpx(1000);

        //std::cout<<"xmin_fit="<<xmin_fit<<"\t xmax_fit="<<xmax_fit<<"\n";
        //getchar();

        hE->SetAxisRange(xmin_fit,xmax_fit,"X");

        hE->Fit("fit","Q","",xmin_fit,xmax_fit);
        if(!forMIP) fgaus->Draw("SAME");

        Double_t *par_fit;
        par_fit=fit->GetParameters();
        Double_t lp=4000;
        Double_t hp=-100;
        Double_t adc_fc; //ADC_per_fired_cell
        Double_t mean_in_photon = 0;
        Double_t DAC_adjust = 0;
        for (Int_t k=0;k<npeaks;k++){
            if (par_fit[3*k+3]<lp) lp=par_fit[3*k+3];
            if (par_fit[3*k+3]>hp) hp=par_fit[3*k+3];
            //std::cout<<"lp="<<lp<<"\t hp="<<hp<<"\t k="<<k<<"\n";
        }
        //std::cout<<"lp="<<lp<<"\t hp="<<hp<<"\t npeaks="<<npeaks<<"\n";
        adc_fc = (hp-lp)/(npeaks-1);
        if(!forMIP) {
            mean_in_photon = meanValue/adc_fc;
            DAC_adjust = (adc_fc-m_aimGain)/0.0335;
        }

        if(forMIP){
            //std::cout<<"uplink="<<uplinkId<<"\t chan="<<chan <<"\t ADCperPhoton="<<adc_fc<<"\n";
            fprintf(resultLog,"%d %d %f\n",uplinkId,chan,adc_fc);
            std::pair<int,int> pair_N_uplink_ch(uplinkId,chan);
            m_map_key_strip_value_N_ADCPerPE.insert(std::map<int,double>::value_type(m_key_N_uplink_ch_value_strip[pair_N_uplink_ch],adc_fc));
        }else{
            //std::cout<<"uplink="<<uplinkId<<"\t chan="<<chan <<"\t ADCperPhoton="<<adc_fc<<"\t meanInPhoton ="<<mean_in_photon<<"\t DAC_adjust = "<<DAC_adjust<<"\t former DAC = "<<setDAC[chan]<<"\t\n";
            fprintf(resultLog,"%d %d %f %f %f %d\n",uplinkId,chan,adc_fc,mean_in_photon,DAC_adjust,setDAC[chan]);

            // make sure that the DAC value will never be out of range!!
            if( (setDAC[chan]+DAC_adjust)<1 )  setDAC[chan] = 1;
            else if((setDAC[chan]+DAC_adjust)>256) setDAC[chan] = 255;
            else setDAC[chan] += DAC_adjust;

#ifdef DUMP_DEBUG
            printf("setDAC[%d] = %d\n",chan,setDAC[chan]);
#endif
        }
        hE->DrawCopy();
        if(!forMIP)  fgaus->Draw("SAME");
        c1->Update();
        //getchar();

        gains->SetPoint(gains_index,chan,adc_fc);
        if(!forMIP) meanInPhoton->SetPoint(gains_index,chan,mean_in_photon);


        gains_index++;
        // }
    }

    if(gains_index!=NUMBER_OF_EFFECT_CHANNELS) {
        printf("Not all the gain values are stored in m_adcPerPhoton.at(%d) for uplink%d!!!!\n",uplinkIndex(uplinkId),uplinkId);
        //getchar();
    }

    fclose(resultLog);
    hE = NULL;

    //open a file to log the DAC values
    if(!forMIP) {
        if(m_dacFromAdjustFile){
            std::ostringstream pathToNewDAC;
            if(m_dacFromCfgFile) pathToNewDAC <<"../data/adjustDAC_uplink"<<uplinkId<<".txt";
            else pathToNewDAC << m_pathToDACFile <<"/adjustDAC_uplink" << uplinkId <<".txt";

            std::fstream newDacFile(pathToNewDAC.str().c_str(),std::fstream::in|std::fstream::out);
            //std::cout<<"newDacFile = "<<pathToNewDAC.str()<<std::endl;

            if(!newDacFile) {
                std::cerr<<"saving new DACs: newDacFile =  "<<pathToNewDAC.str()<<" doesn't exist! exit abnormal!\n"<<std::endl;
                assert(false);
            }else{
                //std::cout<<"DAC is read from adjust file, modified DACs in this file: newDacFile =  "<<pathToNewDAC.str()<<std::endl;
            }

            newDacFile.seekg(0,std::ios::beg);// move read point to the head of the file
            long posR = newDacFile.tellg();
#ifdef DUMP_DEBUG
            printf("before reading get point is newDacFile.tellg() = %ld,\n",posR);
            printf("before reading put point is newDacFile.tellp() = %ld,\n",newDacFile.tellp());
#endif

            std::string s;

            while(true){
                posR = newDacFile.tellg();
#ifdef DUMP_DEBUG
                printf("posR = %ld\n",posR);
                getchar();
#endif


                std::getline(newDacFile,s);
                if(newDacFile.eof()) break;

#ifdef DUMP_DEBUG
                //std::cout<<"s = "<< s <<std::endl;
                getchar();
#endif

                if(s.find("#") ==0)  continue; //skip the comment line
                if(s.find("Preamplifier_input_potential_DAC") == 0) {
                    newDacFile.seekp(posR);
                    newDacFile<<"#";
                }
            }
            newDacFile.clear();
            newDacFile.seekp(0,std::ios::end);
            newDacFile<<"#For uplinkId= "<<uplinkId<<", aimGain = "<<m_aimGain<<",try the following DAC settings"<<std::endl;
            newDacFile<<"Preamplifier_input_potential_DAC ";
            for(int i = 0; i<64;i++) {
                newDacFile<<" || "<< setDAC[i]<<" ||";
                if(i==63) newDacFile<<std::endl;
            }
            newDacFile.close();
        }
    }

    // if need to write new DAC values to the cfg file
    if((!forMIP)&&m_forceToChangeCfgFile){
        std::ostringstream pathToCfgFile;
        pathToCfgFile << m_pathToFormerCfgFile <<"/VATA64V2_uplink" << uplinkId <<".cfg";

        std::fstream cfgFile(pathToCfgFile.str().c_str(),std::fstream::in|std::fstream::out);
        std::cout<<"cfgFile = "<<pathToCfgFile.str()<<std::endl;

        if(!cfgFile) {
            std::cerr<<"To be modified file: cfgFile =  "<<pathToCfgFile.str()<<" doesn't exist! exit abnormal!\n"<<std::endl;
            assert(false);
        }else{
            std::cout<<"To be modified file: cfgFile =  "<<pathToCfgFile.str()<<std::endl;
        }

        cfgFile.seekg(0,std::ios::beg);// move read point to the head of the file
        long posR = cfgFile.tellg();
#ifdef DUMP_DEBUG
        printf("before reading get point is newDacFile.tellg() = %ld,\n",posR);
        printf("before reading put point is newDacFile.tellp() = %ld,\n",cfgFile.tellp());
#endif

        std::string s;
        while(true){
            posR = cfgFile.tellg();
#ifdef DUMP_DEBUG
            printf("posR = %ld\n",posR);
            getchar();
#endif


            std::getline(cfgFile,s);
            if(cfgFile.eof()) break;

#ifdef DUMP_DEBUG
            std::cout<<"s = "<< s <<std::endl;
            getchar();
#endif

            if(s.find("#") ==0)  continue; //skip the comment line
            // comment all the old DAC values
            if(s.find("Preamplifier_input_potential_DAC") == 0) {
                cfgFile.seekp(posR);
                cfgFile<<"#";
            }
        }
        cfgFile.clear();
        cfgFile.seekp(0,std::ios::end);
        cfgFile<<"#For uplinkId = "<<uplinkId<<", aimGain = "<<m_aimGain<<", try the following DAC settings"<<std::endl;
        cfgFile<<"Preamplifier_input_potential_DAC ";
        for(int i = 0; i<64;i++) {
            cfgFile<<" || "<< setDAC[i]<<" ||";
            if(i==63) cfgFile<<std::endl;
        }
        cfgFile.close();
    }




    //make a new TCanvas to plot ADC/photon for all the channels
    if(plot){
        sprintf(tmp,"uplink%d    ADC/photon Variation",uplinkId);
        TCanvas *c2 = new TCanvas(tmp,tmp,10,10,1000,900);
        if(forMIP) c2->Divide(1,1);
        else c2->Divide(1,2);

        gStyle->SetLabelFont(132,"XY");
        gStyle->SetLabelSize(0.06,"XY");

        gStyle->SetTitleFont(132,"XY");
        gStyle->SetTitleSize(0.06,"XY");
        gStyle->SetTitleYOffset(0.8);
        gStyle->SetTitleXOffset(0.8);

        c2->cd(1);

        TH1F *gainVariation = c2->cd(1)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,9,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,15);
        gainVariation ->SetXTitle("channel #");
        gainVariation ->SetYTitle("gain, ADC");

        gains->SetMarkerStyle(20);
        gains->SetMarkerColor(kRed);
        gains->SetMarkerSize(2.);

        gains->Fit("pol1","Q");
        gains->Draw("PE");

        c2->cd(1)->SetGridx();
        c2->cd(1)->SetGridy();

        if(!forMIP){
            c2->cd(2);

            TH1F *meanInPhotonVariation = c2->cd(2)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,20);
            meanInPhotonVariation ->SetXTitle("channel #");
            meanInPhotonVariation ->SetYTitle("mean, p.e.");

            meanInPhoton->SetMarkerStyle(20);
            meanInPhoton->SetMarkerColor(kRed);
            meanInPhoton->SetMarkerSize(2.);

            meanInPhoton->Fit("pol1","Q");
            meanInPhoton->Draw("PE");

            c2->cd(2)->SetGridx();
            c2->cd(2)->SetGridy();
        }
    }

}

Double_t langaufun(Double_t *x, Double_t *par) {

    //Fit parameters:
    //par[0]=Width (scale) parameter of Landau density
    //par[1]=Most Probable (MP, location) parameter of Landau density
    //par[2]=Total area (integral -inf to inf, normalization constant)
    //par[3]=Width (sigma) of convoluted Gaussian function
    //
    //In the Landau distribution (represented by the CERNLIB approximation),
    //the maximum is located at x=-0.22278298 with the location parameter=0.
    //This shift is corrected within this function, so that the actual
    //maximum is identical to the MP parameter.

    // Numeric constants
    Double_t invsq2pi = 0.3989422804014;   // (2 pi)^(-1/2)
    Double_t mpshift  = -0.22278298;       // Landau maximum location

    // Control constants
    Double_t np = 100.0;      // number of convolution steps
    Double_t sc =   5.0;      // convolution extends to +-sc Gaussian sigmas

    // Variables
    Double_t xx;
    Double_t mpc;
    Double_t fland;
    Double_t sum = 0.0;
    Double_t xlow,xupp;
    Double_t step;
    Double_t i;


    // MP shift correction
    mpc = par[1] - mpshift * par[0];

    // Range of convolution integral
    xlow = x[0] - sc * par[3];
    xupp = x[0] + sc * par[3];

    step = (xupp-xlow) / np;

    // Convolution integral of Landau and Gaussian by sum
    for(i=1.0; i<=np/2; i++) {
        xx = xlow + (i-.5) * step;
        fland = TMath::Landau(xx,mpc,par[0]) / par[0];
        sum += fland * TMath::Gaus(x[0],xx,par[3]);

        xx = xupp - (i-.5) * step;
        fland = TMath::Landau(xx,mpc,par[0]) / par[0];
        sum += fland * TMath::Gaus(x[0],xx,par[3]);
    }

    return (par[2] * step * sum * invsq2pi / par[3]);
}


TF1 *langaufit(TH1D *his, Double_t *fitrange, Double_t *startvalues, Double_t *parlimitslo, Double_t *parlimitshi, Double_t *fitparams, Double_t *fiterrors, Double_t *ChiSqr, Int_t *NDF)
{
    // Once again, here are the Landau * Gaussian parameters:
    //   par[0]=Width (scale) parameter of Landau density
    //   par[1]=Most Probable (MP, location) parameter of Landau density
    //   par[2]=Total area (integral -inf to inf, normalization constant)
    //   par[3]=Width (sigma) of convoluted Gaussian function
    //
    // Variables for langaufit call:
    //   his             histogram to fit
    //   fitrange[2]     lo and hi boundaries of fit range
    //   startvalues[4]  reasonable start values for the fit
    //   parlimitslo[4]  lower parameter limits
    //   parlimitshi[4]  upper parameter limits
    //   fitparams[4]    returns the final fit parameters
    //   fiterrors[4]    returns the final fit errors
    //   ChiSqr          returns the chi square
    //   NDF             returns ndf

    Int_t i;
    Char_t FunName[100];

    sprintf(FunName,"Fitfcn_%s",his->GetName());

    TF1 *ffitold = (TF1*)gROOT->GetListOfFunctions()->FindObject(FunName);
    if (ffitold) delete ffitold;

    TF1 *ffit = new TF1(FunName,langaufun,fitrange[0],fitrange[1],4);
    ffit->SetParameters(startvalues);
    ffit->SetParNames("Width","MP","Area","GSigma");

    for (i=0; i<4; i++) {
        ffit->SetParLimits(i, parlimitslo[i], parlimitshi[i]);
    }

    his->Fit(FunName,"RBQ");   // fit within specified range, use ParLimits, do not plot
    his->GetFunction(FunName)->SetLineWidth(3);

    ffit->GetParameters(fitparams);    // obtain fit parameters
    for (i=0; i<4; i++) {
        fiterrors[i] = ffit->GetParError(i);     // obtain fit parameter errors
    }
    ChiSqr[0] = ffit->GetChisquare();  // obtain chi^2
    NDF[0] = ffit->GetNDF();           // obtain ndf

    return (ffit);              // return fit function

}

void DataManager::PlotAnalysisResult(std::string pedestalFilePath, std::string signalFilePath){

    char tmp[256];

    sprintf(tmp," Analysis Result\t%s\t%s",pedestalFilePath.c_str(),signalFilePath.c_str());
    TCanvas *c1 = new TCanvas(tmp,tmp,1280,1280);
    // c1->Divide(2,3);
    c1->Divide(2,2);

    gStyle->SetLabelFont(132,"XY");
    gStyle->SetLabelSize(0.06,"XY");

    gStyle->SetTitleFont(132,"XY");
    gStyle->SetTitleSize(0.06,"XY");
    gStyle->SetTitleYOffset(0.8);
    gStyle->SetTitleXOffset(0.8);

    TGraphErrors* graph = NULL;
    TH1F *hr = NULL;
    TLegend *legend =NULL;

    //draw ADC per Photon
    c1->cd(1);

    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    TGraphErrors* graph_omitDamagedChannels =NULL;

    for(unsigned int i=0; i<m_adcPerPhoton.size();i++){
        graph = m_adcPerPhoton.at(i);
        graph_omitDamagedChannels = new TGraphErrors(1);
        double chan=0;
        double getAdcPerPhoton =0;
        int index_new = 0;

        for(int index = 0; index<NUMBER_OF_EFFECT_CHANNELS; index++){
            graph->GetPoint(index,chan,getAdcPerPhoton);
            //std::cout<<"index= "<<index<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
            if(getAdcPerPhoton<ADC_PER_PHOTON_UPPER_THRESHOLD) {
                graph_omitDamagedChannels->SetPoint(index_new,chan,getAdcPerPhoton);
                //std::cout<<"index_new= "<<index_new<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
                index_new++;
            }
        }

        sprintf(tmp,"uplink%d",m_uplinkId.at(i));
        legend->AddEntry(graph_omitDamagedChannels,tmp, "p");
        if(i==0){
            hr = c1->cd(1)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,8,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,20);
            hr->SetXTitle("channel #");
            hr->SetYTitle("gain(ADC per Photon), ADC");
        }

        graph_omitDamagedChannels->SetMarkerStyle(20+i);
        //graph->SetMarkerColor(kRed+i);
        graph_omitDamagedChannels->SetMarkerColor(2+2*i);
        graph_omitDamagedChannels->SetMarkerSize(1.);

        graph_omitDamagedChannels->Fit("pol1","Q");
        graph_omitDamagedChannels->GetFunction("pol1")->SetLineColor(2+2*i);
        if(i==0) graph_omitDamagedChannels->Draw("PE");
        else graph_omitDamagedChannels->Draw("PE SAME");

        if(i==0){
            c1->cd(1)->SetGridx();
            c1->cd(1)->SetGridy();
        }
    }
    legend->Draw();

    //draw MPV in ADC
    c1->cd(2);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    for(unsigned int i=0; i<m_mpvInAdc.size();i++){
        graph = m_mpvInAdc.at(i);

        sprintf(tmp,"uplink%d",m_uplinkId.at(i));
        legend->AddEntry(graph,tmp, "p");

        if(i==0){
            hr = c1->cd(2)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,40,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,160);
            hr->SetXTitle("channel #");
            hr->SetYTitle("MPV, ADC");
        }

        graph->SetMarkerStyle(20+i);
        // graph->SetMarkerColor(kRed+i);
        graph->SetMarkerColor(2+2*i);
        graph->SetMarkerSize(1.);

        graph->Fit("pol1","Q");
        //graph->GetFunction("pol1")->SetLineColor(kRed+i);
        graph->GetFunction("pol1")->SetLineColor(2+2*i);

        if(i==0) graph->Draw("PE");
        else graph->Draw("PE SAME");

        if(i==0){
            c1->cd(2)->SetGridx();
            c1->cd(2)->SetGridy();
        }
    }
    legend->Draw();

    //draw resultCorr : MPV in photon electron
    c1->cd(3);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC");  //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    for(unsigned int i=0; i<m_resultCorr.size();i++){
        graph = m_resultCorr.at(i);

        sprintf(tmp,"uplink%d",m_uplinkId.at(i));
        legend->AddEntry(graph,tmp, "p");

        if(i==0){
            hr = c1->cd(3)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,15);
            hr->SetXTitle("channel #");
            hr->SetYTitle("MPV, p.e.");
        }
        graph->SetMarkerStyle(20+i);
        //graph->SetMarkerColor(kRed+i);
        graph->SetMarkerColor(2+2*i);
        graph->SetMarkerSize(1.);

        graph->Fit("pol1","Q");
        //graph->GetFunction("pol1")->SetLineColor(kRed+i);
        graph->GetFunction("pol1")->SetLineColor(2+2*i);
        if(i==0) graph->Draw("PE");
        else graph->Draw("PE SAME");

        if(i==0){
            c1->cd(3)->SetGridx();
            c1->cd(3)->SetGridy();
        }
    }
    legend->Draw();

    //draw area:
    c1->cd(4);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC");  //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    for(unsigned int i=0; i<m_area.size();i++){
        hr = m_area.at(i);
        sprintf(tmp,"uplink%d",m_uplinkId.at(i));
        legend->AddEntry(hr,tmp, "L");

        //hr->SetLineColor(kRed+i);
        hr->SetLineColor(2+2*i);
        hr->SetLineWidth(3);
        if(i==0) hr->DrawCopy();
        else hr->DrawCopy("SAME");

        if(i==0){
            c1->cd(4)->SetGridx();
            c1->cd(4)->SetGridy();
        }
    }
    legend->Draw();
}


void DataManager::PlotAnalysisResult_LED(){

    char tmp[256];

    sprintf(tmp," Analysis Result with Light injection");
    TCanvas *c1 = new TCanvas(tmp,tmp,1280,1280);

    c1->Divide(1,2);


    gStyle->SetOptFit(1000);
    gStyle->SetLabelFont(132,"XY");
    gStyle->SetLabelSize(0.06,"XY");

    gStyle->SetTitleFont(132,"XY");
    gStyle->SetTitleSize(0.06,"XY");
    gStyle->SetTitleYOffset(0.8);
    gStyle->SetTitleXOffset(0.8);




    TGraphErrors* graph = NULL;
    TH1F *hr = NULL;
    TLegend *legend =NULL;
    TGraphErrors* graph_omitDamagedChannels = NULL;


    //draw ADC per Photon
    //std::cout<<"\n\n\ndraw adcPerPhoton \n";
    c1->cd(1);

    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    for(unsigned int i=0; i<m_adcPerPhoton.size();i++){

        graph = m_adcPerPhoton.at(i);
        graph_omitDamagedChannels = new TGraphErrors(1);

        double chan=0;
        double getAdcPerPhoton =0;
        int index_new = 0;

        for(int index = 0; index<NUMBER_OF_EFFECT_CHANNELS; index++){
            graph->GetPoint(index,chan,getAdcPerPhoton);
            //std::cout<<"index= "<<index<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
            if(getAdcPerPhoton<ADC_PER_PHOTON_UPPER_THRESHOLD) {
                graph_omitDamagedChannels->SetPoint(index_new,chan,getAdcPerPhoton);
                //std::cout<<"index_new= "<<index_new<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
                index_new++;
            }
        }
        sprintf(tmp,"uplink%d",m_uplinkId.at(i));
        legend->AddEntry(graph_omitDamagedChannels,tmp, "p");
        if(i==0){
            hr = c1->cd(1)->DrawFrame(HEAD_OF_EFFECT_CHANNELS-1,8,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS+1,15);
            //hr = c1->cd(1)->DrawFrame(0,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS+1,14);
            hr->SetXTitle("channel #");
            hr->SetYTitle("gain(ADC per Photon), ADC");
        }

        graph_omitDamagedChannels->SetMarkerStyle(20+i);
        //graph->SetMarkerColor(kRed+i);
        graph_omitDamagedChannels->SetMarkerColor(2+2*i);
        graph_omitDamagedChannels->SetMarkerSize(1.);

        graph_omitDamagedChannels->Fit("pol1","Q");
        graph_omitDamagedChannels->GetFunction("pol1")->SetLineColor(2+2*i);
        if(i==0) graph_omitDamagedChannels->Draw("PE");
        else graph_omitDamagedChannels->Draw("PE SAME");

        if(i==0){
            c1->cd(1)->SetGridx();
            c1->cd(1)->SetGridy();
        }

        legend->Draw();

    }

    //draw meanInPhoton
    //std::cout<<"\n\n\ndraw meaninPhoton \n";
    c1->cd(2);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    for(unsigned int i=0; i<m_meanInPhoton.size();i++){
        graph = m_meanInPhoton.at(i);
        graph_omitDamagedChannels = new TGraphErrors(1);

        double chan=0;
        double getMeanInPhoton =0;
        int index_new = 0;

        for(int index = 0; index<NUMBER_OF_EFFECT_CHANNELS; index++){
            graph->GetPoint(index,chan,getMeanInPhoton);
            //std::cout<<"index= "<<index<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",meanInPhoton = "<<getMeanInPhoton<<std::endl;
            if(getMeanInPhoton>MEAN_IN_PHOTON_UNDER_THRESHOLD) {
                graph_omitDamagedChannels->SetPoint(index_new,chan,getMeanInPhoton);
                //std::cout<<"index_new= "<<index_new<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<chan<<",meanInPhoton = "<<getMeanInPhoton<<std::endl;
                index_new++;

            }
        }

        sprintf(tmp,"uplink%d",m_uplinkId.at(i));
        legend->AddEntry(graph_omitDamagedChannels,tmp, "p");

        if(i==0){
            hr = c1->cd(2)->DrawFrame(HEAD_OF_EFFECT_CHANNELS-1,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS+1,20);
            //hr = c1->cd(2)->DrawFrame(0,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,20);
            hr->SetXTitle("channel #");
            hr->SetYTitle("mean,p.e.");
        }

        graph_omitDamagedChannels->SetMarkerStyle(20+i);
        // graph->SetMarkerColor(kRed+i);
        graph_omitDamagedChannels->SetMarkerColor(2+2*i);
        graph_omitDamagedChannels->SetMarkerSize(1.);

        graph_omitDamagedChannels->Fit("pol1","Q");
        //graph->GetFunction("pol1")->SetLineColor(kRed+i);
        graph_omitDamagedChannels->GetFunction("pol1")->SetLineColor(2+2*i);

        if(i==0) graph_omitDamagedChannels->Draw("PE");
        else graph_omitDamagedChannels->Draw("PE SAME");

        if(i==0){
            c1->cd(2)->SetGridx();
            c1->cd(2)->SetGridy();
        }
    }

    legend->Draw();
}


void DataManager::PlotAnalysisResult_LED_InStrip(){

    // if read in setup file, define the strip range to be showed
    int stripStart = 0;
    int stripEnd = 0;
    SuperLayerMap::iterator superLayer_iter;
    if(m_readInSetupFile){
        int minPosition = 16; // since for two superLayer, position can only be 0~15 ,change here if there are more superLayers
        int maxPosition = -1;
        int currPosition = 0;
        for(int uplinkIndex =0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
            int currUplinkId = m_uplinkId.at(uplinkIndex);
            for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                    if(superLayer_iter->second== currUplinkId) {
                        currPosition = superLayer_iter->first%P_N_DISTANCE + BOARDS_PER_SUPERLAYER*LayerIndex;
                        if(currPosition>maxPosition) maxPosition = currPosition;
                        if(currPosition<minPosition) minPosition = currPosition;
                        break;
                    }
                }
            }
        }
        stripStart = (minPosition/BOARDS_PER_SUPERLAYER)*STRIPS_PER_SUPERLAYER+(minPosition%BOARDS_PER_SUPERLAYER)*STRIPS_PER_GROUP;
        stripEnd = (maxPosition/BOARDS_PER_SUPERLAYER)*STRIPS_PER_SUPERLAYER +(minPosition%BOARDS_PER_SUPERLAYER)*STRIPS_PER_GROUP+(59/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+59%CHANNELS_PER_GROUP;
    }
    char tmp[256];

    sprintf(tmp," Analysis Result In Strip with Light injection");
    TCanvas *c1 = new TCanvas(tmp,tmp,1280,1280);
    if(m_readInSetupFile){
        if(m_corrUplinkPairs.size()) c1->Divide(1,3);
        else c1->Divide(1,2);
    }else{
        if(m_uplink_correlation.size()) c1->Divide(1,3);
        else c1->Divide(1,2);
    }

    gStyle->SetOptFit(1000);
    gStyle->SetLabelFont(132,"XY");
    gStyle->SetLabelSize(0.06,"XY");

    gStyle->SetTitleFont(132,"XY");
    gStyle->SetTitleSize(0.06,"XY");
    gStyle->SetTitleYOffset(0.8);
    gStyle->SetTitleXOffset(0.8);



    TGraphErrors* graph = NULL;
    TH1F *hr = NULL;
    TLegend *legend =NULL;
    TGraphErrors* graph_omitDamagedChannels = NULL;


    //draw ADC per Photon
    //std::cout<<"\n\n\ndraw adcPerPhoton \n";
    c1->cd(1);

    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);


    FILE* resultLog = NULL;
    //open a file to log the resultz
    sprintf(tmp,"../data/GainList_LED_inStrip.txt");
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----Gain List with LED injection ( in strip ) --------\n\n");
    fprintf(resultLog,"strip\t uplink\t channel\t Gain (ADC/photon)\t\n");

    int stripBase = 1;
    for(unsigned int i=0; i<m_adcPerPhoton.size();i++){
        if(i==0) stripBase = 1;
        if((i%2 == 0)&& (i!=0)){
            stripBase +=NUMBER_OF_EFFECT_CHANNELS;
        }

        graph = m_adcPerPhoton.at(i);
        graph_omitDamagedChannels = new TGraphErrors(1);

        double chan=0;
        double getAdcPerPhoton =0;
        int index_new = 0;
        int stripIndex = 0;

        int SuperLayerIndex = 0;
        int positionIndex = 0;

        if(m_readInSetupFile) {
            for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                    if(superLayer_iter->second==m_uplinkId.at(i)) {
                        SuperLayerIndex = LayerIndex;
                        positionIndex = superLayer_iter->first;
                        break;
                    }
                }
            }
        }

        for(int index = 0; index<NUMBER_OF_EFFECT_CHANNELS; index++){
            graph->GetPoint(index,chan,getAdcPerPhoton);
            //std::cout<<"index= "<<index<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
            if(getAdcPerPhoton<ADC_PER_PHOTON_UPPER_THRESHOLD) {
                if(m_readInSetupFile) {
                    if(positionIndex<P_N_DISTANCE){
                        stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+(int)chan%CHANNELS_PER_GROUP;
                    }else{
                        stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+STRIPS_PER_GROUP-((int)chan%CHANNELS_PER_GROUP)-1;
                    }
                }else{
                    //if(i%2 == 0){
                    if((i%2 == 0) || i==1){   //the first two uplink are trigger layers,not corresponded
                        stripIndex = chan-HEAD_OF_EFFECT_CHANNELS+stripBase ;
                    }else{
                        stripIndex = NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS-1-chan+stripBase;
                    }
                }

                graph_omitDamagedChannels->SetPoint(index_new,stripIndex,getAdcPerPhoton);
                //std::cout<<"index_new= "<<index_new<<",strip = "<<stripIndex<<",uplinkId = "<<m_uplinkId.at(i)<<"chan"<<chan<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
                fprintf(resultLog,"%d %d %.0f %f\n",stripIndex,m_uplinkId.at(i),chan,getAdcPerPhoton);
                index_new++;
            }
        }
        sprintf(tmp,"uplink%d",m_uplinkId.at(i));
        legend->AddEntry(graph_omitDamagedChannels,tmp, "p");

        if(i==0){
            if(m_readInSetupFile){
                hr = c1->cd(1)->DrawFrame(stripStart,8,stripEnd,20);
            }else{
                //if(m_uplinkId.size()%2 == 0) hr = c1->cd(1)->DrawFrame(0,8,(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS,15);
                if(m_uplinkId.size()%2 == 0) hr = c1->cd(1)->DrawFrame(0,8,(m_uplinkId.size()/2+1)*NUMBER_OF_EFFECT_CHANNELS,20); //the first two uplink are trigger layers,not corresponded
                else hr = c1->cd(1)->DrawFrame(0,8,(m_uplinkId.size()/2 +1)*NUMBER_OF_EFFECT_CHANNELS,15);
            }
            hr->SetXTitle("strip #");
            hr->SetYTitle("gain(ADC per Photon), ADC");
        }

        graph_omitDamagedChannels->SetMarkerStyle(20+i);
        //graph->SetMarkerColor(kRed+i);
        graph_omitDamagedChannels->SetMarkerColor(2+2*i);
        graph_omitDamagedChannels->SetMarkerSize(1.);

        graph_omitDamagedChannels->Fit("pol1","Q");
        graph_omitDamagedChannels->GetFunction("pol1")->SetLineColor(2+2*i);
        if(i==0) graph_omitDamagedChannels->Draw("PE");
        else graph_omitDamagedChannels->Draw("PE SAME");

        if(i==0){
            c1->cd(1)->SetGridx();
            c1->cd(1)->SetGridy();
        }

        legend->Draw();

    }
    fclose(resultLog);

    //draw meanInPhoton
    //std::cout<<"\n\n\ndraw meaninPhoton \n";
    c1->cd(2);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);


    resultLog = NULL;
    //open a file to log the result
    sprintf(tmp,"../data/MeanInPhoton_LED_inStrip.txt");
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----MeanInPhoton List with LED injection ( in strip ) --------\n\n");
    fprintf(resultLog,"strip\t uplink\t channel\t MeanInPhoton\t\n");

    stripBase = 1;

    for(unsigned int i=0; i<m_meanInPhoton.size();i++){

        if(i==0) stripBase = 1;

        if((i%2 == 0)&& (i!=0)){
            stripBase +=NUMBER_OF_EFFECT_CHANNELS;
        }

        graph = m_meanInPhoton.at(i);
        graph_omitDamagedChannels = new TGraphErrors(1);

        double chan=0;
        double getMeanInPhoton =0;
        int index_new = 0;
        int stripIndex = 0;

        int SuperLayerIndex = 0;
        int positionIndex = 0;
        if(m_readInSetupFile) {
            for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                    if(superLayer_iter->second==m_uplinkId.at(i)) {
                        SuperLayerIndex = LayerIndex;
                        positionIndex = superLayer_iter->first;
                        break;
                    }
                }
            }
        }

        for(int index = 0; index<NUMBER_OF_EFFECT_CHANNELS; index++){
            graph->GetPoint(index,chan,getMeanInPhoton);
            //std::cout<<"index= "<<index<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",meanInPhoton = "<<getMeanInPhoton<<std::endl;
            if(getMeanInPhoton>MEAN_IN_PHOTON_UNDER_THRESHOLD) {
                if(m_readInSetupFile) {
                    if(positionIndex<P_N_DISTANCE){
                        stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+(int)chan%CHANNELS_PER_GROUP;
                    }else{
                        stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+STRIPS_PER_GROUP-((int)chan%CHANNELS_PER_GROUP)-1;
                    }
                }else{
                    if(i%2 == 0){
                        stripIndex = chan-HEAD_OF_EFFECT_CHANNELS+stripBase ;
                    }else{
                        stripIndex = NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS-1-chan+stripBase;
                    }
                }
                graph_omitDamagedChannels->SetPoint(index_new,stripIndex,getMeanInPhoton);
                //std::cout<<"index_new= "<<index_new<<",strip = "<<stripIndex<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<chan<<",meanInPhoton = "<<getMeanInPhoton<<std::endl;
                fprintf(resultLog,"%d %d %.0f %f\n",stripIndex,m_uplinkId.at(i),chan,getMeanInPhoton);
                index_new++;

            }
        }

        sprintf(tmp,"uplink%d",m_uplinkId.at(i));
        legend->AddEntry(graph_omitDamagedChannels,tmp, "p");

        if(i==0){
            if(m_readInSetupFile){
                hr = c1->cd(2)->DrawFrame(stripStart,0,stripEnd,16);
            }else{
                if(m_uplinkId.size()%2 == 0) hr = c1->cd(2)->DrawFrame(0,0,(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS,16);
                else hr = c1->cd(2)->DrawFrame(0,0,(m_uplinkId.size()/2 +1)*NUMBER_OF_EFFECT_CHANNELS,16);
            }

            hr->SetXTitle("strip #");
            hr->SetYTitle("mean,p.e.");
        }

        graph_omitDamagedChannels->SetMarkerStyle(20+i);
        // graph->SetMarkerColor(kRed+i);
        graph_omitDamagedChannels->SetMarkerColor(2+2*i);
        graph_omitDamagedChannels->SetMarkerSize(1.);

        graph_omitDamagedChannels->Fit("pol1","Q");
        //graph->GetFunction("pol1")->SetLineColor(kRed+i);
        graph_omitDamagedChannels->GetFunction("pol1")->SetLineColor(2+2*i);

        if(i==0) graph_omitDamagedChannels->Draw("PE");
        else graph_omitDamagedChannels->Draw("PE SAME");

        if(i==0){
            c1->cd(2)->SetGridx();
            c1->cd(2)->SetGridy();
        }
    }

    legend->Draw();
    fclose(resultLog);

    //draw ratio of number of photons between left and right side of the strip
    //std::cout<<"\n\n\ndraw ratio L/R meanInPhoton \n";
    bool hasCorrelation = false;
    if(m_readInSetupFile){
        if(m_corrUplinkPairs.size()) hasCorrelation = true;
    }else{
        if(m_uplink_correlation.size()) hasCorrelation = true;
    }
    if(hasCorrelation){
        FILE* resultLog = NULL;
        c1->cd(3);
        legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
        legend->SetFillColor(kWhite);
        legend->SetBorderSize(0);
        legend->SetTextFont(132);
        legend->SetTextSize(0.06);

        if(m_readInSetupFile){
            hr = c1->cd(3)->DrawFrame(stripStart,0.3,stripEnd,2.5);
        }else{
            if(m_uplinkId.size()%2 == 0) hr = c1->cd(3)->DrawFrame(0,0.3,(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS,2.5);
            else hr = c1->cd(3)->DrawFrame(0,0.3,(m_uplinkId.size()/2 +1)*NUMBER_OF_EFFECT_CHANNELS,2.5);
        }
        sprintf(tmp,"strip # ");
        hr->SetXTitle(tmp);
        sprintf(tmp,"mean ratio, p.e.");
        hr->SetYTitle(tmp);


        Double_t corrChan = 0;
        Double_t corrMeanInPhoton = 0;
        Double_t chan_aim = 0;
        Double_t meanInPhoton_aim = 0;
        Double_t ratioLR = 0;

        int corrSum = 0;
        if(m_readInSetupFile){
            corrSum = m_corrUplinkPairs.size()/2;
        }else{
            corrSum = m_uplink_correlation.size()/NUMBER_OF_EFFECT_CHANNELS;
        }


        int uplinkID_P = 0;
        int uplinkID_N = 0;
        int uplinkIndex_P = 0;
        int uplinkIndex_N = 0;
        for(int corrIndex=0; corrIndex< corrSum ; corrIndex++){

            if(corrIndex==0) stripBase =  1;
            else stripBase += NUMBER_OF_EFFECT_CHANNELS;

            if(m_readInSetupFile){
                uplinkID_P = m_corrUplinkPairs.at(2*corrIndex);
                uplinkID_N = m_corrUplinkPairs.at(2*corrIndex+1);
                uplinkIndex_P = uplinkIndex(uplinkID_P);
                uplinkIndex_N = uplinkIndex(uplinkID_N);
            }else{
                uplinkID_P = m_uplinkId.at(2*corrIndex);
                uplinkID_N = m_uplinkId.at(2*corrIndex+1);
            }


            //open a file to log the result
            sprintf(tmp,"../data/ratioLR_U%d_U%d_inStrip.txt",uplinkID_P,uplinkID_N);
            resultLog = fopen(tmp,"w");
            fprintf(resultLog,"-----ratioLR U%d/U%d in Strip--------\n\n",uplinkID_P,uplinkID_N);
            fprintf(resultLog,"strip\t uplink_side1\t channel\t uplink_side2\t channel\t ratioLR\n");

            graph = m_meanInPhoton.at(uplinkIndex_P);
            TGraphErrors* graph2 = m_meanInPhoton.at(uplinkIndex_N);


            int SuperLayerIndex = 0;
            int positionIndex = 0;
            if(m_readInSetupFile) {
                for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                    for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                        if(superLayer_iter->second==uplinkID_P) {
                            if(superLayer_iter->first >(P_N_DISTANCE-1)){
                                std::cerr<<"the positionIndex of uplinkID_P = "<<uplinkID_P<<" is larger than 4!"<<std::endl;
                                assert(false);
                            }
                            SuperLayerIndex = LayerIndex;
                            positionIndex = superLayer_iter->first;
                            SuperLayerMap::iterator corrLayer = m_superLayerMap[LayerIndex].find(superLayer_iter->first+P_N_DISTANCE);
                            if(corrLayer==m_superLayerMap[LayerIndex].end() || corrLayer->second != uplinkID_N ){
                                std::cerr<<"the corresponding uplinkID for uplinkID_P = "<<uplinkID_P<<" is not uplinkID_N= "<<uplinkID_N <<" ,but uplinkID= "<<corrLayer->second <<".Check the setup file!"<<std::endl;
                                assert(false);
                            }
                            break;
                        }
                    }
                }
            }

            TGraphErrors* ratio =new TGraphErrors(1);  // since chan14 is dead on both cards until 20_1_2012

            Int_t ratio_index =0;
            Double_t corrNum = 2*HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1;
            int stripIndex = 0;
            for(Int_t j=0; j< NUMBER_OF_EFFECT_CHANNELS;j++){
                graph->GetPoint(j,chan_aim,meanInPhoton_aim);   //
                //std::cout<<"chan_aim = "<<chan_aim<<" on uplinkId = "<<uplinkID_P<<",meanInPhoton_aim = "<<meanInPhoton_aim<<std::endl;
                if(meanInPhoton_aim>MEAN_IN_PHOTON_UNDER_THRESHOLD ){        //change threshold
                    for(Int_t i=0; i< NUMBER_OF_EFFECT_CHANNELS;i++){
                        graph2->GetPoint(i,corrChan,corrMeanInPhoton);
                        //std::cout<<"corrChan = "<<corrChan<<" on uplinkId = "<<uplinkID_N<<",corrMeanInPhoton = "<<corrMeanInPhoton<<std::endl;
                        //getchar();
                        if(m_readInSetupFile){
                            if((int)corrChan/CHANNELS_PER_GROUP == (int)chan_aim/CHANNELS_PER_GROUP){   //if corrChan and  chan_aim belong to the same group
                                if((int)corrChan%CHANNELS_PER_GROUP == STRIPS_PER_GROUP-1-(int)chan_aim%CHANNELS_PER_GROUP){   //if the corrChan and chan_aim are corresponded
                                    if(positionIndex<P_N_DISTANCE){
                                        stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan_aim/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+(int)chan_aim%CHANNELS_PER_GROUP;
                                    }else{
                                        stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan_aim/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+STRIPS_PER_GROUP-((int)chan_aim%CHANNELS_PER_GROUP)-1;
                                    }
                                    //std::cout<<"find corrChan = "<<corrChan <<" for chan_aim = " <<chan_aim <<std::endl;
                                    ratioLR = meanInPhoton_aim/corrMeanInPhoton;   //ratio = m_meanInPhoton.at(0)/m_meaninPhoton.at(1)
                                    ratio->SetPoint(ratio_index,stripIndex,ratioLR);
                                    //std::cout<<"strip = "<<stripIndex<<" ratio: point_index = "<<ratio_index<<",chan_aim = "<<chan_aim<<" on uplinkId = "<<uplinkID_P<<",ratioLR = "<<ratioLR<<std::endl;
                                    // getchar();
                                    fprintf(resultLog,"%d %d %2.0f %d %2.0f %f\n",stripIndex,uplinkID_P,chan_aim,uplinkID_N,corrChan,ratioLR);
                                    ratio_index++;
                                    break;
                                }
                            }
                        }else{
                            if((corrChan == (corrNum-chan_aim))&&(corrMeanInPhoton>MEAN_IN_PHOTON_UNDER_THRESHOLD)){  //change threshold
                                stripIndex= chan_aim-HEAD_OF_EFFECT_CHANNELS+stripBase;
                                ratioLR = meanInPhoton_aim/corrMeanInPhoton;   //ratio = m_meanInPhoton.at(0)/m_meaninPhoton.at(1)
                                ratio->SetPoint(ratio_index,stripIndex,ratioLR);
                                //std::cout<<"strip = "<<stripIndex<<" ratio: point_index = "<<ratio_index<<",chan_aim = "<<chan_aim<<" on uplinkId = "<<uplinkID_P<<",ratioLR = "<<ratioLR<<std::endl;
                                // getchar();
                                fprintf(resultLog,"%d %d %2.0f %d %2.0f %f\n",stripIndex,uplinkID_P,chan_aim,uplinkID_N,corrChan,ratioLR);
                                ratio_index++;
                                break;
                            }
                        }
                    }
                }
            }

            fclose(resultLog);


            sprintf(tmp,"U%d/U%d",uplinkID_P,uplinkID_N);
            legend->AddEntry(ratio,tmp, "p");

            ratio->SetMarkerStyle(20+corrIndex);
            ratio->SetMarkerColor(2+2*corrIndex);
            ratio->SetMarkerSize(1.);


            ratio->Fit("pol1","Q");
            ratio->GetFunction("pol1")->SetLineColor(2+2*corrIndex);

            if(corrIndex==0) ratio->Draw("PE");
            else ratio->Draw("PE SAME");


        }

        c1->cd(3)->SetGridx();
        c1->cd(3)->SetGridy();

        legend->Draw();
    }

}


void DataManager::PlotAnalysisResult_InStrip(std::string pedestalFilePath, std::string signalFilePath){

    // if read in setup file, define the strip range to be showed
    int stripStart = 0;
    int stripEnd = 0;
    SuperLayerMap::iterator superLayer_iter;
    if(m_readInSetupFile){
        int minPosition = 16; // since for two superLayer, position can only be 0~15 ,change here if there are more superLayers
        int maxPosition = -1;
        int currPosition = 0;
        for(int uplinkIndex =0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
            int currUplinkId = m_uplinkId.at(uplinkIndex);
            for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                    if(superLayer_iter->second== currUplinkId) {
                        currPosition = superLayer_iter->first%P_N_DISTANCE + BOARDS_PER_SUPERLAYER*LayerIndex;
                        if(currPosition>maxPosition) maxPosition = currPosition;
                        if(currPosition<minPosition) minPosition = currPosition;
                        break;
                    }
                }
            }
        }
        stripStart = (minPosition/BOARDS_PER_SUPERLAYER)*STRIPS_PER_SUPERLAYER+(minPosition%BOARDS_PER_SUPERLAYER)*STRIPS_PER_GROUP;
        stripEnd = (maxPosition/BOARDS_PER_SUPERLAYER)*STRIPS_PER_SUPERLAYER +(minPosition%BOARDS_PER_SUPERLAYER)*STRIPS_PER_GROUP+(59/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+59%CHANNELS_PER_GROUP;
    }

    char tmp[256];

    sprintf(tmp," Analysis Result_inStrip\t%s\t%s",pedestalFilePath.c_str(),signalFilePath.c_str());
    TCanvas *c1 = new TCanvas(tmp,tmp,1280,1280);
    // c1->Divide(2,3);
    c1->Divide(1,4);

    gStyle->SetOptFit(1000);
    gStyle->SetLabelFont(132,"XY");
    gStyle->SetLabelSize(0.06,"XY");

    gStyle->SetTitleFont(132,"XY");
    gStyle->SetTitleSize(0.06,"XY");
    gStyle->SetTitleYOffset(0.8);
    gStyle->SetTitleXOffset(0.8);

    TGraphErrors* graph = NULL;
    TH1F *hr = NULL;
    TLegend *legend =NULL;

    //draw ADC per Photon
    c1->cd(1);

    FILE* resultLog = NULL;
    //open a file to log the result
    sprintf(tmp,"../data/GainList_cosmics_inStrip.txt");
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----Gain List with cosmics injection ( in strip ) --------\n\n");
    fprintf(resultLog,"strip\t uplink\t channel\t Gain (ADC/photon)\t\n");

    int stripBase = 1;

    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    TGraphErrors* graphInStrip =NULL;


    for(unsigned int i=0; i<m_adcPerPhoton.size();i++){

        if(i==0) stripBase = 1;

        // if((i%2 == 0)&& (i!=0)){
        if(((i%2 == 0)&& (i!=0))||i==1){  //the first two uplink are trigger layers,not corresponded
            stripBase +=NUMBER_OF_EFFECT_CHANNELS;
        }

        graph = m_adcPerPhoton.at(i);
        graphInStrip = new TGraphErrors(1);
        double chan=0;
        double getAdcPerPhoton =0;
        int index_new = 0;
        int stripIndex = 0;

        int SuperLayerIndex = 0;
        int positionIndex = 0;

        if(m_readInSetupFile) {
            for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                    if(superLayer_iter->second==m_uplinkId.at(i)) {
                        SuperLayerIndex = LayerIndex;
                        positionIndex = superLayer_iter->first;
                        break;
                    }
                }
            }
        }

        for(int index = 0; index<graph->GetN(); index++){
            graph->GetPoint(index,chan,getAdcPerPhoton);
            if(((int)chan%CHANNELS_PER_GROUP) >STRIPS_PER_GROUP) continue; // only chan0~11, 16~27,32~43,48~59 are SiPM output
            //std::cout<<"index= "<<index<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
            if(getAdcPerPhoton<ADC_PER_PHOTON_UPPER_THRESHOLD) {
                //if setupFile exists, generate the unique stripIndex for this channel
                if(m_readInSetupFile) {
                    if(positionIndex<P_N_DISTANCE){
                        stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+(int)chan%CHANNELS_PER_GROUP;
                    }else{
                        stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+STRIPS_PER_GROUP-((int)chan%CHANNELS_PER_GROUP)-1;
                    }
                }else{
                    //if(i%2 == 0){
                    if((i%2 == 0) || i==1){   //the first two uplink are trigger layers,not corresponded
                        stripIndex = chan-HEAD_OF_EFFECT_CHANNELS+stripBase ;
                    }else{
                        stripIndex = NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS-1-chan+stripBase;
                    }
                }
                graphInStrip->SetPoint(index_new,stripIndex,getAdcPerPhoton);
                //std::cout<<"index_new= "<<index_new<<",strip = "<<stripIndex<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
                fprintf(resultLog,"%d %d %.0f %f\n",stripIndex,m_uplinkId.at(i),chan,getAdcPerPhoton);
                m_map_key_strip_value_N_ADCPerPE.insert(std::map<int,double>::value_type(stripIndex,getAdcPerPhoton));
                index_new++;
            }
        }


        sprintf(tmp,"uplink%d",m_uplinkId.at(i));
        legend->AddEntry(graphInStrip,tmp, "p");
        if(i==0){
            if(m_readInSetupFile){
                hr = c1->cd(1)->DrawFrame(stripStart,10,stripEnd,14);
            }else{
                //if(m_uplinkId.size()%2 == 0) hr = c1->cd(1)->DrawFrame(0,8,(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS,15);
                if(m_uplinkId.size()%2 == 0) hr = c1->cd(1)->DrawFrame(0,8,(m_uplinkId.size()/2+1)*NUMBER_OF_EFFECT_CHANNELS,20); //the first two uplink are trigger layers,not corresponded
                else hr = c1->cd(1)->DrawFrame(0,8,(m_uplinkId.size()/2 +1)*NUMBER_OF_EFFECT_CHANNELS,15);
            }

            hr->SetXTitle("strip #");
            hr->SetYTitle("gain(ADC per Photon), ADC");
        }

        graphInStrip->SetMarkerStyle(20+i);
        graphInStrip->SetMarkerColor(2+2*i);
        graphInStrip->SetMarkerSize(1.);

        graphInStrip->Fit("pol1","Q");
        graphInStrip->GetFunction("pol1")->SetLineColor(2+2*i);
        if(i==0) graphInStrip->Draw("PE");
        else graphInStrip->Draw("PE SAME");

        if(i==0){
            c1->cd(1)->SetGridx();
            c1->cd(1)->SetGridy();
        }
    }
    legend->Draw();
    fclose(resultLog);

    //draw MPV in ADC
    c1->cd(2);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    resultLog = NULL;
    //open a file to log the result
    sprintf(tmp,"../data/MIP_inADC_cosmics_inStrip.txt");
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----MIP in ADC List with cosmics( in strip ) --------\n\n");
    fprintf(resultLog,"strip\t uplink\t channel\t MPV (in adc)\t\n");

    graphInStrip =NULL;
    double getMpvInAdc = 0;
    double chan = 0;
    int stripIndex = 0;

    for(unsigned int i=0; i<m_mpvInAdc.size();i++){

        if(i==0) stripBase = 1;

        //if((i%2 == 0)&& (i!=0)){
        if(((i%2 == 0)&& (i!=0))||i==1){  //the first two uplink are trigger layers,not corresponded
            stripBase +=NUMBER_OF_EFFECT_CHANNELS;
        }

        graph = m_mpvInAdc.at(i);
        graphInStrip = new TGraphErrors(1);


        int SuperLayerIndex = 0;
        int positionIndex = 0;
        if(m_readInSetupFile) {
            for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                    if(superLayer_iter->second==m_uplinkId.at(i)) {
                        SuperLayerIndex = LayerIndex;
                        positionIndex = superLayer_iter->first;
                        break;
                    }
                }
            }
        }

        for(int index = 0; index<graph->GetN(); index++){
            graph->GetPoint(index,chan,getMpvInAdc);
            //if setupFile exists, generate the unique stripIndex for this channel
            if(m_readInSetupFile) {
                if(positionIndex<P_N_DISTANCE){
                    stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+(int)chan%CHANNELS_PER_GROUP;
                }else{
                    stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+STRIPS_PER_GROUP-((int)chan%CHANNELS_PER_GROUP)-1;
                }
            }else{
                //if(i%2 == 0){
                if((i%2 == 0) || i==1){   //the first two uplink are trigger layers,not corresponded
                    stripIndex = chan-HEAD_OF_EFFECT_CHANNELS+stripBase ;
                }else{
                    stripIndex = NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS-1-chan+stripBase;
                }

            }
            graphInStrip->SetPoint(index,stripIndex,getMpvInAdc);
            //std::cout<<"index= "<<index<<",strip = "<<stripIndex<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",mpvInAdc = "<<getMpvInAdc<<std::endl;
            fprintf(resultLog,"%d %d %.0f %f\n",stripIndex,m_uplinkId.at(i),chan,getMpvInAdc);

        }

        sprintf(tmp,"uplink%d",m_uplinkId.at(i));
        legend->AddEntry(graphInStrip,tmp, "p");

        if(i==0){
            if(m_readInSetupFile){
                hr = c1->cd(2)->DrawFrame(stripStart,60,stripEnd,120);
            }else{
                //if(m_uplinkId.size()%2 ==0) hr = c1->cd(2)->DrawFrame(0,40,(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS,160);
                if(m_uplinkId.size()%2 ==0) hr = c1->cd(2)->DrawFrame(0,40,(m_uplinkId.size()/2+1)*NUMBER_OF_EFFECT_CHANNELS,160);//the first two uplink are trigger layers,not corresponded
                else hr = c1->cd(2)->DrawFrame(0,40,(m_uplinkId.size()/2+1)*NUMBER_OF_EFFECT_CHANNELS,160);
            }
            hr->SetXTitle("strip #");
            hr->SetYTitle("MPV, ADC");

        }

        graphInStrip->SetMarkerStyle(20+i);
        // graph->SetMarkerColor(kRed+i);
        graphInStrip->SetMarkerColor(2+2*i);
        graphInStrip->SetMarkerSize(1.);

        graphInStrip->Fit("pol1","Q");
        graphInStrip->GetFunction("pol1")->SetLineColor(2+2*i);

        if(i==0) graphInStrip->Draw("PE");
        else graphInStrip->Draw("PE SAME");

        if(i==0){
            c1->cd(2)->SetGridx();
            c1->cd(2)->SetGridy();
        }
    }
    legend->Draw();
    fclose(resultLog);

    //draw resultCorr : MPV in photon electron
    c1->cd(3);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC");  //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    resultLog = NULL;
    //open a file to log the result
    sprintf(tmp,"../data/MIP_inPE_cosmics_inStrip.txt");
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----MIP in p.e. List with cosmics ( in strip ) --------\n\n");
    fprintf(resultLog,"strip\t uplink\t channel\t MPV in p.e.\t\n");

    graphInStrip =NULL;
    double getMpvInPE = 0;
    chan = 0;
    stripIndex = 0;

    for(unsigned int i=0; i<m_resultCorr.size();i++){

        if(i==0) stripBase = 1;
        //if((i%2 == 0)&& (i!=0)){
        if(((i%2 == 0)&& (i!=0))||i==1){  //the first two uplink are trigger layers,not corresponded
            stripBase +=NUMBER_OF_EFFECT_CHANNELS;
        }

        graph = m_resultCorr.at(i);
        graphInStrip = new TGraphErrors(1);

        int SuperLayerIndex = 0;
        int positionIndex = 0;
        if(m_readInSetupFile) {
            for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                    if(superLayer_iter->second==m_uplinkId.at(i)) {
                        SuperLayerIndex = LayerIndex;
                        positionIndex = superLayer_iter->first;
                        break;
                    }
                }
            }
        }

        for(int index = 0; index<graph->GetN(); index++){
            graph->GetPoint(index,chan,getMpvInPE);

            //if setupFile exists, generate the unique stripIndex for this channel
            if(m_readInSetupFile) {
                if(positionIndex<P_N_DISTANCE){
                    stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+(int)chan%CHANNELS_PER_GROUP;
                }else{
                    stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+STRIPS_PER_GROUP-((int)chan%CHANNELS_PER_GROUP)-1;
                }
            }else{
                //if(i%2 == 0){
                if((i%2 == 0) || i==1){   //the first two uplink are trigger layers,not corresponded
                    stripIndex = chan-HEAD_OF_EFFECT_CHANNELS+stripBase ;
                }else{
                    stripIndex = NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS-1-chan+stripBase;
                }
            }

            graphInStrip->SetPoint(index,stripIndex,getMpvInPE);
            //std::cout<<"index= "<<index<<",strip = "<<stripIndex<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",mpvInPE = "<<getMpvInPE<<std::endl;
            fprintf(resultLog,"%d %d %.0f %f\n",stripIndex,m_uplinkId.at(i),chan,getMpvInPE);
        }

        sprintf(tmp,"uplink%d",m_uplinkId.at(i));
        legend->AddEntry(graphInStrip,tmp, "p");

        if(i==0){
            if(m_readInSetupFile){
                hr = c1->cd(3)->DrawFrame(stripStart,6,stripEnd,8.5);
            }else{
                //if(m_uplinkId.size()%2 ==0) hr = c1->cd(3)->DrawFrame(0,0,(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS,15);
                if(m_uplinkId.size()%2 ==0) hr = c1->cd(3)->DrawFrame(0,0,(m_uplinkId.size()/2+1)*NUMBER_OF_EFFECT_CHANNELS,15);//the first two uplink are trigger layers,not corresponded
                else hr = c1->cd(3)->DrawFrame(0,0,(m_uplinkId.size()/2+1)*NUMBER_OF_EFFECT_CHANNELS,15);
            }
            hr->SetXTitle("strip #");
            hr->SetYTitle("MPV, p.e.");
        }
        graphInStrip->SetMarkerStyle(20+i);
        graphInStrip->SetMarkerColor(2+2*i);
        graphInStrip->SetMarkerSize(1.);

        graphInStrip->Fit("pol1","Q");
        graphInStrip->GetFunction("pol1")->SetLineColor(2+2*i);
        if(i==0) graphInStrip->Draw("PE");
        else graphInStrip->Draw("PE SAME");

        if(i==0){
            c1->cd(3)->SetGridx();
            c1->cd(3)->SetGridy();
        }
    }
    legend->Draw();
    fclose(resultLog);

    //draw area:
    c1->cd(4);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC");  //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    resultLog = NULL;
    //open a file to log the result
    sprintf(tmp,"../data/area_cosmics_inStrip.txt");
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----area List with cosmics ( in strip ) --------\n\n");
    fprintf(resultLog,"strip\t uplink\t channel\t area_for_meanOfMIPDistribution\t\n");

    graphInStrip =NULL;
    double getArea = 0;
    stripIndex = 0;

    for(unsigned int i=0; i<m_area.size();i++){

        if(i==0) stripBase = 1;
        //if((i%2 == 0)&& (i!=0)){
        if(((i%2 == 0)&& (i!=0))||i==1){  //the first two uplink are trigger layers,not corresponded
            stripBase +=NUMBER_OF_EFFECT_CHANNELS;
        }

        hr = m_area.at(i);
        //TH1F* hrInStrip = new TH1F(tmp,"area_for_meanOfMIPDistribution;Strip;Area",NUMBER_OF_EFFECT_CHANNELS,stripBase,stripBase+NUMBER_OF_EFFECT_CHANNELS);
        TH1F* hrInStrip =NULL;
        if(m_readInSetupFile){
            hrInStrip = new TH1F(tmp,"area_for_meanOfMIPDistribution;Strip;Area",stripEnd-stripStart,stripStart,stripEnd);
        }else{
            //if(m_uplinkId.size()%2 ==0) hrInStrip = new TH1F(tmp,"area_for_meanOfMIPDistribution;Strip;Area",(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS,1,(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS+1);
            if(m_uplinkId.size()%2 ==0) hrInStrip = new TH1F(tmp,"area_for_meanOfMIPDistribution;Strip;Area",(m_uplinkId.size()/2+1)*NUMBER_OF_EFFECT_CHANNELS,1,(m_uplinkId.size()/2+1)*NUMBER_OF_EFFECT_CHANNELS+1);//the first two uplink are trigger layers,not corresponded
            else hrInStrip = new TH1F(tmp,"area_for_meanOfMIPDistribution;Strip;Area",(m_uplinkId.size()/2 +1)*NUMBER_OF_EFFECT_CHANNELS,1,(m_uplinkId.size()/2 +1)*NUMBER_OF_EFFECT_CHANNELS+1);
        }

        int SuperLayerIndex = 0;
        int positionIndex = 0;
        if(m_readInSetupFile) {
            for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                    if(superLayer_iter->second==m_uplinkId.at(i)) {
                        SuperLayerIndex = LayerIndex;
                        positionIndex = superLayer_iter->first;
                        break;
                    }
                }
            }
        }

        for(int readInChan = HEAD_OF_EFFECT_CHANNELS; readInChan<(HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS); readInChan++){
            getArea = hr->GetBinContent(readInChan-HEAD_OF_EFFECT_CHANNELS+1);

            if(m_readInSetupFile){
                if(positionIndex<P_N_DISTANCE){
                    stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)readInChan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+(int)readInChan%CHANNELS_PER_GROUP;
                }else{
                    stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)readInChan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+STRIPS_PER_GROUP-((int)readInChan%CHANNELS_PER_GROUP)-1;
                }
            }else{
                //if(i%2 == 0){
                if((i%2 == 0) || i==1){   //the first two uplink are trigger layers,not corresponded
                    stripIndex = readInChan-HEAD_OF_EFFECT_CHANNELS+stripBase ;
                }else{
                    stripIndex = NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS-1-readInChan+stripBase;
                }
            }
            hrInStrip->Fill(stripIndex,getArea);
            //std::cout<<"strip = "<<stripIndex<<",chan = "<<readInChan<<",uplinkId = "<<m_uplinkId.at(i)<<",area = "<<getArea<<std::endl;
            fprintf(resultLog,"%d %d %.0f %f\n",stripIndex,m_uplinkId.at(i),chan,getArea);
        }


        sprintf(tmp,"uplink%d",m_uplinkId.at(i));
        legend->AddEntry(hrInStrip,tmp, "L");

        //hr->SetLineColor(kRed+i);
        hrInStrip->SetLineColor(2+2*i);
        hrInStrip->SetLineWidth(3);
        if(i==0) hrInStrip->Draw();
        else hrInStrip->Draw("SAME");

        if(i==0){
            c1->cd(4)->SetGridx();
            c1->cd(4)->SetGridy();
        }
    }
    legend->Draw();
    fclose(resultLog);
}


void DataManager::Fill2LayerCorrelation(MergedEvent* event){
    //  m_uplinkId.at(0) ---->Trigger layer side 1
    //  m_uplinkId.at(1) ---->Trigger layer side 2

    //  m_uplinkId.at(2) ---->Signal layer side 1
    //  m_uplinkId.at(3) ---->Signal layer side 2


    if(m_uplinkId.size() == 4) {     //only when there are 4 uplinks, make this correlation

        unsigned short stripIndex_triggerLayer = 0;
        unsigned short stripIndex_signalLayer = 0;


        // find the channel which has the maximum ADC value (pedestal substracted) on trigger layer part 1
        const UplinkData& TriggerUplink1 = event->uplinkData(m_uplinkId.at(0));
        unsigned short max_ADC_triggerU1 = 0;
        unsigned short max_chan_triggerU1 = 0 ;

        for(int chanIndex = HEAD_OF_EFFECT_CHANNELS; chanIndex < (HEAD_OF_EFFECT_CHANNELS + NUMBER_OF_EFFECT_CHANNELS); chanIndex++){
            unsigned short ADC_subPedestal = 0;
            if(TriggerUplink1.adcValue(chanIndex)>(m_pedestal_Mean.at(0)[chanIndex]+COSMIC_THRESHOLD))
                ADC_subPedestal = TriggerUplink1.adcValue(chanIndex)-m_pedestal_Mean.at(0)[chanIndex];
            //printf("uplinkId = %d, TriggerUplink1.adcValue(%d)=%d,m_pedestal_Mean.at(0)[%d]= %f \n",m_uplinkId.at(0),chanIndex,TriggerUplink1.adcValue(chanIndex),chanIndex,m_pedestal_Mean.at(0)[chanIndex]);

            if(ADC_subPedestal > max_ADC_triggerU1) {
                max_ADC_triggerU1 = ADC_subPedestal;
                max_chan_triggerU1 =  chanIndex;
            }
            //printf("max_ADC_triggerU1 = %d,max_chan_triggerU1 = %d\n",max_ADC_triggerU1,max_chan_triggerU1);

        }
        //printf("max_ADC_triggerU1 = %d,max_chan_triggerU1 = %d\n",max_ADC_triggerU1,max_chan_triggerU1);

        // find the channel which has the maximum ADC value (pedestal substracted) on trigger layer part 2
        const UplinkData& TriggerUplink2 = event->uplinkData(m_uplinkId.at(1));
        unsigned short max_ADC_triggerU2 = 0;
        unsigned short max_chan_triggerU2 = 0 ;

        for(int chanIndex = HEAD_OF_EFFECT_CHANNELS; chanIndex < (HEAD_OF_EFFECT_CHANNELS + NUMBER_OF_EFFECT_CHANNELS); chanIndex++){
            unsigned short ADC_subPedestal = 0;
            if(TriggerUplink2.adcValue(chanIndex)>(m_pedestal_Mean.at(1)[chanIndex]+COSMIC_THRESHOLD))
                ADC_subPedestal = TriggerUplink2.adcValue(chanIndex)-m_pedestal_Mean.at(1)[chanIndex];

            //printf("uplinkId = %d, TriggerUplink2.adcValue(%d)=%d,m_pedestal_Mean.at(1)[%d]= %f \n",m_uplinkId.at(1),chanIndex,TriggerUplink2.adcValue(chanIndex),chanIndex,m_pedestal_Mean.at(1)[chanIndex]);

            if(ADC_subPedestal > max_ADC_triggerU2) {
                max_ADC_triggerU2 = ADC_subPedestal;
                max_chan_triggerU2 =  chanIndex;
            }
            //printf("max_ADC_triggerU2 = %d,max_chan_triggerU2 = %d\n",max_ADC_triggerU2,max_chan_triggerU2);
        }
        //printf("max_ADC_triggerU2 = %d,max_chan_triggerU2 = %d\n",max_ADC_triggerU2,max_chan_triggerU2);

        // if((max_ADC_triggerU1>COSMIC_THRESHOLD) || (max_ADC_triggerU2>COSMIC_THRESHOLD)){
        //printf("uplinkId = %d,max_ADC_triggerU1 = %d,max_chan_triggerU1 = %d\n",m_uplinkId.at(0),max_ADC_triggerU1,max_chan_triggerU1);
        //printf("uplinkId = %d,max_ADC_triggerU2 = %d,max_chan_triggerU2 = %d\n",m_uplinkId.at(1),max_ADC_triggerU2,max_chan_triggerU2);
        //getchar();
        if(((max_ADC_triggerU1>COSMIC_THRESHOLD) && (max_ADC_triggerU2<COSMIC_THRESHOLD)) ||((max_ADC_triggerU1<COSMIC_THRESHOLD) && (max_ADC_triggerU2>COSMIC_THRESHOLD))) {

            if(max_ADC_triggerU1 > max_ADC_triggerU2) {
                stripIndex_triggerLayer =  max_chan_triggerU1 - HEAD_OF_EFFECT_CHANNELS +STRIP_BASE_TRIGGERLAYER_PART1;
            }else{
                stripIndex_triggerLayer = max_chan_triggerU2 - HEAD_OF_EFFECT_CHANNELS +STRIP_BASE_TRIGGERLAYER_PART2;
            }


            // find the channel which has the maximum ADC value (pedestal substracted) on signal layer side 1
            const UplinkData& SignalUplink1 = event->uplinkData(m_uplinkId.at(2));
            unsigned short max_ADC_signalU1 = 0;
            unsigned short max_chan_signalU1 = 0 ;

            for(int chanIndex = 0; chanIndex<VATA64_READOUT_CHANNELS;chanIndex++){
                m_signalLayerSide1_chan->Fill(chanIndex,SignalUplink1.adcValue(chanIndex)-m_pedestal_Mean.at(2)[chanIndex]);

                if((chanIndex>HEAD_OF_EFFECT_CHANNELS-1) && (chanIndex<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS)){
                    unsigned short ADC_subPedestal = 0;
                    if(SignalUplink1.adcValue(chanIndex)>(m_pedestal_Mean.at(2)[chanIndex]))
                        ADC_subPedestal = SignalUplink1.adcValue(chanIndex)-m_pedestal_Mean.at(2)[chanIndex];
                    // printf("uplinkId = %d, SignalUplink1.adcValue(%d)=%d,m_pedestal_Mean.at(2)[%d]= %f \n",m_uplinkId.at(2),chanIndex,SignalUplink1.adcValue(chanIndex),chanIndex,m_pedestal_Mean.at(2)[chanIndex]);
                    if(ADC_subPedestal > max_ADC_signalU1) {
                        max_ADC_signalU1 = ADC_subPedestal;
                        max_chan_signalU1 =  chanIndex;
                    }
                    //printf("max_ADC_signalU1 = %d,max_chan_signalU1 = %d\n",max_ADC_signalU1,max_chan_signalU1);

                    //fill m_m_signalLayerSide1_strip
                    m_signalLayerSide1_strip->Fill(chanIndex-HEAD_OF_EFFECT_CHANNELS+STRIP_BASE_SIGNALLAYER,SignalUplink1.adcValue(chanIndex)-m_pedestal_Mean.at(2)[chanIndex]);
                }
            }
            //printf("max_ADC_signalU1 = %d,max_chan_signalU1 = %d\n",max_ADC_signalU1,max_chan_signalU1);



            // find the channel which has the maximum ADC value (pedestal substracted) on signal layer side 2
            const UplinkData& SignalUplink2 = event->uplinkData(m_uplinkId.at(3));
            unsigned short max_ADC_signalU2 = 0;
            unsigned short max_chan_signalU2 = 0 ;

            for(int chanIndex = 0; chanIndex<VATA64_READOUT_CHANNELS;chanIndex++){
                m_signalLayerSide2_chan->Fill(chanIndex,SignalUplink2.adcValue(chanIndex)-m_pedestal_Mean.at(3)[chanIndex]);

                if((chanIndex>HEAD_OF_EFFECT_CHANNELS-1) && (chanIndex<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS)){
                    unsigned short ADC_subPedestal = 0;
                    if(SignalUplink2.adcValue(chanIndex)>(m_pedestal_Mean.at(3)[chanIndex]))
                        ADC_subPedestal = SignalUplink2.adcValue(chanIndex)-m_pedestal_Mean.at(3)[chanIndex];
                    //printf("uplinkId = %d,SignalUplink2.adcValue(%d)=%d,m_pedestal_Mean.at(3)[%d]= %f \n",m_uplinkId.at(3),chanIndex,SignalUplink2.adcValue(chanIndex),chanIndex,m_pedestal_Mean.at(3)[chanIndex]);

                    if(ADC_subPedestal > max_ADC_signalU2) {
                        max_ADC_signalU2 = ADC_subPedestal;
                        max_chan_signalU2 =  chanIndex;
                    }
                    //printf("max_ADC_signalU2 = %d,max_chan_signalU2 = %d\n",max_ADC_signalU2,max_chan_signalU2);

                    //fill m_m_signalLayerSide2_strip
                    m_signalLayerSide2_strip->Fill(HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1-chanIndex+STRIP_BASE_SIGNALLAYER,SignalUplink2.adcValue(chanIndex)-m_pedestal_Mean.at(3)[chanIndex]);
                }
            }
            // printf("max_ADC_signalU2 = %d,max_chan_signalU2 = %d\n",max_ADC_signalU2,max_chan_signalU2);



            if((max_ADC_signalU1!=0) && (max_ADC_signalU2!=0)){
                //printf("uplinkId = %d,max_ADC_signalU1 = %d,max_chan_signalU1 = %d\n",m_uplinkId.at(2),max_ADC_signalU1,max_chan_signalU1);
                //printf("uplinkId = %d,max_ADC_signalU2 = %d,max_chan_signalU2 = %d\n",m_uplinkId.at(3),max_ADC_signalU2,max_chan_signalU2);
                //getchar();
            }

            if(max_chan_signalU1 == 2*HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1-max_chan_signalU2) {
                stripIndex_signalLayer = max_chan_signalU1 - HEAD_OF_EFFECT_CHANNELS +STRIP_BASE_SIGNALLAYER;
                m_2layerCorrHisto1->Fill(stripIndex_triggerLayer,stripIndex_signalLayer,max_ADC_signalU1);
                m_2layerCorrHisto2->Fill(stripIndex_triggerLayer,stripIndex_signalLayer,max_ADC_signalU2);
                //printf("fill layerCorrHisto\n");

                //fill a histogram for signalLayer1
                for(int chanIndex = 0; chanIndex<VATA64_READOUT_CHANNELS;chanIndex++){
                    m_s1Histo_chan->Fill(chanIndex,SignalUplink1.adcValue(chanIndex)-m_pedestal_Mean.at(2)[chanIndex]);
                    if((chanIndex>HEAD_OF_EFFECT_CHANNELS-1) && (chanIndex<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS)){
                        //fill m_m_signalLayerSide2_strip
                        m_s1Histo_strip->Fill(chanIndex-HEAD_OF_EFFECT_CHANNELS+STRIP_BASE_SIGNALLAYER,SignalUplink1.adcValue(chanIndex)-m_pedestal_Mean.at(2)[chanIndex]);
                    }
                }

                //fill a histogram for signalLayer2
                for(int chanIndex = 0; chanIndex<VATA64_READOUT_CHANNELS;chanIndex++){
                    m_s2Histo_chan->Fill(chanIndex,SignalUplink2.adcValue(chanIndex)-m_pedestal_Mean.at(3)[chanIndex]);
                    if((chanIndex>HEAD_OF_EFFECT_CHANNELS-1) && (chanIndex<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS)){
                        //fill m_m_signalLayerSide2_strip
                        m_s2Histo_strip->Fill(HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1-chanIndex+STRIP_BASE_SIGNALLAYER,SignalUplink2.adcValue(chanIndex)-m_pedestal_Mean.at(3)[chanIndex]);
                    }
                }

            }

        }
        // printf("finishe 1 event!\n Press any key to continue...\n");
        //getchar();
    }

}

void DataManager::Plot2LayerCorr(){

    gStyle->SetPalette(1);
    char tmp[256];

    sprintf(tmp, "LayerCorr_triggerU%d_U%d_signalU%d_U%d",m_uplinkId.at(0),m_uplinkId.at(1),m_uplinkId.at(2),m_uplinkId.at(3));
    TCanvas* canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(1,2);

    canvas->cd(1);
    m_2layerCorrHisto1->Draw("lego");


    canvas->cd(2);
    m_2layerCorrHisto2->Draw("lego");

    //-----------------------------------
    sprintf(tmp, "histo_signalLayerSide_strip_U%d_U%d",m_uplinkId.at(2),m_uplinkId.at(3));
    canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(1,2);

    canvas->cd(1);
    double mean = m_signalLayerSide1_strip->GetMean(2);
    double rms = m_signalLayerSide1_strip->GetRMS(2);
    m_signalLayerSide1_strip->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
    canvas->GetPad(1)->SetLogz();
    m_signalLayerSide1_strip->Draw("COLZ");

    canvas->cd(2);
    mean = m_signalLayerSide2_strip->GetMean(2);
    rms = m_signalLayerSide2_strip->GetRMS(2);
    m_signalLayerSide2_strip->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
    canvas->GetPad(2)->SetLogz();
    m_signalLayerSide2_strip->Draw("COLZ");

    //-----------------------------------
    sprintf(tmp, "histo_signalLayerSide_allChannel_U%d_U%d",m_uplinkId.at(2),m_uplinkId.at(3));
    canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(1,2);

    canvas->cd(1);
    mean = m_signalLayerSide1_chan->GetMean(2);
    rms = m_signalLayerSide1_chan->GetRMS(2);
    m_signalLayerSide1_chan->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
    canvas->GetPad(1)->SetLogz();
    m_signalLayerSide1_chan->Draw("COLZ");

    canvas->cd(2);
    mean = m_signalLayerSide2_chan->GetMean(2);
    rms = m_signalLayerSide2_chan->GetRMS(2);
    m_signalLayerSide2_chan->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
    canvas->GetPad(2)->SetLogz();
    m_signalLayerSide2_chan->Draw("COLZ");

    //-----------------------------------
    sprintf(tmp, "Filtered_signalLayer_histo_strip_U%d_U%d",m_uplinkId.at(2),m_uplinkId.at(3));
    canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(1,2);

    canvas->cd(1);
    mean = m_s1Histo_strip->GetMean(2);
    rms = m_s1Histo_strip->GetRMS(2);
    m_s1Histo_strip->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
    canvas->GetPad(1)->SetLogz();
    m_s1Histo_strip->Draw("COLZ");

    canvas->cd(2);
    mean = m_s2Histo_strip->GetMean(2);
    rms = m_s2Histo_strip->GetRMS(2);
    m_s2Histo_strip->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
    canvas->GetPad(2)->SetLogz();
    m_s2Histo_strip->Draw("COLZ");

    //-----------------------------------
    sprintf(tmp, "Filtered_signalLayerSide_histo_allChannel_U%d_U%d",m_uplinkId.at(2),m_uplinkId.at(3));
    canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(1,2);

    canvas->cd(1);
    mean = m_s1Histo_chan->GetMean(2);
    rms = m_s1Histo_chan->GetRMS(2);
    m_s1Histo_chan->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
    canvas->GetPad(1)->SetLogz();
    m_s1Histo_chan->Draw("COLZ");

    canvas->cd(2);
    mean = m_s2Histo_chan->GetMean(2);
    rms = m_s2Histo_chan->GetRMS(2);
    m_s2Histo_chan->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
    canvas->GetPad(2)->SetLogz();
    m_s2Histo_chan->Draw("COLZ");

    //-------------draw projectionY of m_signalLayerSide_chan and m_signalLayerSide_strip-----
    sprintf(tmp, "projectionY_signalLayerSide1_chan_U%d",m_uplinkId.at(2));
    canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(4,3);
    TH1 *hE = NULL;
    for(int chan = HEAD_OF_EFFECT_CHANNELS; chan<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;chan++){
        canvas->cd(chan-HEAD_OF_EFFECT_CHANNELS+1);
        gPad->SetLogy();
        sprintf(tmp,"U%d_chan%d_s1",m_uplinkId.at(2),chan);
        hE = m_signalLayerSide1_chan->ProjectionY(tmp,chan+1,chan+1,"");
        double mean = hE->GetMean();
        double rms = hE->GetRMS();
        hE->SetAxisRange(-50,mean+5*rms);
        hE->DrawCopy();
        canvas->Update();
    }

    sprintf(tmp, "projectionY_signalLayerSide2_chan_U%d",m_uplinkId.at(3));
    canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(4,3);
    hE = NULL;
    for(int chan = HEAD_OF_EFFECT_CHANNELS; chan<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;chan++){
        canvas->cd(chan-HEAD_OF_EFFECT_CHANNELS+1);
        gPad->SetLogy();
        sprintf(tmp,"U%d_chan%d_s2",m_uplinkId.at(3),chan);
        hE = m_signalLayerSide2_chan->ProjectionY(tmp,chan+1,chan+1,"");
        double mean = hE->GetMean();
        double rms = hE->GetRMS();
        hE->SetAxisRange(-50,mean+5*rms);
        hE->DrawCopy();
        canvas->Update();
    }

    sprintf(tmp, "projectionY_signalLayerSide1_strip_U%d",m_uplinkId.at(2));
    canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(4,3);
    hE = NULL;
    for(int strip = STRIP_BASE_SIGNALLAYER; strip<STRIP_BASE_SIGNALLAYER+NUMBER_OF_EFFECT_CHANNELS;strip++){
        canvas->cd(strip-STRIP_BASE_SIGNALLAYER+1);
        gPad->SetLogy();
        sprintf(tmp,"U%d_strip%d_s1",m_uplinkId.at(2),strip);
        hE = m_signalLayerSide1_strip->ProjectionY(tmp,strip-STRIP_BASE_SIGNALLAYER+1,strip-STRIP_BASE_SIGNALLAYER+1,"");
        double mean = hE->GetMean();
        double rms = hE->GetRMS();
        hE->SetAxisRange(-50,mean+5*rms);
        hE->DrawCopy();
        canvas->Update();
    }

    sprintf(tmp, "projectionY_signalLayerSide2_strip_U%d",m_uplinkId.at(3));
    canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(4,3);
    hE = NULL;
    for(int strip = STRIP_BASE_SIGNALLAYER; strip<STRIP_BASE_SIGNALLAYER+NUMBER_OF_EFFECT_CHANNELS;strip++){
        canvas->cd(strip-STRIP_BASE_SIGNALLAYER+1);
        gPad->SetLogy();
        sprintf(tmp,"U%d_strip%d_s2",m_uplinkId.at(3),strip);
        hE = m_signalLayerSide1_strip->ProjectionY(tmp,strip-STRIP_BASE_SIGNALLAYER+1,strip-STRIP_BASE_SIGNALLAYER+1,"");
        double mean = hE->GetMean();
        double rms = hE->GetRMS();
        hE->SetAxisRange(-50,mean+5*rms);
        hE->DrawCopy();
        canvas->Update();
    }
}


void DataManager::SaveAllHistos(bool arg_analyzeLed, bool arg_analyzeCosmics, bool arg_convert, bool arg_findMuonsTracks){
    TFile* recordFile =  new TFile("../data/lect_uplinks_result.root","RECREATE");
    if (arg_convert||arg_findMuonsTracks) {
        tree1->Print();
        tree1->Write();
    }

    if (arg_findMuonsTracks) {
        for(unsigned int i=0;i<NUMBER_OF_LAYER;i++){
            m_TH3_spectra_P[i]->Write();
            m_TH3_spectra_N[i]->Write();
        }
    }

    for(unsigned int i=0;i<m_uplinkId.size();++i){
        m_raw_pedestals[i]->Write();
        m_adcHistogram[i]->Write();
        m_PS_adcHistogram[i]->Write();
        m_PS_pedestals[i]->Write();
    }

    if(m_readInSetupFile){
        std::map<int,TH2*>::iterator stripCorrelation_iter;
        for(stripCorrelation_iter = m_strip_correlation.begin();stripCorrelation_iter!=m_strip_correlation.end();stripCorrelation_iter++){
            stripCorrelation_iter->second->Write();
        }
    }else{
        for (unsigned int i=0;i< m_uplink_correlation.size();i++){
            m_uplink_correlation[i]->Write();
        }
    }

    if (arg_analyzeLed) {
        for(unsigned int i=0; i< m_adcPerPhoton.size();i++){
            m_adcPerPhoton[i]->Write();
        }
    }

    if (arg_analyzeCosmics) {
        for(unsigned int i=0; i< m_adcPerPhoton.size();i++){
            m_adcPerPhoton[i]->Write();
        }

        for(unsigned int i=0; i<m_mpvInAdc.size();i++){
            m_mpvInAdc[i]->Write();
        }

        for(unsigned int i=0; i<m_lSigma.size();i++){
            m_lSigma[i]->Write();
        }

        for(unsigned int i=0; i<m_gSigma.size();i++){
            m_gSigma[i]->Write();
        }

        for(unsigned int i=0; i<m_resultCorr.size();i++){
            m_resultCorr[i]->Write();
        }

        for(unsigned int i=0; i<m_area.size();i++){
            m_area[i]->Write();
        }

        for(unsigned int i=0; i<m_meanInPhoton.size();i++){
            m_meanInPhoton[i]->Write();
        }
    }
    recordFile->Close();
}


Double_t fconvo_func(Double_t *x, Double_t *par) {

    //Fit parameters:
    //par[0]=Width (scale) parameter of Landau density
    //par[1]=Most Probable (MP, location) parameter of Landau density
    //par[2]=normalization for the signal
    //par[3]=readout gain (#ADC/firing cell), fixed parameter
    //par[4]=normalization for the background
    //In the Landau distribution (represented by the CERNLIB approximation),
    //the maximum of the landau is par[1] = mpc - 0.22278298*par[0] (where mpc is the mpv parameter of the landau)
    //This shift is corrected within this function, so that the actual maximum
    //is identical to the MP parameter, par[1].
    //Since we do a convolution with a Poisson distribution,
    //the real maximum of the spectrum is slightly higher than par[1]

    // Numeric constants
    Double_t mpshift  = -0.22278298;       // Landau maximum location

    // Control constants
    Double_t np = 100.0;      // number of convolution steps
    Double_t sc =   5.0;      // convolution extends to +-sc Gaussian sigmas

    // Variables
    Double_t xx;
    Double_t mpc;
    Double_t fland;
    Double_t sum = 0.0;
    Double_t xlow,xupp;
    Double_t step;
    Double_t i;

    // MP shift correction
    mpc = par[1] - mpshift * par[0];

    // Range of convolution integral
    if (x[0]>0) {
        xlow = x[0] - sc * TMath::Sqrt(x[0] / par[3]) * par[3];
        xupp = x[0] + sc * TMath::Sqrt(x[0] / par[3]) * par[3];
    }
    if ((xlow>0)||(xlow==0)) step = (xupp-xlow) / np;
    else step = xupp / np;

    // Convolution integral of Landau and Gaussian by sum
    if ((xlow>0)&&(x[0]>0)) {
        for(i=1.0; i<=np/2; i++) {
            xx = xlow + (i-.5) * step;
            fland = TMath::Landau(xx,mpc,par[0]) / par[0];
            sum += fland * TMath::Poisson(x[0]/par[3],xx/par[3]);

            xx = xupp - (i-.5) * step;
            fland = TMath::Landau(xx,mpc,par[0]) / par[0];
            sum += fland * TMath::Poisson(x[0]/par[3],xx/par[3]);
        }
    }
    if (((xlow<0)||(xlow==0))&&(x[0]>0)) {
        for(i=1.0; i<=np; i++) {
            xx = 0 + (i-.5) * step;
            fland = TMath::Landau(xx,mpc,par[0]) / par[0];
            sum += fland * TMath::Poisson(x[0]/par[3],xx/par[3]);
        }
    }
    if (x[0]<0) sum = 0;

    Double_t sr = par[2] * step * sum;
    Double_t xxx=x[0];
    Int_t bin = background->GetXaxis()->FindBin(xxx);
    Double_t br = par[4]*background->GetBinContent(bin);

    return (sr + br);
}



void DataManager::MPVinADC(int uplinkId,Int_t rebin,bool plot){

    //set TCanvas
    //gStyle->SetOptFit(0000);

    TH2 *PS_adcHistogram_uplink = m_PS_adcHistogram.at(uplinkIndex(uplinkId));
    TH2 *PS_pedestals_uplink = m_PS_pedestals.at(uplinkIndex(uplinkId));

    //printf("analyze m_PS_adcHistogram.at(%d)_uplinkId%d \n",uplinkIndex(uplinkId),uplinkId);
    //printf("analyze m_PS_pedestals.at(%d)_uplinkId%d \n",uplinkIndex(uplinkId),uplinkId);

    char tmp[256];
    TCanvas *c1 = NULL;   //TCanvas for ADCperPhoton
    TH1 *hE = NULL;

    printf("for UplinkId = %d, find MPV in ADC\n",uplinkId);
    FILE* resultLog = NULL;
    //open a file to log the result
    sprintf(tmp,"../data/uplink%d_MPV.txt",uplinkId);
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----find MPV in ADC --------\n\n");
    fprintf(resultLog,"Uplink\t channels\t fconvo maximumX\t fconvo_s maximumX\t fconvo_s mean\t flandau mean\t mpv\t chi2/n\t prob\n");

    //gStyle->SetOptFit();

    TGraphErrors* mpvInAdc = m_mpvInAdc.at(uplinkIndex(uplinkId));

    // TGraphErrors* lSigma = m_lSigma.at(uplinkIndex(uplinkId));

    // TGraphErrors* gSigma = m_gSigma.at(uplinkIndex(uplinkId));

    TH1F* area = m_area.at(uplinkIndex(uplinkId));

    //Int_t rebin=12;
    //Int_t xmin=35; //25 at 78.3V, 35 at 78.8V, 45 at 79.3V
    // Int_t xmin=40;

    int xPadNum = 4;
    int yPadNum = 3;
    int padPerCanvas = xPadNum*yPadNum;

    int canvasNum = 0;
    if(NUMBER_OF_EFFECT_CHANNELS%padPerCanvas == 0)    canvasNum = NUMBER_OF_EFFECT_CHANNELS/padPerCanvas;
    else    canvasNum = NUMBER_OF_EFFECT_CHANNELS/padPerCanvas+1;
    int canvasIndex = 0;

    Int_t graph_index = 0;

    int picIndex = 0;
    for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;chan++){
        if(m_readInSetupFile){
            if((chan%CHANNELS_PER_GROUP)>(STRIPS_PER_GROUP-1)) continue;
        }

        if((picIndex%padPerCanvas) == 0){
            if(m_readInSetupFile){
                sprintf(tmp,"uplink%d_MPV_in_ADC (%d)",uplinkId,picIndex/padPerCanvas);
            }else{
                if(canvasNum==1) sprintf(tmp,"uplink%d_MPV_in_ADC",uplinkId);
                else  sprintf(tmp,"uplink%d_MPV_in_ADC (%d)",uplinkId,canvasIndex+1);
            }

            c1 = new TCanvas(tmp,tmp,10,10,1000,900);
            c1->Divide(xPadNum,yPadNum);
            canvasIndex++;
            //std::cout<<"create a new canvas: now picIndex = "<<picIndex<<" chan = "<<chan<<std::endl;
            //getchar();
        }

        //Double_t mean,rms;
        //int rebin_undo = 1; //better not rebin it

        //c1->cd((chan-HEAD_OF_EFFECT_CHANNELS)%padPerCanvas+1);
        c1->cd(picIndex%padPerCanvas+1);
        picIndex++;
        c1->GetPad((chan-HEAD_OF_EFFECT_CHANNELS)%padPerCanvas+1)->SetLogy();

        sprintf(tmp,"U%d_chan%d",uplinkId,chan);
        hE = PS_adcHistogram_uplink->ProjectionY(tmp,chan+1,chan+1,"");
        //double mean = hE->GetMean();
        double rms = hE->GetRMS();
        // Int_t bin = hE->GetXaxis()->FindBin(mean);
        //  Float_t A = hE->GetBinContent(bin);
        //  area->Fill(chan,A);

        if(rms<2) continue;  //will not fit with MIP

        //hE->Rebin(rebin_undo);


        double xmin_fit = 0;
        double xmax_fit = 3000;

        sprintf(tmp,"background_U%d_chan%d",uplinkId,chan);
        background = PS_pedestals_uplink->ProjectionY(tmp,chan+1,chan+1,"");
        //background->Rebin(rebin_undo);


        /////////////////////////////////////////////////////////
        // fit with convolution landau + poisson + background
        TF1 *fconvo = new TF1("fconvo",fconvo_func,xmin_fit,xmax_fit,5);

        if (uplinkId>43) {
            fconvo->SetParameter(0,10);
            //fconvo->SetParameter(0,3); //for layer 4
            fconvo->SetParameter(1,65);
            fconvo->SetParLimits(1,30,150);
            //fconvo->SetParameter(1,45);//for layer 4
            //fconvo->SetParLimits(1,20,150);//for layer 4
            fconvo->FixParameter(3,rebin);
            fconvo->SetParameter(2,2000);
            //fconvo->SetParameter(2,700);//for layer 4
            fconvo->SetParameter(4,500);
            //fconvo->SetParameter(4,100);//for layer 4
        }
        else {
            fconvo->SetParameter(0,4);
            fconvo->SetParLimits(0,2,6);
            //fconvo->SetParameter(0,1); //for layer 4
            fconvo->SetParameter(1,35); //for P
            fconvo->SetParLimits(1,25,100); //for P
            //fconvo->SetParameter(1,16); //for layer 4
            //fconvo->SetParLimits(1,10,100); //for layer 4
            fconvo->FixParameter(3,rebin);
            fconvo->SetParameter(2,3000);
            fconvo->SetParameter(4,50);
            //fconvo->SetParameter(4,100); //for layer 4
        }
        hE->SetAxisRange(-100,3000,"X");
        hE->Draw();
        hE->Fit("fconvo","R");
        Double_t fconvo_max = fconvo->GetMaximumX(20,120);
        //Double_t fconvo_max = fconvo->GetMaximumX(10,120);// for layer 4, 14
        std::cout<<"fconvo maximumX="<<fconvo_max<<"\n";


        //printf("TF1 *fconvo finished\n");

        TF1 *fconvo_s = new TF1("fconvo_s",fconvo_func,xmin_fit,xmax_fit,5);

        fconvo_s->FixParameter(0,fconvo->GetParameter(0));
        fconvo_s->FixParameter(1,fconvo->GetParameter(1));
        fconvo_s->FixParameter(3,rebin);
        fconvo_s->FixParameter(2,fconvo->GetParameter(2));
        fconvo_s->FixParameter(4,0);
        fconvo_s->SetLineColor(kOrange);
        fconvo_s->SetLineWidth(3);
        fconvo_s->Draw("SAME");
        // Double_t fconvo_s_max = fconvo_s->GetMaximumX(40,120);
        Double_t fconvo_s_max = fconvo_s->GetMaximumX(20,120);
        std::cout<<"fconvo_s maximumX="<<fconvo_s_max<<"\n";
        Double_t area_value=fconvo_s->Integral(0,4000);


        //////////////////////////////////////////////////////
        // calculation of the mean of the MIP distribution
        Double_t mean_MIPdistribution=fconvo_s->Moment(1,xmin_fit,xmax_fit);
        std::cout<<"with first moment, fconvo_s mean="<<mean_MIPdistribution<<"\n";
        Int_t bin = hE->GetXaxis()->FindBin(mean_MIPdistribution);
        Float_t A = hE->GetBinContent(bin);
        area->Fill(chan,A);
        //////////////////////////////////////////////////////

        TF1 *fconvo_bg = new TF1("fconvo_bg",fconvo_func,xmin_fit,xmax_fit,5);
        fconvo_bg->FixParameter(0,fconvo->GetParameter(0));
        fconvo_bg->FixParameter(1,fconvo->GetParameter(1));
        fconvo_bg->FixParameter(3,rebin);
        fconvo_bg->FixParameter(2,0);
        fconvo_bg->FixParameter(4,fconvo->GetParameter(4));
        fconvo_bg->SetLineColor(kRed);
        fconvo_bg->Draw("SAME");


        // plot the landau without convolution
        TF1 *flandau = new TF1("flandau","[2]*TMath::Landau(x,[0],[1])",xmin_fit-100,xmax_fit);
        Double_t mpv_param=fconvo->GetParameter(1)+0.22278298*fconvo->GetParameter(0);
        flandau->SetParameter(0,mpv_param);
        flandau->SetParameter(1,fconvo->GetParameter(0));
        flandau->SetParameter(2,fconvo->GetParameter(2));
        flandau->SetLineColor(kGreen);
        flandau->Draw("SAME");
        std::cout<<"flandau maximumX="<<flandau->GetMaximumX(40,120)<<"\n";
        double mean_flandau=flandau->Moment(1,0,3000);
        std::cout<<"with first moment, flandau mean="<<mean_flandau<<"\n";
        std::cout<<"chi2/n="<<fconvo->GetChisquare()/fconvo->GetNDF()<<" prob="<<fconvo->GetProb();

        double mpv = fconvo_s->GetParameter(1);
        mpvInAdc->SetPoint(graph_index,chan,mpv);
        fprintf(resultLog,"%d %d %f %f %f %f %f %f %f\n",uplinkId,chan,fconvo_max,fconvo_s_max,mean_MIPdistribution,mean_flandau,mpv,fconvo->GetChisquare()/fconvo->GetNDF(),fconvo->GetProb());
        if (m_map_key_uplinkID_value_N_or_P[uplinkId]==0) {
            std::pair<int,int> pair_N_uplink_ch(uplinkId,chan);
            //std::cout<<"pair_N_uplink_ch(uplinkId,chan).first="<<pair_N_uplink_ch.first<<"\n";
            //std::cout<<"pair_N_uplink_ch(uplinkId,chan).second="<<pair_N_uplink_ch.second<<"\n";
            //std::cout<<"m_key_N_uplink_ch_value_strip[pair_N_uplink_ch]="<<m_key_N_uplink_ch_value_strip[pair_N_uplink_ch]<<"\n";
            int strip=m_key_N_uplink_ch_value_strip[pair_N_uplink_ch];
            m_map_key_strip_value_N_MPVadc.insert(std::map<int,double>::value_type(strip,mpv));
            std::cout<<"mpv="<<mpv<<"\n";
            std::cout<<"m_map_key_strip_value_N_MPVadc[i]="<<m_map_key_strip_value_N_MPVadc[m_key_N_uplink_ch_value_strip[pair_N_uplink_ch]]<<"\n";
            m_map_key_strip_value_N_area.insert(std::map<int,double>::value_type(m_key_N_uplink_ch_value_strip[pair_N_uplink_ch],area_value));
            m_map_key_strip_value_N_chi2.insert(std::map<int,double>::value_type(m_key_N_uplink_ch_value_strip[pair_N_uplink_ch],fconvo->GetChisquare()/fconvo->GetNDF()));
        }
        if (m_map_key_uplinkID_value_N_or_P[uplinkId]==1) {
            std::pair<int,int> pair_P_uplink_ch(uplinkId,chan);
            std::cout<<"pair_P_uplink_ch(uplinkId,chan).first="<<pair_P_uplink_ch.first<<"\n";
            std::cout<<"pair_P_uplink_ch(uplinkId,chan).second="<<pair_P_uplink_ch.second<<"\n";
            std::cout<<"[pair_P_uplink_ch]="<<m_key_P_uplink_ch_value_strip[pair_P_uplink_ch]<<"\n";
            m_map_key_strip_value_P_MPVadc.insert(std::map<int,double>::value_type(m_key_P_uplink_ch_value_strip[pair_P_uplink_ch],mpv));
            std::cout<<"m_map_key_strip_value_P_MPVadc="<<m_map_key_strip_value_P_MPVadc[m_key_P_uplink_ch_value_strip[pair_P_uplink_ch]]<<"\n";
            m_map_key_strip_value_P_area.insert(std::map<int,double>::value_type(m_key_P_uplink_ch_value_strip[pair_P_uplink_ch],area_value));
            m_map_key_strip_value_P_chi2.insert(std::map<int,double>::value_type(m_key_P_uplink_ch_value_strip[pair_P_uplink_ch],fconvo->GetChisquare()/fconvo->GetNDF()));
        }
        graph_index++;

        hE->GetXaxis()->SetRangeUser(-50,350);
        c1->Update();
        //getchar();

    }

    fclose(resultLog);
    hE = NULL;

    //make a new TCanvas to plot MPV in ADC for all the channels
    if(plot){
        sprintf(tmp,"uplink%d    MPV_in_ADC Variation",uplinkId);
        TCanvas *c2 = new TCanvas(tmp,tmp,10,10,1000,900);
        c2->Divide(1,1);

        c2->cd(1);

        TH1F *hr = c2->cd(1)->DrawFrame(0,40,13,160);
        hr->SetXTitle("chan #");
        hr->SetYTitle("MPV, ADC");

        mpvInAdc->SetMarkerStyle(20);
        mpvInAdc->SetMarkerColor(kRed);
        mpvInAdc->SetMarkerSize(2.);


        mpvInAdc->Fit("pol1","Q");

        mpvInAdc->Draw("P");

        c2->cd(1)->SetGridx();
        c2->cd(1)->SetGridy();
    }

}


void DataManager::FillPsPedestal(MergedEvent* event)
{
    Settings* settings = Settings::instance();
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
    for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
        int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
        unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
        unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;
        const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);
        for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip) {
            for (unsigned short channel = 0; channel < (channelsPerReadoutChip); ++channel) {
                unsigned short sample = chip * channelsPerReadoutChip + channel;
                m_PS_pedestals[i]->Fill(sample , uplinkData.adcValue(sample)-m_pedestal_Mean.at(i)[sample]);
            }
        }
    }
}

TH2* DataManager::GetPS_pedestals(int uplinkId){
    return m_PS_pedestals.at(uplinkIndex(uplinkId));
}


void DataManager::MPVinPE(int uplinkId){

    char tmp[256];

    //printf("for UplinkId = %d, calculate MIP in p.e.\n",uplinkId);
    FILE* resultLog = NULL;
    //open a file to log the result
    sprintf(tmp,"../data/uplink%d_MPVinPE.txt",uplinkId);
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----find MPV in PE --------\n\n");
    fprintf(resultLog,"Uplink\t channels\t mpv in p.e.\n");


    TGraphErrors* gains =  m_adcPerPhoton.at(uplinkIndex(uplinkId));
    TGraphErrors* mpv = m_mpvInAdc.at(uplinkIndex(uplinkId));
    TGraphErrors* resultCorr = m_resultCorr.at(uplinkIndex(uplinkId));

    double chan = 0;
    double corrChan = 1;
    double measuredGain = 0;
    double measuredMPV = 0;
    for(int i=0; i<mpv->GetN();i++){
        //printf("mpv->GetN() = %d\n",mpv->GetN());
        mpv->GetPoint(i,chan,measuredMPV);
        //printf("chan = %d\n",chan);
        for(int j=0; j<gains->GetN();j++){
            //printf("gains->GetN() = %d\n",gains->GetN());
            gains->GetPoint(j,corrChan,measuredGain);
            //printf("corrChan = %d\n",corrChan);
            //getchar();
            if(corrChan == chan) {
                resultCorr->SetPoint(i,chan,measuredMPV/measuredGain);
                //std::cout<< "i= "<<i <<" chan="<<chan<<" MPV = "<<measuredMPV <<" GAIN= "<<measuredGain<<"MIPinPE = "<<measuredMPV/measuredGain<<std::endl;
                fprintf(resultLog,"%d\t%d\t%.2f\t\n",uplinkId,chan,measuredMPV/measuredGain);
                std::pair<int,int> pair_N_uplink_ch(uplinkId,chan);
                //std::cout<<"pair_N_uplink_ch(uplinkId,chan).first="<<pair_N_uplink_ch.first<<"\n";
                //std::cout<<"pair_N_uplink_ch(uplinkId,chan).second="<<pair_N_uplink_ch.second<<"\n";
                //std::cout<<"m_key_N_uplink_ch_value_strip[pair_N_uplink_ch]="<<m_key_N_uplink_ch_value_strip[pair_N_uplink_ch]<<"\n";
                m_map_key_strip_value_N_MPVinPE.insert(std::map<int,double>::value_type(m_key_N_uplink_ch_value_strip[pair_N_uplink_ch],measuredMPV/measuredGain));
                //getchar();
            }
        }
    }

    fclose(resultLog);
}


DataManager::DataManager(std::string pathToSetupFile, bool arg_convert, bool arg_findMuonsTracks){
    char tmp[128];
    Settings* settings = Settings::instance();

    m_pathToDACFile = "";
    m_pathToFormerCfgFile = "";
    m_dacFromCfgFile = false;
    m_dacFromAdjustFile = false;
    m_forceToChangeCfgFile = false;
    m_aimGain = 12;

    m_readInSetupFile = true;

    // read set up from the setupFile
    std::ifstream setupFile(pathToSetupFile.c_str());
    if(!setupFile){
        std::cerr << "Failure: could not open file: \"" << pathToSetupFile << "\"." << std::endl;
        std::cerr << "Please check if the path is correct or not!" << std::endl;
        assert(false);
    }

    std::string s;
    int temp;
    while (true) {
        std::getline(setupFile, s);
        if (setupFile.eof())
            break;
        const char* searchString = s.c_str();

        if (s.find("#") == 0 || s=="") {
            continue; // Skip commented lines
        }

        if(sscanf(searchString, "L1_P1_P || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P1_P,temp));
        }

        if(sscanf(searchString, "L1_P1_N || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P1_N,temp));
        }

        if(sscanf(searchString, "L1_P2_P || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P2_P,temp));
        }

        if(sscanf(searchString, "L1_P2_N || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P2_N,temp));
        }

        if(sscanf(searchString, "L1_P3_P || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P3_P,temp));
        }

        if(sscanf(searchString, "L1_P3_N || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P3_N,temp));
        }

        if(sscanf(searchString, "L1_P4_P || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P4_P,temp));
        }

        if(sscanf(searchString, "L1_P4_N || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P4_N,temp));
        }

        if(sscanf(searchString, "L2_P1_P || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P1_P,temp));
        }

        if(sscanf(searchString, "L2_P1_N || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P1_N,temp));
        }

        if(sscanf(searchString, "L2_P2_P || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P2_P,temp));
        }

        if(sscanf(searchString, "L2_P2_N || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P2_N,temp));
        }

        if(sscanf(searchString, "L2_P3_P || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P3_P,temp));
        }

        if(sscanf(searchString, "L2_P3_N || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P3_N,temp));
        }

        if(sscanf(searchString, "L2_P4_P || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P4_P,temp));
        }

        if(sscanf(searchString, "L2_P4_N || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P4_N,temp));
        }

    }

    SuperLayerMap::iterator superLayer_iter;
    std::vector<int> uplinksToBeAnalyzed;
    for(int superLayerIndex = 0;superLayerIndex<2;superLayerIndex++){
        for(superLayer_iter= m_superLayerMap[superLayerIndex].begin();superLayer_iter!=m_superLayerMap[superLayerIndex].end();superLayer_iter++){
            if(superLayer_iter->second!=0) uplinksToBeAnalyzed.push_back(superLayer_iter->second);
            //printf("m_superLayerMap[%d][%d] = %d \n",superLayerIndex,superLayer_iter->first,superLayer_iter->second);
            //getchar();
        }
    }

    if (arg_convert) {
        tree1 = new TTree("T1","T1");
        std::ostringstream argu1;
        std::string var;

        argu1.str("");
        argu1<<"E_P["<<NUMBER_OF_LAYER*STRIPS_PER_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("E_P",&E_P,var.c_str());

        argu1.str("");
        argu1<<"E_N["<<NUMBER_OF_LAYER*STRIPS_PER_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("E_N",&E_N,var.c_str());

        argu1.str("");
        argu1<<"PIN_P["<<NUMBER_OF_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("PIN_P",&PIN_P,var.c_str());

        argu1.str("");
        argu1<<"PIN_N["<<NUMBER_OF_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("PIN_N",&PIN_N,var.c_str());
    }

    if (arg_findMuonsTracks) {
        tree1 = new TTree("T1","T1");
        std::ostringstream argu1;
        std::string var;

        argu1<<"strip_max["<<NUMBER_OF_LAYER<<"]/I";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("strip_max",&strip_max,var.c_str());
        //tree1->Branch("strip_max",&strip_max,"strip_max[16]/I");

        argu1.str("");
        argu1<<"E_P_max["<<NUMBER_OF_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("E_P_max",&E_P_max,var.c_str());

        argu1.str("");
        argu1<<"E_N_max["<<NUMBER_OF_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("E_N_max",&E_N_max,var.c_str());

        argu1.str("");
        argu1<<"X_max["<<NUMBER_OF_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("X_max",&X_max,var.c_str());

        argu1.str("");
        argu1<<"Y_max["<<NUMBER_OF_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("Y_max",&Y_max,var.c_str());

        argu1.str("");
        argu1<<"Z_max["<<NUMBER_OF_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("Z_max",&Z_max,var.c_str());

        argu1.str("");
        argu1<<"strip_muon["<<NUMBER_OF_LAYER<<"]/I";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("strip_muon",&strip_muon,var.c_str());

        argu1.str("");
        argu1<<"E_P_muon["<<NUMBER_OF_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("E_P_muon",&E_P_muon,var.c_str());

        argu1.str("");
        argu1<<"E_N_muon["<<NUMBER_OF_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("E_N_muon",&E_N_muon,var.c_str());

        argu1.str("");
        argu1<<"X["<<NUMBER_OF_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("X",&X,var.c_str());

        argu1.str("");
        argu1<<"Y["<<NUMBER_OF_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("Y",&Y,var.c_str());

        argu1.str("");
        argu1<<"Z["<<NUMBER_OF_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("Z",&Z,var.c_str());

        argu1.str("");
        argu1<<"coordinate_layer["<<NUMBER_OF_LAYER<<"]/I";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("coordinate_layer",&coordinate_layer,var.c_str());

        argu1.str("");
        argu1<<"E_P["<<NUMBER_OF_LAYER*STRIPS_PER_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("E_P",&E_P,var.c_str());

        argu1.str("");
        argu1<<"E_N["<<NUMBER_OF_LAYER*STRIPS_PER_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("E_N",&E_N,var.c_str());

        tree1->Branch("theta",&theta,"theta/F");
        tree1->Branch("phi",&phi,"phi/F");
        tree1->Branch("x0",&x0,"x0/F");
        tree1->Branch("Ax",&Ax,"Ax/F");
        tree1->Branch("y0",&y0,"y0/F");
        tree1->Branch("Ay",&Ay,"Ay/F");

        argu1.str("");
        argu1<<"PIN_P["<<NUMBER_OF_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("PIN_P",&PIN_P,var.c_str());

        argu1.str("");
        argu1<<"PIN_N["<<NUMBER_OF_LAYER<<"]/F";
        var=argu1.str();
        std::cout<<var<<"\n";
        tree1->Branch("PIN_N",&PIN_N,var.c_str());

        for (int i=0;i<NUMBER_OF_LAYER;i++) {
            sprintf(tmp, "m_TH3_spectra_P_%03d", i);
            m_TH3_spectra_P[i] = new TH3I(tmp, tmp, STRIPS_PER_LAYER, 0, STRIPS_PER_LAYER,STRIPS_PER_LAYER,0,STRIPS_PER_LAYER, maxAdcValue+500, -500, maxAdcValue);
            m_TH3_spectra_P[i]->GetXaxis()->SetTitle("x (strip)");
            m_TH3_spectra_P[i]->GetYaxis()->SetTitle("y (strip)");
            m_TH3_spectra_P[i]->GetZaxis()->SetTitle("adcValue");
            sprintf(tmp, "m_TH3_spectra_N_%03d", i);
            m_TH3_spectra_N[i] = new TH3I(tmp, tmp, STRIPS_PER_LAYER, 0, STRIPS_PER_LAYER,STRIPS_PER_LAYER,0,STRIPS_PER_LAYER, maxAdcValue+500, -500, maxAdcValue);
            m_TH3_spectra_N[i]->GetXaxis()->SetTitle("x (strip)");
            m_TH3_spectra_N[i]->GetYaxis()->SetTitle("y (strip)");
            m_TH3_spectra_N[i]->GetZaxis()->SetTitle("adcValue");
          }
    }

    for (unsigned int k = 0; k < uplinksToBeAnalyzed.size(); ++k) {
        int uplinkId = uplinksToBeAnalyzed[k];
        //printf("uplinkId = %d\n",uplinkId);
        int usbBoardId = settings->usbBoardIdFromUplinkId(uplinkId);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();

        m_uplinkId.push_back(uplinkId);

        sprintf(tmp, "adcHistogram_uplink_%03d", uplinkId);
        TH2* adcHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        adcHistogram->GetXaxis()->SetTitle("channel");
        adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_adcHistogram.push_back(adcHistogram);

        sprintf(tmp, "PS_adcHistogram_uplink_%03d", uplinkId);
        TH2* PS_adcHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue+500, -500, maxAdcValue);
        PS_adcHistogram->GetXaxis()->SetTitle("channel");
        PS_adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_PS_adcHistogram.push_back(PS_adcHistogram);

        sprintf(tmp, "PS_pedestals_uplink_%03d", uplinkId);
        TH2* PS_pedestals = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue+500, -500, maxAdcValue);
        PS_pedestals->GetXaxis()->SetTitle("channel");
        PS_pedestals->GetYaxis()->SetTitle("adcValue");
        m_PS_pedestals.push_back(PS_pedestals);

        sprintf(tmp, "raw_pedestals_uplink_%03d", uplinkId);
        TH2* raw_pedestals = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        raw_pedestals->GetXaxis()->SetTitle("channel");
        raw_pedestals->GetYaxis()->SetTitle("adcValue");
        m_raw_pedestals.push_back(raw_pedestals);

        TGraphErrors* adcPerPhoton = new TGraphErrors(1);
        m_adcPerPhoton.push_back(adcPerPhoton);

        TGraphErrors* mpvInAdc = new TGraphErrors(1);
        m_mpvInAdc.push_back(mpvInAdc);

        TGraphErrors* resultCorr = new TGraphErrors(1);
        m_resultCorr.push_back(resultCorr);

        TGraphErrors* lSigma = new TGraphErrors(1);
        m_lSigma.push_back(lSigma);

        TGraphErrors* gSigma = new TGraphErrors(1);
        m_gSigma.push_back(gSigma);

        sprintf(tmp, "area_uplink%d", uplinkId);
        TH1F* area = new TH1F(tmp,"area_for_mean_Of_MIP_Distribution;Channel;Area",NUMBER_OF_EFFECT_CHANNELS,HEAD_OF_EFFECT_CHANNELS,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS);
        m_area.push_back(area);

        TGraphErrors* meanPhoton = new TGraphErrors(1);
        m_meanInPhoton.push_back(meanPhoton);
    }


    SuperLayerMap::iterator corrIter;
    for(int superLayerIndex = 0; superLayerIndex<2;superLayerIndex++){
        for(superLayer_iter= m_superLayerMap[superLayerIndex].begin();superLayer_iter->first<4;superLayer_iter++){
            if(superLayer_iter->second == 0) continue;
            corrIter = m_superLayerMap[superLayerIndex].find(superLayer_iter->first+P_N_DISTANCE);
            if(corrIter == m_superLayerMap[superLayerIndex].end()){
                printf("Can not find its corresponding part of %d, check the setup file!\n",superLayer_iter->first);
                assert(false);
            }else{
                if(corrIter->second != 0){
                    m_corrUplinkPairs.push_back(superLayer_iter->second);
                    m_corrUplinkPairs.push_back(corrIter->second);
                }
            }
        }
    }

    if(m_corrUplinkPairs.size()!=0 ){
        for(int i=0; i< (m_corrUplinkPairs.size()/2); i++){
            int Xuplink = m_corrUplinkPairs.at(2*i);
            int Yuplink = m_corrUplinkPairs.at(2*i+1);

            int corrChan = 0;
            int stripIndex = 0;

            for (Int_t i=HEAD_OF_EFFECT_CHANNELS;i<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;i++){
                if((i%CHANNELS_PER_GROUP)>(STRIPS_PER_GROUP-1)) continue; // only 0~11,16~27,32~43,48~59 are SiPM output
                corrChan = (i/CHANNELS_PER_GROUP)*CHANNELS_PER_GROUP+STRIPS_PER_GROUP-1-i%CHANNELS_PER_GROUP;

                int SuperLayerIndex = 0;
                int positionIndex = 0;


                for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                    for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                        if(superLayer_iter->second==Xuplink) {
                            SuperLayerIndex = LayerIndex;
                            positionIndex = superLayer_iter->first;
                            break;
                        }
                    }
                }

                stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+(i/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+i%CHANNELS_PER_GROUP;
                //std::cout<<"chan= "<<i <<" & corrChan = "<<corrChan <<" are correlated!"<<" stripIndex = "<<stripIndex<<std::endl;
                //getchar();
                sprintf(tmp, "uplink_%d_vs_%d_____uplink%d_channel%03d_with_uplink%d_channel%03d",Xuplink,Yuplink,Xuplink,i,Yuplink,corrChan);
                TH2* uplink_correlation = new TH2I(tmp,tmp, (maxAdcValue+500)/10, -500., maxAdcValue, (maxAdcValue+500)/10, -500., maxAdcValue);
                sprintf(tmp,"uplink_%d",Xuplink);
                uplink_correlation->GetXaxis()->SetTitle(tmp);
                sprintf(tmp,"uplink_%d",Yuplink);
                uplink_correlation->GetYaxis()->SetTitle(tmp);
                m_strip_correlation.insert(std::map<int,TH2*>::value_type(stripIndex,uplink_correlation));
                m_map_key_strip_value_N_uplink.insert(std::map<int,int>::value_type(stripIndex,Yuplink));
                m_map_key_strip_value_N_ch.insert(std::map<int,int>::value_type(stripIndex,corrChan));
                m_map_key_strip_value_P_uplink.insert(std::map<int,int>::value_type(stripIndex,Xuplink));
                m_map_key_strip_value_P_ch.insert(std::map<int,int>::value_type(stripIndex,i));
                m_map_key_uplinkID_value_N_or_P.insert(std::map<int,int>::value_type(Yuplink,0));
                m_map_key_uplinkID_value_N_or_P.insert(std::map<int,int>::value_type(Xuplink,1));
                std::pair<int,int> pair_N_uplink_ch(Yuplink,corrChan); //for N<uplinkID,ch>
                //std::cout<<"pair_N_uplink_ch.first="<<pair_N_uplink_ch.first<<"\t pair_N_uplink_ch.second="<<pair_N_uplink_ch.second<<"\n";
                std::pair<int,int> pair_P_uplink_ch(Xuplink,i); //for N<uplinkID,ch>
                m_key_N_uplink_ch_value_strip.insert(std::map<std::pair<int,int>,int>::value_type(pair_N_uplink_ch,stripIndex));
                m_key_P_uplink_ch_value_strip.insert(std::map<std::pair<int,int>,int>::value_type(pair_P_uplink_ch,stripIndex));
            }
        }
    }
}

DataManager::DataManager(bool arg_findRatio, std::string pathToSetupFile){
    char tmp[128];
    Settings* settings = Settings::instance();

    m_pathToDACFile = "";
    m_pathToFormerCfgFile = "";
    m_dacFromCfgFile = false;
    m_dacFromAdjustFile = false;
    m_forceToChangeCfgFile = false;
    m_aimGain = 12;
    m_readInSetupFile = true;

    // read set up from the setupFile
    std::ifstream setupFile(pathToSetupFile.c_str());
    if(!setupFile){
        std::cerr << "Failure: could not open file: \"" << pathToSetupFile << "\"." << std::endl;
        std::cerr << "Please check if the path is correct or not!" << std::endl;
        assert(false);
    }

    std::string s;
    int temp;
    while (true) {
        std::getline(setupFile, s);
        if (setupFile.eof())
            break;
        const char* searchString = s.c_str();

        if (s.find("#") == 0 || s=="") {
            continue; // Skip commented lines
        }

        if(sscanf(searchString, "L1_P1_P || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P1_P,temp));
        }

        if(sscanf(searchString, "L1_P1_N || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P1_N,temp));
        }

        if(sscanf(searchString, "L1_P2_P || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P2_P,temp));
        }

        if(sscanf(searchString, "L1_P2_N || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P2_N,temp));
        }

        if(sscanf(searchString, "L1_P3_P || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P3_P,temp));
        }

        if(sscanf(searchString, "L1_P3_N || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P3_N,temp));
        }

        if(sscanf(searchString, "L1_P4_P || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P4_P,temp));
        }

        if(sscanf(searchString, "L1_P4_N || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P4_N,temp));
        }

        if(sscanf(searchString, "L2_P1_P || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P1_P,temp));
        }

        if(sscanf(searchString, "L2_P1_N || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P1_N,temp));
        }

        if(sscanf(searchString, "L2_P2_P || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P2_P,temp));
        }

        if(sscanf(searchString, "L2_P2_N || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P2_N,temp));
        }

        if(sscanf(searchString, "L2_P3_P || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P3_P,temp));
        }

        if(sscanf(searchString, "L2_P3_N || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P3_N,temp));
        }

        if(sscanf(searchString, "L2_P4_P || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P4_P,temp));
        }

        if(sscanf(searchString, "L2_P4_N || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P4_N,temp));
        }

    }

    SuperLayerMap::iterator superLayer_iter;
    std::vector<int> uplinksToBeAnalyzed;
    for(int superLayerIndex = 0;superLayerIndex<2;superLayerIndex++){
        for(superLayer_iter= m_superLayerMap[superLayerIndex].begin();superLayer_iter!=m_superLayerMap[superLayerIndex].end();superLayer_iter++){
            if(superLayer_iter->second!=0) uplinksToBeAnalyzed.push_back(superLayer_iter->second);
            //printf("m_superLayerMap[%d][%d] = %d \n",superLayerIndex,superLayer_iter->first,superLayer_iter->second);
            //getchar();
        }
    }

    for (unsigned int k = 0; k < uplinksToBeAnalyzed.size(); ++k) {
        int uplinkId = uplinksToBeAnalyzed[k];
        //printf("uplinkId = %d\n",uplinkId);
        int usbBoardId = settings->usbBoardIdFromUplinkId(uplinkId);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();

        m_uplinkId.push_back(uplinkId);

        sprintf(tmp, "adcHistogram_uplink_%03d", uplinkId);
        TH2* adcHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        adcHistogram->GetXaxis()->SetTitle("channel");
        adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_adcHistogram.push_back(adcHistogram);

        sprintf(tmp, "PS_adcHistogram_uplink_%03d", uplinkId);
        TH2* PS_adcHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue+500, -500, maxAdcValue);
        PS_adcHistogram->GetXaxis()->SetTitle("channel");
        PS_adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_PS_adcHistogram.push_back(PS_adcHistogram);

        sprintf(tmp, "PS_pedestals_uplink_%03d", uplinkId);
        TH2* PS_pedestals = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue+500, -500, maxAdcValue);
        PS_pedestals->GetXaxis()->SetTitle("channel");
        PS_pedestals->GetYaxis()->SetTitle("adcValue");
        m_PS_pedestals.push_back(PS_pedestals);

        sprintf(tmp, "raw_pedestals_uplink_%03d", uplinkId);
        TH2* raw_pedestals = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        raw_pedestals->GetXaxis()->SetTitle("channel");
        raw_pedestals->GetYaxis()->SetTitle("adcValue");
        m_raw_pedestals.push_back(raw_pedestals);

        TGraphErrors* adcPerPhoton = new TGraphErrors(1);
        m_adcPerPhoton.push_back(adcPerPhoton);

        TGraphErrors* resultCorr = new TGraphErrors(1);
        m_resultCorr.push_back(resultCorr);
    }


    SuperLayerMap::iterator corrIter;
    for(int superLayerIndex = 0; superLayerIndex<2;superLayerIndex++){
        for(superLayer_iter= m_superLayerMap[superLayerIndex].begin();superLayer_iter->first<4;superLayer_iter++){
            if(superLayer_iter->second == 0) continue;
            corrIter = m_superLayerMap[superLayerIndex].find(superLayer_iter->first+P_N_DISTANCE);
            if(corrIter == m_superLayerMap[superLayerIndex].end()){
                printf("Can not find its corresponding part of %d, check the setup file!\n",superLayer_iter->first);
                assert(false);
            }else{
                if(corrIter->second != 0){
                    m_corrUplinkPairs.push_back(superLayer_iter->second);
                    m_corrUplinkPairs.push_back(corrIter->second);
                }
            }
        }
    }

    if(m_corrUplinkPairs.size()!=0 ){
        for(int i=0; i< (m_corrUplinkPairs.size()/2); i++){
            int Xuplink = m_corrUplinkPairs.at(2*i);
            int Yuplink = m_corrUplinkPairs.at(2*i+1);

            int corrChan = 0;
            int stripIndex = 0;

            for (Int_t i=HEAD_OF_EFFECT_CHANNELS;i<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;i++){
                if((i%CHANNELS_PER_GROUP)>(STRIPS_PER_GROUP-1)) continue; // only 0~11,16~27,32~43,48~59 are SiPM output
                corrChan = (i/CHANNELS_PER_GROUP)*CHANNELS_PER_GROUP+STRIPS_PER_GROUP-1-i%CHANNELS_PER_GROUP;

                int SuperLayerIndex = 0;
                int positionIndex = 0;


                for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                    for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                        if(superLayer_iter->second==Xuplink) {
                            SuperLayerIndex = LayerIndex;
                            positionIndex = superLayer_iter->first;
                            break;
                        }
                    }
                }

                stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+(i/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+i%CHANNELS_PER_GROUP;
                //std::cout<<"chan= "<<i <<" & corrChan = "<<corrChan <<" are correlated!"<<" stripIndex = "<<stripIndex<<std::endl;
                //getchar();

                sprintf(tmp, "LedP_uplink_%d_vs_%d_____uplink%d_channel%03d_with_uplink%d_channel%03d",Xuplink,Yuplink,Xuplink,i,Yuplink,corrChan);
                TH2* LedP_uplink_correlation = new TH2I(tmp,tmp, (maxAdcValue+500)/10, -500., maxAdcValue, (maxAdcValue+500)/10, -500., maxAdcValue);
                sprintf(tmp,"LedP_uplink_%d",Xuplink);
                LedP_uplink_correlation->GetXaxis()->SetTitle(tmp);
                sprintf(tmp,"LedP_uplink_%d",Yuplink);
                LedP_uplink_correlation->GetYaxis()->SetTitle(tmp);

                sprintf(tmp, "LedN_uplink_%d_vs_%d_____uplink%d_channel%03d_with_uplink%d_channel%03d",Xuplink,Yuplink,Xuplink,i,Yuplink,corrChan);
                TH2* LedN_uplink_correlation = new TH2I(tmp,tmp, (maxAdcValue+500)/10, -500., maxAdcValue, (maxAdcValue+500)/10, -500., maxAdcValue);
                sprintf(tmp,"LedN_uplink_%d",Xuplink);
                LedN_uplink_correlation->GetXaxis()->SetTitle(tmp);
                sprintf(tmp,"LedN_uplink_%d",Yuplink);
                LedN_uplink_correlation->GetYaxis()->SetTitle(tmp);

                m_LedP_strip_correlation.insert(std::map<int,TH2*>::value_type(stripIndex,LedP_uplink_correlation));
                m_LedN_strip_correlation.insert(std::map<int,TH2*>::value_type(stripIndex,LedN_uplink_correlation));

                m_map_key_strip_value_N_uplink.insert(std::map<int,int>::value_type(stripIndex,Yuplink));
                m_map_key_strip_value_N_ch.insert(std::map<int,int>::value_type(stripIndex,corrChan));
                m_map_key_strip_value_P_uplink.insert(std::map<int,int>::value_type(stripIndex,Xuplink));
                m_map_key_strip_value_P_ch.insert(std::map<int,int>::value_type(stripIndex,i));
                m_map_key_uplinkID_value_N_or_P.insert(std::map<int,int>::value_type(Yuplink,0));
                m_map_key_uplinkID_value_N_or_P.insert(std::map<int,int>::value_type(Xuplink,1));
                std::pair<int,int> pair_N_uplink_ch(Yuplink,corrChan); //for N<uplinkID,ch>
                //std::cout<<"pair_N_uplink_ch.first="<<pair_N_uplink_ch.first<<"\t pair_N_uplink_ch.second="<<pair_N_uplink_ch.second<<"\n";
                std::pair<int,int> pair_P_uplink_ch(Xuplink,i); //for N<uplinkID,ch>
                m_key_N_uplink_ch_value_strip.insert(std::map<std::pair<int,int>,int>::value_type(pair_N_uplink_ch,stripIndex));
                m_key_P_uplink_ch_value_strip.insert(std::map<std::pair<int,int>,int>::value_type(pair_P_uplink_ch,stripIndex));
            }
        }
    }
}


void DataManager::PlotAnalysisResult_1uplink(int uplinkId, std::string pedestalFilePath, std::string signalFilePath){

    char tmp[256];

    sprintf(tmp,"uplink %d\t%s\t%s",uplinkId,pedestalFilePath.c_str(),signalFilePath.c_str());
    TCanvas *c1 = new TCanvas(tmp,tmp,1280,1280);
    c1->Divide(2,3);


    gStyle->SetLabelFont(132,"XY");
    gStyle->SetLabelSize(0.06,"XY");

    gStyle->SetTitleFont(132,"XY");
    gStyle->SetTitleSize(0.06,"XY");
    gStyle->SetTitleYOffset(0.8);
    gStyle->SetTitleXOffset(0.8);

    //draw ADC per Photon
    c1->cd(1);
    TGraphErrors* graph1 = m_adcPerPhoton.at(uplinkIndex(uplinkId));
    TH1F *hr1 = c1->cd(1)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,8,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,14);

    hr1->SetXTitle("channel #");
    hr1->SetYTitle("gain(ADC per Photon), ADC");

    graph1->SetMarkerStyle(20);
    graph1->SetMarkerColor(kRed);
    graph1->SetMarkerSize(2.);

    graph1->Fit("pol1","Q");
    graph1->Draw("PE");

    c1->cd(1)->SetGridx();
    c1->cd(1)->SetGridy();


    //draw MPV in ADC
    c1->cd(2);
    TGraphErrors* graph2 = m_mpvInAdc.at(uplinkIndex(uplinkId));
    TH1F *hr2 = c1->cd(2)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,40,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,160);

    hr2->SetXTitle("channel #");
    hr2->SetYTitle("MPV, ADC");

    graph2->SetMarkerStyle(21);
    graph2->SetMarkerColor(kRed);
    graph2->SetMarkerSize(2.);

    graph2->Fit("pol1","Q");
    graph2->Draw("PE");

    //c1->cd(2)->SetGridx();
    //c1->cd(2)->SetGridy();

    //draw resultCorr : MPV in photon electron
    c1->cd(3);
    TGraphErrors* graph3 = m_resultCorr.at(uplinkIndex(uplinkId));
    TH1F *hr3 = c1->cd(3)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,15);

    hr3->SetXTitle("channel #");
    hr3->SetYTitle("MPV, p.e.");

    graph3->SetMarkerStyle(22);
    graph3->SetMarkerColor(kRed);
    graph3->SetMarkerSize(2.);

    graph3->Fit("pol1","Q");
    graph3->Draw("PE");

    c1->cd(3)->SetGridx();
    c1->cd(3)->SetGridy();

    //draw area:
    c1->cd(4);
    TH1F *hr4 = m_area.at(uplinkIndex(uplinkId));

    hr4->SetLineColor(kRed);
    hr4->SetLineWidth(3);
    hr4->Draw();

    c1->cd(4)->SetGridx();
    c1->cd(4)->SetGridy();

    //draw Lsigma in photon electron
    c1->cd(5);
    TGraphErrors* graph5 = m_lSigma.at(uplinkIndex(uplinkId));
    TH1F *hr5 = c1->cd(5)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,2.5);

    hr5->SetXTitle("channel #");
    hr5->SetYTitle("Landau width, p.e.");

    graph5->SetMarkerStyle(23);
    graph5->SetMarkerColor(kRed);
    graph5->SetMarkerSize(2.);

    graph5->Fit("pol1","Q");
    graph5->Draw("PE");

    c1->cd(5)->SetGridx();
    c1->cd(5)->SetGridy();

    //draw Gsigma in photon electron
    c1->cd(6);
    TGraphErrors* graph6 = m_gSigma.at(uplinkIndex(uplinkId));
    TH1F *hr6 = c1->cd(6)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,7);

    hr6->SetXTitle("channel #");
    hr6->SetYTitle("Gauss width, p.e.");

    graph6->SetMarkerStyle(33);
    graph6->SetMarkerColor(kRed);
    graph6->SetMarkerSize(2.);

    graph6->Fit("pol1","Q");
    graph6->Draw("PE");

    c1->cd(6)->SetGridx();
    c1->cd(6)->SetGridy();

    //c1->Update();
}


DataManager::DataManager(int uplinksNumber,int* uplinkIdArray,std::string pathToReadInFile){
    m_pathToDACFile = "";
    m_pathToFormerCfgFile = "";
    m_dacFromCfgFile = false;
    m_dacFromAdjustFile = false;
    m_forceToChangeCfgFile = false;
    m_aimGain = 12;
    m_readInSetupFile = false;

    for(int i = 0; i<uplinksNumber;i++) m_uplinkId.push_back(uplinkIdArray[i]);
    TFile *file = new TFile(pathToReadInFile.c_str(),"READ");
    printf("readInFile from %s \n",pathToReadInFile.c_str());

    char tmp[256];

    TH2* histo;

    //Fill the raw pedestals
    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        sprintf(tmp,"raw_pedestals_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
        histo = (TH2*)file->Get(tmp);
        m_raw_pedestals.push_back(histo);
    }

    //Fill the PS_pedestal
    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        sprintf(tmp,"PS_pedestals_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
        histo = (TH2*)file->Get(tmp);
        m_PS_pedestals.push_back(histo);
    }

    //Fill the adcHistogram
    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        sprintf(tmp,"adcHistogram_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
        histo = (TH2*)file->Get(tmp);
        m_adcHistogram.push_back(histo);
    }

    //fill the PS_adcHistogram
    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        sprintf(tmp,"PS_adcHistogram_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
        histo = (TH2*)file->Get(tmp);
        m_PS_adcHistogram.push_back(histo);
    }

    //fill the m_uplink_correlation
    if((m_uplinkId.size()%2==0) && (m_uplinkId.size()!=0) ){
        //printf("fill  m_uplink_correlation :\n");
        for(int corrIndex=0; corrIndex< (m_uplinkId.size()/2); corrIndex++){
            int Xuplink = m_uplinkId.at(2*corrIndex);
            int Yuplink = m_uplinkId.at(2*corrIndex+1);

            //printf("Xuplink = %d, Yuplink = %d\n",Xuplink,Yuplink);

            for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;chan++){
                sprintf(tmp, "uplink_%d_vs_%d_____uplink%d_channel%03d_with_uplink%d_channel%03d",Xuplink,Yuplink,Xuplink,chan,Yuplink,2*HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1-chan);
                histo = (TH2*)file->Get(tmp);
                m_uplink_correlation.push_back(histo);
            }
        }
    }


    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        TGraphErrors* adcPerPhoton = new TGraphErrors(1);
        m_adcPerPhoton.push_back(adcPerPhoton);

        TGraphErrors* mpvInAdc = new TGraphErrors(1);
        m_mpvInAdc.push_back(mpvInAdc);

        TGraphErrors* resultCorr = new TGraphErrors(1);
        m_resultCorr.push_back(resultCorr);

        sprintf(tmp, "area_uplink%d", m_uplinkId.at(uplinkIndex));
        TH1F* area = new TH1F(tmp,"area_for_mean_Of_MIP_Distribution;Channel;Area",NUMBER_OF_EFFECT_CHANNELS,HEAD_OF_EFFECT_CHANNELS,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS);
        m_area.push_back(area);

        TGraphErrors* meanPhoton = new TGraphErrors(1);
        m_meanInPhoton.push_back(meanPhoton);

    }

}

DataManager::DataManager(const std::vector<int>& uplinksToBeAnalyzed)
{
    char tmp[128];
    Settings* settings = Settings::instance();
    // initialize setup
    //m_setup = new PEBS09_TestbeamSetup();

    m_pathToDACFile = "";
    m_pathToFormerCfgFile = "";
    m_dacFromCfgFile = false;
    m_dacFromAdjustFile = false;
    m_forceToChangeCfgFile = false;
    m_aimGain = 12;
    m_readInSetupFile = false;


    for (unsigned int k = 0; k < uplinksToBeAnalyzed.size(); ++k) {
        int uplinkId = uplinksToBeAnalyzed[k];
        int usbBoardId = settings->usbBoardIdFromUplinkId(uplinkId);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();

        m_uplinkId.push_back(uplinkId);

        sprintf(tmp, "adcHistogram_uplink_%03d", uplinkId);
        TH2* adcHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        adcHistogram->GetXaxis()->SetTitle("channel");
        adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_adcHistogram.push_back(adcHistogram);

        sprintf(tmp, "PS_adcHistogram_uplink_%03d", uplinkId);
        TH2* PS_adcHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue+500, -500, maxAdcValue);
        PS_adcHistogram->GetXaxis()->SetTitle("channel");
        PS_adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_PS_adcHistogram.push_back(PS_adcHistogram);

        sprintf(tmp, "PS_pedestals_uplink_%03d", uplinkId);
        TH2* PS_pedestals = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue+500, -500, maxAdcValue);
        PS_pedestals->GetXaxis()->SetTitle("channel");
        PS_pedestals->GetYaxis()->SetTitle("adcValue");
        m_PS_pedestals.push_back(PS_pedestals);

        sprintf(tmp, "raw_pedestals_uplink_%03d", uplinkId);
        TH2* raw_pedestals = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        raw_pedestals->GetXaxis()->SetTitle("channel");
        raw_pedestals->GetYaxis()->SetTitle("adcValue");
        m_raw_pedestals.push_back(raw_pedestals);

        TGraphErrors* adcPerPhoton = new TGraphErrors(1);
        m_adcPerPhoton.push_back(adcPerPhoton);

        TGraphErrors* mpvInAdc = new TGraphErrors(1);
        m_mpvInAdc.push_back(mpvInAdc);

        TGraphErrors* resultCorr = new TGraphErrors(1);
        m_resultCorr.push_back(resultCorr);

        TGraphErrors* lSigma = new TGraphErrors(1);
        m_lSigma.push_back(lSigma);

        TGraphErrors* gSigma = new TGraphErrors(1);
        m_gSigma.push_back(gSigma);

        sprintf(tmp, "area_uplink%d", uplinkId);
        TH1F* area = new TH1F(tmp,"area_for_mean_Of_MIP_Distribution;Channel;Area",NUMBER_OF_EFFECT_CHANNELS,HEAD_OF_EFFECT_CHANNELS,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS);
        m_area.push_back(area);

        TGraphErrors* meanPhoton = new TGraphErrors(1);
        m_meanInPhoton.push_back(meanPhoton);
    }

    //if the number of uplinks to be analyzed is even, make correaltion between 1&2, 3&4, ...
    if((uplinksToBeAnalyzed.size()%2==0) && (uplinksToBeAnalyzed.size()!=0) ){
        for(int i=0; i< (uplinksToBeAnalyzed.size()/2); i++){
            int Xuplink = uplinksToBeAnalyzed.at(2*i);
            int Yuplink = uplinksToBeAnalyzed.at(2*i+1);

            for (Int_t i=HEAD_OF_EFFECT_CHANNELS;i<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;i++){
                sprintf(tmp, "uplink_%d_vs_%d_____uplink%d_channel%03d_with_uplink%d_channel%03d",Xuplink,Yuplink,Xuplink,i,Yuplink,2*HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1-i);
                TH2* uplink_correlation = new TH2I(tmp,tmp, (maxAdcValue+500)/10, -500., maxAdcValue, (maxAdcValue+500)/10, -500., maxAdcValue);
                sprintf(tmp,"uplink_%d",Xuplink);
                uplink_correlation->GetXaxis()->SetTitle(tmp);
                sprintf(tmp,"uplink_%d",Yuplink);
                uplink_correlation->GetYaxis()->SetTitle(tmp);
                m_uplink_correlation.push_back(uplink_correlation);
                //printf("add m_uplink_correlation, now m_uplink_correlation.size() = %d",m_uplink_correlation.size());
            }
        }
    }

    if(uplinksToBeAnalyzed.size() == 4) {
        sprintf(tmp, "LayerCorr1_triggerU%d_U%d_signalU%d",m_uplinkId.at(0),m_uplinkId.at(1),m_uplinkId.at(2));
        m_2layerCorrHisto1 = new TH3I(tmp, tmp, NUMBER_OF_STRIPS_TRIGGERLAYER, STRIP_BASE_TRIGGERLAYER_PART1, STRIP_BASE_TRIGGERLAYER_PART1+NUMBER_OF_STRIPS_TRIGGERLAYER,NUMBER_OF_EFFECT_CHANNELS,STRIP_BASE_SIGNALLAYER,STRIP_BASE_SIGNALLAYER+NUMBER_OF_EFFECT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
        m_2layerCorrHisto1->GetXaxis()->SetTitle("triggerLayer_strip#");
        m_2layerCorrHisto1->GetYaxis()->SetTitle("signalLayer_strip#");
        m_2layerCorrHisto1->GetZaxis()->SetTitle("adcValue");

        sprintf(tmp, "LayerCorr2_triggerU%d_U%d_signalU%d",m_uplinkId.at(0),m_uplinkId.at(1),m_uplinkId.at(3));
        m_2layerCorrHisto2 = new TH3I(tmp, tmp, NUMBER_OF_STRIPS_TRIGGERLAYER, STRIP_BASE_TRIGGERLAYER_PART1, STRIP_BASE_TRIGGERLAYER_PART1+NUMBER_OF_STRIPS_TRIGGERLAYER,NUMBER_OF_EFFECT_CHANNELS,STRIP_BASE_SIGNALLAYER,STRIP_BASE_SIGNALLAYER+NUMBER_OF_EFFECT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
        m_2layerCorrHisto2->GetXaxis()->SetTitle("triggerLayer_strip#");
        m_2layerCorrHisto2->GetYaxis()->SetTitle("signalLayer_strip#");
        m_2layerCorrHisto2->GetZaxis()->SetTitle("adcValue");


        sprintf(tmp, "signalLayer_side1_strip_U%d", m_uplinkId.at(2));
        m_signalLayerSide1_strip = new TH2I(tmp, tmp, NUMBER_OF_EFFECT_CHANNELS,STRIP_BASE_SIGNALLAYER,STRIP_BASE_SIGNALLAYER+NUMBER_OF_EFFECT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
        m_signalLayerSide1_strip->GetXaxis()->SetTitle("strip");
        m_signalLayerSide1_strip->GetYaxis()->SetTitle("adcValue");

        sprintf(tmp, "signalLayer_side2_strip_U%d", m_uplinkId.at(3));
        m_signalLayerSide2_strip = new TH2I(tmp, tmp, NUMBER_OF_EFFECT_CHANNELS,STRIP_BASE_SIGNALLAYER,STRIP_BASE_SIGNALLAYER+NUMBER_OF_EFFECT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
        m_signalLayerSide2_strip->GetXaxis()->SetTitle("strip");
        m_signalLayerSide2_strip->GetYaxis()->SetTitle("adcValue");


        sprintf(tmp, "signalLayer_side1_chan_U%d",m_uplinkId.at(2));
        m_signalLayerSide1_chan= new TH2I(tmp, tmp, VATA64_READOUT_CHANNELS, .0, VATA64_READOUT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
        m_signalLayerSide1_chan->GetXaxis()->SetTitle("channel");
        m_signalLayerSide1_chan->GetYaxis()->SetTitle("adcValue");

        sprintf(tmp, "signalLayer_side2_U%d",m_uplinkId.at(3));
        m_signalLayerSide2_chan= new TH2I(tmp, tmp, VATA64_READOUT_CHANNELS, .0, VATA64_READOUT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
        m_signalLayerSide2_chan->GetXaxis()->SetTitle("channel");
        m_signalLayerSide2_chan->GetYaxis()->SetTitle("adcValue");

        sprintf(tmp, "s1Histo_chan_U%d",m_uplinkId.at(2));
        m_s1Histo_chan= new TH2I(tmp, tmp, VATA64_READOUT_CHANNELS, .0, VATA64_READOUT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
        m_s1Histo_chan->GetXaxis()->SetTitle("channel");
        m_s1Histo_chan->GetYaxis()->SetTitle("adcValue");

        sprintf(tmp, "s2Histo_chan_U%d",m_uplinkId.at(3));
        m_s2Histo_chan= new TH2I(tmp, tmp, VATA64_READOUT_CHANNELS, .0, VATA64_READOUT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
        m_s2Histo_chan->GetXaxis()->SetTitle("channel");
        m_s2Histo_chan->GetYaxis()->SetTitle("adcValue");

        sprintf(tmp, "s1Histo_strip_U%d", m_uplinkId.at(2));
        m_s1Histo_strip = new TH2I(tmp, tmp, NUMBER_OF_EFFECT_CHANNELS,STRIP_BASE_SIGNALLAYER,STRIP_BASE_SIGNALLAYER+NUMBER_OF_EFFECT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
        m_s1Histo_strip->GetXaxis()->SetTitle("strip");
        m_s1Histo_strip->GetYaxis()->SetTitle("adcValue");

        sprintf(tmp, "s2Histo_strip_U%d", m_uplinkId.at(3));
        m_s2Histo_strip = new TH2I(tmp, tmp, NUMBER_OF_EFFECT_CHANNELS,STRIP_BASE_SIGNALLAYER,STRIP_BASE_SIGNALLAYER+NUMBER_OF_EFFECT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
        m_s2Histo_strip->GetXaxis()->SetTitle("strip");
        m_s2Histo_strip->GetYaxis()->SetTitle("adcValue");
    }

    //m_resultLog = fopen("lect_uplinks_result_log.txt","w");

}


void DataManager::WriteResults()
{
    TH1 *hE = NULL;
    char tmp[256];
    //open a file
    FILE* resultLog = NULL;
    resultLog = fopen("../data/results_in_strip.txt","w");
    fprintf(resultLog,"strip N_uplink N_ch P_uplink P_ch rms_N_pedestal rms_P_pedestal <N>/<P> slope");
    fprintf(resultLog," MPV_ADC_N MPV_ADC_P chi2_N chi2_N ADC_per_PE PE_per_MIP area_N area_P\n");

    for(int i=0;i<m_strip_correlation.size();++i){
        TH2 *raw_pedestal_N = m_raw_pedestals.at(uplinkIndex(m_map_key_strip_value_N_uplink[i]));
        sprintf(tmp,"N_U%d_chan%d",m_map_key_strip_value_N_uplink[i],m_map_key_strip_value_N_ch[i]);
        hE = raw_pedestal_N->ProjectionY(tmp,m_map_key_strip_value_N_ch[i]+1,m_map_key_strip_value_N_ch[i]+1,"");
        Float_t rms_N=hE->GetRMS();
        TH2 *raw_pedestal_P = m_raw_pedestals.at(uplinkIndex(m_map_key_strip_value_P_uplink[i]));
        sprintf(tmp,"P_U%d_chan%d",m_map_key_strip_value_P_uplink[i],m_map_key_strip_value_P_ch[i]);
        hE = raw_pedestal_P->ProjectionY(tmp,m_map_key_strip_value_P_ch[i]+1,m_map_key_strip_value_P_ch[i]+1,"");
        Float_t rms_P=hE->GetRMS();

        Float_t ratio_N_P = (m_strip_correlation[i]->GetMean(2) / m_strip_correlation[i]->GetMean(1));
        TF1 *func = new TF1("func","[0]*x",0+4*rms_N,3000);
        m_strip_correlation[i]->Fit("func","RQ");
        Float_t slope=func->GetParameter(0);
        //std::cout<<"m_map_key_strip_value_P_MPVadc[i]="<<m_map_key_strip_value_P_MPVadc[i]<<"\n";
        fprintf(resultLog,"%d %d %d ",i,m_map_key_strip_value_N_uplink[i],m_map_key_strip_value_N_ch[i]);
        fprintf(resultLog,"%d %d %f %f %f %f ",m_map_key_strip_value_P_uplink[i],m_map_key_strip_value_P_ch[i],rms_N,rms_P,ratio_N_P,slope);
        fprintf(resultLog,"%f %f %f %f ",m_map_key_strip_value_N_MPVadc[i],m_map_key_strip_value_P_MPVadc[i],m_map_key_strip_value_N_chi2[i],m_map_key_strip_value_P_chi2[i]);
        fprintf(resultLog,"%f %f %f %f\n",m_map_key_strip_value_N_ADCPerPE[i],m_map_key_strip_value_N_MPVinPE[i],m_map_key_strip_value_N_area[i],m_map_key_strip_value_P_area[i]);
    }
    hE = NULL;
    fclose(resultLog);
}

void DataManager::findRatioLedP()
{
    TH1 *hE = NULL;
    char tmp[256];
    for(int i=0;i<m_LedP_strip_correlation.size();++i){
        ratioLedP[i]=(m_LedP_strip_correlation[i]->GetMean(1) / m_LedP_strip_correlation[i]->GetMean(2));

        TH2 *raw_pedestal_N = m_raw_pedestals.at(uplinkIndex(m_map_key_strip_value_N_uplink[i]));
        sprintf(tmp,"N_U%d_chan%d",m_map_key_strip_value_N_uplink[i],m_map_key_strip_value_N_ch[i]);
        hE = raw_pedestal_N->ProjectionY(tmp,m_map_key_strip_value_N_ch[i]+1,m_map_key_strip_value_N_ch[i]+1,"");
        Float_t rms_N=hE->GetRMS();
        TH2 *raw_pedestal_P = m_raw_pedestals.at(uplinkIndex(m_map_key_strip_value_P_uplink[i]));
        sprintf(tmp,"P_U%d_chan%d",m_map_key_strip_value_P_uplink[i],m_map_key_strip_value_P_ch[i]);
        hE = raw_pedestal_P->ProjectionY(tmp,m_map_key_strip_value_P_ch[i]+1,m_map_key_strip_value_P_ch[i]+1,"");
        Float_t rms_P=hE->GetRMS();

        m_LedP_strip_correlation[i]->GetYaxis()->SetRangeUser(0+2*rms_P,3500);
        m_LedP_strip_correlation[i]->GetXaxis()->SetRangeUser(0+2*rms_N,3500);
        TF1 *func = new TF1("func","[0]*x",0+2*rms_N,3500);
        m_LedP_strip_correlation[i]->Fit("func","RQ");
        ratioLedfitP[i] = 1./func->GetParameter(0);
        m_LedP_strip_correlation[i]->Write();
        }
      hE = NULL;
}

void DataManager::findRatioLedN()
{
    TH1 *hE = NULL;
    char tmp[256];
    for(int i=0;i<m_LedN_strip_correlation.size();++i){
      ratioLedN[i]=(m_LedN_strip_correlation[i]->GetMean(1) / m_LedN_strip_correlation[i]->GetMean(2));

      TH2 *raw_pedestal_N = m_raw_pedestals.at(uplinkIndex(m_map_key_strip_value_N_uplink[i]));
      sprintf(tmp,"N_U%d_chan%d",m_map_key_strip_value_N_uplink[i],m_map_key_strip_value_N_ch[i]);
      hE = raw_pedestal_N->ProjectionY(tmp,m_map_key_strip_value_N_ch[i]+1,m_map_key_strip_value_N_ch[i]+1,"");
      Float_t rms_N=hE->GetRMS();
      TH2 *raw_pedestal_P = m_raw_pedestals.at(uplinkIndex(m_map_key_strip_value_P_uplink[i]));
      sprintf(tmp,"P_U%d_chan%d",m_map_key_strip_value_P_uplink[i],m_map_key_strip_value_P_ch[i]);
      hE = raw_pedestal_P->ProjectionY(tmp,m_map_key_strip_value_P_ch[i]+1,m_map_key_strip_value_P_ch[i]+1,"");
      Float_t rms_P=hE->GetRMS();

      m_LedN_strip_correlation[i]->GetYaxis()->SetRangeUser(0+2*rms_P,3500);
      m_LedN_strip_correlation[i]->GetXaxis()->SetRangeUser(0+2*rms_N,3500);
      TF1 *func = new TF1("func","[0]*x",0+2*rms_N,3500);
      m_LedN_strip_correlation[i]->Fit("func","RQ");
      ratioLedfitN[i] = 1./func->GetParameter(0);
      m_LedN_strip_correlation[i]->Write();
        }
      hE = NULL;
}

void DataManager::findRatio(){
    float ratio, ratiofit;
    int DAC_adjust;
    //open a file to log the ratio and new DAC values
    FILE* result_ratio_MPPC_MAPD = NULL;
    result_ratio_MPPC_MAPD = fopen("../data/result_ratio_MPPC_MAPD.txt","w");
    fprintf(result_ratio_MPPC_MAPD,"uplink\t chan\t strip\t ratioLedN\t ratioLedP\t ratio\t ratiofit\t setDAC\n");

    for(unsigned int i=0; i<m_uplinkId.size();i++) {
        if (m_map_key_uplinkID_value_N_or_P[m_uplinkId.at(i)] == 0) {
            int setDAC[64]; // array to instore the DAC values
            for(int j=0; j<64;j++) {
                if(j==0) setDAC[j] = 0;
                else setDAC[j]=DAC_NOMINAL;
            }

            //////////////// read in the DAC values from file////////////////////////////////////////////////////////////////
            if(m_dacFromCfgFile || m_dacFromAdjustFile){
                std::ostringstream pathToUsedDAC;
                if(m_dacFromCfgFile) pathToUsedDAC << m_pathToDACFile <<"/VATA64V2_uplink"<<m_uplinkId.at(i)<<".cfg";
                else pathToUsedDAC << m_pathToDACFile <<"/adjustDAC_uplink" << m_uplinkId.at(i) <<".txt";

                std::fstream dacFile(pathToUsedDAC.str().c_str(),std::fstream::in|std::fstream::out);

                if(!dacFile) {
                    std::cerr<<"dacFile =  "<<pathToUsedDAC.str()<<" doesn't exist! exit abnormal!\n"<<std::endl;
                    assert(false);
                }else{
                    std::cout<<"dacFile = "<<pathToUsedDAC.str()<<std::endl;
                }

                std::string s;

                while(true){
                    std::getline(dacFile,s);
                    if(dacFile.eof()) break;

                    const char* searchString = s.c_str();

#ifdef DUMP_DEBUG
                    //std::cout<<"s = "<< s <<std::endl;
                    getchar();
#endif

                    if(s.find("#") ==0)  continue; //skip the comment line

                    if(sscanf(searchString, "Preamplifier_input_potential_DAC  || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d ||",
                              &setDAC[0], &setDAC[1], &setDAC[2], &setDAC[3], &setDAC[4],&setDAC[5], &setDAC[6],&setDAC[7], &setDAC[8],&setDAC[9], &setDAC[10],&setDAC[11], &setDAC[12],
                              &setDAC[13], &setDAC[14],&setDAC[15], &setDAC[16], &setDAC[17], &setDAC[18], &setDAC[19],&setDAC[20], &setDAC[21], &setDAC[22],&setDAC[23], &setDAC[24],
                              &setDAC[25], &setDAC[26],&setDAC[27], &setDAC[28], &setDAC[29], &setDAC[30], &setDAC[31],&setDAC[32], &setDAC[33],&setDAC[34],&setDAC[35], &setDAC[36],
                              &setDAC[37],&setDAC[38], &setDAC[39], &setDAC[40], &setDAC[41], &setDAC[42],&setDAC[43],&setDAC[44],&setDAC[45], &setDAC[46],&setDAC[47], &setDAC[48],
                              &setDAC[49], &setDAC[50],&setDAC[51], &setDAC[52],&setDAC[53], &setDAC[54], &setDAC[55],&setDAC[56],&setDAC[57], &setDAC[58],&setDAC[59],&setDAC[60],
                              &setDAC[61], &setDAC[62],&setDAC[63])==64){
                        //printf("read in setDACs from %s\n",pathToUsedDAC.str().c_str());
                        //for(int i = 0; i<64; i++) {printf("setDAC[%d] = %d ",i,setDAC[i]);if(i%6 ==0 &&i!=0) printf("\n");}
                        //printf("\n");
                    }
                }

                dacFile.close();
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;chan++){

                if(m_readInSetupFile){
                    if((chan%CHANNELS_PER_GROUP)>(STRIPS_PER_GROUP-1)) continue;  //only fit for SiPM output
                }

                std::pair<int,int> pair_N_uplink_ch(m_uplinkId.at(i),chan);
                int strip = m_key_N_uplink_ch_value_strip[pair_N_uplink_ch];
                ratio = pow((ratioLedP[strip] * ratioLedN[strip]),0.5);
                ratiofit = pow((ratioLedfitP[strip] * ratioLedfitN[strip]),0.5);
                DAC_adjust = floor((pow(ratio_ideal/ratiofit,0.5) - 1)*overvoltage_ideal_MAPD*255);
                // R/R_ideal = (overvoltage_ideal / (overvoltage_ideal + er))^2 --> er = (sqrt(R_ideal/R) -1) x overvoltage_ideal  --> DAC_adjust = (sqrt(R_ideal/R) - 1) x overvoltage_ideal x 255
                // we assume the response is proportional to overvoltage^2

                // make sure that the DAC value will never be out of range!!
                if( (setDAC[chan]+DAC_adjust)<1 )  setDAC[chan] = 1;
                else if((setDAC[chan]+DAC_adjust)>256) setDAC[chan] = 255;
                else setDAC[chan] += DAC_adjust;

                //printf("uplink=%d chan=%d strip=%d ratioLedN=%f ratioLedP=%f ratio=%f ratiofit=%f setDAC[%d] = %d\n",m_uplinkId.at(i),chan,strip,ratioLedN[strip],ratioLedP[strip],ratio,ratiofit,chan,setDAC[chan]);
                fprintf(result_ratio_MPPC_MAPD,"%d\t %d\t %d\t %f\t %f\t %f\t %f\t %d\n",m_uplinkId.at(i),chan,strip,ratioLedN[strip],ratioLedP[strip],ratio,ratiofit,setDAC[chan]);
#ifdef DUMP_DEBUG
                printf("setDAC[%d] = %d\n",chan,setDAC[chan]);
#endif
            }

            //open a file to log the DAC values
            if(m_dacFromAdjustFile){
                std::ostringstream pathToNewDAC;
                if(m_dacFromCfgFile) pathToNewDAC <<"../data/adjustDAC_uplink"<<m_uplinkId.at(i)<<".txt";
                else pathToNewDAC << m_pathToDACFile <<"/adjustDAC_uplink" << m_uplinkId.at(i) <<".txt";

                std::fstream newDacFile(pathToNewDAC.str().c_str(),std::fstream::in|std::fstream::out);
                //std::cout<<"newDacFile = "<<pathToNewDAC.str()<<std::endl;

                if(!newDacFile) {
                    std::cerr<<"saving new DACs: newDacFile =  "<<pathToNewDAC.str()<<" doesn't exist! exit abnormal!\n"<<std::endl;
                    assert(false);
                }else{
                    //std::cout<<"DAC is read from adjust file, modified DACs in this file: newDacFile =  "<<pathToNewDAC.str()<<std::endl;
                }

                newDacFile.seekg(0,std::ios::beg);// move read point to the head of the file
                long posR = newDacFile.tellg();
#ifdef DUMP_DEBUG
                printf("before reading get point is newDacFile.tellg() = %ld,\n",posR);
                printf("before reading put point is newDacFile.tellp() = %ld,\n",newDacFile.tellp());
#endif

                std::string s;

                while(true){
                    posR = newDacFile.tellg();
#ifdef DUMP_DEBUG
                    printf("posR = %ld\n",posR);
                    getchar();
#endif


                    std::getline(newDacFile,s);
                    if(newDacFile.eof()) break;

#ifdef DUMP_DEBUG
                    //std::cout<<"s = "<< s <<std::endl;
                    getchar();
#endif

                    if(s.find("#") ==0)  continue; //skip the comment line
                    if(s.find("Preamplifier_input_potential_DAC") == 0) {
                        newDacFile.seekp(posR);
                        newDacFile<<"#";
                    }
                }
                newDacFile.clear();
                newDacFile.seekp(0,std::ios::end);
                newDacFile<<"#For uplinkId= "<<m_uplinkId.at(i)<<", aimGain = "<<m_aimGain<<",try the following DAC settings"<<std::endl;
                newDacFile<<"Preamplifier_input_potential_DAC ";
                for(int i = 0; i<64;i++) {
                    newDacFile<<" || "<< setDAC[i]<<" ||";
                    if(i==63) newDacFile<<std::endl;
                }
                newDacFile.close();

            }

            // if need to write new DAC values to the cfg file
            if(m_forceToChangeCfgFile){
                std::ostringstream pathToCfgFile;
                pathToCfgFile << m_pathToFormerCfgFile <<"/VATA64V2_uplink" << m_uplinkId.at(i) <<".cfg";

                std::fstream cfgFile(pathToCfgFile.str().c_str(),std::fstream::in|std::fstream::out);
                std::cout<<"cfgFile = "<<pathToCfgFile.str()<<std::endl;

                if(!cfgFile) {
                    std::cerr<<"To be modified file: cfgFile =  "<<pathToCfgFile.str()<<" doesn't exist! exit abnormal!\n"<<std::endl;
                    assert(false);
                }else{
                    std::cout<<"To be modified file: cfgFile =  "<<pathToCfgFile.str()<<std::endl;
                }

                cfgFile.seekg(0,std::ios::beg);// move read point to the head of the file
                long posR = cfgFile.tellg();
#ifdef DUMP_DEBUG
                printf("before reading get point is newDacFile.tellg() = %ld,\n",posR);
                printf("before reading put point is newDacFile.tellp() = %ld,\n",cfgFile.tellp());
#endif

                std::string s;
                while(true){
                    posR = cfgFile.tellg();
#ifdef DUMP_DEBUG
                    printf("posR = %ld\n",posR);
                    getchar();
#endif


                    std::getline(cfgFile,s);
                    if(cfgFile.eof()) break;

#ifdef DUMP_DEBUG
                    std::cout<<"s = "<< s <<std::endl;
                    getchar();
#endif

                    if(s.find("#") ==0)  continue; //skip the comment line
                    // comment all the old DAC values
                    if(s.find("Preamplifier_input_potential_DAC") == 0) {
                        cfgFile.seekp(posR);
                        cfgFile<<"#";
                    }
                }
                cfgFile.clear();
                cfgFile.seekp(0,std::ios::end);
                cfgFile<<"#For uplinkId = "<<m_uplinkId.at(i)<<", aimGain = "<<m_aimGain<<", try the following DAC settings"<<std::endl;
                cfgFile<<"Preamplifier_input_potential_DAC ";
                for(int i = 0; i<64;i++) {
                    cfgFile<<" || "<< setDAC[i]<<" ||";
                    if(i==63) cfgFile<<std::endl;
                }
                cfgFile.close();
            }
        }
    }
    fclose(result_ratio_MPPC_MAPD);
}
