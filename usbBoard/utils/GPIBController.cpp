#include "GPIBController.h"

// Keep this order!
#include <windows.h>
#include <ni488.h>

#include <conio.h>
#include <stdio.h>

// Toggle GPIB read/write debugging helper
#define DEBUG_GPIB 0

// Debug mode to simulate GPIB communication, ignoring any errors
#define IGNORE_ERRORS 0

// Device name table
const char GPIBController::s_deviceName[8] = { "HP8110A" };

// Primary address table
const int GPIBController::s_primaryAddress = 5; /* HP8110A */

// Secondary address constant
const int GPIBController::s_secondaryAddress = 0;

GPIBController::GPIBController()
    : m_board(0)
    , m_device(0)
    , m_startTime(0)
{
}

void GPIBController::failWithGPIBError(char* msg)
{
#if IGNORE_ERRORS > 0
    return;
#endif

    printf("\n%s\n", msg);
    printf("ibsta = 0x%X  <", ibsta);

    if (ibsta & ERR )  printf(" ERR" );
    if (ibsta & TIMO)  printf(" TIMO");
    if (ibsta & END )  printf(" END" );
    if (ibsta & SRQI)  printf(" SRQI");
    if (ibsta & RQS )  printf(" RQS" );
    if (ibsta & CMPL)  printf(" CMPL");
    if (ibsta & LOK )  printf(" LOK" );
    if (ibsta & REM )  printf(" REM" );
    if (ibsta & CIC )  printf(" CIC" );
    if (ibsta & ATN )  printf(" ATN" );
    if (ibsta & TACS)  printf(" TACS");
    if (ibsta & LACS)  printf(" LACS");
    if (ibsta & DTAS)  printf(" DTAS");
    if (ibsta & DCAS)  printf(" DCAS");

    printf(" >\n");
    printf("iberr = %d", iberr);

    if (iberr == EDVR) printf(" EDVR <DOS error>\n");
    if (iberr == ECIC) printf(" ECIC <Not Controller-In-Charge>\n");
    if (iberr == ENOL) printf(" ENOL <No Listener>\n");
    if (iberr == EADR) printf(" EADR <Address error>\n");
    if (iberr == EARG) printf(" EARG <Invalid argument>\n");
    if (iberr == ESAC) printf(" ESAC <Not System Controller>\n");
    if (iberr == EABO) printf(" EABO <Operation aborted>\n");
    if (iberr == ENEB) printf(" ENEB <No GPIB board>\n");
    if (iberr == EOIP) printf(" EOIP <Async I/O in progress>\n");
    if (iberr == ECAP) printf(" ECAP <No capability>\n");
    if (iberr == EFSO) printf(" EFSO <File system error>\n");
    if (iberr == EBUS) printf(" EBUS <Command error>\n");
    if (iberr == ESTB) printf(" ESTB <Status byte lost>\n");
    if (iberr == ESRQ) printf(" ESRQ <SRQ stuck on>\n");
    if (iberr == ETAB) printf(" ETAB <Table Overflow>\n");

    printf("ibcntl = %ld\n", ibcntl);
    ibonl(m_device, 0);

    ibonl(m_board, 0);
    exit(1);
}

void GPIBController::initialize()
{
    // Access GPIB board
    m_board = ibfindA("GPIB0");
    if (m_board < 0)
        failWithGPIBError("ibfind error");

    // Set the board's primary address to 0
    ibpad(m_board, 0);
    if (ibsta & ERR)
        failWithGPIBError("ibpad error");

    // Become system controller
    ibrsc(m_board, 1);
    if (ibsta & ERR)
        failWithGPIBError("ibrsc error");

    // Become controller in charge
    ibsic(m_board);
    if (ibsta & ERR)
        failWithGPIBError("ibsic error");

    // Raise the remote enable line
    ibsre(m_board, 1);
    if (ibsta & ERR)
        failWithGPIBError("ibsre error");

    short found = 0;

    // Detect if the device is on the bus
    ibln(m_board, s_primaryAddress, s_secondaryAddress, &found);
    if (!found)
        printf("No listener detected at pad=%d (%s)\n", s_primaryAddress, s_deviceName);

    // Create a unit descriptor handle
    m_device = ibdev(0, s_primaryAddress, s_secondaryAddress, T10s, 1, 0);
    if (ibsta & ERR)
        failWithGPIBError("ibdev error");
}

void GPIBController::initializeSignalGenerator()
{
    writeData("*RST");
    
    writeData(":ARM:SOUR EXT1");
    writeData(":ARM:SENS LEV");
    writeData(":ARM:LEV -400MV");
    writeData(":ARM:SLOP POS");

    writeData(":TRIG:COUN 1");
    writeData(":TRIG:SOUR INT1");

    writeData(":FREQ 1KHz");
    
    writeData(":PULS:WIDT1 10US");
    writeData(":PULS:WIDT2 50NS");

    char message[s_maximumMessageBufferSize] = { 0 };
    sprintf(message, ":PULS:DEL1 %dNS", signalDelay);
    writeData(message);
    sprintf(message, ":PULS:DEL2 %dNS", triggerDelay);
    writeData(message);
  
    writeData(":HOLD VOLT");
    writeData(":VOLT1:HIGH 50MV");
    writeData(":VOLT1:LOW 0V");

    writeData(":VOLT1:HIGH 50MV");
    writeData(":VOLT1:LOW 0V");
    writeData(":OUTP1:POL NORM");
    writeData(":OUTP1 OFF");

    writeData(":VOLT2:HIGH 0V");
    writeData(":VOLT2:LOW -800MV");
    writeData(":OUTP2:POL INV");
    writeData(":OUTP2 ON");

    armSignalGenerator();
}

void GPIBController::armSignalGenerator()
{
#if IGNORE_ERRORS > 0
    return;
#endif

    do {
        // Query waits until all pending operations are finished
        writeData("*OPC?");
        readData();
    } while(!atoi(m_buffer));
}

void GPIBController::setAmplitude(int amplitude)
{
    if (amplitude != 0) {
        char message[s_maximumMessageBufferSize] = { 0 };
        sprintf(message, ":VOLT1:HIGH %dMV", amplitude);
        writeData(message);
        writeData(":OUTP1 ON");
    } else {
        writeData(":OUTP1 OFF");
    }
    armSignalGenerator();
}

void GPIBController::readData()
{
    ibrd(m_device, m_buffer, s_maximumBufferSize);
    m_buffer[ibcntl] = '\0';

#if DEBUG_GPIB > 0
    printf("GPIB, read from (%s) <<<< '%s'\n", s_deviceName, m_buffer);
#endif

    if (ibsta & ERR)
        failWithGPIBError("ibrd error");
}

void GPIBController::writeData(char* data)
{
#if DEBUG_GPIB > 0
    printf("GPIB, write to  (%s) >>>> '%s'\n", s_deviceName, data);
#endif

    ibwrt(m_device, data, strlen(data));
    if (ibsta & ERR)
        failWithGPIBError("ibwrt error");
}

void GPIBController::releaseDevices()
{
    // Release the device handle
    ibonl(m_device, 0);
    if (ibsta & ERR)
        failWithGPIBError("ibonl error");

    // Release the board handle
    ibonl(m_board, 0);
    if (ibsta & ERR)
        failWithGPIBError("ibonl error");
}
