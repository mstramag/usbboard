# @file   processTestBeamAugust.py
# @author Albert Puig (albert.puig@epfl.ch)
# @date   15.10.2012
""""""
import os

signalList = [
                "/home/albert/Data/data_testbeam_10_2012/data_Run7082_nonirrad_71_irrad_72.6_long_73.25.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7083_nonirrad_70.76_irrad_72.45_long_72.90.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7084_nonirrad_71.00_irrad_72.58_long_72.90.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7085_nonirrad_71.00_irrad_72.58_long_72.90.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7086_nonirrad_71.25_irrad_72.98_long_73.25.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7087_nonirrad_71.5_irrad_73.98_long_73.25.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7088_nonirrad_71.73_irrad_74.06_long_72.90.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7089_nonirrad_71.73_irrad_74.06_long_72.90.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7090_nonirrad_71.98_irrad_74.84_long_72.90_nomirror.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7091_nonirrad_71.25_irrad_73.00_long_73.25_nomirror.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7092_nonirrad_71.25_irrad_73.00_long_73.25_nomirror.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7093_nonirrad_71.25_irrad_73.00_rot10_long_73.25_nomirror.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7094_nonirrad_71.25_irrad_73.00_rot15_long_72.90_nomirror.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7096_nonirrad_71.25_irrad_73.00_pos2_long_72.90_nomirror.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7097_nonirrad_71.25_irrad_73.00_pos4_long_73.25_nomirror.root",
                "/home/albert/Data/data_testbeam_10_2012/data_run7099_test1_long_73.25_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_run7099_test_long_73.25_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7100_nonirrad_71.80_irrad_74.15_pos2_-5C_long_73.25_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7101_nonirrad_71.32_irrad_73.18_pos3_-5C_long_73.25_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7102_nonirrad_71.56_irrad_73.62_pos3_-5C_long_72.90_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7103_nonirrad_71.56_irrad_73.62_pos3_-5C_long_72.90_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7104_nonirrad_71.79_irrad_74.13_pos3_-5C_long_73.25_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7105_nonirrad_71.79_irrad_74.13_pos3_-5C_long_73.25_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7106_nonirrad_72.02_irrad_74.77_pos3_-5C_long_72.90_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7107_nonirrad_72.02_irrad_74.77_pos3_-5C_long_72.90_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7108_nonirrad_72.26_irrad_75.57_pos3_-5C_long_73.25_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7109_nonirrad_72.49_irrad_76.6_pos3_-5C_long_73.25_middle_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7110_nonirrad_72.72_irrad_78.45_pos3_-5C_long_72.90_middle_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7111_nonirrad_71.70_irrad_74.13_pos3_-5C_long_72.90_farout_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7111_nonirrad_72.72_irrad_78.45_pos3_-5C_long_72.90_far_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7112_nonirrad_71.70_irrad_74.13_pos3_-5C_long_73.25_far_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7113_nonirrad_71.70_irrad_74.13_pos2_-5C_long_73.25_far_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7114_nonirrad_71.70_irrad_74.13_pos2_-5C_long_73.25_far_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7115_nonirrad_71.25_irrad_73.1_pos2_-15C_long_72.90_far_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7116_nonirrad_71.25_irrad_73.1_pos2_-15C_long_72.90_far_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7118_nonirrad_71.25_irrad_73.1_pos3_-15C_long_72.90_far_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7121_nonirrad_71.25_irrad_73.1_pos3_-15C_long_72.90_far_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7122_nonirrad_71.25_irrad_73.1_pos3_-15C_long_72.90_far_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7123_nonirrad_71.25_irrad_73.1_pos3_-15C_long_72.90_far_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7124_nonirrad_71.25_irrad_73.1_pos3_-15C_long_72.90_far_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7125_nonirrad_71.25_irrad_73.1_pos3_-15C_long_72.90_far_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/data_Run7126_nonirrad_71.25_irrad_73.1_pos3_-15C_long_72.90_far_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/led_afterRun7082.root",
                "/home/albert/Data/data_testbeam_10_2012/led_afterRun7083.root",
                "/home/albert/Data/data_testbeam_10_2012/led_afterRun7085.root",
                "/home/albert/Data/data_testbeam_10_2012/led_afterRun7086.root",
                "/home/albert/Data/data_testbeam_10_2012/led_afterRun7087.root",
                "/home/albert/Data/data_testbeam_10_2012/led_afterRun7089.root",
                "/home/albert/Data/data_testbeam_10_2012/led_afterRun7090.root",
                "/home/albert/Data/data_testbeam_10_2012/pedestal_afterRun7082_nonirrad_71_irrad_72.6_long_73.25.root",
                "/home/albert/Data/data_testbeam_10_2012/pedestal_afterRun7083_nonirrad_70.76_irrad_72.45_long_72.90.root",
                "/home/albert/Data/data_testbeam_10_2012/pedestal_afterRun7085_nonirrad_71.00_irrad_72.58_long_72.90.root",
                "/home/albert/Data/data_testbeam_10_2012/pedestal_afterRun7086_nonirrad_71.25_irrad_72.98_long_73.25.root",
                "/home/albert/Data/data_testbeam_10_2012/pedestal_afterRun7087_nonirrad_71.5_irrad_73.98_long_73.25.root",
                "/home/albert/Data/data_testbeam_10_2012/pedestal_afterRun7089_nonirrad_71.73_irrad_74.06_long_72.90.root",
                "/home/albert/Data/data_testbeam_10_2012/pedestal_after_run7101_nonirrad_71.32_irrad_73.18_pos3_-5C_long_73.25_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/pedestal_after_run7105_nonirrad_71.79_irrad_74.13_pos3_-5C_long_73.25_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/pedestal_after_Run7108_nonirrad_72.26_irrad_75.57_pos3_-5C_long_73.25_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/pedestal_after_Run7110_nonirrad_72.72_irrad_78.45_pos3_-5C_long_72.90_middle_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/pedestal_before_run7099_nonirrad_71.25_irrad_73.00_pos2_-5C_long_73.25_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/pedestal_before_run7102_nonirrad_71.56_irrad_73.62_pos3_-5C_long_72.90_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/pedestal_before_run7106_nonirrad_72.02_irrad_74.77_pos3_-5C_long_72.90_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/pedestal_before_Run7109_nonirrad_72.49_irrad_76.6_pos3_-5C_long_73.25_angle45.root",
                "/home/albert/Data/data_testbeam_10_2012/pedestal_Run7090_nonirrad_71.98_irrad_74.84_long_72.90_nomirror.root",
        ]

outDir = '$HOME/Data/Processed/TestBeam_Oct'

if __name__ == '__main__':
    for signalFile in signalList:
        outName = os.path.join(outDir, os.path.splitext(os.path.split(signalFile)[1])[0] + '_converted.root')
        if not os.path.exists(outName):
            try:
                os.system('/home/albert/usbboard/usbBoard/Builds/analysis -s%s -o%s -t%s setup.txt &> %s' % (signalFile, outName, outName.replace('.root', '.txt'), outName.replace('.root', '.log')) )
            except:
                print "Problems processing %s." % signalFile
        #with open(outName.replace('.root', '.log')) as f:
            #print "-"*40
            #print outName.replace('.root','')
            #for line in f:
                #if 'eventsPerAccess' in line:
                    #print "  ->", line
            #print "-"*40

# EOF
