// @file   Helpers.h
// @author Albert Puig (albert.puig@epfl.ch)
// @date   18.09.2012

#ifndef HELPERS_H
#define HELPERS_H

#include <sstream>
#include <stdexcept>
#include <string>

namespace Helpers
{
inline std::string intToStr( int n ) {
  std::ostringstream result;
  result << n;
  return result.str();
}

inline int strToInt( const std::string& s ) {
  int result;
  std::istringstream ss( s );
  ss >> result;
  if ( !ss ) throw std::invalid_argument( "strToInt" );
  return result;
}

template<class T>
std::string StringFrom(T const& t) {
  std::string ret;

  std::stringstream stream;
  stream << t;
  stream >> ret;

  return ret;
}

template <typename Iter>
Iter next(Iter iter) { return ++iter; }

template <typename Iter, typename Cont> // http://stackoverflow.com/questions/3516196/testing-whether-an-iterator-points-to-the-last-item
bool isLast(Iter iter, const Cont& cont) { return (iter != cont.end()) && (next(iter) == cont.end()) ; }

template<typename Obj>
void printVector(std::vector<Obj> vec, std::string delim) {
   for (typename std::vector<Obj>::const_iterator it = vec.begin() ; it != vec.end(); it++) {
     std::cout << *it ;
     if (!isLast(it, vec)) std::cout << delim ;
   }
   std::cout << std::endl ;
}

inline std::string trimStr(std::string const& source, char const* delims = " \t\r\n") {
  std::string result(source);
  std::string::size_type index = result.find_last_not_of(delims);
  if(index != std::string::npos)
    result.erase(++index);

  index = result.find_first_not_of(delims);
  if(index != std::string::npos)
    result.erase(0, index);
  else
    result.erase();
  return result;
}
}

#endif

// EOF
