# @file   processTestBeamAugust.py
# @author Albert Puig (albert.puig@epfl.ch)
# @date   15.10.2012
""""""
import os

signalList = [
        "/home/albert/Data/data_testbeam_8_2012/dataTestbeam/Thursday23Aug2012/Combinedrunwithtelescope/Data/Position10/position10_TRun6319_temp27p2_yuri28p5_240812.root",
        "/home/albert/Data/data_testbeam_8_2012/dataTestbeam/Thursday23Aug2012/Combinedrunwithtelescope/Data/Position11/position11_TRun6320_temp27p1_yuri28p4_240812.root",
        "/home/albert/Data/data_testbeam_8_2012/dataTestbeam/Thursday23Aug2012/Combinedrunwithtelescope/Data/Position1/position1_TRun6311_temp27p4_yuri30p3_240812.root",
        "/home/albert/Data/data_testbeam_8_2012/dataTestbeam/Thursday23Aug2012/Combinedrunwithtelescope/Data/Position2/position2_TRun6308_temp27p6_yuri30p3_230812.root",
        "/home/albert/Data/data_testbeam_8_2012/dataTestbeam/Thursday23Aug2012/Combinedrunwithtelescope/Data/Position3/position3_TRun6307_temp27p5_yuri30p3_230812.root",
        "/home/albert/Data/data_testbeam_8_2012/dataTestbeam/Thursday23Aug2012/Combinedrunwithtelescope/Data/Position4/position4_TRun6312_temp27p3_yuri29p7_240812.root",
        "/home/albert/Data/data_testbeam_8_2012/dataTestbeam/Thursday23Aug2012/Combinedrunwithtelescope/Data/Position6/position6_TRun6315_temp27p1_yuri28p8_240812.root",
        "/home/albert/Data/data_testbeam_8_2012/dataTestbeam/Thursday23Aug2012/Combinedrunwithtelescope/Data/Position7/position7_TRun6316_temp27p1_yuri28p8_240812.root",
        "/home/albert/Data/data_testbeam_8_2012/dataTestbeam/Thursday23Aug2012/Combinedrunwithtelescope/Data/Position8/position8_TRun6317_temp27p1_yuri28p7_240812.root",
        "/home/albert/Data/data_testbeam_8_2012/dataTestbeam/Thursday23Aug2012/Combinedrunwithtelescope/Data/Position9/position9_TRun6318_temp27p1_yuri28p6_240812.root",
        ]

pedestalFile = '/home/albert/Data/data_testbeam_8_2012/dataTestbeam/Thursday23Aug2012/Ledgainchecking/pedestal_26p7.root'

if __name__ == '__main__':
    for signalFile in signalList:
        outName = os.path.split(signalFile)[0].split('/')[-1].lower() + '.root'
        if not os.path.exists(outName):
            os.system('/home/albert/usbboard/usbBoard/Builds/analysis -s%s -o%s setup.txt &> %s' % (signalFile, outName, outName.replace('.root', '.log')) )
        os.system('scp %s ')
        #with open(outName.replace('.root', '.log')) as f:
            #print "-"*40
            #print outName.replace('.root','')
            #for line in f:
                #if 'eventsPerAccess' in line:
                    #print "  ->", line
            #print "-"*40
    if not os.path.exists('pedestal.root'):
        os.system('/home/albert/usbboard/usbBoard/Builds/analysis -p%s -opedestal.root setup.txt &> pedestal.log' % pedestalFile )

# EOF
