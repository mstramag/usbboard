# @file   processIrradiatedPedestalsOct.py
# @author Albert Puig (albert.puig@epfl.ch)
# @date   15.10.2012
""""""

import os

signalList = [
        "/home/albert/Data/data_lab_epfl/pedestal_rad_Gain30.root",
        "/home/albert/Data/data_lab_epfl/pedestal_rad_Gain40.root",
        "/home/albert/Data/data_lab_epfl/pedestal_rad_Gain50.root",
        "/home/albert/Data/data_lab_epfl/pedestal_rad_Gain60.root",
        "/home/albert/Data/data_lab_epfl/pedestal_rad_Gain70.root",
        "/home/albert/Data/data_lab_epfl/pedestal_rad_Gain80.root",
        "/home/albert/Data/data_lab_epfl/pedestal_rad_Gain90.root",
        "/home/albert/Data/data_lab_epfl/pedestal_rad_Gain100.root",
        ]

if __name__ == '__main__':
    for signalFile in signalList:
        outName = ''.join(['irradiatedPedestal_', os.path.split(signalFile)[1].split('_')[-1]]).lower()
        if not os.path.exists(outName):
            os.system('/home/albert/usbboard/usbBoard/Builds/analysis -s%s -o%s setupIrradiated.txt &> %s' % (signalFile, outName, outName.replace('.root', '.log')) )
        with open(outName.replace('.root', '.log')) as f:
            print "-"*40
            print outName.replace('.root','')
            for line in f:
                if 'eventsPerAccess' in line:
                    print "  ->", line
            print "-"*40

# EOF
