// @file   Geometry.cpp
// @author Albert Puig (albert.puig@epfl.ch)
// @date   16.09.2012

#include "Geometry.h"

BaseGeometries BaseGeometryWithChildren::getChildren() {
  BaseGeometries output;
  for ( BaseGeometries::const_iterator child = m_children.begin(); child != m_children.end() ; child++ ) {
    output.push_back(*child);
  }
  return output;
}

int BaseGeometryWithChildren::addChild(BaseGeometry* child) {
  // We don't add if we are already at max capacity!
  if ( 0 != m_maxChildren && m_children.size() == m_maxChildren ) return 0 ;
  m_children.push_back(child) ;
  child->setParent(this) ;
  return m_children.size() ;
}

BaseGeometry* BaseGeometryWithChildren::getChild(std::string name){
  for ( BaseGeometries::const_iterator child = m_children.begin() ; child != m_children.end() ; child++ ){
    if ( !name.compare((*child)->getName()) ) return (*child) ;
  }
  return NULL ;
}

Uplink::~Uplink() {} ;

strings Uplink::getRepresentation(bool numberOnTop=true){
  strings lines;
  std::string lineNumber = Helpers::intToStr(this->getLinkID());
  lineNumber.resize(4, ' ') ;
  std::string lineRepr = "____";
  if ( numberOnTop ) {
    lines.push_back(lineNumber);
    lines.push_back(lineRepr)  ;
  } else {
    lines.push_back(lineRepr)  ;
    lines.push_back(lineNumber);
  }
  return lines;
}

SiPm::~SiPm() {} ;

Uplinks SiPm::getUplinks() {
  Uplinks output;
  for ( BaseGeometries::const_iterator it = m_children.begin(); it != m_children.end() ; it++ ) {
    Uplink* uplink = (Uplink*) (*it) ;
    output.push_back(uplink);
  }
  return output;
}

strings SiPm::getRepresentation(bool numberOnTop=true) {
  strings lines;
  std::string lineUp   = "";
  std::string lineDown = "";
  Uplinks uplinks = getUplinks();
  for (Uplinks::const_iterator uplink = uplinks.begin(); uplink != uplinks.end(); uplink++ ) {
    strings linesTemp = (*uplink)->getRepresentation(numberOnTop) ;
    lineUp   += linesTemp.at(0);
    lineDown += linesTemp.at(1);
  }
  lines.push_back(lineUp) ;
  lines.push_back(lineDown) ;
  return lines;
}

int SiPm::reorder( const unsigned short adcsUnordered[], unsigned short adcs[] ) {
  int channelA ;
  int channelB ;
  for ( int channel = 0 ; channel < 64; channel++ ) {
    channelA = (channel%fChannelsPerHalfConnector)*8+(channel/fChannelsPerHalfConnector)*4+(channel/fChannelsPerConnector)*2-(channel/fChannelsPerConnector)*8;
    channelB = (channel%fChannelsPerHalfConnector)*8+(channel/fChannelsPerHalfConnector)*4+(channel/fChannelsPerConnector)*2+1-(channel/fChannelsPerConnector)*8;
    //printf("Here we are\n");
    //if (!isReversed()) {
  //    adcs[channelB] = adcsUnordered[channel] ; 
//      adcs[channelA] = adcsUnordered[channel+64] ;
 //   } else {
  //      adcs[127-channelB] = adcsUnordered[channel] ;
   //     adcs[127-channelA] = adcsUnordered[channel+64] ;
   // }
    //adcs[channelB] = adcsUnordered[channel] ;   // Guido this was before 14.2.!
    //adcs[channelA] = adcsUnordered[channel+64] ;
    adcs[channelA] = adcsUnordered[channel] ;  // changed by Guido &Zri 14.2.2013 card A even uplink, B odd uplinks !
    adcs[channelB] = adcsUnordered[channel+64] ;  // D'ont change this anymore!!!!

        //std::cout<<"A "<<std::endl; 
        //std::cout<<channel<<" "<<channelA<<std::endl;
        //adcs[channelB] = adcsUnordered[channel+64] ;
        //std::cout<<"B "<<std::endl;
        //std::cout<<channel<<" "<<channelB<<std::endl;
  }
  return 0 ;
}

Layer::~Layer() {} ;

SiPms Layer::getSiPms() {
  SiPms output;
  for ( BaseGeometries::const_iterator it = m_children.begin(); it != m_children.end() ; it++ ) {
    SiPm *sipm = (SiPm*) (*it) ;
    output.push_back(sipm);
  }
  return output;
}

strings Layer::getRepresentation(bool numberOnTop=true) {
  strings lines;
  std::string lineUp   = "";
  std::string lineDown = "";
  SiPms sipms = getSiPms();
  for (SiPms::const_iterator sipm = sipms.begin(); sipm != sipms.end(); sipm++ ) {
    strings linesTemp = (*sipm)->getRepresentation(numberOnTop) ;
    lineUp   += "|" ;
    lineUp   += linesTemp.at(0) ;
    lineDown += "|" ;
    lineDown += linesTemp.at(1) ;
  }
  lineUp   += "|" ;
  lineDown += "|" ;
  lines.push_back(lineUp) ;
  lines.push_back(lineDown) ;
  return lines;
}

Plane::~Plane() {} ;

Layers Plane::getLayers() {
  Layers output;
  for ( BaseGeometries::const_iterator it = m_children.begin(); it != m_children.end() ; it++ ) {
    Layer *layer = (Layer*) (*it) ;
    output.push_back(layer);
  }
  return output;
}

strings Plane::getRepresentation(bool=0) {
  strings lines;
  strings linesTemp;
  bool onTop = true ;
  for (BaseGeometries::const_iterator it = m_children.begin(); it != m_children.end(); it++) {
     linesTemp = ((Layer*) (*it))->getRepresentation(onTop) ;
     for (strings::const_iterator str = linesTemp.begin(); str != linesTemp.end(); str++){
       lines.push_back(*str) ;
     }
     onTop = !onTop ;
  }
  return lines ;
}

Module::~Module() {} ;

Planes Module::getPlanes() {
  Planes output;
  for ( BaseGeometries::const_iterator it = m_children.begin(); it != m_children.end() ; it++ ) {
    Plane *plane = (Plane*) (*it) ;
    output.push_back(plane);
  }
  return output;
}

strings Module::getRepresentation(bool=0) {
  strings lines;
  strings linesTemp;
  lines.push_back("") ;
  for (BaseGeometries::const_iterator it = m_children.begin(); it != m_children.end(); it++) {
    linesTemp = ((Plane*) (*it))->getRepresentation() ;
    for (strings::const_iterator str = linesTemp.begin(); str != linesTemp.end(); str++){
      lines.push_back(*str) ;
    }
    lines.push_back("") ;
  }
  return lines ;
}

Detector::~Detector() {} ;

Modules Detector::getModules() {
  Modules output;
  for ( BaseGeometries::const_iterator it = m_children.begin(); it != m_children.end() ; it++ ) {
    Module *module = (Module*) (*it) ;
    output.push_back(module);
  }
  return output;
}

strings Detector::getRepresentation(bool=0) {
  strings lines;
  strings linesTemp;
  int maxLineSize = 0;
  for (BaseGeometries::const_iterator it = m_children.begin(); it != m_children.end(); it++) {
    linesTemp = ((Module*) (*it))->getRepresentation() ;
    for (strings::const_iterator str = linesTemp.begin(); str != linesTemp.end(); str++){
      maxLineSize = std::max(maxLineSize, (int) (*str).size()) ;
      lines.push_back(*str) ;
    }
  }
  std::string delimiter = "" ;
  delimiter.append(maxLineSize, '-') ;
  lines.insert(lines.begin(), delimiter) ;
  lines.push_back(delimiter) ;
  return lines ;
}

void Detector::show() {
  strings lines = getRepresentation() ;
  for (strings::const_iterator line = lines.begin(); line != lines.end(); line++) {
    std::cout << (*line) << std::endl ;
  }
}

int Detector::loadFromConfig(std::string configPath) {
  //std::cout << "Using config file " << configPath << std::endl ;
  ConfigFile *cfg = new ConfigFile(configPath) ;
  strings detectorElements = cfg->getKeys() ;
  //for(strings::const_iterator i=detectorElements.begin(); i!=detectorElements.end();++i){
    //std::cout<< "detectorElements "<<(*i)<<std::endl;
  //}
  for (strings::const_iterator element = detectorElements.begin(); element != detectorElements.end(); element++){
    // Split and assign names
    strings subelements;
    boost::algorithm::split_regex( subelements, *element, boost::regex("_") ) ;
    std::string uplinkStr;
    if ( subelements.size() != 0 ) {
      uplinkStr = subelements.back() ;
      subelements.pop_back() ;
    } else return 0 ; // Uplinks are required
    //std::cout<<"uplinkStr "<<uplinkStr<<std::endl;
    std::string sipmStr;
    if ( subelements.size() != 0 ) {
      sipmStr = subelements.back() ;
      subelements.pop_back() ;
    } else return 0 ; // SiPms are required
    //std::cout<<"sipmStr "<<sipmStr<<std::endl;
    std::string layerStr;
    if ( subelements.size() != 0 ) {
      layerStr = subelements.back() ;
      subelements.pop_back() ;
    } else layerStr = "" ;
    //std::cout<<"layerStr "<<layerStr<<std::endl;
    std::string planeStr;
    if ( subelements.size() != 0 ) {
      planeStr = subelements.back() ;
      subelements.pop_back() ;
    } else planeStr = "" ; // SiPms are required
    //std::cout<<"planeStr "<<planeStr<<std::endl;
    std::string moduleStr;
    if ( subelements.size() != 0 ) {
      moduleStr = subelements.back() ;
      subelements.pop_back() ;
    } else moduleStr = "" ; // SiPms are required
    //std::cout<<"moduleStr "<<moduleStr<<std::endl;
    // Build hierarchy
    std::string fullStr = moduleStr;
    Module *module = (Module*) this->getChild(fullStr);
    if (!module) {
      module = new Module(fullStr) ;
      this->addModule(module) ;
    }
    if ( planeStr.size() > 0 ) fullStr += planeStr ;
    Plane *plane = (Plane*) module->getChild(fullStr);
    if (!plane) {
      plane = new Plane(fullStr) ;
      module->addPlane(plane) ;
    }
    if ( planeStr.size() > 0 ) fullStr += "_" ;
    if ( layerStr.size() > 0 ) fullStr += layerStr ;
    Layer *layer = (Layer*) plane->getChild(fullStr);
    if (!layer) {
      layer = new Layer(fullStr) ;
      plane->addLayer(layer) ;
    }
    if ( layerStr.size() > 0 ) fullStr += "_" ;
    fullStr += sipmStr ;
    SiPm *sipm = (SiPm*) layer->getChild(fullStr);
    if (!sipm) {
      sipm = new SiPm(fullStr) ;
      sipm->setNChannels(128) ;
      layer->addSiPm(sipm) ;
    }
    fullStr += "_" + uplinkStr ;
    //std::cout<<"fullStr "<<fullStr<<std::endl;
    strings values = cfg->getValue(fullStr) ;
    //std::cout<<"size of values = "<<values.size()<<std::endl;
    //for(strings::const_iterator i=values.begin(); i!=values.end();++i){
      //std::cout<< "values "<<(*i)<<"  ";
    //} 
    //std::cout<<std::endl;
    int linkID = Helpers::strToInt(values.at(0)) ;
    bool isReversed = false ;
    if (values.size() > 1) isReversed = bool(Helpers::strToInt(values.at(1))) ;
    //std::cout<<"linkID "<<linkID<<"  isReversed "<<isReversed<<std::endl;
    Uplink *uplink = new Uplink(fullStr, linkID, isReversed) ;
    sipm->addChild(uplink) ;
  }
  delete cfg;
  return 1 ;
}

// EOF

