// @file   RunData.cpp
// @author Albert Puig (albert.puig@epfl.ch)
// @date   15.09.2012

#include "RunData.h"

RunData::RunData () : m_signalTree(NULL), m_pedestalTree(NULL) {};

RunData::RunData (std::string signalFile, std::string pedestalFile) :
	m_signalFile(NULL),
	m_pedestalFile(NULL),
	m_signalTree(NULL),
	m_pedestalTree(NULL),
	m_settings(NULL) {
  // Load TTrees
  if ( 0 != signalFile.size() ) { // Let's load signal
    std::cout << "Getting signal from " << signalFile << std::endl ;
    m_signalFile = new TFile(signalFile.c_str());
    if (m_signalFile->IsZombie()) {
      std::cout << "Could not load " << signalFile << std::endl ;
      throw 20 ;
    };
    m_signalTree = (TTree*) (m_signalFile->Get("ReadoutData"));;
    if ( !m_signalTree ) throw 30 ;
  }
  if ( 0 != pedestalFile.size() ) { // Let's load pedestal
    std::cout << "Getting pedestal from " << pedestalFile << std::endl ;
    m_pedestalFile = new TFile(pedestalFile.c_str());
    if (m_pedestalFile->IsZombie()) {
      std::cout << "Could not load " << pedestalFile << std::endl ;
      throw 20 ;
    };
    m_pedestalTree = (TTree*) (m_pedestalFile->Get("ReadoutData"));;
    if ( !m_pedestalTree ) throw 30 ;
  }
  // Load Settings
  RunDescription *runDescription = NULL ;
  if (m_pedestalTree) runDescription = (RunDescription*) (m_pedestalTree->GetUserInfo()->First());
  else		      runDescription = (RunDescription*) (m_signalTree->GetUserInfo()->First());
  std::cout << "Dumping Run Description..." << std::endl;
  runDescription->dump(std::cout);
  std::stringstream settingsStream(runDescription->settings());
  m_settings = Settings::instance();
  m_settings->loadFromStream(settingsStream);
  m_initialTime = runDescription->timeStamp() ;
};

int RunData::process(Detector* detector, std::string signalOutFileName, std::string pedestalOutFileName, std::string timestampFileName){
  int          entry ;
  // Cache SiPms from the detector, so we can reorder and avoid looping too much on each event
  SiPms                          sipmCache ;
  std::map<int, UplinkSettings*> uplinkCache ;
  int boardId = -1 ;
  int maxChannels = 0;
  TFile *tmp = new TFile("_tmp.root", "RECREATE") ;
  std::ofstream timestampFile ;
  bool saveTimestamps = false ;
  if ( 0 != timestampFileName.size() ) saveTimestamps = true ;
  //if ( 0 != timestampFileName.size() ) timestampFile.open( timestampFileName.c_str() ) ;
  // Get info from Geometry
  Modules modules = detector->getModules() ;
  for ( Modules::const_iterator module = modules.begin() ; module != modules.end() ; module++ ) {
    Planes planes = (*module)->getPlanes() ;
    for ( Planes::const_iterator plane = planes.begin() ; plane != planes.end() ; plane++ ) {
      Layers layers = (*plane)->getLayers() ;
      for ( Layers::const_iterator layer = layers.begin() ; layer != layers.end() ; layer++ ) {
        SiPms sipms = (*layer)->getSiPms() ;
        // Append the returned sipms at the end of the cache
        //sipmCache.reserve(sipmCache.size() + std::distance(sipms.begin(),sipms.end()));
        //sipmCache.insert(sipmCache.end(), sipms.begin(), sipms.end());
        for ( SiPms::const_iterator sipm = sipms.begin() ; sipm != sipms.end() ; sipm++ ) {
          int nChannels = 0 ;
          Uplinks uplinks = (*sipm)->getUplinks() ;
          if ( uplinks.size() > 0 ) {
	    sipmCache.push_back(*sipm) ;
          }
          std::pair<std::map<int, UplinkSettings*>::iterator,bool> sc ;
          for ( Uplinks::const_iterator uplink = uplinks.begin() ; uplink != uplinks.end() ; uplink++ ) {
            UplinkSettings *upSettings = new UplinkSettings ;
            upSettings->name   = (*uplink)->getName()   ;
            upSettings->linkID = (*uplink)->getLinkID() ;
            if ( boardId < 0 ) boardId = floor(upSettings->linkID/10.0) ;
            int usbBoardId = m_settings->usbBoardIdFromUplinkId((*uplink)->getLinkID());
            upSettings->sampleSize = m_settings->usbBoardConfiguration(usbBoardId).sampleSize();
            upSettings->channelsPerReadoutChip = m_settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
            upSettings->numberOfReadoutChips = upSettings->sampleSize / upSettings->channelsPerReadoutChip;
            sc = uplinkCache.insert( std::pair<int, UplinkSettings*>( upSettings->linkID, upSettings ) ) ;
            if ( false == sc.second ) std::cout << "Problem insterting uplink " << upSettings->linkID << std::endl ;
            nChannels += upSettings->sampleSize ;
          }
          (*sipm)->setNChannels(nChannels) ;
          maxChannels = std::max(maxChannels, nChannels) ;
        }
      }
    }
  }
  uint32_t prevTimestamp  ;
  uint32_t thisTimestamp  ;
  uint32_t prevOverflow = 0  ;
  uint32_t thisOverflow = 0  ;
  std::cout << "Finished analysis of settings" << std::endl ;
  // Write Uplink cache to TTree
  TTree* settingsTree = new TTree("UplinkInfo", "Tree containing the information of all uplinks") ;
  UplinkSettings *settingsForTree = new UplinkSettings() ;
  int            linkID ;
  unsigned short sampleSize ;
  unsigned short channelsPerReadoutChip ;
  int            numberOfReadoutChips ;
  std::string    name ;
  settingsTree->Branch("linkID"                , &linkID, "linkID/I") ;
  settingsTree->Branch("sampleSize"            , &sampleSize, "sampleSize/s") ;
  settingsTree->Branch("channelsPerReadoutChip", &channelsPerReadoutChip, "channelsPerReadoutChip/s") ;
  settingsTree->Branch("numberOfReadoutChips"  , &numberOfReadoutChips, "numberOfReadoutChips/I") ;
  settingsTree->Branch("name"                  , &name) ;
  for (std::map<int, UplinkSettings*>::const_iterator mapIt = uplinkCache.begin() ; mapIt != uplinkCache.end(); mapIt++) {
    settingsForTree = (*mapIt).second ;
    linkID = settingsForTree->linkID;
    sampleSize = settingsForTree->sampleSize;
    channelsPerReadoutChip= settingsForTree->channelsPerReadoutChip ;
    numberOfReadoutChips = settingsForTree->numberOfReadoutChips ;
    name = settingsForTree->name ;
    settingsTree->Fill() ;
  }
  // Also, create TTrees and TBranches to fill with pedestal and signal
  TTree *outPedestal = NULL ;
  TTree *outSignal   = NULL ;
  if ( m_pedestalTree ) outPedestal = new TTree("PedestalData", "Tree to store pedestal data") ;
  if ( m_signalTree )   outSignal   = new TTree("SignalData", "Tree to store signal data") ;
  const int nSiPms = sipmCache.size() ;
  unsigned short* adcs [nSiPms];
  unsigned short* adcsUnordered [nSiPms];
  int itSiPm = 0 ;
  TBranch* b;
  for ( SiPms::const_iterator sipm = sipmCache.begin() ; sipm != sipmCache.end() ; sipm++, itSiPm++ ) {
    std::string name = (*sipm)->getName() ;
    adcs[itSiPm]          = (unsigned short*) malloc(maxChannels*sizeof(unsigned short));
    adcsUnordered[itSiPm] = (unsigned short*) malloc(maxChannels*sizeof(unsigned short));
    if ( m_pedestalTree ) b = (TBranch*) outPedestal->Branch( name.c_str(), adcs[itSiPm], std::string(name+"["+Helpers::intToStr(maxChannels)+"]/s").c_str() ) ;
    if ( m_signalTree )   b = (TBranch*) outSignal->Branch( name.c_str(), adcs[itSiPm], std::string(name+"["+Helpers::intToStr(maxChannels)+"]/s").c_str() ) ;
  }
  b = 0 ;
  MergedEvent *event = 0 ;
  // Let's start with the pedestal
  if ( m_pedestalTree ) {
    std::cout << "Dumping pedestal" << std::endl ;
    m_pedestalTree->SetBranchAddress("events", &event);
    entry = 0 ;
    while ( m_pedestalTree->GetEntry(entry++) ) {
      itSiPm = 0 ;
      for ( SiPms::const_iterator sipm = sipmCache.begin() ; sipm != sipmCache.end() ; sipm++, itSiPm++ ) {
        int nChannelBase = 0 ;
        Uplinks uplinks = (*sipm)->getUplinks() ;
        for ( Uplinks::const_iterator uplink = uplinks.begin() ; uplink != uplinks.end() ; uplink++ ) {
          UplinkSettings* upSettings = uplinkCache[(*uplink)->getLinkID()] ;
          const UplinkData& uplinkData = event->uplinkData(upSettings->linkID);
          for (unsigned short chip = 0; chip < upSettings->numberOfReadoutChips; ++chip) {
            for (unsigned short channel = 0; channel < upSettings->channelsPerReadoutChip; ++channel) {
              unsigned short sample = chip * upSettings->channelsPerReadoutChip + channel;
              adcsUnordered[itSiPm][nChannelBase + sample] = uplinkData.adcValue(sample);
            }
            nChannelBase += upSettings->sampleSize ;
          }
          //if ( 10 == entry ) uplinkData.dump(std::cout) ;
        }
        // Order de ADCs
        if(detector->m_pebs) {
			(*sipm)->reorder_pebs(adcsUnordered[itSiPm], adcs[itSiPm]) ;
		} else {
			(*sipm)->reorder(adcsUnordered[itSiPm], adcs[itSiPm]) ;
		}
        //if ( 0 == entry ) { std::cout << std::endl << (*sipm)->getName() << " " ; for (int i=0 ; i < 128 ; i++) std::cout << adcs[itSiPm][i] << " " ; }
      }
      outPedestal->Fill() ;
    }
    //outPedestal->SetEntries(entry) ;
  }
  event = 0 ;
  int nDifferent = 0 ;
  if ( m_signalTree ) {
    std::cout << "Dumping signal" << std::endl ;
    m_signalTree->SetBranchAddress("events", &event);
    entry = 0 ;
    //while ( m_signalTree->GetEntry(entry++) ) {
    while ( m_signalTree->GetEntry(entry++) && entry<20000) {
      itSiPm = 0 ;
      for ( SiPms::const_iterator sipm = sipmCache.begin() ; sipm != sipmCache.end() ; sipm++, itSiPm++ ) {
        int nChannelBase = 0 ;
        Uplinks uplinks = (*sipm)->getUplinks() ;
        for ( Uplinks::const_iterator uplink = uplinks.begin() ; uplink != uplinks.end() ; uplink++ ) {
          UplinkSettings* upSettings = uplinkCache[(*uplink)->getLinkID()] ;
          const UplinkData& uplinkData = event->uplinkData(upSettings->linkID);
          for (unsigned short chip = 0; chip < upSettings->numberOfReadoutChips; ++chip) {
            for (unsigned short channel = 0; channel < upSettings->channelsPerReadoutChip; ++channel) {
              unsigned short sample = chip * upSettings->channelsPerReadoutChip + channel;
              adcsUnordered[itSiPm][nChannelBase + sample] = uplinkData.adcValue(sample);
            }
            nChannelBase += upSettings->sampleSize ;
          }
          //if ( 10 == entry ) uplinkData.dump(std::cout) ;
        }
      	// Order de ADCs
      	if(detector->m_pebs) {
			(*sipm)->reorder_pebs(adcsUnordered[itSiPm], adcs[itSiPm]) ;
		} else {
			(*sipm)->reorder(adcsUnordered[itSiPm], adcs[itSiPm]) ;
		}
      //  if ( 10 == entry ) { std::cout << std::endl << (*sipm)->getName() << " " ; for (int i=0 ; i < 128 ; i++) std::cout << adcsUnordered[itSiPm][i] << " " ; }
      //  if ( 0 == entry ) { std::cout << std::endl << (*sipm)->getName() << " " ; for (int i=0 ; i < 128 ; i++) std::cout << adcs[itSiPm][i] << " " ; std::cout << std::endl;}
      }
      // Write the timestamp data (if necessary)
      // Timestamps
      char line[100];
      if ( saveTimestamps ) {
        //std::cout << entry ;
        int iter ;
        if ( 0 == entry ) prevTimestamp = 0 ;
        thisTimestamp = getTimestamp(event, boardId) ;
        if ( thisTimestamp < prevTimestamp ) thisOverflow++ ;
        if ( (buildFullTimestamp(thisTimestamp, thisOverflow) - buildFullTimestamp(prevTimestamp, prevOverflow)) > 1e8 ) { if (timestampFile.is_open() ) timestampFile.close() ; }
        itSiPm = 0 ;
        if ( !timestampFile.is_open() ) {
            time_t timeBeginningOfSpill = 1e-8*buildFullTimestamp(thisTimestamp, thisOverflow) + m_initialTime ;
            char timeToChar[16] ;
            struct tm *timeinfo ;
            strftime(timeToChar, 16, "%Y%m%d_%H%M%S", localtime(&timeBeginningOfSpill)) ;
            std::string timeToString ;
            {
            std::stringstream ss ;
            ss << timeToChar ;
            ss >> timeToString ;
            }
            timestampFile.open( (timestampFileName + std::string("_") + timeToString + std::string(".txt")).c_str() ) ;
        }
        timestampFile << "# Event " << entry << "\n" ;
        for ( SiPms::const_iterator sipm = sipmCache.begin() ; sipm != sipmCache.end(); sipm++, itSiPm++ ) {
          std::string id = (*sipm)->getParent()->getName() ;
          int startChannel = (*sipm)->getStartChannel() ;
          for ( int i=0; i<(*sipm)->getNChannels(); i++ ) {
            sprintf(line, "%i   %u    %" PRIu64 "    %s\n", i+startChannel, (unsigned int) adcs[itSiPm][i], buildFullTimestamp(thisTimestamp, thisOverflow), id.c_str()) ;
            timestampFile << line ;
            //timestampFile << i+startChannel << "   " << (unsigned int) adcs[itSiPm][i] << "   " << prevTimestamp << "    " << id << "\n" ;
          }
        }
        //std::cout << std::endl ;
        prevTimestamp  = thisTimestamp ;
        prevOverflow  = thisOverflow ;
      }
      /*if((*adcs[0])>400) {
		  outSignal->Fill() ;
	  }*/
      // Fill tuple
	  outSignal->Fill() ;
    }
    //outSignal->SetEntries(entry) ;
  }
  // Write the file
  std::cout << "Saving output" << std::endl ;
  // Create output file
  bool writeSignal = ( 0 != signalOutFileName.size() ); // Just write if we have a filename
  if ( m_pedestalTree && (0 != pedestalOutFileName.size()) ) { // Just write if we have a filename
    TFile *outPedestalFile = new TFile(pedestalOutFileName.c_str(), "NEW") ;
    if ( outPedestalFile->IsZombie() ) {
      std::cout << "Problems opening the output file, you should check it!" << std::endl;
      return 0 ;
    }
    TTree *settingsTreeNew = settingsTree->CloneTree() ;
    TTree *pedestalTreeNew = outPedestal->CloneTree() ;
    if ( m_signalTree ) {
      if ( 0 == pedestalOutFileName.compare(signalOutFileName) )  {
        TTree *signalTreeNew = outSignal->CloneTree() ;
        writeSignal = false ;
      }
    }
    outPedestalFile->Write("", TObject::kOverwrite) ;
    outPedestalFile->Close() ;
  }
  if ( writeSignal ) {
    TFile *outSignalFile = new TFile(signalOutFileName.c_str(), "NEW") ;
    if ( outSignalFile->IsZombie() ) {
      std::cout << "Problems opening the output file, you should check it!" << std::endl;
      return 0 ;
    }
    TTree *settingsTreeNew2 = settingsTree->CloneTree() ;
    TTree *signalTreeNew    = outSignal->CloneTree() ;
    outSignalFile->Write("", TObject::kOverwrite) ;
  }
  // Cleanup
  tmp->Close() ;
  remove("_tmp.root") ;
  /*std::cout << "Cleaning up" << std::endl ;
  delete settingsForTree ;
  delete event ;
  for ( std::map<std::string, TBranch*>::iterator it = outPedestals.begin() ; it != outPedestals.end() ; it++ ) if ( (*it).second ) delete (*it) .second;
  for ( std::map<std::string, TBranch*>::iterator it = outSignals.begin() ; it != outSignals.end() ; it++ ) if ( (*it).second ) delete (*it).second ;
  delete outPedestal ;
  delete outSignal   ;
  delete outFile     ;*/
  if (timestampFile.is_open() ) timestampFile.close() ;
  std::cout << "Number of events with different timestamp " << nDifferent << "/" << entry-1 << std::endl ;
  return 1 ;
} ;

const uint32_t RunData::getTimestamp(MergedEvent* event, int id) {
  uint32_t timestamp = 0 ;
  for (int i=0; i < 8; i++){
    const UplinkData& uplinkData = event->uplinkData(10*id+i);
    timestamp += (uplinkData.headerWord2() & 0xF) << i*4 ;
    //std::cout << (uint32_t) ((uplinkData.headerWord2() & 0xF) << i*4) << " " << uplinkData.adcValue(34) << " / " ;
  }
//std::cout << " = " << timestamp << std::endl ;
  return timestamp ;
} ;

const uint64_t RunData::buildFullTimestamp(uint32_t leastSignificantWord, uint32_t mostSignificantWord){
  return (uint64_t) mostSignificantWord << 32 | leastSignificantWord ;
}

