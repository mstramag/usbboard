// @file   RunData.h
// @author Albert Puig (albert.puig@epfl.ch)
// @date   15.09.2012

#ifndef RunData_h
#define RunData_h

#define __STDC_FORMAT_MACROS

// ROOT imports
#include <TTree.h>
#include <TFile.h>
#include <TBranch.h>

#include <cmath>
#include <vector>
#include <map>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <assert.h>
#include <stdio.h>

// BOOST
#include <boost/lexical_cast.hpp>
#include <stdint.h>
#include <inttypes.h>

// My includes
#include "Geometry.h"
#include <RunDescription.h>
#include <MergedEvent.h>
#include <Settings.h>
#include <UplinkData.h>

typedef std::vector<unsigned short> AdcVector ;

class RunData {
  public:
    RunData();
    RunData(std::string, std::string);
    ~RunData() {};
    // Functions
    int process(Detector*, std::string, std::string, std::string) ;
    // Access to settings
    //int getUsbBoardIdFromUplinkId(int);
    //unsigned short getSampleSize(int) ;
    //unsigned short
    class UplinkSettings {
    public:
      UplinkSettings() {} ;
      ~UplinkSettings() {} ;
      std::string    name ;
      int            linkID ;
      unsigned short sampleSize ;
      unsigned short channelsPerReadoutChip ;
      unsigned short numberOfReadoutChips ;
    } ;

  private:
    TFile       *m_signalFile    ;
    TFile       *m_pedestalFile  ;
    TTree       *m_signalTree    ;
    TTree       *m_pedestalTree  ;
    Settings    *m_settings      ;
    time_t       m_initialTime   ;
    int          m_signalEntry   ;
    int          m_pedestalEntry ;
    const uint32_t getTimestamp(MergedEvent*, int) ;
    const uint64_t buildFullTimestamp(uint32_t, uint32_t) ;
};

#endif

// EOF
