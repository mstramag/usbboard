#include "ConfigFile.h"

#include <fstream>

ConfigFile::ConfigFile(std::string const& configFile) {
  std::ifstream file(configFile.c_str());

  std::string line ;
  int posSeparator;
  while (std::getline(file,line)) {
    // Check line
    //std::cout << line << std::endl ;
    if (! line.length()) continue;
    if (line[0] == '#') continue;
    if (line[0] == ';') continue;
    // Get data
    strings     values ;
    boost::algorithm::split_regex( values, line, boost::regex( "\\s*\\|\\|\\s*" ) ) ;
    if ( 0 == values.back().size() ) values.pop_back() ;
    // The key is the first element, the rest are values
    std::string key = values.at(0) ;
    values.erase(values.begin())   ;
    // Insert in map
    std::pair<std::map<std::string,strings>::iterator,bool> ret ;
    ret = m_content.insert(std::pair<std::string, strings>(key, values)); 
    if (false == ret.second) std::cout << "Duplicate element " << key << std::endl ;
    else m_keys.push_back(key) ;
  }
  // Sort keys
  m_keys = boost::sort(m_keys) ;
}

strings ConfigFile::getKeys() {
    return m_keys;
}

bool ConfigFile::hasKey(std::string const& key) const {
  return (m_content.end() != m_content.find(key));
}

strings ConfigFile::getValue(std::string const& key) const {
  strings val ;
  if ( m_content.end() == m_content.find(key) ) val = std::vector<std::string>() ;
  else                         			val = m_content.find(key)->second ;
  return val ;
}
