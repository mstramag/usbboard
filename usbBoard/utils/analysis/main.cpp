// @file   main.cpp
// @author Albert Puig (albert.puig@epfl.ch)
// @date   17.09.2012
#include "Geometry.h"
#include "RunData.h"
// Boost
#include <boost/program_options.hpp>
namespace po = boost::program_options;

int main(int argc, char* argv[]) {
  // Define command line options
  po::options_description desc("Program arguments");
  desc.add_options()
            ("help", "produce help message")
            ("setupFile", po::value<std::string>(), "path to the setup file")
            ("pedestalFile,p", po::value<std::string>(), "path to the pedestal file")
            ("signalFile,s", po::value<std::string>(), "path to the signal file")
            ("outputFile,o", po::value<std::string>(), "path to the output file")
            ("split", "split signal and pedestal in different files")
            ("timestamp,t", po::value<std::string>(), "save the timestamp information for the signal file")
            ("pebs", "analysis for pebs (no channel reordering)")
            //("forceDAC,f", "force modify of the DAC in the used config file")
            //("aimGain", po::value<int>()->default_value(12), "aim gain for calculation of the DACs (default=12)")
            //("LED,l", "analyze LED data")
            //("gain,G", po::value<int>()->default_value(0), "set operation GAIN for DACs (mandatory with -l and -c)")
            //("cluster,C", "apply clustering algorithm")
        ;
  po::positional_options_description p;
  p.add("setupFile", -1);
  // Parse the command line
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).
            options(desc).positional(p).run(),
            vm);
  po::notify(vm);
  if (vm.count("help")) {
    std::cout << desc << std::endl ;
    return 1 ;
  }
  // Check arguments
  if ( !((vm.count("pedestalFile")) || (vm.count("signalFile"))) || !(vm.count("setupFile")) || !(vm.count("outputFile"))) {
    std::cout << "Missing arguments!" << std::endl ;
    std::cout << desc << std::endl ;
    return 1 ;
  }
  // Load detector
  Detector *detector = new Detector("MyDetector") ;
  std::cout << "Loading detector..." << std::endl ;
  detector->loadFromConfig(vm["setupFile"].as<std::string>()) ;
  if (vm.count("pebs")) {
	  std::cout << "PEBS option chosen" << std::endl;
	  detector->SetPebs(true);
  }
  std::cout << "Loaded the following schema" << std::endl ;
  detector->show() ;
  // Load data
  std::string pedestalFile = "";
  if ( vm.count("pedestalFile") ) pedestalFile = vm["pedestalFile"].as<std::string>() ;
  std::string signalFile   = "";
  if ( vm.count("signalFile") ) signalFile = vm["signalFile"].as<std::string>() ;
  RunData *data = new RunData( signalFile, pedestalFile ) ;
  // Process
  std::string pedestalOut = "" ;
  std::string signalOut = ""   ;
  if ( vm.count("pedestalFile") && vm.count("signalFile") ) {
    if ( vm.count("split") ) {
      pedestalOut = "pedestal_" + vm["outputFile"].as<std::string>() ;
      signalOut   = "signal_"   + vm["outputFile"].as<std::string>() ;
    } else {
      pedestalOut = vm["outputFile"].as<std::string>() ;
      signalOut   = vm["outputFile"].as<std::string>() ;
    }
  } else if ( vm.count("pedestalFile") ) pedestalOut = vm["outputFile"].as<std::string>() ;
    else if ( vm.count("signalFile") )   signalOut = vm["outputFile"].as<std::string>() ;
  std::string timestampFile = "" ;
  if ( vm.count("timestamp") && vm.count("signalFile") ) {
    timestampFile = vm["timestamp"].as<std::string>() ;
    std::cout << "Storing output timestamps in " << timestampFile << std::endl ;
  }

  data->process(detector, signalOut, pedestalOut, timestampFile) ;

  return 0;
}
