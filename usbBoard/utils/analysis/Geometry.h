// @file   Geometry.h
// @author Albert Puig (albert.puig@epfl.ch)
// @date   16.09.2012

#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <string>
#include <vector>
#include <set>
#include <iostream>
#include <cmath>

#include <boost/regex.hpp>
#include <boost/algorithm/string/regex.hpp>
#include <boost/range/algorithm.hpp>

#include "Helpers.h"
#include "ConfigFile.h"

// Forward declarations
class BaseGeometry ;
class Uplink ;
class SiPm ;
class Layer ;
class Plane ;
class Module ;
class Detector ;

// Typedefs
typedef std::vector<std::string>   strings ;
typedef std::vector<BaseGeometry*> BaseGeometries ;
typedef std::vector<Uplink*>       Uplinks ;
typedef std::vector<SiPm*>         SiPms ;
typedef std::vector<Layer*>        Layers ;
typedef std::vector<Plane*>        Planes ;
typedef std::vector<Module*>       Modules ;

typedef std::vector<unsigned short> AdcVector ;

// Classes

class BaseGeometry {
  public:
    BaseGeometry(std::string name) : m_parent(NULL) { m_name = name; } ;
    ~BaseGeometry() {} ;
    // Functions
    std::string      getName() { return m_name; } ;
    virtual strings  getRepresentation(bool) = 0 ;
    BaseGeometry*    getParent() { return m_parent ; };
    bool    	     setParent(BaseGeometry* parent) { if ( m_parent ) { return false ; } else { return (m_parent = parent); } } ;

  private:
    std::string      m_name ;
    BaseGeometry*    m_parent ;
};

class BaseGeometryWithChildren : public BaseGeometry {
  public:
    BaseGeometryWithChildren(std::string name, int maxChildren=0) :
        BaseGeometry(name), m_maxChildren(maxChildren) {} ;
    ~BaseGeometryWithChildren() {} ;
    // Functions
    int                  addChild(BaseGeometry*) ;
    BaseGeometry*        getChild(std::string) ;
    BaseGeometries       getChildren() ;

  protected:
    BaseGeometries       m_children ;
    int                  m_maxChildren ;
};

class Uplink : public BaseGeometry {
  public:
    Uplink(std::string name) : BaseGeometry(name), m_linkID(0), m_reversed(false) {} ;
    Uplink(std::string name, int linkID, bool isReversed) : BaseGeometry(name), m_linkID(linkID), m_reversed(isReversed) {} ;
    ~Uplink() ;
    // Functions
    strings  getRepresentation(bool) ;
    bool isReversed()            { return m_reversed; } ;
    void setLinkID(int linkID) { m_linkID = linkID; } ;
    int  getLinkID()           { return m_linkID; } ;

  private:
    int   m_linkID;
    bool  m_reversed;

};

class SiPm : public BaseGeometryWithChildren {
  public:
    SiPm(std::string name) :
      BaseGeometryWithChildren(name, 2),
      fChannelsPerHalfConnector(16),
      fChannelsPerConnector(32),
      m_nChannels(0),
      m_startChannel(0) {} ;
    ~SiPm() ;
    // Functions
    strings  getRepresentation(bool) ;
    bool     isReversed() { return ((Uplink*) m_children.at(0))->isReversed(); } ;
    int      addUplink(Uplink* uplink) { return addChild(uplink) ; };
    Uplinks  getUplinks() ;
    int      reorder(const unsigned short[], unsigned short[]);
    int		 reorder_pebs(const unsigned short[], unsigned short[]);
    void     setNChannels(int nChannels) { m_nChannels = nChannels ; } ;
    int      getNChannels() { return m_nChannels ; } ;
    int      getStartChannel() { return m_startChannel ; }
    void     setStartChannel(int channel) { m_startChannel = channel ; }
   private:
    const int fChannelsPerHalfConnector ;
    const int fChannelsPerConnector ;
    int       m_nChannels;
    int       m_startChannel ;
} ;

//class SiPm_128 : public SiPm {
    //SiPm_128(std::string name) : SiPm(name) {} ;
    //~SiPm_128() ;
    //// Functions
    //int reorder(const int[], &int[]) ;
//}

class Layer : public BaseGeometryWithChildren {
  public:
    Layer(std::string name, int maxChildren=0) : BaseGeometryWithChildren(name, maxChildren), m_channelCounter(0) {} ;
    ~Layer() ;
    // Functions
    bool      isReversed() { return ((SiPm*) m_children.at(0))->isReversed(); } ;
    strings   getRepresentation(bool) ;
    int       addSiPm(SiPm* sipm) { sipm->setStartChannel(m_channelCounter) ; m_channelCounter += sipm->getNChannels() ; return addChild(sipm) ; }  ;
    SiPms     getSiPms() ;
  private:
    int       m_channelCounter ;
} ;

class Plane : public BaseGeometryWithChildren {
  public:
    Plane(std::string name, int maxChildren=0) : BaseGeometryWithChildren(name, maxChildren) {} ;
    ~Plane() ;
    // Functions
    strings  getRepresentation(bool) ;
    int      addLayer(Layer* layer) { return addChild(layer) ; } ;
    Layers   getLayers()  ;
} ;

class Module : public BaseGeometryWithChildren {
  public:
    Module(std::string name, int maxChildren=0) : BaseGeometryWithChildren(name, maxChildren) {} ;
    ~Module() ;
    // Functions
    strings  getRepresentation(bool) ;
    int      addPlane(Plane* plane) { return addChild(plane) ; } ;
    Planes   getPlanes()  ;
} ;

class Detector : public BaseGeometryWithChildren {
  public:
    Detector(std::string name, int maxChildren=0) : BaseGeometryWithChildren(name, maxChildren) {} ;
    ~Detector() ;
    // Functions
    strings     getRepresentation(bool) ;
    int         addModule(Module* module) { return addChild(module) ; } ;
    Modules     getModules()  ;
    void        show() ;
    int         loadFromConfig(std::string) ;
    bool		m_pebs;
    void		SetPebs(bool p=false) { m_pebs=p; }
} ;

#endif

// EOF

