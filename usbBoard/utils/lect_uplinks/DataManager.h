#ifndef DataManager_h
#define DataManager_h

#include <vector>
#include <TH1.h>
#include <TF1.h>
#include <TH3.h>
#include <TGraphErrors.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>
#include <map>
#include <stdint.h>

//#define DUMP_DEBUG


class MergedEvent;
class TH1;
class TH2;
class TH3;
class TH2F;
class TFile;
class TTree;
class TProfile;

//class PEBS09_TestbeamSetup;


Double_t fpeaks(Double_t *x, Double_t *par);
Double_t langaufun(Double_t *x, Double_t *par);
TF1 *langaufit(TH1D *his, Double_t *fitrange, Double_t *startvalues, Double_t *parlimitslo, Double_t *parlimitshi, Double_t *fitparams, Double_t *fiterrors, Double_t *ChiSqr, Int_t *NDF);

Double_t fconvo_func(Double_t *x, Double_t *par);

typedef std::map<int,int> SuperLayerMap; //map<positon,uplinkId>

enum SuperLayerPosition{
       P1_P = 0,
       P2_P = 1,
       P3_P = 2,
       P4_P = 3,
       P1_N = 4,
       P2_N = 5,
       P3_N = 6,
       P4_N = 7,
       P_N_DISTANCE = 4

};

 
class DataManager {
    public:
        DataManager(int uplinksNumber,int* uplinkIdArray,std::string pathToReadInFile);
        DataManager(const std::vector<int>& uplinksToBeAnalyzed);
        DataManager(std::string pathToSetupFile);
        DataManager(std::string pathToSetupFile,std::string pathToReadInFile);
        ~DataManager();
	void fill_raw_pedestals(MergedEvent* event);
	void pedestals_write_Mean_RMS();
	void find_working_channels();
        void draw();
	void getEnergy(MergedEvent* event);

        void ProjectionYWithMean(int uplinkId,bool forceInLog = false);
        void ADCPerPhoton(int uplinkId,Float_t gain,bool plot=false,bool forMIP = false);
        void MPVinADC(int uplinkId,Int_t rebin,bool plot=false);
        void MPVinPE(int uplinkId); // It assumes that MPV and ADC values in ADC has already counted.

        void PlotAnalysisResult_1uplink(int uplinkId, std::string pedestalFilePath ="", std::string signalFilePath="");

        void PlotAnalysisResult(std::string pedestalFilePath = "", std::string signalFilePath ="");

        void PlotProjectionY(bool forceInlog);

        void AnalyzeLedData(Float_t gain);

        void AnalyzeCosmicData(Float_t gain,Int_t rebin_mpv);

        void PlotAnalysisResult_LED();

        void PlotAnalysisResult_LED_InStrip();

        void PlotAnalysisResult_InStrip(std::string pedestalFilePath = "", std::string signalFilePath ="");

        void Fill2LayerCorrelation(MergedEvent* event);

        void Plot2LayerCorr();

        void SaveAllHistos(); // write all the generated histos and graphs in to a root file

        void FillPsPedestal(MergedEvent* event);

        TH2* GetPS_pedestals(int uplinkId);

        //void FillAllByExistFiles(std::string analyzeFilePath = "../data/lect_uplinks_result.root");

        void SetPathToDACFile(std::string pathToDACFile){m_pathToDACFile = pathToDACFile;}
        std::string& GetPathToDACFile() { return m_pathToDACFile;}

        void SetPathToFormerCfgFile(std::string pathToFormerCfgFile){m_pathToFormerCfgFile = pathToFormerCfgFile;}
        std::string& GetPathToFormerCfgFile() { return m_pathToFormerCfgFile ;}

        void SetDacFromCfgFile(){m_dacFromCfgFile = true;}
        void ClearDacFromCfgFile(){m_dacFromCfgFile = false;}
        bool IsDacFromCfgFile(){ return m_dacFromCfgFile;}

        void SetDacFromAdjustFile(){m_dacFromAdjustFile = true;}
        void ClearDacFromAdjustFile(){m_dacFromAdjustFile = false;}
        bool IsDacFromAdjustFile(){ return m_dacFromAdjustFile;}

        void SetFroceToChangeCfgFile(){m_forceToChangeCfgFile = true;}
        void ClearFroceToChangeCfgFile(){m_forceToChangeCfgFile = false;}
        bool IsForceToChangeCfgFile(){return m_forceToChangeCfgFile;}

        void SetAimGain(int setGain){m_aimGain = setGain;}
        int GetAimGain(){return m_aimGain;}

        void PlotLedResult_InSiPM128();
        
        
        void FindPeaks();

		void SaveADCHistos(TString file_name);
    




/**********/
        int Get_NumberChannels();
        void Get_RawSignalADCdata_1event(MergedEvent* event, int eventADC[]);
        void signalADC_PedPeakSub_PedSub(int eventADCin[], int eventADCout[], int Ped_Peak_Thrsd_inADC);
        void ReorderChannels_Sipm128(int eventADC[]);
        //void CommonModeSuppression(int eventADCin[], int eventADCout[], double CMS_thrsd_in_pe, int EventNr);
        void Cluster_Search(int eventADCin[], int SeedCutADC, int EventNr);

        void drawHisto_1chan(int ChannelNr);
        void draw_signalADC_1event(int eventADC[], int EventNr[]);
        void drawHisto_CMS(int ChannelNr);
        void drawHisto_Cluster();

        void ConfigHistoTH1(TH1* Histo, TString Title, TString axeXname, TString axeYname, double axeXmin, double axeXmax, double axeYmin, double axeYmax, double LineWidth);
        void ConfigHistoTH2(TH2* Histo, TString Title, TString axeXname, TString axeYname, Double_t axeXmin, Double_t axeXmax, double axeYmin, double axeYmax);






        const static unsigned short maxAdcValue = 4096;
        //const static unsigned short maxNumberPhotons = 50000;
        const static unsigned short HEAD_OF_EFFECT_CHANNELS = 0;   // count as channel0,1,2....
        const static unsigned short NUMBER_OF_EFFECT_CHANNELS = 64;

        const static unsigned short ADC_PER_PHOTON_UPPER_THRESHOLD = 100;
        const static unsigned short MEAN_IN_PHOTON_UNDER_THRESHOLD =0.5;
        //const static unsigned short GAIN_AIM =11;
        const static unsigned short DAC_NORMINAL = 128;

        const static unsigned short STRIP_BASE_TRIGGERLAYER_PART1 = 1;
        const static unsigned short STRIP_BASE_TRIGGERLAYER_PART2 = 1+NUMBER_OF_EFFECT_CHANNELS;
        const static unsigned short NUMBER_OF_STRIPS_TRIGGERLAYER = 2*NUMBER_OF_EFFECT_CHANNELS;
        const static unsigned short STRIP_BASE_SIGNALLAYER = 1+NUMBER_OF_STRIPS_TRIGGERLAYER;
        const static unsigned short COSMIC_THRESHOLD = 30 ;  // threshold to be treated as an cosmic event
        const static unsigned short VATA64_READOUT_CHANNELS = 64;

        const static unsigned short CHANNELS_PER_GROUP=16;
        const static unsigned short STRIPS_PER_GROUP=16;
        const static unsigned short STRIPS_PER_SUPERLAYER = 192;
        const static unsigned short STRIPS_PER_MODULE = 48;
        const static unsigned short BOARDS_PER_SUPERLAYER = 8;


        //This is added for tracker fiber module    by Snow@EPFL,20120619
        const static unsigned short CHANNELS_PER_CONNECTOR = 32;//for vata64board, each board has two connectors.
        const static unsigned short CHANNELS_PER_HALF_CONNECTOR = 16;
        /*
         * Generate the channelIndex of Sipm128
         * @param uplinkID: the uplinkID for the vata64 FE cards,
         *        channelIndexOnVata64Chip: the channelIndex of the vata64 cards, !!!This is counted from 0!
         * @return corresponding channelIndex on Sipm128:  !!This is counted from 1!!!!!
         */
        uint16_t GetChannelIndexOfSipm128(uint16_t uplinkID, uint16_t channelIndexOnVata64Chip);

        uint16_t GetTrackerModuleVataBoardAUplinkID();
        uint16_t GetTrackerModuleVataBoardBUplinkID();

        bool GenerateTrackModulePedSubHisto(std::string mapFile);

 private:
        unsigned int uplinkIndex(int uplinkId);


	//PEBS09_TestbeamSetup* m_setup;
        std::vector<int> m_uplinkId;
        std::vector<unsigned short *> m_state;
        std::vector<float *> m_pedestal_Mean;
        std::vector<float *> m_pedestal_RMS;
	std::vector<TH2*> m_raw_pedestals;
        std::vector<TH2*> m_adcHistogram;
        std::vector<TH2*> m_PS_adcHistogram;
        std::vector<TH2*> m_uplink_correlation;
	std::vector<TGraphErrors*> m_adcPerPhoton;
	std::vector<TGraphErrors*> m_mpvInAdc;
	std::vector<TGraphErrors*> m_lSigma;
	std::vector<TGraphErrors*> m_gSigma;
	std::vector<TGraphErrors*> m_resultCorr;
	std::vector<TH1F*> m_area;
        std::vector<TGraphErrors*> m_meanInPhoton;
        std::vector<TH2*> m_PS_pedestals; // pedestal which substracted by the meanvalue of m_raw_jpedestals.

        TH3I* m_2layerCorrHisto1;
        TH3I* m_2layerCorrHisto2;
        TH2*  m_signalLayerSide1_strip;
        TH2*  m_signalLayerSide2_strip;
        TH2*  m_signalLayerSide1_chan;
        TH2*  m_signalLayerSide2_chan;

        TH2*  m_s1Histo_strip;
        TH2*  m_s2Histo_strip;

        TH2*  m_s1Histo_chan;
        TH2*  m_s2Histo_chan;

/**********/
        int m_signalADC_cutThrsd;
        int m_NumberChannels;
        std::vector<TH2*> m_signalADC_withThrsd;
        std::vector<TH2*> m_signalADC_withThrsd_pedSub;
        std::vector<TH2*> m_signalADC_CMS;
        std::vector<TH1*> m_CMS_val;
        std::vector<TH1*> m_NbCluster;
        std::vector<TH1*> m_ClusterSize;
        std::vector<TH1*> m_ClusterADC;
        std::vector<TH1D*> m_ClusterChannel;




	TFile* file_data;       

        //
        std::string m_pathToDACFile;  // to save the path to DAC source file, either from chip config file or from adjustDAC file.
        bool m_dacFromCfgFile;
        bool m_dacFromAdjustFile;
        std::string m_pathToFormerCfgFile; // only the path to the folder, the file name is generated automatically .
        bool m_forceToChangeCfgFile;

        int m_aimGain;

        SuperLayerMap m_superLayerMap[2];
        bool m_readInSetupFile;
        std::vector<int> m_corrUplinkPairs;
        std::map<int,TH2*> m_strip_correlation; //<stripIndex,correlationHisto>

        //This is added for tracker fiber module    by Snow@EPFL,20120619
        uint16_t _trackerModuleVataBoardA;
        uint16_t _trackerModuleVataBoardB;
        TH2* _trackerModulePedSubHisto;

};

#endif
