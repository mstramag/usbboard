#include "DataManager.h"
#include <Settings.h>

#include <assert.h>
#include <iostream>

#include <MergedEvent.h>
#include <UsbBoardConfiguration.h>


#include <TH1.h>
#include <TH2.h>
#include <TH2F.h>
#include <TF1.h>
#include <TH3.h>
#include <TProfile.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TMath.h>
#include <TObjArray.h>
#include <TLegend.h>
#include <TGraph.h>

#include <TRandom.h>
#include <TSpectrum.h>
#include <TVirtualFitter.h>
#include <TROOT.h>
#include <TStyle.h>


//#include "PEBS09_TestbeamSetup.h"

Int_t npeaks;
TH1 *background = NULL;

DataManager::DataManager(int uplinksNumber,int* uplinkIdArray,std::string pathToReadInFile){
    m_pathToDACFile = "";
    m_pathToFormerCfgFile = "";
    m_dacFromCfgFile = false;
    m_dacFromAdjustFile = false;
    m_forceToChangeCfgFile = false;
    m_aimGain = 50;
    m_readInSetupFile = false;

    _trackerModuleVataBoardA = 0;
    _trackerModuleVataBoardB = 0;

    for(int i = 0; i<uplinksNumber;i++) m_uplinkId.push_back(uplinkIdArray[i]);
    TFile *file = new TFile(pathToReadInFile.c_str(),"READ");
    printf("readInFile from %s \n",pathToReadInFile.c_str());

    char tmp[256];

    TH2* histo;

    //Fill the raw pedestals
    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        sprintf(tmp,"raw_pedestals_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
        histo = (TH2*)file->Get(tmp);
        m_raw_pedestals.push_back(histo);
    }

    //Fill the PS_pedestal
    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        sprintf(tmp,"PS_pedestals_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
        histo = (TH2*)file->Get(tmp);
        m_PS_pedestals.push_back(histo);
    }

    //Fill the adcHistogram
    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        sprintf(tmp,"adcHistogram_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
        histo = (TH2*)file->Get(tmp);
        m_adcHistogram.push_back(histo);
    }

    //fill the PS_adcHistogram
    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        sprintf(tmp,"PS_adcHistogram_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
        histo = (TH2*)file->Get(tmp);
        m_PS_adcHistogram.push_back(histo);
    }

    //fill the m_uplink_correlation
    if((m_uplinkId.size()%2==0) && (m_uplinkId.size()!=0) ){
        //printf("fill  m_uplink_correlation :\n");
        for(int corrIndex=0; corrIndex< (m_uplinkId.size()/2); corrIndex++){
            int Xuplink = m_uplinkId.at(2*corrIndex);
            int Yuplink = m_uplinkId.at(2*corrIndex+1);

            //printf("Xuplink = %d, Yuplink = %d\n",Xuplink,Yuplink);

            for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;chan++){
                 sprintf(tmp, "uplink_%d_vs_%d_____uplink%d_channel%03d_with_uplink%d_channel%03d",Xuplink,Yuplink,Xuplink,chan,Yuplink,2*HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1-chan);
                 histo = (TH2*)file->Get(tmp);
                 m_uplink_correlation.push_back(histo);
            }
        }
    }


    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        TGraphErrors* adcPerPhoton = new TGraphErrors(1);
        m_adcPerPhoton.push_back(adcPerPhoton);

        TGraphErrors* mpvInAdc = new TGraphErrors(1);
        m_mpvInAdc.push_back(mpvInAdc);

        TGraphErrors* resultCorr = new TGraphErrors(1);
        m_resultCorr.push_back(resultCorr);

        sprintf(tmp, "area_uplink%d", m_uplinkId.at(uplinkIndex));
        TH1F* area = new TH1F(tmp,"area_for_mean_Of_MIP_Distribution;Channel;Area",NUMBER_OF_EFFECT_CHANNELS,HEAD_OF_EFFECT_CHANNELS,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS);
        m_area.push_back(area);

        TGraphErrors* meanPhoton = new TGraphErrors(1);
        m_meanInPhoton.push_back(meanPhoton);

    }

}

DataManager::DataManager(const std::vector<int>& uplinksToBeAnalyzed)
{
    char tmp[128];
    Settings* settings = Settings::instance();
    // initialize setup
    //m_setup = new PEBS09_TestbeamSetup();

    m_pathToDACFile = "";
    m_pathToFormerCfgFile = "";
    m_dacFromCfgFile = false;
    m_dacFromAdjustFile = false;
    m_forceToChangeCfgFile = false;
    m_aimGain = 50;
    m_readInSetupFile = false;


    for (unsigned int k = 0; k < uplinksToBeAnalyzed.size(); ++k) {
        int uplinkId = uplinksToBeAnalyzed[k];
        int usbBoardId = settings->usbBoardIdFromUplinkId(uplinkId);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();

        m_uplinkId.push_back(uplinkId);

        sprintf(tmp, "adcHistogram_uplink_%03d", uplinkId);
        TH2* adcHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        adcHistogram->GetXaxis()->SetTitle("channel");
        adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_adcHistogram.push_back(adcHistogram);

        sprintf(tmp, "PS_adcHistogram_uplink_%03d", uplinkId);
        TH2* PS_adcHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue+500, -500, maxAdcValue);
        PS_adcHistogram->GetXaxis()->SetTitle("channel");
        PS_adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_PS_adcHistogram.push_back(PS_adcHistogram);

        sprintf(tmp, "PS_pedestals_uplink_%03d", uplinkId);
        TH2* PS_pedestals = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue+500, -500, maxAdcValue);
        PS_pedestals->GetXaxis()->SetTitle("channel");
        PS_pedestals->GetYaxis()->SetTitle("adcValue");
        m_PS_pedestals.push_back(PS_pedestals);

        sprintf(tmp, "raw_pedestals_uplink_%03d", uplinkId);
        TH2* raw_pedestals = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        raw_pedestals->GetXaxis()->SetTitle("channel");
        raw_pedestals->GetYaxis()->SetTitle("adcValue");
        m_raw_pedestals.push_back(raw_pedestals);

        TGraphErrors* adcPerPhoton = new TGraphErrors(1);
        m_adcPerPhoton.push_back(adcPerPhoton);

        TGraphErrors* mpvInAdc = new TGraphErrors(1);
        m_mpvInAdc.push_back(mpvInAdc);

        TGraphErrors* resultCorr = new TGraphErrors(1);
        m_resultCorr.push_back(resultCorr);

        TGraphErrors* lSigma = new TGraphErrors(1);
        m_lSigma.push_back(lSigma);

        TGraphErrors* gSigma = new TGraphErrors(1);
        m_gSigma.push_back(gSigma);

        sprintf(tmp, "area_uplink%d", uplinkId);
        TH1F* area = new TH1F(tmp,"area_for_mean_Of_MIP_Distribution;Channel;Area",NUMBER_OF_EFFECT_CHANNELS,HEAD_OF_EFFECT_CHANNELS,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS);
	m_area.push_back(area);

        TGraphErrors* meanPhoton = new TGraphErrors(1);
        m_meanInPhoton.push_back(meanPhoton);
     }

     //if the number of uplinks to be analyzed is even, make correaltion between 1&2, 3&4, ...
     if((uplinksToBeAnalyzed.size()%2==0) && (uplinksToBeAnalyzed.size()!=0) ){
         for(int i=0; i< (uplinksToBeAnalyzed.size()/2); i++){
             int Xuplink = uplinksToBeAnalyzed.at(2*i);
             int Yuplink = uplinksToBeAnalyzed.at(2*i+1);

             for (Int_t i=HEAD_OF_EFFECT_CHANNELS;i<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;i++){
                  sprintf(tmp, "uplink_%d_vs_%d_____uplink%d_channel%03d_with_uplink%d_channel%03d",Xuplink,Yuplink,Xuplink,i,Yuplink,2*HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1-i);
                  TH2* uplink_correlation = new TH2I(tmp,tmp, (maxAdcValue+500)/10, -500., maxAdcValue, (maxAdcValue+500)/10, -500., maxAdcValue);
                  sprintf(tmp,"uplink_%d",Xuplink);
                  uplink_correlation->GetXaxis()->SetTitle(tmp);
                  sprintf(tmp,"uplink_%d",Yuplink);
                  uplink_correlation->GetYaxis()->SetTitle(tmp);
                  m_uplink_correlation.push_back(uplink_correlation);
                  //printf("add m_uplink_correlation, now m_uplink_correlation.size() = %d",m_uplink_correlation.size());
             }
         }
     }

     if(uplinksToBeAnalyzed.size() == 4) {
         sprintf(tmp, "LayerCorr1_triggerU%d_U%d_signalU%d",m_uplinkId.at(0),m_uplinkId.at(1),m_uplinkId.at(2));
         m_2layerCorrHisto1 = new TH3I(tmp, tmp, NUMBER_OF_STRIPS_TRIGGERLAYER, STRIP_BASE_TRIGGERLAYER_PART1, STRIP_BASE_TRIGGERLAYER_PART1+NUMBER_OF_STRIPS_TRIGGERLAYER,NUMBER_OF_EFFECT_CHANNELS,STRIP_BASE_SIGNALLAYER,STRIP_BASE_SIGNALLAYER+NUMBER_OF_EFFECT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
         m_2layerCorrHisto1->GetXaxis()->SetTitle("triggerLayer_strip#");
         m_2layerCorrHisto1->GetYaxis()->SetTitle("signalLayer_strip#");
         m_2layerCorrHisto1->GetZaxis()->SetTitle("adcValue");

         sprintf(tmp, "LayerCorr2_triggerU%d_U%d_signalU%d",m_uplinkId.at(0),m_uplinkId.at(1),m_uplinkId.at(3));
         m_2layerCorrHisto2 = new TH3I(tmp, tmp, NUMBER_OF_STRIPS_TRIGGERLAYER, STRIP_BASE_TRIGGERLAYER_PART1, STRIP_BASE_TRIGGERLAYER_PART1+NUMBER_OF_STRIPS_TRIGGERLAYER,NUMBER_OF_EFFECT_CHANNELS,STRIP_BASE_SIGNALLAYER,STRIP_BASE_SIGNALLAYER+NUMBER_OF_EFFECT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
         m_2layerCorrHisto2->GetXaxis()->SetTitle("triggerLayer_strip#");
         m_2layerCorrHisto2->GetYaxis()->SetTitle("signalLayer_strip#");
         m_2layerCorrHisto2->GetZaxis()->SetTitle("adcValue");


         sprintf(tmp, "signalLayer_side1_strip_U%d", m_uplinkId.at(2));
         m_signalLayerSide1_strip = new TH2I(tmp, tmp, NUMBER_OF_EFFECT_CHANNELS,STRIP_BASE_SIGNALLAYER,STRIP_BASE_SIGNALLAYER+NUMBER_OF_EFFECT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
         m_signalLayerSide1_strip->GetXaxis()->SetTitle("strip");
         m_signalLayerSide1_strip->GetYaxis()->SetTitle("adcValue");

         sprintf(tmp, "signalLayer_side2_strip_U%d", m_uplinkId.at(3));
         m_signalLayerSide2_strip = new TH2I(tmp, tmp, NUMBER_OF_EFFECT_CHANNELS,STRIP_BASE_SIGNALLAYER,STRIP_BASE_SIGNALLAYER+NUMBER_OF_EFFECT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
         m_signalLayerSide2_strip->GetXaxis()->SetTitle("strip");
         m_signalLayerSide2_strip->GetYaxis()->SetTitle("adcValue");


         sprintf(tmp, "signalLayer_side1_chan_U%d",m_uplinkId.at(2));
         m_signalLayerSide1_chan= new TH2I(tmp, tmp, VATA64_READOUT_CHANNELS, .0, VATA64_READOUT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
         m_signalLayerSide1_chan->GetXaxis()->SetTitle("channel");
         m_signalLayerSide1_chan->GetYaxis()->SetTitle("adcValue");

         sprintf(tmp, "signalLayer_side2_U%d",m_uplinkId.at(3));
         m_signalLayerSide2_chan= new TH2I(tmp, tmp, VATA64_READOUT_CHANNELS, .0, VATA64_READOUT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
         m_signalLayerSide2_chan->GetXaxis()->SetTitle("channel");
         m_signalLayerSide2_chan->GetYaxis()->SetTitle("adcValue");


         sprintf(tmp, "s1Histo_chan_U%d",m_uplinkId.at(2));
         m_s1Histo_chan= new TH2I(tmp, tmp, VATA64_READOUT_CHANNELS, .0, VATA64_READOUT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
         m_s1Histo_chan->GetXaxis()->SetTitle("channel");
         m_s1Histo_chan->GetYaxis()->SetTitle("adcValue");

         sprintf(tmp, "s2Histo_chan_U%d",m_uplinkId.at(3));
         m_s2Histo_chan= new TH2I(tmp, tmp, VATA64_READOUT_CHANNELS, .0, VATA64_READOUT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
         m_s2Histo_chan->GetXaxis()->SetTitle("channel");
         m_s2Histo_chan->GetYaxis()->SetTitle("adcValue");

         sprintf(tmp, "s1Histo_strip_U%d", m_uplinkId.at(2));
         m_s1Histo_strip = new TH2I(tmp, tmp, NUMBER_OF_EFFECT_CHANNELS,STRIP_BASE_SIGNALLAYER,STRIP_BASE_SIGNALLAYER+NUMBER_OF_EFFECT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
         m_s1Histo_strip->GetXaxis()->SetTitle("strip");
         m_s1Histo_strip->GetYaxis()->SetTitle("adcValue");

         sprintf(tmp, "s2Histo_strip_U%d", m_uplinkId.at(3));
         m_s2Histo_strip = new TH2I(tmp, tmp, NUMBER_OF_EFFECT_CHANNELS,STRIP_BASE_SIGNALLAYER,STRIP_BASE_SIGNALLAYER+NUMBER_OF_EFFECT_CHANNELS, maxAdcValue+500, -500, maxAdcValue);
         m_s2Histo_strip->GetXaxis()->SetTitle("strip");
         m_s2Histo_strip->GetYaxis()->SetTitle("adcValue");
     }

    //m_resultLog = fopen("lect_uplinks_result_log.txt","w");

}

DataManager::~DataManager(){
    //fclose(m_resultLog);
}

unsigned int DataManager::uplinkIndex(int uplinkId)
{
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
    for (int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
        if (*uplinkIdIt == uplinkId)
            return i;
    }
    assert(false);
    return 0;
}


void DataManager::fill_raw_pedestals(MergedEvent* event)
{ 
  Settings* settings = Settings::instance();
  std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
  std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();

  static int nn = 1;

  for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
    int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
    unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
    unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
    unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;
    const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);

    for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip) {
      for (unsigned short channel = 0; channel < (channelsPerReadoutChip); ++channel) {
	unsigned short sample = chip * channelsPerReadoutChip + channel;
   //     std::cout << i << " " << chip << " " << channel << " " << sample << " " << channelsPerReadoutChip << std::endl;
   //     std::cout << *(m_raw_pedestals[i] + i) << std::endl;
	m_raw_pedestals[i]->Fill(sample , uplinkData.adcValue(sample));
   //     std::cout << "  ** " << uplinkData.adcValue(sample) << std::endl;

//        std::cout << uplinkData.adcValue(sample) << " " << m_raw_pedestals[i] << " " << *(m_raw_pedestals[i]+nn+sample) << " ";
     //   std::cout << std::endl;




      }
    }
  }

      nn++;
}


void DataManager::find_working_channels()
{ 
  Settings* settings = Settings::instance();
  std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
  std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();

  for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
    int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
    unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
    unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
    unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;
    unsigned short * ch_state;
    ch_state = new unsigned short[numberOfReadoutChips*channelsPerReadoutChip];

    for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip) {
      for (unsigned short channel = 0; channel < (channelsPerReadoutChip); ++channel) {
	unsigned short sample = chip * channelsPerReadoutChip + channel;
        float mean,rms;
	mean = m_raw_pedestals[i]->ProjectionY("proj1",sample+1,sample+1,"")->GetMean();
        rms = m_raw_pedestals[i]->ProjectionY("proj2",sample+1,sample+1,"")->GetRMS();
	unsigned short state=1;
	if (rms<1) state = 0;
	ch_state[sample]=state;
	//std::cout<< "i = " << i << "     sample = "<< sample << "   rms=" << rms << "   mean=" << mean << "    state = " << ch_state[sample] << std::endl;
      }
    }
    m_state.push_back(ch_state);
    //delete ch_state; //state must not be deleted, otherwise, the content of m_state is also deleted.
  }
}


void DataManager::pedestals_write_Mean_RMS()
{
  Settings* settings = Settings::instance();
  std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
  std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
  for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
    int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
    unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
    unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
    unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;

    float * pedestal_Mean;
    pedestal_Mean = new float[sampleSize];
    float * pedestal_RMS;
    pedestal_RMS = new float[sampleSize];

    for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip) {
      for (unsigned short channel = 0; channel < (channelsPerReadoutChip); ++channel) {
	
	unsigned short sample = chip * channelsPerReadoutChip + channel;
	pedestal_Mean[sample] = m_raw_pedestals[i]->ProjectionY("p1",sample+1,sample+1,"")->GetMean();
	pedestal_RMS[sample] = m_raw_pedestals[i]->ProjectionY("p2",sample+1,sample+1,"")->GetRMS();
      }
    }

    m_pedestal_Mean.push_back(pedestal_Mean);
    m_pedestal_RMS.push_back(pedestal_RMS);
  }
}


void DataManager::getEnergy(MergedEvent* event)
{
  Settings* settings = Settings::instance();
  std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
  std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();

  if(m_readInSetupFile){
       int SuperLayerIndex = 0;
       int positionIndex = 0;
       int stripIndex = 0;
       int XuplinkID = 0;
       int YuplinkID = 0;
       int XuplinkIndex  = 0;
       int YuplinkIndex = 0;
       std::map<int,TH2*>::iterator stripCorrHisto;
       SuperLayerMap::iterator superLayer_iter;
       int corrChan = 0;

       for(int corrIndex = 0; corrIndex< m_corrUplinkPairs.size()/2; corrIndex++){
             XuplinkID = m_corrUplinkPairs.at(2*corrIndex);
             XuplinkIndex = uplinkIndex(XuplinkID);
             YuplinkID = m_corrUplinkPairs.at(2*corrIndex+1);
             YuplinkIndex = uplinkIndex(YuplinkID);
             const UplinkData& XuplinkData = event->uplinkData(XuplinkID);
             const UplinkData& YuplinkData = event->uplinkData(YuplinkID);
             //std::cout<<"correalation for XuplinkID = "<<XuplinkID<<" & YuplinkID = "<<YuplinkID <<"m_corrUplinkPairs.size() = "<<m_corrUplinkPairs.size()<<std::endl;
             //getchar();
             //generate stripIndex
             for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                  for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                      if(superLayer_iter->second==XuplinkID) {
                            SuperLayerIndex = LayerIndex;
                            positionIndex = superLayer_iter->first;
                            break;
                       }
                  }
             }

             for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS;chan++) {
                 if((chan%CHANNELS_PER_GROUP)>(STRIPS_PER_GROUP-1)) continue; // only 0~11,16~27,32~43,48~59 are SiPM output
                 corrChan = (chan/CHANNELS_PER_GROUP)*CHANNELS_PER_GROUP+STRIPS_PER_GROUP-1-chan%CHANNELS_PER_GROUP;
                 stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+(chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+chan%CHANNELS_PER_GROUP;
                 //std::cout<<"for chan = "<<chan <<" on uplink"<<XuplinkID<<" & corrChan = "<<corrChan <<" on uplink" <<YuplinkID<<" ,stripIndex = "<<stripIndex<<std::endl;
                 //getchar();
                 stripCorrHisto = m_strip_correlation.find(stripIndex);
                 if(stripCorrHisto!=m_strip_correlation.end()){
                      stripCorrHisto->second->Fill(XuplinkData.adcValue(chan) - m_pedestal_Mean.at(XuplinkIndex)[chan] , YuplinkData.adcValue(corrChan) - m_pedestal_Mean.at(YuplinkIndex)[corrChan]);
                 }else{
                     std::cerr<< "Cannot find corrHisto for uplink_P= "<<XuplinkID <<" & uplink_N= "<<YuplinkID<<std::endl;
                     assert(false);
                 }
             }
             //std::cout<<"finish 1 correlation!\n";

       }
       //std::cout<<"finish All Correlation\n";

  }else{
       int corrNum = 2*HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1;
       //printf("corrNum = %d\n",corrNum);
       //printf("m_uplink_correlation.size() = %d\n",m_uplink_correlation.size());
  
       int corrSum = m_uplink_correlation.size()/NUMBER_OF_EFFECT_CHANNELS;
       //printf("corrSum = %d\n",corrSum);

       for(int corrIndex=0; corrIndex< corrSum;corrIndex++){
             const UplinkData& XuplinkData = event->uplinkData(m_uplinkId.at(2*corrIndex));
             const UplinkData& YuplinkData = event->uplinkData(m_uplinkId.at(2*corrIndex+1));
             // printf("before for loop \n");
             for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS;chan++) {
                    m_uplink_correlation[corrIndex*NUMBER_OF_EFFECT_CHANNELS+chan-HEAD_OF_EFFECT_CHANNELS]->Fill(XuplinkData.adcValue(chan) - m_pedestal_Mean.at(2*corrIndex)[chan] , YuplinkData.adcValue(corrNum-chan) - m_pedestal_Mean.at(2*corrIndex+1)[corrNum-chan]);
             }
       }
  }

  //printf("finish correalation\n");
 // getchar();

  for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
    int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
    unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
    unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
    unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;
    const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);
    //if (i==0) const UplinkData& uplinkData41 = event->uplinkData(*uplinkIdIt);
    //if (i==1) const UplinkData& uplinkData42 = event->uplinkData(*uplinkIdIt);

    for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip) {
       for (unsigned short channel = 0; channel < channelsPerReadoutChip; ++channel) {
          unsigned short sample = chip * channelsPerReadoutChip + channel;
          m_adcHistogram[i]->Fill(sample , uplinkData.adcValue(sample));
          m_PS_adcHistogram[i]->Fill(sample , uplinkData.adcValue(sample) - m_pedestal_Mean.at(i)[sample]);
       }
    }
  }

}


void DataManager::draw()
{
  char tmp[256];
  TCanvas* canvas = NULL;

  double mean = 0;
  double rms = 0;

  for(unsigned int i=0;i<m_uplinkId.size();++i){
      sprintf(tmp,"uplink%d_histogram",m_uplinkId.at(i));
      canvas = new TCanvas(tmp,tmp,0,0,1200,900);
      canvas->Divide(1,3);
      canvas->cd(1);
      m_raw_pedestals[i]->Draw("COLZ");
      mean = m_raw_pedestals[i]->GetMean(2);
      rms = m_raw_pedestals[i]->GetRMS(2);
      m_raw_pedestals[i]->GetYaxis()->SetRangeUser(mean - 10*rms, mean + 10*rms);

      canvas->GetPad(1)->SetLogz();

      canvas->cd(2);
      m_adcHistogram[i]->Draw("COLZ");
      mean = m_adcHistogram[i]->GetMean(2);
      rms = m_adcHistogram[i]->GetRMS(2);
      m_adcHistogram[i]->GetYaxis()->SetRangeUser(300, mean + 10*rms);
      canvas->GetPad(2)->SetLogz();

      canvas->cd(3);
      m_PS_adcHistogram[i]->Draw("COLZ");
      mean = m_PS_adcHistogram[i]->GetMean(2);
      rms = m_PS_adcHistogram[i]->GetRMS(2);
     // m_PS_adcHistogram[i]->GetYaxis()->SetRangeUser(mean - 10*rms, mean + 10*rms);
     // m_PS_adcHistogram[i]->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
      canvas->GetPad(3)->SetLogz();
  }
/*
  int corrSum = 0;
  if(m_readInSetupFile){
      corrSum = m_corrUplinkPairs.size()/2;
  }else{
      corrSum = m_uplink_correlation.size()/NUMBER_OF_EFFECT_CHANNELS;
  }

  printf("corrSum = %d\n", corrSum);
  int xPadNum = 4;
  int yPadNum = 3;
  int padPerCanvas = xPadNum*yPadNum;


  int canvasNum = 0;
  if(NUMBER_OF_EFFECT_CHANNELS%padPerCanvas == 0)    canvasNum = NUMBER_OF_EFFECT_CHANNELS/padPerCanvas;
  else   canvasNum = NUMBER_OF_EFFECT_CHANNELS/padPerCanvas+1;
  int canvasIndex = 0;



  for(int corrIndex=0; corrIndex< corrSum;corrIndex++){
     //printf("m_uplink_correlation.size()= %d",m_uplink_correlation.size());
     //getchar();
      canvasIndex = 0;
      for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS;chan++){
          if((chan-HEAD_OF_EFFECT_CHANNELS)%padPerCanvas == 0){
               if(canvasNum==1) sprintf(tmp,"correlation uplink%d_uplink%d",m_uplinkId.at(2*corrIndex),m_uplinkId.at(2*corrIndex+1));
               else   sprintf(tmp,"correlation uplink%d_uplink%d  (%d)",m_uplinkId.at(2*corrIndex),m_uplinkId.at(2*corrIndex+1),canvasIndex+1);
               canvas= new TCanvas(tmp,tmp, 0, 0, 1200, 900);
               canvas->Divide(xPadNum,yPadNum);
               canvasIndex++;
          }

          canvas->cd((chan-HEAD_OF_EFFECT_CHANNELS)%padPerCanvas+1);
          printf("canvas->cd(%d)\n",(chan-HEAD_OF_EFFECT_CHANNELS)%padPerCanvas+1);

         // m_uplink_correlation.at(0)->Draw("COLZ");
         // printf("draw the first!\n");

          m_uplink_correlation[corrIndex*NUMBER_OF_EFFECT_CHANNELS+chan-HEAD_OF_EFFECT_CHANNELS]->Draw("COLZ");

       //   printf("m_uplink_correlation[%d]\n",corrIndex*NUMBER_OF_EFFECT_CHANNELS+chan-HEAD_OF_EFFECT_CHANNELS);
        //  getchar();

          double mean = m_uplink_correlation[corrIndex*NUMBER_OF_EFFECT_CHANNELS+chan-HEAD_OF_EFFECT_CHANNELS]->GetMean(1);
          // printf("double mean=%d\n",mean);
          double rms = m_uplink_correlation[corrIndex*NUMBER_OF_EFFECT_CHANNELS+chan-HEAD_OF_EFFECT_CHANNELS]->GetRMS(1);
         // printf("double rms=%d\n",rms);
          //m_uplink_correlation[corrIndex*NUMBER_OF_EFFECT_CHANNELS+chan-HEAD_OF_EFFECT_CHANNELS]->GetXaxis()->SetRangeUser(mean - 10*rms, mean + 10*rms);
          m_uplink_correlation[corrIndex*NUMBER_OF_EFFECT_CHANNELS+chan-HEAD_OF_EFFECT_CHANNELS]->GetXaxis()->SetRangeUser(-50, mean + 100*rms);



          mean = m_uplink_correlation[corrIndex*NUMBER_OF_EFFECT_CHANNELS+chan-HEAD_OF_EFFECT_CHANNELS]->GetMean(2);
          //printf("mean=%d\n",mean);
          rms = m_uplink_correlation[corrIndex*NUMBER_OF_EFFECT_CHANNELS+chan-HEAD_OF_EFFECT_CHANNELS]->GetRMS(2);
          //printf("rms=%d\n",rms);
          //m_uplink_correlation[corrIndex*NUMBER_OF_EFFECT_CHANNELS+chan-HEAD_OF_EFFECT_CHANNELS]->GetYaxis()->SetRangeUser(mean - 10*rms, mean + 10*rms);
          m_uplink_correlation[corrIndex*NUMBER_OF_EFFECT_CHANNELS+chan-HEAD_OF_EFFECT_CHANNELS]->GetYaxis()->SetRangeUser(-50, mean + 100*rms);


          canvas->GetPad((chan-HEAD_OF_EFFECT_CHANNELS)%padPerCanvas+1)->SetLogz();

       }
  }
  printf("finished corr draw\n");
*/
   int corrSum = 0;
   if(m_readInSetupFile){
        corrSum = m_strip_correlation.size();
        printf("corrSum = %d\n", corrSum);
        int xPadNum = 4;
        int yPadNum = 3;
        int padPerCanvas = xPadNum*yPadNum;
        int canvasNum = 0;
        if(corrSum%padPerCanvas == 0)    canvasNum = corrSum/padPerCanvas;
        else   canvasNum = corrSum/padPerCanvas+1;
        int canvasIndex = 0;

        std::map<int,TH2*>::iterator stripCorrelation_iter;
        TH2* currHisto = NULL;
        int picIndex = 0;
        for(stripCorrelation_iter = m_strip_correlation.begin();stripCorrelation_iter!=m_strip_correlation.end();picIndex++,stripCorrelation_iter++){
              if(picIndex%padPerCanvas == 0){
                  if(canvasNum==1) sprintf(tmp,"strip correlation");
                  else sprintf(tmp,"strip correlation (%d)",picIndex/padPerCanvas);
                  canvas = new TCanvas(tmp,tmp,0,0,1200,900);
                  canvas->Divide(xPadNum,yPadNum);
                  canvasIndex++;
              }

              canvas->cd(picIndex%padPerCanvas+1);
              currHisto = stripCorrelation_iter->second;
              currHisto->Draw("COLZ");

              double mean = currHisto->GetMean(1);
              double rms = currHisto->GetRMS(1);
              //currHisto->GetXaxis()->SetRangeUser(-50, mean + 100*rms);
              currHisto->GetXaxis()->SetRangeUser(-50, 3000);

              mean = currHisto->GetMean(2);
              rms = currHisto->GetRMS(2);
              //currHisto->GetYaxis()->SetRangeUser(-50, mean + 100*rms);
              currHisto->GetYaxis()->SetRangeUser(-50, 3000);

              canvas->GetPad(picIndex%padPerCanvas+1)->SetLogz();

//              TProfile *corrProf=currHisto->ProfileY();
//              corrProf->Fit("pol1");
//              corrProf->Draw("SAME");

        }
   }

}

Double_t fpeaks(Double_t *x, Double_t *par){
    Double_t result = par[0] + par[1]*x[0];
    for (Int_t p=0;p<npeaks;p++) {
       Double_t norm  = par[3*p+2];
       Double_t mean  = par[3*p+3];
       Double_t sigma = par[3*p+4];
       result += norm*TMath::Gaus(x[0],mean,sigma);
    }
    return result;
}

void DataManager::ProjectionYWithMean(int uplinkId,bool forceInLog){

    TH2 *PS_adcHistogram_uplink = m_PS_adcHistogram.at(uplinkIndex(uplinkId));
    
    printf("analyze m_PS_adcHistogram.at(%d)_uplinkId%d \n",uplinkIndex(uplinkId),uplinkId);
    //getchar();


    char tmp[256];
    TCanvas *c1 = NULL;
    TH1 *hE = NULL;


    printf("for UplinkId = %d, draw projection and save mean values\n",uplinkId);

    //open a file
    FILE* resultLog = NULL;
    sprintf(tmp,"../data/uplink%d_mean_rms.txt",uplinkId);
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----find mean and RMS for all channels -measurement with LED and high light-----\n\n");
    fprintf(resultLog,"uplink\t channel\t mean\t            rms\t\n");

    printf("onpen file\n");


    int xPadNum = 4;
    int yPadNum = 4;
    int padPerCanvas = xPadNum*yPadNum;
    printf("padPerCanvas = %d\n",padPerCanvas);


    int canvasNum = 0;
    if((NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS)%padPerCanvas == 0)    canvasNum = (NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS)/padPerCanvas;
    else  canvasNum = (NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS)/padPerCanvas+1;
    int canvasIndex = 0;
    printf("canvasNum = %d\n",canvasNum);

    for (Int_t chan=0;chan<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;chan++){
         // measurement with LED and high light
         if(chan%padPerCanvas ==0){      // create a new canvas for padPerCanvas channels
             if(canvasNum == 1){
                  if(forceInLog) sprintf(tmp,"uplink%d_projectionY_in_Log",uplinkId);
                  else sprintf(tmp,"uplink%d_projectionY",uplinkId);

                  printf("only 1 canvas needed \n");
             }else{
                  if(forceInLog) sprintf(tmp,"uplink%d_projectionY_in_Log (%d)",uplinkId,canvasIndex+1);
                  else sprintf(tmp,"uplink%d_projectionY (%d)",uplinkId,canvasIndex+1);

                  printf("canvas %d created\n",canvasIndex+1);
             }

             c1 = new TCanvas(tmp,tmp,10,10,1000,900);
             c1->Divide(xPadNum,yPadNum);
             canvasIndex++;
          }
         // find mean and RMS for all channels

         //for (Int_t chan=0;chan<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;chan++){
         Double_t mean,rms;
         c1->cd(chan%padPerCanvas+1);
         printf("c1->cd(%d)\n",chan%padPerCanvas+1);
         if(forceInLog) gPad->SetLogy();
         sprintf(tmp,"U%d_chan%d",uplinkId,chan);
         hE = PS_adcHistogram_uplink->ProjectionY(tmp,chan+1,chan+1,"");
         mean = hE->GetMean();
         rms = hE->GetRMS();
            
         if(forceInLog) hE->SetAxisRange(-50,200,"X");
         else hE->SetAxisRange(mean-3*rms,mean+3*rms,"X");

         hE->DrawCopy();

          
         if (chan==0) {
               std::cout<<"uplink=" <<uplinkId<<"\t ch="<<chan<<"\t mean="<<mean<<"\t rms="<<rms<<"\n";
               fprintf(resultLog,"%d\t %d\t %f\t %f\t\n",uplinkId,chan,mean,rms);
         }
         if (chan>(HEAD_OF_EFFECT_CHANNELS-1)){
               std::cout<<"uplink="<< uplinkId <<"\t ch="<<chan<<"\t mean="<<mean<<"\t rms="<<rms<<"\n";
               fprintf(resultLog,"%d\t %d\t %f\t %f\t\n",uplinkId,chan,mean,rms);
         }
         c1->Update();
    }


    fclose(resultLog);
     
    hE = NULL;
 }

/*
    for(unsigned int i=0; i<m_uplinkId.size();i++) {
        MPVinADC(m_uplinkId.at(i),rebin_mpv);
        ADCPerPhoton(m_uplinkId.at(i),gain,false,true);


       TH2 *PS_adcHistogram_uplink = m_PS_adcHistogram.at(uplinkIndex(uplinkId));
     char tmp[256];
     TCanvas *c1 = NULL;   //TCanvas for ADCperPhoton
     TH1 *hE = NULL;


             c1 = new TCanvas(tmp,tmp,10,10,1000,900);
             c1->Divide(xPadNum,yPadNum);
       sprintf(tmp,"U%d_chan%d",uplinkId,chan);
       hE = PS_adcHistogram_uplink->ProjectionY(tmp,chan+1,chan+1,"");
*/

 void DataManager::ADCPerPhoton(int uplinkId,Float_t gain,bool plot,bool forMIP){

     TH2 *PS_adcHistogram_uplink = m_PS_adcHistogram.at(uplinkIndex(uplinkId));

     printf("analyze m_PS_adcHistogram.at(%d)_uplinkId%d \n",uplinkIndex(uplinkId),uplinkId);

     char tmp[256];
     TCanvas *c1 = NULL;   //TCanvas for ADCperPhoton
     TH1 *hE = NULL;

     //set TCanvas
     gStyle->SetOptFit(1000);

     printf("for UplinkId = %d, draw projection and save mean values\n",uplinkId);

     FILE* resultLog = NULL;

     TGraphErrors* gains = m_adcPerPhoton.at(uplinkIndex(uplinkId));
     TGraphErrors* meanInPhoton = m_meanInPhoton.at(uplinkIndex(uplinkId));    // only for ligth injection
     Int_t gains_index = 0;

     TGraphErrors* mpvInAdc = m_mpvInAdc.at(uplinkIndex(uplinkId));


     //open a file to log the result
     if(forMIP) sprintf(tmp,"../data/uplink%d_ADCperPhoton_MIP.txt",uplinkId);
     else sprintf(tmp,"../data/uplink%d_ADCperPhoton.txt",uplinkId);
     resultLog = fopen(tmp,"w");
     fprintf(resultLog,"-----find ADC/photon --------\n\n");
     if(forMIP) fprintf(resultLog,"uplink\t channel\t ADC/photon\t\n");
     else   fprintf(resultLog,"uplink channel  ADC/photon      meanPhoton       DAC_adjust  formerDAC_values  AdjustedDAC_values\n");

     
     int setDAC[64]; // array to instore the DAC values
     for(int i=0; i<64;i++) {
          if(i==0) setDAC[i] = 0;
          else setDAC[i]=DAC_NORMINAL;
     }

     //////////////// read in the DAC values from file///////////////////////////////////////////////////////////////////
     if(m_dacFromCfgFile || m_dacFromAdjustFile){
           std::ostringstream pathToUsedDAC;
           if(m_dacFromCfgFile) pathToUsedDAC << m_pathToDACFile <<"/VATA64V2_uplink"<<uplinkId<<".cfg";
           else pathToUsedDAC << m_pathToDACFile <<"/adjustDAC_uplink" << uplinkId <<".txt";

           std::fstream dacFile(pathToUsedDAC.str().c_str(),std::fstream::in|std::fstream::out);

           if(!dacFile) {
                std::cerr<<"dacFile =  "<<pathToUsedDAC.str()<<" doesn't exist! exit abnormal!\n"<<std::endl;
                assert(false);
           }else{
                std::cout<<"dacFile = "<<pathToUsedDAC.str()<<std::endl;
           }

           std::string s;

           while(true){
                std::getline(dacFile,s);
                if(dacFile.eof()) break;

                const char* searchString = s.c_str();

                #ifdef DUMP_DEBUG
                std::cout<<"s = "<< s <<std::endl;
                getchar();
                #endif

                if(s.find("#") ==0)  continue; //skip the comment line

                if(sscanf(searchString, "Preamplifier_input_potential_DAC  || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d ||",
                      &setDAC[0], &setDAC[1], &setDAC[2], &setDAC[3], &setDAC[4],&setDAC[5], &setDAC[6],&setDAC[7], &setDAC[8],&setDAC[9], &setDAC[10],&setDAC[11], &setDAC[12],
                      &setDAC[13], &setDAC[14],&setDAC[15], &setDAC[16], &setDAC[17], &setDAC[18], &setDAC[19],&setDAC[20], &setDAC[21], &setDAC[22],&setDAC[23], &setDAC[24],
                      &setDAC[25], &setDAC[26],&setDAC[27], &setDAC[28], &setDAC[29], &setDAC[30], &setDAC[31],&setDAC[32], &setDAC[33],&setDAC[34],&setDAC[35], &setDAC[36],
                      &setDAC[37],&setDAC[38], &setDAC[39], &setDAC[40], &setDAC[41], &setDAC[42],&setDAC[43],&setDAC[44],&setDAC[45], &setDAC[46],&setDAC[47], &setDAC[48],
                      &setDAC[49], &setDAC[50],&setDAC[51], &setDAC[52],&setDAC[53], &setDAC[54], &setDAC[55],&setDAC[56],&setDAC[57], &setDAC[58],&setDAC[59],&setDAC[60],
                      &setDAC[61], &setDAC[62],&setDAC[63])==64){
                          printf("read in setDACs from %s\n",pathToUsedDAC.str().c_str());
                          for(int i = 0; i<64; i++) {printf("setDAC[%d] = %d ",i,setDAC[i]);if(i%6 ==0 &&i!=0) printf("\n");}
                          printf("\n");
                }
            }

            dacFile.close();
      }
      ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

     int xPadNum = 4;
     int yPadNum = 4;
     int padPerCanvas = xPadNum*yPadNum;


     int canvasNum = 0;
     if(NUMBER_OF_EFFECT_CHANNELS%padPerCanvas == 0)    canvasNum = NUMBER_OF_EFFECT_CHANNELS/padPerCanvas;
     else   canvasNum = NUMBER_OF_EFFECT_CHANNELS/padPerCanvas+1;
     int canvasIndex = 0;
    // printf("canvasNum = %d\n",canvasNum);

     int picIndex = 0;
     for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;chan++){

         if(m_readInSetupFile){
              if((chan%CHANNELS_PER_GROUP)>(STRIPS_PER_GROUP-1)) {
                 //  printf("ADCperPhoton: chan= %d, will be ommited!\n",chan);
                  // getchar();
                   continue;  //only fit for SiPM output
              }
         }

         //if((chan-HEAD_OF_EFFECT_CHANNELS)%padPerCanvas == 0){
         if((picIndex%padPerCanvas) == 0){
             if(m_readInSetupFile){
                   sprintf(tmp,"uplink%d_ADC/photon  (%d)",uplinkId,picIndex/padPerCanvas);
             }else{
                   if(canvasNum == 1) sprintf(tmp,"uplink%d_ADC/photon",uplinkId);
                   else sprintf(tmp,"uplink%d_ADC/photon  (%d)",uplinkId,canvasIndex+1);
             }
             c1 = new TCanvas(tmp,tmp,10,10,1000,900);
             c1->Divide(xPadNum,yPadNum);
             canvasIndex++;
             //std::cout<<"create a new canvas: now picIndex = "<<picIndex<<" chan = "<<chan<<std::endl;
             //getchar();
         }
         Double_t meanValue = 0;
         c1->cd(picIndex%padPerCanvas+1);
         picIndex++;
         sprintf(tmp,"U%d_chan%d",uplinkId,chan);
         hE = PS_adcHistogram_uplink->ProjectionY(tmp,chan+1,chan+1,"");

         if(!forMIP) meanValue = hE->GetMean();    // save the mean value for calculating meanPhoton.

         Double_t mean_full = 0;
         Float_t preThreshold = 0;
         Double_t rms_full = 0;

         double mip = 0;
         if(!forMIP){  //for the led injection, find the fit range automaticlly
               // hE->SetAxisRange(20,400,"X");
         }else {
             double corrChan=0;
             double temp = 70;
             for(int i=0; i<mpvInAdc->GetN();i++){
                   mpvInAdc->GetPoint(i,corrChan,temp);
                   if(corrChan == chan) {
                       mip = temp;
                       printf("mip = %f\n",mip);
                       break;
                   }
             }
             hE->SetAxisRange(mip-3*gain,mip+6*gain,"X");
             //hE->SetAxisRange(35,400,"X");// since the mip begins from ~35, below is the noise
         }
         mean_full = hE->GetMean();
         printf("mean_full= %f\n",mean_full);
         Int_t meanBin = hE->GetXaxis()->FindBin(mean_full);
         printf("meanBin = %d\n",meanBin);
         if(forMIP) {
             Int_t maxBin = hE->GetMaximumBin();
             preThreshold = hE->GetBinContent(maxBin);
            // printf("maxBin = %d, preThreshold = %.0f\n",maxBin,preThreshold);
            // getchar();
         }else    preThreshold = hE->GetBinContent(meanBin);


         rms_full = hE->GetRMS();
         //printf("mean_full = %f, rms_full = %f, preThreshold = %f\n",mean_full,rms_full,preThreshold);
         //getchar();

         Double_t xmin_fitRange = 0;
         if(!forMIP) xmin_fitRange =-50;
         else xmin_fitRange =30;


         Double_t fitRange_L ;
         Double_t fitRange_R;

         if(forMIP) {
//             fitRange_L = mean_full-1.5*rms_full;
//             fitRange_R = mean_full+4*rms_full;z
             if(mip-4*rms_full<30) fitRange_L = 30;
             else fitRange_L = mip-4*gain;

             fitRange_R = mip+5*gain;
             fitRange_R = mip+3*gain;

             //temper
             fitRange_L = 40;//temp for Layer24_cosmics_20120404.root
             fitRange_R = 120;//temp for Layer24_cosmics_20120404.root
//             if((uplinkId == 46 && chan == 11)||uplinkId == 47 && chan!=8) fitRange_R = 110; //temp for Layer24_cosmics_20120404.root
//             if((uplinkId == 46 && chan == 11) || uplinkId == 47 && chan == 7) fitRange_R = 100; //temp for Layer24_cosmics_20120404.root
//             if(uplinkId == 46 && chan == 11) fitRange_R = 95; //temp for Layer24_cosmics_20120404.root

             //if((uplinkId == 46 && chan == 3) || uplinkId == 46 && chan == 6 || uplinkId == 46 && chan == 8) fitRange_R = 115; //temp for Layer24_cosmics_20120405.root

             if(uplinkId == 55 && chan == 25) fitRange_R = 120; //temp for Layer14
             if(uplinkId == 55 && chan == 39) fitRange_R = 120; //temp for Layer13
             if(uplinkId == 55 && chan == 7) fitRange_R = 130; //temp for Layer13
             //if(uplinkId == 46 && chan == 4) fitRange_R = 80; //temp for Layer13
             //if(uplinkId == 46 && chan == 6) fitRange_R = 93; //temp for Layer11_cosmics_20120411.root

         }else{

             fitRange_L = -50;    // Hao
             fitRange_R =gain*3+15;
         }

         printf("before seeking peaks, set AxisRange: fitRange_L = %f, fitRange_R = %f.  gain = %f. \n",fitRange_L,fitRange_R,gain);
         //getchar();


         hE->SetAxisRange(fitRange_L,fitRange_R,"X");
         Int_t max_findPeak = (fitRange_R-fitRange_L)/gain+1;
         TSpectrum *s = new TSpectrum(max_findPeak);

         Int_t nfound = 0;

          // fit with a Gaussian
         Double_t mean=hE->GetMean();
         Double_t rms=hE->GetRMS();
         std::cout<<"chan="<<chan<<"\t mean="<<mean<<"\t rms="<<rms<<"\n";

         if(rms<2) continue; //will not fit for Gain

         Float_t xmin_gaus=mean-4*rms;
         Float_t xmax_gaus=mean+4*rms;
         TF1 *fgaus = new TF1("fgaus","[2]*TMath::Gaus(x,[0],[1])",xmin_gaus,xmax_gaus);
         Int_t bin = hE->GetXaxis()->FindBin(mean);
         Float_t A = hE->GetBinContent(bin);
         fgaus->SetParameters(mean,rms,A);
         //hE->Fit("fgaus","R");

         //hE->Add(fgaus,-0.05); //HAO

         nfound = s->Search(hE,6.0,"nobackground",0.15); //HAO

         Double_t xp_max = -100;
         Double_t xp_min = 4400;
         Double_t par[3000];
         par[0] = 0.;
         par[1] = 0.;
         npeaks = 0;
         Float_t *xpeaks = s->GetPositionX();

         float setThreshold = 0;
         setThreshold = preThreshold;
         printf("setThreshold = %f\n",setThreshold);



          xp_min=fitRange_L;
          xp_max=fitRange_R;
          npeaks=nfound;
          printf("Found %d useful peaks\n",npeaks);


          int i=0,j=0;
          Double_t adc_fc; //ADC_per_fired_cell
          Double_t mean_in_photon = 0;
          Double_t DAC_adjust = 0;
          for(i=0;i<npeaks;i++)
              for(j=0;j<(npeaks-1);j++)
              {
                  if(xpeaks[j]>xpeaks[j+1])
                  {
                  Float_t temp=xpeaks[j];
                  xpeaks[j]=xpeaks[j+1];
                  xpeaks[j+1]=temp;
                  }
              }
         std::cout<<"num="<<npeaks<<"  xpeaks sorted: ";

        for(int i=0;i<npeaks;i++)
              std::cout<< xpeaks[i]<<"  ";
         std::cout<<"\n";

         //npeaks=npeaks-1; //Do not use last peak to calculate adc_fc
         if(npeaks>2)
             npeaks=2;

         std::cout<< "Distance for npeaks:  ";
         for(int i=0;i<(npeaks-1);i++)
               std::cout<< xpeaks[i+1]-xpeaks[i]<<"  ";
          std::cout<<"\n";
         Float_t temp2=0;
         for(int i=0;i<(npeaks-1);i++)
             temp2+=(xpeaks[i+1]-xpeaks[i])*(xpeaks[i+1]-xpeaks[i]);
         temp2=temp2/(npeaks-1);
         temp2=sqrt(temp2);
         std::cout<<temp2<<"\n";
        // getchar();
        adc_fc=temp2;
        //--------------------------------------//
         // adc_fc = (hp-lp)/(npeaks-1);

          if(!forMIP) {
              mean_in_photon = meanValue/adc_fc;
              DAC_adjust = (adc_fc-m_aimGain)/(0.0335*10);//  adjust 27.6.2012 was 0.0335*5) guido
              printf("Guido adc_fc = %f, m_aimGain = %d, DAC_adjust = %f,uplinkId = %d, chan = %d\n",adc_fc,m_aimGain,DAC_adjust,uplinkId,chan);
              //getchar();
          }

          if(forMIP){
                std::cout<<"uplink="<<uplinkId<<"\t chan="<<chan <<"\t ADCperPhoton="<<adc_fc<<"\n";
                fprintf(resultLog,"%d\t %d\t %f\t\n",uplinkId,chan,adc_fc);
          }else{
                std::cout<<"uplink="<<uplinkId<<"\t chan="<<chan <<"\t ADCperPhoton="<<adc_fc<<"\t meanInPhoton ="<<mean_in_photon<<"\t DAC_adjust = "<<DAC_adjust<<"\t former DAC = "<<setDAC[chan]<<"\t\n";
               // fprintf(resultLog,"%d\t %d\t %f\t %f\t %f\t %d\t\n",uplinkId,chan,adc_fc,mean_in_photon,DAC_adjust,setDAC[chan]);
                fprintf(resultLog,"%d\t %d\t %f\t %f\t %f\t %d\t       %d\t\n",uplinkId,chan,adc_fc,mean_in_photon,DAC_adjust,setDAC[chan],setDAC[chan]+int(DAC_adjust));//HAO

                // make sure that the DAC value will never be out of range!!
                if( (setDAC[chan]+DAC_adjust)<1 )  setDAC[chan] = 1;
                else if((setDAC[chan]+DAC_adjust)>256) setDAC[chan] = 255;
                else setDAC[chan] += DAC_adjust;

                #ifdef DUMP_DEBUG
                printf("setDAC[%d] = %d\n",chan,setDAC[chan]);
                #endif
          }
          hE->DrawCopy();

         //if(!forMIP)  fgaus->Draw("SAME");
          c1->Update();
          //getchar();

          gains->SetPoint(gains_index,chan,adc_fc);
          if(!forMIP) meanInPhoton->SetPoint(gains_index,chan,mean_in_photon);


          gains_index++;
         // }
     }

     if(gains_index!=NUMBER_OF_EFFECT_CHANNELS) {
          printf("Not all the gain values are stored in m_adcPerPhoton.at(%d) for uplink%d!!!!\n",uplinkIndex(uplinkId),uplinkId);
          //getchar();
     }

     fclose(resultLog);
     hE = NULL;

     //open a file to log the DAC values
     if(!forMIP) {
         if(m_dacFromAdjustFile){
             std::ostringstream pathToNewDAC;
             if(m_dacFromCfgFile) pathToNewDAC <<"../data/adjustDAC_uplink"<<uplinkId<<".txt";
             else pathToNewDAC << m_pathToDACFile <<"/adjustDAC_uplink" << uplinkId <<".txt";

             std::fstream newDacFile(pathToNewDAC.str().c_str(),std::fstream::in|std::fstream::out);
             std::cout<<"newDacFile = "<<pathToNewDAC.str()<<std::endl;

             if(!newDacFile) {
                 std::cerr<<"saving new DACs: newDacFile =  "<<pathToNewDAC.str()<<" doesn't exist! exit abnormal!\n"<<std::endl;
                 assert(false);
             }else{
                 std::cout<<"DAC is read from adjust file, modified DACs in this file: newDacFile =  "<<pathToNewDAC.str()<<std::endl;
             }

             newDacFile.seekg(0,std::ios::beg);// move read point to the head of the file
             long posR = newDacFile.tellg();
             #ifdef DUMP_DEBUG
                 printf("before reading get point is newDacFile.tellg() = %ld,\n",posR);
                 printf("before reading put point is newDacFile.tellp() = %ld,\n",newDacFile.tellp());
             #endif

             std::string s;

             while(true){
                 posR = newDacFile.tellg();
                #ifdef DUMP_DEBUG
                 printf("posR = %ld\n",posR);
                 getchar();
                #endif


                 std::getline(newDacFile,s);
                 if(newDacFile.eof()) break;

                 #ifdef DUMP_DEBUG
                 std::cout<<"s = "<< s <<std::endl;
                 getchar();
                 #endif

                 if(s.find("#") ==0)  continue; //skip the comment line
                 if(s.find("Preamplifier_input_potential_DAC") == 0) {
                     newDacFile.seekp(posR);
                     newDacFile<<"#";
                 }
             }
             newDacFile.clear();
             newDacFile.seekp(0,std::ios::end);
             newDacFile<<"#For uplinkId= "<<uplinkId<<", aimGain = "<<m_aimGain<<",try the following DAC settings"<<std::endl;
             newDacFile<<"Preamplifier_input_potential_DAC ";
             for(int i = 0; i<64;i++) {
                 newDacFile<<" || "<< setDAC[i]<<" ||";
                 if(i==63) newDacFile<<std::endl;
             }
             newDacFile.close();
         }
//         if(m_dacFromCfgFile){
//             sprintf(tmp,"../data/adjustDAC_uplink%d.txt",uplinkId);
//             printf("since DAC is read from cfg file,generate a new file = %s to save new DACs.\n",tmp);
//             FILE* dacList = fopen(tmp,"w");

//             fprintf(dacList,"#DAC valueds for Gain = %d on uplinkId=%d\n",m_aimGain,uplinkId);
//             fprintf(dacList,"Preamplifier_input_potential_DAC");
//             for(int i=0; i<64;i++) fprintf(dacList," || %d ||",setDAC[i]);
//             fprintf(dacList,"\n");
//             fclose(dacList);
//         }
     }

     // if need to write new DAC values to the cfg file
     if((!forMIP)&&m_forceToChangeCfgFile){
         std::ostringstream pathToCfgFile;
         pathToCfgFile << m_pathToFormerCfgFile <<"/VATA64V2_uplink" << uplinkId <<".cfg";

         std::fstream cfgFile(pathToCfgFile.str().c_str(),std::fstream::in|std::fstream::out);
         std::cout<<"cfgFile = "<<pathToCfgFile.str()<<std::endl;

         if(!cfgFile) {
             std::cerr<<"To be modified file: cfgFile =  "<<pathToCfgFile.str()<<" doesn't exist! exit abnormal!\n"<<std::endl;
             assert(false);
         }else{
             std::cout<<"To be modified file: cfgFile =  "<<pathToCfgFile.str()<<std::endl;
         }

         cfgFile.seekg(0,std::ios::beg);// move read point to the head of the file
         long posR = cfgFile.tellg();
         #ifdef DUMP_DEBUG
             printf("before reading get point is newDacFile.tellg() = %ld,\n",posR);
             printf("before reading put point is newDacFile.tellp() = %ld,\n",cfgFile.tellp());
         #endif

         std::string s;
         while(true){
             posR = cfgFile.tellg();
            #ifdef DUMP_DEBUG
             printf("posR = %ld\n",posR);
             getchar();
            #endif


             std::getline(cfgFile,s);
             if(cfgFile.eof()) break;

             #ifdef DUMP_DEBUG
             std::cout<<"s = "<< s <<std::endl;
             getchar();
             #endif

             if(s.find("#") ==0)  continue; //skip the comment line
             // comment all the old DAC values
             if(s.find("Preamplifier_input_potential_DAC") == 0) {
                 cfgFile.seekp(posR);
                 cfgFile<<"#";
                // cfgFile.seekp(0,std::ios::end);
                // cfgFile<<"Preamplifier_input_potential_DAC ";
                // for(int i = 0; i<64;i++) {
                //     cfgFile<<" || "<< setDAC[i]<<" ||";
                //     if(i==63) cfgFile<<std::endl;
                // }
             }
         }
         cfgFile.clear();
         cfgFile.seekp(0,std::ios::end);
         cfgFile<<"#For uplinkId = "<<uplinkId<<", aimGain = "<<m_aimGain<<", try the following DAC settings"<<std::endl;
         cfgFile<<"Preamplifier_input_potential_DAC ";
         for(int i = 0; i<64;i++) {
             cfgFile<<" || "<< setDAC[i]<<" ||";
             if(i==63) cfgFile<<std::endl;
         }
         cfgFile.close();
     }




     //make a new TCanvas to plot ADC/photon for all the channels
     if(plot){
         sprintf(tmp,"uplink%d    ADC/photon Variation",uplinkId);
         TCanvas *c2 = new TCanvas(tmp,tmp,10,10,1000,900);
         if(forMIP) c2->Divide(1,1);
         else c2->Divide(1,2);

         gStyle->SetLabelFont(132,"XY");
         gStyle->SetLabelSize(0.06,"XY");

         gStyle->SetTitleFont(132,"XY");
         gStyle->SetTitleSize(0.06,"XY");
         gStyle->SetTitleYOffset(0.8);
         gStyle->SetTitleXOffset(0.8);

         c2->cd(1);

         TH1F *gainVariation = c2->cd(1)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,9,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,15);
         gainVariation ->SetXTitle("channel #");
         gainVariation ->SetYTitle("gain, ADC");

         gains->SetMarkerStyle(20);
         gains->SetMarkerColor(kRed);
         gains->SetMarkerSize(2.);

         gains->Fit("pol1");
         gains->Draw("PE");

         c2->cd(1)->SetGridx();
         c2->cd(1)->SetGridy();

         if(!forMIP){
            c2->cd(2);

            TH1F *meanInPhotonVariation = c2->cd(2)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,20);
            meanInPhotonVariation ->SetXTitle("channel #");
            meanInPhotonVariation ->SetYTitle("mean, p.e.");

            meanInPhoton->SetMarkerStyle(20);
            meanInPhoton->SetMarkerColor(kRed);
            meanInPhoton->SetMarkerSize(2.);

            meanInPhoton->Fit("pol1");
            meanInPhoton->Draw("PE");

            c2->cd(2)->SetGridx();
            c2->cd(2)->SetGridy();
         }
     }

 }
Double_t langaufun(Double_t *x, Double_t *par) {
    
    //Fit parameters:
    //par[0]=Width (scale) parameter of Landau density
    //par[1]=Most Probable (MP, location) parameter of Landau density
    //par[2]=Total area (integral -inf to inf, normalization constant)
    //par[3]=Width (sigma) of convoluted Gaussian function
    //
    //In the Landau distribution (represented by the CERNLIB approximation), 
    //the maximum is located at x=-0.22278298 with the location parameter=0.
    //This shift is corrected within this function, so that the actual
    //maximum is identical to the MP parameter.
    
    // Numeric constants
    Double_t invsq2pi = 0.3989422804014;   // (2 pi)^(-1/2)
    Double_t mpshift  = -0.22278298;       // Landau maximum location
    
    // Control constants
    Double_t np = 100.0;      // number of convolution steps
    Double_t sc =   5.0;      // convolution extends to +-sc Gaussian sigmas
    
    // Variables
    Double_t xx;
    Double_t mpc;
    Double_t fland;
    Double_t sum = 0.0;
    Double_t xlow,xupp;
    Double_t step;
    Double_t i;
    
    
    // MP shift correction
    mpc = par[1] - mpshift * par[0]; 
    
    // Range of convolution integral
    xlow = x[0] - sc * par[3];
    xupp = x[0] + sc * par[3];
    
    step = (xupp-xlow) / np;
    
    // Convolution integral of Landau and Gaussian by sum
    for(i=1.0; i<=np/2; i++) {
        xx = xlow + (i-.5) * step;
        fland = TMath::Landau(xx,mpc,par[0]) / par[0];
        sum += fland * TMath::Gaus(x[0],xx,par[3]);
        
        xx = xupp - (i-.5) * step;
        fland = TMath::Landau(xx,mpc,par[0]) / par[0];
        sum += fland * TMath::Gaus(x[0],xx,par[3]);
    }
    
    return (par[2] * step * sum * invsq2pi / par[3]);
}


TF1 *langaufit(TH1D *his, Double_t *fitrange, Double_t *startvalues, Double_t *parlimitslo, Double_t *parlimitshi, Double_t *fitparams, Double_t *fiterrors, Double_t *ChiSqr, Int_t *NDF)
{
    // Once again, here are the Landau * Gaussian parameters:
    //   par[0]=Width (scale) parameter of Landau density
    //   par[1]=Most Probable (MP, location) parameter of Landau density
    //   par[2]=Total area (integral -inf to inf, normalization constant)
    //   par[3]=Width (sigma) of convoluted Gaussian function
    //
    // Variables for langaufit call:
    //   his             histogram to fit
    //   fitrange[2]     lo and hi boundaries of fit range
    //   startvalues[4]  reasonable start values for the fit
    //   parlimitslo[4]  lower parameter limits
    //   parlimitshi[4]  upper parameter limits
    //   fitparams[4]    returns the final fit parameters
    //   fiterrors[4]    returns the final fit errors
    //   ChiSqr          returns the chi square
    //   NDF             returns ndf
    
    Int_t i;
    Char_t FunName[100];
    
    sprintf(FunName,"Fitfcn_%s",his->GetName());
    
    TF1 *ffitold = (TF1*)gROOT->GetListOfFunctions()->FindObject(FunName);
    if (ffitold) delete ffitold;
    
    TF1 *ffit = new TF1(FunName,langaufun,fitrange[0],fitrange[1],4);
    ffit->SetParameters(startvalues);
    ffit->SetParNames("Width","MP","Area","GSigma");
    
    for (i=0; i<4; i++) {
        ffit->SetParLimits(i, parlimitslo[i], parlimitshi[i]);
    }
    
    his->Fit(FunName,"RB");   // fit within specified range, use ParLimits, do not plot
    his->GetFunction(FunName)->SetLineWidth(3);
    
    ffit->GetParameters(fitparams);    // obtain fit parameters
    for (i=0; i<4; i++) {
        fiterrors[i] = ffit->GetParError(i);     // obtain fit parameter errors
    }
    ChiSqr[0] = ffit->GetChisquare();  // obtain chi^2
    NDF[0] = ffit->GetNDF();           // obtain ndf
    
    return (ffit);              // return fit function
    
}
        
void DataManager::PlotAnalysisResult_1uplink(int uplinkId, std::string pedestalFilePath, std::string signalFilePath){

    char tmp[256];

    sprintf(tmp,"uplink %d\t%s\t%s",uplinkId,pedestalFilePath.c_str(),signalFilePath.c_str());
    TCanvas *c1 = new TCanvas(tmp,tmp,1280,1280);
    c1->Divide(2,3);


    gStyle->SetLabelFont(132,"XY");
    gStyle->SetLabelSize(0.06,"XY");

    gStyle->SetTitleFont(132,"XY");
    gStyle->SetTitleSize(0.06,"XY");
    gStyle->SetTitleYOffset(0.8);
    gStyle->SetTitleXOffset(0.8);

    //draw ADC per Photon
    c1->cd(1);
    TGraphErrors* graph1 = m_adcPerPhoton.at(uplinkIndex(uplinkId));
    TH1F *hr1 = c1->cd(1)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,8,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,14);

    hr1->SetXTitle("channel #");
    hr1->SetYTitle("gain(ADC per Photon), ADC");

    graph1->SetMarkerStyle(20);
    graph1->SetMarkerColor(kRed);
    graph1->SetMarkerSize(2.);

    graph1->Fit("pol1");
    graph1->Draw("PE");

    c1->cd(1)->SetGridx();
    c1->cd(1)->SetGridy();


    //draw MPV in ADC
    c1->cd(2);
    TGraphErrors* graph2 = m_mpvInAdc.at(uplinkIndex(uplinkId));
    TH1F *hr2 = c1->cd(2)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,40,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,160);

    hr2->SetXTitle("channel #");
    hr2->SetYTitle("MPV, ADC");

    graph2->SetMarkerStyle(21);
    graph2->SetMarkerColor(kRed);
    graph2->SetMarkerSize(2.);

    graph2->Fit("pol1");
    graph2->Draw("PE");

    //c1->cd(2)->SetGridx();
    //c1->cd(2)->SetGridy();

    //draw resultCorr : MPV in photon electron
    c1->cd(3);
    TGraphErrors* graph3 = m_resultCorr.at(uplinkIndex(uplinkId));
    TH1F *hr3 = c1->cd(3)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,15);

    hr3->SetXTitle("channel #");
    hr3->SetYTitle("MPV, p.e.");

    graph3->SetMarkerStyle(22);
    graph3->SetMarkerColor(kRed);
    graph3->SetMarkerSize(2.);

    graph3->Fit("pol1");
    graph3->Draw("PE");

    c1->cd(3)->SetGridx();
    c1->cd(3)->SetGridy();

    //draw area:
    c1->cd(4);
    TH1F *hr4 = m_area.at(uplinkIndex(uplinkId));

    hr4->SetLineColor(kRed);
    hr4->SetLineWidth(3);
    hr4->Draw();

    c1->cd(4)->SetGridx();
    c1->cd(4)->SetGridy();

    //draw Lsigma in photon electron
    c1->cd(5);
    TGraphErrors* graph5 = m_lSigma.at(uplinkIndex(uplinkId));
    TH1F *hr5 = c1->cd(5)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,2.5);

    hr5->SetXTitle("channel #");
    hr5->SetYTitle("Landau width, p.e.");

    graph5->SetMarkerStyle(23);
    graph5->SetMarkerColor(kRed);
    graph5->SetMarkerSize(2.);

    graph5->Fit("pol1");
    graph5->Draw("PE");

    c1->cd(5)->SetGridx();
    c1->cd(5)->SetGridy();

    //draw Gsigma in photon electron
    c1->cd(6);
    TGraphErrors* graph6 = m_gSigma.at(uplinkIndex(uplinkId));
    TH1F *hr6 = c1->cd(6)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,7);

    hr6->SetXTitle("channel #");
    hr6->SetYTitle("Gauss width, p.e.");

    graph6->SetMarkerStyle(33);
    graph6->SetMarkerColor(kRed);
    graph6->SetMarkerSize(2.);

    graph6->Fit("pol1");
    graph6->Draw("PE");

    c1->cd(6)->SetGridx();
    c1->cd(6)->SetGridy();

    //c1->Update();
}


void DataManager::PlotAnalysisResult(std::string pedestalFilePath, std::string signalFilePath){

    char tmp[256];

    sprintf(tmp," Analysis Result\t%s\t%s",pedestalFilePath.c_str(),signalFilePath.c_str());
    TCanvas *c1 = new TCanvas(tmp,tmp,1280,1280);
   // c1->Divide(2,3);
    c1->Divide(2,2);

    gStyle->SetLabelFont(132,"XY");
    gStyle->SetLabelSize(0.06,"XY");

    gStyle->SetTitleFont(132,"XY");
    gStyle->SetTitleSize(0.06,"XY");
    gStyle->SetTitleYOffset(0.8);
    gStyle->SetTitleXOffset(0.8);

    TGraphErrors* graph = NULL;
    TH1F *hr = NULL;
    TLegend *legend =NULL;



    //draw ADC per Photon
    c1->cd(1);

    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    TGraphErrors* graph_omitDamagedChannels =NULL;


    for(unsigned int i=0; i<m_adcPerPhoton.size();i++){
         graph = m_adcPerPhoton.at(i);
         graph_omitDamagedChannels = new TGraphErrors(1);
         double chan=0;
         double getAdcPerPhoton =0;
         int index_new = 0;

         for(int index = 0; index<NUMBER_OF_EFFECT_CHANNELS; index++){
             graph->GetPoint(index,chan,getAdcPerPhoton);
             std::cout<<"index= "<<index<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
             if(getAdcPerPhoton<ADC_PER_PHOTON_UPPER_THRESHOLD) {
                 graph_omitDamagedChannels->SetPoint(index_new,chan,getAdcPerPhoton);
                 std::cout<<"index_new= "<<index_new<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
                 index_new++;
             }
          }


         sprintf(tmp,"uplink%d",m_uplinkId.at(i));
         legend->AddEntry(graph_omitDamagedChannels,tmp, "p");
         if(i==0){
             hr = c1->cd(1)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,8,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,20);
             hr->SetXTitle("channel #");
             hr->SetYTitle("gain(ADC per Photon), ADC");
         }

         graph_omitDamagedChannels->SetMarkerStyle(20+i);
         //graph->SetMarkerColor(kRed+i);
         graph_omitDamagedChannels->SetMarkerColor(2+2*i);
         graph_omitDamagedChannels->SetMarkerSize(1.);

         graph_omitDamagedChannels->Fit("pol1");
         graph_omitDamagedChannels->GetFunction("pol1")->SetLineColor(2+2*i);
         if(i==0) graph_omitDamagedChannels->Draw("PE");
         else graph_omitDamagedChannels->Draw("PE SAME");

         if(i==0){
             c1->cd(1)->SetGridx();
             c1->cd(1)->SetGridy();
         }
    }
    legend->Draw();



    //draw MPV in ADC
    c1->cd(2);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    for(unsigned int i=0; i<m_mpvInAdc.size();i++){
         graph = m_mpvInAdc.at(i);

         sprintf(tmp,"uplink%d",m_uplinkId.at(i));
         legend->AddEntry(graph,tmp, "p");

         if(i==0){
            hr = c1->cd(2)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,40,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,160);
            hr->SetXTitle("channel #");
            hr->SetYTitle("MPV, ADC");
         }

         graph->SetMarkerStyle(20+i);
        // graph->SetMarkerColor(kRed+i);
         graph->SetMarkerColor(2+2*i);
         graph->SetMarkerSize(1.);

         graph->Fit("pol1");
         //graph->GetFunction("pol1")->SetLineColor(kRed+i);
         graph->GetFunction("pol1")->SetLineColor(2+2*i);

         if(i==0) graph->Draw("PE");
         else graph->Draw("PE SAME");

         if(i==0){
             c1->cd(2)->SetGridx();
             c1->cd(2)->SetGridy();
         }
    }
    legend->Draw();

    //draw resultCorr : MPV in photon electron
    c1->cd(3);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC");  //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    for(unsigned int i=0; i<m_resultCorr.size();i++){
         graph = m_resultCorr.at(i);

         sprintf(tmp,"uplink%d",m_uplinkId.at(i));
         legend->AddEntry(graph,tmp, "p");

         if(i==0){
             hr = c1->cd(3)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,15);
             hr->SetXTitle("channel #");
             hr->SetYTitle("MPV, p.e.");
         }
         graph->SetMarkerStyle(20+i);
         //graph->SetMarkerColor(kRed+i);
         graph->SetMarkerColor(2+2*i);
         graph->SetMarkerSize(1.);

         graph->Fit("pol1");
         //graph->GetFunction("pol1")->SetLineColor(kRed+i);
         graph->GetFunction("pol1")->SetLineColor(2+2*i);
         if(i==0) graph->Draw("PE");
         else graph->Draw("PE SAME");

         if(i==0){
             c1->cd(3)->SetGridx();
             c1->cd(3)->SetGridy();
         }
    }
    legend->Draw();

    //draw area:
    c1->cd(4);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC");  //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    for(unsigned int i=0; i<m_area.size();i++){
         hr = m_area.at(i);
         sprintf(tmp,"uplink%d",m_uplinkId.at(i));
         legend->AddEntry(hr,tmp, "L");

         //hr->SetLineColor(kRed+i);
         hr->SetLineColor(2+2*i);
         hr->SetLineWidth(3);
         if(i==0) hr->DrawCopy();
         else hr->DrawCopy("SAME");

         if(i==0){
             c1->cd(4)->SetGridx();
             c1->cd(4)->SetGridy();
         }
    }
    legend->Draw();
/*
    //draw Lsigma in photon electron
    c1->cd(5);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    for(int i=0; i<m_lSigma.size();i++){
         graph = m_lSigma.at(i);
         sprintf(tmp,"uplink%d",m_uplinkId.at(i));
         legend->AddEntry(graph,tmp, "p");

         if(i==0){
            hr = c1->cd(5)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,2.5);
            hr->SetXTitle("channel #");
            hr->SetYTitle("Landau width, p.e.");
         }
         graph->SetMarkerStyle(20+i);
         //graph->SetMarkerColor(kRed+i);
         graph->SetMarkerColor(2+2*i);
         graph->SetMarkerSize(1.);

         graph->Fit("pol1");
         //graph->GetFunction("pol1")->SetLineColor(kRed+i);
         graph->GetFunction("pol1")->SetLineColor(2+2*i);
         if(i==0) graph->Draw("PE");
         else graph->Draw("PE SAME");

         if(i==0){
             c1->cd(5)->SetGridx();
             c1->cd(5)->SetGridy();
         }
    }
    legend->Draw();

    //draw Gsigma in photon electron
    c1->cd(6);
    legend = new TLegend(0.75,0.94,0.95,0.75,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    for(int i=0; i<m_gSigma.size();i++){
         graph = m_gSigma.at(i);
         sprintf(tmp,"uplink%d",m_uplinkId.at(i));
         legend->AddEntry(graph,tmp, "p");

         if(i==0){
            hr = c1->cd(6)->DrawFrame(HEAD_OF_EFFECT_CHANNELS,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,7);
            hr->SetXTitle("channel #");
            hr->SetYTitle("Gauss width, p.e.");
         }

         graph->SetMarkerStyle(20+i);
         //graph->SetMarkerColor(kRed+i);
         graph->SetMarkerColor(2+2*i);
         graph->SetMarkerSize(1.);

         graph->Fit("pol1");
         //graph->GetFunction("pol1")->SetLineColor(kRed+i);
         graph->GetFunction("pol1")->SetLineColor(2+2*i);
         if(i==0) graph->Draw("PE");
         else graph->Draw("PE SAME");

         if(i==0){
             c1->cd(6)->SetGridx();
             c1->cd(6)->SetGridy();
         }
    }
    legend->Draw();
    //c1->Update();
  */
}


void DataManager::PlotProjectionY(bool forceInLog){
    printf("m_uplinkId.size() = %d\n",m_uplinkId.size());
    for(unsigned int i=0; i<m_uplinkId.size();i++)  ProjectionYWithMean(m_uplinkId.at(i),forceInLog);
}

void DataManager::AnalyzeLedData(Float_t gain){
    for(unsigned int i=0; i<m_uplinkId.size();i++) {
        //if(m_uplinkId.at(i) == 46)  ADCPerPhoton(m_uplinkId.at(i),40,200,15,150,false,true);
        //else ADCPerPhoton(m_uplinkId.at(i),xmin_ADC,xmax_ADC,max_npeak,threshold,false);
         ADCPerPhoton(m_uplinkId.at(i),gain,false);
    }

    PlotAnalysisResult_LED();
    //PlotAnalysisResult_LED_InStrip();

}

void DataManager::AnalyzeCosmicData(Float_t gain,Int_t rebin_mpv){
    for(unsigned int i=0; i<m_uplinkId.size();i++) {
        //if(m_uplinkId.at(i) == 45 ||m_uplinkId.at(i) ==47)  ADCPerPhoton(m_uplinkId.at(i),45,100,5,400,false,true);  //for cosmics_27_1_2012
        //else ADCPerPhoton(m_uplinkId.at(i),xmin_ADC_gain,xmax_ADC_gain,max_npeak_gain,threshold,false,true);
        MPVinADC(m_uplinkId.at(i),rebin_mpv);
        ADCPerPhoton(m_uplinkId.at(i),gain,false,true);
        MPVinPE(m_uplinkId.at(i));
       // MPVinADC(m_uplinkId.at(i),rebin_mpv);
    }

    PlotAnalysisResult();
    PlotAnalysisResult_InStrip();

}


void DataManager::PlotAnalysisResult_LED(){

    char tmp[256];

    sprintf(tmp," Analysis Result with Light injection");
    TCanvas *c1 = new TCanvas(tmp,tmp,1280,1280);

    c1->Divide(1,2);


    gStyle->SetOptFit(1000);
    gStyle->SetLabelFont(132,"XY");
    gStyle->SetLabelSize(0.06,"XY");

    gStyle->SetTitleFont(132,"XY");
    gStyle->SetTitleSize(0.06,"XY");
    gStyle->SetTitleYOffset(0.8);
    gStyle->SetTitleXOffset(0.8);




    TGraphErrors* graph = NULL;
    TH1F *hr = NULL;
    TLegend *legend =NULL;
    TGraphErrors* graph_omitDamagedChannels = NULL;


    //draw ADC per Photon
    std::cout<<"\n\n\ndraw adcPerPhoton \n";
    c1->cd(1);

    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    for(unsigned int i=0; i<m_adcPerPhoton.size();i++){

         graph = m_adcPerPhoton.at(i);
         graph_omitDamagedChannels = new TGraphErrors(1);

         double chan=0;
         double getAdcPerPhoton =0;
         int index_new = 0;

         for(int index = 0; index<NUMBER_OF_EFFECT_CHANNELS; index++){
             graph->GetPoint(index,chan,getAdcPerPhoton);
             std::cout<<"index= "<<index<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
             if(getAdcPerPhoton<ADC_PER_PHOTON_UPPER_THRESHOLD) {
                 graph_omitDamagedChannels->SetPoint(index_new,chan,getAdcPerPhoton);
                 std::cout<<"index_new= "<<index_new<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
                 index_new++;
             }
          }
          sprintf(tmp,"uplink%d",m_uplinkId.at(i));
          legend->AddEntry(graph_omitDamagedChannels,tmp, "p");
          if(i==0){
                 hr = c1->cd(1)->DrawFrame(HEAD_OF_EFFECT_CHANNELS-1,8,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS+1,80);
                 //hr = c1->cd(1)->DrawFrame(0,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS+1,14);
                 hr->SetXTitle("channel #");
                 hr->SetYTitle("gain(ADC per Photon), ADC");
          }

          graph_omitDamagedChannels->SetMarkerStyle(20+i);
          //graph->SetMarkerColor(kRed+i);
          graph_omitDamagedChannels->SetMarkerColor(2+2*i);
          graph_omitDamagedChannels->SetMarkerSize(1.);

          graph_omitDamagedChannels->Fit("pol1");
          graph_omitDamagedChannels->GetFunction("pol1")->SetLineColor(2+2*i);
          if(i==0) graph_omitDamagedChannels->Draw("PE");
          else graph_omitDamagedChannels->Draw("PE SAME");

          if(i==0){
               c1->cd(1)->SetGridx();
               c1->cd(1)->SetGridy();
           }

          legend->Draw();

    }

    //draw meanInPhoton
    std::cout<<"\n\n\ndraw meaninPhoton \n";
    c1->cd(2);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    for(unsigned int i=0; i<m_meanInPhoton.size();i++){
         graph = m_meanInPhoton.at(i);
         graph_omitDamagedChannels = new TGraphErrors(1);

         double chan=0;
         double getMeanInPhoton =0;
         int index_new = 0;

         for(int index = 0; index<NUMBER_OF_EFFECT_CHANNELS; index++){
             graph->GetPoint(index,chan,getMeanInPhoton);
             std::cout<<"index= "<<index<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",meanInPhoton = "<<getMeanInPhoton<<std::endl;
             if(getMeanInPhoton>MEAN_IN_PHOTON_UNDER_THRESHOLD) {
                 graph_omitDamagedChannels->SetPoint(index_new,chan,getMeanInPhoton);
                 std::cout<<"index_new= "<<index_new<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<chan<<",meanInPhoton = "<<getMeanInPhoton<<std::endl;
                 index_new++;

             }
          }

         sprintf(tmp,"uplink%d",m_uplinkId.at(i));
         legend->AddEntry(graph_omitDamagedChannels,tmp, "p");

         if(i==0){
            hr = c1->cd(2)->DrawFrame(HEAD_OF_EFFECT_CHANNELS-1,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS+1,20);
            //hr = c1->cd(2)->DrawFrame(0,0,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS,20);
            hr->SetXTitle("channel #");
            hr->SetYTitle("mean,p.e.");
         }

         graph_omitDamagedChannels->SetMarkerStyle(20+i);
        // graph->SetMarkerColor(kRed+i);
         graph_omitDamagedChannels->SetMarkerColor(2+2*i);
         graph_omitDamagedChannels->SetMarkerSize(1.);

         graph_omitDamagedChannels->Fit("pol1");
         //graph->GetFunction("pol1")->SetLineColor(kRed+i);
         graph_omitDamagedChannels->GetFunction("pol1")->SetLineColor(2+2*i);

         if(i==0) graph_omitDamagedChannels->Draw("PE");
         else graph_omitDamagedChannels->Draw("PE SAME");

         if(i==0){
             c1->cd(2)->SetGridx();
             c1->cd(2)->SetGridy();
         }
    }

    legend->Draw();

//    //draw ratio of number of photons between left and right side of the strip
//    std::cout<<"\n\n\ndraw ratio L/R meanInPhoton \n";
//    bool hasCorrelation = false;
//    if(m_readInSetupFile){
//        if(m_corrUplinkPairs.size()) hasCorrelation = true;
//        else hasCorrelation = false;
//    }else{
//        if(m_uplink_correlation.size()) hasCorrelation = true;
//        else hasCorrelation = false;
//    }
//    if(hasCorrelation){
//         FILE* resultLog = NULL;

//         c1->cd(3);
//         legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
//         legend->SetFillColor(kWhite);
//         legend->SetBorderSize(0);
//         legend->SetTextFont(132);
//         legend->SetTextSize(0.06);

//         hr = c1->cd(3)->DrawFrame(0,0.3,NUMBER_OF_EFFECT_CHANNELS,2.5);
//         sprintf(tmp,"strip X-> U%d_chX+3,  strip # ",m_uplinkId.at(0));
//         hr->SetXTitle(tmp);
//         sprintf(tmp,"mean ratio, p.e.");
//         hr->SetYTitle(tmp);

//         Double_t corrChan = 0;
//         Double_t corrMeanInPhoton = 0;
//         Double_t chan_aim = 0;
//         Double_t meanInPhoton_aim = 0;
//         Double_t ratioLR = 0;

//         int corrSum = m_uplink_correlation.size()/NUMBER_OF_EFFECT_CHANNELS;

//         for(int corrIndex=0; corrIndex< corrSum ; corrIndex++){

//              //open a file to log the result
//              sprintf(tmp,"../data/ratioLR_U%d_U%d.txt",m_uplinkId.at(2*corrIndex),m_uplinkId.at(2*corrIndex+1));
//              resultLog = fopen(tmp,"w");
//              fprintf(resultLog,"-----ratioLR U%d/U%d --------\n\n",m_uplinkId.at(2*corrIndex),m_uplinkId.at(2*corrIndex+1));
//              fprintf(resultLog,"uplink_side1\t channel\t uplink_side2\t channel\t ratioLR\n");

//              graph = m_meanInPhoton.at(2*corrIndex);
//              TGraphErrors* graph2 = m_meanInPhoton.at(2*corrIndex+1);

//              TGraphErrors* ratio =new TGraphErrors(1);  // since chan14 is dead on both cards until 20_1_2012

//              Int_t ratio_index =0;
//              Double_t corrNum = 2*HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1;
//              for(Int_t j=0; j< NUMBER_OF_EFFECT_CHANNELS;j++){
//                     graph->GetPoint(j,chan_aim,meanInPhoton_aim);   //
//                     std::cout<<"chan_aim = "<<chan_aim<<" on uplinkId = "<<m_uplinkId.at(2*corrIndex)<<",meanInPhoton_aim = "<<meanInPhoton_aim<<std::endl;
//                     if(meanInPhoton_aim>MEAN_IN_PHOTON_UNDER_THRESHOLD ){        //change threshold
//                           for(Int_t i=0; i< NUMBER_OF_EFFECT_CHANNELS;i++){
//                                 graph2->GetPoint(i,corrChan,corrMeanInPhoton);
//                                 std::cout<<"corrChan = "<<corrChan<<" on uplinkId = "<<m_uplinkId.at(2*corrIndex+1)<<",corrMeanInPhoton = "<<corrMeanInPhoton<<std::endl;
//                                 if((corrChan == (corrNum-chan_aim))&&(corrMeanInPhoton>MEAN_IN_PHOTON_UNDER_THRESHOLD)){  //change threshold
//                                      ratioLR = meanInPhoton_aim/corrMeanInPhoton;   //ratio = m_meanInPhoton.at(0)/m_meaninPhoton.at(1)
//                                      ratio->SetPoint(ratio_index,chan_aim-3,ratioLR);
//                                      std::cout<<"ratio: point_index = "<<ratio_index<<",chan_aim = "<<chan_aim<<" on uplinkId = "<<m_uplinkId.at(2*corrIndex)<<",ratioLR = "<<ratioLR<<std::endl;
//                                     // getchar();
//                                      fprintf(resultLog,"%d\t %2.0f\t %d\t %2.0f\t %f\t\n",m_uplinkId.at(2*corrIndex),chan_aim,m_uplinkId.at(2*corrIndex+1),corrChan,ratioLR);
//                                      ratio_index++;
//                                      break;
//                                 }
//                          }
//                     }
//              }

//              fclose(resultLog);

//              sprintf(tmp,"U%d/U%d",m_uplinkId.at(corrIndex*2),m_uplinkId.at(corrIndex*2+1));
//              legend->AddEntry(ratio,tmp, "p");

//              ratio->SetMarkerStyle(20+corrIndex);
//              ratio->SetMarkerColor(2+2*corrIndex);
//              ratio->SetMarkerSize(1.);


//              ratio->Fit("pol1");
//              ratio->GetFunction("pol1")->SetLineColor(2+2*corrIndex);

//              if(corrIndex==0) ratio->Draw("PE");
//              else ratio->Draw("PE SAME");

//          }

//         c1->cd(3)->SetGridx();
//         c1->cd(3)->SetGridy();

//         legend->Draw();
//    }

}


void DataManager::PlotAnalysisResult_LED_InStrip(){

    // if read in setup file, define the strip range to be showed
    int stripStart = 0;
    int stripEnd = 0;
    SuperLayerMap::iterator superLayer_iter;
    if(m_readInSetupFile){
        int minPosition = 16; // since for two superLayer, position can only be 0~15 ,change here if there are more superLayers
        int maxPosition = -1;
        int currPosition = 0;
        for(int uplinkIndex =0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
            int currUplinkId = m_uplinkId.at(uplinkIndex);
            for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                     if(superLayer_iter->second== currUplinkId) {
                         currPosition = superLayer_iter->first%P_N_DISTANCE + BOARDS_PER_SUPERLAYER*LayerIndex;
                         if(currPosition>maxPosition) maxPosition = currPosition;
                         if(currPosition<minPosition) minPosition = currPosition;
                         break;
                     }
                }
            }
        }
        stripStart = (minPosition/BOARDS_PER_SUPERLAYER)*STRIPS_PER_SUPERLAYER+(minPosition%BOARDS_PER_SUPERLAYER)*STRIPS_PER_GROUP;
        stripEnd = (maxPosition/BOARDS_PER_SUPERLAYER)*STRIPS_PER_SUPERLAYER +(minPosition%BOARDS_PER_SUPERLAYER)*STRIPS_PER_GROUP+(59/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+59%CHANNELS_PER_GROUP;
    }
    char tmp[256];

    sprintf(tmp," Analysis Result In Strip with Light injection");
    TCanvas *c1 = new TCanvas(tmp,tmp,1280,1280);
    if(m_readInSetupFile){
        if(m_corrUplinkPairs.size()) c1->Divide(1,3);
        else c1->Divide(1,2);
    }else{
        if(m_uplink_correlation.size()) c1->Divide(1,3);
        else c1->Divide(1,2);
    }

    gStyle->SetOptFit(1000);
    gStyle->SetLabelFont(132,"XY");
    gStyle->SetLabelSize(0.06,"XY");

    gStyle->SetTitleFont(132,"XY");
    gStyle->SetTitleSize(0.06,"XY");
    gStyle->SetTitleYOffset(0.8);
    gStyle->SetTitleXOffset(0.8);



    TGraphErrors* graph = NULL;
    TH1F *hr = NULL;
    TLegend *legend =NULL;
    TGraphErrors* graph_omitDamagedChannels = NULL;


    //draw ADC per Photon
    std::cout<<"\n\n\ndraw adcPerPhoton \n";
    c1->cd(1);

    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);


    FILE* resultLog = NULL;
    //open a file to log the resultz
    sprintf(tmp,"../data/GainList_LED_inStrip.txt");
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----Gain List with LED injection ( in strip ) --------\n\n");
    fprintf(resultLog,"strip\t uplink\t channel\t Gain (ADC/photon)\t\n");

    int stripBase = 1;
    for(unsigned int i=0; i<m_adcPerPhoton.size();i++){
         if(i==0) stripBase = 1;
         if((i%2 == 0)&& (i!=0)){
             stripBase +=NUMBER_OF_EFFECT_CHANNELS;
         }

         graph = m_adcPerPhoton.at(i);
         graph_omitDamagedChannels = new TGraphErrors(1);

         double chan=0;
         double getAdcPerPhoton =0;
         int index_new = 0;
         int stripIndex = 0;

         int SuperLayerIndex = 0;
         int positionIndex = 0;

         if(m_readInSetupFile) {
             for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                 for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                      if(superLayer_iter->second==m_uplinkId.at(i)) {
                          SuperLayerIndex = LayerIndex;
                          positionIndex = superLayer_iter->first;
                          break;
                      }
                 }
             }
         }

         for(int index = 0; index<NUMBER_OF_EFFECT_CHANNELS; index++){
             graph->GetPoint(index,chan,getAdcPerPhoton);
             std::cout<<"index= "<<index<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
             if(getAdcPerPhoton<ADC_PER_PHOTON_UPPER_THRESHOLD) {
                 if(m_readInSetupFile) {
                      if(positionIndex<P_N_DISTANCE){
                            stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+(int)chan%CHANNELS_PER_GROUP;
                      }else{
                            stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+STRIPS_PER_GROUP-((int)chan%CHANNELS_PER_GROUP)-1;
                      }
                 }else{
                     //if(i%2 == 0){
                     if((i%2 == 0) || i==1){   //the first two uplink are trigger layers,not corresponded
                         stripIndex = chan-HEAD_OF_EFFECT_CHANNELS+stripBase ;
                     }else{
                         stripIndex = NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS-1-chan+stripBase;
                     }
                 }

                 graph_omitDamagedChannels->SetPoint(index_new,stripIndex,getAdcPerPhoton);
                 std::cout<<"index_new= "<<index_new<<",strip = "<<stripIndex<<",uplinkId = "<<m_uplinkId.at(i)<<"chan"<<chan<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
                 fprintf(resultLog,"%d\t %d\t %.0f\t %f\t\n",stripIndex,m_uplinkId.at(i),chan,getAdcPerPhoton);
                 index_new++;
             }
          }
          sprintf(tmp,"uplink%d",m_uplinkId.at(i));
          legend->AddEntry(graph_omitDamagedChannels,tmp, "p");

          if(i==0){
              if(m_readInSetupFile){
                  hr = c1->cd(1)->DrawFrame(stripStart,8,stripEnd,20);
              }else{
                  //if(m_uplinkId.size()%2 == 0) hr = c1->cd(1)->DrawFrame(0,8,(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS,15);
                  if(m_uplinkId.size()%2 == 0) hr = c1->cd(1)->DrawFrame(0,8,(m_uplinkId.size()/2+1)*NUMBER_OF_EFFECT_CHANNELS,20); //the first two uplink are trigger layers,not corresponded
                  else hr = c1->cd(1)->DrawFrame(0,8,(m_uplinkId.size()/2 +1)*NUMBER_OF_EFFECT_CHANNELS,15);
              }
             hr->SetXTitle("strip #");
             hr->SetYTitle("gain(ADC per Photon), ADC");
          }

          graph_omitDamagedChannels->SetMarkerStyle(20+i);
          //graph->SetMarkerColor(kRed+i);
          graph_omitDamagedChannels->SetMarkerColor(2+2*i);
          graph_omitDamagedChannels->SetMarkerSize(1.);

          graph_omitDamagedChannels->Fit("pol1");
          graph_omitDamagedChannels->GetFunction("pol1")->SetLineColor(2+2*i);
          if(i==0) graph_omitDamagedChannels->Draw("PE");
          else graph_omitDamagedChannels->Draw("PE SAME");

          if(i==0){
               c1->cd(1)->SetGridx();
               c1->cd(1)->SetGridy();
           }

          legend->Draw();

    }
    fclose(resultLog);





    //draw meanInPhoton
    std::cout<<"\n\n\ndraw meaninPhoton \n";
    c1->cd(2);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);


    resultLog = NULL;
    //open a file to log the result
    sprintf(tmp,"../data/MeanInPhoton_LED_inStrip.txt");
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----MeanInPhoton List with LED injection ( in strip ) --------\n\n");
    fprintf(resultLog,"strip\t uplink\t channel\t MeanInPhoton\t\n");

    stripBase = 1;

    for(unsigned int i=0; i<m_meanInPhoton.size();i++){

        if(i==0) stripBase = 1;

        if((i%2 == 0)&& (i!=0)){
            stripBase +=NUMBER_OF_EFFECT_CHANNELS;
         }

         graph = m_meanInPhoton.at(i);
         graph_omitDamagedChannels = new TGraphErrors(1);

         double chan=0;
         double getMeanInPhoton =0;
         int index_new = 0;
         int stripIndex = 0;

         int SuperLayerIndex = 0;
         int positionIndex = 0;
         if(m_readInSetupFile) {
             for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                 for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                      if(superLayer_iter->second==m_uplinkId.at(i)) {
                          SuperLayerIndex = LayerIndex;
                          positionIndex = superLayer_iter->first;
                          break;
                      }
                 }
             }
         }

         for(int index = 0; index<NUMBER_OF_EFFECT_CHANNELS; index++){
             graph->GetPoint(index,chan,getMeanInPhoton);
             std::cout<<"index= "<<index<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",meanInPhoton = "<<getMeanInPhoton<<std::endl;
             if(getMeanInPhoton>MEAN_IN_PHOTON_UNDER_THRESHOLD) {
                 if(m_readInSetupFile) {
                      if(positionIndex<P_N_DISTANCE){
                            stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+(int)chan%CHANNELS_PER_GROUP;
                      }else{
                            stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+STRIPS_PER_GROUP-((int)chan%CHANNELS_PER_GROUP)-1;
                      }
                 }else{
                      if(i%2 == 0){
                            stripIndex = chan-HEAD_OF_EFFECT_CHANNELS+stripBase ;
                      }else{
                            stripIndex = NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS-1-chan+stripBase;
                      }
                 }
                 graph_omitDamagedChannels->SetPoint(index_new,stripIndex,getMeanInPhoton);
                 std::cout<<"index_new= "<<index_new<<",strip = "<<stripIndex<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<chan<<",meanInPhoton = "<<getMeanInPhoton<<std::endl;
                 fprintf(resultLog,"%d\t %d\t %.0f\t %f\t\n",stripIndex,m_uplinkId.at(i),chan,getMeanInPhoton);
                 index_new++;

             }
          }

         sprintf(tmp,"uplink%d",m_uplinkId.at(i));
         legend->AddEntry(graph_omitDamagedChannels,tmp, "p");

         if(i==0){
             if(m_readInSetupFile){
                  hr = c1->cd(2)->DrawFrame(stripStart,0,stripEnd,16);
             }else{
                 if(m_uplinkId.size()%2 == 0) hr = c1->cd(2)->DrawFrame(0,0,(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS,16);
                 else hr = c1->cd(2)->DrawFrame(0,0,(m_uplinkId.size()/2 +1)*NUMBER_OF_EFFECT_CHANNELS,16);
             }

            hr->SetXTitle("strip #");
            hr->SetYTitle("mean,p.e.");
         }

         graph_omitDamagedChannels->SetMarkerStyle(20+i);
        // graph->SetMarkerColor(kRed+i);
         graph_omitDamagedChannels->SetMarkerColor(2+2*i);
         graph_omitDamagedChannels->SetMarkerSize(1.);

         graph_omitDamagedChannels->Fit("pol1");
         //graph->GetFunction("pol1")->SetLineColor(kRed+i);
         graph_omitDamagedChannels->GetFunction("pol1")->SetLineColor(2+2*i);

         if(i==0) graph_omitDamagedChannels->Draw("PE");
         else graph_omitDamagedChannels->Draw("PE SAME");

         if(i==0){
             c1->cd(2)->SetGridx();
             c1->cd(2)->SetGridy();
         }
    }

    legend->Draw();
    fclose(resultLog);

    //draw ratio of number of photons between left and right side of the strip
    std::cout<<"\n\n\ndraw ratio L/R meanInPhoton \n";
    bool hasCorrelation = false;
    if(m_readInSetupFile){
        if(m_corrUplinkPairs.size()) hasCorrelation = true;
    }else{
        if(m_uplink_correlation.size()) hasCorrelation = true;
    }
    if(hasCorrelation){
         FILE* resultLog = NULL;
         c1->cd(3);
         legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
         legend->SetFillColor(kWhite);
         legend->SetBorderSize(0);
         legend->SetTextFont(132);
         legend->SetTextSize(0.06);

         if(m_readInSetupFile){
               hr = c1->cd(3)->DrawFrame(stripStart,0.3,stripEnd,2.5);
         }else{
               if(m_uplinkId.size()%2 == 0) hr = c1->cd(3)->DrawFrame(0,0.3,(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS,2.5);
               else hr = c1->cd(3)->DrawFrame(0,0.3,(m_uplinkId.size()/2 +1)*NUMBER_OF_EFFECT_CHANNELS,2.5);
         }
         sprintf(tmp,"strip # ");
         hr->SetXTitle(tmp);
         sprintf(tmp,"mean ratio, p.e.");
         hr->SetYTitle(tmp);


         Double_t corrChan = 0;
         Double_t corrMeanInPhoton = 0;
         Double_t chan_aim = 0;
         Double_t meanInPhoton_aim = 0;
         Double_t ratioLR = 0;

         int corrSum = 0;
         if(m_readInSetupFile){
              corrSum = m_corrUplinkPairs.size()/2;
         }else{
              corrSum = m_uplink_correlation.size()/NUMBER_OF_EFFECT_CHANNELS;
         }


         int uplinkID_P = 0;
         int uplinkID_N = 0;
         int uplinkIndex_P = 0;
         int uplinkIndex_N = 0;
         for(int corrIndex=0; corrIndex< corrSum ; corrIndex++){

              if(corrIndex==0) stripBase =  1;
              else stripBase += NUMBER_OF_EFFECT_CHANNELS;

              if(m_readInSetupFile){
                  uplinkID_P = m_corrUplinkPairs.at(2*corrIndex);
                  uplinkID_N = m_corrUplinkPairs.at(2*corrIndex+1);
                  uplinkIndex_P = uplinkIndex(uplinkID_P);
                  uplinkIndex_N = uplinkIndex(uplinkID_N);
              }else{
                  uplinkID_P = m_uplinkId.at(2*corrIndex);
                  uplinkID_N = m_uplinkId.at(2*corrIndex+1);
              }


              //open a file to log the result
              sprintf(tmp,"../data/ratioLR_U%d_U%d_inStrip.txt",uplinkID_P,uplinkID_N);
              resultLog = fopen(tmp,"w");
              fprintf(resultLog,"-----ratioLR U%d/U%d in Strip--------\n\n",uplinkID_P,uplinkID_N);
              fprintf(resultLog,"strip\t uplink_side1\t channel\t uplink_side2\t channel\t ratioLR\n");

              graph = m_meanInPhoton.at(uplinkIndex_P);
              TGraphErrors* graph2 = m_meanInPhoton.at(uplinkIndex_N);


              int SuperLayerIndex = 0;
              int positionIndex = 0;
              if(m_readInSetupFile) {
                  for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                      for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                           if(superLayer_iter->second==uplinkID_P) {
                               if(superLayer_iter->first >(P_N_DISTANCE-1)){
                                   std::cerr<<"the positionIndex of uplinkID_P = "<<uplinkID_P<<" is larger than 4!"<<std::endl;
                                   assert(false);
                               }
                               SuperLayerIndex = LayerIndex;
                               positionIndex = superLayer_iter->first;
                               SuperLayerMap::iterator corrLayer = m_superLayerMap[LayerIndex].find(superLayer_iter->first+P_N_DISTANCE);
                               if(corrLayer==m_superLayerMap[LayerIndex].end() || corrLayer->second != uplinkID_N ){
                                   std::cerr<<"the corresponding uplinkID for uplinkID_P = "<<uplinkID_P<<" is not uplinkID_N= "<<uplinkID_N <<" ,but uplinkID= "<<corrLayer->second <<".Check the setup file!"<<std::endl;
                                   assert(false);
                               }
                               break;
                           }
                      }
                  }
              }

              TGraphErrors* ratio =new TGraphErrors(1);  // since chan14 is dead on both cards until 20_1_2012

              Int_t ratio_index =0;
              Double_t corrNum = 2*HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1;
              int stripIndex = 0;
              for(Int_t j=0; j< NUMBER_OF_EFFECT_CHANNELS;j++){
                     graph->GetPoint(j,chan_aim,meanInPhoton_aim);   //
                     std::cout<<"chan_aim = "<<chan_aim<<" on uplinkId = "<<uplinkID_P<<",meanInPhoton_aim = "<<meanInPhoton_aim<<std::endl;
                     if(meanInPhoton_aim>MEAN_IN_PHOTON_UNDER_THRESHOLD ){        //change threshold
                           for(Int_t i=0; i< NUMBER_OF_EFFECT_CHANNELS;i++){
                                 graph2->GetPoint(i,corrChan,corrMeanInPhoton);
                                 //std::cout<<"corrChan = "<<corrChan<<" on uplinkId = "<<uplinkID_N<<",corrMeanInPhoton = "<<corrMeanInPhoton<<std::endl;
                                 //getchar();
                                 if(m_readInSetupFile){
                                      if((int)corrChan/CHANNELS_PER_GROUP == (int)chan_aim/CHANNELS_PER_GROUP){   //if corrChan and  chan_aim belong to the same group
                                           if((int)corrChan%CHANNELS_PER_GROUP == STRIPS_PER_GROUP-1-(int)chan_aim%CHANNELS_PER_GROUP){   //if the corrChan and chan_aim are corresponded
                                               if(positionIndex<P_N_DISTANCE){
                                                     stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan_aim/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+(int)chan_aim%CHANNELS_PER_GROUP;
                                               }else{
                                                     stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan_aim/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+STRIPS_PER_GROUP-((int)chan_aim%CHANNELS_PER_GROUP)-1;
                                               }
                                               std::cout<<"find corrChan = "<<corrChan <<" for chan_aim = " <<chan_aim <<std::endl;
                                               ratioLR = meanInPhoton_aim/corrMeanInPhoton;   //ratio = m_meanInPhoton.at(0)/m_meaninPhoton.at(1)
                                               ratio->SetPoint(ratio_index,stripIndex,ratioLR);
                                               std::cout<<"strip = "<<stripIndex<<" ratio: point_index = "<<ratio_index<<",chan_aim = "<<chan_aim<<" on uplinkId = "<<uplinkID_P<<",ratioLR = "<<ratioLR<<std::endl;
                                               // getchar();
                                               fprintf(resultLog,"%d\t %d\t %2.0f\t %d\t %2.0f\t %f\t\n",stripIndex,uplinkID_P,chan_aim,uplinkID_N,corrChan,ratioLR);
                                               ratio_index++;
                                               break;
                                           }
                                      }
                                 }else{
                                      if((corrChan == (corrNum-chan_aim))&&(corrMeanInPhoton>MEAN_IN_PHOTON_UNDER_THRESHOLD)){  //change threshold
                                           stripIndex= chan_aim-HEAD_OF_EFFECT_CHANNELS+stripBase;
                                           ratioLR = meanInPhoton_aim/corrMeanInPhoton;   //ratio = m_meanInPhoton.at(0)/m_meaninPhoton.at(1)
                                           ratio->SetPoint(ratio_index,stripIndex,ratioLR);
                                           std::cout<<"strip = "<<stripIndex<<" ratio: point_index = "<<ratio_index<<",chan_aim = "<<chan_aim<<" on uplinkId = "<<uplinkID_P<<",ratioLR = "<<ratioLR<<std::endl;
                                           // getchar();
                                           fprintf(resultLog,"%d\t %d\t %2.0f\t %d\t %2.0f\t %f\t\n",stripIndex,uplinkID_P,chan_aim,uplinkID_N,corrChan,ratioLR);
                                           ratio_index++;
                                           break;
                                      }
                                 }
                          }
                     }
              }

              fclose(resultLog);


              sprintf(tmp,"U%d/U%d",uplinkID_P,uplinkID_N);
              legend->AddEntry(ratio,tmp, "p");

              ratio->SetMarkerStyle(20+corrIndex);
              ratio->SetMarkerColor(2+2*corrIndex);
              ratio->SetMarkerSize(1.);


              ratio->Fit("pol1");
              ratio->GetFunction("pol1")->SetLineColor(2+2*corrIndex);

              if(corrIndex==0) ratio->Draw("PE");
              else ratio->Draw("PE SAME");


          }

         c1->cd(3)->SetGridx();
         c1->cd(3)->SetGridy();

         legend->Draw();
    }

}


void DataManager::PlotAnalysisResult_InStrip(std::string pedestalFilePath, std::string signalFilePath){

    // if read in setup file, define the strip range to be showed
    int stripStart = 0;
    int stripEnd = 0;
    SuperLayerMap::iterator superLayer_iter;
    if(m_readInSetupFile){
        int minPosition = 16; // since for two superLayer, position can only be 0~15 ,change here if there are more superLayers
        int maxPosition = -1;
        int currPosition = 0;
        for(int uplinkIndex =0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
            int currUplinkId = m_uplinkId.at(uplinkIndex);
            for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                     if(superLayer_iter->second== currUplinkId) {
                         currPosition = superLayer_iter->first%P_N_DISTANCE + BOARDS_PER_SUPERLAYER*LayerIndex;
                         if(currPosition>maxPosition) maxPosition = currPosition;
                         if(currPosition<minPosition) minPosition = currPosition;
                         break;
                     }
                }
            }
        }
        stripStart = (minPosition/BOARDS_PER_SUPERLAYER)*STRIPS_PER_SUPERLAYER+(minPosition%BOARDS_PER_SUPERLAYER)*STRIPS_PER_GROUP;
        stripEnd = (maxPosition/BOARDS_PER_SUPERLAYER)*STRIPS_PER_SUPERLAYER +(minPosition%BOARDS_PER_SUPERLAYER)*STRIPS_PER_GROUP+(59/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+59%CHANNELS_PER_GROUP;
    }

    char tmp[256];

    sprintf(tmp," Analysis Result_inStrip\t%s\t%s",pedestalFilePath.c_str(),signalFilePath.c_str());
    TCanvas *c1 = new TCanvas(tmp,tmp,1280,1280);
   // c1->Divide(2,3);
    c1->Divide(1,4);

    gStyle->SetOptFit(1000);
    gStyle->SetLabelFont(132,"XY");
    gStyle->SetLabelSize(0.06,"XY");

    gStyle->SetTitleFont(132,"XY");
    gStyle->SetTitleSize(0.06,"XY");
    gStyle->SetTitleYOffset(0.8);
    gStyle->SetTitleXOffset(0.8);

    TGraphErrors* graph = NULL;
    TH1F *hr = NULL;
    TLegend *legend =NULL;



    //draw ADC per Photon
    c1->cd(1);

    FILE* resultLog = NULL;
    //open a file to log the result
    sprintf(tmp,"../data/GainList_cosmics_inStrip.txt");
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----Gain List with cosmics injection ( in strip ) --------\n\n");
    fprintf(resultLog,"strip\t uplink\t channel\t Gain (ADC/photon)\t\n");

    int stripBase = 1;

    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    TGraphErrors* graphInStrip =NULL;


    for(unsigned int i=0; i<m_adcPerPhoton.size();i++){

         if(i==0) stripBase = 1;

        // if((i%2 == 0)&& (i!=0)){
         if(((i%2 == 0)&& (i!=0))||i==1){  //the first two uplink are trigger layers,not corresponded
            stripBase +=NUMBER_OF_EFFECT_CHANNELS;
         }

         graph = m_adcPerPhoton.at(i);
         graphInStrip = new TGraphErrors(1);
         double chan=0;
         double getAdcPerPhoton =0;
         int index_new = 0;
         int stripIndex = 0;

         int SuperLayerIndex = 0;
         int positionIndex = 0;

         if(m_readInSetupFile) {
             for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                 for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                      if(superLayer_iter->second==m_uplinkId.at(i)) {
                          SuperLayerIndex = LayerIndex;
                          positionIndex = superLayer_iter->first;
                          break;
                      }
                 }
             }
         }

         for(int index = 0; index<graph->GetN(); index++){
             graph->GetPoint(index,chan,getAdcPerPhoton);
             if(((int)chan%CHANNELS_PER_GROUP) >STRIPS_PER_GROUP) continue; // only chan0~11, 16~27,32~43,48~59 are SiPM output
             std::cout<<"index= "<<index<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
             if(getAdcPerPhoton<ADC_PER_PHOTON_UPPER_THRESHOLD) {
                 //if setupFile exists, generate the unique stripIndex for this channel
                 if(m_readInSetupFile) {
                      if(positionIndex<P_N_DISTANCE){
                            stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+(int)chan%CHANNELS_PER_GROUP;
                      }else{
                            stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+STRIPS_PER_GROUP-((int)chan%CHANNELS_PER_GROUP)-1;
                      }
                 }else{
                     //if(i%2 == 0){
                     if((i%2 == 0) || i==1){   //the first two uplink are trigger layers,not corresponded
                         stripIndex = chan-HEAD_OF_EFFECT_CHANNELS+stripBase ;
                     }else{
                         stripIndex = NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS-1-chan+stripBase;
                     }
                 }
                 graphInStrip->SetPoint(index_new,stripIndex,getAdcPerPhoton);
                 std::cout<<"index_new= "<<index_new<<",strip = "<<stripIndex<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
                 fprintf(resultLog,"%d\t %d\t %.0f\t %f\t\n",stripIndex,m_uplinkId.at(i),chan,getAdcPerPhoton);
                 index_new++;
             }
          }


         sprintf(tmp,"uplink%d",m_uplinkId.at(i));
         legend->AddEntry(graphInStrip,tmp, "p");
         if(i==0){
             if(m_readInSetupFile){
                 hr = c1->cd(1)->DrawFrame(stripStart,10,stripEnd,14);
             }else{
                 //if(m_uplinkId.size()%2 == 0) hr = c1->cd(1)->DrawFrame(0,8,(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS,15);
                 if(m_uplinkId.size()%2 == 0) hr = c1->cd(1)->DrawFrame(0,8,(m_uplinkId.size()/2+1)*NUMBER_OF_EFFECT_CHANNELS,20); //the first two uplink are trigger layers,not corresponded
                 else hr = c1->cd(1)->DrawFrame(0,8,(m_uplinkId.size()/2 +1)*NUMBER_OF_EFFECT_CHANNELS,15);
             }

             hr->SetXTitle("strip #");
             hr->SetYTitle("gain(ADC per Photon), ADC");
         }

         graphInStrip->SetMarkerStyle(20+i);
         graphInStrip->SetMarkerColor(2+2*i);
         graphInStrip->SetMarkerSize(1.);

         graphInStrip->Fit("pol1");
         graphInStrip->GetFunction("pol1")->SetLineColor(2+2*i);
         if(i==0) graphInStrip->Draw("PE");
         else graphInStrip->Draw("PE SAME");

         if(i==0){
             c1->cd(1)->SetGridx();
             c1->cd(1)->SetGridy();
         }
    }
    legend->Draw();
    fclose(resultLog);



    //draw MPV in ADC
    c1->cd(2);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    resultLog = NULL;
    //open a file to log the result
    sprintf(tmp,"../data/MIP_inADC_cosmics_inStrip.txt");
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----MIP in ADC List with cosmics( in strip ) --------\n\n");
    fprintf(resultLog,"strip\t uplink\t channel\t MPV (in adc)\t\n");

    graphInStrip =NULL;
    double getMpvInAdc = 0;
    double chan = 0;
    int stripIndex = 0;

    for(unsigned int i=0; i<m_mpvInAdc.size();i++){

         if(i==0) stripBase = 1;

         //if((i%2 == 0)&& (i!=0)){
         if(((i%2 == 0)&& (i!=0))||i==1){  //the first two uplink are trigger layers,not corresponded
            stripBase +=NUMBER_OF_EFFECT_CHANNELS;
         }

         graph = m_mpvInAdc.at(i);
         graphInStrip = new TGraphErrors(1);


         int SuperLayerIndex = 0;
         int positionIndex = 0;
         if(m_readInSetupFile) {
             for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                 for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                      if(superLayer_iter->second==m_uplinkId.at(i)) {
                          SuperLayerIndex = LayerIndex;
                          positionIndex = superLayer_iter->first;
                          break;
                      }
                 }
             }
         }


         for(int index = 0; index<graph->GetN(); index++){
             graph->GetPoint(index,chan,getMpvInAdc);
             //if setupFile exists, generate the unique stripIndex for this channel
             if(m_readInSetupFile) {
                  if(positionIndex<P_N_DISTANCE){
                        stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+(int)chan%CHANNELS_PER_GROUP;
                  }else{
                        stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+STRIPS_PER_GROUP-((int)chan%CHANNELS_PER_GROUP)-1;
                  }
             }else{
                 //if(i%2 == 0){
                 if((i%2 == 0) || i==1){   //the first two uplink are trigger layers,not corresponded
                     stripIndex = chan-HEAD_OF_EFFECT_CHANNELS+stripBase ;
                 }else{
                     stripIndex = NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS-1-chan+stripBase;
                 }

             }
             graphInStrip->SetPoint(index,stripIndex,getMpvInAdc);
             std::cout<<"index= "<<index<<",strip = "<<stripIndex<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",mpvInAdc = "<<getMpvInAdc<<std::endl;
             fprintf(resultLog,"%d\t %d\t %.0f\t %f\t\n",stripIndex,m_uplinkId.at(i),chan,getMpvInAdc);

         }

         sprintf(tmp,"uplink%d",m_uplinkId.at(i));
         legend->AddEntry(graphInStrip,tmp, "p");

         if(i==0){
             if(m_readInSetupFile){
                  hr = c1->cd(2)->DrawFrame(stripStart,60,stripEnd,120);
             }else{
                  //if(m_uplinkId.size()%2 ==0) hr = c1->cd(2)->DrawFrame(0,40,(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS,160);
                  if(m_uplinkId.size()%2 ==0) hr = c1->cd(2)->DrawFrame(0,40,(m_uplinkId.size()/2+1)*NUMBER_OF_EFFECT_CHANNELS,160);//the first two uplink are trigger layers,not corresponded
                  else hr = c1->cd(2)->DrawFrame(0,40,(m_uplinkId.size()/2+1)*NUMBER_OF_EFFECT_CHANNELS,160);
             }
             hr->SetXTitle("strip #");
             hr->SetYTitle("MPV, ADC");

         }

         graphInStrip->SetMarkerStyle(20+i);
        // graph->SetMarkerColor(kRed+i);
         graphInStrip->SetMarkerColor(2+2*i);
         graphInStrip->SetMarkerSize(1.);

         graphInStrip->Fit("pol1");
         graphInStrip->GetFunction("pol1")->SetLineColor(2+2*i);

         if(i==0) graphInStrip->Draw("PE");
         else graphInStrip->Draw("PE SAME");

         if(i==0){
             c1->cd(2)->SetGridx();
             c1->cd(2)->SetGridy();
         }
    }
    legend->Draw();
    fclose(resultLog);

    //draw resultCorr : MPV in photon electron
    c1->cd(3);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC");  //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    resultLog = NULL;
    //open a file to log the result
    sprintf(tmp,"../data/MIP_inPE_cosmics_inStrip.txt");
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----MIP in p.e. List with cosmics ( in strip ) --------\n\n");
    fprintf(resultLog,"strip\t uplink\t channel\t MPV in p.e.\t\n");

    graphInStrip =NULL;
    double getMpvInPE = 0;
    chan = 0;
    stripIndex = 0;

    for(unsigned int i=0; i<m_resultCorr.size();i++){

        if(i==0) stripBase = 1;
        //if((i%2 == 0)&& (i!=0)){
        if(((i%2 == 0)&& (i!=0))||i==1){  //the first two uplink are trigger layers,not corresponded
           stripBase +=NUMBER_OF_EFFECT_CHANNELS;
        }

         graph = m_resultCorr.at(i);
         graphInStrip = new TGraphErrors(1);

         int SuperLayerIndex = 0;
         int positionIndex = 0;
         if(m_readInSetupFile) {
             for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                 for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                      if(superLayer_iter->second==m_uplinkId.at(i)) {
                          SuperLayerIndex = LayerIndex;
                          positionIndex = superLayer_iter->first;
                          break;
                      }
                 }
             }
         }

         for(int index = 0; index<graph->GetN(); index++){
             graph->GetPoint(index,chan,getMpvInPE);

             //if setupFile exists, generate the unique stripIndex for this channel
             if(m_readInSetupFile) {
                  if(positionIndex<P_N_DISTANCE){
                        stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+(int)chan%CHANNELS_PER_GROUP;
                  }else{
                        stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)chan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+STRIPS_PER_GROUP-((int)chan%CHANNELS_PER_GROUP)-1;
                  }
             }else{
                  //if(i%2 == 0){
                  if((i%2 == 0) || i==1){   //the first two uplink are trigger layers,not corresponded
                     stripIndex = chan-HEAD_OF_EFFECT_CHANNELS+stripBase ;
                  }else{
                     stripIndex = NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS-1-chan+stripBase;
                  }
             }

             graphInStrip->SetPoint(index,stripIndex,getMpvInPE);
             std::cout<<"index= "<<index<<",strip = "<<stripIndex<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",mpvInPE = "<<getMpvInPE<<std::endl;
             fprintf(resultLog,"%d\t %d\t %.0f\t %f\t\n",stripIndex,m_uplinkId.at(i),chan,getMpvInPE);
         }

         sprintf(tmp,"uplink%d",m_uplinkId.at(i));
         legend->AddEntry(graphInStrip,tmp, "p");

         if(i==0){
             if(m_readInSetupFile){
                 hr = c1->cd(3)->DrawFrame(stripStart,6,stripEnd,8.5);
             }else{
                 //if(m_uplinkId.size()%2 ==0) hr = c1->cd(3)->DrawFrame(0,0,(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS,15);
                 if(m_uplinkId.size()%2 ==0) hr = c1->cd(3)->DrawFrame(0,0,(m_uplinkId.size()/2+1)*NUMBER_OF_EFFECT_CHANNELS,15);//the first two uplink are trigger layers,not corresponded
                 else hr = c1->cd(3)->DrawFrame(0,0,(m_uplinkId.size()/2+1)*NUMBER_OF_EFFECT_CHANNELS,15);
             }
             hr->SetXTitle("strip #");
             hr->SetYTitle("MPV, p.e.");
         }
         graphInStrip->SetMarkerStyle(20+i);
         graphInStrip->SetMarkerColor(2+2*i);
         graphInStrip->SetMarkerSize(1.);

         graphInStrip->Fit("pol1");
         graphInStrip->GetFunction("pol1")->SetLineColor(2+2*i);
         if(i==0) graphInStrip->Draw("PE");
         else graphInStrip->Draw("PE SAME");

         if(i==0){
             c1->cd(3)->SetGridx();
             c1->cd(3)->SetGridy();
         }
    }
    legend->Draw();
    fclose(resultLog);

    //draw area:
    c1->cd(4);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC");  //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    resultLog = NULL;
    //open a file to log the result
    sprintf(tmp,"../data/area_cosmics_inStrip.txt");
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----area List with cosmics ( in strip ) --------\n\n");
    fprintf(resultLog,"strip\t uplink\t channel\t area_for_meanOfMIPDistribution\t\n");

    graphInStrip =NULL;
    double getArea = 0;
    stripIndex = 0;

    for(unsigned int i=0; i<m_area.size();i++){

         if(i==0) stripBase = 1;
         //if((i%2 == 0)&& (i!=0)){
         if(((i%2 == 0)&& (i!=0))||i==1){  //the first two uplink are trigger layers,not corresponded
           stripBase +=NUMBER_OF_EFFECT_CHANNELS;
         }

         hr = m_area.at(i);
         //TH1F* hrInStrip = new TH1F(tmp,"area_for_meanOfMIPDistribution;Strip;Area",NUMBER_OF_EFFECT_CHANNELS,stripBase,stripBase+NUMBER_OF_EFFECT_CHANNELS);
         TH1F* hrInStrip =NULL;
         if(m_readInSetupFile){
             hrInStrip = new TH1F(tmp,"area_for_meanOfMIPDistribution;Strip;Area",stripEnd-stripStart,stripStart,stripEnd);
         }else{
             //if(m_uplinkId.size()%2 ==0) hrInStrip = new TH1F(tmp,"area_for_meanOfMIPDistribution;Strip;Area",(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS,1,(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS+1);
             if(m_uplinkId.size()%2 ==0) hrInStrip = new TH1F(tmp,"area_for_meanOfMIPDistribution;Strip;Area",(m_uplinkId.size()/2+1)*NUMBER_OF_EFFECT_CHANNELS,1,(m_uplinkId.size()/2+1)*NUMBER_OF_EFFECT_CHANNELS+1);//the first two uplink are trigger layers,not corresponded
             else hrInStrip = new TH1F(tmp,"area_for_meanOfMIPDistribution;Strip;Area",(m_uplinkId.size()/2 +1)*NUMBER_OF_EFFECT_CHANNELS,1,(m_uplinkId.size()/2 +1)*NUMBER_OF_EFFECT_CHANNELS+1);
         }

         int SuperLayerIndex = 0;
         int positionIndex = 0;
         if(m_readInSetupFile) {
             for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                 for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                      if(superLayer_iter->second==m_uplinkId.at(i)) {
                          SuperLayerIndex = LayerIndex;
                          positionIndex = superLayer_iter->first;
                          break;
                      }
                 }
             }
         }

         for(int readInChan = HEAD_OF_EFFECT_CHANNELS; readInChan<(HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS); readInChan++){
             getArea = hr->GetBinContent(readInChan-HEAD_OF_EFFECT_CHANNELS+1);

             if(m_readInSetupFile){
                  if(positionIndex<P_N_DISTANCE){
                       stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)readInChan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+(int)readInChan%CHANNELS_PER_GROUP;
                  }else{
                       stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+((int)readInChan/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+STRIPS_PER_GROUP-((int)readInChan%CHANNELS_PER_GROUP)-1;
                  }
             }else{
                  //if(i%2 == 0){
                  if((i%2 == 0) || i==1){   //the first two uplink are trigger layers,not corresponded
                        stripIndex = readInChan-HEAD_OF_EFFECT_CHANNELS+stripBase ;
                  }else{
                        stripIndex = NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS-1-readInChan+stripBase;
                  }
             }
             hrInStrip->Fill(stripIndex,getArea);
             std::cout<<"strip = "<<stripIndex<<",chan = "<<readInChan<<",uplinkId = "<<m_uplinkId.at(i)<<",area = "<<getArea<<std::endl;
             fprintf(resultLog,"%d\t %d\t %.0f\t %f\t\n",stripIndex,m_uplinkId.at(i),chan,getArea);
         }


         sprintf(tmp,"uplink%d",m_uplinkId.at(i));
         legend->AddEntry(hrInStrip,tmp, "L");

         //hr->SetLineColor(kRed+i);
         hrInStrip->SetLineColor(2+2*i);
         hrInStrip->SetLineWidth(3);
         if(i==0) hrInStrip->Draw();
         else hrInStrip->Draw("SAME");

         if(i==0){
             c1->cd(4)->SetGridx();
             c1->cd(4)->SetGridy();
         }
    }
    legend->Draw();
    fclose(resultLog);
/*
    //draw Lsigma in photon electron
    c1->cd(5);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    resultLog = NULL;
    //open a file to log the result
    sprintf(tmp,"../data/lSigma_cosmics_inStrip.txt");
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----lSigma List with cosmics ( in strip ) --------\n\n");
    fprintf(resultLog,"strip\t uplink\t channel\t lSigma\t\n");

    graphInStrip =NULL;
    double getLsigma = 0;
    chan = 0;
    stripIndex = 0;

    for(int i=0; i<m_lSigma.size();i++){

         if(i==0) stripBase = 1;
         if((i%2 == 0)&& (i!=0)){
           stripBase +=NUMBER_OF_EFFECT_CHANNELS;
         }

         graph = m_lSigma.at(i);

         graphInStrip = new TGraphErrors(1);

         for(int index = 0; index<graph->GetN(); index++){
             graph->GetPoint(index,chan,getLsigma);
             if(i%2 == 0){
                 stripIndex = chan-HEAD_OF_EFFECT_CHANNELS+stripBase ;
             }else{
                 stripIndex = NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS-1-chan+stripBase;
             }
             graphInStrip->SetPoint(index,stripIndex,getLsigma);
             std::cout<<"index= "<<index<<",strip = "<<stripIndex<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",Lsigma = "<<getLsigma<<std::endl;
             fprintf(resultLog,"%d\t %d\t %.0f\t %f\t\n",stripIndex,m_uplinkId.at(i),chan,getLsigma);
         }


         sprintf(tmp,"uplink%d",m_uplinkId.at(i));
         legend->AddEntry(graphInStrip,tmp, "p");

         if(i==0){
            if(m_uplinkId.size()%2 ==0) hr = c1->cd(5)->DrawFrame(0,0,(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS,2.5);
            else hr = c1->cd(5)->DrawFrame(0,0,(m_uplinkId.size()/2 +1)*NUMBER_OF_EFFECT_CHANNELS,2.5);
            hr->SetXTitle("strip #");
            hr->SetYTitle("Landau width, p.e.");
         }
         graphInStrip->SetMarkerStyle(20+i);
         graphInStrip->SetMarkerColor(2+2*i);
         graphInStrip->SetMarkerSize(1.);

         graphInStrip->Fit("pol1");
         graphInStrip->GetFunction("pol1")->SetLineColor(2+2*i);
         if(i==0) graphInStrip->Draw("PE");
         else graphInStrip->Draw("PE SAME");

         if(i==0){
             c1->cd(5)->SetGridx();
             c1->cd(5)->SetGridy();
         }
    }
    legend->Draw();
    fclose(resultLog);

    //draw Gsigma in photon electron
    c1->cd(6);
    legend = new TLegend(0.75,0.94,0.95,0.75,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    resultLog = NULL;
    //open a file to log the result
    sprintf(tmp,"../data/gSigma_cosmics_inStrip.txt");
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----gSigma List with cosmics ( in strip ) --------\n\n");
    fprintf(resultLog,"strip\t uplink\t channel\t gSigma\t\n");

    graphInStrip =NULL;
    double getGsigma = 0;
    chan = 0;
    stripIndex = 0;

    for(int i=0; i<m_gSigma.size();i++){

         if(i==0) stripBase = 1;
         if((i%2 == 0)&& (i!=0)){
            stripBase +=NUMBER_OF_EFFECT_CHANNELS;
         }

         graph = m_gSigma.at(i);

         graphInStrip = new TGraphErrors(1);

         for(int index = 0; index<graph->GetN(); index++){
             graph->GetPoint(index,chan,getGsigma);
             if(i%2 == 0){
                 stripIndex = chan-HEAD_OF_EFFECT_CHANNELS+stripBase ;
             }else{
                 stripIndex = NUMBER_OF_EFFECT_CHANNELS+HEAD_OF_EFFECT_CHANNELS-1-chan+stripBase;
             }
             graphInStrip->SetPoint(index,stripIndex,getGsigma);
             std::cout<<"index= "<<index<<",strip = "<<stripIndex<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",Gsigma = "<<getGsigma<<std::endl;
             fprintf(resultLog,"%d\t %d\t %.0f\t %f\t\n",stripIndex,m_uplinkId.at(i),chan,getGsigma);
         }



         sprintf(tmp,"uplink%d",m_uplinkId.at(i));
         legend->AddEntry(graphInStrip,tmp, "p");

         if(i==0){
            if(m_uplinkId.size()%2 ==0) hr = c1->cd(6)->DrawFrame(0,0,(m_uplinkId.size()/2)*NUMBER_OF_EFFECT_CHANNELS,7);
            else hr = c1->cd(6)->DrawFrame(0,0,(m_uplinkId.size()/2 +1)*NUMBER_OF_EFFECT_CHANNELS,7);
            hr->SetXTitle("strip #");
            hr->SetYTitle("Gauss width, p.e.");
         }

         graphInStrip->SetMarkerStyle(20+i);
         graphInStrip->SetMarkerColor(2+2*i);
         graphInStrip->SetMarkerSize(1.);

         graphInStrip->Fit("pol1");
         graphInStrip->GetFunction("pol1")->SetLineColor(2+2*i);
         if(i==0) graphInStrip->Draw("PE");
         else graphInStrip->Draw("PE SAME");

         if(i==0){
             c1->cd(6)->SetGridx();
             c1->cd(6)->SetGridy();
         }
    }
    legend->Draw();

    fclose(resultLog);
    //c1->Update();
    */
}


void DataManager::Fill2LayerCorrelation(MergedEvent* event){
    //  m_uplinkId.at(0) ---->Trigger layer side 1
    //  m_uplinkId.at(1) ---->Trigger layer side 2

    //  m_uplinkId.at(2) ---->Signal layer side 1
    //  m_uplinkId.at(3) ---->Signal layer side 2


    if(m_uplinkId.size() == 4) {     //only when there are 4 uplinks, make this correlation

        unsigned short stripIndex_triggerLayer = 0;
        unsigned short stripIndex_signalLayer = 0;


         // find the channel which has the maximum ADC value (pedestal substracted) on trigger layer part 1
         const UplinkData& TriggerUplink1 = event->uplinkData(m_uplinkId.at(0));
         unsigned short max_ADC_triggerU1 = 0;
         unsigned short max_chan_triggerU1 = 0 ;

         for(int chanIndex = HEAD_OF_EFFECT_CHANNELS; chanIndex < (HEAD_OF_EFFECT_CHANNELS + NUMBER_OF_EFFECT_CHANNELS); chanIndex++){
	     unsigned short ADC_subPedestal = 0;
	     if(TriggerUplink1.adcValue(chanIndex)>(m_pedestal_Mean.at(0)[chanIndex]+COSMIC_THRESHOLD))
                      ADC_subPedestal = TriggerUplink1.adcValue(chanIndex)-m_pedestal_Mean.at(0)[chanIndex];
             //printf("uplinkId = %d, TriggerUplink1.adcValue(%d)=%d,m_pedestal_Mean.at(0)[%d]= %f \n",m_uplinkId.at(0),chanIndex,TriggerUplink1.adcValue(chanIndex),chanIndex,m_pedestal_Mean.at(0)[chanIndex]);

             if(ADC_subPedestal > max_ADC_triggerU1) {
                     max_ADC_triggerU1 = ADC_subPedestal;
                     max_chan_triggerU1 =  chanIndex;
             }
             //printf("max_ADC_triggerU1 = %d,max_chan_triggerU1 = %d\n",max_ADC_triggerU1,max_chan_triggerU1);

         }
         //printf("max_ADC_triggerU1 = %d,max_chan_triggerU1 = %d\n",max_ADC_triggerU1,max_chan_triggerU1);

         // find the channel which has the maximum ADC value (pedestal substracted) on trigger layer part 2
         const UplinkData& TriggerUplink2 = event->uplinkData(m_uplinkId.at(1));
         unsigned short max_ADC_triggerU2 = 0;
         unsigned short max_chan_triggerU2 = 0 ;

         for(int chanIndex = HEAD_OF_EFFECT_CHANNELS; chanIndex < (HEAD_OF_EFFECT_CHANNELS + NUMBER_OF_EFFECT_CHANNELS); chanIndex++){
	     unsigned short ADC_subPedestal = 0;
	     if(TriggerUplink2.adcValue(chanIndex)>(m_pedestal_Mean.at(1)[chanIndex]+COSMIC_THRESHOLD))
                     ADC_subPedestal = TriggerUplink2.adcValue(chanIndex)-m_pedestal_Mean.at(1)[chanIndex];

             //printf("uplinkId = %d, TriggerUplink2.adcValue(%d)=%d,m_pedestal_Mean.at(1)[%d]= %f \n",m_uplinkId.at(1),chanIndex,TriggerUplink2.adcValue(chanIndex),chanIndex,m_pedestal_Mean.at(1)[chanIndex]);

             if(ADC_subPedestal > max_ADC_triggerU2) {
                     max_ADC_triggerU2 = ADC_subPedestal;
                     max_chan_triggerU2 =  chanIndex;
             }
             //printf("max_ADC_triggerU2 = %d,max_chan_triggerU2 = %d\n",max_ADC_triggerU2,max_chan_triggerU2);
         }
         //printf("max_ADC_triggerU2 = %d,max_chan_triggerU2 = %d\n",max_ADC_triggerU2,max_chan_triggerU2);

        // if((max_ADC_triggerU1>COSMIC_THRESHOLD) || (max_ADC_triggerU2>COSMIC_THRESHOLD)){
              //printf("uplinkId = %d,max_ADC_triggerU1 = %d,max_chan_triggerU1 = %d\n",m_uplinkId.at(0),max_ADC_triggerU1,max_chan_triggerU1);
              //printf("uplinkId = %d,max_ADC_triggerU2 = %d,max_chan_triggerU2 = %d\n",m_uplinkId.at(1),max_ADC_triggerU2,max_chan_triggerU2);
              //getchar();
         if(((max_ADC_triggerU1>COSMIC_THRESHOLD) && (max_ADC_triggerU2<COSMIC_THRESHOLD)) ||((max_ADC_triggerU1<COSMIC_THRESHOLD) && (max_ADC_triggerU2>COSMIC_THRESHOLD))) {

               if(max_ADC_triggerU1 > max_ADC_triggerU2) {
                   stripIndex_triggerLayer =  max_chan_triggerU1 - HEAD_OF_EFFECT_CHANNELS +STRIP_BASE_TRIGGERLAYER_PART1;
               }else{
                   stripIndex_triggerLayer = max_chan_triggerU2 - HEAD_OF_EFFECT_CHANNELS +STRIP_BASE_TRIGGERLAYER_PART2;
               }


               // find the channel which has the maximum ADC value (pedestal substracted) on signal layer side 1
               const UplinkData& SignalUplink1 = event->uplinkData(m_uplinkId.at(2));
               unsigned short max_ADC_signalU1 = 0;
               unsigned short max_chan_signalU1 = 0 ;

               for(int chanIndex = 0; chanIndex<VATA64_READOUT_CHANNELS;chanIndex++){
                   m_signalLayerSide1_chan->Fill(chanIndex,SignalUplink1.adcValue(chanIndex)-m_pedestal_Mean.at(2)[chanIndex]);

                   if((chanIndex>HEAD_OF_EFFECT_CHANNELS-1) && (chanIndex<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS)){
                       unsigned short ADC_subPedestal = 0;
                       if(SignalUplink1.adcValue(chanIndex)>(m_pedestal_Mean.at(2)[chanIndex]))
                            ADC_subPedestal = SignalUplink1.adcValue(chanIndex)-m_pedestal_Mean.at(2)[chanIndex];
                      // printf("uplinkId = %d, SignalUplink1.adcValue(%d)=%d,m_pedestal_Mean.at(2)[%d]= %f \n",m_uplinkId.at(2),chanIndex,SignalUplink1.adcValue(chanIndex),chanIndex,m_pedestal_Mean.at(2)[chanIndex]);
                       if(ADC_subPedestal > max_ADC_signalU1) {
                            max_ADC_signalU1 = ADC_subPedestal;
                            max_chan_signalU1 =  chanIndex;
                       }
                       //printf("max_ADC_signalU1 = %d,max_chan_signalU1 = %d\n",max_ADC_signalU1,max_chan_signalU1);

                       //fill m_m_signalLayerSide1_strip
                       m_signalLayerSide1_strip->Fill(chanIndex-HEAD_OF_EFFECT_CHANNELS+STRIP_BASE_SIGNALLAYER,SignalUplink1.adcValue(chanIndex)-m_pedestal_Mean.at(2)[chanIndex]);
                   }
               }
               //printf("max_ADC_signalU1 = %d,max_chan_signalU1 = %d\n",max_ADC_signalU1,max_chan_signalU1);



               // find the channel which has the maximum ADC value (pedestal substracted) on signal layer side 2
               const UplinkData& SignalUplink2 = event->uplinkData(m_uplinkId.at(3));
               unsigned short max_ADC_signalU2 = 0;
               unsigned short max_chan_signalU2 = 0 ;

               for(int chanIndex = 0; chanIndex<VATA64_READOUT_CHANNELS;chanIndex++){
                   m_signalLayerSide2_chan->Fill(chanIndex,SignalUplink2.adcValue(chanIndex)-m_pedestal_Mean.at(3)[chanIndex]);

                   if((chanIndex>HEAD_OF_EFFECT_CHANNELS-1) && (chanIndex<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS)){
                       unsigned short ADC_subPedestal = 0;
                       if(SignalUplink2.adcValue(chanIndex)>(m_pedestal_Mean.at(3)[chanIndex]))
                             ADC_subPedestal = SignalUplink2.adcValue(chanIndex)-m_pedestal_Mean.at(3)[chanIndex];
                        //printf("uplinkId = %d,SignalUplink2.adcValue(%d)=%d,m_pedestal_Mean.at(3)[%d]= %f \n",m_uplinkId.at(3),chanIndex,SignalUplink2.adcValue(chanIndex),chanIndex,m_pedestal_Mean.at(3)[chanIndex]);

                       if(ADC_subPedestal > max_ADC_signalU2) {
                             max_ADC_signalU2 = ADC_subPedestal;
                             max_chan_signalU2 =  chanIndex;
                       }
                       //printf("max_ADC_signalU2 = %d,max_chan_signalU2 = %d\n",max_ADC_signalU2,max_chan_signalU2);

                       //fill m_m_signalLayerSide2_strip
                       m_signalLayerSide2_strip->Fill(HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1-chanIndex+STRIP_BASE_SIGNALLAYER,SignalUplink2.adcValue(chanIndex)-m_pedestal_Mean.at(3)[chanIndex]);
                    }
               }
               // printf("max_ADC_signalU2 = %d,max_chan_signalU2 = %d\n",max_ADC_signalU2,max_chan_signalU2);



               if((max_ADC_signalU1!=0) && (max_ADC_signalU2!=0)){
                    //printf("uplinkId = %d,max_ADC_signalU1 = %d,max_chan_signalU1 = %d\n",m_uplinkId.at(2),max_ADC_signalU1,max_chan_signalU1);
                    //printf("uplinkId = %d,max_ADC_signalU2 = %d,max_chan_signalU2 = %d\n",m_uplinkId.at(3),max_ADC_signalU2,max_chan_signalU2);
                    //getchar();
               }

               if(max_chan_signalU1 == 2*HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1-max_chan_signalU2) {
                    stripIndex_signalLayer = max_chan_signalU1 - HEAD_OF_EFFECT_CHANNELS +STRIP_BASE_SIGNALLAYER;
                    m_2layerCorrHisto1->Fill(stripIndex_triggerLayer,stripIndex_signalLayer,max_ADC_signalU1);
                    m_2layerCorrHisto2->Fill(stripIndex_triggerLayer,stripIndex_signalLayer,max_ADC_signalU2);
                    //printf("fill layerCorrHisto\n"); 

                    //fill a histogram for signalLayer1
                    for(int chanIndex = 0; chanIndex<VATA64_READOUT_CHANNELS;chanIndex++){
                        m_s1Histo_chan->Fill(chanIndex,SignalUplink1.adcValue(chanIndex)-m_pedestal_Mean.at(2)[chanIndex]);
                        if((chanIndex>HEAD_OF_EFFECT_CHANNELS-1) && (chanIndex<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS)){
                            //fill m_m_signalLayerSide2_strip
                            m_s1Histo_strip->Fill(chanIndex-HEAD_OF_EFFECT_CHANNELS+STRIP_BASE_SIGNALLAYER,SignalUplink1.adcValue(chanIndex)-m_pedestal_Mean.at(2)[chanIndex]);
                         }
                    }

                    //fill a histogram for signalLayer2
                    for(int chanIndex = 0; chanIndex<VATA64_READOUT_CHANNELS;chanIndex++){
                        m_s2Histo_chan->Fill(chanIndex,SignalUplink2.adcValue(chanIndex)-m_pedestal_Mean.at(3)[chanIndex]);
                        if((chanIndex>HEAD_OF_EFFECT_CHANNELS-1) && (chanIndex<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS)){
                            //fill m_m_signalLayerSide2_strip
                            m_s2Histo_strip->Fill(HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1-chanIndex+STRIP_BASE_SIGNALLAYER,SignalUplink2.adcValue(chanIndex)-m_pedestal_Mean.at(3)[chanIndex]);
                         }
                    }

               }

          }
         // printf("finishe 1 event!\n Press any key to continue...\n");
          //getchar();
     }

}

void DataManager::Plot2LayerCorr(){

    gStyle->SetPalette(1);
    char tmp[256];

    sprintf(tmp, "LayerCorr_triggerU%d_U%d_signalU%d_U%d",m_uplinkId.at(0),m_uplinkId.at(1),m_uplinkId.at(2),m_uplinkId.at(3));
    TCanvas* canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(1,2);

    canvas->cd(1);
    m_2layerCorrHisto1->Draw("lego");


    canvas->cd(2);
    m_2layerCorrHisto2->Draw("lego");

    //-----------------------------------
    sprintf(tmp, "histo_signalLayerSide_strip_U%d_U%d",m_uplinkId.at(2),m_uplinkId.at(3));
    canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(1,2);

    canvas->cd(1);
    double mean = m_signalLayerSide1_strip->GetMean(2);
    double rms = m_signalLayerSide1_strip->GetRMS(2);
    m_signalLayerSide1_strip->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
    canvas->GetPad(1)->SetLogz();
    m_signalLayerSide1_strip->Draw("COLZ");

    canvas->cd(2);
    mean = m_signalLayerSide2_strip->GetMean(2);
    rms = m_signalLayerSide2_strip->GetRMS(2);
    m_signalLayerSide2_strip->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
    canvas->GetPad(2)->SetLogz();
    m_signalLayerSide2_strip->Draw("COLZ");

    //-----------------------------------
    sprintf(tmp, "histo_signalLayerSide_allChannel_U%d_U%d",m_uplinkId.at(2),m_uplinkId.at(3));
    canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(1,2);

    canvas->cd(1);
    mean = m_signalLayerSide1_chan->GetMean(2);
    rms = m_signalLayerSide1_chan->GetRMS(2);
    m_signalLayerSide1_chan->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
    canvas->GetPad(1)->SetLogz();
    m_signalLayerSide1_chan->Draw("COLZ");

    canvas->cd(2);
    mean = m_signalLayerSide2_chan->GetMean(2);
    rms = m_signalLayerSide2_chan->GetRMS(2);
    m_signalLayerSide2_chan->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
    canvas->GetPad(2)->SetLogz();
    m_signalLayerSide2_chan->Draw("COLZ");

    //-----------------------------------
    sprintf(tmp, "Filtered_signalLayer_histo_strip_U%d_U%d",m_uplinkId.at(2),m_uplinkId.at(3));
    canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(1,2);

    canvas->cd(1);
    mean = m_s1Histo_strip->GetMean(2);
    rms = m_s1Histo_strip->GetRMS(2);
    m_s1Histo_strip->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
    canvas->GetPad(1)->SetLogz();
    m_s1Histo_strip->Draw("COLZ");

    canvas->cd(2);
    mean = m_s2Histo_strip->GetMean(2);
    rms = m_s2Histo_strip->GetRMS(2);
    m_s2Histo_strip->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
    canvas->GetPad(2)->SetLogz();
    m_s2Histo_strip->Draw("COLZ");

    //-----------------------------------
    sprintf(tmp, "Filtered_signalLayerSide_histo_allChannel_U%d_U%d",m_uplinkId.at(2),m_uplinkId.at(3));
    canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(1,2);

    canvas->cd(1);
    mean = m_s1Histo_chan->GetMean(2);
    rms = m_s1Histo_chan->GetRMS(2);
    m_s1Histo_chan->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
    canvas->GetPad(1)->SetLogz();
    m_s1Histo_chan->Draw("COLZ");

    canvas->cd(2);
    mean = m_s2Histo_chan->GetMean(2);
    rms = m_s2Histo_chan->GetRMS(2);
    m_s2Histo_chan->GetYaxis()->SetRangeUser(-50, mean + 10*rms);
    canvas->GetPad(2)->SetLogz();
    m_s2Histo_chan->Draw("COLZ");

    //-------------draw projectionY of m_signalLayerSide_chan and m_signalLayerSide_strip-----
    sprintf(tmp, "projectionY_signalLayerSide1_chan_U%d",m_uplinkId.at(2));
    canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(4,3);
    TH1 *hE = NULL;
    for(int chan = HEAD_OF_EFFECT_CHANNELS; chan<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;chan++){
        canvas->cd(chan-HEAD_OF_EFFECT_CHANNELS+1);
        gPad->SetLogy();
        sprintf(tmp,"U%d_chan%d_s1",m_uplinkId.at(2),chan);
        hE = m_signalLayerSide1_chan->ProjectionY(tmp,chan+1,chan+1,"");
        double mean = hE->GetMean();
        double rms = hE->GetRMS();
        hE->SetAxisRange(-50,mean+5*rms);
        hE->DrawCopy();
        canvas->Update();
     }

    sprintf(tmp, "projectionY_signalLayerSide2_chan_U%d",m_uplinkId.at(3));
    canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(4,3);
    hE = NULL;
    for(int chan = HEAD_OF_EFFECT_CHANNELS; chan<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;chan++){
        canvas->cd(chan-HEAD_OF_EFFECT_CHANNELS+1);
        gPad->SetLogy();
        sprintf(tmp,"U%d_chan%d_s2",m_uplinkId.at(3),chan);
        hE = m_signalLayerSide2_chan->ProjectionY(tmp,chan+1,chan+1,"");
        double mean = hE->GetMean();
        double rms = hE->GetRMS();
        hE->SetAxisRange(-50,mean+5*rms);
        hE->DrawCopy();
        canvas->Update();
     }

    sprintf(tmp, "projectionY_signalLayerSide1_strip_U%d",m_uplinkId.at(2));
    canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(4,3);
    hE = NULL;
    for(int strip = STRIP_BASE_SIGNALLAYER; strip<STRIP_BASE_SIGNALLAYER+NUMBER_OF_EFFECT_CHANNELS;strip++){
        canvas->cd(strip-STRIP_BASE_SIGNALLAYER+1);
        gPad->SetLogy();
        sprintf(tmp,"U%d_strip%d_s1",m_uplinkId.at(2),strip);
        hE = m_signalLayerSide1_strip->ProjectionY(tmp,strip-STRIP_BASE_SIGNALLAYER+1,strip-STRIP_BASE_SIGNALLAYER+1,"");
        double mean = hE->GetMean();
        double rms = hE->GetRMS();
        hE->SetAxisRange(-50,mean+5*rms);
        hE->DrawCopy();
        canvas->Update();
     }

    sprintf(tmp, "projectionY_signalLayerSide2_strip_U%d",m_uplinkId.at(3));
    canvas =  new TCanvas(tmp,tmp,0,0,1200,900);
    canvas->Divide(4,3);
    hE = NULL;
    for(int strip = STRIP_BASE_SIGNALLAYER; strip<STRIP_BASE_SIGNALLAYER+NUMBER_OF_EFFECT_CHANNELS;strip++){
        canvas->cd(strip-STRIP_BASE_SIGNALLAYER+1);
        gPad->SetLogy();
        sprintf(tmp,"U%d_strip%d_s2",m_uplinkId.at(3),strip);
        hE = m_signalLayerSide1_strip->ProjectionY(tmp,strip-STRIP_BASE_SIGNALLAYER+1,strip-STRIP_BASE_SIGNALLAYER+1,"");
        double mean = hE->GetMean();
        double rms = hE->GetRMS();
        hE->SetAxisRange(-50,mean+5*rms);
        hE->DrawCopy();
        canvas->Update();
     }



}


void DataManager::SaveAllHistos(){
    TFile* recordFile =  new TFile("../data/lect_uplinks_result.root","RECREATE");

    for(unsigned int i=0;i<m_uplinkId.size();++i){
        m_raw_pedestals[i]->Write();
        m_adcHistogram[i]->Write();
        m_PS_adcHistogram[i]->Write();
        m_PS_pedestals[i]->Write();
    }

    if(m_readInSetupFile){
        std::map<int,TH2*>::iterator stripCorrelation_iter;
        for(stripCorrelation_iter = m_strip_correlation.begin();stripCorrelation_iter!=m_strip_correlation.end();stripCorrelation_iter++){
            stripCorrelation_iter->second->Write();
        }
    }else{
        for (unsigned int i=0;i< m_uplink_correlation.size();i++){
            m_uplink_correlation[i]->Write();
        }
    }

    for(unsigned int i=0; i< m_adcPerPhoton.size();i++){
        m_adcPerPhoton[i]->Write();
    }

    for(unsigned int i=0; i<m_mpvInAdc.size();i++){
        m_mpvInAdc[i]->Write();
    }

    for(unsigned int i=0; i<m_lSigma.size();i++){
        m_lSigma[i]->Write();
    }

    for(unsigned int i=0; i<m_gSigma.size();i++){
        m_gSigma[i]->Write();
    }

    for(unsigned int i=0; i<m_resultCorr.size();i++){
        m_resultCorr[i]->Write();
    }

    for(unsigned int i=0; i<m_area.size();i++){
        m_area[i]->Write();
    }

    for(unsigned int i=0; i<m_meanInPhoton.size();i++){
        m_meanInPhoton[i]->Write();
    }

    /*
    m_2layerCorrHisto1->Write();
    m_2layerCorrHisto2->Write();

    m_signalLayerSide1_strip->Write();
    m_signalLayerSide2_strip->Write();

    m_signalLayerSide1_chan->Write();
    m_signalLayerSide2_chan->Write();

    m_s1Histo_strip->Write();
    m_s2Histo_strip->Write();
    m_s1Histo_chan->Write();
    m_s2Histo_strip->Write();
    */

    recordFile->Close();

}


Double_t fconvo_func(Double_t *x, Double_t *par) {

  //Fit parameters:
  //par[0]=Width (scale) parameter of Landau density
  //par[1]=Most Probable (MP, location) parameter of Landau density
  //par[2]=normalization for the signal
  //par[3]=readout gain (#ADC/firing cell), fixed parameter
  //par[4]=normalization for the background
  //In the Landau distribution (represented by the CERNLIB approximation),
  //the maximum of the landau is par[1] = mpc - 0.22278298*par[0] (where mpc is the mpv parameter of the landau)
  //This shift is corrected within this function, so that the actual maximum
  //is identical to the MP parameter, par[1].
  //Since we do a convolution with a Poisson distribution,
  //the real maximum of the spectrum is slightly higher than par[1]

  // Numeric constants
  Double_t mpshift  = -0.22278298;       // Landau maximum location

  // Control constants
  Double_t np = 100.0;      // number of convolution steps
  Double_t sc =   5.0;      // convolution extends to +-sc Gaussian sigmas

  // Variables
  Double_t xx;
  Double_t mpc;
  Double_t fland;
  Double_t sum = 0.0;
  Double_t xlow,xupp;
  Double_t step;
  Double_t i;

  // MP shift correction
  mpc = par[1] - mpshift * par[0];

  // Range of convolution integral
  if (x[0]>0) {
    xlow = x[0] - sc * TMath::Sqrt(x[0] / par[3]) * par[3];
    xupp = x[0] + sc * TMath::Sqrt(x[0] / par[3]) * par[3];
  }
  if ((xlow>0)||(xlow==0)) step = (xupp-xlow) / np;
  else step = xupp / np;

  // Convolution integral of Landau and Gaussian by sum
  if ((xlow>0)&&(x[0]>0)) {
    for(i=1.0; i<=np/2; i++) {
      xx = xlow + (i-.5) * step;
      fland = TMath::Landau(xx,mpc,par[0]) / par[0];
      sum += fland * TMath::Poisson(x[0]/par[3],xx/par[3]);

      xx = xupp - (i-.5) * step;
      fland = TMath::Landau(xx,mpc,par[0]) / par[0];
      sum += fland * TMath::Poisson(x[0]/par[3],xx/par[3]);
    }
  }
  if (((xlow<0)||(xlow==0))&&(x[0]>0)) {
    for(i=1.0; i<=np; i++) {
      xx = 0 + (i-.5) * step;
      fland = TMath::Landau(xx,mpc,par[0]) / par[0];
      sum += fland * TMath::Poisson(x[0]/par[3],xx/par[3]);
    }
  }
  if (x[0]<0) sum = 0;

  Double_t sr = par[2] * step * sum;
  Double_t xxx=x[0];
  Int_t bin = background->GetXaxis()->FindBin(xxx);
  Double_t br = par[4]*background->GetBinContent(bin);

  return (sr + br);
}



void DataManager:: MPVinADC(int uplinkId,Int_t rebin,bool plot){

    //set TCanvas
     //gStyle->SetOptFit(0000);


     TH2 *PS_adcHistogram_uplink = m_PS_adcHistogram.at(uplinkIndex(uplinkId));
     TH2 *PS_pedestals_uplink = m_PS_pedestals.at(uplinkIndex(uplinkId));

     printf("analyze m_PS_adcHistogram.at(%d)_uplinkId%d \n",uplinkIndex(uplinkId),uplinkId);
     printf("analyze m_PS_pedestals.at(%d)_uplinkId%d \n",uplinkIndex(uplinkId),uplinkId);

     char tmp[256];
     TCanvas *c1 = NULL;   //TCanvas for ADCperPhoton
     TH1 *hE = NULL;



     printf("for UplinkId = %d, find MPV in ADC\n",uplinkId);
     FILE* resultLog = NULL;
     //open a file to log the result
     sprintf(tmp,"../data/uplink%d_MPV.txt",uplinkId);
     resultLog = fopen(tmp,"w");
     fprintf(resultLog,"-----find MPV in ADC --------\n\n");
     fprintf(resultLog,"Uplink\t channels\t fconvo maximumX\t fconvo_s maximumX\t fconvo_s mean\t flandau mean\t mpv\t\n");




     //gStyle->SetOptFit();

     TGraphErrors* mpvInAdc = m_mpvInAdc.at(uplinkIndex(uplinkId));



    // TGraphErrors* lSigma = m_lSigma.at(uplinkIndex(uplinkId));

    // TGraphErrors* gSigma = m_gSigma.at(uplinkIndex(uplinkId));

     TH1F* area = m_area.at(uplinkIndex(uplinkId));






     //Int_t rebin=12;
     //Int_t xmin=35; //25 at 78.3V, 35 at 78.8V, 45 at 79.3V
    // Int_t xmin=40;

     int xPadNum = 4;
     int yPadNum = 3;
     int padPerCanvas = xPadNum*yPadNum;


     int canvasNum = 0;
     if(NUMBER_OF_EFFECT_CHANNELS%padPerCanvas == 0)    canvasNum = NUMBER_OF_EFFECT_CHANNELS/padPerCanvas;
     else    canvasNum = NUMBER_OF_EFFECT_CHANNELS/padPerCanvas+1;
     int canvasIndex = 0;

     Int_t graph_index = 0;
     Double_t measuredGain = 0;
     Double_t gain_chan = 0;


     int picIndex = 0;
     for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;chan++){
         if(m_readInSetupFile){
             if((chan%CHANNELS_PER_GROUP)>(STRIPS_PER_GROUP-1)) continue;
         }

         if((picIndex%padPerCanvas) == 0){
             if(m_readInSetupFile){
                 sprintf(tmp,"uplink%d_MPV_in_ADC (%d)",uplinkId,picIndex/padPerCanvas);
             }else{
                 if(canvasNum==1) sprintf(tmp,"uplink%d_MPV_in_ADC",uplinkId);
                 else  sprintf(tmp,"uplink%d_MPV_in_ADC (%d)",uplinkId,canvasIndex+1);
             }

             c1 = new TCanvas(tmp,tmp,10,10,1000,900);
             c1->Divide(xPadNum,yPadNum);
             canvasIndex++;
             //std::cout<<"create a new canvas: now picIndex = "<<picIndex<<" chan = "<<chan<<std::endl;
             //getchar();
         }

         //Double_t mean,rms;
         //int rebin_undo = 1; //better not rebin it

         //c1->cd((chan-HEAD_OF_EFFECT_CHANNELS)%padPerCanvas+1);
         c1->cd(picIndex%padPerCanvas+1);
         picIndex++;
         c1->GetPad((chan-HEAD_OF_EFFECT_CHANNELS)%padPerCanvas+1)->SetLogy();

         sprintf(tmp,"U%d_chan%d",uplinkId,chan);
         hE = PS_adcHistogram_uplink->ProjectionY(tmp,chan+1,chan+1,"");
         //double mean = hE->GetMean();
         double rms = hE->GetRMS();
        // Int_t bin = hE->GetXaxis()->FindBin(mean);
        //  Float_t A = hE->GetBinContent(bin);
       //  area->Fill(chan,A);

         if(rms<2) continue;  //will not fit with MIP

         //hE->Rebin(rebin_undo);


         double xmin_fit = 0;
         double xmax_fit = 3000;

         sprintf(tmp,"background_U%d_chan%d",uplinkId,chan);
         background = PS_pedestals_uplink->ProjectionY(tmp,chan+1,chan+1,"");
         //background->Rebin(rebin_undo);


         /////////////////////////////////////////////////////////
         // fit with convolution landau + poisson + background

         TF1 *fconvo = new TF1("fconvo",fconvo_func,xmin_fit,xmax_fit,5);
         fconvo->SetParameter(0,10);
         //fconvo->SetParameter(1,65);
         if(uplinkId>43)fconvo->SetParameter(1,65);
         //if(uplinkId>43)fconvo->SetParameter(1,100);
         else fconvo->SetParameter(1,120); //for MAPD

         //temp
         //if(uplinkId == 40 && chan == 10) fconvo->SetParameter(1,100);//only used for layer11_cosmic_20120411.root


         if(uplinkId>43)fconvo->SetParLimits(1,20,150);
         else fconvo->SetParLimits(1,20,100); //for MAPD
         fconvo->FixParameter(3,rebin);
         fconvo->SetParameter(2,10000);
         fconvo->SetParameter(4,40);
         hE->SetAxisRange(-100,3000,"X");
         hE->Draw();
         hE->Fit("fconvo","R");
         Double_t fconvo_max = fconvo->GetMaximumX(40,120);
         std::cout<<"fconvo maximumX="<<fconvo_max<<"\n";


         printf("TF1 *fconvo finished\n");

         TF1 *fconvo_s = new TF1("fconvo_s",fconvo_func,xmin_fit,xmax_fit,5);

         fconvo_s->FixParameter(0,fconvo->GetParameter(0));
         fconvo_s->FixParameter(1,fconvo->GetParameter(1));
         fconvo_s->FixParameter(3,rebin);
         fconvo_s->FixParameter(2,fconvo->GetParameter(2));
         fconvo_s->FixParameter(4,0);
         fconvo_s->SetLineColor(kOrange);
         fconvo_s->SetLineWidth(3);
         fconvo_s->Draw("SAME");
        // Double_t fconvo_s_max = fconvo_s->GetMaximumX(40,120);
         Double_t fconvo_s_max = fconvo_s->GetMaximumX(20,120);
         std::cout<<"fconvo_s maximumX="<<fconvo_s_max<<"\n";


         //////////////////////////////////////////////////////
         // calculation of the mean of the MIP distribution
         Double_t mean_MIPdistribution=fconvo_s->Moment(1,xmin_fit,xmax_fit);
         std::cout<<"with first moment, fconvo_s mean="<<mean_MIPdistribution<<"\n";
         Int_t bin = hE->GetXaxis()->FindBin(mean_MIPdistribution);
         Float_t A = hE->GetBinContent(bin);
         area->Fill(chan,A);
         //////////////////////////////////////////////////////

         TF1 *fconvo_bg = new TF1("fconvo_bg",fconvo_func,xmin_fit,xmax_fit,5);
         fconvo_bg->FixParameter(0,fconvo->GetParameter(0));
         fconvo_bg->FixParameter(1,fconvo->GetParameter(1));
         fconvo_bg->FixParameter(3,rebin);
         fconvo_bg->FixParameter(2,0);
         fconvo_bg->FixParameter(4,fconvo->GetParameter(4));
         fconvo_bg->SetLineColor(kRed);
         fconvo_bg->Draw("SAME");


         // plot the landau without convolution
         TF1 *flandau = new TF1("flandau","[2]*TMath::Landau(x,[0],[1])",xmin_fit-100,xmax_fit);
         Double_t mpv_param=fconvo->GetParameter(1)+0.22278298*fconvo->GetParameter(0);
         flandau->SetParameter(0,mpv_param);
         flandau->SetParameter(1,fconvo->GetParameter(0));
         flandau->SetParameter(2,fconvo->GetParameter(2));
         flandau->SetLineColor(kGreen);
         flandau->Draw("SAME");
         std::cout<<"flandau maximumX="<<flandau->GetMaximumX(40,120)<<"\n";
         double mean_flandau=flandau->Moment(1,0,3000);
         std::cout<<"with first moment, flandau mean="<<mean_flandau<<"\n";

         double mpv = fconvo_s->GetParameter(1);
         std::cout<<"mpv = "<<mpv<<std::endl;
         mpvInAdc->SetPoint(graph_index,chan,mpv);
         fprintf(resultLog,"%d\t %d\t %f\t %f\t %f\t %f\t %f\t\n",uplinkId,chan,fconvo_max,fconvo_s_max,mean_MIPdistribution,mean_flandau,mpv);
         graph_index++;

         /*
         for(int i = 0; i<NUMBER_OF_EFFECT_CHANNELS;i++){
              adcPerPhoton->GetPoint(i,gain_chan,measuredGain);
              if((gain_chan == chan) && (measuredGain<ADC_PER_PHOTON_UPPER_THRESHOLD)){
                     mpvInAdc->SetPoint(graph_index,chan,mpv);
                     resultCorr->SetPoint(graph_index,chan,mpv/measuredGain);
                     std::cout<<"graph_index= "<<graph_index<<std::endl;
                     fprintf(resultLog,"%d\t %d\t %f\t %f\t %f\t %f\t %f\t %f\t\n",uplinkId,chan,fconvo_max,fconvo_s_max,mean_MIPdistribution,mean_flandau,mpv,mpv/measuredGain);
                     graph_index++;
              }
          }
          */

          hE->GetXaxis()->SetRangeUser(-50,350);
          c1->Update();
          //getchar();

      }

      fclose(resultLog);
      hE = NULL;

     //make a new TCanvas to plot MPV in ADC for all the channels
     if(plot){
        sprintf(tmp,"uplink%d    MPV_in_ADC Variation",uplinkId);
        TCanvas *c2 = new TCanvas(tmp,tmp,10,10,1000,900);
        c2->Divide(1,1);

        c2->cd(1);

        TH1F *hr = c2->cd(1)->DrawFrame(0,40,13,160);
        hr->SetXTitle("chan #");
        hr->SetYTitle("MPV, ADC");

        mpvInAdc->SetMarkerStyle(20);
        mpvInAdc->SetMarkerColor(kRed);
        mpvInAdc->SetMarkerSize(2.);


        mpvInAdc->Fit("pol1");

        mpvInAdc->Draw("P");

        c2->cd(1)->SetGridx();
        c2->cd(1)->SetGridy();
     }

 }


void DataManager::FillPsPedestal(MergedEvent* event)
{
  Settings* settings = Settings::instance();
  std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
  std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();

  for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
    int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
    unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
    unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
    unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;
    const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);

    for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip) {
      for (unsigned short channel = 0; channel < (channelsPerReadoutChip); ++channel) {
        unsigned short sample = chip * channelsPerReadoutChip + channel;
        m_PS_pedestals[i]->Fill(sample , uplinkData.adcValue(sample)-m_pedestal_Mean.at(i)[sample]);
      }
    }
  }
}

TH2* DataManager::GetPS_pedestals(int uplinkId){
    return m_PS_pedestals.at(uplinkIndex(uplinkId));
}


//void DataManager::FillAllByExistFiles(std::string analyzeFilePath){
//    TFile *file = new TFile(analyzeFilePath.c_str(),"READ");

//    char tmp[256];

//    //Fill the raw pedestals
//    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
//        sprintf(tmp,"raw_pedestals_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
//        m_raw_pedestals[uplinkIndex] = (TH2*)file->Get(tmp);
//    }

//    //Fill the PS_pedestal
//    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
//        sprintf(tmp,"PS_pedestals_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
//        m_PS_pedestals[uplinkIndex] = (TH2*)file->Get(tmp);
//    }

//    //Fill the adcHistogram
//    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
//        sprintf(tmp,"adcHistogram_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
//        m_adcHistogram[uplinkIndex] = (TH2*)file->Get(tmp);
//    }

//    //fill the PS_adcHistogram
//    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
//        sprintf(tmp,"PS_adcHistogram_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
//        m_PS_adcHistogram[uplinkIndex] = (TH2*)file->Get(tmp);
//    }

//    //fill the m_uplink_correlation
//    if((m_uplinkId.size()%2==0) && (m_uplinkId.size()!=0) ){
//        for(int corrIndex=0; corrIndex< (m_uplinkId.size()/2); corrIndex++){
//            int Xuplink = m_uplinkId.at(2*corrIndex);
//            int Yuplink = m_uplinkId.at(2*corrIndex+1);

//            for (Int_t chan=HEAD_OF_EFFECT_CHANNELS;chan<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;chan++){
//                 sprintf(tmp, "uplink_%d_vs_%d_____uplink%d_channel%03d_with_uplink%d_channel%03d",Xuplink,Yuplink,Xuplink,chan,Yuplink,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS-1-chan);
//                 m_uplink_correlation[corrIndex*NUMBER_OF_EFFECT_CHANNELS+chan-HEAD_OF_EFFECT_CHANNELS] = (TH2*)file->Get(tmp);
//            }
//        }
//    }

//}

void DataManager::MPVinPE(int uplinkId){

    char tmp[256];

    printf("for UplinkId = %d, calculate MIP in p.e.\n",uplinkId);
    FILE* resultLog = NULL;
    //open a file to log the result
    sprintf(tmp,"../data/uplink%d_MPVinPE.txt",uplinkId);
    resultLog = fopen(tmp,"w");
    fprintf(resultLog,"-----find MPV in PE --------\n\n");
    fprintf(resultLog,"Uplink\t channels\t mpv in p.e.\n");


    TGraphErrors* gains =  m_adcPerPhoton.at(uplinkIndex(uplinkId));
    TGraphErrors* mpv = m_mpvInAdc.at(uplinkIndex(uplinkId));
    TGraphErrors* resultCorr = m_resultCorr.at(uplinkIndex(uplinkId));

    double chan = 0;
    double corrChan = 1;
    double measuredGain = 0;
    double measuredMPV = 0;
    for(int i=0; i<mpv->GetN();i++){
        //printf("mpv->GetN() = %d\n",mpv->GetN());
        mpv->GetPoint(i,chan,measuredMPV);
        //printf("chan = %d\n",chan);
        for(int j=0; j<gains->GetN();j++){
             //printf("gains->GetN() = %d\n",gains->GetN());
             gains->GetPoint(j,corrChan,measuredGain);
             //printf("corrChan = %d\n",corrChan);
             //getchar();
             if(corrChan == chan) {
                 resultCorr->SetPoint(i,chan,measuredMPV/measuredGain);
                 std::cout<< "i= "<<i <<" chan="<<chan<<" MPV = "<<measuredMPV <<" GAIN= "<<measuredGain<<"MIPinPE = "<<measuredMPV/measuredGain<<std::endl;
                 fprintf(resultLog,"%d\t%d\t%.2f\t\n",uplinkId,chan,measuredMPV/measuredGain);
                 //getchar();
             }
        }
    }

    fclose(resultLog);
}


DataManager::DataManager(std::string pathToSetupFile){
    char tmp[128];
    Settings* settings = Settings::instance();


    m_pathToDACFile = "";
    m_pathToFormerCfgFile = "";
    m_dacFromCfgFile = false;
    m_dacFromAdjustFile = false;
    m_forceToChangeCfgFile = false;
    m_aimGain = 50;

    m_readInSetupFile = true;

// read set up from the setupFile
    std::ifstream setupFile(pathToSetupFile.c_str());
    if(!setupFile){
        std::cerr << "Failure: could not open file: \"" << pathToSetupFile << "\"." << std::endl;
        std::cerr << "Please check if the path is correct or not!" << std::endl;
        assert(false);
    }

    std::string s;
    int temp;
    while (true) {
        std::getline(setupFile, s);
        if (setupFile.eof())
            break;
        const char* searchString = s.c_str();

        if (s.find("#") == 0 || s=="") {
            continue; // Skip commented lines
        }

        if(sscanf(searchString, "L1_P1_P || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P1_P,temp));
        }

        if(sscanf(searchString, "L1_P1_N || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P1_N,temp));
        }

        if(sscanf(searchString, "L1_P2_P || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P2_P,temp));
        }

        if(sscanf(searchString, "L1_P2_N || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P2_N,temp));
        }

        if(sscanf(searchString, "L1_P3_P || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P3_P,temp));
        }

        if(sscanf(searchString, "L1_P3_N || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P3_N,temp));
        }

        if(sscanf(searchString, "L1_P4_P || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P4_P,temp));
        }

        if(sscanf(searchString, "L1_P4_N || %d ||",&temp )==1){
            m_superLayerMap[0].insert(SuperLayerMap::value_type(P4_N,temp));
        }

        if(sscanf(searchString, "L2_P1_P || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P1_P,temp));
        }

        if(sscanf(searchString, "L2_P1_N || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P1_N,temp));
        }

        if(sscanf(searchString, "L2_P2_P || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P2_P,temp));
        }

        if(sscanf(searchString, "L2_P2_N || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P2_N,temp));
        }

        if(sscanf(searchString, "L2_P3_P || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P3_P,temp));
        }

        if(sscanf(searchString, "L2_P3_N || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P3_N,temp));
        }

        if(sscanf(searchString, "L2_P4_P || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P4_P,temp));
        }

        if(sscanf(searchString, "L2_P4_N || %d ||",&temp )==1){
            m_superLayerMap[1].insert(SuperLayerMap::value_type(P4_N,temp));
        }

     }


    SuperLayerMap::iterator superLayer_iter;
    std::vector<int> uplinksToBeAnalyzed;
    for(int superLayerIndex = 0;superLayerIndex<2;superLayerIndex++){
        for(superLayer_iter= m_superLayerMap[superLayerIndex].begin();superLayer_iter!=m_superLayerMap[superLayerIndex].end();superLayer_iter++){
             if(superLayer_iter->second!=0) uplinksToBeAnalyzed.push_back(superLayer_iter->second);
             //printf("m_superLayerMap[%d][%d] = %d \n",superLayerIndex,superLayer_iter->first,superLayer_iter->second);
             //getchar();
        }
    }

    printf("Datamanager::uplinksToBeAnalyzed.size() = %d\n",uplinksToBeAnalyzed.size());

    for (unsigned int k = 0; k < uplinksToBeAnalyzed.size(); ++k) {
        int uplinkId = uplinksToBeAnalyzed[k];
        printf("uplinkId = %d\n",uplinkId);
        int usbBoardId = settings->usbBoardIdFromUplinkId(uplinkId);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();

        m_uplinkId.push_back(uplinkId);

        sprintf(tmp, "adcHistogram_uplink_%03d", uplinkId);
        TH2* adcHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        adcHistogram->GetXaxis()->SetTitle("channel");
        adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_adcHistogram.push_back(adcHistogram);

        sprintf(tmp, "PS_adcHistogram_uplink_%03d", uplinkId);
        TH2* PS_adcHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue+500, -500, maxAdcValue);
        PS_adcHistogram->GetXaxis()->SetTitle("channel");
        PS_adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_PS_adcHistogram.push_back(PS_adcHistogram);

        sprintf(tmp, "PS_pedestals_uplink_%03d", uplinkId);
        TH2* PS_pedestals = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue+500, -500, maxAdcValue);
        PS_pedestals->GetXaxis()->SetTitle("channel");
        PS_pedestals->GetYaxis()->SetTitle("adcValue");
        m_PS_pedestals.push_back(PS_pedestals);

        sprintf(tmp, "raw_pedestals_uplink_%03d", uplinkId);
        TH2* raw_pedestals = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        raw_pedestals->GetXaxis()->SetTitle("channel");
        raw_pedestals->GetYaxis()->SetTitle("adcValue");
        m_raw_pedestals.push_back(raw_pedestals);

        TGraphErrors* adcPerPhoton = new TGraphErrors(1);
        m_adcPerPhoton.push_back(adcPerPhoton);

        TGraphErrors* mpvInAdc = new TGraphErrors(1);
        m_mpvInAdc.push_back(mpvInAdc);

        TGraphErrors* resultCorr = new TGraphErrors(1);
        m_resultCorr.push_back(resultCorr);

        TGraphErrors* lSigma = new TGraphErrors(1);
        m_lSigma.push_back(lSigma);

        TGraphErrors* gSigma = new TGraphErrors(1);
        m_gSigma.push_back(gSigma);

        sprintf(tmp, "area_uplink%d", uplinkId);
        TH1F* area = new TH1F(tmp,"area_for_mean_Of_MIP_Distribution;Channel;Area",NUMBER_OF_EFFECT_CHANNELS,HEAD_OF_EFFECT_CHANNELS,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS);
        m_area.push_back(area);

        TGraphErrors* meanPhoton = new TGraphErrors(1);
        m_meanInPhoton.push_back(meanPhoton);

        // TH2* PS_pedestals = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue+500, -500, maxAdcValue);
        // xxxx
        sprintf(tmp, "NOT USED**SignalADC after 1st thrsd 1chan U%d", uplinkId);
        TH2* signalHist_1chan = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, 0, maxAdcValue);  // +500
        m_signalADC_withThrsd.push_back(signalHist_1chan);

        sprintf(tmp, "NOT USED**SignalADC after PedSub 1chan U%d", uplinkId);
        TH2* signalHist_1chan1 = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, 0, maxAdcValue); // +500
        m_signalADC_withThrsd_pedSub.push_back(signalHist_1chan1);
     //
        sprintf(tmp, "SignalADC CMS U%d", uplinkId);
        TH2* signalHist_CMS_ADC = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, 0, maxAdcValue); // +500
        m_signalADC_CMS.push_back(signalHist_CMS_ADC);
     //
        sprintf(tmp, "CMS val U%d", uplinkId);
        TH1* signalHist_CMS_val = new TH1I(tmp, tmp, 1000, 0, 1000);
        m_CMS_val.push_back(signalHist_CMS_val);
     }
    TH1* clusterSize_hist = new TH1I("Cluster size", "Cluster size", 1000, 0, 1000);
    m_ClusterSize.push_back(clusterSize_hist);

    TH1* NbCluster_hist = new TH1I("Cluster number", "Cluster number", 1000, 0, 1000);
    m_NbCluster.push_back(NbCluster_hist);

    TH1* ClusterADC_hist = new TH1I("ClusterADC", "ClusterADC", maxAdcValue, 0, maxAdcValue);   //+500
    m_ClusterADC.push_back(ClusterADC_hist);

    TH1D* ClusterChannel_hist = new TH1D("Cluster channel", "Cluster channel", 1000, 0, 1000);
    m_ClusterChannel.push_back(ClusterChannel_hist);




    SuperLayerMap::iterator corrIter;
    for(int superLayerIndex = 0; superLayerIndex<2;superLayerIndex++){
        for(superLayer_iter= m_superLayerMap[superLayerIndex].begin();superLayer_iter->first<4;superLayer_iter++){
               if(superLayer_iter->second == 0) continue;
               corrIter = m_superLayerMap[superLayerIndex].find(superLayer_iter->first+P_N_DISTANCE);
               if(corrIter == m_superLayerMap[superLayerIndex].end()){
                    printf("Can not find its corresponding part of %d, check the setup file!\n",superLayer_iter->first);
                    assert(false);
               }else{
                    if(corrIter->second != 0){
                         m_corrUplinkPairs.push_back(superLayer_iter->second);
                         m_corrUplinkPairs.push_back(corrIter->second);
                    }
              }
         }
     }

//    printf("corrUplinkPairs = %d \n",corrUplinkPairs.size());
//    for(int i = 0; i<corrUplinkPairs.size();i++){
//         printf("corrUplinkPairs[%d] = %d\n",i,corrUplinkPairs.at(i)) ;
//    }
//    getchar();

    /*
     if(m_corrUplinkPairs.size()!=0 ){
         for(int i=0; i< (m_corrUplinkPairs.size()/2); i++){
             int Xuplink = m_corrUplinkPairs.at(2*i);
             int Yuplink = m_corrUplinkPairs.at(2*i+1);

             int corrChan = 0;

             for (Int_t i=HEAD_OF_EFFECT_CHANNELS;i<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;i++){
                  if((i%CHANNELS_PER_GROUP)>(STRIPS_PER_GROUP-1)) continue; // only 0~11,16~27,32~43,48~59 are SiPM output
                  corrChan = (i/CHANNELS_PER_GROUP)*CHANNELS_PER_GROUP+STRIPS_PER_GROUP-1-i%CHANNELS_PER_GROUP;
                  //std::cout<<"chan= "<<i <<" & corrChan = "<<corrChan <<" are correlated!"<<std::endl;
                  //getchar();
                  sprintf(tmp, "uplink_%d_vs_%d_____uplink%d_channel%03d_with_uplink%d_channel%03d",Xuplink,Yuplink,Xuplink,i,Yuplink,corrChan);
                  TH2* uplink_correlation = new TH2I(tmp,tmp, (maxAdcValue+500)/10, -500., maxAdcValue, (maxAdcValue+500)/10, -500., maxAdcValue);
                  sprintf(tmp,"uplink_%d",Xuplink);
                  uplink_correlation->GetXaxis()->SetTitle(tmp);
                  sprintf(tmp,"uplink_%d",Yuplink);
                  uplink_correlation->GetYaxis()->SetTitle(tmp);
                  m_uplink_correlation.push_back(uplink_correlation);
                  //printf("add m_uplink_correlation, now m_uplink_correlation.size() = %d",m_uplink_correlation.size());
             }
         }
     }
     */
     if(m_corrUplinkPairs.size()!=0 ){
        for(int i=0; i< (m_corrUplinkPairs.size()/2); i++){
            int Xuplink = m_corrUplinkPairs.at(2*i);
            int Yuplink = m_corrUplinkPairs.at(2*i+1);

            int corrChan = 0;
            int stripIndex = 0;

            for (Int_t i=HEAD_OF_EFFECT_CHANNELS;i<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;i++){
                 if((i%CHANNELS_PER_GROUP)>(STRIPS_PER_GROUP-1)) continue; // only 0~11,16~27,32~43,48~59 are SiPM output
                 corrChan = (i/CHANNELS_PER_GROUP)*CHANNELS_PER_GROUP+STRIPS_PER_GROUP-1-i%CHANNELS_PER_GROUP;

                 int SuperLayerIndex = 0;
                 int positionIndex = 0;


                 for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                      for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                           if(superLayer_iter->second==Xuplink) {
                                SuperLayerIndex = LayerIndex;
                                positionIndex = superLayer_iter->first;
                                break;
                           }
                      }
                 }

                 stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+(i/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+i%CHANNELS_PER_GROUP;
                 //std::cout<<"chan= "<<i <<" & corrChan = "<<corrChan <<" are correlated!"<<" stripIndex = "<<stripIndex<<std::endl;
                 //getchar();
                 sprintf(tmp, "uplink_%d_vs_%d_____uplink%d_channel%03d_with_uplink%d_channel%03d",Xuplink,Yuplink,Xuplink,i,Yuplink,corrChan);
                 TH2* uplink_correlation = new TH2I(tmp,tmp, (maxAdcValue+500)/10, -500., maxAdcValue, (maxAdcValue+500)/10, -500., maxAdcValue);
                 sprintf(tmp,"uplink_%d",Xuplink);
                 uplink_correlation->GetXaxis()->SetTitle(tmp);
                 sprintf(tmp,"uplink_%d",Yuplink);
                 uplink_correlation->GetYaxis()->SetTitle(tmp);
                 m_strip_correlation.insert(std::map<int,TH2*>::value_type(stripIndex,uplink_correlation));
            }
        }
     }


}

DataManager::DataManager(std::string pathToSetupFile,std::string pathToReadInFile){
    m_pathToDACFile = "";
    m_pathToFormerCfgFile = "";
    m_dacFromCfgFile = false;
    m_dacFromAdjustFile = false;
    m_forceToChangeCfgFile = false;
    m_aimGain = 50;

    m_readInSetupFile = true;


    // read set up from the setupFile
    std::ifstream setupFile(pathToSetupFile.c_str());
    if(!setupFile){
        std::cerr << "Failure: could not open file: \"" << pathToSetupFile << "\"." << std::endl;
        std::cerr << "Please check if the path is correct or not!" << std::endl;
        assert(false);
    }

    std::string s;
    int temp;
    while (true) {
        std::getline(setupFile, s);
        if (setupFile.eof()) break;
        const char* searchString = s.c_str();

        if (s.find("#") == 0 || s=="") {
            continue; // Skip commented lines
        }

        if(sscanf(searchString, "L1_P1_P || %d ||",&temp )==1){
             m_superLayerMap[0].insert(SuperLayerMap::value_type(P1_P,temp));
        }

        if(sscanf(searchString, "L1_P1_N || %d ||",&temp )==1){
             m_superLayerMap[0].insert(SuperLayerMap::value_type(P1_N,temp));
        }

        if(sscanf(searchString, "L1_P2_P || %d ||",&temp )==1){
             m_superLayerMap[0].insert(SuperLayerMap::value_type(P2_P,temp));
        }

        if(sscanf(searchString, "L1_P2_N || %d ||",&temp )==1){
             m_superLayerMap[0].insert(SuperLayerMap::value_type(P2_N,temp));
        }

        if(sscanf(searchString, "L1_P3_P || %d ||",&temp )==1){
             m_superLayerMap[0].insert(SuperLayerMap::value_type(P3_P,temp));
        }

        if(sscanf(searchString, "L1_P3_N || %d ||",&temp )==1){
             m_superLayerMap[0].insert(SuperLayerMap::value_type(P3_N,temp));
        }

        if(sscanf(searchString, "L1_P4_P || %d ||",&temp )==1){
             m_superLayerMap[0].insert(SuperLayerMap::value_type(P4_P,temp));
        }

        if(sscanf(searchString, "L1_P4_N || %d ||",&temp )==1){
             m_superLayerMap[0].insert(SuperLayerMap::value_type(P4_N,temp));
        }

        if(sscanf(searchString, "L2_P1_P || %d ||",&temp )==1){
             m_superLayerMap[1].insert(SuperLayerMap::value_type(P1_P,temp));
        }

        if(sscanf(searchString, "L2_P1_N || %d ||",&temp )==1){
             m_superLayerMap[1].insert(SuperLayerMap::value_type(P1_N,temp));
        }

        if(sscanf(searchString, "L2_P2_P || %d ||",&temp )==1){
             m_superLayerMap[1].insert(SuperLayerMap::value_type(P2_P,temp));
        }

        if(sscanf(searchString, "L2_P2_N || %d ||",&temp )==1){
             m_superLayerMap[1].insert(SuperLayerMap::value_type(P2_N,temp));
        }

        if(sscanf(searchString, "L2_P3_P || %d ||",&temp )==1){
             m_superLayerMap[1].insert(SuperLayerMap::value_type(P3_P,temp));
        }

        if(sscanf(searchString, "L2_P3_N || %d ||",&temp )==1){
             m_superLayerMap[1].insert(SuperLayerMap::value_type(P3_N,temp));
        }

        if(sscanf(searchString, "L2_P4_P || %d ||",&temp )==1){
             m_superLayerMap[1].insert(SuperLayerMap::value_type(P4_P,temp));
        }

        if(sscanf(searchString, "L2_P4_N || %d ||",&temp )==1){
             m_superLayerMap[1].insert(SuperLayerMap::value_type(P4_N,temp));
        }

    }




     SuperLayerMap::iterator superLayer_iter;
     for(int superLayerIndex = 0;superLayerIndex<2;superLayerIndex++){
          for(superLayer_iter= m_superLayerMap[superLayerIndex].begin();superLayer_iter!=m_superLayerMap[superLayerIndex].end();superLayer_iter++){
                 if(superLayer_iter->second!=0) m_uplinkId.push_back(superLayer_iter->second);
                 //printf("m_superLayerMap[%d][%d] = %d \n",superLayerIndex,superLayer_iter->first,superLayer_iter->second);
                 //getchar();
          }
     }


    TFile *file = new TFile(pathToReadInFile.c_str(),"READ");
    printf("readInFile from %s \n",pathToReadInFile.c_str());

    char tmp[256];

    TH2* histo;

    //Fill the raw pedestals
    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        sprintf(tmp,"raw_pedestals_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
        histo = (TH2*)file->Get(tmp);
        m_raw_pedestals.push_back(histo);
    }

    //Fill the PS_pedestal
    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        sprintf(tmp,"PS_pedestals_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
        histo = (TH2*)file->Get(tmp);
        m_PS_pedestals.push_back(histo);
    }

    //Fill the adcHistogram
    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        sprintf(tmp,"adcHistogram_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
        histo = (TH2*)file->Get(tmp);
        m_adcHistogram.push_back(histo);
    }

    //fill the PS_adcHistogram
    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        sprintf(tmp,"PS_adcHistogram_uplink_%03d;1",m_uplinkId.at(uplinkIndex));
        histo = (TH2*)file->Get(tmp);
        m_PS_adcHistogram.push_back(histo);
    }


   //std::vector<int> corrUplinkPairs;
    SuperLayerMap::iterator corrIter;
    for(int superLayerIndex = 0; superLayerIndex<2;superLayerIndex++){
        for(superLayer_iter= m_superLayerMap[superLayerIndex].begin();superLayer_iter->first<4;superLayer_iter++){
               if(superLayer_iter->second == 0) continue;
               corrIter = m_superLayerMap[superLayerIndex].find(superLayer_iter->first+P_N_DISTANCE);
               if(corrIter == m_superLayerMap[superLayerIndex].end()){
                    printf("Can not find its corresponding part of %d, check the setup file!\n",superLayer_iter->first);
                    assert(false);
               }else{
                    if(corrIter->second != 0){
                         m_corrUplinkPairs.push_back(superLayer_iter->second);
                         m_corrUplinkPairs.push_back(corrIter->second);
                    }
              }
         }
     }

    if(m_corrUplinkPairs.size()!=0 ){

       for(int i=0; i< (m_corrUplinkPairs.size()/2); i++){
           int Xuplink = m_corrUplinkPairs.at(2*i);
           int Yuplink = m_corrUplinkPairs.at(2*i+1);

           int corrChan = 0;
           int stripIndex = 0;

           for (Int_t i=HEAD_OF_EFFECT_CHANNELS;i<HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS;i++){
                if((i%CHANNELS_PER_GROUP)>(STRIPS_PER_GROUP-1)) continue; // only 0~11,16~27,32~43,48~59 are SiPM output
                corrChan = (i/CHANNELS_PER_GROUP)*CHANNELS_PER_GROUP+STRIPS_PER_GROUP-1-i%CHANNELS_PER_GROUP;

                int SuperLayerIndex = 0;
                int positionIndex = 0;


                for(int LayerIndex = 0;LayerIndex<2;LayerIndex++){
                     for(superLayer_iter= m_superLayerMap[LayerIndex].begin();superLayer_iter!=m_superLayerMap[LayerIndex].end();superLayer_iter++){
                          if(superLayer_iter->second==Xuplink) {
                               SuperLayerIndex = LayerIndex;
                               positionIndex = superLayer_iter->first;
                               break;
                          }
                     }
                }

                stripIndex = SuperLayerIndex*STRIPS_PER_SUPERLAYER+(positionIndex%P_N_DISTANCE)*STRIPS_PER_GROUP+(i/CHANNELS_PER_GROUP)*STRIPS_PER_MODULE+i%CHANNELS_PER_GROUP;
                //std::cout<<"chan= "<<i <<" & corrChan = "<<corrChan <<" are correlated!"<<" stripIndex = "<<stripIndex<<std::endl;
                //getchar();
                sprintf(tmp, "uplink_%d_vs_%d_____uplink%d_channel%03d_with_uplink%d_channel%03d",Xuplink,Yuplink,Xuplink,i,Yuplink,corrChan);
                histo = (TH2*)file->Get(tmp);
                m_strip_correlation.insert(std::map<int,TH2*>::value_type(stripIndex,histo));
           }
       }
    }



    for(unsigned int uplinkIndex=0;uplinkIndex<m_uplinkId.size();uplinkIndex++){
        TGraphErrors* adcPerPhoton = new TGraphErrors(1);
        m_adcPerPhoton.push_back(adcPerPhoton);

        TGraphErrors* mpvInAdc = new TGraphErrors(1);
        m_mpvInAdc.push_back(mpvInAdc);

        TGraphErrors* resultCorr = new TGraphErrors(1);
        m_resultCorr.push_back(resultCorr);

        sprintf(tmp, "area_uplink%d", m_uplinkId.at(uplinkIndex));
        TH1F* area = new TH1F(tmp,"area_for_mean_Of_MIP_Distribution;Channel;Area",NUMBER_OF_EFFECT_CHANNELS,HEAD_OF_EFFECT_CHANNELS,HEAD_OF_EFFECT_CHANNELS+NUMBER_OF_EFFECT_CHANNELS);
        m_area.push_back(area);

        TGraphErrors* meanPhoton = new TGraphErrors(1);
        m_meanInPhoton.push_back(meanPhoton);

    }

}


uint16_t DataManager::GetChannelIndexOfSipm128(uint16_t uplinkID, uint16_t channelIndexOnVata64Chip){
    uint16_t ch_Sipm128 = 0;
    if((_trackerModuleVataBoardA!=0)&&(uplinkID == _trackerModuleVataBoardA)){
        ch_Sipm128 = (channelIndexOnVata64Chip%CHANNELS_PER_HALF_CONNECTOR)*8+(channelIndexOnVata64Chip/CHANNELS_PER_HALF_CONNECTOR)*4+(channelIndexOnVata64Chip/CHANNELS_PER_CONNECTOR)*2+1-(channelIndexOnVata64Chip/CHANNELS_PER_CONNECTOR)*8;
//        printf("boardA: uplinkID = %d, channelIndexOnVata64Chip = %d,ch_Sipm128 =%d\n",uplinkID,channelIndexOnVata64Chip,ch_Sipm128);
//        getchar();
        return ch_Sipm128;
    }else if((_trackerModuleVataBoardB!=0)&&(uplinkID == _trackerModuleVataBoardB)){
        ch_Sipm128 = (channelIndexOnVata64Chip%CHANNELS_PER_HALF_CONNECTOR)*8+(channelIndexOnVata64Chip/CHANNELS_PER_HALF_CONNECTOR)*4+(channelIndexOnVata64Chip/CHANNELS_PER_CONNECTOR)*2+2-(channelIndexOnVata64Chip/CHANNELS_PER_CONNECTOR)*8;
//        printf("boardB:uplinkID = %d, channelIndexOnVata64Chip = %d,ch_Sipm128 =%d\n",uplinkID,channelIndexOnVata64Chip,ch_Sipm128);
//        getchar();
        return ch_Sipm128;
    }else{
        printf("Error: DataManager::GetChannelIndexOfSipm128(uint16_t, uint16_t) can not generate channelIndex correctly! _trackerModuleVataBoardA = %d, _trackerModuleVataBoardB = %d\n",_trackerModuleVataBoardA,_trackerModuleVataBoardB);
        assert(false);
    }
}


uint16_t DataManager::GetTrackerModuleVataBoardAUplinkID(){
    return _trackerModuleVataBoardA;
}

uint16_t DataManager::GetTrackerModuleVataBoardBUplinkID(){
    return _trackerModuleVataBoardB;
}


bool DataManager::GenerateTrackModulePedSubHisto(std::string mapFile){

    // read set up from the setupFile
        std::ifstream setupFile(mapFile.c_str());
        if(!setupFile){
            std::cerr << "Failure: could not open file: \"" << mapFile << "\"." << std::endl;
            std::cerr << "Please check if the path is correct or not!" << std::endl;
            assert(false);
        }

        std::string s;
        int temp;
        while (true) {
            std::getline(setupFile, s);
            if (setupFile.eof())
                break;
            const char* searchString = s.c_str();

            if (s.find("#") == 0 || s=="") {
                continue; // Skip commented lines
            }

            if(sscanf(searchString, "VATA_BOARD_A || %d ||",&temp )==1){
                _trackerModuleVataBoardA = temp;
            }

            if(sscanf(searchString, "VATA_BOARD_B || %d ||",&temp )==1){
                _trackerModuleVataBoardB = temp;
            }
         }
        printf("DataManager::GenerateTrackModulePedSubHisto :  _trackerModuleVataBoardA = %d, _trackerModuleVataBoardB =%d\n",_trackerModuleVataBoardA,_trackerModuleVataBoardB);
    if(_trackerModuleVataBoardA==0 || _trackerModuleVataBoardB==0){
        printf("error:  DataManager::GenerateTrackModulePedSubHisto()---_trackerModuleVataBoardA or _trackerModuleVataBoardB is not set! _trackerModuleVataBoardA = %d,_trackerModuleVataBoardB = %d",_trackerModuleVataBoardA,_trackerModuleVataBoardB);
        return false;
    }

    //initialize histos
    char tmp[256];
    sprintf(tmp,"trackModule_A=uplink%d_B=uplink%d",_trackerModuleVataBoardA,_trackerModuleVataBoardB);
    _trackerModulePedSubHisto = new TH2I(tmp,tmp,128,1,129,maxAdcValue+500,-500,maxAdcValue);

    //fill Histos
    for(uint8_t uplinkIndex = 0; uplinkIndex<m_uplinkId.size();uplinkIndex++){
        if(m_uplinkId.at(uplinkIndex)==_trackerModuleVataBoardA  || m_uplinkId.at(uplinkIndex)==_trackerModuleVataBoardB){
            for(uint16_t binX=1;binX<64+1;binX++){
                 for(int16_t binY =-500; binY<maxAdcValue+1;binY++){
                      uint16_t chan_Sipm128 = GetChannelIndexOfSipm128(m_uplinkId.at(uplinkIndex),binX-1);
                     // if(binY ==0){ printf("chan_vata = %d, chan_sipm = %d\n",binX-1,chan_Sipm128); getchar();}
                      uint16_t binX_Sipm128 = chan_Sipm128;
                      uint16_t bin_cont = m_PS_adcHistogram.at(uplinkIndex)->GetBinContent(binX,binY);
                      _trackerModulePedSubHisto->SetBinContent(binX_Sipm128,binY,bin_cont);
                 }
            }
        }
    }

    TCanvas* canvas = new TCanvas("TrackerModuleHisto_A_B_Sipm128","TrackerModuleHisto_A_B_Sipm128",0,0,1200,900);
    canvas->Divide(1,3);

    for(uint8_t uplinkIndex = 0; uplinkIndex<m_uplinkId.size();uplinkIndex++){
        if(m_uplinkId.at(uplinkIndex)==_trackerModuleVataBoardA){
            canvas->cd(1);
           // printf("m_uplinkId.at(%d) = %d for boardA\n",uplinkIndex,m_uplinkId.at(uplinkIndex));
            m_PS_adcHistogram.at(uplinkIndex)->Draw("COLZ");
        }

        if(m_uplinkId.at(uplinkIndex)==_trackerModuleVataBoardB){
            canvas->cd(2);
           // printf("m_uplinkId.at(%d) = %d for boardB\n",uplinkIndex,m_uplinkId.at(uplinkIndex));
            m_PS_adcHistogram.at(uplinkIndex)->Draw("COLZ");
        }
    }
    canvas->cd(3);
    _trackerModulePedSubHisto->Draw("COLZ");

    TFile* resultFile = new TFile("../data/trackerModuleResult.root","RECREATE");
    _trackerModulePedSubHisto->Write();
    resultFile->Close();
    return true;

}


void DataManager::PlotLedResult_InSiPM128(){
    char tmp[256];

    sprintf(tmp," Analysis Result in SiPM128");
    TCanvas *c1 = new TCanvas(tmp,tmp,1280,1280);

    c1->Divide(1,2);


    gStyle->SetOptFit(1000);
    gStyle->SetLabelFont(132,"XY");
    gStyle->SetLabelSize(0.06,"XY");

    gStyle->SetTitleFont(132,"XY");
    gStyle->SetTitleSize(0.06,"XY");
    gStyle->SetTitleYOffset(0.8);
    gStyle->SetTitleXOffset(0.8);




    TGraphErrors* graph = NULL;
    TH1F *hr = NULL;
    TLegend *legend =NULL;
    TGraphErrors* graph_omitDamagedChannels = NULL;


    //draw ADC per Photon
    std::cout<<"\n\n\ndraw adcPerPhoton \n";
    c1->cd(1);

    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    int index_new =0;
    graph_omitDamagedChannels = new TGraphErrors(1);
    for(unsigned int i=0; i<m_uplinkId.size();i++){
        //fill a new graph with arranged channel index of SiPM128
        if(m_uplinkId.at(i)==_trackerModuleVataBoardA || m_uplinkId.at(i)==_trackerModuleVataBoardB){
              graph = m_adcPerPhoton.at(i);
              double chan=0;
              double getAdcPerPhoton =0;
              int chan_SiPM128 = 0;
              for(int index = 0; index<NUMBER_OF_EFFECT_CHANNELS; index++){
                   graph->GetPoint(index,chan,getAdcPerPhoton);
                   std::cout<<"index= "<<index<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",getAdcPerPhoton = "<<getAdcPerPhoton<<std::endl;
                   if(getAdcPerPhoton<ADC_PER_PHOTON_UPPER_THRESHOLD) {
                        chan_SiPM128 = GetChannelIndexOfSipm128(m_uplinkId.at(i),chan);
                        graph_omitDamagedChannels->SetPoint(index_new,chan_SiPM128,getAdcPerPhoton);
                        std::cout<<"index_new= "<<index_new<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",getAdcPerPhoton = "<<getAdcPerPhoton<<",chan_SiPM128 = "<<chan_SiPM128<<std::endl;
                        index_new++;
                    }
              }
         }
    }

    //Draw the graph
    sprintf(tmp,"A_uplink%d,B_uplinkID%d",_trackerModuleVataBoardA,_trackerModuleVataBoardB);
    legend->AddEntry(graph_omitDamagedChannels,tmp, "p");
    hr = c1->cd(1)->DrawFrame(1,8,128,80);
    hr->SetXTitle("channel_SiPM #");
    hr->SetYTitle("gain(ADC per Photon), ADC");


    graph_omitDamagedChannels->SetMarkerStyle(20);
    graph_omitDamagedChannels->SetMarkerColor(2);
    graph_omitDamagedChannels->SetMarkerSize(1.);

    graph_omitDamagedChannels->Fit("pol1");
    graph_omitDamagedChannels->GetFunction("pol1")->SetLineColor(2);
    graph_omitDamagedChannels->Draw("PE");


    c1->cd(1)->SetGridx();
    c1->cd(1)->SetGridy();
    legend->Draw();


    //draw meanInPhoton
    std::cout<<"\n\n\ndraw meaninPhoton \n";
    c1->cd(2);
    legend = new TLegend(0.75,0.94,0.95,0.68,"","NDC"); //position and formatting
    legend->SetFillColor(kWhite);
    legend->SetBorderSize(0);
    legend->SetTextFont(132);
    legend->SetTextSize(0.06);

    graph_omitDamagedChannels = new TGraphErrors(1);
    index_new = 0;
    for(unsigned int i=0; i<m_uplinkId.size();i++){

        //fill a new graph with arranged channel index of SiPM128
         if(m_uplinkId.at(i)==_trackerModuleVataBoardA || m_uplinkId.at(i)==_trackerModuleVataBoardB){
               graph = m_meanInPhoton.at(i);

               double chan=0;
               double getMeanInPhoton =0;
               uint16_t chan_SiPM128 = 0;

               for(int index = 0; index<NUMBER_OF_EFFECT_CHANNELS; index++){
                   graph->GetPoint(index,chan,getMeanInPhoton);
                   std::cout<<"index= "<<index<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<",meanInPhoton = "<<getMeanInPhoton<<std::endl;
                   if(getMeanInPhoton>MEAN_IN_PHOTON_UNDER_THRESHOLD) {
                       chan_SiPM128 = GetChannelIndexOfSipm128(m_uplinkId.at(i),chan_SiPM128);
                       graph_omitDamagedChannels->SetPoint(index_new,chan_SiPM128,getMeanInPhoton);
                       std::cout<<"index_new= "<<index_new<<",chan = "<<chan<<",uplinkId = "<<m_uplinkId.at(i)<<chan<<",meanInPhoton = "<<getMeanInPhoton<<" chan_SiPM128 = "<<chan_SiPM128<<std::endl;
                       index_new++;
                   }
               }
           }
     }
     sprintf(tmp,"A_uplink%d,B_uplinkID%d",_trackerModuleVataBoardA,_trackerModuleVataBoardB);
     legend->AddEntry(graph_omitDamagedChannels,tmp, "p");

     hr = c1->cd(2)->DrawFrame(1,0,128,20);
     hr->SetXTitle("channel_SiPM128 #");
     hr->SetYTitle("mean,p.e.");

     graph_omitDamagedChannels->SetMarkerStyle(20);
     graph_omitDamagedChannels->SetMarkerColor(2);
     graph_omitDamagedChannels->SetMarkerSize(1.);

     graph_omitDamagedChannels->Fit("pol1");
     graph_omitDamagedChannels->GetFunction("pol1")->SetLineColor(2);

     graph_omitDamagedChannels->Draw("PE");

     c1->cd(2)->SetGridx();
     c1->cd(2)->SetGridy();
     legend->Draw();

}


void DataManager::FindPeaks() {
	
	std::cout << "********** FindPeaks **********" << std::endl;
	
	TSpectrum * s = new TSpectrum();
	TCanvas * c = new TCanvas();
	TH1D * hist = m_PS_adcHistogram[0]->ProjectionY("ch0_py",1,1);
	hist->Draw();
	s->Search(hist,0.001,"new",0.1);
	
	
	const int npeaks = s->GetNPeaks();
	std::cout << "Npeaks: " << npeaks << std::endl;
	
	Float_t *xpeaks = s->GetPositionX();
	for (int i = 0; i < npeaks; i++) {
		
		std::cout << xpeaks[i] << std::endl;
		
	}
	
	return;
}

void DataManager::SaveADCHistos(TString file_name){
    
    TFile* recordFile =  new TFile(file_name.Replace(file_name.Length()-5,10,"_Yprj.root"),"RECREATE");
	    
    for(unsigned int i=0;i<m_uplinkId.size();++i){
        
        for(unsigned int bin=0 ; bin < m_PS_adcHistogram[0]-> GetNbinsX() ; ++bin) {
        
			std::ostringstream convert;   		// stream used for the conversion int -> string
			convert << bin;      				// insert the textual representation of 'int' in the characters in the stream
			TString channel_number = convert.str();
        
			TH1D * hist = m_PS_adcHistogram[0]->ProjectionY("ch"+channel_number+"_py",bin+1,bin+1);
			hist->Write();		
		
		}
    
    }

    recordFile->Close();
	
	return;
}

