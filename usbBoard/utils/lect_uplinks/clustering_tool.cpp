#include "DataManager.h"
#include <Settings.h>

#include <assert.h>
#include <iostream>
using namespace std;

#include <MergedEvent.h>
#include <UsbBoardConfiguration.h>


#include <TH1.h>
#include <TH2.h>
#include <TH2F.h>
#include <TF1.h>
#include <TH3.h>
#include <TProfile.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TMath.h>
#include <TObjArray.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TGraphSmooth.h>

#include <TRandom.h>
#include <TSpectrum.h>
#include <TVirtualFitter.h>
#include <TROOT.h>
#include <TStyle.h>

#define CANVAS_X  1400
#define CANVAS_Y  1000

void DataManager::ConfigHistoTH1(TH1* Histo, TString Title, TString axeXname, TString axeYname, Double_t axeXmin, Double_t axeXmax, double axeYmin, double axeYmax, double LineWidth)
{
    if(LineWidth) Histo->SetLineWidth(LineWidth); // 1 or 2
    if(Title != "") Histo->SetTitle(Title);
    if(Title != "") Histo->GetXaxis()->SetTitle(axeXname);
    if(Title != "") Histo->GetYaxis()->SetTitle(axeYname);
    if(axeXmax) Histo->SetAxisRange(axeXmin, axeXmax, "x");
    if(axeYmax) Histo->SetAxisRange(axeYmin, axeYmax, "y");

    Histo->SetTitleSize(0.06, "x");
    Histo->SetTitleSize(0.06, "y");
    Histo->SetTitleOffset(1, "x");
    Histo->SetTitleOffset(1, "y");
}
void DataManager::ConfigHistoTH2(TH2* Histo, TString Title, TString axeXname, TString axeYname, Double_t axeXmin, Double_t axeXmax, double axeYmin, double axeYmax)
{
    if(Title != "") Histo->SetTitle(Title);
    if(Title != "") Histo->GetXaxis()->SetTitle(axeXname);
    if(Title != "") Histo->GetYaxis()->SetTitle(axeYname);
    if(axeXmax) Histo->SetAxisRange(axeXmin, axeXmax, "x");
    if(axeYmax) Histo->SetAxisRange(axeYmin, axeYmax, "y");

    Histo->SetTitleSize(0.06, "x");
    Histo->SetTitleSize(0.06, "y");
    Histo->SetTitleOffset(1, "x");
    Histo->SetTitleOffset(1, "y");
}

void DataManager::drawHisto_Cluster()
{
    std::cout << "*****>>> DataManager::" << __func__ << "() *****" << std::endl;
    char tmp[256];
    int Xmax;

    sprintf(tmp,"Histogram clusterization 1");
    TCanvas* canvas = new TCanvas(tmp,tmp,0,0,CANVAS_X, CANVAS_Y);
    canvas->Divide(1,2);

    canvas->cd(1);
    m_NbCluster[0]->Draw("");
    Xmax = m_NbCluster[0]->FindLastBinAbove(0, 1); // (thrsd, 1=Xaxis)
    ConfigHistoTH1(m_NbCluster[0], "Number of cluster per event", "Number of cluster per event","Number of Entries", 0, Xmax+5, 0, 0, 2);

    canvas->cd(2);
    m_ClusterSize[0]->Draw("");
    Xmax = m_ClusterSize[0]->FindLastBinAbove(0, 1);
    ConfigHistoTH1(m_ClusterSize[0], "Cluster size per event", "Cluster size per event","Number of Entries", 0, Xmax+1, 0, 0, 2);

    sprintf(tmp,"Histogram clusterization 2");
    TCanvas* canvas2 = new TCanvas(tmp,tmp,0,0,CANVAS_X, CANVAS_Y);
    canvas2->Divide(1,2);

    canvas2->cd(1);
    m_ClusterADC[0]->Draw("");
    Xmax = m_ClusterADC[0]->FindLastBinAbove(2, 1);
    ConfigHistoTH1(m_ClusterADC[0], "ClusterADC per cluster", "ClusterADC per cluster","Number of Entries", 0, Xmax, 0, 0, 1);

    canvas2->cd(2);
    m_ClusterChannel[0]->Draw("");
     ConfigHistoTH1(m_ClusterChannel[0], "Cluster channel per cluster", "Cluster channel per cluster","Number of Entries", 0, 128, 0, 0, 1);
}

void DataManager::drawHisto_CMS(int ChannelNr)
{
    std::cout << "*****>>> DataManager::" << __func__ << "() *****" << std::endl;
    TH1 *hE = NULL;
    char tmp[256];
    int Xmax;

    TCanvas* canvas = NULL;   //   TCanvas* canvas2 = NULL;

    for(unsigned int i=0;i<m_uplinkId.size();++i)
    {
        sprintf(tmp,"Histogram CMS ADC Uplink%d", m_uplinkId.at(i));
        canvas = new TCanvas(tmp,tmp,0,0,CANVAS_X, CANVAS_Y);
        canvas->Divide(1,3);

        canvas->cd(1);
        m_signalADC_CMS[i]->Draw("COLZ");
 //       mean = m_raw_pedestals[i]->GetMean(2);      rms = m_raw_pedestals[i]->GetRMS(2);
 //       m_signalADC_CMS[i]->GetYaxis()->SetRangeUser(mean - 10*rms, mean + 10*rms);
        sprintf(tmp, "SignalADC CMS  U%d", m_uplinkId.at(i));                                       // MAY CRASH HERE !!!
        ConfigHistoTH2(m_signalADC_CMS[i], "", "channel","adcValue", 0, 0, 0, 2000);   // tmp
        canvas->GetPad(1)->SetLogz();

         canvas->cd(2);
        TH2 *ADC_CMS_1chan_Hist = m_signalADC_CMS.at(uplinkIndex(m_uplinkId.at(i)));
        hE = ADC_CMS_1chan_Hist->ProjectionY(tmp,ChannelNr,ChannelNr,"");
        sprintf(tmp,"SignalADC CMS channel%d U%d", ChannelNr, m_uplinkId.at(i));
        Xmax = hE->FindLastBinAbove(0, 1);
        ConfigHistoTH1(hE, tmp, "adcValue","Number of Entries", 0, Xmax, 0, 0, 1);
        hE->DrawCopy();  canvas->Update();

        canvas->cd(3);
        m_CMS_val[i]->Draw("");
        sprintf(tmp, "CMS val U%d", m_uplinkId.at(i));
        Xmax = m_CMS_val[0]->FindLastBinAbove(1, 1);
        ConfigHistoTH1(m_CMS_val[i], tmp, "CMSvalue","Number of Entries", 1, Xmax, 0, 0, 2);
    }
}

void DataManager::draw_signalADC_1event(int eventADC[], int EventNr[])
{
    std::cout << "*****>>> DataManager::" << __func__ << "() *****" << std::endl;
    TGraph *grin, *grout;
    TCanvas *vC1;
    char tmp[256];
    int x[m_NumberChannels];
    int eventADCtoPlot[m_NumberChannels];

    for(int i=0; i<m_NumberChannels; i++) x[i]=i;

    sprintf(tmp,"Plot all channels for 1 event"); //  m_uplinkId.at(0)
    vC1 = new TCanvas(tmp,tmp,0,0,CANVAS_X, CANVAS_Y);
    vC1->Divide(2,2);

    for(int i=0; i<4; i++)
    {
        vC1->cd(i+1);

        for(int j=0; j<m_NumberChannels; j++)  eventADCtoPlot[j] = *(eventADC+j+m_NumberChannels*i);

        // initialize graph with data
        grin = new TGraph(m_NumberChannels,x,eventADCtoPlot);
        TGraphSmooth *gs = new TGraphSmooth("normal");
        grout = gs->Approx(grin,"linear");
    
        TH1F *vFrame = gPad->DrawFrame(0,-50,m_NumberChannels,1000); // xmin, ymin, xmax, ymax

        sprintf(tmp,"SignalADC PedSub reordered for event%d", *(EventNr+i)); // m_uplinkId.at(0)
        vFrame->SetTitle(tmp); vFrame->SetTitleSize(0.04); vFrame->SetYTitle("signalADC"); vFrame->SetXTitle("Channel");
        grin->SetMarkerColor(kRed);        grin->SetMarkerStyle(5);  grin->SetMarkerSize(0.7); grin->Draw("P"); grout->DrawClone("LP");
    }
}

void DataManager::drawHisto_1chan(int ChannelNr)
{
    std::cout << "*****>>> DataManager::" << __func__ << "() *****" << std::endl;
   // TH1I *h1 = new TH1I("h1", "h1 title", 1000, 0, 1000);
    TH1 *hE = NULL;
    char tmp[256];
    TCanvas* canvas = NULL;
    int Xmax;

    for(unsigned int i=0;i<m_uplinkId.size();++i)
    {
        sprintf(tmp,"Histogram 1chan Uplink%d channel%d",m_uplinkId.at(i), ChannelNr);
        canvas = new TCanvas(tmp,tmp,0,0,CANVAS_X, CANVAS_Y);
        canvas->Divide(1,3);

        canvas->cd(1);
        TH2 *adcHistogram_Hist = m_adcHistogram.at(uplinkIndex(m_uplinkId.at(i))); // uplinkId
        int mean = m_adcHistogram[i]->GetMean(2);        int rms = m_adcHistogram[i]->GetRMS(2);
        m_adcHistogram[i]->GetYaxis()->SetRangeUser(0, mean + 10*rms); // overwrite the previous range to start from 0
        sprintf(tmp,"SignalADC raw channel%d U%d", ChannelNr, m_uplinkId.at(i));
        hE = adcHistogram_Hist->ProjectionY(tmp,ChannelNr,ChannelNr,"");
        Xmax = hE->FindLastBinAbove(0, 1);
        ConfigHistoTH1(hE, tmp, "adcValue","Number of Entries", 0, Xmax, 0, 0, 0);
        hE->DrawCopy(); canvas->Update();

        canvas->cd(2);
        TH2 *signalADC_withThrsd_Hist = m_signalADC_withThrsd.at(uplinkIndex(m_uplinkId.at(i))); // uplinkId
        sprintf(tmp,"SignalADC after 1st thrsd channel%d U%d", ChannelNr, m_uplinkId.at(i));
        hE = signalADC_withThrsd_Hist->ProjectionY(tmp,ChannelNr,ChannelNr,"");
        Xmax = hE->FindLastBinAbove(0, 1);
        ConfigHistoTH1(hE, tmp, "adcValue","Number of Entries", 1, Xmax, 0, 0, 0);
        hE->DrawCopy(); canvas->Update();

        canvas->cd(3);
        TH2 *signalADC_withThrsd_pedSub_Hist = m_signalADC_withThrsd_pedSub.at(uplinkIndex(m_uplinkId.at(i))); // uplinkId
        sprintf(tmp,"SignalADC after PedSub channel%d U%d", ChannelNr, m_uplinkId.at(i));
        hE = signalADC_withThrsd_pedSub_Hist->ProjectionY(tmp,ChannelNr,ChannelNr,"");
        Xmax = hE->FindLastBinAbove(0, 1);
        ConfigHistoTH1(hE, tmp, "adcValue","Number of Entries", 1, Xmax, 0, 0, 0);
        hE->DrawCopy(); canvas->Update();
    }
}


void DataManager::Cluster_Search(int eventADC[], int SeedCutADC, int EventNr)
{
    int NbClusterMax = 200;  // was 30 giovanni

    int NbCluster = 0;
    int ClusterSize[NbClusterMax];
    int ClusterADC[NbClusterMax];
    double ClusterChannel[NbClusterMax];
    bool ClusterOpen = false;

    for(int i=0; i<NbClusterMax; i++)
    {
        ClusterSize[i] = 0;
        ClusterADC[i] = 0;
        ClusterChannel[i] = 0;
    }

    if(EventNr == 1)
        cout << "Cluster for the first event: " << endl;

    for(int i=0; i<m_NumberChannels; i++)
    {
        // Find a new cluster or add a hit to a cluster
        if(eventADC[i] >= SeedCutADC)
        {
            ClusterSize[NbCluster]++;
            ClusterADC[NbCluster] += eventADC[i];
            ClusterChannel[NbCluster] += eventADC[i]*i;
            ClusterOpen = true;
            if(EventNr == 1) cout << i << "[" <<  eventADC[i] << "] ";
        }
        else
        {   // end of the cluster or no cluster open
            if(ClusterOpen)
            {
                if(EventNr == 1) cout << " => " << ClusterChannel[NbCluster]/ClusterADC[NbCluster];
                NbCluster++;
                ClusterOpen = false;
                if(EventNr == 1) cout<<endl;
            }
        }
    }
    // If the last chan is in a cluster, close the current cluster
    if(ClusterOpen)
    {
        if(EventNr == 1) cout << " => " << ClusterChannel[NbCluster]/ClusterADC[NbCluster];
        NbCluster++;
    }

        m_NbCluster[0]->Fill(NbCluster);

        //giovanni  uncomment these lines for having the localization of the cells fired with number of ADC counts (note the cuts on seed and ADC)
        /*      if(NbCluster == 2) {
                  for(int jj=0; jj<128; ++jj) printf("%5d",eventADC[jj]);
                  printf("\nOK?\n");
                  char ans[8];
                  scanf("%s",&ans[0]);
              }
      */
//        if(ClusterSize[1] > 5) {
//                  for(int jj=0; jj<128; ++jj) printf("%5d",eventADC[jj]);
//                  printf("\nOK?\n");
//                  char ans[8];
//                  scanf("%s",&ans[0]);
//              }

            for(int j=0; j<NbClusterMax; j++)
        {
            if((ClusterSize[j] > 0) || (j==0))
                m_ClusterSize[0]->Fill(ClusterSize[j]);

            if((ClusterADC[j] > 0)) // || (j==0)
            {
                if(NbCluster == 1) // was commented giovanni
                {//to comment with 274
                m_ClusterADC[0]->Fill(ClusterADC[j]);
                }// to comment with 274
                m_ClusterChannel[0]->Fill(ClusterChannel[j]/ClusterADC[j]);
            }
        }

         if(EventNr == 1) cout<< "NbCluster=" << NbCluster << endl << endl;

   }

void DataManager::ReorderChannels_Sipm128(int eventADC[])
{
 /*
    Settings* settings = Settings::instance();
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
    int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);    unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
    unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
    unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;
*/
    uint16_t ch_Sipm128 = 0;
    int eventADCcopy[m_NumberChannels];
    for(int i=0; i<m_NumberChannels ; i++) *(eventADCcopy+i) = *(eventADC+i);

    for(int i=0; i<m_NumberChannels ; i++)
    {
//        if((0 <= i) && (i <= 63))
//            ch_Sipm128 = (i%CHANNELS_PER_HALF_CONNECTOR)*8+(i/CHANNELS_PER_HALF_CONNECTOR)*4+(i/CHANNELS_PER_CONNECTOR)*2+1-(i/CHANNELS_PER_CONNECTOR)*8;
//        if((64 <= i) &&  (i <= 127))
//            ch_Sipm128 = (i%CHANNELS_PER_HALF_CONNECTOR)*8+(i/CHANNELS_PER_HALF_CONNECTOR)*4+(i/CHANNELS_PER_CONNECTOR)*2+2-(i/CHANNELS_PER_CONNECTOR)*8;
          if((0 <= i) && (i <= 63)){
                   if(m_uplinkId.at(0)== _trackerModuleVataBoardA){
                            ch_Sipm128 = GetChannelIndexOfSipm128(_trackerModuleVataBoardA,i);
                   }else{
                            ch_Sipm128 = GetChannelIndexOfSipm128(_trackerModuleVataBoardB,i);
                   }
          }
          if((64 <= i) &&  (i <= 127)){
              if(m_uplinkId.at(1)== _trackerModuleVataBoardA){
                       ch_Sipm128 = GetChannelIndexOfSipm128(_trackerModuleVataBoardA,i-64);
              }else{
                       ch_Sipm128 = GetChannelIndexOfSipm128(_trackerModuleVataBoardB,i-64);
              }
          }

        *(eventADC+ch_Sipm128) = *(eventADCcopy+i);
    }
}

// !!! The CMS value is different for each uplink.
//void DataManager::CommonModeSuppression(int eventADCin[], int eventADCout[], double CMS_thrsd_in_pe, int EventNr) //double CMS_thrsd_in_pe
//{
//  Settings* settings = Settings::instance();
//  std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
//  std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
//  int CMS_sum, Nb_signalADC_underThrsd;

//  if(EventNr == 2) cout << "CMS value for the 100 first event (from ev2): ";
//  for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i)
//  {
//    CMS_sum = 0; Nb_signalADC_underThrsd = 0;

//    int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);    unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
//    //unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
//    // unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;

//    for (unsigned short sample = 0; sample < sampleSize; ++sample)
//        if(*(eventADCin+sample+i*64) < CMS_thrsd_in_pe)
//        {
//       //     if(EventNr == 2) cout << *(eventADCin+sample+i*64) << "* ";
//            CMS_sum += *(eventADCin+sample+i*64);
//            Nb_signalADC_underThrsd++;
//        }

//    if((EventNr>1) && (EventNr<100)) cout << CMS_sum;
//    m_CMS_val[i]->Fill(CMS_sum);  // Maybe better to read the val after the division
//    CMS_sum = (CMS_sum + sampleSize/2)/sampleSize; // 64     Divide by the nbr of chan of 1 uplink, +32 to round up
//    //  ???or need to divide by the nnr of chan under the thrsd???  Nb_signalADC_underThrsd  !!!if Nb_signalADC_underThrsd = 0

//    if((EventNr>1) && (EventNr<100)) cout << "(" << CMS_sum << ") ";

//        for (unsigned short sample = 0; sample < sampleSize; ++sample) // 64
//        {
//            if((*(eventADCin+sample+i*64) - CMS_sum) < 0)
//                *(eventADCout+sample+i*64) = 0;
//            else
//                *(eventADCout+sample+i*64) = *(eventADCin+sample+i*64) - CMS_sum;

//            m_signalADC_CMS[i]->Fill(sample, *(eventADCout+sample+i*64));
//        }
//  }
//  if((EventNr>1) && (EventNr<100)) cout << "*";
//  if(EventNr == 100) cout << endl << endl;
//}

void DataManager::signalADC_PedPeakSub_PedSub(int eventADCin[], int eventADCout[], int Ped_Peak_Thrsd_inADC)
{
  Settings* settings = Settings::instance();
  std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
  std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();

  for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i)
  {
      int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);    unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
 //     unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
 //     unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;    const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);

    for (unsigned short sample = 0; sample < sampleSize; ++sample)
    {
   //     cout << sample << "-" << sample+i*64 << " "; 
        if((*(eventADCin+sample+i*64)-m_pedestal_Mean.at(i)[sample]) > Ped_Peak_Thrsd_inADC)
        {
            m_signalADC_withThrsd[i]->Fill(sample, *(eventADCin+sample+i*64));
            m_signalADC_withThrsd_pedSub[i]->Fill(sample, *(eventADCin+sample+i*64)-m_pedestal_Mean.at(i)[sample]);
            *(eventADCout+sample+i*64) = *(eventADCin+sample+i*64)  - m_pedestal_Mean.at(i)[sample];

        }
        else
        {
            m_signalADC_withThrsd[i]->Fill(sample, 0);
            m_signalADC_withThrsd_pedSub[i]->Fill(sample, 0);
            *(eventADCout+sample+i*64) = 0;
        }

        m_adcHistogram[i]->Fill(sample , *(eventADCin+sample+i*64));       // was done is get_signal();
        m_PS_adcHistogram[i]->Fill(sample ,  *(eventADCin+sample+i*64)-m_pedestal_Mean.at(i)[sample]);   // was done is get_signal();
    }
  }
}
void DataManager::Get_RawSignalADCdata_1event(MergedEvent* event, int eventADC[])
{
    Settings* settings = Settings::instance();
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();

    for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i)
    {
        int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);    unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
        //unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
        // unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;
        const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);

        for (unsigned short sample = 0; sample < sampleSize; ++sample)
            *(eventADC+sample+i*64) = uplinkData.adcValue(sample);
    }
}

int DataManager::Get_NumberChannels()
{
    Settings* settings = Settings::instance();
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();

    m_NumberChannels = 0;
    for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i)
    {
        int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
        unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
        unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;

        m_NumberChannels += (numberOfReadoutChips*channelsPerReadoutChip);   // sampleSize???
            cout << "sampleSize=" << sampleSize << "     channelsPerReadoutChip=" << channelsPerReadoutChip << "     numberOfReadoutChips=" << numberOfReadoutChips << endl;
    }

    cout << "m_NumberChannels=" << m_NumberChannels << endl << endl;
    // numberOfReadoutChips=1 channelsPerReadoutChip=64 sampleSize=64   for 2 uplink

    return m_NumberChannels;
}




