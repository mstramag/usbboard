#include "LinearityPlots.h"

#include <math.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <TMultiGraph.h>
#include <TList.h>
#include <TH1.h>
#include <TH2.h>
#include <TPaveText.h>

#include <iostream>
#include <fstream>
#include <cstdlib>

LinearityPlots::LinearityPlots(const int boardNumber, const std::vector<int>& chipIds)
    : m_nAmplitudes(0)
    , m_chipIds(chipIds), m_boardNumber(boardNumber)
{
}

LinearityPlots::~LinearityPlots()
{
}

int LinearityPlots::calibratedAmplitude(int ampl)
{
    int n = 29;

    int p[][2] = {
        {   0,    0},
        {  50,   43},
        {  75,   72},
        { 100,  100},
        { 125,  127},
        { 150,  152},
        { 175,  177},
        { 200,  201},
        { 225,  224},
        { 250,  248},
        { 275,  271},
        { 300,  295},
        { 325,  319},
        { 350,  344},
        { 375,  370},
        { 400,  398},
        { 500,  499},
        { 600,  601},
        { 700,  700},
        { 800,  798},
        { 900,  899},
        {1000, 1001},
        {1100, 1100},
        {1200, 1201},
        {1300, 1301},
        {1400, 1400},
        {1500, 1500},
        {1600, 1599},
        {1700, 1699},
        {1800, 1792},
    };
    if (ampl < p[0][0] || p[n-1][0] < ampl) {
        fprintf(stderr, "WARNING: call for not calibrated amplitude %dmV\n", ampl);
        return ampl;
    }
    int i = 0;
    for (; i < n ; ++i) {
        if (p[i][0] >= ampl)
            break;
    }
    if (p[i][0] == ampl) {
        return p[i][1];
    } else {
        fprintf(stderr, "WARNING: call for not calibrated amplitude %dmV\n", ampl);
        //TODO interpolate.
        return ampl;
    }
}

void LinearityPlots::analyze(const std::vector<int>& amplitudes, const std::vector<TH2*>& adcHistograms)
{
    m_nAmplitudes = amplitudes.size();
/*    for (int ch = 0; ch < nCh; ++ch) {
        char rootName[128];

        TGraphErrors* meanValue = new TGraphErrors;
        sprintf(rootName, "linearity%03d", ch);
        meanValue->SetTitle(rootName);
        m_meanValues.push_back(meanValue);

        TGraphErrors* meanResidual = new TGraphErrors;
        sprintf(rootName, "residual%03d", ch);
        meanResidual->SetTitle(rootName);
        m_meanResiduals.push_back(meanResidual);

        sprintf(rootName, "linearityFunction_%03d", ch);
        m_meanValueFits.push_back(new TF1(rootName, "pol1"));
    }

    for (int ch = 0; ch < nCh; ++ch) {
        for (int i = 0; i < m_nAmplitudes; ++i) {
            TH1* pulseHistogram = adcHistograms[i]->ProjectionY("_px", ch + 1, ch + 1);
            float mean = pulseHistogram->GetMean();
            float rms = pulseHistogram->GetRMS();
            int nPoints = m_meanValues[i]->GetN();
            //m_meanValues[ch]->SetPoint(nPoints, calibratedAmplitude(amplitudes[i]), mean);
			m_meanValues[ch]->SetPoint(nPoints, amplitudes[i], mean);
            m_meanValues[ch]->SetPointError(nPoints, 0, rms / (sqrt(pulseHistogram->GetEntries() - 1)));
        }
    }
*/
	
	for (int ch = 0; ch < nCh; ++ch) {
        char rootName[128];

        TGraphErrors* meanValue = new TGraphErrors;
        sprintf(rootName, "linearity%03d", ch);
        meanValue->SetTitle(rootName);
        meanValue->SetName(rootName);
        meanValue->SetLineColor(kBlue);
        meanValue->SetMarkerColor(kBlue);
        m_meanValues.push_back(meanValue);

        TGraphErrors* meanResidual = new TGraphErrors;
        sprintf(rootName, "residual%03d", ch);
        meanResidual->SetTitle(rootName);
        meanResidual->SetName(rootName);
        m_meanResiduals.push_back(meanResidual);

        sprintf(rootName, "linearityFunction_%03d", ch);
        TF1* f = new TF1(rootName, "pol1");
        f->SetLineColor(kRed);
        f->SetLineWidth(1);
        f->SetLineStyle(2);
        f->SetRange(0, 10000);
        f->SetParameters(400, 2.6);
        m_meanValueFits.push_back(f);
    }

    for (int i = 0; i < m_nAmplitudes; ++i) {
        //TH2* hist = adcHistograms.find(*it)->second;
        for (int ch = 0; ch < nCh; ++ch) {
            TH1* pulseHistogram = adcHistograms[i]->ProjectionY("_px", ch + 1, ch + 1);
            float mean = pulseHistogram->GetMean();
            float rms = pulseHistogram->GetRMS();
            int nPoints = m_meanValues[ch]->GetN();
            m_meanValues[ch]->SetPoint(nPoints, calibratedAmplitude(amplitudes[i]), mean);
            m_meanValues[ch]->SetPointError(nPoints, 0, rms);
        }
    }

}

void LinearityPlots::draw()
{
    drawRawDataAndFit();
    //drawResiduals();
}

void LinearityPlots::drawRawDataAndFit()
{

/*    char rootName[256];
	sprintf(rootName, "board%02d_a%03d_b%03d_c%03d_d%03d_e%03d_f%03d_g%03d_h%03d", m_boardNumber, m_chipIds[0], m_chipIds[1], m_chipIds[2], m_chipIds[3], m_chipIds[4], m_chipIds[5], m_chipIds[6],m_chipIds[7]);
    m_canvases.push_back(new TCanvas(rootName, rootName, 1));
    for (int ch = 0; ch < nCh; ++ch) {
*/
		 /*
		if ((ch % nChPerChip) == 0) {
            //sprintf(rootName, "va_%03d_linearity", m_chipIds[ch/nChPerChip]);
			sprintf(rootName, "board%02d_a%03d_b%03d_c%03d_d%03d_e%03d_f%03d_g%03d_h%03d", m_boardNumber, m_chipIds[0], m_chipIds[1], m_chipIds[2], m_chipIds[3], m_chipIds[4], m_chipIds[5], m_chipIds[6],m_chipIds[7]);
            m_canvases.push_back(new TCanvas(rootName, rootName, 1));
        }
		*/
		/*
        for (int i = 0; i < m_nAmplitudes; ++i) {
            //m_meanValues[i]->Draw((ch % nChPerChip) == 0 ? "AP" : "PSAME");
			m_meanValues[i]->Draw((ch % nCh) == 0 ? "AP" : "PSAME");
            m_meanValues[i]->SetMarkerColor(kBlue);
            m_meanValues[i]->GetXaxis()->SetTitle("Amplitude [mV]");
            m_meanValues[i]->GetYaxis()->SetTitle("Mean ADC-Count");
            m_meanValues[i]->GetYaxis()->CenterTitle();

            m_meanValueFits[i]->SetLineColor(kRed);
            m_meanValueFits[i]->SetLineWidth(2);

            m_meanValues[i]->Fit(m_meanValueFits[i], "EQ");
            m_meanValueFits[i]->Draw("SAME");
		*/

/*		for (int i = 0; i < m_nAmplitudes; ++i) {
		
			m_meanValues[i]->Draw((ch % nCh) == 0 ? "AP" : "PSAME");
            m_meanValues[i]->SetMarkerColor(kBlue);
            m_meanValues[i]->GetXaxis()->SetTitle("Amplitude [mV]");
            m_meanValues[i]->GetYaxis()->SetTitle("Mean ADC-Count");
            m_meanValues[i]->GetYaxis()->CenterTitle();

            m_meanValueFits[i]->SetLineColor(kRed);
            m_meanValueFits[i]->SetLineWidth(2);

            m_meanValues[i]->Fit(m_meanValueFits[i], "EQ");
            m_meanValueFits[i]->Draw("SAME");
*/
            // Extract fit results
            //double chi2 = m_meanValueFits[i]->GetChisquare();
            //double chi2Ndf = chi2 / m_meanValueFits[i]->GetNDF();

            //double intercept = m_meanValueFits[i]->GetParameter(0);
            //double interceptError = m_meanValueFits[i]->GetParError(1);

            //double slope = m_meanValueFits[i]->GetParameter(1);
            //double slopeError = m_meanValueFits[i]->GetParError(1);

            //fprintf(stderr, "%d %02d \t %4.3lf \t %4.3lf\n", (ch/nChPerChip)+1, (ch%nChPerChip)+1, intercept, slope);
            //fprintf(stdout, "Chi2                : %4.1f (per ndf: %4.1f)\n", chi2, chi2Ndf);
            //fprintf(stderr, "intercept [adc]     : %4.3lf +/- %4.3lf\n", intercept, interceptError);
            //fprintf(stderr, "slope     [adc/mV]  : %4.3lf +/- %4.3lf\n", slope, slopeError);

            //TPaveText* paveText = new TPaveText(0.16, 0.63, 0.50, 0.87, "ndc");
            //paveText->SetFillColor(19);

            /*sprintf(tmp, "f(x) = a + b #upoint x");
            TText* text1 = paveText->AddText(tmp);
            text1->SetTextSize(0.035);

            sprintf(tmp, "a = %3.3lf #pm %3.3lf [adc]", intercept, interceptError);
            TText* text2 = paveText->AddText(tmp);
            text2->SetTextSize(0.035);

            sprintf(tmp, "b = %3.3lf #pm %3.3lf [adc/mV]", slope, slopeError);
            TText* text3 = paveText->AddText(tmp);
            text3->SetTextSize(0.035);

            sprintf(tmp, "#chi^{2} = %4.1lf (per ndf: %4.1lf)", chi2, chi2Ndf);
            TText* text4 = paveText->AddText(tmp);
            text4->SetTextSize(0.035);

            paveText->Draw();*/
//        }
//        m_meanValues[0]->GetYaxis()->SetRangeUser(0, 3000);
//    }

	char rootName[256];
	sprintf(rootName, "board%02d_a%03d_b%03d_c%03d_d%03d_e%03d_f%03d_g%03d_h%03d", m_boardNumber, m_chipIds[0], m_chipIds[1], m_chipIds[2], m_chipIds[3], m_chipIds[4], m_chipIds[5], m_chipIds[6],m_chipIds[7]);
    m_canvases.push_back(new TCanvas(rootName, rootName, 1));

	TMultiGraph* mg = new TMultiGraph;
    for (int ch = 0; ch < nCh; ++ch) {
        m_meanValues[ch]->Fit(m_meanValueFits[ch], "EQN0");
        mg->Add(m_meanValues[ch]);
        mg->GetListOfFunctions()->Add(m_meanValueFits[ch]);
    }
    mg->Draw("AP");
    mg->GetXaxis()->SetTitle("Amplitude [mV]");
    mg->GetYaxis()->SetTitle("Mean ADC-Count");
    mg->GetYaxis()->SetRangeUser(0, 3000);
    gPad->Modified();
    gPad->Update();


}

void LinearityPlots::drawResiduals()
{
    char tmp[128];
    for (int ch = 0; ch < nCh; ++ch) {
        for (int point = 0; point < m_nAmplitudes; ++point) {
            double x, y;
            m_meanValues[ch]->GetPoint(point, x, y);

            double yErr = m_meanValues[ch]->GetEY()[point];

            // yErr remains the same, if we had an error on the amplitude, we could weigh it with the fits derivate here.
            m_meanResiduals[ch]->SetPoint(point, x, y - m_meanValueFits[ch]->Eval(x));
            m_meanResiduals[ch]->SetPointError(point, 0, yErr);
        }

        char title[128];
        //sprintf(title, "[Uplink %03d] Linearity analysis - Residuals", uplinkId);

        TCanvas* canvas = new TCanvas(tmp, title, 1);
        canvas->cd();
        m_meanResiduals[ch]->Draw("APL");
        m_meanResiduals[ch]->SetLineColor(kRed);
        m_meanResiduals[ch]->SetLineWidth(2);
        m_meanResiduals[ch]->SetMarkerColor(kBlue);
        m_meanResiduals[ch]->GetXaxis()->SetTitle("Amplitude [mV]");
        m_meanResiduals[ch]->GetYaxis()->SetTitle("Mean ADC-Count");
        m_meanResiduals[ch]->GetYaxis()->CenterTitle();
    }
}

void LinearityPlots::save()
{
#ifdef WIN32
    char* path = "C:/linearity_analysis/";
#else
    char path[1024];
    sprintf(path, "%s/linearity_analysis/", getenv("HOME"));
#endif
    char fileName[1024];
    /*
	for (int chip = 0; chip < nChips; ++chip) {
		sprintf(fileName, "%s%s.eps", path, m_canvases[chip]->GetName());
        m_canvases[chip]->SaveAs(fileName);
        sprintf(fileName, "%s%s.png", path, m_canvases[chip]->GetName());
        m_canvases[chip]->SaveAs(fileName);
        sprintf(fileName, "%s%s.root", path, m_canvases[chip]->GetName());
        m_canvases[chip]->SaveAs(fileName);
        sprintf(fileName, "%s%s_table.txt", path, m_canvases[chip]->GetName());
        FILE* file = fopen(fileName, "w");
        fprintf(file, "VA-ID \t channel \t pedestal \t pedestal RMS \t gain\n");
        for (int ch = 0; ch < nChPerChip; ++ch) {
            float pedestal = m_meanValues[chip*nChPerChip+ch]->GetX()[0];
            float pedestalRms = 1;
            float gain = m_meanValueFits[chip*nChPerChip+ch]->GetParameter(1);
            fprintf(file, "%03d\t%02d\t%f\t%f\t%f\n", m_chipIds[chip], ch+1, pedestal, pedestalRms, gain);
        }
        fclose(file);
    }
	*/
	sprintf(fileName, "%s%s.eps", path, m_canvases[0]->GetName());
    m_canvases[0]->SaveAs(fileName);
    sprintf(fileName, "%s%s.png", path, m_canvases[0]->GetName());
    m_canvases[0]->SaveAs(fileName);
    sprintf(fileName, "%s%s.root", path, m_canvases[0]->GetName());
    m_canvases[0]->SaveAs(fileName);
    sprintf(fileName, "%s%s_table.txt", path, m_canvases[0]->GetName());
    FILE* file = fopen(fileName, "w");
    fprintf(file, "BoardNo \t VA-ID \t channel \t pedestal \t pedestal RMS \t gain\n");
    //for (int ch = 0; ch < nChPerChip; ++ch) {
	for (int chip = 0; chip < nChips; ++chip) {
		for (int ch = 0; ch < nChPerChip; ++ch) {
			float pedestal = m_meanValues[chip*nChPerChip+ch]->GetX()[0];
			float pedestalRms = m_meanValues[chip*nChPerChip+ch]->GetEY()[0];
			float gain = m_meanValueFits[chip*nChPerChip+ch]->GetParameter(1);
			fprintf(file, "%02d\t%03d\t%02d\t%f\t%f\t%f\n", m_boardNumber ,m_chipIds[chip], ch+1, pedestal, pedestalRms, gain);
		}
    }
    fclose(file);
	




	std::cout<<std::endl;
	std::cout<<"Done!"<<std::endl;
}

