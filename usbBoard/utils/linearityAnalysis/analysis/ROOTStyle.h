#ifndef ROOTStyle_h
#define ROOTStyle_h

#include <TColor.h>
#include <TStyle.h>
#include <TROOT.h>

class ROOTStyle : public TStyle {
public:
    ROOTStyle(const char* identifier = "NewROOTDefaultStyle")
        : TStyle(identifier, identifier)
    {
        gROOT->GetStyle("Plain")->Copy(*this);
 
        SetOptStat(0);
        SetCanvasColor(10);
        SetCanvasBorderMode(0);
        SetFrameLineWidth(1);
        SetFrameFillColor(10);
        SetPadColor(10);
        SetPadGridX(true);
        SetPadGridY(true);
        SetHistLineWidth(1);
        SetFuncWidth(1);
        SetMarkerStyle(20);
        SetTitleOffset(1.1, "Y");
        SetLineScalePS(0.1);
        SetTextFont(42);
        SetTitleFont(42, "X");
        SetTitleFont(42, "Y");
        SetTitleFont(42, "Z");
        SetLabelFont(42, "X");
        SetLabelFont(42, "Y");
        SetLabelFont(42, "Z");
        SetLabelSize(0.03, "X");
        SetLabelSize(0.03, "Y");
        SetLabelSize(0.03, "Z");

        int NRGBs = 5;
        int NCont = 255;
        double stops[] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
        double red[]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
        double green[] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
        double blue[]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
        TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
        SetNumberContours(NCont);

        cd();
    }
};

#endif
