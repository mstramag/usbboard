#ifndef LinearityPlots_h
#define LinearityPlots_h

#include <vector>

class TF1;
class TH2;
class TGraphErrors;
class TCanvas;

const int nChips = 8;
const int nChPerChip = 32;
const int nCh = nChips*nChPerChip;
const int uplinkId = 1;

class LinearityPlots {
public:
    LinearityPlots(const int boardNumber, const std::vector<int>& chipIds);
    ~LinearityPlots();

    void analyze(const std::vector<int>& amplitudes, const std::vector<TH2*>& adcHistograms);
    void draw();
    void save();

private:
    void drawRawDataAndFit();
    void drawResiduals();
    int calibratedAmplitude(int);

    int m_nAmplitudes;
    std::vector<TGraphErrors*> m_meanValues;
    std::vector<TGraphErrors*> m_meanResiduals;
    std::vector<TCanvas*> m_canvases;
    std::vector<TF1*> m_meanValueFits;
    std::vector<int> m_chipIds;
	int m_boardNumber;
};

#endif
