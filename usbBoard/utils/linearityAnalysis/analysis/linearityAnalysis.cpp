#include <iostream>
#include <sstream>
#include <cstdlib>
#include <vector>
#include <assert.h>
#include <vector>
#include <string>

#include <TApplication.h>
#include <TFile.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TTree.h>
#include <TH2.h>
#include <TCanvas.h>


//#include "LinearityRunDescription.h"
#include "LinearityPlots.h"
//#include "MergedEvent.h"
#include "ROOTStyle.h"
//#include "Settings.h"

int main(int argc, char* argv[])
{
	//const int numberOfAmplitudes=3;
    
    if (argc != 11) {
        std::cerr << "usage: " << argv[0] << " <root file name> <board number> <chipID 1> <chipID 2> ... <chipID 8>" << std::endl;
        exit(1);
    }
    
    TApplication app("linearityAnalysis", &argc, argv);
    new ROOTStyle;

#ifdef WIN32
    gSystem->Load("linearityTestShared.dll");
#endif

/*	
	TFile file(app.Argv(1));
    gROOT->cd();
*/    
	
	
	
	std::vector<TH2*> histograms;
	
	//TFile* histogramFile = new TFile(argv[1]);
	//gROOT->cd();

	TFile histogramFile(app.Argv(1));
    gROOT->cd();
	
	const int minimumAmplitude =50;
	const int maximumAmplitude =150;
	const int stepAmplitude    =10;
	

	for (int amplitude=minimumAmplitude; amplitude<=maximumAmplitude; amplitude+=stepAmplitude) {
		//gROOT->cd();
		char histogramName[256];
		sprintf(histogramName, "%04d mV", amplitude);
		std::cout<<"reading histogram: "<<histogramName<<" ..."<<std::endl;
		histograms.push_back( (TH2I*)(((TH2I*)histogramFile.Get(histogramName)->Clone()) ));
		
	}
	
	histogramFile.Close();
	//delete histogramFile;
	//histogramFile=NULL;
	
	

/*	
	TTree* tree = static_cast<TTree*>(file.Get("ReadoutData"));

    LinearityRunDescription* runDescription = static_cast<LinearityRunDescription*>(tree->GetUserInfo()->First());
    runDescription->dump(std::cout);
    std::stringstream settingsStream(runDescription->settings());
    
    MergedEvent* event = 0;
    tree->SetBranchAddress("events", &event);
    int amplitude = 0;
    tree->SetBranchAddress("amplitude", &amplitude);
    int channel = 0;
    tree->SetBranchAddress("channel", &channel);

    //long nEvents = runDescription->measurementParams[EVENTS];
	double amplitudeStep = runDescription->measurementParams[STEP];
	double amplitudeRange = runDescription->measurementParams[AMPL];
 */
	
	
	double amplitudeStep = stepAmplitude;//ToDo 2
	double amplitudeRange = maximumAmplitude-minimumAmplitude;//ToDo 100
    int nAmplitudeSteps = amplitudeRange/amplitudeStep+1;
    std::vector<int> amplitudes(nAmplitudeSteps, -1);

	for (int i = 0; i<nAmplitudeSteps; i++) {
		amplitudes[i]=minimumAmplitude+i*stepAmplitude;
	}
	
	
	

    //std::vector<TH2*> adcHistogram(nAmplitudeSteps);
    /*
	for (int amplitudeIndex = 0; amplitudeIndex < nAmplitudeSteps; ++amplitudeIndex) {
        amplitudes[amplitudeIndex] = amplitudeIndex*amplitudeStep;
        char rootName[128];
        sprintf(rootName, "adcHistogram_%03dmV", amplitudes[amplitudeIndex]);
        adcHistogram[amplitudeIndex] = new TH2I(rootName, rootName, nCh, 0, nCh, 3000, 0, 3000);
    }
	
    long entries = tree->GetEntries();
    for (long it = 0; it < entries; ++it) {
        tree->GetEntry(it);
        int amplitudeIndex = -1;
        
        //std::cout << channel << '\t' << amplitude << std::endl;
        for (int i = 0; i < nAmplitudeSteps; ++i) {
            if (amplitude == amplitudes[i]) {
                amplitudeIndex = i;
                //break;
            }
        }
        assert(amplitudeIndex >= 0);
        
        adcHistogram[amplitudeIndex]->Fill(channel, event->uplinkData(uplinkId).adcValue(channel));
        if ((it+1) % 1000 == 0)
            std::cout << '.' << std::flush;
    }
    std::cout << std::endl;
	
    for (int i = 0; i < nAmplitudeSteps; ++i) {
        char rootName[128];
        sprintf(rootName, "%03dmV", amplitudes[i]);
        new TCanvas(rootName, rootName, 1);
        adcHistogram[i]->Draw("COLZ");
    }
*/	
	for (int i = 0; i < nAmplitudeSteps; ++i) {
        char rootName[128];
        sprintf(rootName, "%03dmV", amplitudes[i]);//ToDo
        new TCanvas(rootName, rootName, 1);
        histograms[i]->Draw("COLZ");
    }
    
	int boardNumber = atoi(argv[1]);

	std::vector<int> chipIds;
	for (int i = 2; i<argc ;i++) {
		chipIds.push_back(atoi(argv[i]));
	}

    //LinearityPlots* linPlots = new LinearityPlots(runDescription->chipIds);
	LinearityPlots* linPlots = new LinearityPlots(boardNumber, chipIds);
    linPlots->analyze(amplitudes, histograms);
    linPlots->draw();
    linPlots->save();
    
    app.Run();

    
    return 0;
}
