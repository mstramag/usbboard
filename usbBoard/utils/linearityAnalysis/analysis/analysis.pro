CONFIG -= qt
CONFIG += debug

TEMPLATE = app
TARGET = linearityAnalysis

exists($(USBBOARDPATH)) {
    include($(USBBOARDPATH)/usbBoard.pri)
    include($(USBBOARDPATH)/RootCInt.pri)
    USBBOARDPATH=$(USBBOARDPATH)
} else {

    include(../../../usbBoard.pri)
    include(../../../RootCInt.pri)
    USBBOARDPATH=../../..
}

HEADERS += \
    LinearityPlots.h \
    ROOTStyle.h

SOURCES += \
    linearityAnalysis.cpp \
    LinearityPlots.cpp

DEPENDPATH += \
    $$USBBOARDPATH/utils/linearityAnalysis/shared \
    $$USBBOARDPATH/readout \
    $$USBBOARDPATH/event

INCLUDEPATH+= \
    $$DEPENDPATH

unix {
    LIBS += -L$$USBBOARDPATH/Builds -llinearityTestShared -lusbBoardEvent -lusbBoardReadout

    !NO_QUSB {
        LIBS += -L$$USBBOARDPATH/support -lquickusb
    }
}

win* {
    LIBS += -L..$${QMAKE_DIR_SEP}..$${QMAKE_DIR_SEP}..$${QMAKE_DIR_SEP}#Builds -lusbBoardEvent -lusbBoardReadout -llinearityTestShared
    CONFIG += console embed_manifest_exe
}
