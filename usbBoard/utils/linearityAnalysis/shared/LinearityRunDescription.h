#ifndef LinearityRunDescription_h
#define LinearityRunDescription_h

#ifdef __CINT__
#include "../../../event/RunDescription.h"
#else
#include "RunDescription.h"
#endif

#include <vector>

// Corresponds to the list of options recognized in CommandlineHandler/linearityTest.*
enum Options {
    AMPL = 0,
    CHAN,
    DELAY,
    EVENTS,
    STEP
};

static const int s_measurementOptionCount = 5;

class LinearityRunDescription : public RunDescription {
public:
    LinearityRunDescription();
    LinearityRunDescription(int _measurementParams[s_measurementOptionCount], bool _isInverted);
    virtual ~LinearityRunDescription();

    // Corresponds to the Options enum above
    int measurementParams[s_measurementOptionCount];
    bool isInverted;

    void dump(std::ostream& stream) const;

    std::vector<int> chipIds;

#ifndef NO_ROOT
    ClassDef(LinearityRunDescription, 1)
#endif
};

#endif
