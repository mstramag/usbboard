#include "LinearityRunDescription.h"

#include "Settings.h"
#include <sstream>

#ifndef NO_ROOT
ClassImp(LinearityRunDescription)
#endif

LinearityRunDescription::LinearityRunDescription()
    : RunDescription()
    , isInverted(false)
{
    for (int i = 0; i < s_measurementOptionCount; ++i)
        measurementParams[i] = -1;
}

LinearityRunDescription::LinearityRunDescription(int _measurementParams[s_measurementOptionCount], bool _isInverted)
    : RunDescription()
    , isInverted(_isInverted)
{
    for (int i = 0; i < s_measurementOptionCount; ++i)
        measurementParams[i] = _measurementParams[i];

    time_t timeStamp = time(0);
    setRunNumber((unsigned long) timeStamp);
    setTimeStamp(timeStamp);

    std::stringstream settingsStream;
    Settings::instance()->writeToStream(settingsStream);
    setSettings(settingsStream.str());
}

LinearityRunDescription::~LinearityRunDescription()
{
}

void LinearityRunDescription::dump(std::ostream& stream) const
{
    stream << "+++ LinearityRunDescription +++" << std::endl
            << "   channel:\t" << measurementParams[CHAN]   << std::endl
            << "amp. range:\t" << measurementParams[AMPL]   << " [mV]" << std::endl
            << "amp.  step:\t" << measurementParams[STEP]   << " [mV]" << std::endl
            << "     delay:\t" << measurementParams[DELAY]  << " [ns]" << std::endl
            << "    events:\t" << measurementParams[EVENTS] << " (per amplitude setting)" << std::endl
            << "--- LinearityRunDescription ---" << std::endl;
}
