TEMPLATE = lib
TARGET = linearityTestShared
CONFIG -= qt

HEADERS += \
    LinearityRunDescription.h

SOURCES += \
    LinearityRunDescription.cpp

CREATE_ROOT_DICT_FOR_CLASSES += \
    LinearityRunDescription.h

exists($(USBBOARDPATH)) {
    include($(USBBOARDPATH)/usbBoard.pri)
    include($(USBBOARDPATH)/RootCInt.pri)
    USBBOARDPATH=$(USBBOARDPATH)
} else {
    include(../../../usbBoard.pri)
    include(../../../RootCInt.pri)
    USBBOARDPATH=../../..
}

DEPENDPATH += \
    $$USBBOARDPATH/utils/linearityAnalysis/shared \
    $$USBBOARDPATH/event \
    $$USBBOARDPATH/readout

INCLUDEPATH += \
    $$DEPENDPATH

unix {
    LIBS += -L$$USBBOARDPATH/Builds -lusbBoardEvent -lusbBoardReadout

    !NO_QUSB {
        LIBS += -L$$USBBOARDPATH/support -lquickusb
    }
}

win* {
    LIBS += -L..$${QMAKE_DIR_SEP}..$${QMAKE_DIR_SEP}..$${QMAKE_DIR_SEP}Builds -lusbBoardEvent -lusbBoardReadout
    CONFIG += console embed_manifest_exe
    DEF_FILE=shared.def
}
