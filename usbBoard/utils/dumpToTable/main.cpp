#include <iostream>
#include <sstream>

#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>

#include <MergedEvent.h>

#include "TSystem.h" // for gSystem->Load("libTree") below

using namespace std;

int main(int argc, char** argv) {
  if (argc != 2) {
    cout << "Usage: " << argv[0] << " <root file>\n\n"
	 << "The program converts the <root file> (in \"ECAL\" format, eg. after ecalTest program)\n"
	 << "into an ASCI table, one line per event with all ADC values.\n"
	 << "The table has a header with usbBoard.uplink.channel triples.\n\n";
    return 0;
  }
  gSystem->Load("libTree"); // without this line -> "no dictionary for class TTree/TBranchElement/TBranch/... is available -> crash
  TFile signalFile(argv[1]);
  gROOT->cd();
  TTree* tree = static_cast<TTree*>(signalFile.Get("ReadoutData"));
  MergedEvent* event = 0;
  tree->SetBranchAddress("events", &event);
  for (long i=1; i<tree->GetEntries(); ++i) {
    tree->GetEntry(i);
    if (i==1) cout << event->dump_adcNamesToStr() << endl;

    event->dump_data(cout);
  }
  signalFile.Close();
  
  return 0;
}
