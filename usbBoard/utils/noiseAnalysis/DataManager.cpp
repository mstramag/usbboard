#include "DataManager.h"
#include <Settings.h>

#include <assert.h>
#include <iostream>

#include <MergedEvent.h>
#include <UsbBoardConfiguration.h>

#include <TH1.h>
#include <TH2.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TMath.h>
#include <TObjArray.h>
#include <TLegend.h>

DataManager::DataManager(const std::vector<int>& uplinksToBeAnalyzed)
{
    char tmp[128];
    Settings* settings = Settings::instance();
    for (unsigned int k = 0; k < uplinksToBeAnalyzed.size(); ++k) {
        int uplinkId = uplinksToBeAnalyzed[k];
        int usbBoardId = settings->usbBoardIdFromUplinkId(uplinkId);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();

        m_uplinkId.push_back(uplinkId);
        m_pedestalPosition.push_back(std::vector<float>(sampleSize, 0));

        sprintf(tmp, "adcHistogram_uplink_%03d", uplinkId);
        TH2* adcHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        adcHistogram->GetXaxis()->SetTitle("channel");
        adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_adcHistogram.push_back(adcHistogram);

        sprintf(tmp, "pedestalCorrectedAdcHistogram_uplink_%03d", uplinkId);
        TH2* pedestalCorrectedAdcHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        pedestalCorrectedAdcHistogram->GetXaxis()->SetTitle("channel");
        pedestalCorrectedAdcHistogram->GetYaxis()->SetTitle("adcValue");
        m_pedestalCorrectedAdcHistogram.push_back(pedestalCorrectedAdcHistogram);

        sprintf(tmp, "commonModeCorrectedAdcHistogram_uplink_%03d", uplinkId);
        TH2* commonModeCorrectedAdcHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        commonModeCorrectedAdcHistogram->GetXaxis()->SetTitle("channel");
        commonModeCorrectedAdcHistogram->GetYaxis()->SetTitle("adcValue");
        m_commonModeCorrectedAdcHistogram.push_back(commonModeCorrectedAdcHistogram);

        sprintf(tmp, "noiseHistogram_uplink_%03d", uplinkId);
        TH1* noiseHistogram = new TH1F(tmp, tmp, sampleSize, .0, sampleSize);
        noiseHistogram->SetLineColor(kRed);
        noiseHistogram->SetMinimum(0.);
        noiseHistogram->SetMaximum(50.);
        noiseHistogram->GetXaxis()->SetTitle("channel");
        noiseHistogram->GetYaxis()->SetTitle("noise / adcCounts");
        m_noiseHistogram.push_back(noiseHistogram);

        sprintf(tmp, "commonModeCorrectedNoiseHistogram_uplink_%03d", uplinkId);
        TH1* commonModeCorrectedNoiseHistogram = new TH1F(tmp, tmp, sampleSize, .0, sampleSize);
        commonModeCorrectedNoiseHistogram->SetLineColor(kGreen);
        commonModeCorrectedNoiseHistogram->SetMinimum(0.);
        commonModeCorrectedNoiseHistogram->SetMaximum(50.);
        commonModeCorrectedNoiseHistogram->GetXaxis()->SetTitle("channel");
        commonModeCorrectedNoiseHistogram->GetYaxis()->SetTitle("noiseHistogram / adcCounts");
        m_commonModeCorrectedNoiseHistogram.push_back(commonModeCorrectedNoiseHistogram);
    }
}

DataManager::~DataManager()
{}

unsigned int DataManager::uplinkIndex(int uplinkId)
{
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
    for (int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
        if (*uplinkIdIt == uplinkId)
            return i;
    }
    assert(false);
    return 0;
}

void DataManager::fillPedestal(unsigned long numberOfPedestalEvents, MergedEvent* event)
{
    Settings* settings = Settings::instance();
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
    for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
        int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
        const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);
        for (unsigned short sample = 0; sample < sampleSize; ++sample) {
            m_pedestalPosition[i][sample]+= double(uplinkData.adcValue(sample))/double(numberOfPedestalEvents);
        }
    }
}

void DataManager::fillEvent(MergedEvent* event)
{
    Settings* settings = Settings::instance();
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
    for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
        int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
        unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
        unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;
        const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);
        int* commonMode = new int[channelsPerReadoutChip];
        for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip) {
            for (unsigned short channel = 0; channel < channelsPerReadoutChip; ++channel) {
                unsigned short sample = chip * channelsPerReadoutChip + channel;
                commonMode[channel] = uplinkData.adcValue(sample) - m_pedestalPosition[i][sample];
            }
            double median = TMath::Median(channelsPerReadoutChip, commonMode);
            for (unsigned short channel = 0; channel < channelsPerReadoutChip; ++channel) {
                unsigned short sample = chip * channelsPerReadoutChip + channel;
                m_adcHistogram[i]->Fill(sample, uplinkData.adcValue(sample));
                double pedestalCorrectedValue = uplinkData.adcValue(sample) - m_pedestalPosition[i][sample] + 400.;
                m_pedestalCorrectedAdcHistogram[i]->Fill(sample, pedestalCorrectedValue);
                m_commonModeCorrectedAdcHistogram[i]->Fill(sample, pedestalCorrectedValue - median);
            }
        }
        delete[] commonMode;
    }
}

void DataManager::draw()
{
    Settings* settings = Settings::instance();
    char tmp[128];
    for (unsigned int i = 0; i < m_uplinkId.size(); ++i) {
        int uplinkId = m_uplinkId[i];
        int usbBoardId = settings->usbBoardIdFromUplinkId(uplinkId);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();

        sprintf(tmp, "canvas_uplink_%03d", m_uplinkId[i]);
        TCanvas* canvas = new TCanvas(tmp, tmp, 1);
        canvas->Divide(2, 2);
        canvas->cd(1);
        m_adcHistogram[i]->Draw("COLZ");
        canvas->cd(2);
        m_pedestalCorrectedAdcHistogram[i]->Draw("COLZ");
        
        canvas->cd(3);
        for (unsigned int chBin = 1; chBin <= sampleSize; ++chBin) {
            m_noiseHistogram[i]->SetBinContent(chBin, m_pedestalCorrectedAdcHistogram[i]->ProjectionY("_px", chBin, chBin)->GetRMS());
        }
        m_noiseHistogram[i]->Draw();

        for (unsigned int chBin = 1; chBin <= sampleSize; ++chBin) {
            m_commonModeCorrectedNoiseHistogram[i]->SetBinContent(chBin, m_commonModeCorrectedAdcHistogram[i]->ProjectionY("_px", chBin, chBin)->GetRMS());
        }
        m_commonModeCorrectedNoiseHistogram[i]->Draw("SAME");

        TLegend* legend = new TLegend(.2, .65, .8, .8);
        legend->AddEntry(m_noiseHistogram[i], "raw noise", "l");
        legend->AddEntry(m_commonModeCorrectedNoiseHistogram[i], "common mode corrected noise", "l");
        legend->Draw();

        canvas->cd(4);
        m_commonModeCorrectedAdcHistogram[i]->Draw("COLZ");
    }
}

