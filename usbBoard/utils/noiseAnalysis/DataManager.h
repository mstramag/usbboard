#ifndef DataManager_h
#define DataManager_h

#include <vector>

class MergedEvent;
class TH1;
class TH2;

class DataManager {
    public:
        DataManager(const std::vector<int>& uplinksToBeAnalyzed);
        ~DataManager();
        void fillPedestal(unsigned long numberOfPedestalEvents, MergedEvent* event);
        void fillEvent(MergedEvent* event);
        void draw();

        const static unsigned short maxAdcValue = 3000;
    private:
        unsigned int uplinkIndex(int uplinkId);

        std::vector<int> m_uplinkId;
        std::vector< std::vector <float> > m_pedestalPosition;
        std::vector<TH2*> m_adcHistogram;
        std::vector<TH2*> m_pedestalCorrectedAdcHistogram;
        std::vector<TH2*> m_commonModeCorrectedAdcHistogram;
        std::vector<TH1*> m_noiseHistogram;
        std::vector<TH1*> m_commonModeCorrectedNoiseHistogram;
};

#endif
