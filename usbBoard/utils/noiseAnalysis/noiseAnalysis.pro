CONFIG -= qt
CONFIG += debug

exists($(USBBOARDPATH)) {
    include($(USBBOARDPATH)/usbBoard.pri)
    include($(USBBOARDPATH)/RootCInt.pri)
    USBBOARDPATH=$(USBBOARDPATH)
} else {

    include(../../usbBoard.pri)
    include(../../RootCInt.pri)
    USBBOARDPATH=../..
}

HEADERS+= \
    DataManager.h

SOURCES+= \
    noiseAnalysis.cpp \
    Datamanager.cpp

DEPENDPATH+= \
    $$USBBOARDPATH/readout \
    $$USBBOARDPATH/event

INCLUDEPATH+= \
    $$DEPENDPATH

unix {
    LIBS += -L$$USBBOARDPATH/Builds -lusbBoardEvent -lusbBoardReadout

    !NO_QUSB {
        LIBS += -L$$USBBOARDPATH/support -lquickusb
    }
}

win* {
    LIBS += -L..$${QMAKE_DIR_SEP}..$${QMAKE_DIR_SEP}Builds -lusbBoardReadout
    CONFIG += console embed_manifest_exe
}
