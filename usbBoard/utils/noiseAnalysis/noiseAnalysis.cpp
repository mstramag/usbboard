#include <iostream>
#include <sstream>
#include <cstdlib>
#include <vector>

#include <TH1.h>
#include <TApplication.h>
#include <TFile.h>
#include <TTree.h>
#include <TBrowser.h>
#include <TROOT.h>
#include <TSystem.h>

#include <TColor.h>
#include <TStyle.h>

#include <RunDescription.h>
#include <MergedEvent.h>

#include "Settings.h"
#include "DataManager.h"

void setRootStyle()
{
    gStyle->SetOptStat(0);
    gStyle->SetCanvasColor(10);
    gStyle->SetCanvasBorderMode(0);
    gStyle->SetFrameLineWidth(1);
    gStyle->SetFrameFillColor(10);
    gStyle->SetPadColor(10);
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    gStyle->SetPadTopMargin(0.15);
    gStyle->SetPadBottomMargin(0.15);
    gStyle->SetPadLeftMargin(0.15);
    gStyle->SetPadRightMargin(0.15);
    gStyle->SetPadGridX(true);
    gStyle->SetPadGridY(true);
    gStyle->SetStatColor(10);
    gStyle->SetStatX(.8);
    gStyle->SetStatY(.8);
    gStyle->SetStatH(.09);
    gStyle->SetHistLineWidth(1);
    gStyle->SetFuncWidth(1);
    gStyle->SetMarkerStyle(20);
    gStyle->SetTitleOffset(1.5, "y");

    gStyle->SetLineScalePS(3.0);
    gStyle->SetTextFont(42);
    gStyle->SetTitleFont(42, "X");
    gStyle->SetTitleFont(42, "Y");
    gStyle->SetTitleFont(42, "Z");
    gStyle->SetLabelFont(42, "X");
    gStyle->SetLabelFont(42, "Y");
    gStyle->SetLabelFont(42, "Z");
    gStyle->SetLabelSize(0.03, "X");
    gStyle->SetLabelSize(0.03, "Y");
    gStyle->SetLabelSize(0.03, "Z");

    int NRGBs = 5;
    int NCont = 255;
    double stops[] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    double red[]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    double green[] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    double blue[]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}

int main(int argc, char* argv[])
{
    if (argc < 2) {
        std::cerr << "usage:   " << argv[0] << " <root file name>" << " <list of uplink IDs>" << std::endl;
        std::cerr << "example: " << argv[0] << " test.root " << "13 31 5" << std::endl;
        std::cerr << "The uplink IDs should exist in the run file. If the list is empty all uplinks are drawn." << std::endl;
        exit(1);
    }
    
    setRootStyle();
    TApplication app("noiseAnalysis", &argc, argv);

    TFile file(app.Argv(1));
    gROOT->cd();
    TTree* tree = static_cast<TTree*>(file.Get("ReadoutData"));

    RunDescription* runDescription = static_cast<RunDescription*>(tree->GetUserInfo()->First());
    runDescription->dump(std::cout);
    Settings::instance()->loadFromRunDescription(*runDescription);
    Settings::instance()->writeToStream(std::cout);
     
    std::vector<int> uplinksToBeAnalyzed;
    for (int i = 0; i < app.Argc() - 2; ++i) {
        uplinksToBeAnalyzed.push_back(atoi(app.Argv(i+2)));
    }
    if (uplinksToBeAnalyzed.size() == 0) {
        Settings* settings = Settings::instance();
        for (unsigned int i = 0; i < settings->numberOfUsbBoards(); ++i) {
            for (unsigned int j = 1; j < s_uplinksPerUsbBoard + 1; ++j) {
                int usbBoardId = settings->usbBoardId(i);
                int uplinkId = settings->uplinkId(usbBoardId, j);
                uplinksToBeAnalyzed.push_back(uplinkId);
            }
        }
    }

    DataManager dataManager(uplinksToBeAnalyzed);

    MergedEvent* event = 0;
    tree->SetBranchAddress("events", &event);
    long entries = tree->GetEntries();

    //TODO: pass two root files: one with pedestal and one with signal.
    std::cout << "taking pedestals";
    for (long it = 0; it < entries; ++it) {
        tree->GetEntry(it);
        dataManager.fillPedestal(entries, event);
        if ((it+1) % 1000 == 0)
            std::cout << '.' << std::flush;
    }
    std::cout << std::endl;

    std::cout << "taking events";
    for (long it = 0; it < entries; ++it) {
        tree->GetEntry(it);
        dataManager.fillEvent(event);
        if ((it+1) % 1000 == 0)
            std::cout << '.' << std::flush;
    }
    std::cout << std::endl;

    dataManager.draw();
    file.Close();

    app.Run();

    return 0;
}
