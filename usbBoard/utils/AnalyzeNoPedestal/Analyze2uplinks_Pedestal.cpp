#include <iostream>
#include <sstream>
#include <cstdlib>
#include <vector>

#include <TH1.h>
#include <TApplication.h>
#include <TFile.h>
#include <TTree.h>
#include <TBrowser.h>
#include <TROOT.h>

#include <TColor.h>
#include <TStyle.h>

#include <RunDescription.h>
#include <MergedEvent.h>

#include <Settings.h>
#include "A2DataManager_Pedestal.h"

void setRootStyle()
{
  gStyle->SetOptStat(1);
  gStyle->SetCanvasColor(10);
  gStyle->SetCanvasBorderMode(0);
  gStyle->SetFrameLineWidth(1);
  gStyle->SetFrameFillColor(10);
  gStyle->SetPadColor(10);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetPadTopMargin(0.15);
  gStyle->SetPadBottomMargin(0.15);
  gStyle->SetPadLeftMargin(0.15);
  gStyle->SetPadRightMargin(0.15);
  gStyle->SetPadGridX(true);
  gStyle->SetPadGridY(true);
  gStyle->SetStatColor(10);
  gStyle->SetStatX(.8);
  gStyle->SetStatY(.8);
  gStyle->SetStatH(.09);
  gStyle->SetHistLineWidth(1);
  gStyle->SetFuncWidth(1);
  gStyle->SetMarkerStyle(20);
  gStyle->SetTitleOffset(1.5, "y");
  
  gStyle->SetLineScalePS(3.0);
  gStyle->SetTextFont(42);
  gStyle->SetTitleFont(42, "X");
  gStyle->SetTitleFont(42, "Y");
  gStyle->SetTitleFont(42, "Z");
  gStyle->SetLabelFont(42, "X");
  gStyle->SetLabelFont(42, "Y");
  gStyle->SetLabelFont(42, "Z");
  gStyle->SetLabelSize(0.03, "X");
  gStyle->SetLabelSize(0.03, "Y");
  gStyle->SetLabelSize(0.03, "Z");
  
  int NRGBs = 5;
  int NCont = 255;
  double stops[] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
  double red[]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
  double green[] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
  double blue[]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);
}

int main(int argc, char* argv[])
{
  // Use command line options to define the parameters and files for the root display
    if (argc != 6) {
    std::cerr << "usage:   " << argv[0]
              << " <pedestal file name"
              << " <signal file name>"
              << " <uplink1 IDs to be analyzed>"
              << " <uplink2 IDs to be analyzed>"
              << " <Channel to show (1..128)> " << std::endl;
    std::cerr << "example: " << argv[0] << " pedestal.root"<< " data.root" <<" 41 42 33" << std::endl;
    exit(1);
    }

  setRootStyle();
  TApplication app("Readout_128CH_IT_Testbeam", &argc, argv);
  TFile pedestalFile(app.Argv(1));
  int uplink1ToBeAnalyzed = atoi(app.Argv(3));
  int uplink2ToBeAnalyzed = atoi(app.Argv(4));
  int showChannel = atoi(app.Argv(5));
  gROOT->cd();
  DataManager dataManager;

 
  //////////////////// Filling histogram with pedestal data 

  // get Tree from pedestal file
  TTree* pedestalTree = static_cast<TTree*>(pedestalFile.Get("ReadoutData")); 
  // get run information 
  RunDescription* runDescription = static_cast<RunDescription*>(pedestalTree->GetUserInfo()->First());
  runDescription->dump(std::cout);
  std::stringstream settingsStream(runDescription->settings());
  Settings::instance()->loadFromStream(settingsStream);
  // declaration of the histogram in the DataManager
  dataManager.addPedestalUplink(uplink1ToBeAnalyzed);
  dataManager.addPedestalUplink(uplink2ToBeAnalyzed);
  MergedEvent *pedestalEvent = 0;
  pedestalTree->SetBranchAddress("events",&pedestalEvent);
  long pedestalEntries = pedestalTree->GetEntries();
  // fill the histogram 
  for(long it = 0;it < pedestalEntries; ++it){
      pedestalTree->GetEntry(it);
      dataManager.fillPedestalEvent(pedestalEvent);
  }
  // get the mean value
  dataManager.getPedestalMeanValue();
  pedestalFile.Close();
  ///////////////////////////// Filling histogram with pedestal data end

  ///////////// data histogram filling start
  // get Tree from data file
  TFile signalFile(app.Argv(2));
  gROOT->cd();
  TTree* signalTree = static_cast<TTree*>(signalFile.Get("ReadoutData"));
  // declaration of the histogram in the DataManager
  dataManager.addUplink(uplink1ToBeAnalyzed);
  dataManager.addUplink(uplink2ToBeAnalyzed);
  MergedEvent* event = 0;
  signalTree->SetBranchAddress("events", &event);
  long signalEntries = signalTree->GetEntries();
  // fill the histogram 
  for (long it = 0; it < signalEntries; ++it) {
    signalTree->GetEntry(it);
    dataManager.fillEvent(event);
  }
  ///////////////////////////// data histogram filling  end
 
  // draw the histograms
  dataManager.draw(showChannel);
  signalFile.Close();

  app.Run();
    
  return 0;
}
