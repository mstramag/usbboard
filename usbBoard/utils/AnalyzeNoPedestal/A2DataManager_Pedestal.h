#ifndef DataManager_h
#define DataManager_h

#include <vector>

class MergedEvent;
class TH1;
class TH2;
class TH3;
class TH2F;
class TFile;
class TTree;
class TProfile;
 
class MeanValue{
public:
    MeanValue(){};
    ~MeanValue(){};
    void setMeanValue(double meanValue){
        m_meanValue.push_back(meanValue);
    };
    void setUplinkId(int uplinkId){
        m_uplinkId = uplinkId;
    };
    double getMeanValue(int channel){return m_meanValue[channel];};
    int getUplinkId(){return m_uplinkId;};
private:
    std::vector<double> m_meanValue;
    int m_uplinkId;
};

class DataManager {
    public:
        DataManager();
        ~DataManager();
        void addUplink(const int uplinksToBeAnalyzed);
        void fillEvent(MergedEvent* event);
        void draw(int showChannel);
	void getEnergy(MergedEvent* event);

        void addPedestalUplink(const int uplinksToBeAnalyzed);
        void fillPedestalEvent(MergedEvent* event);
        void getPedestalMeanValue();

        const static unsigned short maxAdcValue = 4095;
        const static unsigned short maxNumberPhotons = 50000;

 private:
        unsigned int uplinkIndex(int uplinkId);

        std::vector<int> m_uplinkId;
        std::vector<TH2*> m_adcHistogram;
        std::vector<TH2*> m_commonModeCorrectedAdcHistogram;
        std::vector<TH2*> m_uplink_41_vs_40;

        std::vector<TH2*> m_pedestalHistogram;
        std::vector<MeanValue*> m_meanValue;

	//public:
	TFile* file_data;       
};

#endif
