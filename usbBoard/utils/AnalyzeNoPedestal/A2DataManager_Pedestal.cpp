#include "A2DataManager_Pedestal.h"
#include <Settings.h>
#include <cstring>
#include <assert.h>
#include <iostream>
 
#include <MergedEvent.h>
#include <UsbBoardConfiguration.h>

#include <TH1.h>
#include <TH2.h>
#include <TH2F.h>
#include <TF1.h>
#include <TProfile.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TMath.h>
#include <TObjArray.h>
#include <TLegend.h>
DataManager::DataManager(){

}

// declaration of a uplink histogram for data
void DataManager::addUplink(const int uplinksToBeAnalyzed)
{
    char tmp[128];
    Settings* settings = Settings::instance();


        int uplinkId = uplinksToBeAnalyzed;
        int usbBoardId = settings->usbBoardIdFromUplinkId(uplinkId);
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
        std::cerr <<"DataManager::DataManager, sampleSize"<<sampleSize<<std::endl;

        std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
        std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
        bool added = false;
        for(uplinkIdIt = m_uplinkId.begin();uplinkIdIt != uplinkIdItEnd; uplinkIdIt ++ ){
            if(*uplinkIdIt == uplinkId)
                added = true;
        }
        if(!added)
            m_uplinkId.push_back(uplinkId);

        sprintf(tmp, "Uplink %03d", uplinkId);
        TH2* adcHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, 1500, -100, 600);
        adcHistogram->GetXaxis()->SetTitle("channel");
        adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_adcHistogram.push_back(adcHistogram);

}

// declaration of a uplink histogram for pedestal
void DataManager::addPedestalUplink(const int uplinksToBeAnalyzed){
    char tmp[128];
    Settings* settings = Settings::instance();

    int uplinkId = uplinksToBeAnalyzed;
    int usbBoardId = settings->usbBoardIdFromUplinkId(uplinkId);
    unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
    std::cerr <<"DataManager::DataManager, sampleSize"<<sampleSize<<std::endl;

    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
    bool added = false;
    for(uplinkIdIt = m_uplinkId.begin();uplinkIdIt != uplinkIdItEnd; uplinkIdIt ++ ){
        if(*uplinkIdIt == uplinkId)
            added = true;
    }
    if(!added)
        m_uplinkId.push_back(uplinkId);

    sprintf(tmp, "Pedestal Uplink %03d", uplinkId);
    TH2* pedestalHistogram = new TH2I(tmp, tmp, sampleSize, .0, sampleSize, 200, 400, 800);
    pedestalHistogram->GetXaxis()->SetTitle("channel");
    pedestalHistogram->GetYaxis()->SetTitle("adcValue");
    m_pedestalHistogram.push_back(pedestalHistogram);

}


DataManager::~DataManager()
{}

// conversion from uplink id to uplink Index 
unsigned int DataManager::uplinkIndex(int uplinkId)
{
    std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
    std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
    for (int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
        if (*uplinkIdIt == uplinkId)
            return i;
    }
    assert(false);
    return 0;
}
// fill the data histograms (for two uplinks) use the parmeters of the readout settings
// note that this is very strongly related with the readout settings (number of samples
// per event per uplink...) 

void DataManager::fillEvent(MergedEvent* event)
{ 
  Settings* settings = Settings::instance();
  std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
  std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
  std::vector<TH2*>::iterator adcHistIt = m_adcHistogram.begin();
  std::vector<TH2*>::iterator adcHistItEnd = m_adcHistogram.end();

  std::vector<MeanValue*>::iterator meanValueIt = m_meanValue.begin();
  std::vector<MeanValue*>::iterator meanValueItEnd = m_meanValue.end();
  for (;uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt){
      int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
      unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
      unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
      unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;
      const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);
      //int* commonMode = new int[channelsPerReadoutChip];
      for(adcHistIt=m_adcHistogram.begin();adcHistIt != adcHistItEnd;++adcHistIt){
          int adcHistUplinkId;
          sscanf((*adcHistIt)->GetName(),"Uplink %d", &adcHistUplinkId);
          //printf("Uplink ID %d == %d",adcHistUplinkId ,*uplinkIdIt );
          if(adcHistUplinkId == *uplinkIdIt){
              for(meanValueIt = m_meanValue.begin();meanValueIt != meanValueItEnd; meanValueIt++){
		//printf("Uplink ID %d == %d\n",(*meanValueIt)->getUplinkId(),*uplinkIdIt );
                //getchar();
                  if((*meanValueIt)->getUplinkId() == *uplinkIdIt){
                      for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip){
                          for (unsigned short channel = 0; channel < channelsPerReadoutChip; ++channel){
                              unsigned short sample = chip * channelsPerReadoutChip + channel;
                              // invert the order of the channels to adapt to the dectector connection
                              (*adcHistIt)->Fill(63-sample,uplinkData.adcValue(sample)-(*meanValueIt)->getMeanValue(63-sample));
                              //getchar();
//std::cerr << "@ Channel "<< sample <<" Mean Value = " << (*adcHistIt)->ProjectionY("pedestalProjectionY", sample, sample, "")->GetMean()<< std::endl;
                          }
                      }
                  }
              }

          }
      }
      //delete[] commonMode;
  }


}

// fill the pedestal histograms (for two uplinks) 

void DataManager::fillPedestalEvent(MergedEvent *event){
  Settings* settings = Settings::instance();
  std::vector<int>::iterator uplinkIdIt = m_uplinkId.begin();
  std::vector<int>::iterator uplinkIdItEnd = m_uplinkId.end();
  std::vector<TH2*>::iterator pedestalHistIt = m_pedestalHistogram.begin();
  std::vector<TH2*>::iterator pedestalHistItEnd = m_pedestalHistogram.end();
  for (;uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt){
      int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
      unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
      unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip();
      unsigned short numberOfReadoutChips = sampleSize / channelsPerReadoutChip;
      const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);
      //int* commonMode = new int[channelsPerReadoutChip];
      for(pedestalHistIt=m_pedestalHistogram.begin();pedestalHistIt != pedestalHistItEnd;++pedestalHistIt){
          int adcHistUplinkId;
          //getchar();
          sscanf((*pedestalHistIt)->GetName(),"Pedestal Uplink %d", &adcHistUplinkId);
          if(adcHistUplinkId == *uplinkIdIt){
              for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip){
                  for (unsigned short channel = 0; channel < channelsPerReadoutChip; ++channel){
                      unsigned short sample = chip * channelsPerReadoutChip + channel;
                      // invert the order of the channels to adapt to the dectector connection
                      (*pedestalHistIt)->Fill(63-sample,uplinkData.adcValue(sample));
                      //std::cerr << "@ Channel "<< sample <<" Mean Value = " << (*pedestalHistIt)->ProjectionY("pedestalProjectionY", sample, sample, "")->GetMean()<< std::endl;
                  }
              }
          }
      }
      //delete[] commonMode;
  }
}

// draw the histograms 
void DataManager::draw(int showChannel)
{
  
  TCanvas* canvas = new TCanvas("c1","c1", 1);
  canvas->Divide(2,2);
  // draw pedestal histogram  uplink first and second
  canvas->cd(1);
  m_pedestalHistogram[0]->Draw("COLZ");
  canvas->cd(2);
  m_pedestalHistogram[1]->Draw("COLZ");
    // draw data histogram  uplink first and second
  canvas->cd(3);
  m_adcHistogram[0]->Draw("COLZ");
  canvas->cd(4);
  m_adcHistogram[1]->Draw("COLZ");
  // draw single channel projection (choose between two uplinks to select one out of 128
  // canvas->cd(5);
  // TH1D* proj;
  // if (showChannel<65)
  //     proj= m_adcHistogram[0]-> ProjectionY("channel", showChannel ,showChannel ,"");
  // else
  //     proj= m_adcHistogram[1]-> ProjectionY("channel", showChannel-64,showChannel-64,"");
  // proj -> GetXaxis()->SetRangeUser(-100,500);
  // proj-> Draw("");
}


// pedestal value calculation
void DataManager::getPedestalMeanValue(){
    std::vector<TH2*>::iterator pedestalHistIt = m_pedestalHistogram.begin();
    std::vector<TH2*>::iterator pedestalHistItEnd = m_pedestalHistogram.end();
    int uplinkId;
    for(pedestalHistIt=m_pedestalHistogram.begin(); pedestalHistIt != pedestalHistItEnd; pedestalHistIt++){
        sscanf((*pedestalHistIt)->GetName(), "Pedestal Uplink %d", &uplinkId);
        MeanValue * pedestalMeanvalue = new MeanValue();
        pedestalMeanvalue->setUplinkId(uplinkId);
        for(int i=0;i<64;i++){
            pedestalMeanvalue->setMeanValue((*pedestalHistIt)->ProjectionY("pedestal",i+1,i+1,"")->GetMean());
            std::cerr<<(*pedestalHistIt)->ProjectionY("pedestal",i+1,i+1,"")->GetMean()<<std::endl;
        }

        m_meanValue.push_back(pedestalMeanvalue);
    }
}


