#include "DataManager.h"
#include <Settings.h>

#include <assert.h>
#include <iostream>

#include <MergedEvent.h>
#include <UsbBoardConfiguration.h>

#include <TH1.h>
#include <TH2.h>
#include <TH2F.h>
#include <TF1.h>
#include <TH3.h>
#include <TProfile.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TMath.h>
#include <TObjArray.h>
#include <TLegend.h>
#include <TGraph.h>

#include <TRandom.h>
#include <TSpectrum.h>
#include <TVirtualFitter.h>
#include <TROOT.h>
#include <TStyle.h>

DataManager::DataManager(std::string pathToSetupFile){
    char tmp[128];
    Settings* settings = Settings::instance();

    m_forceToChangeCfgFile = false;
    m_readInSetupFile = true;

    // read set up from the setupFile
    std::ifstream setupFile(pathToSetupFile.c_str());
    if(!setupFile){
        std::cerr << "Failure: could not open file: \"" << pathToSetupFile << "\"." << std::endl;
        std::cerr << "Please check if the path is correct or not!" << std::endl;
        assert(false);
    }

    std::string s;
    int temp;
    float gain;
    while (true) {
        std::getline(setupFile, s);
        if (setupFile.eof())
            break;
        const char* searchString = s.c_str();

        if (s.find("#") == 0 || s=="") {
            continue; // Skip commented lines
        }

        if(sscanf(searchString, "S1_UP0 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P1_P,temp));
        }

        if(sscanf(searchString, "S1_UP1 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P1_N,temp));
        }

        if(sscanf(searchString, "S2_UP0 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P2_P,temp));
        }

        if(sscanf(searchString, "S2_UP1 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P2_N,temp));
        }

        if(sscanf(searchString, "S3_UP0 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P3_P,temp));
        }

        if(sscanf(searchString, "S3_UP1 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P3_N,temp));
        }

        if(sscanf(searchString, "S4_UP0 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P4_P,temp));
        }

        if(sscanf(searchString, "S4_UP1 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P4_N,temp));
        }


        if(sscanf(searchString, "S5_UP0 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P5_P,temp));
        }

        if(sscanf(searchString, "S5_UP1 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P5_N,temp));
        }

        if(sscanf(searchString, "S6_UP0 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P6_P,temp));
        }

        if(sscanf(searchString, "S6_UP1 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P6_N,temp));
        }

        if(sscanf(searchString, "S7_UP0 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P7_P,temp));
        }

        if(sscanf(searchString, "S7_UP1 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P7_N,temp));
        }

        if(sscanf(searchString, "S8_UP0 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P8_P,temp));
        }

        if(sscanf(searchString, "S8_UP1 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P8_N,temp));
        }

        if(sscanf(searchString, "S1_GAIN || %f ||",&gain )==1){
            m_SipmGains.push_back(gain);
        }

        if(sscanf(searchString, "S2_GAIN || %f ||",&gain )==1){
            m_SipmGains.push_back(gain);
        }

        if(sscanf(searchString, "S3_GAIN || %f ||",&gain )==1){
            m_SipmGains.push_back(gain);
        }

        if(sscanf(searchString, "S4_GAIN || %f ||",&gain )==1){
            m_SipmGains.push_back(gain);
        }

    }


    SuperLayerMap::iterator superLayer_iter;
    std::vector<int> uplinksToBeAnalyzed;

//    for(int superLayerIndex = 0;superLayerIndex<2;superLayerIndex++){
        for(superLayer_iter= m_superLayerMap.begin();superLayer_iter!=m_superLayerMap.end();superLayer_iter++){
            if(superLayer_iter->second!=0)
                uplinksToBeAnalyzed.push_back(superLayer_iter->second);
        }
//    }

    // Each SiPM uses two uplinks
    m_NumberOfSipm = uplinksToBeAnalyzed.size() / 2;

    for (int it =0; it < m_NumberOfSipm; ++it){

        std::vector<int> uplinkId;

        uplinkId.push_back(uplinksToBeAnalyzed[it*2]);
        uplinkId.push_back(uplinksToBeAnalyzed[it*2 + 1]);

        m_Sipm_uplinkId.push_back(uplinkId);

        //std::cout << "uplinkId.at(0) = " << uplinkId.at(0) << "\tuplinkId.at(1) = " << uplinkId.at(1) << std::endl;

    }

    printf("\nDatamanager::uplinksToBeAnalyzed.size() = %d\n",uplinksToBeAnalyzed.size());
    printf("Datamanager::m_NumberOfSipm = %d\n\n",m_NumberOfSipm);


    for (unsigned int chip = 0; chip < m_Sipm_uplinkId.size(); ++chip) {

        std::vector<int> uplinkId;
        uplinkId = m_Sipm_uplinkId.at(chip);

        printf("uplinkId = %d and %d (SiPM %d) | Gain = %.2f\n",uplinkId.at(0), uplinkId.at(1), chip+1, m_SipmGains[chip]);
        int usbBoardId = settings->usbBoardIdFromUplinkId(uplinkId.at(0));
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
        sampleSize = sampleSize * 2; // SiPM = 2 * VATA64

        sprintf(tmp, "SiPM128_adcHistogram_SiPM[%d]", chip+1);
        TH2* adcHistogram = new TH2I(tmp, "SiPM128_adcHistogram", sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        adcHistogram->GetXaxis()->SetTitle("channel");
        adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_adcHistogram.push_back(adcHistogram);

        sprintf(tmp, "SiPM128_PS_adcHistogram_SiPM[%d]", chip+1);
        TH2* PS_adcHistogram = new TH2I(tmp, "SiPM128_PS_adcHistogram", sampleSize, .0, sampleSize, maxAdcValue+500, -500, maxAdcValue);
        PS_adcHistogram->GetXaxis()->SetTitle("channel");
        PS_adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_PS_adcHistogram.push_back(PS_adcHistogram);

        sprintf(tmp, "SiPM128_raw_pedestals_SiPM[%d]", chip+1);
        TH2* raw_pedestals = new TH2I(tmp, "SiPM128_raw_pedestals", sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        raw_pedestals->GetXaxis()->SetTitle("channel");
        raw_pedestals->GetYaxis()->SetTitle("adcValue");
        m_raw_pedestals.push_back(raw_pedestals);

    }

    TH1* clusterSize_hist = new TH1I("Cluster size", "Cluster size", 15, 0, 15);
    m_ClusterSize.push_back(clusterSize_hist);

    TH1* NbCluster_hist = new TH1I("Cluster number", "Cluster number", 10  , 0, 10);
    m_NbCluster.push_back(NbCluster_hist);

    TH1* ClusterADC_hist = new TH1I("ClusterADC", "ClusterADC", maxAdcValue, 0, maxAdcValue);   //+500
    m_ClusterADC.push_back(ClusterADC_hist);

    const unsigned short maxPos = SIPM_READOUT_CHANNELS * m_NumberOfSipm;
    TH1D* ClusterChannel_hist = new TH1D("Cluster channel", "Cluster channel", maxPos , 0.5, maxPos + 0.5);
    m_ClusterChannel.push_back(ClusterChannel_hist);

    TH1D* ClusterSum = new TH1D("Cluster Sum", "Cluster Sum", 1000, 0, 1000);
    m_ClusterSum.push_back(ClusterSum);

}

DataManager::~DataManager(){
    //fclose(m_resultLog);
}

void DataManager::fill_raw_pedestals(MergedEvent* event)
{
    Settings* settings = Settings::instance();
    std::vector<int> uplinkId;

    unsigned short numberOfReadoutChips = m_Sipm_uplinkId.size();

    for (unsigned int chip = 0; chip < numberOfReadoutChips; ++chip) {

        uplinkId = m_Sipm_uplinkId.at(chip);

        std::vector<int>::iterator uplinkIdIt = uplinkId.begin();
        std::vector<int>::iterator uplinkIdItEnd = uplinkId.end();

        static int nn = 1;

        for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
            int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
            //            unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize(); //64
            unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip(); //64
            //            unsigned short numberOfReadoutChips = sampleSize * 2/ channelsPerReadoutChip; //2
            const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);

            //           for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip) {
            for (unsigned short channel = 0; channel < (channelsPerReadoutChip); ++channel) {
                unsigned short sample = i * channelsPerReadoutChip + channel;
                //std::cout << i << " " << chip << " " << channel << " " << sample << " " << channelsPerReadoutChip << std::endl;
                //std::cout << *(m_raw_pedestals[i] + i) << std::endl;
                m_raw_pedestals[chip]->Fill(GetChannelIndexOfSipm128(sample) - 1 , uplinkData.adcValue(channel));
                //counter++;
                //std::cout << "  ** " << uplinkData.adcValue(channel) << std::endl;
                //std::cout << uplinkData.adcValue(sample) << " " << m_raw_pedestals[i] << " " << *(m_raw_pedestals[i]+nn+sample) << " ";
                //std::cout << std::endl;

            }
            //}
        }
        nn++;
    }
}

void DataManager::pedestals_write_Mean_RMS()
{

    float * pedestal_Mean;
    float * pedestal_RMS;

    unsigned short channelsPerReadoutChip = SIPM_READOUT_CHANNELS;
    unsigned short numberOfReadoutChips = m_Sipm_uplinkId.size();

    for (unsigned int chip = 0; chip < numberOfReadoutChips; ++chip) {
        pedestal_Mean = new float[channelsPerReadoutChip];
        pedestal_RMS = new float[channelsPerReadoutChip];

        for (unsigned short channel = 0; channel <channelsPerReadoutChip; ++channel) {
            pedestal_Mean[channel] = m_raw_pedestals[chip]->ProjectionY("p1",channel+1,channel+1,"")->GetMean();
            pedestal_RMS[channel] = m_raw_pedestals[chip]->ProjectionY("p2",channel+1,channel+1,"")->GetRMS();
        }

        m_pedestal_Mean.push_back(pedestal_Mean);
        m_pedestal_RMS.push_back(pedestal_RMS);

    }
}

void DataManager::draw()
{
    char tmp[256];
    TCanvas* canvas = NULL;
    std::vector<int> uplinkId;

    double mean = 0;
    double rms = 0;

    std::cout << "\n*****>>> DataManager::" << __func__ << "() *****" << std::endl;

    for(unsigned int i=0;i<m_Sipm_uplinkId.size();++i){


        uplinkId = m_Sipm_uplinkId.at(i);

        sprintf(tmp,"Physical SiPM128 Order from uplink %d and %d",uplinkId.at(0),uplinkId.at(1));
        canvas = new TCanvas(tmp,tmp,0,0,1400,1000);
        canvas->Divide(1,3);
        canvas->cd(1);
        m_raw_pedestals[i]->Draw("COLZ");
        mean = m_raw_pedestals[i]->GetMean(2);
        rms = m_raw_pedestals[i]->GetRMS(2);
        m_raw_pedestals[i]->GetYaxis()->SetRangeUser(mean - 10*rms, mean + 10*rms);

        canvas->GetPad(1)->SetLogz();

        canvas->cd(2);
        m_adcHistogram[i]->Draw("COLZ");
        mean = m_adcHistogram[i]->GetMean(2);
        rms = m_adcHistogram[i]->GetRMS(2);
        m_adcHistogram[i]->GetYaxis()->SetRangeUser(300, mean + 10*rms);
        canvas->GetPad(2)->SetLogz();

        canvas->cd(3);
        m_PS_adcHistogram[i]->Draw("COLZ");
        mean = m_PS_adcHistogram[i]->GetMean(2);
        rms = m_PS_adcHistogram[i]->GetRMS(2);
        canvas->GetPad(3)->SetLogz();
    }
}

void DataManager::SaveAllHistos(){

    TFile* recordFile =  new TFile("/home/it/usbboard/usbBoard/data/lect_sipm_result.root","RECREATE");

    for(unsigned int i=0;i<m_Sipm_uplinkId.size();++i){
        m_raw_pedestals[i]->Write();
        m_adcHistogram[i]->Write();
        m_PS_adcHistogram[i]->Write();
    }
    m_ClusterADC[0]->Write();
    m_ClusterChannel[0]->Write();
    m_ClusterSize[0]->Write();
    m_ClusterSum[0]->Write();

    recordFile->Close();

}

uint16_t DataManager::GetChannelIndexOfSipm128(uint16_t channelIndexOnVata64Chip){

    uint16_t ch_Sipm128 = 0;

    int Reorder_bychannel[128] = {  2,10,18,26,34,42,50,58,66,74,82,90,98,106,114,122,
                                    6,14,22,30,38,46,54,62,70,78,86,94,102,110,118,126,
                                    4,12,20,28,36,44,52,60,68,76,84,92,100,108,116,124,
                                    8,16,24,32,40,48,56,64,72,80,88,96,104,112,120,128,
                                    1, 9,17,25,33,41,49,57,65,73,81,89,97,105,113,121,
                                    5,13,21,29,37,45,53,61,69,77,85,93,101,109,117,125,
                                    3,11,19,27,35,43,51,59,67,75,83,91,99,107,115,123,
                                    7,15,23,31,39,47,55,63,71,79,87,95,103,111,119,127};

    ch_Sipm128 = Reorder_bychannel[channelIndexOnVata64Chip];
    return ch_Sipm128;

}

void DataManager::GetsignalADC_PedSub(std::vector<std::vector<int> > &in_eventADC, std::vector<std::vector<int> > &out_eventADC_PS)
{
    unsigned short numberOfReadoutChips = in_eventADC.size();

    for (unsigned int chip = 0; chip < numberOfReadoutChips; ++chip) {
        for (unsigned short channel = 0; channel < in_eventADC[chip].size(); ++channel) {
            out_eventADC_PS[chip][channel] = in_eventADC[chip][channel] - m_pedestal_Mean.at(chip)[channel];
            m_PS_adcHistogram[chip]->Fill(channel , out_eventADC_PS[chip][channel]);
        }
    }
}


void DataManager::GetRawsignalADCdata_1event(MergedEvent* event, std::vector<std::vector<int> > &eventADC)
{
    Settings* settings = Settings::instance();
    std::vector<int> uplinkId;

    unsigned short phyChannel;
    unsigned short numberOfReadoutChips = m_Sipm_uplinkId.size();

    for (unsigned int chip = 0; chip < numberOfReadoutChips; ++chip) {

        uplinkId = m_Sipm_uplinkId.at(chip);

        std::vector<int>::iterator uplinkIdIt = uplinkId.begin();
        std::vector<int>::iterator uplinkIdItEnd = uplinkId.end();


        for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i)
        {
            int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
            unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
            const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);

            for (unsigned short channel = 0; channel < sampleSize; ++channel){
                phyChannel = GetChannelIndexOfSipm128(channel+i*sampleSize);
                eventADC[chip][phyChannel - 1] = uplinkData.adcValue(channel);
                m_adcHistogram[chip]->Fill(phyChannel - 1, uplinkData.adcValue(channel) );
            }
        }
    }
}

int DataManager::Get_NumberChannels()
{
    m_NumberChannels = SIPM_READOUT_CHANNELS;
    return m_NumberChannels;
}

int DataManager::Get_NumberSIPM()
{
    return m_NumberOfSipm;
}
