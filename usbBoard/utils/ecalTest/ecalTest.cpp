#include <iostream>
#include <sstream>
#include <cstdlib>
#include <time.h> 

#include <Settings.h>
#include <UsbBoardManager.h>
#include <RunDescription.h>
#include <MergedEvent.h>

#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <assert.h>

#ifndef NO_ROOT
#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#endif

#define CHANGE_DAC_RUN_TIMES 10
#define MPPC_BOARD_ID 5
#define MAPD_BOARD_ID 10

struct globalArgs_t			//------added by lht 10:24@5-2-2011
{
  unsigned long arg_nEvents;		/* -n option */
  const char * arg_settingsPath;	/* -p option */
  const char * arg_outputFileName;	/* -o option */
  const char * arg_continuousMode;	/* -s option */
  bool arg_force;			/* -f option */
  int arg_dump;		        	/* -d option */
  bool arg_readSensorMode;              /* -r option */  //-----added by Snow 2012.03.26 @EPFL, this mode will continuously readout sensor values from AD7888
  bool arg_continuousChangingDAC;       /* -l option */  //-----added by Snow 2012.03.30 @EPFL,this mode will continuously run the following procedure by CHANGE_DAC_RUN_TIMES times: read nEvents--->increase/decrease the DAC for led injection intensity--changing DAC again...
  int arg_stepDACforContinuousChaningDAC;

  bool arg_ledInjectionMode;            /* -L option */
  int arg_ledInjectionBoardSel;

  bool arg_ChangeledIntenseByStep;    /* -i option */
  int arg_ChangeledIntenseStep;

  bool arg_ChangeHoldDelayTime;       /* -H option */
  int arg_holdDelayTime;
  
  bool arg_changeTriggerType;		// -t option: change trigger type
  TriggerMode arg_triggerType;			// 0=internal, 1=external

}globalArgs;
static const char *optString = "n:p:o:s:d:l:L:i:H:t:frh?";

int main(int argc, char* argv[])
{

  #ifdef NEW_USBBOARD
    printf("Using new usbBoard libraries\n");
  #endif

  /*Initialize globalArgs before we get to work */
  globalArgs.arg_nEvents = 100;//Pujiang
  globalArgs.arg_settingsPath = NULL;
  globalArgs.arg_outputFileName = NULL;
  globalArgs.arg_continuousMode = NULL;
  globalArgs.arg_force = false;
  globalArgs.arg_dump = 3;
  globalArgs.arg_readSensorMode = false;
  globalArgs.arg_continuousChangingDAC = false;
  globalArgs.arg_stepDACforContinuousChaningDAC = 0;
  globalArgs.arg_ledInjectionMode = false;
  globalArgs.arg_ledInjectionBoardSel = 0;

  globalArgs.arg_ChangeledIntenseByStep = false;
  globalArgs.arg_ChangeledIntenseStep = 0;

  globalArgs.arg_ChangeHoldDelayTime = false; // Normally, this parameter should be defined in cfg file.
  globalArgs.arg_holdDelayTime = 30;
  
  globalArgs.arg_changeTriggerType = false;
  globalArgs.arg_triggerType = internal;
  int trigtype(0);
    
  int opt = 0;

  //Get paremeter from the commond
  opt = getopt(argc, argv, optString);
  if(opt == -1){
    std::cerr <<  "There is no opption in the command! Type \"ecalTest -h\" for help." << std::endl;
    exit(EXIT_FAILURE);
  }
  while(opt != -1){
    switch(opt){
    case 'n':
      globalArgs.arg_nEvents = atoi(optarg);
      break;
    case 'p':
      globalArgs.arg_settingsPath = optarg;
      break;
    case 'o':
      globalArgs.arg_outputFileName = optarg;
      break;
    case 's':
      globalArgs.arg_continuousMode = optarg;
      break;
    case 'f':
      globalArgs.arg_force = true;
      break;
    case 'r':
      globalArgs.arg_readSensorMode = true;
      break;
    case 'd':
      globalArgs.arg_dump = atoi(optarg);
      break;
    case 'l':
      globalArgs.arg_continuousChangingDAC = true;
      globalArgs.arg_stepDACforContinuousChaningDAC = atoi(optarg);
      printf("Set the mode to continuously change led injection intensity by stepDAC = %d\n",globalArgs.arg_stepDACforContinuousChaningDAC);
      printf("Make sure it is the mode you want, if yes, press any key to continue....\n");
      //if(globalArgs.arg_stepDACforContinuousChaningDAC>0) printf("stepDAC>0, stepDAC = %d\n",globalArgs.arg_stepDACforContinuousChaningDAC);
      //else printf("stepDAC<0, stepDAC = %d\n",-1*globalArgs.arg_stepDACforContinuousChaningDAC);
      //getchar();
      break;
    case 'L':
      globalArgs.arg_ledInjectionMode = true;
      globalArgs.arg_ledInjectionBoardSel = atoi(optarg);
      printf("running light injection Mode\n");
      break;
    case 'i':
      globalArgs.arg_ChangeledIntenseByStep = true;
      globalArgs.arg_ChangeledIntenseStep = atoi(optarg);
      printf("light intensity on all injection FE board will be changed by %d\n",globalArgs.arg_ChangeledIntenseStep);
      break;
    case 'H':
      globalArgs.arg_ChangeHoldDelayTime = true;
      globalArgs.arg_holdDelayTime = atoi(optarg);
      printf("change holdDelayTime = %d\n",globalArgs.arg_holdDelayTime);
      break;
    case 't':
		globalArgs.arg_changeTriggerType = true;
		trigtype = atoi(optarg);
		if(!(trigtype==0 || trigtype==1)) {
			std::cerr <<  "Option -t should be either 0 or 1!" << std::endl;
			exit(EXIT_FAILURE);
		} else {
			if(trigtype == 0) {
				globalArgs.arg_triggerType = internal;
			} else if(trigtype == 1) {
				globalArgs.arg_triggerType = external;
			}
		}
		break;
    case 'h':
    case '?':
        std::cerr << "Usage: ecalTest -n EventsNumber -p cfg_FilePath -o OutputRootFileName [-s Uplink_Channel] [-f force usb board firmaware download] [-d Uplink to dump on console (9 don't dump any, 10 dump all)] [-r readSensorMode] [-l stepDACvalues(>0 increase,<0 decrease)] [-L ledInjectionBoardSelect(0=MPPC BOARD;1=MAPD BOARD)] [-i changeLedInjectionStep (>0 increase,<0 decrease)] [-H holdDelayTime] " << std::endl;
      std::cerr << "Such as:" << std::endl
                << "./ecalTest -n 1000 -p /home/cc/usbBoard/cfgFiles -o test.root -s 1_1" << std::endl
                << "or something like:" << std:: endl
                <<"./ecalTest -d 2 -n 1000 -p /home/cc/usbBoard/cfgFiles -o test.root -f" << std::endl;
      std::cerr <<"\n\nExample for led injection" <<std::endl
               << "./ecalTest -d 2 -n 1000 -p /home/cc/usbBoard/cfgFiles -o test.root -L 0 "<<std::endl;
      std::cerr <<"\n\n for test beam, use the options:" <<std::endl
               << "./ecalTest ... -t <trigger type> -L <led injection mode> "<<std::endl
               << "<trigger type> = 0 for internal trigger "<<std::endl
               << "<trigger type> = 1 for external trigger "<<std::endl
               << "<led injection mode> = 1 for led injection "<<std::endl
               << "for NO led injection, don't use the option"<<std::endl<<std::endl;               

      exit(EXIT_FAILURE);
      break;
    default:
      std::cerr << "Wrong options! Type \"ecalTest -h\" for help." << std::endl;
      break;
    }
    opt = getopt(argc, argv, optString);
  }

  std::string settingsPath;
  std::string outputFileName;
  std::string continuousMode;
  int uplinkDumpNumber=2;
  unsigned long nEvents = 0;
  if(globalArgs.arg_nEvents != 0)
    nEvents = globalArgs.arg_nEvents;
  else{
    std::cerr << "Missing option -n. Type \"ecalTest -h\" for help." << std::endl;
    exit(EXIT_FAILURE);
  }
  if(globalArgs.arg_dump != 0)
    uplinkDumpNumber = globalArgs.arg_dump;

  if(globalArgs.arg_settingsPath != NULL)
    settingsPath = globalArgs.arg_settingsPath;
  else{
    std::cerr << "Missing option -p. Type \"ecalTest -h\" for help." << std::endl;
    exit(EXIT_FAILURE);
  }
  if(globalArgs.arg_outputFileName != NULL)
    outputFileName = globalArgs.arg_outputFileName;
  else{
    std::cerr << "Missing option -o. Type \"ecalTest -h\" for help." << std::endl;
    exit(EXIT_FAILURE);
  }
  if(globalArgs.arg_continuousMode != NULL)
    continuousMode = globalArgs.arg_continuousMode;

  unsigned int selectChannel = 1;
  unsigned int selectUplink = 1;

    // reading settings
    Settings* settings = Settings::instance();
    settings->setConfigPath(settingsPath);
    settings->setConfigFpgaOption(globalArgs.arg_force);
    settings->setDumpUplink(uplinkDumpNumber);


    //Setting up the UsbBoardManager
    UsbBoardManager usbBoardManager;
    usbBoardManager.initUsbBoards();
    usbBoardManager.setBusy(false); // only after all init is done, remove the busy for all boards


   //Change holdDelayTime if necessary
    if(globalArgs.arg_ChangeHoldDelayTime){
         usbBoardManager.setHoldDelayTime(globalArgs.arg_holdDelayTime);
    }
    
    // Change trigger type if option is given
    if(globalArgs.arg_changeTriggerType) {
		usbBoardManager.setTriggerType(globalArgs.arg_triggerType);
		std::cerr << "Trigger type has been changed!" << std::endl;
	}

    if(globalArgs.arg_readSensorMode){
        while(1){
              if(!usbBoardManager.readFrontEndBoardsSensors()){
                     std::cout << "Error while reading frontend baord sensors";
                     //  assert(false);
                     //getchar();
              }
              //getchar();
        }
    }


    //Light Injection Mode
    if(globalArgs.arg_ledInjectionMode){
           usbBoardManager.setLedInjectionModeOn(true);
           //usbBoardManager.setHoldDelayTime(globalArgs.arg_holdDelayTime);
           switch(globalArgs.arg_ledInjectionBoardSel){
               case 0: usbBoardManager.setLedOn(MPPC_BOARD_ID,1,8,true);  //set all MPPC FE boards ON
                       usbBoardManager.setLedOn(MAPD_BOARD_ID,1,8,false); //set all MAPD FE boards OFF
                       break;
               case 1:
					break;	// just set LED injection mode to true, the ones which are switched on are defined in the config files
               default: usbBoardManager.setLedOn(MAPD_BOARD_ID,1,8,true);
                        usbBoardManager.setLedOn(MPPC_BOARD_ID,1,8,false);
                       break;
           }
    }

    //Change lightIntenseByStep if necessary
    if(globalArgs.arg_ChangeledIntenseByStep){
        printf("change light intense: intense step =%d\n",globalArgs.arg_ChangeledIntenseStep);
        if(globalArgs.arg_ChangeledIntenseStep>0) usbBoardManager.IncreaseLedIntense(globalArgs.arg_ChangeledIntenseStep);
        else usbBoardManager.DecreaseLedIntense(-1*globalArgs.arg_ChangeledIntenseStep);
        //getchar();
    }

    //Check if we use the continuous mode
    if(globalArgs.arg_continuousMode != NULL)
      {
        if(sscanf(globalArgs.arg_continuousMode, "%d_%d", &selectUplink, &selectChannel) == 2){
            std::cerr << "Select Uplink " << selectUplink <<" Channel " << selectChannel << std::endl;
            usbBoardManager.runContinuousMode(selectUplink,selectChannel); //runContinuousMode(unsigned int uplinkSlot, unsigned int channel)
            // getchar();
          }
          else{
            std::cerr << "There is something wrong with the -s option! Please check the command!" << std::endl;
            assert(false);
          }
          }

    //Pujiang*/
    RunDescription* runDescription = Settings::instance()->createRunDescription(123456, "test run.");
    //runDescription->setMonitors(monitors);

    if(settings->GetSensorInfos() != ""){
        runDescription->setSensorInfos(settings->GetSensorInfos());
    }

    runDescription->dump(std::cerr);

    //Setting up the eventsPerAccess events to be saved in the tree
    typedef MergedEvent* MergedEventPtr;
    MergedEventPtr* event = new MergedEventPtr[Settings::instance()->eventsPerAccess()];
    for (int i = 0; i < Settings::instance()->eventsPerAccess(); ++i) {
        event[i] = settings->createMergedEvent();
    }

    MergedEvent* eventPointer = 0;

#ifndef NO_ROOT
    //Setting up the data tree
    TFile treeFile(outputFileName.c_str(), "RECREATE");
    TTree tree("ReadoutData", "UsbBoardReadoutData");
    // call the GetUserInfo only when the first event is received
    //tree.GetUserInfo()->Add(runDescription);
    tree.Branch("events", &eventPointer);
    //Setting up the data tree for online monitoring
    // use same filename with online in the filename
    TFile *treeFileOnline;
    TTree *treeOnline;

#endif

    //Reading data
    time_t startTime = time(0);

    // for check keyboard input

    int ret;
    struct timeval tv;
    fd_set rdfds;
    tv.tv_sec =0;
    tv.tv_usec =50;

    int runLoopNum = 1;
    if(globalArgs.arg_continuousChangingDAC) runLoopNum = CHANGE_DAC_RUN_TIMES; //HAO


         // before readout data from fifo, resetDaq first.
         usbBoardManager.resetDaq();
         std::cerr<<" reset all the UsbBoard DAQ " << std::endl;
         std::cerr<<" begin to read data, waiting for trigger.... "<<std::endl;
         printf("\n");
         printf("\n");

         // declaration some variable for online data
         int eventOnlineCnt=0;
         int onlineEventFileCnt = 0;
         std::ostringstream os;
         std::string onlineFileName;



         os << outputFileName << "_daily_0.root";
         onlineFileName = os.str();

         treeFileOnline = NULL;
         treeOnline =NULL;
    for(int runIndex=0; runIndex<runLoopNum; runIndex++){
uint32_t TMPtriggerTimeStamp[2]={0,0};
         for (unsigned long eventCounter = 0; eventCounter < nEvents; eventCounter+= Settings::instance()->eventsPerAccess()) {

              if(eventCounter%1500==0){    // guido 11.2012
              //if(1){
                  //usbBoardManager.readFpgaRegister(0x1000);  // trig select
                  //usbBoardManager.readFpgaRegister(0x1002);  // bit 1 busy switch
                  //usbBoardManager.readFpgaRegister(0x1004);  // event size
                  usbBoardManager.readFpgaRegister(0x1003);  // fifo used
                   //printf("LSB Time stamp:\t");
                  //usbBoardManager.readFpgaRegister(0x1033);  //  LSB Time stamp
                  //printf("MSB Time stamp:\t");
                  //usbBoardManager.readFpgaRegister(0x1034);  //  MSB Time stamp
                  //usbBoardManager.readFpgaRegister(0x1035);  //  LSB is busy
                  //printf("Event ID:\n");
 		  //usbBoardManager.readFpgaRegister(0x1005);  //  Event ID
                  printf("Trig cnt:\n");
                  usbBoardManager.readFpgaRegister(0x1010);  //  Ext trigger counter
                  printf("eventCounter = %d\n",eventCounter);
             }
               int stopDAQ = usbBoardManager.takeEvents(event,TMPtriggerTimeStamp);
              // stopDAQ is set 1 on a keystroke when waiting for events in usb board
              if(stopDAQ!=0) {
                   #ifndef NO_ROOT
                   treeFile.cd();
                   tree.Write();
                   treeFile.Close();
                   treeFileOnline->cd();
                   treeOnline->Write();
                   delete treeOnline;
                   treeFileOnline->Close();
                   delete treeFileOnline;
                   #endif
                   assert(false);
              }

              for (int i = 0; i < Settings::instance()->eventsPerAccess(); ++i) {
              // detect if the online file needs to be reopend
              if(!treeFileOnline && (runIndex == 0)){
                   treeFileOnline = new TFile(onlineFileName.c_str(), "RECREATE");
                   treeOnline = new TTree("ReadoutData", "UsbBoardReadoutData");
                   treeOnline->GetUserInfo()->Add(runDescription);
                   treeOnline->Branch("events", &eventPointer);
              }
              eventPointer = event[i];
              int eventNumber = eventPointer->eventNumber();
              // write user info and time to root file when first events are received
              if(eventNumber==0) {
                   runDescription->setTimeStamp(time(0));
                   tree.GetUserInfo()->Add(runDescription);}
                   // printing event number on console
                   if(!(eventNumber%1000)) {
                        time_t timeNow;
                        time(&timeNow);
                        std::cerr << "Event " << eventNumber << " name =" << outputFileName <<" " << ctime(&timeNow);
                   }
#ifdef NO_ROOT
                   eventPointer->dump(std::cerr);
#else
                   //eventPointer->dump(std::cerr);
                   tree.Fill();
                  // monitor 1 event in 100 and write it to the online file
                  /////// 1       Guido change this parameter for monitoring
                   if(runLoopNum==1) {
                     if(!(eventNumber%1)) {
                         eventOnlineCnt++;
                         treeOnline->Fill();
                     }
                     /////// 2       Guido change this parameter for monitoring
                     if(eventOnlineCnt==30000) {  // for cosmics about 1day
                       // close file and reopen new file
                         treeFileOnline->cd();
                         treeOnline->Write();
                         treeOnline->GetUserInfo()->Clear();
                         delete treeOnline;
                         treeOnline = NULL;
                         treeFileOnline->Close();
                         delete treeFileOnline;
                         treeFileOnline = NULL;

                         onlineEventFileCnt ++;
                         eventOnlineCnt=0;
                         os.str(std::string());
                         os << outputFileName << "_daily_"<< onlineEventFileCnt <<".root";
                         onlineFileName = os.str();
                         std::cerr<<"onlineFileName = "<<onlineFileName<<std::endl;
                      }

                    } // end if runLoopNum=1
#endif
                     //check key board, if there is an input, break and exit
                     FD_ZERO(&rdfds);
                     FD_SET(0,&rdfds);
                     ret = select(1,&rdfds,NULL,NULL,&tv);
                     //std::cerr<<"ret:"<<ret<<std::endl;
                     //if(ret!=0)
                     if(0){
                          #ifndef NO_ROOT
                          treeFile.cd();
                          tree.Write();
                          treeFile.Close();
                          treeFileOnline->cd();
                          treeOnline->Write();
                          delete treeOnline;
                          treeFileOnline->Close();
                          delete treeFileOnline;
                          #endif
                          assert(false);
                      }
                 ////////////////////////////////////////////////////////////////////
               }
           }

           if(globalArgs.arg_continuousChangingDAC){
                  usbBoardManager.setBusy(true);
                  if(globalArgs.arg_stepDACforContinuousChaningDAC >0) {
                      printf("stepDAC>0 \n");
                      usbBoardManager.IncreaseLedIntense(globalArgs.arg_stepDACforContinuousChaningDAC);                      
                  }else {
                      printf("stepDAC<0 \n");
                      usbBoardManager.DecreaseLedIntense(-1*globalArgs.arg_stepDACforContinuousChaningDAC);                      
                  }

                  printf("changing light intensity....busy is set,waiting 3s....\n");
                  sleep(3);
                  printf("New run..\n");
                  usbBoardManager.setBusy(false);
           }

    }
    time_t endTime = time(0);
    std::cerr << "time: " << (endTime-startTime) << " sec" << std::endl;
    std::cerr << "rate: " << float(nEvents)/(endTime-startTime) << " Hz" << std::endl;

#ifndef NO_ROOT
    treeFile.cd();
    tree.Write();
    tree.GetUserInfo()->Clear();
    treeFile.Close();
    printf("treeFile closed!\n");
    // online data file
//    treeFileOnline->cd();
//    treeOnline->Write();
//    //treeOnline->GetUserInfo()->Clear();
//    delete treeOnline;
//    treeFileOnline->Close();
//    delete treeFileOnline;
#endif

    for (int i = 0; i < Settings::instance()->eventsPerAccess(); ++i)
        delete event[i];
    delete[] event;
    //Pujiang
     /*  if (inputChangeTime<17){
          inputChangeTime ++;
          goto CycleMark;
        }*/

    return 0;
}
