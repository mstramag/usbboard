#CONFIG += release
CONFIG += debug

CONFIG -= qt

exists($(USBBOARDPATH)) {
    include($(USBBOARDPATH)/usbBoard.pri)
    include($(USBBOARDPATH)/RootCInt.pri)
    USBBOARDPATH=$(USBBOARDPATH)

} else {
    include(../../usbBoard.pri)
    include(../../RootCInt.pri)
    USBBOARDPATH=../..
}

SOURCES += \
    ecalTest.cpp

DEPENDPATH += \
    $$USBBOARDPATH/readout \
    $$USBBOARDPATH/event

INCLUDEPATH+= \
    $$DEPENDPATH

unix {
    LIBS += -L$$USBBOARDPATH/Builds -lusbBoardEvent -lusbBoardReadout

    !NO_QUSB {
        LIBS += -L$$USBBOARDPATH/support -lquickusb
    }
}

win* {
    LIBS += -L..$${QMAKE_DIR_SEP}..$${QMAKE_DIR_SEP}Builds -lusbBoardReadout
    CONFIG += console embed_manifest_exe
}
