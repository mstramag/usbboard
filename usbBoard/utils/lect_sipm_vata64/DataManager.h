#ifndef DataManager_h
#define DataManager_h

#include <vector>
#include <TH1.h>
#include <TF1.h>
#include <TH3.h>
#include <TGraphErrors.h>
#include <TLine.h>
#include <TText.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>
#include <map>
#include <stdint.h>
#include <TTree.h>
//#define DUMP_DEBUG

using namespace std;

class MergedEvent;
class TH1;
class TH2;
class TH3;
class TH2F;
class TFile;
class TTree;
class TProfile;

typedef std::map<int,int> SuperLayerMap;    //map<positon,uplinkId>

enum SuperLayerPosition{
    P1_P = 0,
    P1_N = 1,
    P2_P = 2,
    P2_N = 3,
    P3_P = 4,
    P3_N = 5,
    P4_P = 6,
    P4_N = 7,
    P5_P = 8,
    P5_N = 9,
    P6_P = 10,
    P6_N = 11,
    P7_P = 12,
    P7_N = 13,
    P8_P = 14,
    P8_N = 15,
    P9_P = 16,
    P9_N = 17,
    P10_P = 18,
    P10_N = 19
};

typedef struct {
  int first_channel;
  std::vector<float> PE;
} Cluster;

class DataManager {
public:

    const static unsigned short maxAdcValue = 4096;
    const static unsigned short SIPM_READOUT_CHANNELS = 128;


    long counter = 0;

    DataManager(std::string pathToSetupFile, std::vector<std::string> telescope_layers={});
    ~DataManager();

    void fill_raw_pedestals(MergedEvent* event);
    void pedestals_write_Mean_RMS();

    void draw();
    void SaveAllHistos();

    void GetRawsignalADCdata_1event(MergedEvent* event, std::vector<std::vector<int> > &eventADC);
    void GetsignalADC_PedSub(std::vector<std::vector<int> > &in_eventADC, std::vector<std::vector<int> > &out_eventADC_PS);

    // Clustering tools
    void ConfigHistoTH1(TH1* Histo, TString Title, TString axeXname, TString axeYname, double axeXmin, double axeXmax, double axeYmin, double axeYmax, double LineWidth);
    void ConfigHistoTH2(TH2* Histo, TString Title, TString axeXname, TString axeYname, Double_t axeXmin, Double_t axeXmax, double axeYmin, double axeYmax);
    void Cluster_Search(std::vector<std::vector<int> > &eventADCin, float threshold[3], int it,  unsigned int thrs_id=0);
    void Cluster_Plot_Event(std::vector<std::vector<int> > &eventADCin, float threshold[3], unsigned int ev_id);
    void Cluster_DrawHisto(bool noise_cluster_option=false);
    void Cluster_FillTree();
    void Cluster_InitBranches();
    void Add_Cluster_Histos(unsigned int n_add);

    void tracking(int evnum =-1);

    uint16_t GetChannelIndexOfSipm128(uint16_t channelIndexOnVata64Chip);
    int Get_NumberChannels();
    int Get_NumberSIPM();
    
    void printSipmInfos();
    
    

private:

    unsigned int uplinkIndex(int uplinkId);

    std::vector< std::vector<int> > m_Sipm_uplinkId;
    std::vector<float *> m_pedestal_Mean;
    std::vector<float *> m_pedestal_RMS;

    std::vector<TH2*> m_raw_pedestals;
    std::vector<TH2*> m_adcHistogram;
    std::vector<TH2*> m_PS_adcHistogram;

    /**********/

    int m_NumberChannels;
    int m_NumberOfSipm;
    std::vector<float> m_SipmGains;
    std::vector<std::string> m_SipmNames;

    int m_NbCluster_Tot_buff;
    TH1I* m_NbCluster_Tot;
    TH1I* m_ClusterSize_Tot;
    std::vector<TH1*> m_NbCluster;
    std::vector<TH1*> m_ClusterSize;
    std::vector<TH1*> m_ClusterADC;
    std::vector<TH1D*> m_ClusterChannel;
    std::vector<TH1D*> m_ClusterSum;
    std::vector< std::vector<TH1*> > m_EventCluster;
    std::vector<TH2D*> m_ClusterDisplay;
    
    std::vector<int> m_Nabove05;
    std::vector<int> m_Nabove15;
    std::vector<int> m_Nabove25;

    TTree * m_TrackingTree;
    std::vector<double> ClusterPosition;
    std::vector<int> SiPMNum;
    int EventNumber;

    TFile* file_data;

    std::string m_pathToFormerCfgFile; // only the path to the folder, the file name is generated automatically .
    bool m_forceToChangeCfgFile;

    SuperLayerMap m_superLayerMap;
    bool m_readInSetupFile;

};

#endif
