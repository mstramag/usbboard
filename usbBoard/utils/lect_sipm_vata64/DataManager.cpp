#include "DataManager.h"
#include <Settings.h>

#include <assert.h>
#include <iostream>

#include <MergedEvent.h>
#include <UsbBoardConfiguration.h>

#include <TH1.h>
#include <TH2.h>
#include <TH2F.h>
#include <TF1.h>
#include <TH3.h>
#include <TProfile.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TMath.h>
#include <TObjArray.h>
#include <TLegend.h>
#include <TGraph.h>

#include <TRandom.h>
#include <TSpectrum.h>
#include <TVirtualFitter.h>
#include <TROOT.h>
#include <TStyle.h>

DataManager::DataManager(std::string pathToSetupFile, std::vector<std::string> telescope_layers){
    char tmp[128];
    Settings* settings = Settings::instance();

    m_forceToChangeCfgFile = false;
    m_readInSetupFile = true;
    // read set up from the setupFile
    std::ifstream setupFile(pathToSetupFile.c_str());
    if(!setupFile){
        std::cerr << "Failure: could not open file: \"" << pathToSetupFile << "\"." << std::endl;
        std::cerr << "Please check if the path is correct or not!" << std::endl;
        assert(false);
    }

    std::string s;
    int temp;
    float gain;
    while (true) {
        std::getline(setupFile, s);
        if (setupFile.eof())
            break;
        const char* searchString = s.c_str();

        if (s.find("#") == 0 || s=="") {
            continue; // Skip commented lines
        }

        if(sscanf(searchString, "S1_UP0 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P1_P,temp));
        }

        if(sscanf(searchString, "S1_UP1 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P1_N,temp));
        }

        if(sscanf(searchString, "S2_UP0 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P2_P,temp));
        }

        if(sscanf(searchString, "S2_UP1 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P2_N,temp));
        }

        if(sscanf(searchString, "S3_UP0 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P3_P,temp));
        }

        if(sscanf(searchString, "S3_UP1 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P3_N,temp));
        }

        if(sscanf(searchString, "S4_UP0 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P4_P,temp));
        }

        if(sscanf(searchString, "S4_UP1 || %d ||",&temp )==1){
            m_superLayerMap.insert(SuperLayerMap::value_type(P4_N,temp));
        }

	if(sscanf(searchString, "S5_UP0 || %d ||",&temp )==1){
	  m_superLayerMap.insert(SuperLayerMap::value_type(P5_P,temp));
        }

        if(sscanf(searchString, "S5_UP1 || %d ||",&temp )==1){
	  m_superLayerMap.insert(SuperLayerMap::value_type(P5_N,temp));
        }

        if(sscanf(searchString, "S6_UP0 || %d ||",&temp )==1){
	  m_superLayerMap.insert(SuperLayerMap::value_type(P6_P,temp));
        }

        if(sscanf(searchString, "S6_UP1 || %d ||",&temp )==1){
	  m_superLayerMap.insert(SuperLayerMap::value_type(P6_N,temp));
        }

        if(sscanf(searchString, "S7_UP0 || %d ||",&temp )==1){
	  m_superLayerMap.insert(SuperLayerMap::value_type(P7_P,temp));
        }

        if(sscanf(searchString, "S7_UP1 || %d ||",&temp )==1){
	  m_superLayerMap.insert(SuperLayerMap::value_type(P7_N,temp));
        }

        if(sscanf(searchString, "S8_UP0 || %d ||",&temp )==1){
	  m_superLayerMap.insert(SuperLayerMap::value_type(P8_P,temp));
        }

        if(sscanf(searchString, "S8_UP1 || %d ||",&temp )==1){
	  m_superLayerMap.insert(SuperLayerMap::value_type(P8_N,temp));
        }

        if(sscanf(searchString, "S9_UP0 || %d ||",&temp )==1){
          m_superLayerMap.insert(SuperLayerMap::value_type(P9_P,temp));
        }

        if(sscanf(searchString, "S9_UP1 || %d ||",&temp )==1){
          m_superLayerMap.insert(SuperLayerMap::value_type(P9_N,temp));
        }

        if(sscanf(searchString, "S10_UP0 || %d ||",&temp )==1){
          m_superLayerMap.insert(SuperLayerMap::value_type(P10_P,temp));
        }

        if(sscanf(searchString, "S10_UP1 || %d ||",&temp )==1){
          m_superLayerMap.insert(SuperLayerMap::value_type(P10_N,temp));
        }

        if(sscanf(searchString, "S1_GAIN || %f ||",&gain )==1){
            m_SipmGains.push_back(gain);
        }

        if(sscanf(searchString, "S2_GAIN || %f ||",&gain )==1){
            m_SipmGains.push_back(gain);
        }

        if(sscanf(searchString, "S3_GAIN || %f ||",&gain )==1){
            m_SipmGains.push_back(gain);
        }

        if(sscanf(searchString, "S4_GAIN || %f ||",&gain )==1){
            m_SipmGains.push_back(gain);
        }
	if(sscanf(searchString, "S5_GAIN || %f ||",&gain )==1){
	  m_SipmGains.push_back(gain);
        }

        if(sscanf(searchString, "S6_GAIN || %f ||",&gain )==1){
	  m_SipmGains.push_back(gain);
        }

        if(sscanf(searchString, "S7_GAIN || %f ||",&gain )==1){
	  m_SipmGains.push_back(gain);
        }

        if(sscanf(searchString, "S8_GAIN || %f ||",&gain )==1){
	  m_SipmGains.push_back(gain);
        }

	if(sscanf(searchString, "S9_GAIN || %f ||",&gain )==1){
          m_SipmGains.push_back(gain);
        }

        if(sscanf(searchString, "S10_GAIN || %f ||",&gain )==1){
          m_SipmGains.push_back(gain);
        }
    }

    SuperLayerMap::iterator superLayer_iter;
    std::vector<int> uplinksToBeAnalyzed;

//    for(int superLayerIndex = 0;superLayerIndex<2;superLayerIndex++){
        for(superLayer_iter= m_superLayerMap.begin();superLayer_iter!=m_superLayerMap.end();superLayer_iter++){
            if(superLayer_iter->second!=0) {
                uplinksToBeAnalyzed.push_back(superLayer_iter->second);
			}
        }
//    }
	
	m_NumberOfSipm = uplinksToBeAnalyzed.size() / 2;
    for (int it =0; it < m_NumberOfSipm; ++it){

        std::vector<int> uplinkId;

        uplinkId.push_back(uplinksToBeAnalyzed[it*2]);
        uplinkId.push_back(uplinksToBeAnalyzed[it*2 + 1]);

        m_Sipm_uplinkId.push_back(uplinkId);
        
        if(0) { // default SiPM naming
	        char name[128];
	        sprintf(name,"%d",it);
	        std::string namestr(name);
	        m_SipmNames.push_back(namestr);
		}
		if(1) { // SiPM naming for telescope testbeam
			if(	uplinkId[0]==100 && uplinkId[1]==101 ) m_SipmNames.push_back("X1");
			if(	uplinkId[0]==102 && uplinkId[1]==103 ) m_SipmNames.push_back("Y1");
			if(	uplinkId[0]==104 && uplinkId[1]==105 ) m_SipmNames.push_back("X2");
			if(	uplinkId[0]==106 && uplinkId[1]==107 ) m_SipmNames.push_back("Y2");
			if(	uplinkId[0]==120 && uplinkId[1]==121 ) m_SipmNames.push_back("X3"); // ketek 
			if(	uplinkId[0]==122 && uplinkId[1]==123 ) m_SipmNames.push_back("Y3"); // ketek
			if(	uplinkId[0]==124 && uplinkId[1]==125 ) m_SipmNames.push_back("X4");
			if(	uplinkId[0]==126 && uplinkId[1]==127 ) m_SipmNames.push_back("Y4");
			if(	uplinkId[0]==150 && uplinkId[1]==151 ) m_SipmNames.push_back("X5");
			if(	uplinkId[0]==152 && uplinkId[1]==153 ) m_SipmNames.push_back("Y5");
		}

        //std::cout << "uplinkId.at(0) = " << uplinkId.at(0) << "\tuplinkId.at(1) = " << uplinkId.at(1) << std::endl;

    }
    
    // If some telescope layers are specified, chnage the uplinkIds
    std::vector<int> new_uplinksToBeAnalyzed;
    std::vector< std::vector<int> > new_Sipm_uplinkId;
    std::vector<float> new_SipmGains;
    std::vector<std::string> new_SipmNames;
    
    if(telescope_layers.size()>0) {
		for(unsigned int l(0); l<telescope_layers.size(); ++l) {
			std::vector<int> uplinkId;
			
			if(telescope_layers[l] == "X1") {uplinkId.push_back(100); uplinkId.push_back(101);}
			if(telescope_layers[l] == "X2") {uplinkId.push_back(104); uplinkId.push_back(105);}
			if(telescope_layers[l] == "X3") {uplinkId.push_back(120); uplinkId.push_back(121);} // ketek
			if(telescope_layers[l] == "X4") {uplinkId.push_back(124); uplinkId.push_back(125);}
			if(telescope_layers[l] == "X5") {uplinkId.push_back(150); uplinkId.push_back(151);}
			
			if(telescope_layers[l] == "Y1") {uplinkId.push_back(102); uplinkId.push_back(103);}
			if(telescope_layers[l] == "Y2") {uplinkId.push_back(106); uplinkId.push_back(107);}
			if(telescope_layers[l] == "Y3") {uplinkId.push_back(122); uplinkId.push_back(123);} // ketek
			if(telescope_layers[l] == "Y4") {uplinkId.push_back(126); uplinkId.push_back(127);}
			if(telescope_layers[l] == "Y5") {uplinkId.push_back(152); uplinkId.push_back(153);}
			
			std::cout << "Telescope station: " << telescope_layers[l] << " with uplinkIds : " << uplinkId[0] << " and " << uplinkId[1] << std::endl;
			
			for (int it =0; it < m_NumberOfSipm; ++it) {
				if(m_Sipm_uplinkId[it][0]==uplinkId[0] && m_Sipm_uplinkId[it][1]==uplinkId[1]) {
					printf("TO BE ANALYSED: uplinkId = %d and %d (SiPM %d) | Gain = %.2f\n",uplinkId.at(0), uplinkId.at(1), it+1, m_SipmGains[it]);
					new_uplinksToBeAnalyzed.push_back(uplinkId[0]); new_uplinksToBeAnalyzed.push_back(uplinkId[1]); 
					new_Sipm_uplinkId.push_back(uplinkId);
					new_SipmGains.push_back(m_SipmGains[it]);
					new_SipmNames.push_back(telescope_layers[l]);
				}
			}
		}
		// Changes the parameters with the new ones
		uplinksToBeAnalyzed.clear();
		for(unsigned int i(0); i<new_uplinksToBeAnalyzed.size(); ++i) {
			uplinksToBeAnalyzed.push_back(new_uplinksToBeAnalyzed[i]);
		}
		m_Sipm_uplinkId.clear();
		for(unsigned int i(0); i<new_Sipm_uplinkId.size(); ++i) {
			std::vector<int> uplinkId;
			for(unsigned int k(0); k<new_Sipm_uplinkId[i].size(); ++k) {
				uplinkId.push_back(new_Sipm_uplinkId[i][k]);
			}
			m_Sipm_uplinkId.push_back(uplinkId);
		}
		m_SipmGains.clear();
		for(unsigned int i(0); i<new_SipmGains.size(); ++i) {
			m_SipmGains.push_back(new_SipmGains[i]);
		}
		m_SipmNames.clear();
		for(unsigned int i(0); i<new_SipmNames.size(); ++i) {
			m_SipmNames.push_back(new_SipmNames[i]);
		}
		m_NumberOfSipm = uplinksToBeAnalyzed.size() / 2;
		std::cout << "upLinkIds to be analysed have been updated given the options that you passed" << std::endl;
	}

    printf("\nDatamanager::uplinksToBeAnalyzed.size() = %d\n",uplinksToBeAnalyzed.size());
    printf("Datamanager::m_NumberOfSipm = %d\n\n",m_NumberOfSipm);

    
    for (unsigned int chip = 0; chip < m_Sipm_uplinkId.size(); ++chip) {

        std::vector<int> uplinkId;
        uplinkId = m_Sipm_uplinkId.at(chip);

        //printf("uplinkId = %d and %d (SiPM %d) | Gain = %.2f\n",uplinkId.at(0), uplinkId.at(1), chip+1, m_SipmGains[chip]);
        std::cout << "uplinkId = " << uplinkId.at(0) << " and " << uplinkId.at(1) << " (SiPM " << m_SipmNames[chip] << ") | Gain = " << m_SipmGains[chip] << std::endl;
        int usbBoardId = settings->usbBoardIdFromUplinkId(uplinkId.at(0));
        unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
        sampleSize = sampleSize * 2; // SiPM = 2 * VATA64
		
		//sprintf(tmp,std::string("SiPM128_adcHistogram_SiPM[") + m_SipmNames[chip] + "]");
        //sprintf(tmp, "SiPM128_adcHistogram_SiPM[%d]", chip+1);
        //TH2* adcHistogram = new TH2I(tmp, "SiPM128_adcHistogram", sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        TH2* adcHistogram = new TH2I(std::string("SiPM128_adcHistogram_SiPM[" + m_SipmNames[chip] + "]").c_str(), std::string("SiPM128_adcHistogram_SiPM[" + m_SipmNames[chip] + "]").c_str(), sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        adcHistogram->GetXaxis()->SetTitle("channel");
        adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_adcHistogram.push_back(adcHistogram);
        
        //sprintf(tmp, "SiPM128_PS_adcHistogram_SiPM[%d]", chip+1);
        //TH2* PS_adcHistogram = new TH2I(tmp, "SiPM128_PS_adcHistogram", sampleSize, .0, sampleSize, maxAdcValue+500, -500, maxAdcValue);
        TH2* PS_adcHistogram = new TH2I(std::string("SiPM128_PS_adcHistogram_SiPM[" + m_SipmNames[chip] + "]").c_str(), std::string("SiPM128_PS_adcHistogram_SiPM[" + m_SipmNames[chip] + "]").c_str(), sampleSize, .0, sampleSize, maxAdcValue+500, -500, maxAdcValue);
        PS_adcHistogram->GetXaxis()->SetTitle("channel");
        PS_adcHistogram->GetYaxis()->SetTitle("adcValue");
        m_PS_adcHistogram.push_back(PS_adcHistogram);

        //sprintf(tmp, "SiPM128_raw_pedestals_SiPM[%d]", chip+1);
        //TH2* raw_pedestals = new TH2I(tmp, "SiPM128_raw_pedestals", sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        TH2* raw_pedestals = new TH2I(std::string("SiPM128_raw_pedestals_SiPM[" + m_SipmNames[chip] + "]").c_str(), std::string("SiPM128_raw_pedestals_SiPM[" + m_SipmNames[chip] + "]").c_str(), sampleSize, .0, sampleSize, maxAdcValue, .0, maxAdcValue);
        raw_pedestals->GetXaxis()->SetTitle("channel");
        raw_pedestals->GetYaxis()->SetTitle("adcValue");
        m_raw_pedestals.push_back(raw_pedestals);

    }
    m_NbCluster_Tot = new TH1I("Cluster number per event", "Cluster number per event", 10, 4.5, 14.5);
    m_ClusterSize_Tot = new TH1I("Cluster size all Sipms", "Cluster size all Sipms", 15, -0.5, 14.5);

    // Clustering histograms (17 for different seed thresholds from 1.5 to 5.5 with 0.25 steps)
	//for(unsigned int step_nb(0); step_nb<17; ++step_nb) {
	for(unsigned int step_nb(0); step_nb<m_Sipm_uplinkId.size(); ++step_nb) {
		
		
		//sprintf(tmp, "Cluster size %d", step_nb);
		//sprintf(tmp, "Cluster size SiPM ", step_nb);
	    TH1* clusterSize_hist = new TH1I(std::string("Cluster size SiPM " + m_SipmNames[step_nb]).c_str(), std::string("Cluster size SiPM " + m_SipmNames[step_nb]).c_str(), 15, -0.5, 14.5);
	    m_ClusterSize.push_back(clusterSize_hist);
	    
	    //sprintf(tmp, "Cluster number %d", step_nb);
	    TH1* NbCluster_hist = new TH1I(std::string("Cluster number SiPM " + m_SipmNames[step_nb]).c_str(), std::string("Cluster number SiPM " + m_SipmNames[step_nb]).c_str(), 10  , -0.5, 9.5);
	    m_NbCluster.push_back(NbCluster_hist);
		
		//sprintf(tmp, "ClusterADC %d", step_nb);
	    TH1* ClusterADC_hist = new TH1I(std::string("ClusterADC SiPM " + m_SipmNames[step_nb]).c_str(), std::string("ClusterADC SiPM " + m_SipmNames[step_nb]).c_str(), maxAdcValue, 0, maxAdcValue);   //+500
	    m_ClusterADC.push_back(ClusterADC_hist);
	
		//sprintf(tmp, "Cluster channel %d", step_nb);
	    const unsigned short maxPos = SIPM_READOUT_CHANNELS * m_NumberOfSipm;
	    TH1D* ClusterChannel_hist = new TH1D(std::string("Cluster channel SiPM " + m_SipmNames[step_nb]).c_str(), std::string("Cluster channel SiPM " + m_SipmNames[step_nb]).c_str(), maxPos , 0.5, maxPos + 0.5);
	    m_ClusterChannel.push_back(ClusterChannel_hist);
	    
	    //sprintf(tmp, "Cluster Sum %d", step_nb);
	    TH1D* ClusterSum = new TH1D(std::string("Cluster sum SiPM " + m_SipmNames[step_nb]).c_str(), std::string("Cluster sum SiPM " + m_SipmNames[step_nb]).c_str(), 1000, 0, 1000);
	    m_ClusterSum.push_back(ClusterSum);
	    
	    //sprintf(tmp, "Cluster Display %d", step_nb);
	    TH2D* ClusterDisplay = new TH2D(std::string("Cluster display SiPM " + m_SipmNames[step_nb]).c_str(), std::string("Cluster display SiPM " + m_SipmNames[step_nb]).c_str(), 15, -7.5, 7.5, 1000, 0, 30);
	    m_ClusterDisplay.push_back(ClusterDisplay);

    m_TrackingTree = new TTree("TrackingTree","TrackingTree");
    m_TrackingTree->Branch("ClusterPosition", &ClusterPosition);
    m_TrackingTree->Branch("SiPMNum", &SiPMNum);
    m_TrackingTree->Branch("EventNumber",&EventNumber);


	    
	}
	
	// For measurement of DCR and X-talk
    for(unsigned int i(0); i<SIPM_READOUT_CHANNELS * m_NumberOfSipm; ++i) {
		m_Nabove05.push_back(0);
		m_Nabove15.push_back(0);
		m_Nabove25.push_back(0);
	}

}

DataManager::~DataManager(){
    //fclose(m_resultLog);
}

void DataManager::printSipmInfos() {
	std::cout << std::endl;
	std::cout << "Infos for SiPMs selected:" << std::endl;
	for(unsigned int i(0); i<m_NumberOfSipm; ++i) {
		std::cout << "SiPM " << m_SipmNames[i] << ": ";
		std::cout << "uplinks " << m_Sipm_uplinkId[i][0] << " and " << m_Sipm_uplinkId[i][1] << ", ";
		std::cout << "gain " << m_SipmGains[i] << std::endl;
	}
	std::cout << std::endl;
	return;
}

void DataManager::fill_raw_pedestals(MergedEvent* event)
{
    Settings* settings = Settings::instance();
    std::vector<int> uplinkId;

    unsigned short numberOfReadoutChips = m_Sipm_uplinkId.size();

    for (unsigned int chip = 0; chip < numberOfReadoutChips; ++chip) {

        uplinkId = m_Sipm_uplinkId.at(chip);

        std::vector<int>::iterator uplinkIdIt = uplinkId.begin();
        std::vector<int>::iterator uplinkIdItEnd = uplinkId.end();

        static int nn = 1;

        for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i) {
            int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
            //            unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize(); //64
            unsigned short channelsPerReadoutChip = settings->usbBoardConfiguration(usbBoardId).channelsPerReadoutChip(); //64
            //            unsigned short numberOfReadoutChips = sampleSize * 2/ channelsPerReadoutChip; //2
            const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);

            //           for (unsigned short chip = 0; chip < numberOfReadoutChips; ++chip) {
            for (unsigned short channel = 0; channel < (channelsPerReadoutChip); ++channel) {
                unsigned short sample = i * channelsPerReadoutChip + channel;
                //std::cout << i << " " << chip << " " << channel << " " << sample << " " << channelsPerReadoutChip << std::endl;
                //std::cout << *(m_raw_pedestals[i] + i) << std::endl;
                m_raw_pedestals[chip]->Fill(GetChannelIndexOfSipm128(sample) - 1 , uplinkData.adcValue(channel));
                counter++;
                //std::cout << "  ** " << uplinkData.adcValue(channel) << std::endl;
                //std::cout << uplinkData.adcValue(sample) << " " << m_raw_pedestals[i] << " " << *(m_raw_pedestals[i]+nn+sample) << " ";
                //std::cout << std::endl;

            }
            //}
        }
        nn++;
    }
}

void DataManager::pedestals_write_Mean_RMS()
{

    float * pedestal_Mean;
    float * pedestal_RMS;

    unsigned short channelsPerReadoutChip = SIPM_READOUT_CHANNELS;
    unsigned short numberOfReadoutChips = m_Sipm_uplinkId.size();

    for (unsigned int chip = 0; chip < numberOfReadoutChips; ++chip) {
        pedestal_Mean = new float[channelsPerReadoutChip];
        pedestal_RMS = new float[channelsPerReadoutChip];

        for (unsigned short channel = 0; channel <channelsPerReadoutChip; ++channel) {
            pedestal_Mean[channel] = m_raw_pedestals[chip]->ProjectionY("p1",channel+1,channel+1,"")->GetMean();
            pedestal_RMS[channel] = m_raw_pedestals[chip]->ProjectionY("p2",channel+1,channel+1,"")->GetRMS();
        }

        m_pedestal_Mean.push_back(pedestal_Mean);
        m_pedestal_RMS.push_back(pedestal_RMS);

    }
}

void DataManager::draw()
{
    char tmp[256];
    TCanvas* canvas = NULL;
    std::vector<int> uplinkId;

    double mean = 0;
    double rms = 0;

    std::cout << "\n*****>>> DataManager::" << __func__ << "() *****" << std::endl;

    for(unsigned int i=0;i<m_Sipm_uplinkId.size();++i){


        uplinkId = m_Sipm_uplinkId.at(i);

        sprintf(tmp,"Physical SiPM128 Order from uplink %d and %d",uplinkId.at(0),uplinkId.at(1));
        std::string ctitlestr(tmp);
        std::string ctitle = ctitlestr + " (SiPM " + m_SipmNames[i] + ")";
        canvas = new TCanvas(ctitle.c_str(),ctitle.c_str(),0,0,1400,1000);
        canvas->Divide(1,3);
        canvas->cd(1);
        m_raw_pedestals[i]->Draw("COLZ");
        mean = m_raw_pedestals[i]->GetMean(2);
        rms = m_raw_pedestals[i]->GetRMS(2);
        m_raw_pedestals[i]->GetYaxis()->SetRangeUser(mean - 10*rms, mean + 10*rms);

        canvas->GetPad(1)->SetLogz();

        canvas->cd(2);
        m_adcHistogram[i]->Draw("COLZ");
        mean = m_adcHistogram[i]->GetMean(2);
        rms = m_adcHistogram[i]->GetRMS(2);
        m_adcHistogram[i]->GetYaxis()->SetRangeUser(300, mean + 10*rms);
        canvas->GetPad(2)->SetLogz();

        canvas->cd(3);
        m_PS_adcHistogram[i]->Draw("COLZ");
        mean = m_PS_adcHistogram[i]->GetMean(2);
        rms = m_PS_adcHistogram[i]->GetRMS(2);
        canvas->GetPad(3)->SetLogz();
    }
}

void DataManager::SaveAllHistos(){
    char tmp[128];
    std::vector<int> uplinkId;
    uplinkId = m_Sipm_uplinkId.at(0);
    sprintf(tmp,"/home/it/usbboard/usbBoard/data_TB/lect_sipm_result.root"); 
    TFile* recordFile =  new TFile(tmp,"RECREATE");
    m_NbCluster_Tot->Write();
    m_ClusterSize_Tot->Write();

    m_TrackingTree->Write();
    for(int i =0; i< m_NumberOfSipm; i++){
      m_NbCluster[i]->Write();
      // m_ClusterADC[0]->Write();                                                                                                                          
      m_ClusterChannel[i]->Write();
      m_ClusterSize[i]->Write();
      m_ClusterSum[i]->Write();
    }
    for(unsigned int i=0;i<m_Sipm_uplinkId.size();++i){
        m_raw_pedestals[i]->Write();
        m_adcHistogram[i]->Write();
        m_PS_adcHistogram[i]->Write();
    }

    recordFile->Close();

}

uint16_t DataManager::GetChannelIndexOfSipm128(uint16_t channelIndexOnVata64Chip){

    uint16_t ch_Sipm128 = 0;
	
	    int Reorder_bychannel[128] = {  2,10,18,26,34,42,50,58,66,74,82,90,98,106,114,122,
	                                    6,14,22,30,38,46,54,62,70,78,86,94,102,110,118,126,
	                                    4,12,20,28,36,44,52,60,68,76,84,92,100,108,116,124,
	                                    8,16,24,32,40,48,56,64,72,80,88,96,104,112,120,128,
	                                    1, 9,17,25,33,41,49,57,65,73,81,89,97,105,113,121,
	                                    5,13,21,29,37,45,53,61,69,77,85,93,101,109,117,125,
	                                    3,11,19,27,35,43,51,59,67,75,83,91,99,107,115,123,
	                                    7,15,23,31,39,47,55,63,71,79,87,95,103,111,119,127};
	
	    ch_Sipm128 = Reorder_bychannel[channelIndexOnVata64Chip];

    return ch_Sipm128;
    //return channelIndexOnVata64Chip;

}

void DataManager::GetsignalADC_PedSub(std::vector<std::vector<int> > &in_eventADC, std::vector<std::vector<int> > &out_eventADC_PS)
{
    unsigned short numberOfReadoutChips = in_eventADC.size();

    for (unsigned int chip = 0; chip < numberOfReadoutChips; ++chip) {
        for (unsigned short channel = 0; channel < in_eventADC[chip].size(); ++channel) {
            out_eventADC_PS[chip][channel] = in_eventADC[chip][channel] - m_pedestal_Mean.at(chip)[channel];
            m_PS_adcHistogram[chip]->Fill(channel , out_eventADC_PS[chip][channel]);
        }
    }
}


void DataManager::GetRawsignalADCdata_1event(MergedEvent* event, std::vector<std::vector<int> > &eventADC)
{
    Settings* settings = Settings::instance();
    std::vector<int> uplinkId;

    unsigned short phyChannel;
    unsigned short numberOfReadoutChips = m_Sipm_uplinkId.size();

    for (unsigned int chip = 0; chip < numberOfReadoutChips; ++chip) {

        uplinkId = m_Sipm_uplinkId.at(chip);

        std::vector<int>::iterator uplinkIdIt = uplinkId.begin();
        std::vector<int>::iterator uplinkIdItEnd = uplinkId.end();


        for (unsigned int i = 0; uplinkIdIt != uplinkIdItEnd; ++uplinkIdIt, ++i)
        {
            int usbBoardId = settings->usbBoardIdFromUplinkId(*uplinkIdIt);
            unsigned short sampleSize = settings->usbBoardConfiguration(usbBoardId).sampleSize();
            const UplinkData& uplinkData = event->uplinkData(*uplinkIdIt);

            for (unsigned short channel = 0; channel < sampleSize; ++channel){
                phyChannel = GetChannelIndexOfSipm128(channel+i*sampleSize);
                eventADC[chip][phyChannel - 1] = uplinkData.adcValue(channel);
                m_adcHistogram[chip]->Fill(phyChannel - 1, uplinkData.adcValue(channel) );
            }
        }
    }
}

int DataManager::Get_NumberChannels()
{
    m_NumberChannels = SIPM_READOUT_CHANNELS;
    return m_NumberChannels;
}

int DataManager::Get_NumberSIPM()
{
    return m_NumberOfSipm;
}
