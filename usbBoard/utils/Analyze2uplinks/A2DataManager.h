#ifndef DataManager_h
#define DataManager_h

#include <vector>

class MergedEvent;
class TH1;
class TH2;
class TH3;
class TH2F;
class TFile;
class TTree;
class TProfile;
 
class DataManager {
    public:
        DataManager();
        ~DataManager();
        void addUplink(const int uplinksToBeAnalyzed);
        void fillEvent(MergedEvent* event);
        void draw(int firstChannel, int lastChannel);
	void getEnergy(MergedEvent* event);

        const static unsigned short maxAdcValue = 4095;
        const static unsigned short maxNumberPhotons = 50000;

 private:
        unsigned int uplinkIndex(int uplinkId);

        std::vector<int> m_uplinkId;
        std::vector<TH2*> m_adcHistogram;
        std::vector<TH2*> m_commonModeCorrectedAdcHistogram;
        std::vector<TH2*> m_uplink_41_vs_40;

	//public:
	TFile* file_data;       
};

#endif
