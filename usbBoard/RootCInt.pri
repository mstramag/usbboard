!contains(CONFIG, NO_ROOT) {
    # Cross-platform check for ROOT
    win* {
        ROOTDIR=$$system(echo %ROOTSYS%)
    } else {
        ROOTDIR=$(ROOTSYS)$${QMAKE_DIR_SEP}
    }

    ROOTINCDIR=$${ROOTDIR}include$${QMAKE_DIR_SEP}
    !exists($${ROOTINCDIR}TObject.h) {
        error("Could NOT find ROOT!")
    }

    ROOTBINDIR=$${ROOTDIR}bin$${QMAKE_DIR_SEP}
    
    win* {
        LIBS += -L$${ROOTDIR}lib \
                -llibCore -llibCint -llibReflex -llibRIO -llibNet -llibHist -llibGraf \
                -llibGraf3d -llibGpad -llibTree -llibRint -llibPostscript -llibMatrix \
                -llibPhysics -llibMathCore -llibThread

        LIBS += -include:_G__cpp_setupG__Net        \
                -include:_G__cpp_setupG__IO         \
                -include:_G__cpp_setupG__Hist       \
                -include:_G__cpp_setupG__Graf       \
                -include:_G__cpp_setupG__G3D        \
                -include:_G__cpp_setupG__GPad       \
                -include:_G__cpp_setupG__Tree       \
                -include:_G__cpp_setupG__Thread     \
                -include:_G__cpp_setupG__Rint       \
                -include:_G__cpp_setupG__PostScript \
                -include:_G__cpp_setupG__Matrix     \
                -include:_G__cpp_setupG__Physics
    } else {
        LIBS += $$system(root-config --cflags --libs)

    }

    INCLUDEPATH += $${ROOTINCDIR}

    !isEmpty(CREATE_ROOT_DICT_FOR_CLASSES) {
        for(class, CREATE_ROOT_DICT_FOR_CLASSES) {
            win* {
                VS_MAGIC="$(ProjectDir)"
                fixedClass=$${VS_MAGIC}$$replace(class, /, )
            } else {
                fixedClass=$$class
            }            

            CREATE_ROOT_DICT_FOR_CLASSES_MODIFIED+=$$fixedClass
        }

        rootcint.commands = $${ROOTBINDIR}rootcint -f ${QMAKE_FILE_OUT} -c -I.. ${QMAKE_FILE_IN}
        export(rootcint.commands)

        rootcint.input = CREATE_ROOT_DICT_FOR_CLASSES_MODIFIED
        export(rootcint.input)

        rootcint.output = $${OBJECTS_DIR}$${QMAKE_DIR_SEP}${QMAKE_FILE_BASE}Dict.cpp
        export(rootcint.output)

        rootcint.variable_out = GENERATED_SOURCES
        export(rootcint.variable_out)

        QMAKE_EXTRA_COMPILERS += rootcint
    }
}
