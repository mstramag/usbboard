include(usbBoard.pri)

exists($(USBBOARDPATH)) {
    USBBOARDPATH=$(USBBOARDPATH)
} else {
    USBBOARDPATH=.
}
!exists($$USBBOARDPATH/usbBoard.pro){
    error(Please set USBBOARDPATH environment variable!)
}

unix {
    !exists($(PWD)/support/libquickusb.so) {
        system("ln -s libquickusb.so.2.0.0 support/libquickusb.so")
        system("ln -s libquickusb.so.2.0.0 support/libquickusb.so.1.0.0")
        system("ln -s libquickusb.so.2.0.0 support/libquickusb.so.2")
    }
}

#CONFIG += release
CONFIG += debug

TEMPLATE = subdirs
SUBDIRS = event readout utils
CONFIG += ordered
