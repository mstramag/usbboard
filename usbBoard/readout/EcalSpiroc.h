#ifndef EcalSpiroc_h
#define ECalSpiroc_h

#include "FrontEndBoard.h"

#include <iostream>
#include <vector>
#include <string>
#include <unistd.h>

#include "../TypeDefs.h"

// Define
#define  FIRMWARE_VERSION_REG_ADDR       0x0000
#define  GENERAL_CTRL_REG0_ADDR          0x0001        
//bit0:reset_cfg_reg, bit1:start_cfg_reg
#define  GENERAL_CTRL_REG1_ADDR          0x0002     
//bit4:CLK_READ_ctrl, bit5:SRIN_READ_ctrl, bit6:RESETB_READ_ctrl, bit7:HOLDB_Backup_ctrl,
#define  GENERAL_CTRL_REG2_ADDR          0x0003
#define  GENERAL_CTRL_REG3_ADDR          0x0004        
//bit0 : 1 c-code read access ; 0 USB_board read access, bit1:NPWDA, bit2:NPWDB
#define  GENERAL_CTRL_REG4_ADDR          0x0005        // unused
/////////////////////////////////////////////////////////////////////////////////////
////// in SPIROC-II we only use FIRMWARE_VERSION_REG and GENERAL_CTRL_REG0 //////////
//GENERAL_CTRL_REG0:
//bit0: reset_cfg_reg,    bit1: start_cfg_reg,
//bit2: CLK_READ_ctrl, bit3: SRIN_READ_ctrl, bit4: RESETB_READ_ctrl, bit5: HOLDB_Backup_ctrl,
//bit6: 0 c-code read access ; 1 USB_board read access, 
//bit7: NPWDA--Amplifier for Uplink disable, bit8: NPWDB--Amplifier for debug disable,
//bit9: external trigger enable,
//bit10: TESTPULSE_ctrl,
//bit14: SPIROC-II config done, bit15: SPIROC-II config error. these two bits are read only.
/////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// in SPIROC-A we still only use FIRMWARE_VERSION_REG and GENERAL_CTRL_REG0most bit definitions keep the same as SPIROC-II ///////////
///////Most bit definitions keep the same as SPIROC-II, but there are some small differences( marked with **)                  ///////////
////// Just change the name, function keep the same as before( marked with * )                                                 ///////////
/////  change the function ( marked with **)                                                                                   ///////////

//GENERAL_CTRL_REG0:
//bit0: reset_cfg_reg,     bit1: start_cfg_reg,
//bit2: CLK_READ_spi(*)    bit3: SRIN_READ_spi(*)  bit4: RESETB_READ_spi(*)  bit5: HOLDB_Backup_spi(*)
//bit6: select_for_read     0= c-code read access ; 1= USB_board read access, 
//bit7: select_sc_nPROBE(**)
//bit8&9: TRIG_SELECT(**)
//bit10: TESTPULSE_spi(*) 
//bit12: LG_EN_spi(**)
//bit13: HG_EN_spi(**)
//bit14: config done   (read only)
//bit15: config error. (read only)
/////////////////////////////////////////////////////////////////////////////////////

#define  SPIROC_CFG_RAM_BADDR            0x4000
#define  SPIROC_CFG_RB_RAM_BADDR         0x4080

// UFM CMD
#define UFM_CMD_WREN     0x60            //enable write to UFM
#define UFM_CMD_WRDI     0x04            //----disable write to UFM
#define UFM_CMD_RDSR     0x05            //----read status register
#define UFM_CMD_WRSR     0x01            //----write status register
#define UFM_CMD_READ     0x03            //----read data from UFM
#define UFM_CMD_WRITE    0x02            //----write data to UFM
#define UFM_CMD_SERASE   0x40            //----sector erase
#define UFM_CMD_BERASE   0x06            //----erase the entire UFM block(both sectors)

//QUSB SPI switch CMD
#define UFM_ERASE        0xD3
#define UFM_READ_SR      0xB5
#define UFM_WRITE        0x81
#define UFM_READ         0x42
#define UFM_READ_WORD    0x4A
#define SC_REGISTER      0x60

/* // */
/* #define  SPIROC_ON_PORTA                     0xBE // PA6='0' and PA5 ='1' ,    */
/* #define  SPIROC_ON_PORTE                     0x04 // PE6='0' and pE2 ='1' */
/* #define  SPIROC_channel_select               0xBD // 0xBD - nSS3,0xBB - nSS4,0xB7 - nSS5,0xAF - nSS6      PA0 <=> reset */
/* #define  SPIROC_portA_dir                    0xFF */
/* #define  SPIROC_portE_dir                    0x44 */

//
#define  SPItoUFM             0xC3C3
#define  SPItoREGISTER        0x3C3C


struct SPIROC_A_CFG_TYPE
{
    int EN_input_dac;
    int DAC_reference;
    int input_8bits_DAC[32];
    int input_DAC_off[32];
    int low_gain_PA_bias;
    int high_gain_preamplifier_PP;
    int EN_high_gain_PA;
    int low_gain_preamplifier_PP;
    int EN_low_gain_PA;
    int capacitor_HG_PA_comp;
    int capacitor_HG_PA_fdbck;
    int capacitor_LG_PA_fdbck;
    int capacitor_LG_PA_comp;
    int preamp_in_calib_cofig[32];
    int low_gain_slow_shaper_PP;
    int EN_low_gain_slow_shaper;
    int time_constant_LG_shaper;
    int high_gain_slow_shaper_PP;
    int EN_high_gain_slow_shaper;
    int time_constant_HG_shaper;
    int fast_shapers_follower_PP;
    int EN_fast_shaper;
    int fast_shaper_PP;
    int T_H;
    int EN_T_H;
    int T_H_bias;
    int EN_discri;
    int discriminator_PP;
    int RS_or_discri;
    int discriminator_mask[32];
    int DAC;
    int DAC_slope;
    int DAC_PP;
    int EN_DAC;
    int bandgap_PP;
    int EN_bandgap;
    int high_gain_OTAq_PP;
    int low_gain_OTAq_pp;
    int porbe_OTAq_PP;
    int LVDS_rec_PP;
    int EN_high_gain_OTAq;
    int EN_low_gain_OTAq;
    int EN_probe_OTAq;
    int EN_LVDS_rec;
    int EN_out_dig;
    int EN_OR32;
    int EN_32_trigger;
    int NC;
};

// -----------------   EcalSpiroc Configuration  ---------------------
class EcalSpirocConfiguration : public FrontEndBoardConfiguration {
    public:
        EcalSpirocConfiguration(int slotNo,int uplinkId);
        ~EcalSpirocConfiguration();

        void dump(std::ostream& stream = std::cerr) const;
        bool loadFromString(const char*);

        int selectForRead() const;
        void setSelectForRead(int selectForRead);

        int selectScNprobe() const;
        void setSelectScNprobe(int selectScNprobe);
        
        int trigSelect() const;
        void setTrigSelect(int trigSelect);

        void setSlowControlCfgFile(const std::string& fileName);
        const std::string& slowControlCfgFile() const;

        unsigned short m_FEchipConfigArray[80];

        bool loadConfig(const std::string& filename); 
        bool loadFromStream(std::istream& stream);

        void convertCfgToVector(unsigned char temp[456]);

        void writeToStream(std::ostream& stream) const;

        void convertVectorTo16bitsArray(int dumpOn,unsigned char temp[456],unsigned short array[]);

        void chipConfigArray(unsigned short chipCfgArray[],int chipType);

        //  for Analogue Test
        void setDACandRegenerateArray(int Value,unsigned short configArray[]);

    private:
        SPIROC_A_CFG_TYPE  m_cfgSpirocA;
	//   unsigned short m_array[100];

        int m_selectForRead;
        int m_selectScNprobe;
        int m_trigSelect;

        std::string m_slowControlCfgFile;
};


// ---------------- EcalSpiroc ------------------------------------------------
class EcalSpiroc : public FrontEndBoard {
    public:
        EcalSpiroc(UsbBoard*, FrontEndBoardType, int);
        ~EcalSpiroc();
    
        virtual bool configure(FrontEndBoardConfiguration*);

        void spiToUfmOrRegister(unsigned char portNum,unsigned short data,bool force = false);
        void spiRegisterWrite(unsigned char portNum,unsigned short addr,unsigned short data);
        unsigned short spiRegisterRead(unsigned char portNum,unsigned short addr);
        unsigned char spiUfmStatusRead(unsigned char portNum);
        void spiUfmErase(unsigned char portNum);
        void spiUfmWrite(unsigned char portNum,unsigned short addr,unsigned short data);
        unsigned short spiUfmReadWord(unsigned char portNum,unsigned short start_addr);
        void spiUfmReadBlock(unsigned char portNum,unsigned short start_addr,unsigned short word_num,unsigned short data[512]);
        void selectReadChannel(unsigned int channelIndex);
	void selectProbeBit(unsigned int bitIndex);

        void setClkReadSpi(char setValue);
        void setClkScProbe(char setValue);
        void setSrinReadSpi(char setValue);
        void setSrinScProbe(char setValue);
        void setResetbReadSpi(char setValue);
        void setResetbScSpi(char setValue);
        void setHoldbSpi(char setValue) ;
        void setSelectForRead(char setValue);
        void setTestpulseSpi(char setValue);
        void setLgEnSpi(char setValue);
        
        void dumpFirmwareVersion();
        void dumpGeneCtrlReg0();
        void resetChipConfig();
	void resetProbeRegister();      
        bool spiSetTrigSelect(char trigSelect);
        bool spiSetSelectScNprobe(char selectScNprobe); 
        bool compareUfmToConfigArray(unsigned short chipConfigArray[],unsigned int compareArrayNum);      
        void writeConfigArrayToUfm(unsigned short chipConfigArray[], unsigned int writeConfigArrayNum);
        void sendStartConfigCMD();   
        bool loadConfigToSpiroc();     

        bool spiConfigSpirocA(int dumpOn,unsigned short chipConfigArray[],char trigSelect, char select_read,char selectScNprobe);

        void spirocAnalogTest();
        void setScNreadDebug(char setValue);


        const static unsigned int s_configArrayNum_spirocA = 29;
        const static unsigned int s_configArrayNum_spirocII = 44;

        void setHgEnSpi(char setValue);
        void selectContinuousMode(unsigned int channelIndex);
    
    private:
        unsigned short m_generalCtrlReg0;
        EcalSpirocConfiguration* m_configuration;
        unsigned short m_spiToUfmRegisterSwitch;
};

#endif
