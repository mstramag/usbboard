#include "VATA64V2.h"
#include "Settings.h"

#include <assert.h>
#include <string>
#include <cstring>
#include <limits.h>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <sstream>
#include <sys/stat.h> 

//#define DUMP_DEBUG

 const  SENSOR_CAL_PARAMS VATA64V2::SensorCalParasList[]={
	  // SensorCalParas{cala_biasA,calb_biasA,cala_biasB,calb_biasB,cala_curr,calb_curr,cala_temp,calb_temp}
	      {1,           1,       1,         1,         1,        1,        1,        1},  //SensorCalParas[0]---> no calibration                                                                             
              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1},  //vata64_board_1                                                                                                   
              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1},  //vata64_board_2
              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1},  //vata64_board_3
              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1},  //vata64_board_4
              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1},  //vata64_board_5                                                                                     
              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1},  //vata64_board_6                                                                                                             
              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1},  //vata64_board_7                                                                                                             
              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1},  //vata64_board_8                                                                                                             
              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1},  //vata64_board_9                                                                                                             
              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1},  //vata64_board_10 

              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1},  //vata64_board_11                                                                                                             
              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1},  //vata64_board_12                                                                                                            
                                                                                                              
              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1},  //vata64_board_13                                                                                                            
                                                                                                              
              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1},  //vata64_board_14                                                                                                            

              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1},  //vata64_board_15                                                                                                           
                                                                                                              
              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1},  //vata64_board_16
              {0.025779,    0.290887,0.025729,  0.403967,  0.0846,   0.3692,   1,        1}  //default calibration params
};

VATA64V2Configuration::VATA64V2Configuration(int slotNo,int uplinkId)
  : FrontEndBoardConfiguration(VATA64V2Type, slotNo,uplinkId)
  , m_trigSelect(-1)
  , m_vata64BoardIndex(0) 
  , m_slowControlCfgFile("")
  , m_pathToAdjustDACs("")
  , m_executeAdjustDACs(false)
  ,m_ledA1_On(false)
  ,m_ledA2_On(false)
  ,m_ledB1_On(false)
  ,m_ledB2_On(false)
  ,m_ledA_DAC(1000)
  ,m_ledB_DAC(1000)
{

    std::cerr
        << "VATA64V2Configuration::VATA64V2Configuration() called."
        <<  std::endl;

}

VATA64V2Configuration::~VATA64V2Configuration()
{}

void VATA64V2Configuration::dump(std::ostream& stream) const
{
    std::cerr
        << "VATA64V2Configuration::dump(std::ostream& stream) called."
        <<  std::endl;
    stream
        << "VATA64V2:trigSelect || " << m_trigSelect << " || " << std::endl
        << "VATA64V2:pathToSlowControlCfgFile || " << m_slowControlCfgFile << std::endl
        << "VATA64V2:pathToDirOfAdjustDACs || "<<m_pathToAdjustDACs <<std::endl;
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadFromString
//Function:     extract useful infomation for  vata64 settings or configuration infos from the usbBoard config file
//Variables:    searchSting(const char*):one line read from usbBoard config file          
//Returns:      (bool)
//              true = the given line is for  configuration, read & store it successfully
//              false= the given line does't belong to spirocA configuration
//-------------------------------------------------------------------------------------------------------------------//
bool VATA64V2Configuration::loadFromString(const char* searchString)
{
  //char cString[100];
      char *cString = new char[strlen(searchString) + 1];
      unsigned short temp[8];
      int slotNo = getSlotNo();
      // bool getline = false;  
      bool getline = true;   // NOT finished: after add new parameters in config file, set this initialized value to false!!!  
//      std::cerr
//        << "VATA64V2Configuration::loadFromString(const char*) called."
//        <<  std::endl;


      if(sscanf(searchString, "VATA64V2:pathToSlowControlCfgFile || %[^\n]", cString) == 1) {
          std::ostringstream os;
          //os << cString << "/VATA64V2_slot" << m_slotNo << ".cfg" ;
          os << cString << "/VATA64V2_uplink" << GetUplinkId() << ".cfg" ;
            m_slowControlCfgFile = os.str();
            getline = true;
      }

      if(sscanf(searchString, "VATA64V2:pathToDirOfAdjustDACs || %[^\n]", cString) == 1) {
          std::ostringstream os;
          os << cString << "/adjustDAC_uplink" << GetUplinkId() << ".txt" ;
          m_pathToAdjustDACs = os.str();
          getline = true;
      }
      delete [] cString;

      return getline;
}



//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadConfig
//Function:     read VATA64 slow control config file, store all the settings to struct VATA64_CFG_TYPE 
//Variables:    filePath(string): path to the slow contro config file of VATA64            
//Returns:      (bool) return error value of this process.
//              true = error exist!
//              false= execute successfully!
//-------------------------------------------------------------------------------------------------------------------//  
bool VATA64V2Configuration::loadConfig(const std::string& filePath) {

    std::stringstream stream;
    stream << filePath ;             //get config file name
    std::cerr << "Information: Loading VATA64 configuration from file " << stream.str() << std::endl;
    std::ifstream fileStream(stream.str().c_str());      // open config file 
    // "ifstream" provides an interface to read data from files as input streams

    bool err= false;
    if (!fileStream){
      std::cerr << "Failure: could not open file: \"" << stream.str()<< "\"." << std::endl;
      std::cerr << "Please check if the path is right or the frontend board configure file exists!" << std::endl;
           assert(false);
     }

    std::cerr << "Choose VATA64, reading Config file..."<<std::endl;
    err = loadFromStream(fileStream);                           // load config file, store all the config settings in struct VATA64_CFG_TYPE ( defined in TypeDefs.h )

    return(err);
}



//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadFromStream
//Function:     extract config values from stream, and store them in m_cfgVATA64 ( struct VATA64_CFG_TYPE ( defined in TypeDefs.h ))
//              m_cfgSpirocA is a private variable defined in Class VATA64Configuration
//Variables:    stream(istream): path to the slow control config file of VATA64    
//                              (istream objects are stream objects used to read and interpret input from sequences of characters)      
//Returns:      (bool) return error value of this process.
//              true = error exist!
//              false= execute successfully!
//-------------------------------------------------------------------------------------------------------------------//
bool VATA64V2Configuration::loadFromStream(std::istream& stream) {
    bool err = true;
    //unsigned int line = 1;
    bool isInChipConfigRange = false;
    std::string s;
    while (true) {
        std::getline(stream, s);
        if (stream.eof())
            break;
        const char* searchString = s.c_str();

        if(isInChipConfigRange) err = true;      // During each loop,err will be set back to 0 ,as long as one sentence in correct format has been read.Otherwise it will stay in 1
        else err = false;

        if (s.find("#") == 0 || s=="") {
//            if (s.find("End of the chip config") == 0 ){
//                err = false ;
//                isInChipConfigRange = false;
//                break;
//            }

            if(s.find("Begin of chip config") == 0){
                err = true;
                isInChipConfigRange = true;
            }

            continue; // Skip commented lines
        }



        if(sscanf(searchString, "DAC_on || %d ||",&m_cfgVATA64.DAC_on )==1){
            err = false;
        }

        if(sscanf(searchString, "Lg || %d ||",&m_cfgVATA64.Lg )==1){

            err = false;
        }

        if(sscanf(searchString, "Rfp_bp || %d ||",&m_cfgVATA64.Rfp_bp )==1){
            err = false;
        }

        if(sscanf(searchString, "Rfs0_b || %d ||",&m_cfgVATA64.Rfs0_b )==1){
            err = false;
        }

        if(sscanf(searchString, "Rfs1 || %d ||",&m_cfgVATA64.Rfs1 )==1){
            err = false;
        }

        if(sscanf(searchString, "Rfs2_b || %d ||",&m_cfgVATA64.Rfs2_b )==1){
            err = false;
        }

        if(sscanf(searchString, "Lgs || %d ||",&m_cfgVATA64.Lgs )==1){
            err = false;
        }

        if(sscanf(searchString, "Ssc0_b || %d ||",&m_cfgVATA64.Ssc0_b )==1){
            err = false;
        }

        if(sscanf(searchString, "Ssc1 || %d ||",&m_cfgVATA64.Ssc1 )==1){
            err = false;
        }

        if(sscanf(searchString, "Ssc2 || %d ||",&m_cfgVATA64.Ssc2)==1){
            err = false;
        }

        if(sscanf(searchString, "Tp50_dc || %d ||",&m_cfgVATA64.Tp50_dc)==1){
            err = false;
        }

       if(sscanf(searchString, "Bypass || %d ||",&m_cfgVATA64.Bypass)==1){
            err = false;
        }

       if(sscanf(searchString, "Sel || %d ||",&m_cfgVATA64.Sel)==1){
            err = false;
       }

       if(sscanf(searchString, "Shabi_hp1b || %d ||",&m_cfgVATA64.Shabi_hp1b)==1){
            err = false;
       }

       if(sscanf(searchString, "Shabi_hp2 || %d ||",&m_cfgVATA64.Shabi_hp2)==1){
            err = false;
       }

       if(sscanf(searchString, "Shabi_hp3b || %d ||",&m_cfgVATA64.Shabi_hp3b)==1){
            err = false;
       }

       if(sscanf(searchString, "SHsense_en || %d ||",&m_cfgVATA64.SHsense_en)==1){
            err = false;
       }

       if(sscanf(searchString, "SHgen_en || %d ||",&m_cfgVATA64.SHgen_en)==1){
            err = false;
       }

       if(sscanf(searchString, "Buf_sel || %d ||",&m_cfgVATA64.Buf_sel)==1){
            err = false;
       }

       if(sscanf(searchString, "Test_on || %d ||",&m_cfgVATA64.Test_on)==1){
            err = false;
       }

       if(sscanf(searchString, "Vfp_en || %d ||",&m_cfgVATA64.Vfp_en)==1){
            err = false;
       }

       if(sscanf(searchString, "Vfsfclamp_en || %d ||",&m_cfgVATA64.Vfsfclamp_en)==1){
            err = false;
       }

       if(sscanf(searchString, "reserved || %d ||",&m_cfgVATA64.reserved)==1){
            err = false;
       }


      if(sscanf(searchString, "Disable_register   || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d ||",
                    &m_cfgVATA64.Disable_register[0],
                    &m_cfgVATA64.Disable_register[1],
                    &m_cfgVATA64.Disable_register[2],
                    &m_cfgVATA64.Disable_register[3],
                    &m_cfgVATA64.Disable_register[4],
                    &m_cfgVATA64.Disable_register[5],
                    &m_cfgVATA64.Disable_register[6],
                    &m_cfgVATA64.Disable_register[7],
                    &m_cfgVATA64.Disable_register[8],
                    &m_cfgVATA64.Disable_register[9],
                    &m_cfgVATA64.Disable_register[10],
                    &m_cfgVATA64.Disable_register[11],
                    &m_cfgVATA64.Disable_register[12],
                    &m_cfgVATA64.Disable_register[13],
                    &m_cfgVATA64.Disable_register[14],
                    &m_cfgVATA64.Disable_register[15],
                    &m_cfgVATA64.Disable_register[16],
                    &m_cfgVATA64.Disable_register[17],
                    &m_cfgVATA64.Disable_register[18],
                    &m_cfgVATA64.Disable_register[19],
                    &m_cfgVATA64.Disable_register[20],
                    &m_cfgVATA64.Disable_register[21],
                    &m_cfgVATA64.Disable_register[22],
                    &m_cfgVATA64.Disable_register[23],
                    &m_cfgVATA64.Disable_register[24],
                    &m_cfgVATA64.Disable_register[25],
                    &m_cfgVATA64.Disable_register[26],
                    &m_cfgVATA64.Disable_register[27],
                    &m_cfgVATA64.Disable_register[28],
                    &m_cfgVATA64.Disable_register[29],
                    &m_cfgVATA64.Disable_register[30],
		    &m_cfgVATA64.Disable_register[31],
                    &m_cfgVATA64.Disable_register[32],
                    &m_cfgVATA64.Disable_register[33],
                    &m_cfgVATA64.Disable_register[34],
                    &m_cfgVATA64.Disable_register[35],
                    &m_cfgVATA64.Disable_register[36],
                    &m_cfgVATA64.Disable_register[37],
                    &m_cfgVATA64.Disable_register[38],
                    &m_cfgVATA64.Disable_register[39],
                    &m_cfgVATA64.Disable_register[40],
                    &m_cfgVATA64.Disable_register[41],
                    &m_cfgVATA64.Disable_register[42],
                    &m_cfgVATA64.Disable_register[43],
                    &m_cfgVATA64.Disable_register[44],
                    &m_cfgVATA64.Disable_register[45],
                    &m_cfgVATA64.Disable_register[46],
                    &m_cfgVATA64.Disable_register[47],
                    &m_cfgVATA64.Disable_register[48],
                    &m_cfgVATA64.Disable_register[49],
                    &m_cfgVATA64.Disable_register[50],
                    &m_cfgVATA64.Disable_register[51],
                    &m_cfgVATA64.Disable_register[52],
                    &m_cfgVATA64.Disable_register[53],
                    &m_cfgVATA64.Disable_register[54],
                    &m_cfgVATA64.Disable_register[55],
                    &m_cfgVATA64.Disable_register[56],
                    &m_cfgVATA64.Disable_register[57],
                    &m_cfgVATA64.Disable_register[58],
                    &m_cfgVATA64.Disable_register[59],
                    &m_cfgVATA64.Disable_register[60],
                    &m_cfgVATA64.Disable_register[61],
                    &m_cfgVATA64.Disable_register[62],
                    &m_cfgVATA64.Disable_register[63])==64){
                        err = false;
                    }

       if(sscanf(searchString, "Disable_bit_tst_channel || %d ||",&m_cfgVATA64.Disable_bit_tst_channel)==1){
            err = false;
       }

      if(sscanf(searchString, "Threshold_alignment_DAC  || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d ||",
                    &m_cfgVATA64.Threshold_alignment_DAC[0],
                    &m_cfgVATA64.Threshold_alignment_DAC[1],
                    &m_cfgVATA64.Threshold_alignment_DAC[2],
                    &m_cfgVATA64.Threshold_alignment_DAC[3],
                    &m_cfgVATA64.Threshold_alignment_DAC[4],
                    &m_cfgVATA64.Threshold_alignment_DAC[5],
                    &m_cfgVATA64.Threshold_alignment_DAC[6],
                    &m_cfgVATA64.Threshold_alignment_DAC[7],
                    &m_cfgVATA64.Threshold_alignment_DAC[8],
                    &m_cfgVATA64.Threshold_alignment_DAC[9],
                    &m_cfgVATA64.Threshold_alignment_DAC[10],
                    &m_cfgVATA64.Threshold_alignment_DAC[11],
                    &m_cfgVATA64.Threshold_alignment_DAC[12],
                    &m_cfgVATA64.Threshold_alignment_DAC[13],
                    &m_cfgVATA64.Threshold_alignment_DAC[14],
                    &m_cfgVATA64.Threshold_alignment_DAC[15],
                    &m_cfgVATA64.Threshold_alignment_DAC[16],
                    &m_cfgVATA64.Threshold_alignment_DAC[17],
                    &m_cfgVATA64.Threshold_alignment_DAC[18],
                    &m_cfgVATA64.Threshold_alignment_DAC[19],
                    &m_cfgVATA64.Threshold_alignment_DAC[20],
                    &m_cfgVATA64.Threshold_alignment_DAC[21],
                    &m_cfgVATA64.Threshold_alignment_DAC[22],
                    &m_cfgVATA64.Threshold_alignment_DAC[23],
                    &m_cfgVATA64.Threshold_alignment_DAC[24],
                    &m_cfgVATA64.Threshold_alignment_DAC[25],
                    &m_cfgVATA64.Threshold_alignment_DAC[26],
                    &m_cfgVATA64.Threshold_alignment_DAC[27],
                    &m_cfgVATA64.Threshold_alignment_DAC[28],
                    &m_cfgVATA64.Threshold_alignment_DAC[29],
                    &m_cfgVATA64.Threshold_alignment_DAC[30],
		    &m_cfgVATA64.Threshold_alignment_DAC[31],
                    &m_cfgVATA64.Threshold_alignment_DAC[32],
                    &m_cfgVATA64.Threshold_alignment_DAC[33],
                    &m_cfgVATA64.Threshold_alignment_DAC[34],
                    &m_cfgVATA64.Threshold_alignment_DAC[35],
                    &m_cfgVATA64.Threshold_alignment_DAC[36],
                    &m_cfgVATA64.Threshold_alignment_DAC[37],
                    &m_cfgVATA64.Threshold_alignment_DAC[38],
                    &m_cfgVATA64.Threshold_alignment_DAC[39],
                    &m_cfgVATA64.Threshold_alignment_DAC[40],
                    &m_cfgVATA64.Threshold_alignment_DAC[41],
                    &m_cfgVATA64.Threshold_alignment_DAC[42],
                    &m_cfgVATA64.Threshold_alignment_DAC[43],
                    &m_cfgVATA64.Threshold_alignment_DAC[44],
                    &m_cfgVATA64.Threshold_alignment_DAC[45],
                    &m_cfgVATA64.Threshold_alignment_DAC[46],
                    &m_cfgVATA64.Threshold_alignment_DAC[47],
                    &m_cfgVATA64.Threshold_alignment_DAC[48],
                    &m_cfgVATA64.Threshold_alignment_DAC[49],
                    &m_cfgVATA64.Threshold_alignment_DAC[50],
                    &m_cfgVATA64.Threshold_alignment_DAC[51],
                    &m_cfgVATA64.Threshold_alignment_DAC[52],
                    &m_cfgVATA64.Threshold_alignment_DAC[53],
                    &m_cfgVATA64.Threshold_alignment_DAC[54],
                    &m_cfgVATA64.Threshold_alignment_DAC[55],
                    &m_cfgVATA64.Threshold_alignment_DAC[56],
                    &m_cfgVATA64.Threshold_alignment_DAC[57],
                    &m_cfgVATA64.Threshold_alignment_DAC[58],
                    &m_cfgVATA64.Threshold_alignment_DAC[59],
                    &m_cfgVATA64.Threshold_alignment_DAC[60],
                    &m_cfgVATA64.Threshold_alignment_DAC[61],
                    &m_cfgVATA64.Threshold_alignment_DAC[62],
                    &m_cfgVATA64.Threshold_alignment_DAC[63])==64){
                        err = false;
                    }

       if(sscanf(searchString, "Threshold_alignment_DAC_tst_channel || %d ||",&m_cfgVATA64.Threshold_alignment_DAC_tst_channel)==1){
            err = false;
       }

       if(sscanf(searchString, "Preamplifier_input_potential_DAC  || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d ||",
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[0],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[1],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[2],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[3],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[4],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[5],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[6],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[7],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[8],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[9],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[10],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[11],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[12],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[13],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[14],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[15],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[16],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[17],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[18],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[19],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[20],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[21],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[22],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[23],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[24],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[25],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[26],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[27],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[28],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[29],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[30],
		    &m_cfgVATA64.Preamplifier_input_potential_DAC[31],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[32],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[33],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[34],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[35],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[36],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[37],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[38],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[39],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[40],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[41],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[42],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[43],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[44],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[45],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[46],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[47],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[48],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[49],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[50],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[51],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[52],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[53],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[54],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[55],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[56],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[57],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[58],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[59],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[60],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[61],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[62],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[63])==64){

                        err = false;
                    }

       if(sscanf(searchString, "Preamplifier_input_potential_DAC_tst_channel || %d ||",&m_cfgVATA64.Preamplifier_input_potential_DAC_tst_channel)==1){
            err = false;
       }

       if(sscanf(searchString, "Holdbi_Bias_DAC || %d ||",&m_cfgVATA64.Holdbi_Bias_DAC)==1){
            err = false;
       }

       if (err) {
            printf("The following line in the cfg file is either in a wrong format or not included in chip Config Range! Check your cfg file = %s!\n  wrongLine = %s\n",m_slowControlCfgFile.c_str(),searchString);
            assert(false);
            return false;
        }  
    }


    return !err;
}



//--------------------------------------------------------------------------------------------------------------------//
//Name:         convertCfgToVector
//Function:     convert m_cfgVATA64 ( struct VATA64_CFG_TYPE ( defined in TypeDefs.h ))into Vector.
//              m_cfgVATA64 is a private variable defined in Class VATA64Configuration,which stores all the settings reading from the slow control config file
//Variables:    temp(unsigned char[]): point to an array to place the conversion result. Each member of this array corresponds to one bit of the slow control register
//              i.e.temp[index] corresponds to BIT(index) of the slow control register      
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------// 
void  VATA64V2Configuration::convertCfgToVector(unsigned char temp[873]) {

    int  bit_total =0;
    int  bit,ch;

    temp[bit_total++] = (m_cfgVATA64.DAC_on                          )&0x01;  
    temp[bit_total++] = (m_cfgVATA64.Lg                              )&0x01;  
    temp[bit_total++] = (m_cfgVATA64.Rfp_bp                          )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Rfs0_b                          )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Rfs1                            )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Rfs2_b                          )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Lgs                             )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Ssc0_b                          )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Ssc1                            )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Ssc2                            )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Tp50_dc                         )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Bypass                          )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Sel                             )&0x01; 

    temp[bit_total++] = (m_cfgVATA64.Shabi_hp1b                      )&0x01;  
    temp[bit_total++] = (m_cfgVATA64.Shabi_hp2                       )&0x01;  
    temp[bit_total++] = (m_cfgVATA64.Shabi_hp3b                     )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.SHsense_en                      )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.SHgen_en                        )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Buf_sel                         )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Test_on                         )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Vfp_en                          )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Vfsfclamp_en                    )&0x01; 

    for(bit=0;bit<2;bit++) //from LSB to MSB
        temp[bit_total++] = (m_cfgVATA64.reserved >>bit )&0x01;
 
    for(ch=0;ch<64;ch++)  //from ch0 to ch63 
        temp[bit_total++] = (m_cfgVATA64.Disable_register[ch]        )&0x01;

    temp[bit_total++] = (m_cfgVATA64.Disable_bit_tst_channel         )&0x01;  

    for(ch=0;ch<64;ch++) {  //from ch0 to ch63 
        for(bit=0;bit<4;bit++)//from LSB to MSB
            temp[bit_total++] = (m_cfgVATA64.Threshold_alignment_DAC[ch]>>bit  )&0x01;
    } 

    for(bit=0;bit<4;bit++) //from LSB to MSB
        temp[bit_total++] = (m_cfgVATA64.Threshold_alignment_DAC_tst_channel >>bit )&0x01;

    for(ch=0;ch<64;ch++) {  //from ch0 to ch63 
      // for(bit=0;bit<8;bit++)//from LSB to MSB-----------deleted by lht 16:34@4.27.2011
      for(bit=7;bit>=0;bit--)                 //-----------added by lht 16:36@4.27.2011
            temp[bit_total++] = (m_cfgVATA64.Preamplifier_input_potential_DAC[ch]>>bit  )&0x01;
    } 

    //for(bit=0;bit<8;bit++) //from LSB to MSB-------------deleted by lht 16:39@4.27.2011
    for(bit=7;bit>=0;bit--)                   //-----------added by lht 16:40@4.27.2011
        temp[bit_total++] = (m_cfgVATA64.Preamplifier_input_potential_DAC_tst_channel >>bit )&0x01;

    for(bit=0;bit<4;bit++) //from LSB to MSB
        temp[bit_total++] = (m_cfgVATA64.Holdbi_Bias_DAC >>bit        )&0x01;
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         writeToStream
//Function:     dump m_cfgVATA64 ( struct VATA64_CFG_TYPE ( defined in TypeDefs.h ))
//              m_cfgVATA64 is a private variable defined in Class VATA64Configuration,which stores all the settings reading from the slow control config file
//Variables:    stream(ostream):  output stream
//                                (ostream objects are stream objects used to write and format output as sequences of characters      
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------// 
void VATA64V2Configuration::writeToStream(std::ostream& stream) const
{
    stream
        <<" DAC_on:    "<<m_cfgVATA64.DAC_on << std::endl
        <<" Lg    :    "<<m_cfgVATA64.Lg     << std::endl 
        <<" Rfp_bp:    "<<m_cfgVATA64.Rfp_bp << std::endl
        <<" Rfs0_b:    "<<m_cfgVATA64.Rfs0_b << std::endl
        <<" Rfs1  :    "<<m_cfgVATA64.Rfs1   << std::endl
        <<" Rfs2_b:    "<<m_cfgVATA64.Rfs2_b << std::endl
        <<" Lgs   :    "<<m_cfgVATA64.Lgs    << std::endl
        <<" Ssc0_b:    "<<m_cfgVATA64.Ssc0_b << std::endl
        <<" Ssc1  :    "<<m_cfgVATA64.Ssc1   << std::endl
        <<" Ssc2  :    "<<m_cfgVATA64.Ssc2   << std::endl
        <<" Tp50_dc:   "<<m_cfgVATA64.Tp50_dc<< std::endl
        <<" Bypass:    "<<m_cfgVATA64.Bypass << std::endl

        <<" Sel   :        "<<m_cfgVATA64.Sel           << std::endl
        <<" Shabi_hp1b:    "<<m_cfgVATA64.Shabi_hp1b    << std::endl 
        <<" Shabi_hp2 :    "<<m_cfgVATA64.Shabi_hp2     << std::endl
        <<" Shaibi_hp3b:   "<<m_cfgVATA64.Shabi_hp3b   << std::endl
        <<" SHsense_en :   "<<m_cfgVATA64.SHsense_en    << std::endl
        <<" SHgen_en   :   "<<m_cfgVATA64.SHgen_en      << std::endl
        <<" Buf_sel    :   "<<m_cfgVATA64.Buf_sel       << std::endl
        <<" Test_on    :   "<<m_cfgVATA64.Test_on       << std::endl
        <<" Vfp_en     :   "<<m_cfgVATA64.Vfp_en        << std::endl
        <<" Vfsfclamp_en : "<<m_cfgVATA64.Vfsfclamp_en  << std::endl
        <<" reserved     : " <<m_cfgVATA64.reserved     << std::endl;
    
    stream
        <<" Disable_register: ";
    for(unsigned int j = 0; j<64 ; j++)
        stream<<m_cfgVATA64.Disable_register[j] <<"  ";
    stream
        <<std::endl;

    stream
        <<" Disable_bit_tst_channel: "<<m_cfgVATA64.Disable_bit_tst_channel <<std::endl;

    stream
        <<" Threshold_alignment_DAC: ";
    for(unsigned int j = 0; j<64 ; j++)
        stream<<m_cfgVATA64.Threshold_alignment_DAC[j] <<"  ";
    stream
        <<std::endl;

   stream
        <<" Threshold_alignment_DAC_tst_channel: "<<m_cfgVATA64.Threshold_alignment_DAC_tst_channel <<std::endl;

    stream
        <<" Preamplifier_input_potential_DAC: ";
    for(unsigned int j = 0; j<64 ; j++)
        stream<<m_cfgVATA64.Preamplifier_input_potential_DAC[j] <<"  ";
    stream
        <<std::endl;

    stream
        <<" Preamplifier_input_potential_DAC_tst_channel: "<<m_cfgVATA64.Preamplifier_input_potential_DAC_tst_channel <<std::endl
        <<" Holdbi_Bias_DAC: "<<m_cfgVATA64.Holdbi_Bias_DAC <<std::endl;
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         convertVectorTo16bitsArray
//Function:     convert config vector into 16bits array, which will be written into UFM
//Variables:    temp(unsigned char[]): vectors converted from m_cfgSpirocA
//              array(unsigned short[]): point to the array to place the 16bits array written into UFM
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------// 
void  VATA64V2Configuration::convertVectorTo16bitsArray(int dumpOn, unsigned char temp[873],unsigned short array[])
{
    short               bit;
    short               p;
    unsigned short  pad_short;

    unsigned short  temp_debug;
    short              index;
    /*=============convert to word====== order config bit 0 first in=======*/
    //    p =0;    
    //    index = 0;
    //    temp_debug = 0;

    //    for(bit=0;bit<(456+8);bit++)
    //    {  
    //      pad_short <<=1; 

    //      if(bit <456)
    //        pad_short += temp[bit];
    //      else
    //        pad_short += 0;  // fill the last byte with 0

    //       if((bit%16)==15)  
    //       {   array[index] = pad_short;

    //     if(dumpOn==1){
    //           std::cerr<<"Info@16bitsArray:array["<<index<<"]=";
    //           for(unsigned int i = 0; i<16; i++)
    //           std::cerr<<((array[index]>>(15-i))&0x0001)<<" ";
    //           std::cerr<<std::endl;
    //     }
    //           index++;
    //       }
    //    }
    /*=============convert to word====== order config bit 0 last in=======*/
    p =0;    
    index = 0;
    temp_debug = 0;

    for(bit=872+7;bit>=0;bit--)
    {  
        pad_short <<=1; 

        if(bit >=7)
            pad_short += temp[bit-7];
        else
            pad_short += 0;  // fill the last byte with 0

        if((bit%16)==0)  
        {   array[index] = pad_short;

            if(dumpOn==1){
                std::cerr<<"Info@16bitsArray:array["<<index<<"]=";
                for(unsigned int i = 0; i<16; i++)
                    std::cerr<<((array[index]>>(15-i))&0x0001)<<" ";
                std::cerr<<std::endl;
            }
            index++;
        }
    }
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         setPreamplifierInputPortentialDAC
//Function:     set Preamplifier input potential DAC value without refering to the config file, then update 16bits config arrays.
//Variables:    Value (int): DAC value to be set
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//    
void VATA64V2Configuration::setPreamplifierInputPortentialDAC(int Value,unsigned short configArray[]){
  unsigned char temp[873];
  unsigned int dumpOn = 0;
  for(unsigned int channel=0; channel<64;channel++){
        m_cfgVATA64.Preamplifier_input_potential_DAC[channel] = Value;
   // std::cerr<<"DAC value written"<<m_cfgVATA64.Preamplifier_input_potential_DAC[channel];
  }
  convertCfgToVector(temp);
  convertVectorTo16bitsArray(dumpOn, temp, configArray);
}



// //----------------------------------------------------------------------------//
// //Name:          selectForRead
// //Function:      provide info that which kind of signals are used to access "READ interface" of Spiroc to other Classes which need this value.
// //               Because m_select_nSPI_uplink is a private variable for Class VATA64Configuration, which only can be accessed by Class VATA64Configuration itself
// //               Parameter with the identical name in Firmware corresponds to bit6 of Gene_ctrl_reg0 (Firmwares for SpirocA and SpirocII use the same bit, 01.06.2010)
// //               0= signals from SPI (set by C code);    1= signals from uplink(H,HB,S,SB)
// //Parameters:    None
// //Returns:       (int) return 0= signals from SPI (set by C code)
// //                            1= signals from uplink(H,HB,S,SB)
// //---------------------------------------------------------------------------//
// int VATA64Configuration::select_nSPI_uplink() const
// {
//     return m_select_nSPI_uplink;
// }

// //----------------------------------------------------------------------------//
// //Name:          setSelect_nSPI_uplink
// //Function:      select signals used to access "READ interface" of Spiroc without refering to the config file
// //Parameters:    select_nSPI_uplink(int):  Only 0 or 1 could be used, otherwise the program will break off with an error report!
// //               0= signals from SPI (set by C code),  1= signals from uplink(H,HB,S,SB)
// //Returns:       None
// //---------------------------------------------------------------------------//
// void VATA64Configuration::setSelect_nSPI_uplink(int select_nSPI_uplink)
// {
//     m_select_nSPI_uplink = select_nSPI_uplink;
// }


//----------------------------------------------------------------------------//
//Name:          trigSelect
//Function:      provide trigger type of spiroc to other Classes which need this value.
//               Because m_trigSelect is a private variable for Class VATA64Configuration, which only can be accessed by Class VATA64Configuration itself
//               For VATA64,it corresponds to bit7(trigSelect) of Gene_ctrl_reg0 (08.07.2010)
//               In VATA64 firmware, the default value for Gene_ctrl_reg0(7) has been set to '0'.
//Parameters:    None
//Returns:       (int) return trigger type value
//               0=UL_HOLD(use H,HB,S,SB as trigger signal);  1=NIM_trigger;   
//---------------------------------------------------------------------------//
int VATA64V2Configuration::trigSelect() const
{
    return m_trigSelect;
}

unsigned short VATA64V2Configuration::vata64BoardIndex() const
{
    return m_vata64BoardIndex;
}
//----------------------------------------------------------------------------//
//Name:          setTrigSelect
//Function:      set trigger type of VATA64 without refering to the config file
//Parameters:    trigSelect(int): has different meanings & ranges for spirocII ,spirocA and VATA64 
//               Only 0~1 could be used for VATA64, otherwise the program will break off with an error report!
//               0=UL_HOLD(use H,HB,S,SB as trigger signal);  1=NIM_trigger;   
//Returns:       None
//---------------------------------------------------------------------------//
void VATA64V2Configuration::setTrigSelect(int trigSelect)
{
    m_trigSelect = trigSelect;
}

//----------------------------------------------------------------------------//
//Name:          setSlowControlCfgFile
//Function:      set a new path to the slow control config file without refering to the UsbBoard config file
//Parameters:    fileName(string) : the format is like "/home/pebs_ecal/usbBoard/readout/readoutSettings/VATA64_EPFL.cfg"
//Returns:       None
//---------------------------------------------------------------------------//
void VATA64V2Configuration::setSlowControlCfgFile(const std::string& fileName)
{
    m_slowControlCfgFile = fileName;
}

//----------------------------------------------------------------------------//
//Name:          firmwareFileName     
//Function:      provide path to the slow control config file to other Classes which need this value.
//               Because m_slowControlCfgFile is a private variable for Class UsbBoardConfiguration, which only can be accessed by Class UsbBoardConfiguration itself
//Parameters:    None
//Returns:       (string) return path to the slow control config file 
//---------------------------------------------------------------------------//
const std::string& VATA64V2Configuration::slowControlCfgFile() const
{
    return m_slowControlCfgFile;
}




// ------------------------- VATA64V2 --------------------------------
VATA64V2::VATA64V2(UsbBoard* board, FrontEndBoardType type, int slotNo)
    : FrontEndBoard(board, type, slotNo)
    , m_configuration(0)
    , m_spiToUfmRegisterSwitch(0)
{
    m_sensorCalParas = SensorCalParasList[17]; // initialize as default calibration paras which has not been optimized for a specified vata64_board. This value will be changed when initialized .
    m_sensorsInfo.biasA_adc = 0;
    m_sensorsInfo.biasB_adc = 0;
    m_sensorsInfo.currA_adc = 0;
    m_sensorsInfo.currB_adc = 0;
    m_sensorsInfo.tempA_adc = 0;
    m_sensorsInfo.tempB_adc = 0;

    m_sensorsInfo.biasA_V = 0;
    m_sensorsInfo.biasB_V = 0;
    m_sensorsInfo.currA_nA = 0;
    m_sensorsInfo.currB_nA = 0;
    m_sensorsInfo.tempA_C = 0;
    m_sensorsInfo.tempB_C = 0;

}

VATA64V2::~VATA64V2()
{}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         configure
//Function:     configure front end chip, if this function has been switched on in the UsbBoard cfg file
//Variables:    None
//Returns:      (bool)
//              true = config successfully
//              false= error exist during configuration
//-------------------------------------------------------------------------------------------------------------------//
bool VATA64V2::configure(FrontEndBoardConfiguration* configuration)
{
    m_configuration = static_cast<VATA64V2Configuration*>(configuration);

    m_configuration->dump();

    std::cerr << "VATA64V2::configure() called." << std::endl;
    //return true; //TODO
    bool err = false;
#ifndef NO_QUSB

    //VATA64V2Configuration chipConfig(m_slotNo);
    std::string chipConfigFilePath = m_configuration->slowControlCfgFile();        // get the path to slow control config file

    unsigned char temp[873];        //--------added by lht 11:22@ 29-4-2011
    unsigned short configArray[100];


    // first config board settings, i.e. generalCtrlRegister, DAC for led intensity..
    std::cout<< "load board settings" <<std::endl;
    m_configuration->loadBoardSettings(chipConfigFilePath);


    //Just used to debug led DAC functions
    while(0) {
        ledA1On();
        ledB1On();
        setAD5322ChanA(0x00FF);
        //getchar();
    }
    // Set LED injection
    setAD5322ChanA(m_configuration->GetLedA_DAC());
    setAD5322ChanB(m_configuration->GetLedB_DAC());

    if(m_configuration->IsLedA1_On()){
        ledA1On();
    }else{
        ledA1Off();
    }

    if(m_configuration->IsLedA2_On()){
        ledA2On();
    }else{
        ledA2Off();
    }

    if(m_configuration->IsLedB1_On()){
        ledB1On();
    }else{
        ledB1Off();
    }

    if(m_configuration->IsLedB2_On()){
        ledB2On();
    }else{
        ledB2Off();
    }


     std::cerr << "before loadConfig " << std::endl;
     std::cerr << chipConfigFilePath << std::endl;

     printf("\n\n\nspi load vata64 chip configuration....\n\n");
     m_configuration->loadConfig(chipConfigFilePath);

     //if in adjustDAC mode, read in setting files and change m_cfgVATA64;
     if(m_configuration->IsAdjustDACMode()){
         int setDACs[64];
         for(int i=0;i<64;i++) setDACs[i] = 255;

         std::cout << "slot = "<< m_slotNo <<" (uplinkId = " <<uplinkId()<< ") is set to adjustDAC mode, now read in DAC values from "<<m_configuration->GetPathToAdjustDACs() <<std::endl;
         std::ifstream dacFile(m_configuration->GetPathToAdjustDACs().c_str(),std::ifstream::in);
         if(dacFile.eof()) {
             std::cerr<<"dacFile =  "<<m_configuration->GetPathToAdjustDACs()<<" is empty! exit abnormal!\n"<<std::endl;
             assert(false);
         }

         std::string s;

         while(true){
             std::getline(dacFile,s);
             if(dacFile.eof()) break;

             const char* searchString = s.c_str();

             #ifdef DUMP_DEBUG
             std::cout<<"s = "<< s <<std::endl;
             //getchar();
             #endif

             if(s.find("#") ==0)  continue; //skip the comment line

             if(sscanf(searchString, "Preamplifier_input_potential_DAC  || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d ||",
                          &setDACs[0], &setDACs[1], &setDACs[2], &setDACs[3], &setDACs[4],&setDACs[5], &setDACs[6],&setDACs[7], &setDACs[8],&setDACs[9], &setDACs[10],&setDACs[11], &setDACs[12],
                          &setDACs[13], &setDACs[14],&setDACs[15], &setDACs[16], &setDACs[17], &setDACs[18], &setDACs[19],&setDACs[20], &setDACs[21], &setDACs[22],&setDACs[23], &setDACs[24],
                          &setDACs[25], &setDACs[26],&setDACs[27], &setDACs[28], &setDACs[29], &setDACs[30], &setDACs[31],&setDACs[32], &setDACs[33],&setDACs[34],&setDACs[35], &setDACs[36],
                          &setDACs[37],&setDACs[38], &setDACs[39], &setDACs[40], &setDACs[41], &setDACs[42],&setDACs[43],&setDACs[44],&setDACs[45], &setDACs[46],&setDACs[47], &setDACs[48],
                          &setDACs[49], &setDACs[50],&setDACs[51], &setDACs[52],&setDACs[53], &setDACs[54], &setDACs[55],&setDACs[56],&setDACs[57], &setDACs[58],&setDACs[59],&setDACs[60],
                          &setDACs[61], &setDACs[62],&setDACs[63])==64){

                     for(int i = 0; i<64; i++) {
                         if(setDACs[i]>255 || setDACs[i]<1) setDACs[i] = 255;
                     }
             }
         }
         #ifdef DUMP_DEBUG
         for(int i = 0; i<64; i++) {printf("setDACs[%d] = %d ",i,setDACs[i]);if(i%16 ==0 &&i!=0) printf("\n");}
         #endif
         //chipConfig.SetPreamplifierInputPortentialDACs(setDACs);
         m_configuration->SetPreamplifierInputPortentialDACs(setDACs);
     }


     //chipConfig.convertCfgToVector(temp);
     //chipConfig.convertVectorTo16bitsArray(0, temp, configArray);

     //in order to Check m_configuration values, uncomment the following two sentences.
//     m_configuration->writeToStream(std::cout);
//     getchar();

     m_configuration->convertCfgToVector(temp);
     m_configuration->convertVectorTo16bitsArray(0, temp, configArray);

     printf("\n\nspiConfigVATA64()----------------\n\n");
     if (spiConfigVATA64(0, configArray, m_configuration->trigSelect() ))  err = true;



     // enable Amplifiers IC6,IC7,IC8,IC9 on the vata64 board. Actually,the firmware has already set their default value to 1 ( Gene_ctrl_Reg0 bit 9,10,11)
     setNPWD(1);





     // select channel for continuous mode operation
     //////////////////////////////
     // Add only this two lines below to use the continous mode  
     //selectContinuousMode(28);      // select channel 1
     //getchar();
     //selectContinuousMode(47);      f
     //getchar();
     ////////////////////////////////

     // change dac value and reconfig
     /////////////////////////////////////////// 
     if (0) {
	 for(unsigned int DACValue=0; DACValue<256; DACValue+=16){
           //chipConfig.setPreamplifierInputPortentialDAC(DACValue,configArray);
           m_configuration->setPreamplifierInputPortentialDAC(DACValue,configArray);
           if ( spiConfigVATA64(1, configArray, m_configuration->trigSelect() ))  err = true;
           std::cerr << "DACValue"<< DACValue << std::endl;
           getchar();
         }
     }
     /////////////////////////////////////////////
        
     // Choose Uplink or SPI control for the read port 
     //////////////////////////
     //setSelect_nSPI_uplink(0);       // use spi
     setSelect_nSPI_uplink(1);       // use uplink
     ////////////////////////

     //
    
#endif
    if (err) {
        std::cout << "Could not configure DAQ" << std::endl;
        assert(false);
        return false;
      } else {
        return true;
    }
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiRegisterWrite
//Function:     write data to a given register in FOGA on the front end board
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              addr(unsigned short): address of the register to be written, currently for spirocA and spirocII front end board , we only use two register, already defined in FrontEndChipMacroDefinition.h (01.06.2010)
//                                    FIRMWARE_VERSION_REG_ADDR = 0x0000
//                                    GENERAL_CTRL_REG0 = 0x0001      ( detail definition for each bit, refer to FrontEndMacroDefiniton.h )
//              data(unsigned short): data to be written into the given register
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
void VATA64V2::spiRegisterWrite(unsigned char portNum, unsigned short addr, unsigned short data) {

    // By lht, July 20, 13:45. For new parallel spi interface
    unsigned char buf[7];   //[0] FE select; [1],[2] device select; [3],[4] address; [5],[6] data
    buf[0] = getSlotNo();
    buf[1] = (SPItoREGISTER >> 8) & 0xFF;
    buf[2] = (SPItoREGISTER     ) & 0xFF;
    buf[3] = (addr >> 8) & 0xFF;
    buf[4] = (addr     ) & 0xFF;
    buf[5] = (data >> 8) & 0xFF;
    buf[6] = (data     ) & 0xFF;
    write(portNum, buf, 7);
}
#else
void VATA64V2::spiRegisterWrite(uint16_t addr, uint16_t data) {

    uint16_t sendData[4];

    sendData[0] = getSlotNo();
    sendData[1] = SPItoREGISTER;
    sendData[2] = addr;
    sendData[3] = data;

    writeQSPI(sendData,CMD_REG_RAM_BASE_VATA64+getSlotNo()*CMD_REG_SINGLE_WIDTH_VATA64,4);
}
#endif

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiRegisterRead
//Function:     read data from a given register in FOGA on the front end board
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              addr(unsigned short): address of the register to be written, currently for spirocA and spirocII front end board , we only use two register, already defined in FrontEndChipMacroDefinition.h (01.06.2010)
//                                    FIRMWARE_VERSION_REG_ADDR = 0x0000
//                                    GENERAL_CTRL_REG0 = 0x0001      ( detail definition for each bit, refer to FrontEndMacroDefiniton.h )
//Returns:      (unsigned short): return the data value read back from the given register
//-------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
unsigned short VATA64V2::spiRegisterRead(unsigned char portNum, unsigned short addr) {

    // By lht, July 20, 13:59. For new parallel spi interface
    unsigned char buf[7];   //[0] FE select; [1],[2] device select; [3],[4] address; [5],[6] data
    unsigned short readData;
    buf[0] = getSlotNo();
    buf[1] = (SPItoREGISTER >> 8) & 0xFF;
    buf[2] = (SPItoREGISTER     ) & 0xFF;
    buf[3] = ((addr >> 8) & 0x7F) | 0x80;
    buf[4] = (addr     ) & 0xFF;
    buf[5] = 0x00;
    buf[6] = 0x00;
    writeRead(portNum, buf, 7);
    readData = buf[5];
    readData <<= 8;
    readData += buf[6];

//     std::cerr << "Info@Config_VATA64: readRegister @ "<< addr << "=";
//     for(unsigned int i = 0; i<16; i++)
//         std::cerr << ((readData>>(15-i))&0x0001);
//     std::cerr << std::endl;

     return(readData);
}
#else
uint16_t VATA64V2::spiRegisterRead(uint16_t addr){
     uint16_t sendData[4];
     uint16_t receiveData[4];

     sendData[0] = getSlotNo();
     sendData[1] = SPItoREGISTER;
     sendData[2] = addr| 0x8000;
     sendData[3] = 0;
//     printf("spiRegisterRead- slotNo = %d\n",getSlotNo());
//     printf("addr = %#x\n",addr);
//     getchar();

     readWriteQSPI(sendData,CMD_REG_RAM_BASE_VATA64+getSlotNo()*CMD_REG_SINGLE_WIDTH_VATA64,receiveData,4);

#ifdef DUMP_DEBUG
     for(uint8_t i=0; i<4; ++i){
         printf("sendData[%d] = %#x\n",i,sendData[i]);
         printf("receiveData[%d] = %#x\n",i,receiveData[i]);
     }

     fprintf(stdout, "VATA64V2::spiRegisterRead(uint16_t)---receiveData[3]=%#x ",receiveData[3]);
     fprintf(stdout,"in binary readData=");
     DumpOctInBin_16b(receiveData[3]);
#endif

     return(receiveData[3]);
}

#endif

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmStatusRead
//Function:     read the staus register in UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//Returns:      (unsigned char): return current status of the UFM
//Note:         before use function, remember to switch SPI communication to QUSB & UFM first, using function "spiToUfmOrRegister(unsigned char portNum, unsigned short data)"
//-------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
unsigned char VATA64V2::spiUfmStatusRead(unsigned char portNum) {
    // By lht, July 20, 14:09. For new parallel spi interface
    unsigned char buf[5];   //[0] FE select; [1],[2] device select;[3] UFM_CMD
    buf[0] = getSlotNo();
    buf[1] = (SPItoUFM >> 8) & 0xFF;
    buf[2] = (SPItoUFM     ) & 0xFF;
    buf[3] = UFM_CMD_RDSR;
    buf[4] = 0x00;
    writeRead(portNum, buf, 5);
    return buf[4];
}
#else
uint8_t VATA64V2::spiUfmStatusRead(){

    uint16_t sendData[3];
    uint16_t receiveData[3] ;

    sendData[0] = getSlotNo();
    sendData[1] = SPItoUFM;
    sendData[2] = UFM_CMD_RDSR &0x00FF;

    readWriteQSPI(sendData,CMD_UFM_RAM_BASE_VATA64+getSlotNo()*CMD_UFM_SINGLE_WIDTH_VATA64,receiveData,3);
    uint8_t ret = receiveData[2];
    return ret;
}

#endif

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmErase
//Function:     erase the content in UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
void VATA64V2::spiUfmErase(unsigned char portNum) {
    // By lht, July 20, 14:16. For new parallel spi interface
    unsigned char buf[4];   //[0] FE select; [1],[2] device select;[3] UFM_CMD
    buf[0] = getSlotNo();
    buf[1] = (SPItoUFM >> 8) & 0xFF;
    buf[2] = (SPItoUFM     ) & 0xFF;
    buf[3] = UFM_CMD_WREN;
    write(portNum, buf, 4);
    sleep(1);
    buf[3] = UFM_CMD_BERASE;
    write(portNum, buf, 4);
}
#else
void VATA64V2::spiUfmErase(){
    uint16_t sendData[3];

    sendData[0] = getSlotNo();
    sendData[1] = SPItoUFM;
    sendData[2] = UFM_CMD_WREN & 0x00FF;

#ifdef DUMP_DEBUG
    for(uint8_t i=0;i<3;++i) printf("\nVATA64V2::spiUfmErase()---sendData[%d]=%#x\n",i,sendData[i]);
#endif

    writeQSPI(sendData,CMD_UFM_RAM_BASE_VATA64+getSlotNo()*CMD_UFM_SINGLE_WIDTH_VATA64,3);
    sleep(1);
    sendData[2]= UFM_CMD_BERASE & 0x00FF;
    writeQSPI(sendData,CMD_UFM_RAM_BASE_VATA64+getSlotNo()*CMD_UFM_SINGLE_WIDTH_VATA64,3);
}
#endif

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmWrite
//Function:     write one word into UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              addr(unsigned short): the address in UFM to be written into
//              data(unsigned short): the data to be written into the UFM at the given address
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
void VATA64V2::spiUfmWrite(unsigned char portNum, unsigned short addr, unsigned short data) {
    // By lht, July 20, 14:25. For new parallel spi interface
    unsigned char buf[8];   //[0] FE select; [1],[2] device select;[3] UFM_CMD,[4], [5] address; [6],[7] data
    buf[0] = getSlotNo();
    buf[1] = (SPItoUFM >> 8) & 0xFF;
    buf[2] = (SPItoUFM     ) & 0xFF;
    buf[3] = UFM_CMD_WRITE;
    buf[4] = (addr >> 8)&0xFF;
    buf[5] = (addr   )&0xFF;
    buf[6] = (data >> 8)&0xFF;
    buf[7] = (data   )&0xFF;
    write(portNum, buf, 8);
}
#else
void VATA64V2::spiUfmWrite(uint16_t addr, uint16_t data){
    uint16_t sendData[5];
    sendData[0] = getSlotNo();
    sendData[1] = SPItoUFM;
    sendData[2] = UFM_CMD_WRITE & 0x00FF;
    sendData[3] = addr;
    sendData[4] = data;
    writeQSPI(sendData,CMD_UFM_RAM_BASE_VATA64+getSlotNo()*CMD_UFM_SINGLE_WIDTH_VATA64,5);
}
#endif

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmReadWord
//Function:     read one word from UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              start_addr(unsigned short): the first address in UFM to be read
//Returns:      (unsigned short) return data (16bits) read from UFM
//-------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
unsigned short VATA64V2::spiUfmReadWord(unsigned char portNum, unsigned short start_addr) {
  /*
    unsigned char buf[6];
    unsigned short data;
    spiToUfmOrRegister(0,0xC3C3);  // make sure that  spi is linked to write ufm 

    buf[0] =  UFM_CMD_READ;
    buf[1] = (start_addr>>8)&0xFF;
    buf[2] = (start_addr   )&0xFF;
    writeRead(portNum, buf, 5);
    data=(buf[3] << 8)+buf[4];
    return data;
  */
    // By lht, July 20, 14:30. For new parallel spi interface
    unsigned char buf[8];   //[0] FE select; [1],[2] device select;[3] UFM_CMD,[4], [5] address; [6],[7] data
    unsigned short data;
    buf[0] = getSlotNo();
    buf[1] = (SPItoUFM >> 8) & 0xFF;
    buf[2] = (SPItoUFM     ) & 0xFF;
    buf[3] = UFM_CMD_READ;
    buf[4] = (start_addr >> 8)&0xFF;
    buf[5] = (start_addr     )&0xFF;
    buf[6] = 0x00;
    buf[7] = 0x00;
    writeRead(portNum, buf, 8);
    data = (buf[6] << 8) + buf[7];
    return data;
}
#else
uint16_t VATA64V2::spiUfmReadWord(uint16_t addr){
    uint16_t sendData[5];
    uint16_t receiveData[5];
    sendData[0] = getSlotNo();
    sendData[1] = SPItoUFM;
    sendData[2] = UFM_CMD_READ & 0x00FF;
    sendData[3] = addr;
    sendData[4] = 0x0000;

    readWriteQSPI(sendData,CMD_UFM_RAM_BASE_VATA64+getSlotNo()*CMD_UFM_SINGLE_WIDTH_VATA64,receiveData,5);
    return receiveData[4];
}
#endif
//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmReadBlock
//Function:     read a sequence of words from UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              word_num(unsigned short): the number of words to be read
//              data(unsigned short[]): a pointer to the buffer that store the rceive data
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
void VATA64V2::spiUfmReadBlock(unsigned char portNum, unsigned short start_addr, unsigned short word_num, unsigned short data[512]) {
    unsigned char buf[64];
    unsigned short num, j;
    /*
    spiToUfmOrRegister(0,0xC3C3);  // make sure that  spi is linked to write ufm

    for (j=0; j<(word_num/30 + 1); j++)
    {
        if ((word_num-30*j) <= 30)
            num = word_num - 30*j;
        else
            num = 30;
        start_addr += 30*j;
        buf[0] =  UFM_CMD_READ;
        buf[1] = (start_addr>>8)&0xFF;
        buf[2] = (start_addr   )&0xFF;
        writeRead(portNum, buf, num*2+3);
        for(unsigned short i=0; i<num; i++) {
            data[i+j*30]=(buf[2*i+3] << 8)+buf[2*i+4];
            //                      std::cerr << "Info@SPIUfmReadBlock:readUFMfifo    readData[" << i+j*30 << "]=";
            //                         for(unsigned int bit=0; bit<16; bit++)
            //               std::cerr << ((data[i+j*30]>>(15-bit))&0x0001) << " ";
            //             std::cerr << std::endl;
        }
    }
    */
    // By lht, July 20, 14:45. For new parallel spi interface
    for(j=0; j<(word_num/29 + 1); j++)
    {
        if((word_num-29*j) <= 29)
            num = word_num - 29 * j;
        else
            num = 29;
        start_addr += 29 * j;
        buf[0] = getSlotNo();
        buf[1] = (SPItoUFM >> 8) & 0xFF;
        buf[2] = (SPItoUFM     ) & 0xFF;
        buf[3] = UFM_CMD_READ;
        buf[4] = (start_addr >> 8)&0xFF;
        buf[5] = (start_addr     )&0xFF;
        writeRead(portNum, buf, num*2+6);
        for(unsigned short i = 0; i < num; i++)
        {
            data[i + j*29] = (buf[2*i + 6] << 8) + buf[2*i + 7];
        }
    }
}
#else
bool VATA64V2::spiUfmReadBlock(uint16_t startAddr, uint16_t wordNum,uint16_t* data){
    bool error = false;
    if(wordNum>1024){
        printf("wordNum= %d is out of range, it should be less than 1024!\n",wordNum);
        return false;
    }
    uint16_t* sendData = new uint16_t[wordNum+4];
    uint16_t* receiveData = new uint16_t[wordNum+4];

    sendData[0] = getSlotNo();
    sendData[1] = SPItoUFM;
    sendData[2] = UFM_CMD_READ & 0x00FF;
    sendData[3] = startAddr;
    for(uint16_t i=0;i<wordNum;++i) sendData[i+4]=0;

    error |=!readWriteQSPI(sendData,CMD_UFM_RAM_BASE_VATA64+getSlotNo()*CMD_UFM_SINGLE_WIDTH_VATA64,receiveData,wordNum+4);

    for(uint16_t i=0; i<wordNum;++i) data[i]= receiveData[i+4];

    delete[] sendData;
    delete[] receiveData;
    return (!error);
}
#endif
//--------------------------------------------------------------------------------------------------------------------//
//Name:         selectReadChannel
//Function:     using signals send by SPI to access READ interface of spiroc, select a certain channel to be readout
//Variables:    channelIndex(unsigned int): channel index (count from 0,1,2...)
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64V2::selectReadChannel(unsigned int channelIndex) {
    setSelect_nSPI_uplink(0); // switch to use SPI signals to access spirocA READ register
    setCkbSpi(1);         //initial CLK_READ_SPI

    setDresetSpi(1);      // reset Read Register low active
    setDresetSpi(0);      // remove reset

    setShiftInbSpi(0);        // set '0' to SRIN_READ_SPI

    setCkbSpi(0);        // clock 0
    setCkbSpi(1);        // clock 1
    std::cerr << "select READ Register Channel 0" << std::endl;

    setShiftInbSpi(1);       //  set '1' to SRIN_READ_SPI

    for(unsigned int i=0; i<channelIndex; i++) {
        setCkbSpi(0);      //clock in
        setCkbSpi(1);
        std::cerr << "select READ Register Channel " << (i+1) << std::endl;
    }
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         setCkbSpi
//Function:     using signals send by SPI to access READ interface of spiroc, set CKB_spi to be hign/low
//Variables:    setValue(char) :  0=set CKB_spi to LOW    1=set CKB_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64V2::setCkbSpi(char setValue) {
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);  // read register value
    m_generalCtrlReg0 &= 0xFFFB;                                 // mask the bit
    m_generalCtrlReg0 |= ((setValue & 0x1) << 2);                   // set bit to new setValue
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0); // write register
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);  // read register value
    m_generalCtrlReg0 &= 0xFFFB;                                 // mask the bit
    m_generalCtrlReg0 |= ((setValue & 0x1) << 2);                   // set bit to new setValue
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR,m_generalCtrlReg0);
#endif
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         setShiftInbSpi
//Function:     using signals send by SPI to access READ interface of spiroc, set SHIFT_IN_B_spi to be hign/low
//Variables:    setValue(char):  0=set SHIFT_IN_B_spi to LOW    1=set SHIFT_IN_B_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64V2::setShiftInbSpi(char setValue) {
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFF7) | ((setValue & 0x01) << 3);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0); // write register
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFF7) | ((setValue & 0x01) << 3);
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0); // write register
#endif
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setDresetSpi
//Function:     using signals send by SPI to access READ interface of spiroc, set Dreset_spi to be hign/low
//Variables:    setValue(char):  0=set Dreset _spi to LOW    1=set Dreset_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64V2::setDresetSpi(char setValue) {
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFEF) | ((setValue & 0x01) << 4);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0); // write register
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFEF) | ((setValue & 0x01) << 4);
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR,m_generalCtrlReg0);
#endif
}

// //-------------------------------------------------------------------------------------------------------------------//
// //Name:         setResetbScSpi
// //Function:     using signals send by SPI to access READ interface of spiroc(actually here we access the Slow Control interface,2010.06.14 snow@EPFL), set RESETB_SC_spi to be hign/low 
// //Variables:    setValue(char):  0=set RESETB_SC_spi to LOW    1=set RESETB_SC_spi to HIGH                                          
// //Returns:      None  
// //-------------------------------------------------------------------------------------------------------------------//
// void VATA64::setResetbScSpi(char setValue) {
//         m_generalCtrlReg0 = spiRegisterRead(0,GENERAL_CTRL_REG0_ADDR);
// 	m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xF7FF) | ((setValue & 0x01)<<11);
// 	spiRegisterWrite(0,GENERAL_CTRL_REG0_ADDR,m_generalCtrlReg0);
// } 

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setHoldSpi
//Function:     using signals send by SPI to access READ interface of spiroc, set HOLD_spi to be hign/low
//Variables:    setValue(char):  0=set Hold_spi to LOW    1=set Hold_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64V2::setHoldSpi(char setValue) {
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFDF) | ((setValue & 0x01) << 5);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0); // write register
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFDF) | ((setValue & 0x01) << 5);
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR,m_generalCtrlReg0);
#endif
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setSelect_nSPI_uplink
//Function:     select signals to access READ interface of the  FrontEnd chip: 0= spi(set by C code); 1 = uplink(H, HB, S, SB);
//Variables:    setValue(char):  0=use signals from SPI(programed by C code) to access READ register of spiroc
//                               1=use signals from uplink(H, HB, S, SB) to access READ register of spiroc
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64V2::setSelect_nSPI_uplink(char setValue) {
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFBF) | ((setValue & 0x01) << 6);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0); // write register
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFBF) | ((setValue & 0x01) << 6);
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR,m_generalCtrlReg0);
#endif
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         SetTrigSelect
//Function:     set bit7 of Reg0(reset_cfg) in spirocA firmware
//Variables:    trigSelect(char):0=set bit7 to '0' =>UL_HOLD(use H, HB, S, SB as trigger signal);
//                               1=set bit7 to '1' =>NIM_trigger;
//Returns:      bool
//              true = successful
//              false= Error!
//-------------------------------------------------------------------------------------------------------------------//
void VATA64V2::setTrigSelect(char setValue) {
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFF7F) | ((setValue & 0x01) << 7);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0); // write register
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFF7F) | ((setValue & 0x01) << 7);
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR,m_generalCtrlReg0);
#endif
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         seTestpulseSpi
//Function:     set Testpulse to be high/Low, when using SPI to access READ interface of the  FrontEnd chip
//Variables:    setValue(char): 0=set Testpulse_spi to LOW    1=set Testpulse_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64V2::setTestpulseSpi(char setValue) {
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFEFF) | ((setValue & 0x01) << 8);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0); // write register
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFEFF) | ((setValue & 0x01) << 8);
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR,m_generalCtrlReg0);
#endif
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setNPWD
//Function:     set NPWD to be high/Low
//Variables:    setValue(char): 0=set NPWD to LOW    1=set NPWD to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64V2::setNPWD(char setValue) {
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFDFF) | ((setValue & 0x01) << 9);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0); // write register
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFDFF) | ((setValue & 0x01) << 9);
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR,m_generalCtrlReg0);
#endif
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         dumpFirmwareVersion
//Function:     Read and dump SPIROCA/SPIROCII card FPGA firmware version
//Variables:    
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
// Read and dump SPIROCA/SPIROCII card FPGA firmware version
void VATA64V2::dumpFirmwareVersion() {
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, FIRMWARE_VERSION_REG_ADDR);
#else
    m_generalCtrlReg0 = spiRegisterRead(FIRMWARE_VERSION_REG_ADDR);
#endif
    std::cerr << "Firmware version v" << m_generalCtrlReg0 << ".0 " << std::endl;
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         dumpFirmwareVersion
//Function:     Read and dump the current status of Gene_ctrl_reg0 in VATA64 firmware
//Variables:    
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
// Read and dump Gene_ctrl_reg0 status
void VATA64V2::dumpGeneCtrlReg0() {
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
#endif
    std::cerr << "Info@Config_VATA64: GENERAL_REG0=";
    for(unsigned int i = 0; i<16; i++)
        std::cerr << ((m_generalCtrlReg0>>(15-i))&0x0001);
    std::cerr << std::endl;
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         resetChipConfig
//Function:     Reset the Slow Control interface by setting bit0 of Reg0(reset_cfg)in spiroc firmware
//Variables:    
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64V2::resetChipConfig() {
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);

    m_generalCtrlReg0 |= 0x0001;  // set reset
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);

    m_generalCtrlReg0 &= 0xFFFE; // remove reset
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);

    m_generalCtrlReg0 |= 0x0001;  // set reset
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);

    m_generalCtrlReg0 &= 0xFFFE; // remove reset
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
#endif

    std::cerr << "resetChipConfig finished!" << std::endl;
}



//---------------------------------------------------------------------------------------------------------------------//
// Name:        compareUfmToConfigArray
// Function:    compare UFM content to chipConfigArray.
// Parameters:  dumpOn(int): = print or not the contents we write into and read back from UFM.  1=print,  0=don´t print
//              chipConfigArray = 16bits Word Array converted from FrontEndChip config file
//              compareArrayNum = Number of the Array you want to compare
// Returns:     (bool)
//              false = UFM content is identical to chipConfigArray
//              true = UFM content is different to chipConfigArray
//--------------------------------------------------------------------------------------------------------------------//
bool VATA64V2::compareUfmToConfigArray(int dumpOn,unsigned short chipConfigArray[], unsigned int compareArrayNum) {

    unsigned short chipConfigArray_readBack[60];
    bool err=false;

    //spiToUfmOrRegister(0, 0xC3C3);   // set SPI communication to UsbBoard <-> UFM
#ifndef NEW_USBBOARD
    spiUfmReadBlock(0, 0x0000, compareArrayNum, chipConfigArray_readBack);
    //getchar();
#else
    spiUfmReadBlock(0x0000, compareArrayNum, chipConfigArray_readBack);
#endif


    ///list out what we write in and read back from the UFM   
    if(dumpOn==1){
        std::cerr << "Info@compareUfmToConfigArray:Compare write in and read back:" << std::endl;
        for(unsigned int index=0;index<s_configArrayNum_VATA64;index++) {
            std::cerr << "chipConfigArray         [" << index << "]=";
            for(unsigned int i = 0; i<16; i++)
              std::cerr << ((chipConfigArray[index]>>(15-i))&0x0001) << " ";
            std::cerr << std::endl;

            std::cerr << "chipConfigArray_readBack[" << index << "]=";
            for(unsigned int i = 0; i<16; i++)
              std::cerr << ((chipConfigArray_readBack[index]>>(15-i))&0x0001) << " ";
            std::cerr << std::endl;
 
            std::cerr << std::endl;      
	}
    }
    ////////////////////////////////////////////////////////

    for(unsigned int i=0; i<compareArrayNum; i++) {
        if (chipConfigArray_readBack[i] != chipConfigArray[i])
        {
            err = true;
            std::cerr << "Infomation:Content in UFM is diffenert from Config file" << std::endl;
            break;
        }
    }
    return(err);
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        writeConfigArrayToUfm
// Function:    write chip Config Array into UFM
// Parameters:  chipConfigArray = 16bits Word Array converted from FrontEndChip config file
//              writeConfigArrayNum = Number of the Array you want to write into UFM
// Return:      None
//---------------------------------------------------------------------------------------------------------------------//
void VATA64V2::writeConfigArrayToUfm(unsigned short chipConfigArray[], unsigned int writeConfigArrayNum) {

#ifndef NEW_USBBOARD
    unsigned char ufmStatus = spiUfmStatusRead(0) ;


#else
     unsigned char ufmStatus = spiUfmStatusRead() ;
#endif
     while(0){
#ifndef NEW_USBBOARD
     ufmStatus = spiUfmStatusRead(0) ;

#else
     ufmStatus = spiUfmStatusRead() ;
#endif

     std::cerr << "Info:before erase UFM, ufmStatus = ";
       for(unsigned int i = 0; i<8; i++)
         std::cerr << ((ufmStatus>>(8-i))&0x0001);
       std::cerr << std::endl;  
       // getchar();
     }

 
    std::cerr << "Info:Write Config Arrays to UFM....." << std::endl;
    unsigned short addr = 0x0000;
    addr = 0x0000;

    //spiToUfmOrRegister(0, 0xC3C3);   // set SPI communication to UsbBoard <-> UFM
    std::cerr << "Erase UFM 1st!" << std::endl;
#ifndef NEW_USBBOARD
    spiUfmErase(0);
#else
    spiUfmErase();
#endif
    sleep(1);   // wait 1s  !!Don't delete it!!  after spiUfmErase(0), we have to wait! Otherwise, ufmerase will not be successful!

    std::cerr << "Erase UFM 2nd!" << std::endl;
#ifndef NEW_USBBOARD
    spiUfmErase(0);
#else
    spiUfmErase();
#endif
    sleep(1);  // wait 1s  !!Don't delete it!!  after spiUfmErase(0), we have to wait! Otherwise, ufmerase will not be successful!


    //only for debug-------------------------------------
////    printf("stop after ufmErase\n");
////    getchar();
//    unsigned short chipConfigArray_readBack[60];
//    for(int i = 0; i<60; i++) chipConfigArray_readBack[i] = 0;
//#ifdef NEW_USBBOARD
//    spiUfmReadBlock(0x0000, writeConfigArrayNum, chipConfigArray_readBack);
//#else
//    spiUfmReadBlock(0,0x0000, writeConfigArrayNum, chipConfigArray_readBack);
//#endif

//        std::cerr << "Info@compareUfmToConfigArray:Compare write in and read back:" << std::endl;
//        for(unsigned int index=0;index<s_configArrayNum_VATA64;index++) {
//            std::cerr << "chipConfigArray_readBack[" << index << "]=";
//            for(unsigned int i = 0; i<16; i++)
//              std::cerr << ((chipConfigArray_readBack[index]>>(15-i))&0x0001) << " ";
//            std::cerr << std::endl;

//            std::cerr << std::endl;
//        }
    //----------------------------------------------------------

    for(unsigned int i=0; i<writeConfigArrayNum; i++, addr++) {
#ifndef NEW_USBBOARD
        spiUfmWrite(0, addr, chipConfigArray[i]);
#else
        spiUfmWrite(addr, chipConfigArray[i]);
#endif
    }
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        sendStartConfigCMD
// Function:    set "start_cfg" in VATA64 firmware, the front end board will begin to write slow contro configuration from UFM to Slow Control Register
// Parameters:
// Returns:      None
//---------------------------------------------------------------------------------------------------------------------//
void VATA64V2::sendStartConfigCMD() {
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 |= 0x0002;
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);  // send config from UFM to SPIROCA
    m_generalCtrlReg0 &= 0xFFFD;
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);  // load config
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 |= 0x0002;
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);  // send config from UFM to SPIROCA
    m_generalCtrlReg0 &= 0xFFFD;
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);  // load config
#endif

}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        loadConfigToVATA64
// Function:    begin to configure Slow Control Register of VATA64 by setting "start_cfg". The configuration will be executed twice.compare UFM content with output of SROUT_SC at the end of the second one, in order to check the writing process is correct or not.
// Parameters:
// Returns:(bool)
//              false = configuration succeed
//              true  = configuration error
//--------------------------------------------------------------------------------------------------------------------//
bool VATA64V2::loadConfigToVATA64() {
    bool err = false;
    unsigned short generalCtrlReg0;


    // -------------------1st time config------------------------------

    //resetChipConfig();        // reset
    sendStartConfigCMD();
    std::cerr << "send Start_cfg!" << std::endl;
    while (1)
    {
#ifndef NEW_USBBOARD
        generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
#else
        generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
#endif
        //dumpGeneCtrlReg0();
        if (generalCtrlReg0 & 0x4000) {//config finished
            std::cerr << "Infomation: 1st load  Config from UFM to Spiroc,   Finished!" << std::endl;
            //dumpGeneCtrlReg0();
            if (generalCtrlReg0 & 0x8000)    {   //config error
                std::cerr << "ERROR:config error = 1, There is something wrong in VATA64 configuration! Please check!" << std::endl;
            }
            break;
        }
    }

    // --------------------2nd time ------------------------------

    sendStartConfigCMD();
    std::cerr << "Infomation: 2nd Load Config from UFM to Spiroc!...." << std::endl;
    while (1)
    {
#ifndef NEW_USBBOARD
        generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
#else
        generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
#endif
        if (generalCtrlReg0 & 0x4000)  {   //config finished
            if (generalCtrlReg0 & 0x8000)    {   //config error
                std::cerr << "ERROR:config error = 1, There is something wrong in VATA64 configuration!please check!" << std::endl;
                err = true;
                //assert(false);
            }else{
                std::cerr << "Infomation:Config Spiroc Successfully!" << std::endl;
            }
            break;

        }
    }
    return(err);

}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        spiConfigVATA64
// Function:    configure Both Gene_Ctrl_Reg0 in VATA64 firmware and slow control register in spirocA chip
// Parameters:
// Returns:    (bool)
//              false = configuration succeed
//              true  = configuration error
//--------------------------------------------------------------------------------------------------------------------//
//config the spirocA with the slow control setting
bool VATA64V2::spiConfigVATA64(int dumpOn, unsigned short chipConfigArray[], char trigSelect)
{

    bool  err = false;

    std::cerr << "Begin to config VATA64V2..." << std::endl;

    
    while(0){
        dumpFirmwareVersion();
        getchar();
        dumpGeneCtrlReg0();
        getchar();
    }
    dumpFirmwareVersion();

    dumpGeneCtrlReg0();


    // Test R/W Gene_ctrl_reg0,
    // a loop to write an increasing number (from 0 to 0x3FFF) to Gene_ctrl_reg0 and read it back to check
//     for(unsigned short value=0; value<0x4000; value+=16) {
//         spiRegisterWrite(0,1,value);
//         if((spiRegisterRead(0,1)&0x3FFF)!= value) {
// 	    std::cerr << "for R/W Value  " << value <<"  is not correct!"<< std::endl; 
//             assert(false);
// 	}
//     }
//     std::cerr << "finish check register R/W, set GeneCtrlReg0 = 0xC4" << std::endl;
//     spiRegisterWrite(0,1,0xC4);
//     dumpGeneCtrlReg0();
//     getchar();
    //////////////////////////////////////////////////////////////////////////////////////////



    if (dumpOn==1) {
        std::cerr << "Info@spiConfigVATA64:Array write into UFM:" << std::endl;
        for(unsigned int index=0;index<s_configArrayNum_VATA64;index++) {
            std::cerr << "chipConfigArray[" << index << "]=";
            for(unsigned int i = 0; i<16; i++)
                std::cerr << ((chipConfigArray[index]>>(15-i))&0x0001) << " ";
            std::cerr << std::endl;
        }
    }

    // getchar();


    
    // reset the VATA64 config
    // resetChipConfig();

    // set trigSelect
    // setTrigSelect(trigSelect);

    std::cerr << "Info@spiConfigVATA64:after register config:" << std::endl;
    dumpGeneCtrlReg0();


    //--------------End of Reg0 Config--------------------------------------


    //--------------Begin to write config arrays into UFM ------------------
  
  
//     while(1){
//        writeConfigArrayToUfm(chipConfigArray, s_configArrayNum_VATA64);
//        compareUfmToConfigArray(1,chipConfigArray, s_configArrayNum_VATA64);
//     }

    // compare UFM content to chipConfigArray
    // 0 = UFM content is identical to chipConfigArray, skip unnecessary writing config arrays into UFM
    // 1 = UFM content is different to chipConfigArray, write config array again into UFM
    if (compareUfmToConfigArray(1,chipConfigArray, s_configArrayNum_VATA64) == 1) {
        do{
            writeConfigArrayToUfm(chipConfigArray, s_configArrayNum_VATA64);
        }while(compareUfmToConfigArray(1,chipConfigArray, s_configArrayNum_VATA64));
        std::cerr << "Info@Config_VATA64:Write Config to UFM successfully! " << std::endl;
    }else{// continue writing config arrays in UFM until UFM content is identical to chipConfigArray
         std::cerr << "Info@Config_VATA64:content in UFM is identical to cfg file. " << std::endl;
    }


    // load config file from UFM into VATA64
    bool loadConfigErr = false;
    do {
        std::cout<<"load config arrays in UFM to vata64 chip....."<<std::endl;
        loadConfigErr=loadConfigToVATA64();
    }while(loadConfigErr == true);

    //select the read access
    setSelect_nSPI_uplink(1);// select Up-Link access of the read interface
    return(err);
}

//-------------------------for Analogue Test of the chip-----------------------------------------
//---------------------------------------------------------------------------------------------------------------------//
// Name:        analogTest
// Function:    Use SPI (C code programed) signals to access spiroc READ register, in order to do analog test for the chip
// Parameters:
// Returns:None
//--------------------------------------------------------------------------------------------------------------------//
void VATA64V2::analogTest() {
    setHoldSpi(0) ;        // set Hold signal to Analogue memory Hold signal

    setDresetSpi(1);    // set Dreset
    setDresetSpi(0);    // move Dreset

    selectReadChannel(1);
    while(0) {
        std::cerr << "set pulse" << std::endl;
        setTestpulseSpi(1);          // set Testpulse =1
        setTestpulseSpi(0);          // set Testpulse =0
    }

    //       setHoldSpi(0) ;        // set Hold signal to Analogue memory Hold signal
    //getchar();
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        selectContinousMode
// Function:    Use SPI (C code programed) signals to select a certain channel to be readout 
// Parameters:  channelIndex (unsigned int): the selected channel index (0,1....)         
// Returns:None
//--------------------------------------------------------------------------------------------------------------------//
void VATA64V2::selectContinuousMode(unsigned int channelIndex){
     setSelect_nSPI_uplink(0); // switch to use SPI signals to access spirocA READ register

     setDresetSpi(1);    // set Dreset, in order to clear read register
     setDresetSpi(0);    // move Dreset
     selectReadChannel(channelIndex);    //selec readout channel
}
//---------------------------------------------------------------------------------------------------------------------//
// Name:        readSensor
// Function:    Read sensor value(AD7888) on vata card
// Parameters:  data(point of unsigned short array): buffer where sotore the value---> BIAS_A_MON  CURR_A_MON  TEMP_A_MON  BIAS_B_MON  CURR_B_MON  TEMP_B_MON
// Returns:     true if read back successfully
//--------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
bool VATA64V2::readSensor(unsigned short * data){
    unsigned char temp[30];

    for(int i=0; i<6; i++){
        temp[5*i] = getSlotNo();
        temp[5*i+1] = (SPItoADC >> 8) & 0xFF;
        temp[5*i+2] = (SPItoADC     ) & 0xFF;
        temp[5*i+3] = CMD_Array[2*i+2];
        temp[5*i+4] = CMD_Array[2*i+3];
    }
    bool noErr;
    unsigned char firstCMD[5];
    firstCMD[0] = getSlotNo();
    firstCMD[1] = (SPItoADC >> 8) & 0xFF;
    firstCMD[2] = (SPItoADC     ) & 0xFF;
    firstCMD[3] = CMD_Array[0];
    firstCMD[4] = CMD_Array[1];


    noErr = write(0,firstCMD,5);

    assert(noErr);
    for(int i=0; i<3; i++){
        //getchar();
        noErr = writeRead2Cycles(0,&temp[10*i],5,&temp[10*i+5],5);
        assert(noErr);
    }		//read 6 channels

    for(int i=0; i<6; i++){
        temp[5*i+3] = temp[5*i+3] & 0x7F; //since  the first leading zero may not be set up in time for the Qusb to read it correctly(SCL maintain '0' while spi on idle status)
        std::cerr << int(temp[5*i+3]) << " while i = "<< i <<std::endl;
        if((temp[5*i+3] & 0xF0) != 0){
            std::cout << "slot no = " << this->m_slotNo << std::endl;
            std::cout << "Something wrong with the value reading back from AD7888" << std::endl;     //other 3 leading bits should be 0
            std::cerr << int(temp[5*i+3]) << std::endl;
            //getchar();
            return false;
        }
        data[i] =(temp[5*i+3]<<8) + temp[5*i+4];
        //std::cerr << data[i] << " data[] while i = "<< i <<std::endl;
    }
    return true;
}
#else
bool VATA64V2::readSensor(unsigned short * data){
    uint16_t temp[5];
    uint16_t readBack[5];

    temp[0] = getSlotNo();
    temp[1] = SPItoADC;
    for(int i=0; i<3; i++){
        temp[i+2] = (CMD_Array[2*i+2]<<8)|CMD_Array[2*i+3];
    }
    bool noErr = true;
    noErr = readWriteQSPI(temp,CMD_ADC_RAM_BASE_VATA64+getSlotNo()*CMD_ADC_SINGLE_WIDTH_VATA64,readBack,5);

    for(int i=0; i<3; i++){
        readBack[i+2] = readBack[i+2] & 0x7FFF; //since  the first leading zero may not be set up in time for the Qusb to read it correctly(SCL maintain '0' while spi on idle status)
        std::cerr << readBack[i+2] << " while i = "<< i <<std::endl;
        if((readBack[i+2] & 0xF000) != 0){
            std::cout << "slot no = " << this->m_slotNo << std::endl;
            std::cout << "Something wrong with the value reading back from AD7888" << std::endl;     //other 3 leading bits should be 0
            std::cerr << int(temp[i+2]) << std::endl;
            //getchar();
            return false;
        }
        data[i] =readBack[i+2];
        //std::cerr << data[i] << " data[] while i = "<< i <<std::endl;
    }
    return true;
}

#endif

bool VATA64V2::getSensorValues(float* sensorValues){
    unsigned short adValues[10];
    for(unsigned int i=0;i<10;i++) adValues[i] = 0;
    if(!readSensor(adValues)){
         printf("VATA64V2::getSensorValues()---readSsensor() failed!\n");
         return false;
    }
    
    m_sensorCalParas = SensorCalParasList[m_configuration->vata64BoardIndex()];
    
    sensorValues[0] = (adValues[0] - m_sensorCalParas.calb_biasA)/m_sensorCalParas.cala_biasA;
    sensorValues[1] = (adValues[1] - m_sensorCalParas.calb_curr)/m_sensorCalParas.cala_curr;
    sensorValues[3] = (adValues[3] - m_sensorCalParas.calb_biasB)/m_sensorCalParas.cala_biasB;
    sensorValues[4] = (adValues[4] - m_sensorCalParas.calb_curr)/m_sensorCalParas.cala_curr;

    
    std::cout << "*******************************************************" << std::endl
              << "Sensor on VATA64 Card:" << std::endl
              << "BIAS_A_MON = " << adValues[0] << "        BIAS_B_MON = " << adValues[3] << std::endl
              << "CURR_A_MON = " << adValues[1] << "        CURR_B_MON = " << adValues[4] << std::endl
              << "TEMP_A_MON = " << adValues[2] << "        TEMP_B_MON = " << adValues[5] << std::endl                             
              << "Based on measured result of vata64_board_"<< m_configuration->vata64BoardIndex() <<", get the following results:"<<std::endl;
             
          printf("biasA_sipm = %.2f V        biasB_sipm = %.2f V \n", sensorValues[0], sensorValues[3]);
          printf("currA_sipm = %.2f nA       currB_sipm = %.2f nA\n", sensorValues[3],sensorValues[4]);
          std::cout << "*******************************************************" << std::endl;
    return true;
} 



void VATA64V2Configuration::SetPreamplifierInputPortentialDACs(int setValues[]){
   for(int i=0; i<64;i++)  m_cfgVATA64.Preamplifier_input_potential_DAC[i] = setValues[i];
}

void VATA64V2Configuration::PreamplifierInputPortentialDACs(unsigned short* DACvalues){
   for(int i=0; i<64; i++) DACvalues[i]=m_cfgVATA64.Preamplifier_input_potential_DAC[i];
}


//---------------------------------------------------------------------------------------------------------------------//
// Name:        readSensor
// Function:    Read sensor value(AD7888) on vata card,saved as private parameters.
// Returns:     true if read back successfully
//--------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
bool VATA64V2::readSensor(){
    unsigned char temp[30];

    for(int i=0; i<6; i++){
        temp[5*i] = getSlotNo();
        temp[5*i+1] = (SPItoADC >> 8) & 0xFF;
        temp[5*i+2] = (SPItoADC     ) & 0xFF;
        temp[5*i+3] = CMD_Array[2*i+2];
        temp[5*i+4] = CMD_Array[2*i+3];
    }
    bool noErr;
    unsigned char firstCMD[5];
    firstCMD[0] = getSlotNo();
    firstCMD[1] = (SPItoADC >> 8) & 0xFF;
    firstCMD[2] = (SPItoADC     ) & 0xFF;
    firstCMD[3] = CMD_Array[0];
    firstCMD[4] = CMD_Array[1];


    noErr = write(0,firstCMD,5);

    assert(noErr);
    for(int i=0; i<3; i++){
        //getchar();
        noErr = writeRead2Cycles(0,&temp[10*i],5,&temp[10*i+5],5);
        assert(noErr);
    }		//read 6 channels

    unsigned int data[6];
    for(int i=0;i<6;i++) data[i] = 0;


    for(int i=0; i<6; i++){
        temp[5*i+3] = temp[5*i+3] & 0x7F; //since  the first leading zero may not be set up in time for the Qusb to read it correctly(SCL maintain '0' while spi on idle status)
        //std::cerr << int(temp[5*i+3]) << " while i = "<< i <<std::endl;
        if((temp[5*i+3] & 0xF0) != 0){
            std::cout << "slot no = " << this->m_slotNo << std::endl;
            std::cout << "Something wrong with the value reading back from AD7888" << std::endl;     //other 3 leading bits should be 0
            std::cerr << int(temp[5*i+3]) << std::endl;
            //getchar();
            return false;
        }
        data[i] =(temp[5*i+3]<<8) + temp[5*i+4];
        //std::cerr << data[i] << " data[] while i = "<< i <<std::endl;
    }

    //record the adc value
    m_sensorsInfo.biasA_adc = data[0];
    m_sensorsInfo.biasB_adc = data[3];
    m_sensorsInfo.currA_adc = data[1];
    m_sensorsInfo.currB_adc = data[4];
    m_sensorsInfo.tempA_adc = data[2];
    m_sensorsInfo.tempB_adc = data[5];

    //caculate the physical values of the sensor.
    float cala_biasA = 0.0258;
    float calb_biasA = 0;


    float cala_currA = 12;
    float calb_currA = 0;

    float cala_tempA = -0.1842;
    float calb_tempA = 621.86;

    float offset_tempA =30;


// The following part is specialized for ECAL testbeam setup on 2012.07    , by Snow@EPFL
//    switch(uplinkId()){

//       case 50:
//          cala_biasA = 0.0258;     calb_biasA = -0.4;
//          cala_currA = 12;      calb_currA = 0;
//          offset_tempA = 1;

//          break;
//       case 51:
//          cala_biasA = 0.0258;     calb_biasA = -0.6;
//          cala_currA = 12;      calb_currA = 0;
//          offset_tempA = 6;
//          break;
//       case 52:
//          cala_biasA = 0.0258;     calb_biasA = -0.3;
//          cala_currA = 12;      calb_currA = 0;
//          offset_tempA = 30;
//          break;
//       case 53:
//          cala_biasA = 0.0258;     calb_biasA = -0.3;
//          cala_currA = 12;      calb_currA = 0;
//          offset_tempA = -13;
//          break;
//       case 54:
//          cala_biasA = 0.0258;     calb_biasA = 0.6;
//          cala_currA = 12;      calb_currA = 0;
//          break;
//       case 55:
//          cala_biasA = 0.0258;     calb_biasA = -0.5;
//          cala_currA = 12;      calb_currA = 0;
//          break;
//       case 56:
//          cala_biasA = 0.0258;     calb_biasA = -0.4;
//          cala_currA = 12;      calb_currA = 0;
//          break;
//       case 57:
//          cala_biasA = 0.0258;     calb_biasA = 0.1;
//          cala_currA = 12;      calb_currA = 0;
//          break;
//       case 100:
//          cala_biasA = 0.0258;     calb_biasA = 0.4;
//          cala_currA = 12;      calb_currA = 0;
//          offset_tempA = -4;
//          break;
//       case 101:
//          cala_biasA = 0.0258;     calb_biasA = 0.7;
//          cala_currA = 12;      calb_currA = 0;
//          offset_tempA = -13;
//          break;
//       case 102:
//          cala_biasA = 0.0258;     calb_biasA = 0;
//          cala_currA = 12;      calb_currA = 0;
//          offset_tempA = -16;
//          break;
//       case 103:
//          cala_biasA = 0.0258;     calb_biasA = 0.2;
//          cala_currA = 12;      calb_currA = 0;
//          offset_tempA = -18;
//          break;
//       case 104:
//          cala_biasA = 0.0258;     calb_biasA = 0.4;
//          cala_currA = 12;      calb_currA = 0;
//          break;
//       case 105:
//          cala_biasA = 0.0258;     calb_biasA = 0.1;
//          cala_currA = 12;      calb_currA = 0;
//          break;
//       case 106:
//          cala_biasA = 0.0258;     calb_biasA = -0.3;
//          cala_currA = 12;      calb_currA = 0;
//          break;
//       case 107:
//          cala_biasA = 0.0258;     calb_biasA = -0.1;
//          cala_currA = 12;      calb_currA = 0;
//          break;
//       default:
//          cala_biasA = 0.0258;     calb_biasA = 0;
//          cala_currA = 12;      calb_currA = 0;  //  m_sensorsInfo.tempB_C = cala_tempB*m_sensorsInfo.tempB_adc+calb_tempB;
//    }

    m_sensorsInfo.biasA_V = cala_biasA*m_sensorsInfo.biasA_adc+calb_biasA-2.5; // values refer to GND
   // m_sensorsInfo.biasB_V = cala_biasB*m_sensorsInfo.biasB_adc+calb_biasB-2.5;

    m_sensorsInfo.currA_nA = cala_currA*m_sensorsInfo.currA_adc+calb_currA;
   // m_sensorsInfo.currB_nA = (m_sensorsInfo.currB_adc-calb_currB)/cala_currB;

   // m_sensorsInfo.tempA_C = cala_tempA*m_sensorsInfo.tempA_adc+calb_tempA;
    m_sensorsInfo.tempA_C = (4.1*4096/(2.5*(m_sensorsInfo.tempA_adc-offset_tempA))-2)/0.00385;


    printf("--------    sensor values      ---------\n");
    printf("---                    uplinkId = %d                   ---\n",uplinkId());
    //printf("---   biasA = %.1f V   (toGND)        biasA(ADC) = %d---\n",m_sensorsInfo.biasA_V,m_sensorsInfo.biasA_adc);
    printf("---   biasA = %.1f V   (-0.5V DAC on FE to get detector bias)        \n",m_sensorsInfo.biasA_V);
    //printf("---   currA = %.2f uA                   currA(ADC) = %d---\n",m_sensorsInfo.currA_nA/1000,m_sensorsInfo.currA_adc);
    printf("---   currA = %.2f uA                   \n",m_sensorsInfo.currA_nA/1000);
    if(m_sensorsInfo.tempA_adc > 2800){
    printf("---   tempA = %.1f C                    tempA(ADC) = %d---\n",m_sensorsInfo.tempA_C,m_sensorsInfo.tempA_adc);
    //printf("---   tempA = %.1f C                    \n",m_sensorsInfo.tempA_C);
    }else{
    //printf("---   tempA = NOT ATTACHED              tempA(ADC) = %d---\n",m_sensorsInfo.tempA_adc);
    printf("---   tempA = NOT ATTACHED              \n",m_sensorsInfo.tempA_adc);
    }
    printf("--------------------------------------------------\n\n");

    std::stringstream sensorInfo;
    sensorInfo << "-------------sensor values -----------------------\n"
               << "---          uplinkId = "<<uplinkId()<<"                     ---\n"
               << "---   biasA = "<<m_sensorsInfo.biasA_V<<" V   ---\n"
               << "---   biasA(ADC) ="<<m_sensorsInfo.biasA_adc << " ---\n"
               << "---   currA = "<<m_sensorsInfo.currA_nA<<" nA   ---\n"
               << "---   currA(ADC) ="<<m_sensorsInfo.currA_adc << " ---\n"
               << "---   tempA = "<<m_sensorsInfo.tempA_C<<" C   ---\n"
               << "---   tempA(ADC) =  "<<m_sensorsInfo.tempA_adc <<" ---\n"
               <<std::endl;
    Settings::instance()->AddSensorInfos(uplinkId(),sensorInfo.str());
    return true;
}


#else
bool VATA64V2::readSensor(){
    uint16_t temp[5];
    uint16_t readBack[5];
    unsigned int data[3];
    for(int i=0;i<3;i++) data[i] = 0;

    temp[0] = getSlotNo();
    temp[1] = SPItoADC;
    for(int i=0; i<3; i++){
        temp[i+2] = (CMD_Array[2*i+2]<<8)|CMD_Array[2*i+3];
    }
    bool noErr = true;
    noErr = readWriteQSPI(temp,CMD_ADC_RAM_BASE_VATA64+getSlotNo()*CMD_ADC_SINGLE_WIDTH_VATA64,readBack,5);

    for(int i=0; i<3; i++){
        readBack[i+2] = readBack[i+2] & 0x7FFF; //since  the first leading zero may not be set up in time for the Qusb to read it correctly(SCL maintain '0' while spi on idle status)
        std::cerr << readBack[i+2] << " while i = "<< i <<std::endl;
        if((readBack[i+2] & 0xF000) != 0){
            std::cout << "slot no = " << this->m_slotNo << std::endl;
            std::cout << "Something wrong with the value reading back from AD7888" << std::endl;     //other 3 leading bits should be 0
            std::cerr << int(temp[i+2]) << std::endl;
            //getchar();
            return false;
        }
        data[i] =readBack[i+2];
        //std::cerr << data[i] << " data[] while i = "<< i <<std::endl;
    }


    //record the adc value
    m_sensorsInfo.biasA_adc = data[0];
    m_sensorsInfo.currA_adc = data[1];
    m_sensorsInfo.tempA_adc = data[2];

    //caculate the physical values of the sensor.
    float cala_biasA = 0.0258;
    float calb_biasA = 0;
    float cala_currA = 12;
    float calb_currA = 0;

    float offset_tempA = 0;

    switch(uplinkId()){
       case 47:
          cala_biasA = 0.025779;     calb_biasA = 0.290887;
          cala_currA = 0.0846;      calb_currA = 0.3692;
          printf("for uplink47 use calibration value for vata64_board_2\n");
          break;

       case 46:
          cala_biasA = 0.025713;     calb_biasA = 0.238812;
          cala_currA = 0.0846;      calb_currA = 0.3692;
          printf("use slot46 (uplink6)calibration value for vata64_board_4\n");
          break;
       default:
          cala_biasA = 0.025779;     calb_biasA = 0.290887;
          cala_currA = 0.0846;      calb_currA = 0.3692;
          printf("use default calibration value, this might not be optimized for your vata64 cards \n");
    }



    m_sensorsInfo.biasA_V = cala_biasA*m_sensorsInfo.biasA_adc+calb_biasA-2.5; // values refer to GND
   // m_sensorsInfo.biasB_V = cala_biasB*m_sensorsInfo.biasB_adc+calb_biasB-2.5;

    m_sensorsInfo.currA_nA = cala_currA*m_sensorsInfo.currA_adc+calb_currA;
   // m_sensorsInfo.currB_nA = (m_sensorsInfo.currB_adc-calb_currB)/cala_currB;

   // m_sensorsInfo.tempA_C = cala_tempA*m_sensorsInfo.tempA_adc+calb_tempA;
    m_sensorsInfo.tempA_C = (4.1*4096/(2.5*(m_sensorsInfo.tempA_adc-offset_tempA))-2)/0.00385;


    printf("-------------sensor values -----------------------\n");
    printf("---          uplinkId = %d                     ---\n",uplinkId());
    //printf("---   biasA = %.3f V          ---\n",m_sensorsInfo.biasA_V);
    printf("---   biasA(ADC) = %d         ---\n",m_sensorsInfo.biasA_adc);
    //printf("---   currA = %.3f nA         ---\n",m_sensorsInfo.currA_nA);
    printf("---   currA(ADC) = %d         ---\n",m_sensorsInfo.currA_adc);
    //printf("---   tempA = %.3f C          ---\n",m_sensorsInfo.tempA_C);
    printf("---   tempA(ADC) = %d         ---\n",m_sensorsInfo.tempA_adc);
    printf("--------------------------------------------------\n");

    std::stringstream sensorInfo;
    sensorInfo << "-------------sensor values -----------------------\n"
               << "---          uplinkId = "<<uplinkId()<<"                     ---\n"
      //         << "---   biasA = "<<m_sensorsInfo.biasA_V<<" V   ---\n"
               << "---   biasA(ADC) ="<<m_sensorsInfo.biasA_adc << " ---\n"
      //         << "---   currA = "<<m_sensorsInfo.currA_nA<<" nA   ---\n"
               << "---   currA(ADC) ="<<m_sensorsInfo.currA_adc << " ---\n"
      //         << "---   tempA = "<<m_sensorsInfo.tempA_C<<" C   ---\n"
               << "---   tempA(ADC) =  "<<m_sensorsInfo.tempA_adc <<" ---\n"
               <<std::endl;
    Settings::instance()->AddSensorInfos(uplinkId(),sensorInfo.str());
    return true;
}

#endif


#ifndef NEW_USBBOARD
void VATA64V2::spiWriteAD5322(unsigned char portNum, unsigned short inputShiftReg){
    unsigned char buf[7];   //[0] FE select; [1],[2] device select; [3],[4] address; [5],[6] data
    buf[0] = getSlotNo();
    buf[1] = (SPItoLED >> 8) & 0xFF;
    buf[2] = (SPItoLED     ) & 0xFF;
    //buf[3] = (inputShiftReg >> 8) & 0xFF;
    //buf[4] = (inputShiftReg     ) & 0xFF;
    //inputShiftReg = 0x07FF;
    buf[3] = (inputShiftReg >> 8) & 0xFF;
    buf[4] = (inputShiftReg     ) & 0xFF;
    write(portNum, buf, 5);
}
#else
void VATA64V2::spiWriteAD5322(uint16_t inputShiftReg){
    uint16_t sendData[3];

    sendData[0] = getSlotNo();
    sendData[1] = SPItoADC;
    sendData[2] = inputShiftReg;

    writeQSPI(sendData,CMD_ADC_RAM_BASE_VATA64+getSlotNo()*CMD_ADC_SINGLE_WIDTH_VATA64,3);
}
#endif

void VATA64V2::setAD5322ChanA( unsigned short DAvalue){
    unsigned short inputShiftReg = DAvalue & DATA_MASK;
#ifndef NEW_USBBOARD
    spiWriteAD5322(0,inputShiftReg);
#else
    spiWriteAD5322(inputShiftReg);
#endif

    m_configuration->SetLedA_DAC(DAvalue);
}

void VATA64V2::setAD5322ChanB(unsigned short DAvalue){
    unsigned short inputShiftReg = (DAvalue & DATA_MASK)+SELECT_nA_B;
#ifndef NEW_USBBOARD
    spiWriteAD5322(0,inputShiftReg);
#else
    spiWriteAD5322(inputShiftReg);
#endif
    m_configuration->SetLedB_DAC(DAvalue);

}

void VATA64V2::ledA1On(){
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 |= LED_A1;
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    m_configuration->SetLedA1_On(true);
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 |= LED_A1;
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    m_configuration->SetLedA1_On(true);
#endif
}

void VATA64V2::ledA1Off(){
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 &= ~LED_A1;
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    m_configuration->SetLedA1_On(false);
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 &= ~LED_A1;
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    m_configuration->SetLedA1_On(false);
#endif
}

void VATA64V2::ledA2On(){
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 |= LED_A2;
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    m_configuration->SetLedA2_On(true);
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 |= LED_A2;
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    m_configuration->SetLedA2_On(true);
#endif
}

void VATA64V2::ledA2Off(){
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 &= ~LED_A2;
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    m_configuration->SetLedA2_On(false);
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 &= ~LED_A2;
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    m_configuration->SetLedA2_On(false);
#endif
}

void VATA64V2::ledB1On(){
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 |= LED_B1;
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    m_configuration->SetLedB1_On(true);
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 |= LED_B1;
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    m_configuration->SetLedB1_On(true);
#endif
}

void VATA64V2::ledB1Off(){
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 &= ~LED_B1;
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    m_configuration->SetLedB1_On(false);
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 &= ~LED_B1;
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    m_configuration->SetLedB1_On(false);
#endif
}

void VATA64V2::ledB2On(){
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 |= LED_B2;
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    m_configuration->SetLedB2_On(true);
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 |= LED_B2;
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    m_configuration->SetLedB2_On(true);
#endif
}

void VATA64V2::ledB2Off(){
#ifndef NEW_USBBOARD
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 &= ~LED_B2;
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    m_configuration->SetLedB2_On(false);
#else
    m_generalCtrlReg0 = spiRegisterRead(GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 &= ~LED_B2;
    spiRegisterWrite(GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    m_configuration->SetLedB2_On(false);
#endif
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadBoardSettings
//Function:     read VATA64 slow control config file, store all the settings to struct VATA64_CFG_TYPE
//Variables:    filePath(string): path to the slow contro config file of VATA64
//Returns:      (bool) return error value of this process.
//              true = error exist!
//              false= execute successfully!
//-------------------------------------------------------------------------------------------------------------------//
bool VATA64V2Configuration::loadBoardSettings(const std::string &filePath){
    std::stringstream stream;
    stream << filePath ;             //get config file name
    std::cerr << "Information: Loading VATA64 configuration from file " << stream.str() << std::endl;
    std::ifstream fileStream(stream.str().c_str());      // open config file
    if (!fileStream){
      std::cerr << "Failure: could not open file: \"" << stream.str()<< "\"." << std::endl;
      std::cerr << "Please check if the path is right or the frontend board configure file exists!" << std::endl;
           assert(false);
     }

    std::cerr << "Choose VATA64, reading Config file..."<<std::endl;

    /////////// read in Settings from the config file////////////
    //  Scan will begin from "# Begin of board Settings" , and stop if "#--End of board Settings--" appears.
    //  That means , all the settings should be writtin in the above range.

    printf("---------------VATA64V2Configuration::loadBoardSettings  -- uplink %d /////-----------------\n",GetUplinkId());
    bool err = true;
    bool isInSettingsRange = false;
    unsigned int temp = 0;
    std::string s;
    while (true) {
        std::getline(fileStream, s);
        //printf("line  = %s\n",s.c_str());
        if (stream.eof())
            break;
        const char* searchString = s.c_str();


        if(isInSettingsRange) err = true;      // Once the line belongs to the settings range,During each loop,err will be set back to false ,as long as one sentence in correct format has been read.Otherwise it will stay in ture
        else err = false;

        if (s.find("#") == 0 || s=="") {
            if(s.find("# End of board Settings") == 0 ){
                err = false ;
                isInSettingsRange = false;
                break;
            }

            if(s.find("# Begin of board Settings") == 0){
                printf("%s \n",searchString);
                err = false;
                isInSettingsRange = true;
            }
            continue; // Skip commented lines
        }

        if(sscanf(searchString,"trigSelect || %d ||", &m_trigSelect)==1){
              err = false;
              printf("trigSelect = %d\n", m_trigSelect);
        }

        if(sscanf(searchString, "ledA_Intensity || %hu ||", &m_ledA_DAC) == 1) {
              err = false;
              printf("ledA_Intensity = %d\n", m_ledA_DAC);
        }

        if(sscanf(searchString, "ledB_Intensity || %hu ||", &m_ledB_DAC) == 1) {
              err = false;
              printf("ledB_Intensity = %d\n", m_ledB_DAC);
        }

        if(sscanf(searchString, "LED_A1_TEST_ON || %d ||", &temp) == 1) {
            err  = false;
            if(temp == 0) {
                 m_ledA1_On = false;
                 printf("m_ledA1On = false\n");
            }else{
                 m_ledA1_On = true;
                 printf("m_ledA1On = true\n");
            }
        }

        if(sscanf(searchString, "LED_A2_TEST_ON || %d ||", &temp) == 1) {
            err  = false;
            if(temp == 0) {
                 m_ledA2_On = false;
                 printf("m_ledA2_On = false\n");
            }else{
                 m_ledA2_On = true;
                 printf("m_ledA1_On = true\n");
            }
        }

        if(sscanf(searchString, "LED_B1_TEST_ON || %d ||", &temp) == 1) {
            err  = false;
            if(temp == 0) {
                 m_ledB1_On = false;
                 printf("m_ledB1On = false\n");
            }else{
                 m_ledB1_On = true;
                 printf("m_ledB1On = true\n");
            }
        }

        if(sscanf(searchString, "LED_B2_TEST_ON || %d ||", &temp) == 1) {
            err  = false;
            if(temp == 0) {
                 m_ledB2_On = false;
                 printf("m_ledB2_On = false\n");
            }else{
                 m_ledB2_On = true;
                 printf("m_ledB1_On = true\n");
            }
        }

        if(err){
            printf("The following line in the cfg file is either in a wrong format or not included in Settings Range! Check your cfg file = %s!\n",m_slowControlCfgFile.c_str());
            std::cout<< searchString<<std::endl;
            assert(false);
            return false;
        }
    }
    printf("---------------End of VATA64V2Configuration::loadBoardSettings  -- uplink %d /////-----------------\n",GetUplinkId());
    return !err ;

}


bool VATA64V2::IsLedA1On(){
     return m_configuration->IsLedA1_On();
}

bool VATA64V2::IsLedA2On(){
     return m_configuration->IsLedA2_On();
}

bool VATA64V2::IsLedB1On(){
     return m_configuration->IsLedB1_On();
}

bool VATA64V2::IsLedB2On(){
     return m_configuration->IsLedB2_On();
}

unsigned short VATA64V2::GetLedA_DAC(){
    return m_configuration->GetLedA_DAC();
}

unsigned short VATA64V2::GetLedB_DAC(){
    return m_configuration->GetLedB_DAC();
}


void VATA64V2::DecreaseLedIntense(unsigned short stepDAC){
    unsigned short currDACvalue = 0;
    if(IsLedA1On() || IsLedA2On()){   //change channelA DAC of AD5322
         currDACvalue = GetLedA_DAC();
         //printf("currDACvalue = %d\n",currDACvalue);

         if((currDACvalue-stepDAC)>0){
              setAD5322ChanA(currDACvalue-stepDAC);
         }else{
               std::cerr<<"uplinkID = "<< uplinkId() <<" currDACvalue for DAC channelA is "<<currDACvalue<<" ,if decreased by stepDAC = "<<stepDAC <<" the value will be below zero! "<<std::endl;
               std::cerr<<"So, we will keep the currDACvalue , not decrease it any more!"<<std::endl;
               assert(false);
         }
    }
    if(IsLedB1On() || IsLedB2On()){
        //change channelB DAC of AD5322
        currDACvalue = GetLedB_DAC();
        //printf("currDACvalue = %d\n",currDACvalue);
        //getchar();
        if((currDACvalue-stepDAC)>0){
             setAD5322ChanB(currDACvalue-stepDAC);
        }else{
             std::cerr<<"uplinkID = "<< uplinkId()<<"currDACvalue for DAC channelB is "<<currDACvalue<<" ,if decreased by stepDAC = "<<stepDAC <<" the value will be below zero! "<<std::endl;
             std::cerr<<"So, we will keep the currDACvalue , not decrease it any more!"<<std::endl;
             assert(false);
        }
    }
}

void VATA64V2::IncreaseLedIntense(unsigned short stepDAC){
    unsigned short currDACvalue = 0;
    if(IsLedA1On() || IsLedA2On()){   //change channelA DAC of AD5322
        //change channelA DAC of AD5322
         currDACvalue = GetLedA_DAC();
         //printf("currDACvalue = %d\n",currDACvalue);

         if((currDACvalue+stepDAC)<4096){
              setAD5322ChanA(currDACvalue+stepDAC);
         }else{
              std::cerr<<"uplinkID = "<< uplinkId() <<" currDACvalue for DAC channelA is "<<currDACvalue<<" ,if increase by stepDAC = "<<stepDAC <<" the value will be more than 4096! "<<std::endl;
              std::cerr<<"So, we will keep the currDACvalue , not increase it any more!"<<std::endl;
              assert(false);
         }
    }

    if(IsLedB1On() ||IsLedB2On()){
         //change channelB DAC of AD5322
         currDACvalue = GetLedB_DAC();
         //printf("currDACvalue = %d\n",currDACvalue);
         //getchar();
         if((currDACvalue+stepDAC)<4096){
              setAD5322ChanB(currDACvalue+stepDAC);
         }else{
              std::cerr<<"uplinkID = "<< uplinkId()<<"currDACvalue for DAC channelB is "<<currDACvalue<<" ,if increase by stepDAC = "<<stepDAC <<" the value will be more than 4096! "<<std::endl;
              std::cerr<<"So, we will keep the currDACvalue , not increase it any more!"<<std::endl;
              assert(false);
         }
    }

}

void VATA64V2::DumpOctInBin_8b(uint8_t value){
     for(uint8_t i=0;i<8;++i){
           fprintf(stdout,"%d",(value>>(7-i))&0x01);
           if(i==7) fprintf(stdout,"\n");
     }
}

void VATA64V2::DumpOctInBin_16b(uint16_t value){
     for(uint8_t i=0;i<16;++i){
          fprintf(stdout,"%d",(value>>(15-i))&0x01);
          if(i==15) fprintf(stdout,"\n");
     }
}



void VATA64V2::SetAllLedOn(bool on){
    if(on){
        ledA1On();
        ledA2On();
        ledB1On();
        ledB2On();
    }else{
        ledA1Off();
        ledA2Off();
        ledB1Off();
        ledB2Off();
    }
}
