#include "VATA64.h"
#include "Settings.h"

#include <assert.h>
#include <string>
#include <cstring>
#include <limits.h>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <sstream>
#include <sys/stat.h> 
 


VATA64Configuration::VATA64Configuration(int slotNo,int uplinkId)
  : FrontEndBoardConfiguration(VATA64Type, slotNo, uplinkId)
  , m_trigSelect(-1)
  , m_slowControlCfgFile("")
{
    std::cerr
        << "VATA64Configuration::VATA64Configuration() called."
        <<  std::endl;
}

VATA64Configuration::~VATA64Configuration()
{}

void VATA64Configuration::dump(std::ostream& stream) const
{
    std::cerr
        << "VATA64Configuration::dump(std::ostream& stream) called."
        <<  std::endl;
    stream
        << "VATA64:trigSelect || " << m_trigSelect << " || " << std::endl
        << "VATA64:pathToSlowControlCfgFile || " << m_slowControlCfgFile << std::endl;
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadFromString
//Function:     extract useful infomation for spirocA configuration from the usbBoard config file 
//Variables:    searchSting(const char*):one line read from usbBoard config file          
//Returns:      (bool)
//              true = the given line is for spirocA configuration, read & store it successfully
//              false= the given line does't belong to spirocA configuration
//-------------------------------------------------------------------------------------------------------------------//
bool VATA64Configuration::loadFromString(const char* searchString)
{
  //char cString[100];
      char *cString = new char[strlen(searchString) + 1];
      // bool getline = false;  
      bool getline = true;   // NOT finished: after add new parameters in config file, set this initialized value to false!!!  
      std::cerr
        << "VATA64Configuration::loadFromString(const char*) called."
        <<  std::endl;     
      if(sscanf(searchString, "VATA64:trigSelect || %d ||", &m_trigSelect)==1)  getline =true;
        
      if(sscanf(searchString, "VATA64:pathToSlowControlCfgFile || %[^\n]", cString) == 1) {
            m_slowControlCfgFile = cString;
            getline = true;
      }
      delete [] cString;
     
    return getline;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadConfig
//Function:     read VATA64 slow control config file, store all the settings to struct VATA64_CFG_TYPE 
//Variables:    filePath(string): path to the slow contro config file of VATA64            
//Returns:      (bool) return error value of this process.
//              true = error exist!
//              false= execute successfully!
//-------------------------------------------------------------------------------------------------------------------//  
bool VATA64Configuration::loadConfig(const std::string& filePath) {  

    std::stringstream stream;
    stream << filePath ;             //get config file name
    std::cerr << "Information: Loading VATA64 configuration from file " << stream.str() << std::endl;
    std::ifstream fileStream(stream.str().c_str());      // open config file 
    // "ifstream" provides an interface to read data from files as input streams

    bool err= false;
    if (!fileStream){
      std::cerr << "Failure: could not open file: \"" << stream.str()<< "\"." << std::endl;
      std::cerr << "Please check if the path is right or the frontend board configure file exists!" << std::endl;
           assert(false);
     }      

    std::cerr << "Choose VATA64, reading Config file..."<<std::endl;
    err = loadFromStream(fileStream);                           // load config file, store all the config settings in struct SPIROC_A_CFG_TYPE ( defined in TypeDefs.h )

    return(err);
}



//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadFromStream
//Function:     extract config values from stream, and store them in m_cfgVATA64 ( struct VATA64_CFG_TYPE ( defined in TypeDefs.h ))
//              m_cfgSpirocA is a private variable defined in Class VATA64Configuration
//Variables:    stream(istream): path to the slow control config file of VATA64    
//                              (istream objects are stream objects used to read and interpret input from sequences of characters)      
//Returns:      (bool) return error value of this process.
//              true = error exist!
//              false= execute successfully!
//-------------------------------------------------------------------------------------------------------------------//
bool VATA64Configuration::loadFromStream(std::istream& stream) {
    unsigned int err = 1;
    unsigned int line = 1;
    std::string s;
    while (true) {
        std::getline(stream, s);
        if (stream.eof())
            break;
        const char* searchString = s.c_str();
        err = 1;      // During each loop,err will be set back to 0 ,as long as one sentence in correct format has been read.Otherwise it will stay in 1

        if (s.find("#") == 0) {
            if (s.find("# End of the config file") == 0 ){
                err = 0 ;
                break;
            }  

            err = 0 ;
            continue; // Skip commented lines
        }

        if(sscanf(searchString, "DAC_on || %d ||",&m_cfgVATA64.DAC_on )==1){
            line++;
            err = 0;
        }

        if(sscanf(searchString, "Lg || %d ||",&m_cfgVATA64.Lg )==1){
            line++;
            err = 0;
        }

        if(sscanf(searchString, "Rfp_bp || %d ||",&m_cfgVATA64.Rfp_bp )==1){
            line++;
            err = 0;
        }

        if(sscanf(searchString, "Rfs0_b || %d ||",&m_cfgVATA64.Rfs0_b )==1){
            line++;
            err = 0;
        }

        if(sscanf(searchString, "Rfs1 || %d ||",&m_cfgVATA64.Rfs1 )==1){
            line++;
            err = 0;
        }

        if(sscanf(searchString, "Rfs2_b || %d ||",&m_cfgVATA64.Rfs2_b )==1){
            line++;
            err = 0;
        }

        if(sscanf(searchString, "Lgs || %d ||",&m_cfgVATA64.Lgs )==1){
            line++;
            err = 0;
        }

        if(sscanf(searchString, "Ssc0_b || %d ||",&m_cfgVATA64.Ssc0_b )==1){
            line++;
            err = 0;
        }

        if(sscanf(searchString, "Ssc1 || %d ||",&m_cfgVATA64.Ssc1 )==1){
            line++;
            err = 0;
        }

        if(sscanf(searchString, "Ssc2 || %d ||",&m_cfgVATA64.Ssc2)==1){
            line++;
            err = 0;
        }

        if(sscanf(searchString, "Tp50_dc || %d ||",&m_cfgVATA64.Tp50_dc)==1){
            line++;
            err = 0;
        }

       if(sscanf(searchString, "Bypass || %d ||",&m_cfgVATA64.Bypass)==1){
            line++;
            err = 0;
        }

       if(sscanf(searchString, "Sel || %d ||",&m_cfgVATA64.Sel)==1){
            line++;
            err = 0;
       }

       if(sscanf(searchString, "Shabi_hp1b || %d ||",&m_cfgVATA64.Shabi_hp1b)==1){
            line++;
            err = 0;
       }

       if(sscanf(searchString, "Shabi_hp2 || %d ||",&m_cfgVATA64.Shabi_hp2)==1){
            line++;
            err = 0;
       }

       if(sscanf(searchString, "Shabi_hp3b || %d ||",&m_cfgVATA64.Shabi_hp3b)==1){
            line++;
            err = 0;
       }

       if(sscanf(searchString, "SHsense_en || %d ||",&m_cfgVATA64.SHsense_en)==1){
            line++;
            err = 0;
       }

       if(sscanf(searchString, "SHgen_en || %d ||",&m_cfgVATA64.SHgen_en)==1){
            line++;
            err = 0;
       }

       if(sscanf(searchString, "Buf_sel || %d ||",&m_cfgVATA64.Buf_sel)==1){
            line++;
            err = 0;
       }

       if(sscanf(searchString, "Test_on || %d ||",&m_cfgVATA64.Test_on)==1){
            line++;
            err = 0;
       }

       if(sscanf(searchString, "Vfp_en || %d ||",&m_cfgVATA64.Vfp_en)==1){
            line++;
            err = 0;
       }

       if(sscanf(searchString, "Vfsfclamp_en || %d ||",&m_cfgVATA64.Vfsfclamp_en)==1){
            line++;
            err = 0;
       }

       if(sscanf(searchString, "reserved || %d ||",&m_cfgVATA64.reserved)==1){
            line++;
            err = 0;
       }


      if(sscanf(searchString, "Disable_register   || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d ||",
                    &m_cfgVATA64.Disable_register[0],
                    &m_cfgVATA64.Disable_register[1],
                    &m_cfgVATA64.Disable_register[2],
                    &m_cfgVATA64.Disable_register[3],
                    &m_cfgVATA64.Disable_register[4],
                    &m_cfgVATA64.Disable_register[5],
                    &m_cfgVATA64.Disable_register[6],
                    &m_cfgVATA64.Disable_register[7],
                    &m_cfgVATA64.Disable_register[8],
                    &m_cfgVATA64.Disable_register[9],
                    &m_cfgVATA64.Disable_register[10],
                    &m_cfgVATA64.Disable_register[11],
                    &m_cfgVATA64.Disable_register[12],
                    &m_cfgVATA64.Disable_register[13],
                    &m_cfgVATA64.Disable_register[14],
                    &m_cfgVATA64.Disable_register[15],
                    &m_cfgVATA64.Disable_register[16],
                    &m_cfgVATA64.Disable_register[17],
                    &m_cfgVATA64.Disable_register[18],
                    &m_cfgVATA64.Disable_register[19],
                    &m_cfgVATA64.Disable_register[20],
                    &m_cfgVATA64.Disable_register[21],
                    &m_cfgVATA64.Disable_register[22],
                    &m_cfgVATA64.Disable_register[23],
                    &m_cfgVATA64.Disable_register[24],
                    &m_cfgVATA64.Disable_register[25],
                    &m_cfgVATA64.Disable_register[26],
                    &m_cfgVATA64.Disable_register[27],
                    &m_cfgVATA64.Disable_register[28],
                    &m_cfgVATA64.Disable_register[29],
                    &m_cfgVATA64.Disable_register[30],
		    &m_cfgVATA64.Disable_register[31],
                    &m_cfgVATA64.Disable_register[32],
                    &m_cfgVATA64.Disable_register[33],
                    &m_cfgVATA64.Disable_register[34],
                    &m_cfgVATA64.Disable_register[35],
                    &m_cfgVATA64.Disable_register[36],
                    &m_cfgVATA64.Disable_register[37],
                    &m_cfgVATA64.Disable_register[38],
                    &m_cfgVATA64.Disable_register[39],
                    &m_cfgVATA64.Disable_register[40],
                    &m_cfgVATA64.Disable_register[41],
                    &m_cfgVATA64.Disable_register[42],
                    &m_cfgVATA64.Disable_register[43],
                    &m_cfgVATA64.Disable_register[44],
                    &m_cfgVATA64.Disable_register[45],
                    &m_cfgVATA64.Disable_register[46],
                    &m_cfgVATA64.Disable_register[47],
                    &m_cfgVATA64.Disable_register[48],
                    &m_cfgVATA64.Disable_register[49],
                    &m_cfgVATA64.Disable_register[50],
                    &m_cfgVATA64.Disable_register[51],
                    &m_cfgVATA64.Disable_register[52],
                    &m_cfgVATA64.Disable_register[53],
                    &m_cfgVATA64.Disable_register[54],
                    &m_cfgVATA64.Disable_register[55],
                    &m_cfgVATA64.Disable_register[56],
                    &m_cfgVATA64.Disable_register[57],
                    &m_cfgVATA64.Disable_register[58],
                    &m_cfgVATA64.Disable_register[59],
                    &m_cfgVATA64.Disable_register[60],
                    &m_cfgVATA64.Disable_register[61],
                    &m_cfgVATA64.Disable_register[62],
                    &m_cfgVATA64.Disable_register[63])==64){
                        line++;
                        err = 0;
                    }

       if(sscanf(searchString, "Disable_bit_tst_channel || %d ||",&m_cfgVATA64.Disable_bit_tst_channel)==1){
            line++;
            err = 0;
       }

      if(sscanf(searchString, "Threshold_alignment_DAC  || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d ||",
                    &m_cfgVATA64.Threshold_alignment_DAC[0],
                    &m_cfgVATA64.Threshold_alignment_DAC[1],
                    &m_cfgVATA64.Threshold_alignment_DAC[2],
                    &m_cfgVATA64.Threshold_alignment_DAC[3],
                    &m_cfgVATA64.Threshold_alignment_DAC[4],
                    &m_cfgVATA64.Threshold_alignment_DAC[5],
                    &m_cfgVATA64.Threshold_alignment_DAC[6],
                    &m_cfgVATA64.Threshold_alignment_DAC[7],
                    &m_cfgVATA64.Threshold_alignment_DAC[8],
                    &m_cfgVATA64.Threshold_alignment_DAC[9],
                    &m_cfgVATA64.Threshold_alignment_DAC[10],
                    &m_cfgVATA64.Threshold_alignment_DAC[11],
                    &m_cfgVATA64.Threshold_alignment_DAC[12],
                    &m_cfgVATA64.Threshold_alignment_DAC[13],
                    &m_cfgVATA64.Threshold_alignment_DAC[14],
                    &m_cfgVATA64.Threshold_alignment_DAC[15],
                    &m_cfgVATA64.Threshold_alignment_DAC[16],
                    &m_cfgVATA64.Threshold_alignment_DAC[17],
                    &m_cfgVATA64.Threshold_alignment_DAC[18],
                    &m_cfgVATA64.Threshold_alignment_DAC[19],
                    &m_cfgVATA64.Threshold_alignment_DAC[20],
                    &m_cfgVATA64.Threshold_alignment_DAC[21],
                    &m_cfgVATA64.Threshold_alignment_DAC[22],
                    &m_cfgVATA64.Threshold_alignment_DAC[23],
                    &m_cfgVATA64.Threshold_alignment_DAC[24],
                    &m_cfgVATA64.Threshold_alignment_DAC[25],
                    &m_cfgVATA64.Threshold_alignment_DAC[26],
                    &m_cfgVATA64.Threshold_alignment_DAC[27],
                    &m_cfgVATA64.Threshold_alignment_DAC[28],
                    &m_cfgVATA64.Threshold_alignment_DAC[29],
                    &m_cfgVATA64.Threshold_alignment_DAC[30],
		    &m_cfgVATA64.Threshold_alignment_DAC[31],
                    &m_cfgVATA64.Threshold_alignment_DAC[32],
                    &m_cfgVATA64.Threshold_alignment_DAC[33],
                    &m_cfgVATA64.Threshold_alignment_DAC[34],
                    &m_cfgVATA64.Threshold_alignment_DAC[35],
                    &m_cfgVATA64.Threshold_alignment_DAC[36],
                    &m_cfgVATA64.Threshold_alignment_DAC[37],
                    &m_cfgVATA64.Threshold_alignment_DAC[38],
                    &m_cfgVATA64.Threshold_alignment_DAC[39],
                    &m_cfgVATA64.Threshold_alignment_DAC[40],
                    &m_cfgVATA64.Threshold_alignment_DAC[41],
                    &m_cfgVATA64.Threshold_alignment_DAC[42],
                    &m_cfgVATA64.Threshold_alignment_DAC[43],
                    &m_cfgVATA64.Threshold_alignment_DAC[44],
                    &m_cfgVATA64.Threshold_alignment_DAC[45],
                    &m_cfgVATA64.Threshold_alignment_DAC[46],
                    &m_cfgVATA64.Threshold_alignment_DAC[47],
                    &m_cfgVATA64.Threshold_alignment_DAC[48],
                    &m_cfgVATA64.Threshold_alignment_DAC[49],
                    &m_cfgVATA64.Threshold_alignment_DAC[50],
                    &m_cfgVATA64.Threshold_alignment_DAC[51],
                    &m_cfgVATA64.Threshold_alignment_DAC[52],
                    &m_cfgVATA64.Threshold_alignment_DAC[53],
                    &m_cfgVATA64.Threshold_alignment_DAC[54],
                    &m_cfgVATA64.Threshold_alignment_DAC[55],
                    &m_cfgVATA64.Threshold_alignment_DAC[56],
                    &m_cfgVATA64.Threshold_alignment_DAC[57],
                    &m_cfgVATA64.Threshold_alignment_DAC[58],
                    &m_cfgVATA64.Threshold_alignment_DAC[59],
                    &m_cfgVATA64.Threshold_alignment_DAC[60],
                    &m_cfgVATA64.Threshold_alignment_DAC[61],
                    &m_cfgVATA64.Threshold_alignment_DAC[62],
                    &m_cfgVATA64.Threshold_alignment_DAC[63])==64){
                        line++;
                        err = 0;
                    }

       if(sscanf(searchString, "Threshold_alignment_DAC_tst_channel || %d ||",&m_cfgVATA64.Threshold_alignment_DAC_tst_channel)==1){
            line++;
            err = 0;
       }

       if(sscanf(searchString, "Preamplifier_input_potential_DAC  || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d ||",
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[0],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[1],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[2],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[3],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[4],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[5],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[6],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[7],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[8],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[9],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[10],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[11],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[12],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[13],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[14],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[15],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[16],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[17],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[18],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[19],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[20],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[21],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[22],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[23],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[24],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[25],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[26],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[27],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[28],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[29],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[30],
		    &m_cfgVATA64.Preamplifier_input_potential_DAC[31],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[32],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[33],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[34],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[35],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[36],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[37],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[38],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[39],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[40],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[41],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[42],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[43],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[44],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[45],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[46],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[47],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[48],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[49],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[50],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[51],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[52],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[53],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[54],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[55],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[56],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[57],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[58],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[59],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[60],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[61],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[62],
                    &m_cfgVATA64.Preamplifier_input_potential_DAC[63])==64){
                        line++;
                        err = 0;
                    }

       if(sscanf(searchString, "Preamplifier_input_potential_DAC_tst_channel || %d ||",&m_cfgVATA64.Preamplifier_input_potential_DAC_tst_channel)==1){
            line++;
            err = 0;
       }

       if(sscanf(searchString, "Holdbi_Bias_DAC || %d ||",&m_cfgVATA64.Holdbi_Bias_DAC)==1){
            line++;
            err = 0;
       }

       if (err) {
            std:: cerr << " Error@ VATA64 config file:  Format incorrect in LINE= "<<line<<"! (#comments are not included in LINE NUM)Please check!" << std:: endl;
            assert(false);
            break;
        }  
    }

    if (err) {
        std:: cerr << " Error: Front End Configuration failed!" << std:: endl;
        assert(false);
        return false;
    }    
    return true;
}



//--------------------------------------------------------------------------------------------------------------------//
//Name:         convertCfgToVector
//Function:     convert m_cfgVATA64 ( struct VATA64_CFG_TYPE ( defined in TypeDefs.h ))into Vector.
//              m_cfgVATA64 is a private variable defined in Class VATA64Configuration,which stores all the settings reading from the slow control config file
//Variables:    temp(unsigned char[]): point to an array to place the conversion result. Each member of this array corresponds to one bit of the slow control register
//              i.e.temp[index] corresponds to BIT(index) of the slow control register      
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------// 
void  VATA64Configuration::convertCfgToVector(unsigned char temp[873]) { 

    int  bit_total =0;
    int  bit,ch;

    temp[bit_total++] = (m_cfgVATA64.DAC_on                          )&0x01;  
    temp[bit_total++] = (m_cfgVATA64.Lg                              )&0x01;  
    temp[bit_total++] = (m_cfgVATA64.Rfp_bp                          )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Rfs0_b                          )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Rfs1                            )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Rfs2_b                          )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Lgs                             )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Ssc0_b                          )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Ssc1                            )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Ssc2                            )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Tp50_dc                         )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Bypass                          )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Sel                             )&0x01; 

    temp[bit_total++] = (m_cfgVATA64.Shabi_hp1b                      )&0x01;  
    temp[bit_total++] = (m_cfgVATA64.Shabi_hp2                       )&0x01;  
    temp[bit_total++] = (m_cfgVATA64.Shabi_hp3b                     )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.SHsense_en                      )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.SHgen_en                        )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Buf_sel                         )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Test_on                         )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Vfp_en                          )&0x01; 
    temp[bit_total++] = (m_cfgVATA64.Vfsfclamp_en                    )&0x01; 

    for(bit=0;bit<2;bit++) //from LSB to MSB
        temp[bit_total++] = (m_cfgVATA64.reserved >>bit )&0x01;
 
    for(ch=0;ch<64;ch++)  //from ch0 to ch63 
        temp[bit_total++] = (m_cfgVATA64.Disable_register[ch]        )&0x01;

    temp[bit_total++] = (m_cfgVATA64.Disable_bit_tst_channel         )&0x01;  

    for(ch=0;ch<64;ch++) {  //from ch0 to ch63 
        for(bit=0;bit<4;bit++)//from LSB to MSB
            temp[bit_total++] = (m_cfgVATA64.Threshold_alignment_DAC[ch]>>bit  )&0x01;
    } 

    for(bit=0;bit<4;bit++) //from LSB to MSB
        temp[bit_total++] = (m_cfgVATA64.Threshold_alignment_DAC_tst_channel >>bit )&0x01;

    for(ch=0;ch<64;ch++) {  //from ch0 to ch63 
      // for(bit=0;bit<8;bit++)//from LSB to MSB-----------deleted by lht 16:34@4.27.2011
      for(bit=7;bit>=0;bit--)                 //-----------added by lht 16:36@4.27.2011
            temp[bit_total++] = (m_cfgVATA64.Preamplifier_input_potential_DAC[ch]>>bit  )&0x01;
    } 

    //for(bit=0;bit<8;bit++) //from LSB to MSB-------------deleted by lht 16:39@4.27.2011
    for(bit=7;bit>=0;bit--)                   //-----------added by lht 16:40@4.27.2011
        temp[bit_total++] = (m_cfgVATA64.Preamplifier_input_potential_DAC_tst_channel >>bit )&0x01;

    for(bit=0;bit<4;bit++) //from LSB to MSB
        temp[bit_total++] = (m_cfgVATA64.Holdbi_Bias_DAC >>bit        )&0x01;
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         writeToStream
//Function:     dump m_cfgVATA64 ( struct VATA64_CFG_TYPE ( defined in TypeDefs.h ))
//              m_cfgVATA64 is a private variable defined in Class VATA64Configuration,which stores all the settings reading from the slow control config file
//Variables:    stream(ostream):  output stream
//                                (ostream objects are stream objects used to write and format output as sequences of characters      
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------// 
void VATA64Configuration::writeToStream(std::ostream& stream) const
{
    stream
        <<" DAC_on:    "<<m_cfgVATA64.DAC_on << std::endl
        <<" Lg    :    "<<m_cfgVATA64.Lg     << std::endl 
        <<" Rfp_bp:    "<<m_cfgVATA64.Rfp_bp << std::endl
        <<" Rfs0_b:    "<<m_cfgVATA64.Rfs0_b << std::endl
        <<" Rfs1  :    "<<m_cfgVATA64.Rfs1   << std::endl
        <<" Rfs2_b:    "<<m_cfgVATA64.Rfs2_b << std::endl
        <<" Lgs   :    "<<m_cfgVATA64.Lgs    << std::endl
        <<" Ssc0_b:    "<<m_cfgVATA64.Ssc0_b << std::endl
        <<" Ssc1  :    "<<m_cfgVATA64.Ssc1   << std::endl
        <<" Ssc2  :    "<<m_cfgVATA64.Ssc2   << std::endl
        <<" Tp50_dc:   "<<m_cfgVATA64.Tp50_dc<< std::endl
        <<" Bypass:    "<<m_cfgVATA64.Bypass << std::endl

        <<" Sel   :        "<<m_cfgVATA64.Sel           << std::endl
        <<" Shabi_hp1b:    "<<m_cfgVATA64.Shabi_hp1b    << std::endl 
        <<" Shabi_hp2 :    "<<m_cfgVATA64.Shabi_hp2     << std::endl
        <<" Shaibi_hp3b:   "<<m_cfgVATA64.Shabi_hp3b   << std::endl
        <<" SHsense_en :   "<<m_cfgVATA64.SHsense_en    << std::endl
        <<" SHgen_en   :   "<<m_cfgVATA64.SHgen_en      << std::endl
        <<" Buf_sel    :   "<<m_cfgVATA64.Buf_sel       << std::endl
        <<" Test_on    :   "<<m_cfgVATA64.Test_on       << std::endl
        <<" Vfp_en     :   "<<m_cfgVATA64.Vfp_en        << std::endl
        <<" Vfsfclamp_en : "<<m_cfgVATA64.Vfsfclamp_en  << std::endl
        <<" reserved     : " <<m_cfgVATA64.reserved     << std::endl;
    
    stream
        <<" Disable_register: ";
    for(unsigned int j = 0; j<64 ; j++)
        stream<<m_cfgVATA64.Disable_register[j] <<"  ";
    stream
        <<std::endl;

    stream
        <<" Disable_bit_tst_channel: "<<m_cfgVATA64.Disable_bit_tst_channel <<std::endl;

    stream
        <<" Threshold_alignment_DAC: ";
    for(unsigned int j = 0; j<64 ; j++)
        stream<<m_cfgVATA64.Threshold_alignment_DAC[j] <<"  ";
    stream
        <<std::endl;

   stream
        <<" Threshold_alignment_DAC_tst_channel: "<<m_cfgVATA64.Threshold_alignment_DAC_tst_channel <<std::endl;

    stream
        <<" Preamplifier_input_potential_DAC: ";
    for(unsigned int j = 0; j<64 ; j++)
        stream<<m_cfgVATA64.Preamplifier_input_potential_DAC[j] <<"  ";
    stream
        <<std::endl;

    stream
        <<" Preamplifier_input_potential_DAC_tst_channel: "<<m_cfgVATA64.Preamplifier_input_potential_DAC_tst_channel <<std::endl
        <<" Holdbi_Bias_DAC: "<<m_cfgVATA64.Holdbi_Bias_DAC <<std::endl;
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         convertVectorTo16bitsArray
//Function:     convert config vector into 16bits array, which will be written into UFM
//Variables:    temp(unsigned char[]): vectors converted from m_cfgSpirocA
//              array(unsigned short[]): point to the array to place the 16bits array written into UFM
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------// 
void  VATA64Configuration::convertVectorTo16bitsArray(int dumpOn, unsigned char temp[873],unsigned short array[])
{
    short               bit;
    short               p;
    unsigned short  pad_short;

    unsigned short  temp_debug;
    short              index;
    /*=============convert to word====== order config bit 0 first in=======*/
    //    p =0;    
    //    index = 0;
    //    temp_debug = 0;

    //    for(bit=0;bit<(456+8);bit++)
    //    {  
    //      pad_short <<=1; 

    //      if(bit <456)
    //        pad_short += temp[bit];
    //      else
    //        pad_short += 0;  // fill the last byte with 0

    //       if((bit%16)==15)  
    //       {   array[index] = pad_short;

    //     if(dumpOn==1){
    //           std::cerr<<"Info@16bitsArray:array["<<index<<"]=";
    //           for(unsigned int i = 0; i<16; i++)
    //           std::cerr<<((array[index]>>(15-i))&0x0001)<<" ";
    //           std::cerr<<std::endl;
    //     }
    //           index++;
    //       }
    //    }
    /*=============convert to word====== order config bit 0 last in=======*/
    p =0;    
    index = 0;
    temp_debug = 0;

    for(bit=872+7;bit>=0;bit--)
    {  
        pad_short <<=1; 

        if(bit >=7)
            pad_short += temp[bit-7];
        else
            pad_short += 0;  // fill the last byte with 0

        if((bit%16)==0)  
        {   array[index] = pad_short;

            if(dumpOn==1){
                std::cerr<<"Info@16bitsArray:array["<<index<<"]=";
                for(unsigned int i = 0; i<16; i++)
                    std::cerr<<((array[index]>>(15-i))&0x0001)<<" ";
                std::cerr<<std::endl;
            }
            index++;
        }
    }
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         setPreamplifierInputPortentialDAC
//Function:     set Preamplifier input potential DAC value without refering to the config file, then update 16bits config arrays.
//Variables:    Value (int): DAC value to be set
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//    
void VATA64Configuration::setPreamplifierInputPortentialDAC(int Value,unsigned short configArray[]){
  unsigned char temp[873];
  unsigned int dumpOn = 0;
  for(unsigned int channel=0; channel<64;channel++){
        m_cfgVATA64.Preamplifier_input_potential_DAC[channel] = Value;
   // std::cerr<<"DAC value written"<<m_cfgVATA64.Preamplifier_input_potential_DAC[channel];
  }
  convertCfgToVector(temp);
  convertVectorTo16bitsArray(dumpOn, temp, configArray);
}



// //----------------------------------------------------------------------------//
// //Name:          selectForRead
// //Function:      provide info that which kind of signals are used to access "READ interface" of Spiroc to other Classes which need this value.
// //               Because m_select_nSPI_uplink is a private variable for Class VATA64Configuration, which only can be accessed by Class VATA64Configuration itself
// //               Parameter with the identical name in Firmware corresponds to bit6 of Gene_ctrl_reg0 (Firmwares for SpirocA and SpirocII use the same bit, 01.06.2010)
// //               0= signals from SPI (set by C code);    1= signals from uplink(H,HB,S,SB)
// //Parameters:    None
// //Returns:       (int) return 0= signals from SPI (set by C code)
// //                            1= signals from uplink(H,HB,S,SB)
// //---------------------------------------------------------------------------//
// int VATA64Configuration::select_nSPI_uplink() const
// {
//     return m_select_nSPI_uplink;
// }

// //----------------------------------------------------------------------------//
// //Name:          setSelect_nSPI_uplink
// //Function:      select signals used to access "READ interface" of Spiroc without refering to the config file
// //Parameters:    select_nSPI_uplink(int):  Only 0 or 1 could be used, otherwise the program will break off with an error report!
// //               0= signals from SPI (set by C code),  1= signals from uplink(H,HB,S,SB)
// //Returns:       None
// //---------------------------------------------------------------------------//
// void VATA64Configuration::setSelect_nSPI_uplink(int select_nSPI_uplink)
// {
//     m_select_nSPI_uplink = select_nSPI_uplink;
// }


//----------------------------------------------------------------------------//
//Name:          trigSelect
//Function:      provide trigger type of spiroc to other Classes which need this value.
//               Because m_trigSelect is a private variable for Class VATA64Configuration, which only can be accessed by Class VATA64Configuration itself
//               For VATA64,it corresponds to bit7(trigSelect) of Gene_ctrl_reg0 (08.07.2010)
//               In VATA64 firmware, the default value for Gene_ctrl_reg0(7) has been set to '0'.
//Parameters:    None
//Returns:       (int) return trigger type value
//               0=UL_HOLD(use H,HB,S,SB as trigger signal);  1=NIM_trigger;   
//---------------------------------------------------------------------------//
int VATA64Configuration::trigSelect() const
{
    return m_trigSelect;
}

//----------------------------------------------------------------------------//
//Name:          setTrigSelect
//Function:      set trigger type of VATA64 without refering to the config file
//Parameters:    trigSelect(int): has different meanings & ranges for spirocII ,spirocA and VATA64 
//               Only 0~1 could be used for VATA64, otherwise the program will break off with an error report!
//               0=UL_HOLD(use H,HB,S,SB as trigger signal);  1=NIM_trigger;   
//Returns:       None
//---------------------------------------------------------------------------//
void VATA64Configuration::setTrigSelect(int trigSelect)
{
    m_trigSelect = trigSelect;
}

//----------------------------------------------------------------------------//
//Name:          setSlowControlCfgFile
//Function:      set a new path to the slow control config file without refering to the UsbBoard config file
//Parameters:    fileName(string) : the format is like "/home/pebs_ecal/usbBoard/readout/readoutSettings/VATA64_EPFL.cfg"
//Returns:       None
//---------------------------------------------------------------------------//
void VATA64Configuration::setSlowControlCfgFile(const std::string& fileName)
{
    m_slowControlCfgFile = fileName;
}

//----------------------------------------------------------------------------//
//Name:          firmwareFileName     
//Function:      provide path to the slow control config file to other Classes which need this value.
//               Because m_slowControlCfgFile is a private variable for Class UsbBoardConfiguration, which only can be accessed by Class UsbBoardConfiguration itself
//Parameters:    None
//Returns:       (string) return path to the slow control config file 
//---------------------------------------------------------------------------//
const std::string& VATA64Configuration::slowControlCfgFile() const
{
    return m_slowControlCfgFile;
}




// ------------------------- VATA64 --------------------------------
VATA64::VATA64(UsbBoard* board, FrontEndBoardType type, int slotNo)
    : FrontEndBoard(board, type, slotNo)
    , m_configuration(0)
    , m_spiToUfmRegisterSwitch(0)
{
}

VATA64::~VATA64()
{}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         configure
//Function:     configure front end chip, if this function has been switched on in the UsbBoard cfg file
//Variables:    None
//Returns:      (bool)
//              true = config successfully
//              false= error exist during configuration
//-------------------------------------------------------------------------------------------------------------------//
bool VATA64::configure(FrontEndBoardConfiguration* configuration)
{
    m_configuration = static_cast<VATA64Configuration*>(configuration);

    std::cerr << "VATA64::configure() called." << std::endl;
    //return true; //TODO
    bool err = false;
#ifndef NO_QUSB

    //VATA64Configuration chipConfig(m_slotNo);
    std::string chipConfigFilePath = m_configuration->slowControlCfgFile();        // get the path to slow control config file
    //std::string chipConfigFilePath = "/home/pebs_ecal/usbBoard/readout/readoutSettings/vata64/VATA64_EPFL.cfg";
    //std::cout<<"!!!Note:vata64 cfg file path=home/pebs_ecal/usbBoard/readout/readoutSettings/vata64/VATA64_EPFL.cfg"<<std::endl;
    //unsigned char temp[872];       //--------deleted by lht 11:22@ 29-4-2011
    unsigned char temp[873];        //--------added by lht 11:22@ 29-4-2011
    unsigned short configArray[100];


     std::cerr << "before loadConfig " << std::endl;
     // while(0)     dumpGeneCtrlReg0();
     //   while(1)     setTrigSelect(1);
     /*
     chipConfig.loadConfig(chipConfigFilePath);
     chipConfig.convertCfgToVector(temp);
     chipConfig.convertVectorTo16bitsArray(0, temp, configArray);
     */
     m_configuration->loadConfig(chipConfigFilePath);
     m_configuration->convertCfgToVector(temp);
     m_configuration->convertVectorTo16bitsArray(0, temp, configArray);

     if (spiConfigVATA64(0, configArray, m_configuration->trigSelect() ))  err = true;

     // enable Amplifiers IC6,IC7,IC8,IC9 on the vata64 board. Actually,the firmware has already set their default value to 1 ( Gene_ctrl_Reg0 bit 9,10,11)
     setNPWDA(1);
     setNPWDB(1);
     setNPWDC(1);
 
           
     // select channel for continuous mode operation
     //////////////////////////////
     // Add only this two lines below to use the continous mode  
     //selectContinuousMode(28);      // select channel 1
     //getchar();
     //selectContinuousMode(47);      
     //getchar();
     ////////////////////////////////
     // guido
     // change dac value and reconfig
     /////////////////////////////////////////// 
     if (0) {
	 for(unsigned int DACValue=0; DACValue<256; DACValue+=16){
           //chipConfig.setPreamplifierInputPortentialDAC(DACValue,configArray);
           m_configuration->setPreamplifierInputPortentialDAC(DACValue,configArray);
           if ( spiConfigVATA64(1, configArray, m_configuration->trigSelect() ))  err = true; 
           std::cerr << "DACValue"<< DACValue << std::endl;
           getchar();
         }
     }
     /////////////////////////////////////////////
        
     // Choose Uplink or SPI control for the read port 
     //////////////////////////
     //setSelect_nSPI_uplink(0);       // use spi
     setSelect_nSPI_uplink(1);       // use uplink
     ////////////////////////

    
#endif
    if (err) {
        std::cout << "Could not configure DAQ" << std::endl;
        assert(false);
        return false;
      } else {
        return true;
    }
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiToUfmOrRegister
//Function:     select QUSB SPI communication between QUSB & UFM  or  QUSB & Register in FPGA on the front end board
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              data(unsigned short):Only two values can be used here  0xC3C3= QUSB & UFM, 0x3C3C= QUSB & Register
//                                   Two macro definition have been defined in FrontEndChipMacroDefinition.h, use it ! ( SPItoUFM = 0xC3C3, SPItoREGISTER = 0x3C3C )
//
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
//void VATA64::spiToUfmOrRegister(unsigned char portNum, unsigned short data,bool force) {
  /*
    if(data != m_spiToUfmRegisterSwitch || force){     // executed only when change spi between Ufm and R/W Register.
        unsigned char buf[2];
        buf[0] = data >> 8;
        buf[1] = data & 0xFF;
        write(portNum, buf, 2);
        m_spiToUfmRegisterSwitch = data;
	if (data==0xC3C3){ 
           printf("set spi to UFM \n");
           //getchar();
        }
	if (data==0x3C3C) { 
           printf("set spi to GeneCtrlReg \n"); 
           //getchar();
        }   
    }
  */
  // Useless in new parallel spi interface
  // By lht, July 20, 13:45. For new parallel spi interface
//}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiRegisterWrite
//Function:     write data to a given register in FOGA on the front end board
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              addr(unsigned short): address of the register to be written, currently for spirocA and spirocII front end board , we only use two register, already defined in FrontEndChipMacroDefinition.h (01.06.2010)
//                                    FIRMWARE_VERSION_REG_ADDR = 0x0000
//                                    GENERAL_CTRL_REG0 = 0x0001      ( detail definition for each bit, refer to FrontEndMacroDefiniton.h )
//              data(unsigned short): data to be written into the given register
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64::spiRegisterWrite(unsigned char portNum, unsigned short addr, unsigned short data) {
  /*
    unsigned char buf[4];  //[0]--SPI cmd, [1], [2]--address, [3], [4]--data
    
    spiToUfmOrRegister(0,0x3C3C);  // make sure that  spi is linked to register R/W

    buf[0] = (addr>>8)&0xFF;
    buf[1] = (addr   )&0xFF;
    buf[2] = (data>>8)&0xFF;
    buf[3] = (data   )&0xFF;
    write(portNum, buf, 4);
  */
    // By lht, July 20, 13:45. For new parallel spi interface
    unsigned char buf[7];   //[0] FE select; [1],[2] device select; [3],[4] address; [5],[6] data
    buf[0] = getSlotNo();
    buf[1] = (SPItoREGISTER >> 8) & 0xFF;
    buf[2] = (SPItoREGISTER     ) & 0xFF;
    buf[3] = (addr >> 8) & 0xFF;
    buf[4] = (addr     ) & 0xFF;
    buf[5] = (data >> 8) & 0xFF;
    buf[6] = (data     ) & 0xFF;
    write(portNum, buf, 7);
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiRegisterRead
//Function:     read data from a given register in FOGA on the front end board
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              addr(unsigned short): address of the register to be written, currently for spirocA and spirocII front end board , we only use two register, already defined in FrontEndChipMacroDefinition.h (01.06.2010)
//                                    FIRMWARE_VERSION_REG_ADDR = 0x0000
//                                    GENERAL_CTRL_REG0 = 0x0001      ( detail definition for each bit, refer to FrontEndMacroDefiniton.h )
//Returns:      (unsigned short): return the data value read back from the given register
//-------------------------------------------------------------------------------------------------------------------//
unsigned short VATA64::spiRegisterRead(unsigned char portNum, unsigned short addr) {
  /*
    unsigned char buf[4];  //[0]--SPI cmd, [1], [2]--address, [3], [4]--data
    unsigned short  readData;

    spiToUfmOrRegister(0,0x3C3C);  // make sure that  spi is linked to register R/W

    buf[0] = ((addr>>8)&0x7F) | 0x80;
    buf[1] = (addr   )&0xFF;
    buf[2] = 0x00;
    buf[3] = 0x00;
    writeRead(portNum, buf, 4);
    readData =  buf[2];
    readData <<= 8;
    readData += buf[3];
  */
    // By lht, July 20, 13:59. For new parallel spi interface
    unsigned char buf[7];   //[0] FE select; [1],[2] device select; [3],[4] address; [5],[6] data
    unsigned short readData;
    buf[0] = getSlotNo();
    buf[1] = (SPItoREGISTER >> 8) & 0xFF;
    buf[2] = (SPItoREGISTER     ) & 0xFF;
    buf[3] = ((addr >> 8) & 0x7F) | 0x80;
    buf[4] = (addr     ) & 0xFF;
    buf[5] = 0x00;
    buf[6] = 0x00;
    writeRead(portNum, buf, 7);
    readData = buf[5];
    readData <<= 8;
    readData += buf[6];

//     std::cerr << "Info@Config_VATA64: readRegister @ "<< addr << "=";
//     for(unsigned int i = 0; i<16; i++)
//         std::cerr << ((readData>>(15-i))&0x0001);
//     std::cerr << std::endl;

     return(readData);
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmStatusRead
//Function:     read the staus register in UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//Returns:      (unsigned char): return current status of the UFM
//Note:         before use function, remember to switch SPI communication to QUSB & UFM first, using function "spiToUfmOrRegister(unsigned char portNum, unsigned short data)"
//-------------------------------------------------------------------------------------------------------------------//
unsigned char VATA64::spiUfmStatusRead(unsigned char portNum) {
  /*
    unsigned char buf[2];  //[0]--SPI cmd

    spiToUfmOrRegister(0,0xC3C3);  // make sure that  spi is linked to register R/W 

    buf[0] = UFM_CMD_RDSR;
    buf[1] = 0x00;
    writeRead(portNum, buf, 2);
    return buf[1];
  */
    // By lht, July 20, 14:09. For new parallel spi interface
    unsigned char buf[5];   //[0] FE select; [1],[2] device select;[3] UFM_CMD
    buf[0] = getSlotNo();
    buf[1] = (SPItoUFM >> 8) & 0xFF;
    buf[2] = (SPItoUFM     ) & 0xFF;
    buf[3] = UFM_CMD_RDSR;
    buf[4] = 0x00;
    writeRead(portNum, buf, 5);
    return buf[4];
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmErase
//Function:     erase the content in UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64::spiUfmErase(unsigned char portNum) {
  /*
    unsigned char buf[2];  //[0]--SPI cmd
    spiToUfmOrRegister(0,0xC3C3);  // make sure that  spi is linked to register R/W 

    buf[0] = UFM_CMD_WREN;
    write(portNum, buf, 1);   //write enable
    sleep(1);
    buf[0] = UFM_CMD_BERASE;
    write(portNum, buf, 1);   //erase
  */
    // By lht, July 20, 14:16. For new parallel spi interface
    unsigned char buf[4];   //[0] FE select; [1],[2] device select;[3] UFM_CMD
    buf[0] = getSlotNo();
    buf[1] = (SPItoUFM >> 8) & 0xFF;
    buf[2] = (SPItoUFM     ) & 0xFF;
    buf[3] = UFM_CMD_WREN;
    write(portNum, buf, 4);
    sleep(1);
    buf[3] = UFM_CMD_BERASE;
    write(portNum, buf, 4);
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmWrite
//Function:     write one word into UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              addr(unsigned short): the address in UFM to be written into
//              data(unsigned short): the data to be written into the UFM at the given address
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64::spiUfmWrite(unsigned char portNum, unsigned short addr, unsigned short data) {
  /*
    unsigned char buf[5];
    spiToUfmOrRegister(0,0xC3C3);  // make sure that  spi is linked to write ufm

    buf[0] = UFM_CMD_WRITE;
    buf[1] = (addr>>8)&0xFF;
    buf[2] = (addr   )&0xFF;
    buf[3] = (data>>8)&0xFF;
    buf[4] = (data   )&0xFF;
    write(portNum, buf, 5);
  */
    // By lht, July 20, 14:25. For new parallel spi interface
    unsigned char buf[8];   //[0] FE select; [1],[2] device select;[3] UFM_CMD,[4], [5] address; [6],[7] data
    buf[0] = getSlotNo();
    buf[1] = (SPItoUFM >> 8) & 0xFF;
    buf[2] = (SPItoUFM     ) & 0xFF;
    buf[3] = UFM_CMD_WRITE;
    buf[4] = (addr >> 8)&0xFF;
    buf[5] = (addr   )&0xFF;
    buf[6] = (data >> 8)&0xFF;
    buf[7] = (data   )&0xFF;
    write(portNum, buf, 8);
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmReadWord
//Function:     read one word from UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              start_addr(unsigned short): the first address in UFM to be read
//Returns:      (unsigned short) return data (16bits) read from UFM
//-------------------------------------------------------------------------------------------------------------------//
unsigned short VATA64::spiUfmReadWord(unsigned char portNum, unsigned short start_addr) {
  /*
    unsigned char buf[6];
    unsigned short data;
    spiToUfmOrRegister(0,0xC3C3);  // make sure that  spi is linked to write ufm 

    buf[0] =  UFM_CMD_READ;
    buf[1] = (start_addr>>8)&0xFF;
    buf[2] = (start_addr   )&0xFF;
    writeRead(portNum, buf, 5);
    data=(buf[3] << 8)+buf[4];
    return data;
  */
    // By lht, July 20, 14:30. For new parallel spi interface
    unsigned char buf[8];   //[0] FE select; [1],[2] device select;[3] UFM_CMD,[4], [5] address; [6],[7] data
    unsigned short data;
    buf[0] = getSlotNo();
    buf[1] = (SPItoUFM >> 8) & 0xFF;
    buf[2] = (SPItoUFM     ) & 0xFF;
    buf[3] = UFM_CMD_READ;
    buf[4] = (start_addr >> 8)&0xFF;
    buf[5] = (start_addr     )&0xFF;
    buf[6] = 0x00;
    buf[7] = 0x00;
    writeRead(portNum, buf, 8);
    data = (buf[6] << 8) + buf[7];
    return data;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmReadBlock
//Function:     read a sequence of words from UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              word_num(unsigned short): the number of words to be read
//              data(unsigned short[]): a pointer to the buffer that store the rceive data
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64::spiUfmReadBlock(unsigned char portNum, unsigned short start_addr, unsigned short word_num, unsigned short data[512]) {
    unsigned char buf[64];
    unsigned short num, j;
    /*
    spiToUfmOrRegister(0,0xC3C3);  // make sure that  spi is linked to write ufm

    for (j=0; j<(word_num/30 + 1); j++)
    {
        if ((word_num-30*j) <= 30)
            num = word_num - 30*j;
        else
            num = 30;
        start_addr += 30*j;
        buf[0] =  UFM_CMD_READ;
        buf[1] = (start_addr>>8)&0xFF;
        buf[2] = (start_addr   )&0xFF;
        writeRead(portNum, buf, num*2+3);
        for(unsigned short i=0; i<num; i++) {
            data[i+j*30]=(buf[2*i+3] << 8)+buf[2*i+4];
            //                      std::cerr << "Info@SPIUfmReadBlock:readUFMfifo    readData[" << i+j*30 << "]=";
            //                         for(unsigned int bit=0; bit<16; bit++)
            //               std::cerr << ((data[i+j*30]>>(15-bit))&0x0001) << " ";
            //             std::cerr << std::endl;
        }
    }
    */
    // By lht, July 20, 14:45. For new parallel spi interface
    for(j=0; j<(word_num/29 + 1); j++)
    {
        if((word_num-29*j) <= 29)
            num = word_num - 29 * j;
        else
            num = 29;
        start_addr += 29 * j;
        buf[0] = getSlotNo();
        buf[1] = (SPItoUFM >> 8) & 0xFF;
        buf[2] = (SPItoUFM     ) & 0xFF;
        buf[3] = UFM_CMD_READ;
        buf[4] = (start_addr >> 8)&0xFF;
        buf[5] = (start_addr     )&0xFF;
        writeRead(portNum, buf, num*2+6);
        for(unsigned short i = 0; i < num; i++)
        {
            data[i + j*29] = (buf[2*i + 6] << 8) + buf[2*i + 7];
        }
    }
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         selectReadChannel
//Function:     using signals send by SPI to access READ interface of spiroc, select a certain channel to be readout
//Variables:    channelIndex(unsigned int): channel index (count from 0,1,2...)
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64::selectReadChannel(unsigned int channelIndex) {
    setSelect_nSPI_uplink(0); // switch to use SPI signals to access spirocA READ register
    setCkbSpi(1);         //initial CLK_READ_SPI

    setDresetSpi(1);      // reset Read Register low active
    setDresetSpi(0);      // remove reset

    setShiftInbSpi(0);        // set '0' to SRIN_READ_SPI

    setCkbSpi(0);        // clock 0
    setCkbSpi(1);        // clock 1
    std::cerr << "select READ Register Channel 0" << std::endl;

    setShiftInbSpi(1);       //  set '1' to SRIN_READ_SPI

    for(unsigned int i=0; i<channelIndex; i++) {
        setCkbSpi(0);      //clock in
        setCkbSpi(1);
        std::cerr << "select READ Register Channel " << (i+1) << std::endl;
    }
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         setCkbSpi
//Function:     using signals send by SPI to access READ interface of spiroc, set CKB_spi to be hign/low
//Variables:    setValue(char) :  0=set CKB_spi to LOW    1=set CKB_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64::setCkbSpi(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);  // read register value
    m_generalCtrlReg0 &= 0xFFFB;                                 // mask the bit
    m_generalCtrlReg0 |= ((setValue & 0x1) << 2);                   // set bit to new setValue
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0); // write register
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         setShiftInbSpi
//Function:     using signals send by SPI to access READ interface of spiroc, set SHIFT_IN_B_spi to be hign/low
//Variables:    setValue(char):  0=set SHIFT_IN_B_spi to LOW    1=set SHIFT_IN_B_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64::setShiftInbSpi(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFF7) | ((setValue & 0x01) << 3);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setDresetSpi
//Function:     using signals send by SPI to access READ interface of spiroc, set Dreset_spi to be hign/low
//Variables:    setValue(char):  0=set Dreset _spi to LOW    1=set Dreset_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64::setDresetSpi(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFEF) | ((setValue & 0x01) << 4);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

// //-------------------------------------------------------------------------------------------------------------------//
// //Name:         setResetbScSpi
// //Function:     using signals send by SPI to access READ interface of spiroc(actually here we access the Slow Control interface,2010.06.14 snow@EPFL), set RESETB_SC_spi to be hign/low 
// //Variables:    setValue(char):  0=set RESETB_SC_spi to LOW    1=set RESETB_SC_spi to HIGH                                          
// //Returns:      None  
// //-------------------------------------------------------------------------------------------------------------------//
// void VATA64::setResetbScSpi(char setValue) {
//         m_generalCtrlReg0 = spiRegisterRead(0,GENERAL_CTRL_REG0_ADDR);
// 	m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xF7FF) | ((setValue & 0x01)<<11);
// 	spiRegisterWrite(0,GENERAL_CTRL_REG0_ADDR,m_generalCtrlReg0);
// } 

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setHoldSpi
//Function:     using signals send by SPI to access READ interface of spiroc, set HOLD_spi to be hign/low
//Variables:    setValue(char):  0=set Hold_spi to LOW    1=set Hold_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64::setHoldSpi(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFDF) | ((setValue & 0x01) << 5);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setSelect_nSPI_uplink
//Function:     select signals to access READ interface of the  FrontEnd chip: 0= spi(set by C code); 1 = uplink(H, HB, S, SB);
//Variables:    setValue(char):  0=use signals from SPI(programed by C code) to access READ register of spiroc
//                               1=use signals from uplink(H, HB, S, SB) to access READ register of spiroc
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64::setSelect_nSPI_uplink(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFBF) | ((setValue & 0x01) << 6);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         SetTrigSelect
//Function:     set bit7 of Reg0(reset_cfg) in spirocA firmware
//Variables:    trigSelect(char):0=set bit7 to '0' =>UL_HOLD(use H, HB, S, SB as trigger signal);
//                               1=set bit7 to '1' =>NIM_trigger;
//Returns:      bool
//              true = successful
//              false= Error!
//-------------------------------------------------------------------------------------------------------------------//
void VATA64::setTrigSelect(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFF7F) | ((setValue & 0x01) << 7);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         seTestpulseSpi
//Function:     set Testpulse to be high/Low, when using SPI to access READ interface of the  FrontEnd chip
//Variables:    setValue(char): 0=set Testpulse_spi to LOW    1=set Testpulse_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64::setTestpulseSpi(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFEFF) | ((setValue & 0x01) << 8);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}


//-------------------------------------------------------------------------------------------------------------------//
//Name:         setNPWDA
//Function:     set NPWDA to be high/Low
//Variables:    setValue(char): 0=set NPWDA to LOW    1=set NPWDA to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64::setNPWDA(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFDFF) | ((setValue & 0x01) << 9);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setNPWDB
//Function:     set NPWDB to be high/Low
//Variables:    setValue(char): 0=set NPWDB to LOW    1=set NPWDB to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64::setNPWDB(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFBFF) | ((setValue & 0x01) << 10);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setNPWDC
//Function:     set NPWDC to be high/Low
//Variables:    setValue(char): 0=set NPWDC to LOW    1=set NPWDC to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64::setNPWDC(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xF7FF) | ((setValue & 0x01) << 11);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}



//-------------------------------------------------------------------------------------------------------------------//
//Name:         dumpFirmwareVersion
//Function:     Read and dump SPIROCA/SPIROCII card FPGA firmware version
//Variables:    
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
// Read and dump SPIROCA/SPIROCII card FPGA firmware version
void VATA64::dumpFirmwareVersion() {
    m_generalCtrlReg0 = spiRegisterRead(0, FIRMWARE_VERSION_REG_ADDR);
    std::cerr << "Firmware version v" << m_generalCtrlReg0 << ".0 " << std::endl;
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         dumpFirmwareVersion
//Function:     Read and dump the current status of Gene_ctrl_reg0 in VATA64 firmware
//Variables:    
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
// Read and dump Gene_ctrl_reg0 status
void VATA64::dumpGeneCtrlReg0() {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    std::cerr << "Info@Config_VATA64: GENERAL_REG0=";
    for(unsigned int i = 0; i<16; i++)
        std::cerr << ((m_generalCtrlReg0>>(15-i))&0x0001);
    std::cerr << std::endl;
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         resetChipConfig
//Function:     Reset the Slow Control interface by setting bit0 of Reg0(reset_cfg)in spiroc firmware
//Variables:    
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void VATA64::resetChipConfig() {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);

    m_generalCtrlReg0 |= 0x0001;  // set reset
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);

    m_generalCtrlReg0 &= 0xFFFE; // remove reset
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);

    std::cerr << "resetChipConfig finished!" << std::endl;
}



//---------------------------------------------------------------------------------------------------------------------//
// Name:        compareUfmToConfigArray
// Function:    compare UFM content to chipConfigArray.
// Parameters:  dumpOn(int): = print or not the contents we write into and read back from UFM.  1=print,  0=don´t print
//              chipConfigArray = 16bits Word Array converted from FrontEndChip config file
//              compareArrayNum = Number of the Array you want to compare
// Returns:     (bool)
//              false = UFM content is identical to chipConfigArray
//              true = UFM content is different to chipConfigArray
//--------------------------------------------------------------------------------------------------------------------//
bool VATA64::compareUfmToConfigArray(int dumpOn,unsigned short chipConfigArray[], unsigned int compareArrayNum) {

    unsigned short chipConfigArray_readBack[60];
    bool err=false;

    //spiToUfmOrRegister(0, 0xC3C3);   // set SPI communication to UsbBoard <-> UFM

    spiUfmReadBlock(0, 0x0000, compareArrayNum, chipConfigArray_readBack);


    ///list out what we write in and read back from the UFM   
    if(dumpOn==1){
        std::cerr << "Info@compareUfmToConfigArray:Compare write in and read back:" << std::endl;
        for(unsigned int index=0;index<s_configArrayNum_VATA64;index++) {
            std::cerr << "chipConfigArray         [" << index << "]=";
            for(unsigned int i = 0; i<16; i++)
              std::cerr << ((chipConfigArray[index]>>(15-i))&0x0001) << " ";
            std::cerr << std::endl;

            std::cerr << "chipConfigArray_readBack[" << index << "]=";
            for(unsigned int i = 0; i<16; i++)
              std::cerr << ((chipConfigArray_readBack[index]>>(15-i))&0x0001) << " ";
            std::cerr << std::endl;
 
            std::cerr << std::endl;      
	}
    }
    ////////////////////////////////////////////////////////

    for(unsigned int i=0; i<compareArrayNum; i++) {
        if (chipConfigArray_readBack[i] != chipConfigArray[i])
        {
            err = true;
            std::cerr << "Infomation:Content in UFM is diffenert from Config file" << std::endl;
            break;
        }
    }
    return(err);
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        writeConfigArrayToUfm
// Function:    write chip Config Array into UFM
// Parameters:  chipConfigArray = 16bits Word Array converted from FrontEndChip config file
//              writeConfigArrayNum = Number of the Array you want to write into UFM
// Return:      None
//---------------------------------------------------------------------------------------------------------------------//
void VATA64::writeConfigArrayToUfm(unsigned short chipConfigArray[], unsigned int writeConfigArrayNum) {
    
     unsigned char ufmStatus = spiUfmStatusRead(0) ;  
     std::cerr << "Info:before erase UFM, ufmStatus = ";
       for(unsigned int i = 0; i<8; i++)
         std::cerr << ((ufmStatus>>(8-i))&0x0001);
       std::cerr << std::endl;  
       // getchar();

 
    std::cerr << "Info:Write Config Arrays to UFM....." << std::endl;
    unsigned short addr = 0x0000;
    addr = 0x0000;

    //spiToUfmOrRegister(0, 0xC3C3);   // set SPI communication to UsbBoard <-> UFM
    std::cerr << "Erase UFM 1st!" << std::endl;
    spiUfmErase(0);
    sleep(1);   // wait 1s  !!Don't delete it!!  after spiUfmErase(0), we have to wait! Otherwise, ufmerase will not be successful! 

    std::cerr << "Erase UFM 2nd!" << std::endl;
    spiUfmErase(0);
    sleep(1);  // wait 1s  !!Don't delete it!!  after spiUfmErase(0), we have to wait! Otherwise, ufmerase will not be successful! 

    for(unsigned int i=0; i<writeConfigArrayNum; i++, addr++) {
        spiUfmWrite(0, addr, chipConfigArray[i]);
    }
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        sendStartConfigCMD
// Function:    set "start_cfg" in VATA64 firmware, the front end board will begin to write slow contro configuration from UFM to Slow Control Register
// Parameters:
// Returns:      None
//---------------------------------------------------------------------------------------------------------------------//
void VATA64::sendStartConfigCMD() {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 |= 0x0002;
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);  // send config from UFM to SPIROCA
    m_generalCtrlReg0 &= 0xFFFD;
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);  // load config
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        loadConfigToVATA64
// Function:    begin to configure Slow Control Register of VATA64 by setting "start_cfg". The configuration will be executed twice.compare UFM content with output of SROUT_SC at the end of the second one, in order to check the writing process is correct or not.
// Parameters:
// Returns:(bool)
//              false = configuration succeed
//              true  = configuration error
//--------------------------------------------------------------------------------------------------------------------//
bool VATA64::loadConfigToVATA64() {
    bool err = false;
    unsigned short generalCtrlReg0;


    // -------------------1st time config------------------------------

    //resetChipConfig();        // reset
    sendStartConfigCMD();
    std::cerr << "send Start_cfg!" << std::endl;
    while (1)
    {
        generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
        dumpGeneCtrlReg0();
        if (generalCtrlReg0 & 0x4000) {//config finished
            std::cerr << "Infomation: 1st load  Config from UFM to Spiroc,   Finished!" << std::endl;
	    dumpGeneCtrlReg0();
            if (generalCtrlReg0 & 0x8000)    {   //config error
                std::cerr << "ERROR:config error = 1, There is something wrong in VATA64 configuration! Please check!" << std::endl;
            }
            break;
        }
    }

    // --------------------2nd time ------------------------------

    sendStartConfigCMD();
    std::cerr << "Infomation: 2nd Load Config from UFM to Spiroc!...." << std::endl;
    while (1)
    {
        generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
        if (generalCtrlReg0 & 0x4000)  {   //config finished
            if (generalCtrlReg0 & 0x8000)    {   //config error
                std::cerr << "ERROR:config error = 1, There is something wrong in VATA64 configuration!please check!" << std::endl;
                err = true;
                //assert(false);
            }else{
                std::cerr << "Infomation:Config Spiroc Successfully!" << std::endl;
            }
            break;

        }
    }
    return(err);

}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        spiConfigVATA64
// Function:    configure Both Gene_Ctrl_Reg0 in VATA64 firmware and slow control register in spirocA chip
// Parameters:
// Returns:    (bool)
//              false = configuration succeed
//              true  = configuration error
//--------------------------------------------------------------------------------------------------------------------//
//config the spirocA with the slow control setting
bool VATA64::spiConfigVATA64(int dumpOn, unsigned short chipConfigArray[], char trigSelect)
{

    bool  err = false;

    std::cerr << "Begin to config VATA64..." << std::endl;

    
    dumpFirmwareVersion();
    dumpGeneCtrlReg0();
    // getchar();


    // Test R/W Gene_ctrl_reg0,
    // a loop to write an increasing number (from 0 to 0x3FFF) to Gene_ctrl_reg0 and read it back to check
//     for(unsigned short value=0; value<0x4000; value+=16) {
//         spiRegisterWrite(0,1,value);
//         if((spiRegisterRead(0,1)&0x3FFF)!= value) {
// 	    std::cerr << "for R/W Value  " << value <<"  is not correct!"<< std::endl; 
//             assert(false);
// 	}
//     }
//     std::cerr << "finish check register R/W, set GeneCtrlReg0 = 0xC4" << std::endl;
//     spiRegisterWrite(0,1,0xC4);
//     dumpGeneCtrlReg0();
//     getchar();
    //////////////////////////////////////////////////////////////////////////////////////////



    if (dumpOn==1) {
        std::cerr << "Info@spiConfigVATA64:Array write into UFM:" << std::endl;
        for(unsigned int index=0;index<s_configArrayNum_VATA64;index++) {
            std::cerr << "chipConfigArray[" << index << "]=";
            for(unsigned int i = 0; i<16; i++)
                std::cerr << ((chipConfigArray[index]>>(15-i))&0x0001) << " ";
            std::cerr << std::endl;
        }
    }

    // getchar();

    // ---------------------Begin to Config Reg0----------------------------------


    // Read and dump SPIROC card FPGA firmware version
    // Important! In order to confirm the version currently used in Spiroc card firmware
    // Always run this function before other further more commands.
 

    // test whether the C code can read and write register sucessfully!   
//     std::cerr << "set GeneCtrlReg0 bit7 to 1" << std::endl;
//     setTrigSelect(1);
//     dumpGeneCtrlReg0();

//     std::cerr << "set GeneCtrlReg0 bit7 to 0" << std::endl;
//     setTrigSelect(0);
//     dumpGeneCtrlReg0();
//     getchar();
    ////////////////////////////////////////////////////////////////////////////
    
    // reset the VATA64 config
    // resetChipConfig();

    // set trigSelect
    // setTrigSelect(trigSelect);

    std::cerr << "Info@spiConfigVATA64:after register config:" << std::endl;
    dumpGeneCtrlReg0();


    //--------------End of Reg0 Config--------------------------------------


    //--------------Begin to write config arrays into UFM ------------------
  
  
//     while(1){
//        writeConfigArrayToUfm(chipConfigArray, s_configArrayNum_VATA64);
//        compareUfmToConfigArray(1,chipConfigArray, s_configArrayNum_VATA64);
//     }

    // compare UFM content to chipConfigArray
    // 0 = UFM content is identical to chipConfigArray, skip unnecessary writing config arrays into UFM
    // 1 = UFM content is different to chipConfigArray, write config array again into UFM
    if (compareUfmToConfigArray(0,chipConfigArray, s_configArrayNum_VATA64) == 1) {
        do{
            writeConfigArrayToUfm(chipConfigArray, s_configArrayNum_VATA64);
        }while(compareUfmToConfigArray(0,chipConfigArray, s_configArrayNum_VATA64));
    }// continue writing config arrays in UFM until UFM content is identical to chipConfigArray
    std::cerr << "Info@Config_VATA64:Write Config to UFM successfully! " << std::endl;

    // load config file from UFM into VATA64
    bool loadConfigErr = false;
    do {
        loadConfigErr=loadConfigToVATA64();
    }while(loadConfigErr == true);

    //select the read access
    setSelect_nSPI_uplink(1);// select Up-Link access of the read interface
    return(err);
}

//-------------------------for Analogue Test of the chip-----------------------------------------
//---------------------------------------------------------------------------------------------------------------------//
// Name:        analogTest
// Function:    Use SPI (C code programed) signals to access spiroc READ register, in order to do analog test for the chip
// Parameters:
// Returns:None
//--------------------------------------------------------------------------------------------------------------------//
void VATA64::analogTest() {
    setHoldSpi(0) ;        // set Hold signal to Analogue memory Hold signal

    setDresetSpi(1);    // set Dreset
    setDresetSpi(0);    // move Dreset

    selectReadChannel(1);
    while(0) {
        std::cerr << "set pulse" << std::endl;
        setTestpulseSpi(1);          // set Testpulse =1
        setTestpulseSpi(0);          // set Testpulse =0
    }

    //       setHoldSpi(0) ;        // set Hold signal to Analogue memory Hold signal
    getchar();
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        selectContinousMode
// Function:    Use SPI (C code programed) signals to select a certain channel to be readout 
// Parameters:  channelIndex (unsigned int): the selected channel index (0,1....)         
// Returns:None
//--------------------------------------------------------------------------------------------------------------------//
void VATA64::selectContinuousMode(unsigned int channelIndex){
     setSelect_nSPI_uplink(0); // switch to use SPI signals to access spirocA READ register

     setDresetSpi(1);    // set Dreset, in order to clear read register
     setDresetSpi(0);    // move Dreset
     selectReadChannel(channelIndex);    //selec readout channel
}


