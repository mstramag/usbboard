#ifndef UsbBoardConfiguration_h
#define UsbBoardConfiguration_h

#include <iostream>
#include <vector>
#include <string>

#include "FrontEndBoard.h"
#include "../TypeDefs.h"

//#define NEW_USBBOARD

class UsbBoardConfiguration {
    public:
        UsbBoardConfiguration();
        ~UsbBoardConfiguration();

        bool loadFromFile(const std::string& filePath, int boardId);
        bool loadFromFile(const std::string& fileName);

        int usbBoardId() const;
        void setUsbBoardId(int id);

        bool withUplinkAdapter() const;

        bool ledInjectionModeOn() const;//Pujiang

        int holdDelayTime() const;
        void setHoldDelayTime(int hold_delay_time);

        int holdType() const;
        void setHoldType(int hold_type);

        int calDelay() const;
        void setCalDelay(int calDelay);

        int delayAfterReadout() const;
        void setDelayAfterReadout(int delayAfterReadout);

        int clkSelect() const;
        void setClkSelect(int clkSelect);

        int fifoWriteDelay() const;
        void setFifoWriteDelay(int fifoWriteDelay);

        int busyOutputSwitch() const;
        void setBusyOutputSwitch(int setBusyOutputSwitch);

        ReadoutMode readoutMode() const;
        void setReadoutMode(ReadoutMode mode);

        TriggerMode triggerMode() const;
        void setTriggerMode(TriggerMode mode);

        int uplinkId(unsigned short uplinkSlot) const;
        void setUplinkId(unsigned short uplinkSlot, int id);

        void setFirmwareFileName(const std::string& fileName);
        const std::string& firmwareFileName() const;

        bool uplinkIdExists(int id) const;

        static unsigned short sampleSize(const ReadoutMode& mode);
        unsigned short sampleSize() const;

        static unsigned short channelsPerReadoutChip(const ReadoutMode& mode);
        unsigned short channelsPerReadoutChip() const;

        void setEventsPerAccess(int eventsPerAccess) {m_eventsPerAccess = eventsPerAccess;}
        int eventsPerAccess() {return m_eventsPerAccess;}

        void setFrontEndBoardType(unsigned short slot, FrontEndBoardType);
        FrontEndBoardType frontEndBoardType(unsigned short slot) const;

        void setExecuteFrontEndBoardConfiguration(unsigned short slot, bool);
        bool executeFrontEndBoardConfiguration(unsigned short slot) const; //here slot is uplinkID

        void setFrontEndBoardConfiguration(unsigned short slot, FrontEndBoardConfiguration*);
        FrontEndBoardConfiguration* frontEndBoardConfiguration(unsigned short slot) const;

        void writeToStream(std::ostream& stream = std::cerr) const;
        bool loadFromStream(std::istream& stream);

        unsigned char biasSelect() const {return m_biasSelect;}
        float biasVoltageX() const {return m_biasVoltageX;}
        float biasVoltageY() const {return m_biasVoltageY;}

        //const static unsigned int s_physicalFifoSize = 2*1024; //bytes for each uplink
    const static unsigned int s_physicalFifoSize = 2*1024*2; //bytes for each uplink - changed Olivier 31.10.2016: increased FIFO size

        bool executeAdjustDACMode(unsigned short slot) const;

#ifdef NEW_USBBOARD
        bool IsSelfTrigEnable() const;
        uint32_t SelfTrigPeriod() const;
        uint32_t SelfTrigLeadTime() const;
#endif

    private:
        int m_usbBoardId;
        bool m_withUplinkAdapter;
        bool m_ledInjectionModeOn;
        int m_holdDelayTime;
        int m_holdType;
        int m_calDelay;
        int m_delayAfterReadout;
        int m_clkSelect;
        int m_fifoWriteDelay;
        int m_busyOutputSwitch;
        int m_eventsPerAccess;
        unsigned char m_biasSelect;
        float m_biasVoltageX;
        float m_biasVoltageY;
        ReadoutMode m_readoutMode;
        TriggerMode m_triggerMode;
        FrontEndBoardType m_frontEndBoardType[s_uplinksPerUsbBoard];
        bool m_executeFrontEndBoardConfiguration[s_uplinksPerUsbBoard];
        FrontEndBoardConfiguration* m_frontEndBoardConfiguration[s_uplinksPerUsbBoard];
        std::string m_firmwareFileName;
        int m_uplinkId[s_uplinksPerUsbBoard];
        std::string m_comment[s_uplinksPerUsbBoard];
        bool m_adjustDACMode[s_uplinksPerUsbBoard];

        //This will only be used for NewUsbBoard   --by Snow@2012.05.17,EPFL
#ifdef NEW_USBBOARD
        bool m_selfTrigEnalbe;
        uint32_t m_selfTrigPeriod;
        uint32_t m_selfTrigLeadTime;
#endif
};

#endif
