#include "UsbBoard.h"
#include "FrontEndBoard.h"
#include "HpeVa256.h"
#include "EcalSpiroc.h"
#include "EcalSpirocII.h"
#include "VATA64.h"
#include "VATA64V2.h"
#include "VATA64V2_DC.h"
#include <string>
#include <iostream>
#include <unistd.h>
#include <assert.h>
#include <limits.h>
#include <cstring>
#include <fstream>
#if !defined(NO_QUSB) && !defined(WIN32)
#include <usb.h>
#endif

#include "Settings.h"

#define DUMP_DEBUG


//#define CHECK_FIRST_FIVE_EVENT

//--------------------------------------------------------------------------------------------------------------------//
//Name:         UsbBoard
//Function:     defaul constructor, initialize member variables
//Variables:    handle provided by QuickUsb.
//-------------------------------------------------------------------------------------------------------------------//
UsbBoard::UsbBoard(HANDLE handle)
    : m_handle(handle)
    , m_spiSwitch(-1)
{
#ifndef NO_QUSB
    m_quickUsbSerialNumber = quickUsbSerialNumber(true);
#else
    m_quickUsbSerialNumber = 0;
#endif
    for (unsigned int slot = 0; slot < s_uplinksPerUsbBoard; ++ slot)
      m_frontEndBoards[slot] = 0;
    std::cerr << "Information: Found USB board with serial number " << m_quickUsbSerialNumber << " and Board ID " << usbBoardId() << "." << std::endl<< std::endl;
#ifdef NEW_USBBOARD
    _qspiMode = QSPI_MSB_FIRST | BAUD_1M;
    _qspiDelay = 0;
    _qspiCmdStartAdr = QSPI_CMD_RAM_BASE_ADR;
    _qspiRepeat = false;
    _qspiRepeatPeriod = 40000000; //default 0.1s
    _isBiasAOn = false;
    _isBiasBOn = false;
#endif

}

UsbBoard::~UsbBoard(){
  for (unsigned int slot = 0; slot < s_uplinksPerUsbBoard; ++ slot){
    if ( m_frontEndBoards[slot] ){
      delete m_frontEndBoards[slot];
      m_frontEndBoards[slot] = 0;
    }
  }
  //std::cerr << "Information: ~UsbBoard()" << std::endl<< std::endl;
  //sleep(1);
  //powerOnBias('F');
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         quickUsbSerialNumber
//Function:     return QuickUSB serial number, either from a saved member variable or read from QuickUSB interface.
//Variables:    readFromBoard, true: read from QuickUSB interface, false: return member variable
//Returns:      (int) return QUSB serial number
//----------------------------------------------------------------------------------------------------------// ---------//
int UsbBoard::quickUsbSerialNumber(bool readFromBoard)
 {
     if (readFromBoard) {
 #ifndef NO_QUSB
         char buffer[s_cStringLength];
         if (QuickUsbGetStringDescriptor(m_handle, 3, buffer, s_cStringLength)) {
             return atoi(buffer);
         }
         std::cerr << "Failure: QuickUSB serial number could not be read." << std::endl;
         assert(false);
 #endif
     }
     return m_quickUsbSerialNumber;
 }

// //--------------------------------------------------------------------------------------------------------------------//
// //Name:         setBusy
// //Function:     set bit1 of Reg2 in UsbBoard Firmware.
// //Variables:    value(bool)
// //              true = impose busy output 1, its to fake a busy state
// //              false = no operation
// //Returns:      (bool)
// //              true = no error
// //              false = write to register error!
// //-------------------------------------------------------------------------------------------------------------------//
 bool UsbBoard::setBusy(bool value) const
 {
 #ifndef NO_QUSB
     bool err = false;
#ifndef NEW_USBBOARD
	printf("Called setBusy ******************************************: %d \n",value);
     unsigned short registerValue = readFpgaRegister(0x1002);
     if (value) {
         if (!writeFpgaRegister(0x1002, registerValue | 0x0002)) err = true;
     } else {
         if (!writeFpgaRegister(0x1002, registerValue & 0xFFFD)) err = true;
     }
#else
     unsigned short registerValue = readFpgaRegister(CLOCK_MODE_BUSY_SET_REG);
     if (value) {
         if (!writeFpgaRegister(CLOCK_MODE_BUSY_SET_REG, registerValue | MANUAL_SET_BUSY)) err = true;
     } else {
         if (!writeFpgaRegister(CLOCK_MODE_BUSY_SET_REG, registerValue & (~MANUAL_SET_BUSY))) err = true;
     }
#endif
     if (err) {
         assert(false);
         return false;
     }
 #endif
     return true;
 }

// //--------------------------------------------------------------------------------------------------------------------//
// //Name:         configureFpga
// //Function:     upload firmware to FPGA by QUSB device
// //Variables:    force(bool)
// //              true = always configurate FPGA, no matter the code has been loaded successfully or not yet.
// //              false= check FPGA status, if the VHDL has already been loaded, skip the config process.
// //Returns:      (bool)
// //              true = config successfully
// //              false= error exist during configuration
// //-------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
bool UsbBoard::configureFpga(bool force)
 {
 #ifndef NO_QUSB
     //temp for debug

     writeFpgaRegister(0x1000, 0x000A);
     uint16_t temp_reg = readFpgaRegister(0x1000);
     printf("after write reg0x1000 to A, readback temp_reg = %X\n",temp_reg);

     //check whether the register has correctly been set. If not the VHDL code
     //has not been loaded yet. At least that's my interpretation.

     std::cerr
         << "UsbBoard::configureFPGA() called."
         <<  std::endl;

     if(force==true)
       std::cerr<< "UsbBoard::configureFPGA is force to execute once the program is executed" <<  std::endl;

     if (force || readFpgaRegister(0x1000) != 0x000A) {

         fpgaSetReset();
         bool err = false;
         const unsigned short blockLength = 64;
         unsigned char fpgaData[blockLength];
         FILE* fpgaFile = 0;

         setSpiSwitch(4);  
         unsigned short fpgaConfigStatus = 0;

	 //fpgaFile = fopen("/home/pebs_ecal/usbBoard/readout/readoutSettings/test.c", "rb");
	 //if(!fpgaFile) printf("open test.c failed!\n");
         //printf("OK \n");
	 //getchar();
         if (!QuickUsbIsFpgaConfigured(m_handle, &fpgaConfigStatus)) err = true;
         assert(!err);

       
	 if (fpgaConfigStatus == 0) { //if not already configured  
             //config FPGA
            std::cerr << "Information: FPGA not Configured! Configuring..." << std::endl;
            QuickUsbStartFpgaConfiguration(m_handle); // PE6 will be set to 0!!!

            std::cerr<<"usbBoardID="<<usbBoardId()<<std::endl;
	   
	    //std::cerr<<"Attention!! firmware path is fixed to ../readoutSettings/va32_readout_top_v1.rbf"<<std::endl;
	    //fpgaFile =fopen("/home/pebs_ecal/usbBoard/readout/readoutSettings/va32_readout_top_v1.rbf","rb");
	    
	    std::cerr<< "firmware path ="<< Settings::instance()->usbBoardConfiguration(usbBoardId()).firmwareFileName(); 
	    // std::cerr<<"EEE"<<std::endl;
           fpgaFile = fopen(Settings::instance()->usbBoardConfiguration(usbBoardId()).firmwareFileName().c_str(), "rb");
            
	   //std::cout <<"open file=" <<Settings::instance()->usbBoardConfiguration(usbBoardId()).firmwareFileName()<<"end"<<std::endl;
	    /// printf("open file=%s\n",Settings::instance()->usbBoardConfiguration(usbBoardId()).firmwareFileName().c_str());
	    //
            //getchar();
                        


            if (!fpgaFile) {
	      std::cerr << "Failure: could not open file: \"" << Settings::instance()->usbBoardConfiguration(usbBoardId()).firmwareFileName() << "\"." << std::endl;
	      std::cerr << "Please check if the path is right or the rbf file exists!" << std::endl;
	      //std::cerr << "EEE"<<std::endl;
                assert(false);
            }
           
            while (!feof(fpgaFile)) {
                int readLength = fread(fpgaData, 1, blockLength, fpgaFile);
                if (!QuickUsbWriteFpgaData(m_handle, fpgaData, readLength)) {
                    std::cerr << "Failure: FPGA configuration data could not be written." << std::endl;
                    assert(false);
                }
                if (ferror(fpgaFile)) {
                    std::cerr << "Failure: error reading FPGA configuration file." << std::endl;
                    assert(false);
                }
            }
            fclose(fpgaFile);
        }// end of if(fpgaConfigStatus == 0)
    }
    resetFpga();
#endif
    return true;
}  
#else

 bool UsbBoard::configureFpga(bool force){
 #ifndef NO_QUSB

      //check whether the register has correctly been set. If not the VHDL code
      //has not been loaded yet. At least that's my interpretation.
      unsigned char temp_char;

      temp_char= readFpgaRegister(FIRMWARE_VERSION_REG);
      printf("firmware Version = %d\n",temp_char);
      writeFpgaRegister(FE_TYPE_TRIG_MODE_REG,0x000A);
      temp_char=readFpgaRegister(FE_TYPE_TRIG_MODE_REG);
      printf("set FE_TYPE_TRIG_MODE_REG = 0x000A, readback it = %#x\n",temp_char);

      if(force==true)
          std::cerr<< "UsbBoard::configureFPGA is force to execute once the program is executed" <<  std::endl;

      bool interForce = false;
      if (interForce ||force || readFpgaRegister(FE_TYPE_TRIG_MODE_REG) != 0x000A) {
          printf("Before configure FPGA, reset fpga and change to PS mode!\n");
          if(!SetPSMode()) return false;

          fpgaSetnReset();

          bool err = false;


          //read portE
//          if (!QuickUsbReadPortDir(m_handle, 0x0004, &temp_char)) err = true;
//          printf("PORTE dir = %#x\n",temp_char);
//          temp_char = 0x0B;
//          if (!QuickUsbWritePortDir(m_handle, 0x0004, temp_char)) err = true;
//          if (!QuickUsbReadPortDir(m_handle, 0x0004, &temp_char)) err = true;
//          printf("after PORTE dir = %#x\n",temp_char);


          //bool err = false;
          const unsigned short blockLength = 64;
          unsigned char fpgaData[blockLength];
          FILE* fpgaFile = 0;

          unsigned short fpgaConfigStatus = 0;


         //config FPGA
         std::cerr << "Information: FPGA not Configured! Configuring..." << std::endl;
         QuickUsbStartFpgaConfiguration(m_handle); // PE6 will be set to 0!!!

         std::cerr<<"usbBoardID="<<usbBoardId()<<std::endl;
         std::cerr<< "firmware path ="<< Settings::instance()->usbBoardConfiguration(usbBoardId()).firmwareFileName()<<std::endl;
         fpgaFile = fopen(Settings::instance()->usbBoardConfiguration(usbBoardId()).firmwareFileName().c_str(), "rb");


         if (!fpgaFile) {
               std::cerr << "Failure: could not open file: \"" << Settings::instance()->usbBoardConfiguration(usbBoardId()).firmwareFileName() << "\"." << std::endl;
               std::cerr << "Please check if the path is right or the rbf file exists!" << std::endl;
               //std::cerr << "EEE"<<std::endl;
                 assert(false);
         }

         while (!feof(fpgaFile)) {
               int readLength = fread(fpgaData, 1, blockLength, fpgaFile);
               if (!QuickUsbWriteFpgaData(m_handle, fpgaData, readLength)) {
                     std::cerr << "Failure: FPGA configuration data could not be written." << std::endl;
                     assert(false);
               }
               if (ferror(fpgaFile)) {
                     std::cerr << "Failure: error reading FPGA configuration file." << std::endl;
                     assert(false);
               }
         }
         fclose(fpgaFile);


         //wait for fpga configuration finished.
         for (int i = 0; i < 1000; i++) {
               if (!QuickUsbIsFpgaConfigured(m_handle, &fpgaConfigStatus)) err = true;
                     assert(!err);
                     if (fpgaConfigStatus == 1) {
                             printf("fpgaConfigStatus == 1, configureFPGA finished! \n");
                             break;
                     } else {
                             printf("configureFPGA() -usbBoardID = %d - fpga config status is %i after uploading vhdl. waiting!\n",
                                     usbBoardId(), fpgaConfigStatus);
                             //usleep(10);
                             sleep(10);
                    }
              }
         }else{
              printf(" Don't need to configure FPGA\n");
         }

         resetFpga();
         temp_char= readFpgaRegister(FIRMWARE_VERSION_REG);
         printf("after configureFPGA:firmware Version = %d\n",temp_char);
 #endif
         return true;
 }
#endif

//--------------------------------------------------------------------------------------------------------------------//
//Name:         resetFpga
//Function:     PIN PA0 of the QUSB device is used to send "reset" signal to the UsbBoard firmware FPGA, this reset
//              will reset the whole FPGA, not only the usbBoard DAQ related part
//Variables:
//Returns:
//-------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
 bool UsbBoard::resetFpga()
{
#ifndef NO_QUSB
    bool err = false;
    unsigned char temp_char;
    if (!QuickUsbReadPortDir(m_handle, 0x0000, &temp_char)) err = true;
    temp_char |= 0x01;
    if (!QuickUsbWritePortDir(m_handle, 0x0000, temp_char)) err = true;      //set PA0 to output
    temp_char = 0x01;
    if (!QuickUsbWritePort(m_handle, 0x0000, &temp_char,1)) err = true;     // set PA0 = '1'
    // printf("Reset QUSB DAQ FPGA User logic\n");
    temp_char = 0x00;
    if (!QuickUsbWritePort(m_handle, 0x0000, &temp_char,1)) err = true;     // set PA0 = '0'
    assert(!err);
#endif
    return true;
}
#else
 bool UsbBoard::resetFpga()   // in New Firmware, since it is nRESET_FPGA, low level effective for this reset signal. In order to reset FPGA, set nRESET_FPGA = 0, then release it . by Snow @2012.05.21. EPFL
{
#ifndef NO_QUSB
    bool err = false;
    unsigned char temp_char;
    uint16_t length;

    if (!QuickUsbReadPortDir(m_handle, 0x0000, &temp_char)) err = true;
    temp_char |= 0x01;
    if (!QuickUsbWritePortDir(m_handle, 0x0000, temp_char)) err = true;      //set PA0 to output

    length =1;
    if(!QuickUsbReadPort(m_handle,0x0000,&temp_char,&length)) err = true;
    printf("before reset PORT_A = %#x\n",temp_char);

    temp_char &= 0xFE;
    if (!QuickUsbWritePort(m_handle, 0x0000, &temp_char,1)) err = true;     // set PA0 = '0'
    if(!QuickUsbReadPort(m_handle,0x0000,&temp_char,&length)) err = true;
//    printf("set reset : PORT_A = %#x\n",temp_char);
//    getchar();


    temp_char |=0x01;
    if (!QuickUsbWritePort(m_handle, 0x0000, &temp_char,1)) err = true;     // set PA0 = '1'
    if(!QuickUsbReadPort(m_handle,0x0000,&temp_char,&length)) err = true;
//    printf("clear reset : PORT_A = %#x\n",temp_char);
//    getchar();

    assert(!err);
#endif
    return true;
}
#endif

//--------------------------------------------------------------------------------------------------------------------//
//Name:         configureDaq
//Function:     1.write config variables getting from cfg file to their corresponding registers in UsbBoard Firmware
//              2.configurate front end chip, if this function has been switched on in the UsbBoard cfg file
//Variables:    None
//Returns:      (bool)
//              true = config successfully
//              false = error exist during configuration
//-------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
bool UsbBoard::configureDaq()
{
    bool err = false;
#ifndef NO_QUSB
    const UsbBoardConfiguration& config = Settings::instance()->usbBoardConfiguration(usbBoardId());
    std::cerr
        << "UsbBoard::configureDaq() called."
        <<  std::endl;


    //---- Begin to write config variables into corresponding Registers in the UsbBoard----------------

    unsigned short reg0 = readFpgaRegister(0x1000);


    switch (config.triggerMode()) {
        case internal : reg0 = reg0 | 0x0008; break;
        case external : reg0 = reg0 | 0x0010; break;
        case calibration : reg0 = (reg0 & 0xFFE7) | 0x0100; break;
        //case led      : reg0 = (reg0 & 0xFEE7) | 0x0200;break;
        case triggerModeEnd : err = true; break;

    }


    if(config.ledInjectionModeOn())
        reg0 = reg0 | 0x0200;
    else
        reg0 = reg0 & 0xFDFF;//Pujiang


    if (!writeFpgaRegister(0x1000, reg0)) err = true;

    // set hold_delay_time : the slow hold signal will be delayed by a period of time=hlod_delay_time*Clock
    if (!writeFpgaRegister(0x1027, (config.holdDelayTime() & 0x01FF))) err = true;
    // set hold_type, 0->fast hold, 1-> slow hold; bit0->Uplink1, bit1->Uplink2, ...
    if (!writeFpgaRegister(0x1028, (config.holdType() & 0x00FF))) err = true;
    // wait time after every event's reset period, time=delay_value*10ns
    if (!writeFpgaRegister(0x1008, (config.delayAfterReadout() & 0xFFFF))) err = true;
    // delay between testpulse and hold signal. time=(delay_value-10)*10ns. just for SPIROC_EPFL mode
    if (!writeFpgaRegister(0x1009, (config.calDelay() & 0xFFFF))) err = true;

    unsigned short reg2 = readFpgaRegister(0x1002);
   // reg2 = (reg2 & 0x0003) + (config.clkSelect() & 0x0001) + ((config.busyOutputSwitch()<<1) & 0x0002);
   reg2 = (reg2 & 0x0003) + (config.clkSelect() & 0x0001) +  0x0002; // busy 1
    if (!writeFpgaRegister(0x1002, reg2)) err = true;

    reg0 = readFpgaRegister(0x1000);
    reg0 = (reg0 & 0xFFF8) + (config.readoutMode() & 0x0007);

    if (!writeFpgaRegister(0x1000, reg0)) err = true;
    reg0 = (reg0 & 0x0FFF) + (config.usbBoardId() << 12);

    // set the uplink ID
    if (!writeFpgaRegister(0x1011, ((reg0 & 0xF00F) + ((config.uplinkId(1) & 0x00FF) << 4)))) err = true;
    if (!writeFpgaRegister(0x1012, ((reg0 & 0xF00F) + ((config.uplinkId(2) & 0x00FF) << 4)))) err = true;
    if (!writeFpgaRegister(0x1013, ((reg0 & 0xF00F) + ((config.uplinkId(3) & 0x00FF) << 4)))) err = true;
    if (!writeFpgaRegister(0x1014, ((reg0 & 0xF00F) + ((config.uplinkId(4) & 0x00FF) << 4)))) err = true;
    if (!writeFpgaRegister(0x1015, ((reg0 & 0xF00F) + ((config.uplinkId(5) & 0x00FF) << 4)))) err = true;
    if (!writeFpgaRegister(0x1016, ((reg0 & 0xF00F) + ((config.uplinkId(6) & 0x00FF) << 4)))) err = true;
    if (!writeFpgaRegister(0x1017, ((reg0 & 0xF00F) + ((config.uplinkId(7) & 0x00FF) << 4)))) err = true;
    if (!writeFpgaRegister(0x1018, ((reg0 & 0xF00F) + ((config.uplinkId(8) & 0x00FF) << 4)))) err = true;

    setFifoWriteDelay(config.fifoWriteDelay());
    //----Finish writing config variables in corresponding registers in the UsbBoard

    //dumpStatus();
    bool withAdapter = Settings::instance()->usbBoardConfiguration(usbBoardId()).withUplinkAdapter();

    if(withAdapter){
        if (!powerOnBias(config.biasSelect())) err = true;
        if (!this->setBiasVoltage(config.biasVoltageX(),config.biasVoltageY())) err =true;
        printf("set bias,biasX(uplink1~4) = %.2fV     biasY(uplink5~8) = %.2fV\n",config.biasVoltageX(),config.biasVoltageY());
        printf("press any key to continue configuring DAQ\n");
//getchar();

       //The following part is only for calibrating bias easily  ---add by Snow 2011.12.08 @epfl
       /*
        float biasX = 50 ;
        float biasY = 50 ;
        for (;biasX<100.5;biasX++,biasY++){
           if (!this->setBiasVoltage(biasX,biasY)) err =true;
           sleep(1);
           printf("set bias,biasX(uplink1~4) = %.2fV     biasY(uplink5~8) = %.2fV\n",biasX,biasY);
           printf("press any key to continue configuring DAQ\n");
           //getchar();
       }
       */

    }

#endif
    if (err) {
        std::cout << "Failure: Could not configure DAQ." << std::endl;
        assert(false);
        return false;
    } else {
        return true;
    }
}

#else
bool UsbBoard::configureDaq()
{
    bool err = false;
#ifndef NO_QUSB
    const UsbBoardConfiguration& config = Settings::instance()->usbBoardConfiguration(usbBoardId());
    std::cerr
        << "UsbBoard::configureDaq() called."
        <<  std::endl;


    //---- Begin to write config variables into corresponding Registers in the UsbBoard----------------

   unsigned short reg0 = readFpgaRegister(FE_TYPE_TRIG_MODE_REG);

   if(config.IsSelfTrigEnable()) {
        reg0 |= SELF_TRIGGER_ENABLE; printf("configureDaq()- internal trigger\n");
   }else{
        reg0 &= ~SELF_TRIGGER_ENABLE; printf("configureDaq()- external trigger\n");

   }

    if(config.ledInjectionModeOn())
        reg0 |= LED_INJECTION_MODE_ENABLE;
    else
        reg0 &= ~LED_INJECTION_MODE_ENABLE;

    if (!writeFpgaRegister(FE_TYPE_TRIG_MODE_REG, reg0)) err = true;
    if(!err){
        uint16_t reg_back = readFpgaRegister(FE_TYPE_TRIG_MODE_REG);
        printf("read back FE_TYPE_TRIG_MODE_REG = %#x\n",reg_back);
        //getchar();
        if(reg_back != reg0){
            printf("writeFpga Register failed!!\n");
            //getchar();
            return false;
        }
    }
    // set hold_delay_time : the slow hold signal will be delayed by a period of time=hlod_delay_time*Clock
    if (!writeFpgaRegister(HOLD_DELAY_TIME_REG, (config.holdDelayTime() & 0xFFFF))) err = true;  //?why it is limited by 0x01FF???
    if(!err){
        uint16_t reg_back = readFpgaRegister(HOLD_DELAY_TIME_REG);
        printf("read back HOLD_DELAY_TIME_REG = %#x\n",reg_back);
        //getchar();
        if(reg_back != (config.holdDelayTime() & 0xFFFF)){
            printf("HOLD_DELAY_TIME_REG = %#x, supposed to be = %#x\n",reg_back,config.holdDelayTime() & 0xFFFF);
            //getchar();
            return false;
        }
    }
    // set hold_type, 0->fast hold, 1-> slow hold; bit0->Uplink1, bit1->Uplink2, ...
    if (!writeFpgaRegister(HOLD_TYPE_REG, (config.holdType() & 0x00FF))) err = true;
    // wait time after every event's reset period, time=delay_value*10ns
    if (!writeFpgaRegister(DELAY_AFTER_READOUT_REG, (config.delayAfterReadout() & 0xFFFF))) err = true;

    //!!!!!!!!Need to be confirmed!!!!!!!!! Then modify it properly!
    // Attention, in new UsbBoard, parameter calDelay is actually used as settings for SELF_TRIGGER_LEAD_TIME, which defined the delay between
    if (!writeFpgaRegister(SELF_TRIGGER_LEAD_TIME_LOW_REG, (config.calDelay() & 0xFFFF))) err = true;

    unsigned short reg2 = readFpgaRegister(CLOCK_MODE_BUSY_SET_REG);
    reg2 = (reg2 & 0x0003) + (config.clkSelect() & 0x0001) + ((config.busyOutputSwitch()<<1) & 0x0002);
    if (!writeFpgaRegister(CLOCK_MODE_BUSY_SET_REG, reg2)) err = true;
    reg2 = readFpgaRegister(CLOCK_MODE_BUSY_SET_REG);
//    printf("after set regs, CLOCK_MODE_BUSY_SET_REG = %#x\n",reg2);
//    getchar();

    reg0 = readFpgaRegister(FE_TYPE_TRIG_MODE_REG);
    reg0 = (reg0 & 0xFFF8) + (config.readoutMode() & 0x0007);

    if (!writeFpgaRegister(FE_TYPE_TRIG_MODE_REG, reg0)) err = true;


    reg0 = (reg0 & 0x0FFF) + (config.usbBoardId() << 12);
    // set the uplink ID
    if (!writeFpgaRegister(HEADER1_REG, ((reg0 & 0xF00F) + ((config.uplinkId(1) & 0x00FF) << 4)))) err = true;
    if (!writeFpgaRegister(HEADER2_REG, ((reg0 & 0xF00F) + ((config.uplinkId(2) & 0x00FF) << 4)))) err = true;
    if (!writeFpgaRegister(HEADER3_REG, ((reg0 & 0xF00F) + ((config.uplinkId(3) & 0x00FF) << 4)))) err = true;
    if (!writeFpgaRegister(HEADER4_REG, ((reg0 & 0xF00F) + ((config.uplinkId(4) & 0x00FF) << 4)))) err = true;
    if (!writeFpgaRegister(HEADER5_REG, ((reg0 & 0xF00F) + ((config.uplinkId(5) & 0x00FF) << 4)))) err = true;
    if (!writeFpgaRegister(HEADER6_REG, ((reg0 & 0xF00F) + ((config.uplinkId(6) & 0x00FF) << 4)))) err = true;
    if (!writeFpgaRegister(HEADER7_REG, ((reg0 & 0xF00F) + ((config.uplinkId(7) & 0x00FF) << 4)))) err = true;
    if (!writeFpgaRegister(HEADER8_REG, ((reg0 & 0xF00F) + ((config.uplinkId(8) & 0x00FF) << 4)))) err = true;

    setFifoWriteDelay(config.fifoWriteDelay());
    //----Finish writing config variables in corresponding registers in the UsbBoard

    if (!powerOnBias(config.biasSelect())) err = true;
    if(err) printf("powerOnBias failed!\n");
    if (!this->SetBiasA(config.biasVoltageX())) err =true;
    else{
        printf("set bias,biasA= %.2fV \n",config.biasVoltageX());
    }

    if (!this->SetBiasB(config.biasVoltageY())) err =true;
    else{
        printf("set bias,biasB= %.2fV \n",config.biasVoltageY());
    }


    //add to initialize selfTrigPeriod
    uint32_t longTemp =config.SelfTrigPeriod();
    uint16_t temp=longTemp>>16;
    writeFpgaRegister(SELF_TRIGGER_PERIOD_HIGH_REG,temp);
    temp = longTemp;
    writeFpgaRegister(SELF_TRIGGER_PERIOD_LOW_REG,temp);


    //initialize selfTriggerLeadTime
    longTemp = config.SelfTrigLeadTime();
    temp = longTemp>>16;
    writeFpgaRegister(SELF_TRIGGER_LEAD_TIME_HIGH_REG,temp);
    temp = longTemp;
    writeFpgaRegister(SELF_TRIGGER_LEAD_TIME_LOW_REG,temp);


    printf("After configureDAQ()\n");
    dumpStatus();

    //initialize QueuedSPI
    err |=!InitQueuedSpi();



#endif
    if (err) {
        std::cout << "Failure: Could not configure DAQ." << std::endl;
        assert(false);
        return false;
    } else {
        printf("finished configureDAQ\n");
        return true;
    }
}

#endif  //for ifndef NEW_USBBOARD
//--------------------------------------------------------------------------------------------------------------------//
//Name:         runContinuousMode
//Function:     set the frontEnd board on the given uplink to continuous mode, which means the output of this board will be fixed to one slected channel.
//              Most of the time I use it to observe the analogue output of the frontend chip
//Variables:    uplinkSlot (unsigned int)(count from 1,2,3...8)
//              channel(unsigned int)(count from 0,1,2...)
//Returns:      void
//-------------------------------------------------------------------------------------------------------------------//
void UsbBoard::runContinuousMode(unsigned int uplinkSlot, unsigned int channel)
{

#ifndef NO_QUSB
  m_frontEndBoards[uplinkSlot-1]->selectContinuousMode(channel);
  getchar();
#endif
}



//--------------------------------------------------------------------------------------------------------------------//
//Name:         resetDaq
//Function:     set bit1 of Reg1 in the UsbBoard firmware to 1, this could reset the whole UsbBoard
//Variables:    None
//Returns:      (bool)
//              true = config successfully
//              false= error exist during configuration/
//-------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
bool UsbBoard::resetDaq() const
{
#ifndef NO_QUSB
    bool err = false;
    unsigned short reg = readFpgaRegister(0x1001);
    if (!writeFpgaRegister(0x1001, reg | 0x0006)) err = true; //set the bit 1 and 2 of register1 to '1' for reset
    if (!writeFpgaRegister(0x1001, reg)) err = true; //remove reset
    assert(!err);
#endif
    return true;
}
#else
bool UsbBoard::resetDaq() const
{
#ifndef NO_QUSB
    bool err = false;
    unsigned short reg = 0;
    reg = readFpgaRegister(RESET_DAQ_REG);
    printf("before set RESET_FIFO = 1, fifoCount = %d \n",getFIFOCount());
    if (!writeFpgaRegister(RESET_DAQ_REG, reg | RESET_FIFO)) err = true;
    printf("when set RESET_FIFO = 1, fifoCount = %d \n",getFIFOCount());
    if (!writeFpgaRegister(RESET_DAQ_REG, reg & (~RESET_FIFO))) err = true; //remove reset
    printf("after set RESET_FIFO = 1, fifoCount = %d \n",getFIFOCount());
    assert(!err);
#endif
    return true;
}
#endif

//--------------------------------------------------------------------------------------------------------------------//
//Name:         configureFrontEndBoards
//Function:     refering to parameters frontEndBoardType and executeFrontEndBoardConfiguration set in usbBoard config file, each uplink can decide separately whether it is necessary to configurate the FrontEnd Board or not, if necessary, each uplink can choose its own config type. 
//Variables:    None
//Returns:      (bool)
//              true = error exists
//              false= cofig sucessuflly
//-------------------------------------------------------------------------------------------------------------------//
bool UsbBoard::configureFrontEndBoards()
{    
    bool err = false;
#ifndef NO_QUSB
    const UsbBoardConfiguration& config = Settings::instance()->usbBoardConfiguration(usbBoardId());
    for (unsigned int slot = 1; slot <= s_uplinksPerUsbBoard; ++slot) {
        std::cerr <<"UsbBoard::configureFrontEndBoards"<< "uplink:"<<slot << std::endl;
        if (m_frontEndBoards[slot-1]) {
            delete m_frontEndBoards[slot-1];
            m_frontEndBoards[slot-1] = 0;
        }
        FrontEndBoardType type = config.frontEndBoardType(slot);
        switch (type) {
            case 0 :
                m_frontEndBoards[slot-1] = new HpeVa256(this, type, slot-1);
                break;
            case 1 :
                m_frontEndBoards[slot-1] = 0; //TODO
                break;
            case 2 :
                m_frontEndBoards[slot-1] = 0; //TODO
                break;
            case 3 :
                m_frontEndBoards[slot-1] = new EcalSpiroc(this, type, slot-1);
                break;
 	    case 4 :
                m_frontEndBoards[slot-1] = new EcalSpirocII(this, type, slot-1);
                break;
 	    case 5 :
                m_frontEndBoards[slot-1] = new VATA64(this, type, slot-1);
                break;
            case 6 :
                m_frontEndBoards[slot-1] = new VATA64V2(this, type, slot-1);
                break;
	    case 7 :
	        m_frontEndBoards[slot-1] = new VATA64V2_DC(this,type, slot-1);
	        break;
            default :
                m_frontEndBoards[slot-1] = 0;
        }
        if (m_frontEndBoards[slot-1] && config.executeFrontEndBoardConfiguration(slot)) {
            assert(m_frontEndBoards[slot-1]);

            //first check whether in adjustDACMode
            if(config.executeAdjustDACMode(slot) && type ==VATA64V2Type) static_cast<VATA64V2Configuration*>(config.frontEndBoardConfiguration(slot))->SetAdjustDACMode();
            //m_frontEndBoards[slot-1]->setSlotNo(slot-1);    //Added by lht for ID FE
            if (!m_frontEndBoards[slot-1]->configure(config.frontEndBoardConfiguration(slot))) err = true;     // 
        }
    }
#endif
    return !err;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         frontEndBoardBySlot
//Function:     Refering to the given slot index(1,2...8), find out its corresponding FrontEnd board configuration parameters.
//Variables:    slot (unsigned int)
//Returns:      (FrontEndBoard*)
//-------------------------------------------------------------------------------------------------------------------//
FrontEndBoard* UsbBoard::frontEndBoardBySlot(unsigned int slot)
{
#ifndef NO_QUSB
    assert(0 < slot && slot < s_uplinksPerUsbBoard+1);
#endif    
    return m_frontEndBoards[slot-1];
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         frontEndBoardByUplinkId
//Function:     Refering to the given uplinkId, find out its corresponding FrontEnd board configuration parameters.
//Variables:    uplinkId  (int)
//Returns:      (FrontEndBoard*)
//-------------------------------------------------------------------------------------------------------------------//
FrontEndBoard* UsbBoard::frontEndBoardByUplinkId(int uplinkId)
{
#ifndef NO_QUSB
    for (unsigned int slot = 0; slot < s_uplinksPerUsbBoard; ++slot)
        if (m_frontEndBoards[slot] && m_frontEndBoards[slot]->uplinkId() == uplinkId)
            return m_frontEndBoards[slot];
#endif    
    return 0;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         sampleSize
//Function:     Get sampleSize (= number of clocks) of the readout mode
//Variables:    None
//Returns:      (unsigned short) return sampleSize value
//-------------------------------------------------------------------------------------------------------------------//
unsigned short UsbBoard::sampleSize() const
{
    return Settings::instance()->usbBoardConfiguration(usbBoardId()).sampleSize();
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         setFifoWriteDelay
//Function:     write fifoWriteDelay value into FPGA register (Reg7)
//Variables:    delayValue(int): this value should not be larger than 16 bits
//Returns:      (bool)
//              true = write register successfully
//              false = error
//-------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
bool UsbBoard::setFifoWriteDelay(int delayValue)
{
#ifndef NO_QUSB
    return writeFpgaRegister(0x1007, delayValue);
#else
    return true;
#endif
}
#else
bool UsbBoard::setFifoWriteDelay(int delayValue)
{
#ifndef NO_QUSB
    return writeFpgaRegister(SAMPLE_DELAY_REG, delayValue);
#else
    return true;
#endif
}
#endif

//--------------------------------------------------------------------------------------------------------------------//
//Name:         readData
//Function:     read a given number(=eventsPerAccess) of data from the UsbBoard, store them in a buffer
//Variables:    data(unsigned char*): pointer to a buffer where data values stored
//              checkNumberOfAvailableEvents(bool): true = check before reading data from UsbBoard
//                                                  false = read data directly without checking fifo status
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
int UsbBoard::readData(unsigned char* data, int usbBoardId,uint32_t (& TMPtriggerTimeStamp)[2], bool checkNumberOfAvailableEvents) const
{
#ifndef NO_QUSB
    bool err = false;
    unsigned short fifo_status;
    unsigned short fifo_usedw = 0, fifo_err = 0;

    struct timeval tv;
    fd_set rdfds;
    tv.tv_sec =0;
    tv.tv_usec =50;

    if (checkNumberOfAvailableEvents) {
      //if(0){
        do {   
	  // guido 16.8.2011  force to stop daq when keystroke
          // check key board, if there is an input, break and exit 
	  if(1)  {
            FD_ZERO(&rdfds);
            FD_SET(0,&rdfds);
            int ret = select(1,&rdfds,NULL,NULL,&tv);
            if(ret!=0)  
		return 1;   
     	    }




            fifo_status = readFpgaRegister(0x1003);     
	    //std::cerr<<"wait to read!"<<std::endl;

            fifo_err = (fifo_status >> 14) & 0x0001;
            if (fifo_err){
              //fifo_usedw = fifo_status & 0x07FF;
                fifo_usedw = (fifo_status & 0x07FF)*2; // Increased FIFO size - Olivier 31.10.2016
	       break;
	    }
 
            //fifo_usedw is the number of samples for one uplink, not including the headers!
            //Should be less than 2048. At least this is my understanding now
            
            //fifo_usedw = fifo_status & 0x07FF;
            fifo_usedw = (fifo_status & 0x07FF)*2; // Increased FIFO size - Olivier 31.10.2016
            
//             std::cerr << "Info@UsbBoard::readData   fifo_used = "<<fifo_usedw<<std::endl;
//             std::cerr << "Info@UsbBoard::readData   fifo_flags = ";
//                 for(unsigned int i = 0; i<5; i++)
// 		  std::cerr<<((fifo_status>>(15-i))&0x0001)<<" ";
//                 std::cerr<<std::endl;
	} while (fifo_usedw < Settings::instance()->eventsPerAccess()*sampleSize());
    
      
      // std::cerr << "Sample size is  =" << sampleSize() << '.' << std::endl;
      //  if (fifo_err) {
	  // std::cerr << "The Fifo has encountered an overflow or underflow error: fifo_status  =" << fifo_status <<"@ usedw0"<< fifo_usedw << '.' << std::endl;
          //  assert(false);
	//  }
    }

    unsigned long expectedDataStreamLength = 2 * (2+sampleSize()) * Settings::instance()->eventsPerAccess() * s_uplinksPerUsbBoard;
    unsigned long readDataStreamLength = expectedDataStreamLength;

    if (!QuickUsbReadData(m_handle, data, &readDataStreamLength)) err = true;
//     std::cerr << "readDatalenght="<< readDataStreamLength<<std::endl;
//     getchar();
    unsigned short currentValue;

    // guido 11.2012
      

    for (unsigned int eventNum = 0; eventNum <Settings::instance()->eventsPerAccess(); eventNum++){
        int sID;
	//if(usbBoardId==12)sID=0;
	if(usbBoardId==11)sID=0;
	//if(usbBoardId==12)sID=1;
	if(usbBoardId==15)sID=1;
        // for each event
        uint32_t triggerTimeStamp=0;
        int uplinkSel = 1;
        int eventCounter =  (data[0+(2+sampleSize())*2*uplinkSel+eventNum*(2+sampleSize())*8*2] ) | (data[1+(2+sampleSize())*2*uplinkSel+eventNum*(2+sampleSize())*8*2]<<8);
        if(eventCounter%1500==0){
        // printf("\n");
        for(int uplink = 0; uplink<8; uplink++){
          currentValue =(data[2+(2+sampleSize())*2*uplink+eventNum*(2+sampleSize())*8*2]) | (data[3+(2+sampleSize())*2*uplink+eventNum*(2+sampleSize())*8*2]<<8);
          currentValue &= 0x000F;
          triggerTimeStamp |= currentValue<<(uplink*4);
              }
        int Delta_t=(int)(0x7FFFFFFF&triggerTimeStamp)-(int)(0x7FFFFFFF&(TMPtriggerTimeStamp[sID]));
        int Timestamfordummies=(int)(0x7FFFFFFF&triggerTimeStamp);
 	printf("usbboard =   %d, \t",usbBoardId);
        //printf("Timestamp for dummies =   %d, \t",Timestamfordummies/16);
	printf("Delta T =   %X, \n",Delta_t);
        int TMPTimestamfordummies=(int)(0x7FFFFFFF&(TMPtriggerTimeStamp[sID]));
        //printf("TMPimestamp for dummies =   %d, \t",TMPTimestamfordummies/16);
	TMPtriggerTimeStamp[sID]=triggerTimeStamp;
	//printf("triggerTimeStamp =   0x%08X, \n",triggerTimeStamp);
        }


         }




 // ----- check readout data( only for debug)-------
    //unsigned int channelSel=31;  // give the index of the channel inserted signal

    // unsigned short currentValueforCheck;  // just for debug on 2010.07.03
    
      //for (unsigned int i = 4+(channelSel-5)*2+uplinkSel*(2+sampleSize())*2; i < 4+(channelSel+10)*2+uplinkSel*(2+sampleSize())*2; i=i+2) {  
      // for (unsigned int i = 4; i < 4+66*8; i=i+2) {  
      // for (unsigned int i = uplinkSel*(2+sampleSize())*2; i < uplinkSel*(2+sampleSize())*2+4; i=i+2) {

    for (unsigned int eventNum = 0; eventNum <Settings::instance()->eventsPerAccess(); eventNum++){  
      // for each event 
      int dumpUplink = Settings::instance()-> getDumpUplink() ;
      int firstDumpUplink, lastDumpUplink;
      if (dumpUplink>9){  // dump all
	firstDumpUplink=0;
	lastDumpUplink=8;
      }
      else if (dumpUplink==9){  // don't dump
	firstDumpUplink=dumpUplink;
	lastDumpUplink=dumpUplink;
      }
      else {
	firstDumpUplink=dumpUplink-1; // dump one
	lastDumpUplink=dumpUplink;
      }
        
        for (unsigned int uplinkSel = firstDumpUplink; uplinkSel < lastDumpUplink ; uplinkSel++){
	  // Guido selcect uplink 2 to be displayed
           // for each uplink 
	  // getchar();
#ifdef CHECK_FIRST_FIVE_EVENT
            if(eventNum == 1)   {

                printf("Uplink =%d  (uplink count from 1,2...8) \n",uplinkSel+1);
                printf("//////////eventNum = %d\n",eventNum);
                //getchar();
            }
#else
         printf("Uplink =%d  (uplink count from 1,2...8) \n",uplinkSel+1);
#endif

           // print the two header words
 	 currentValue =(data[0+(2+sampleSize())*2*uplinkSel+eventNum*(2+sampleSize())*8*2] ) | (data[1+(2+sampleSize())*2*uplinkSel+eventNum*(2+sampleSize())*8*2]<<8);
#ifdef CHECK_FIRST_TEN_EVENT
         if(eventNum<10)     printf("Event ID =%4d \n",currentValue);   // only show the first 10 events
#else
 	 printf("Event ID =%4d \n",currentValue);
#endif
         currentValue =(data[2+(2+sampleSize())*2*uplinkSel+eventNum*(2+sampleSize())*8*2]) | (data[3+(2+sampleSize())*2*uplinkSel+eventNum*(2+sampleSize())*8*2]<<8);
#ifdef CHECK_FIRST_TEN_EVENT
         if(eventNum<10)    printf("Header   =%4X \n",currentValue);
#else
            printf("Header   =%4X \n",currentValue);
#endif

           // print ADC data
            for (unsigned int i = 4+uplinkSel*(2+sampleSize())*2+eventNum*(2+sampleSize())*8*2; i < 4+sampleSize()*2+uplinkSel*(2+sampleSize())*2+eventNum*(2+sampleSize())*8*2; i=i+2) { 
                  currentValue =(data[i]) | (data[i+1]<<8);
                //printf("%4X ",currentValue);
                  if (((i-(4+uplinkSel*(2+sampleSize())*2+eventNum*(2+sampleSize())*8*2))%32)==0)  {
#ifdef CHECK_FIRST_TEN_EVENT
                     if(eventNum<10)     printf("\n") ; // only show the first 10 events
#else
                          printf("\n") ;
#endif
                  }
                  currentValue =(data[i]) | (data[i+1]<<8);
          
// 	       // printf("ADCdata[%d]=%4d ",(i-4-uplinkSel*(2+sampleSize())*2+eventNum*(2+sampleSize())*8*2)/2,currentValue);     // print value with its index together

#ifdef CHECK_FIRST_TEN_EVENT
           if(eventNum<10)    printf("%4d ",currentValue);   // only show the first 10 events
#else
 	       printf("%4d ",currentValue);    // print value without index
#endif


	       //check a certain channel signal 
               //if ((currentValue > 0x350)&&(currentValue < 0xa00)) printf("  *signal*");
               //if ( (currentValue > 0x350) && (currentValue < 0xa00) && ((i-4-uplinkSel*(2+sampleSize())*2+eventNum*(2+sampleSize())*8*2)/2!=channelSel) )  printf("  *error in position of the signal*");
               /////////////////////////////////////////////


	       //check readout data 2010.07.03
//  	       if( (i-4-uplinkSel*(2+sampleSize())*2-eventNum*(2+sampleSize())*8*2)/2==0 ) currentValueforCheck = currentValue;
//                if( currentValueforCheck!= currentValue){
// 		  printf("*****error   ");
//                   assert(false);
// 	       }
//                currentValueforCheck++;
/////////////////////////////////////////////////////////////////////////
	    }
#ifdef CHECK_FIRST_TEN_EVENT
           if(eventNum<10)    { // only show the first 10 events
               printf("\n");
               printf("\n");
           }
#else
              printf("\n");
              printf("\n");
#endif
      
          }// end of show read data from all the uplink selected 
	//printf("\n") ; 
	//printf("\n") ; 
	//printf("\n") ; 
    }// end of all the events per access



    fifo_status = readFpgaRegister(0x1003);  
    fifo_err = (fifo_status >> 14) & 0x0001;
    if (fifo_err) {
      	std::cerr << "After read!!!The Fifo has encountered an overflow or underflow error: fifo_status  =" << fifo_status <<"@ usedw0"<< fifo_usedw << '.' << std::endl;
       assert(false);
    }
  
    //    getchar();
 // -----End of check readout data ----------------

    if (expectedDataStreamLength != readDataStreamLength) {
        std::cerr << "Failure: read length error!" << std::endl;
        assert(false);
    }
#endif
    return 0;
}
#else
int UsbBoard::readData(unsigned char* data, bool checkNumberOfAvailableEvents) const
{
#ifndef NO_QUSB
    bool err = false;

    struct timeval tv;
    fd_set rdfds;
    tv.tv_sec =0;
    tv.tv_usec =50;

    printf("----------------readData()-------------------------\n");

    uint32_t fifoCount = getFIFOCount();
    printf("fifoCount = %#x, %d \n",fifoCount,fifoCount);

    if (checkNumberOfAvailableEvents) {

        uint32_t fifo_usedw = 0;
        uint32_t fifoPerAccess = Settings::instance()->eventsPerAccess()*(2+sampleSize())*s_uplinksPerUsbBoard;  // by Snow, since the new usbBoard firmware, the number of FIFO used words includes 2-word headers for each uplink, further more, it is the FIFO count for all uplinks, not only one.
        printf("fifoPerAccess = %d\n",fifoPerAccess);
        uint16_t reg = 0;
//        reg = readFpgaRegister(CLOCK_MODE_BUSY_SET_REG);
//        printf("CLOCK_MODE_BUSY_SET_REG = %#x\n",reg);
        do {
          // guido 16.8.2011  force to stop daq when keystroke
          // check key board, if there is an input, break and exit
          if(1)  {
              FD_ZERO(&rdfds);
              FD_SET(0,&rdfds);
              int ret = select(1,&rdfds,NULL,NULL,&tv);
              if(ret!=0)
                return 1;
          }


//          if(IsFIFOOverFlow() || IsFIFOUnderFlow()) {
//              if(IsFIFOUnderFlow()) printf("fifo underflow....\n");
//              if(IsFIFOOverFlow()) printf("fifo overflow....\n");
//              printf("fifoCount = %#x, %u \n",getFIFOCount());
//              assert(false);
//              break;
//          }


          fifo_usedw = getFIFOCount();
//          printf("getFIFOCount = %d\n",fifo_usedw);
//          getchar();

        } while (fifo_usedw < fifoPerAccess);
    }


    unsigned long expectedDataStreamLength = 2 * (2+sampleSize()) * Settings::instance()->eventsPerAccess() * s_uplinksPerUsbBoard;
    unsigned long readDataStreamLength = expectedDataStreamLength;

    if (!QuickUsbReadData(m_handle, data, &readDataStreamLength)) err = true;
     std::cerr << "readDatalenght="<< readDataStreamLength<<std::endl;
     std::cerr << "expectedReadDatalenght="<< expectedDataStreamLength<<std::endl;
//     getchar();


 // ----- check readout data( only for debug)-------
    //unsigned int channelSel=31;  // give the index of the channel inserted signal
      unsigned short currentValue;

    // unsigned short currentValueforCheck;  // just for debug on 2010.07.03

      //for (unsigned int i = 4+(channelSel-5)*2+uplinkSel*(2+sampleSize())*2; i < 4+(channelSel+10)*2+uplinkSel*(2+sampleSize())*2; i=i+2) {
      // for (unsigned int i = 4; i < 4+66*8; i=i+2) {
      // for (unsigned int i = uplinkSel*(2+sampleSize())*2; i < uplinkSel*(2+sampleSize())*2+4; i=i+2) {




          }




      for (unsigned int eventNum = 0; eventNum <Settings::instance()->eventsPerAccess(); eventNum++){
      // for each event
      int dumpUplink = Settings::instance()-> getDumpUplink() ;
      int firstDumpUplink, lastDumpUplink;
      if (dumpUplink>9){  // dump all
        firstDumpUplink=0;
        lastDumpUplink=8;
      }
      else if (dumpUplink==9){  // don't dump
        firstDumpUplink=dumpUplink;
        lastDumpUplink=dumpUplink;
      }
      else {
        firstDumpUplink=dumpUplink-1; // dump one
        lastDumpUplink=dumpUplink;
      }

        for (unsigned int uplinkSel = firstDumpUplink; uplinkSel < lastDumpUplink ; uplinkSel++){
          // Guido selcect uplink 2 to be displayed
           // for each uplink
          // getchar();
#ifdef CHECK_FIRST_FIVE_EVENT
            if(eventNum == 1)   {

                printf("Uplink =%d  (uplink count from 1,2...8) \n",uplinkSel+1);
                printf("//////////eventNum = %d\n",eventNum);
                //getchar();
            }
#else
         printf("Uplink =%d  (uplink count from 1,2...8) \n",uplinkSel+1);
#endif

           // print the two header words
         currentValue =(data[0+(2+sampleSize())*2*uplinkSel+eventNum*(2+sampleSize())*8*2] ) | (data[1+(2+sampleSize())*2*uplinkSel+eventNum*(2+sampleSize())*8*2]<<8);
#ifdef CHECK_FIRST_TEN_EVENT
         if(eventNum<10)     printf("Event ID =%4d \n",currentValue);   // only show the first 10 events
#else
         printf("Event ID =%4d \n",currentValue);
#endif
         currentValue =(data[2+(2+sampleSize())*2*uplinkSel+eventNum*(2+sampleSize())*8*2]) | (data[3+(2+sampleSize())*2*uplinkSel+eventNum*(2+sampleSize())*8*2]<<8);
#ifdef CHECK_FIRST_TEN_EVENT
         if(eventNum<10)    printf("Header   =%4X \n",currentValue);
#else
            printf("Header   =%4X \n",currentValue);
#endif

           // print ADC data
            for (unsigned int i = 4+uplinkSel*(2+sampleSize())*2+eventNum*(2+sampleSize())*8*2; i < 4+sampleSize()*2+uplinkSel*(2+sampleSize())*2+eventNum*(2+sampleSize())*8*2; i=i+2) {
                  currentValue =(data[i]) | (data[i+1]<<8);
                //printf("%4X ",currentValue);
                  if (((i-(4+uplinkSel*(2+sampleSize())*2+eventNum*(2+sampleSize())*8*2))%32)==0)  {
#ifdef CHECK_FIRST_TEN_EVENT
                     if(eventNum<10)     printf("\n") ; // only show the first 10 events
#else
                          printf("\n") ;
#endif
                  }
                  currentValue =(data[i]) | (data[i+1]<<8);

// 	       // printf("ADCdata[%d]=%4d ",(i-4-uplinkSel*(2+sampleSize())*2+eventNum*(2+sampleSize())*8*2)/2,currentValue);     // print value with its index together

#ifdef CHECK_FIRST_TEN_EVENT
           if(eventNum<10)    printf("%4d ",currentValue);   // only show the first 10 events
#else
               printf("%4d ",currentValue);    // print value without index
#endif


               //check a certain channel signal
               //if ((currentValue > 0x350)&&(currentValue < 0xa00)) printf("  *signal*");
               //if ( (currentValue > 0x350) && (currentValue < 0xa00) && ((i-4-uplinkSel*(2+sampleSize())*2+eventNum*(2+sampleSize())*8*2)/2!=channelSel) )  printf("  *error in position of the signal*");
               /////////////////////////////////////////////


               //check readout data 2010.07.03
//  	       if( (i-4-uplinkSel*(2+sampleSize())*2-eventNum*(2+sampleSize())*8*2)/2==0 ) currentValueforCheck = currentValue;
//                if( currentValueforCheck!= currentValue){
// 		  printf("*****error   ");
//                   assert(false);
// 	       }
//                currentValueforCheck++;
/////////////////////////////////////////////////////////////////////////
            }
#ifdef CHECK_FIRST_TEN_EVENT
           if(eventNum<10)    { // only show the first 10 events
               printf("\n");
               printf("\n");
           }
#else
              printf("\n");
              printf("\n");
#endif

          }// end of show read data from all the uplink selected
        //printf("\n") ;
        //printf("\n") ;
        //printf("\n") ;
    }// end of all the events per access

    if (IsFIFOUnderFlow()) {
        std::cerr << "After read!!!The Fifo has encountered an underflow !\n";
        fifoCount = getFIFOCount();
        printf("fifoCount = %#x, %u \n",fifoCount,fifoCount);
        assert(false);
    }

    if (IsFIFOOverFlow()) {
        std::cerr << "After read!!!The Fifo has encountered an overflow !\n";
        fifoCount = getFIFOCount();
        printf("fifoCount = %#x, %u \n",fifoCount,fifoCount);
       assert(false);
    }

 // -----End of check readout data ----------------

    if (expectedDataStreamLength != readDataStreamLength) {
        std::cerr << "Failure: read length error!" << std::endl;
        assert(false);
    }
#endif
    return 0;
}

#endif
//--------------------------------------------------------------------------------------------------------------------//
//Name:         dumpStatus
//Function:     used for debug. After writing all the config variables to their corresponding registers, operate this function, all the related register values will be displayed
//Variables:    stream(ostream) *ostream objects are stream objects used to write and format ouput as sequences of characters
//Returns:      None
//
//-------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
void UsbBoard::dumpStatus(std::ostream& stream)
{
#ifndef NO_QUSB
    bool err = false;
    stream << "Information: holdDelayTime = " << readFpgaRegister(0x1027) << std::endl;
    stream << "Information: holdType = ";
    for(unsigned int i = 0; i < 8; i++)
        stream << ((readFpgaRegister(0x1028)>>i) & 0x0001) << " ";
    stream << std::endl;
    stream << "Information: delayAfterReadout = " << readFpgaRegister(0x1008) << std::endl;
    stream << "Information: calDelay = " << readFpgaRegister(0x1009) << std::endl;
    stream << "Information: clkSelect = " << (readFpgaRegister(0x1002) & 0x0001) << std::endl;
    stream << "Information: busyOutputSwitch = " << ((readFpgaRegister(0x1002)>>1) & 0x0001) << std::endl;
    stream << "Information: readmode = " << ReadoutModeIdentifier[(readFpgaRegister(0x1000)&0x0007)] << std::endl;
    stream << "Information: triggerMode = " << TriggerModeIdentifier[((readFpgaRegister(0x1000)>>3) & 0x0003)-1] << std::endl;
    stream << "Information: uplinkID = ";
    for(unsigned int i = 0; i < 8; i++)
        stream << ((readFpgaRegister(0x1011+i)>>4) & 0x00FF) << " || ";
    stream << std::endl;
    stream << "Information: fifoWriteDelay = " << readFpgaRegister(0x1007) << std::endl;
    assert(!err);
#endif
}
#else
void UsbBoard::dumpStatus(std::ostream& stream)
{
#ifndef NO_QUSB
    bool err = false;
    stream << "Information: holdDelayTime = " << readFpgaRegister(HOLD_DELAY_TIME_REG) << std::endl;
    stream << "Information: holdType = ";
    for(unsigned int i = 0; i < 8; i++)
        stream << ((readFpgaRegister(HOLD_TYPE_REG)>>i) & 0x0001) << " ";
    stream << std::endl;
    stream << "Information: delayAfterReadout = " << readFpgaRegister(DELAY_AFTER_READOUT_REG) << std::endl;
    stream << "Information: clkSelect = " << (readFpgaRegister(CLOCK_MODE_BUSY_SET_REG) & 0x0001) << std::endl;
    stream << "Information: busyOutputSwitch = " << ((readFpgaRegister(CLOCK_MODE_BUSY_SET_REG)>>1) & 0x0001) << std::endl;
    stream << "Information: readmode = " << ReadoutModeIdentifier[(readFpgaRegister(FE_TYPE_TRIG_MODE_REG)&0x0007)] << std::endl;
#ifndef NEW_USBBOARD
    stream << "Information: triggerMode = " << TriggerModeIdentifier[((readFpgaRegister(FE_TYPE_TRIG_MODE_REG)>>3) & 0x0001)] << std::endl;
#else
    stream << "Information: selfTrigEnable = "<< ((readFpgaRegister(FE_TYPE_TRIG_MODE_REG)>>3)&0x0001)<<std::endl;
    stream << "Information: selfTrigPeriodHigh = "<< readFpgaRegister(SELF_TRIGGER_PERIOD_HIGH_REG)<<std::endl;
    stream << "Information: selfTrigPeriodLow = "<< readFpgaRegister(SELF_TRIGGER_PERIOD_LOW_REG)<<std::endl;
    stream << "Information: selfTrigLeadTimeHigh = "<< readFpgaRegister(SELF_TRIGGER_LEAD_TIME_HIGH_REG)<<std::endl;
    stream << "Information: selfTrigLeadTimeLow = "<< readFpgaRegister(SELF_TRIGGER_LEAD_TIME_LOW_REG)<<std::endl;
#endif
    stream << "Information: setBiasA = " << readFpgaRegister(BIAS_SET_A_REG) << std::endl;
    stream << "Information: setBiasB = " << readFpgaRegister(BIAS_SET_B_REG) << std::endl;
    stream << "Information: uplinkID = ";
    for(unsigned int i = 0; i < 8; i++)
        stream << ((readFpgaRegister(HEADER1_REG+i)>>4) & 0x00FF) << " || ";
    stream << std::endl;
    stream << "Information: fifoWriteDelay = " << readFpgaRegister(SAMPLE_DELAY_REG) << std::endl;
    assert(!err);
#endif
}
#endif
//--------------------------------------------------------------------------------------------------------------------//
//Name:         usbBoardId
//Function:     Get the usbBoard ID by QUSB device serial number
//Variables:    None
//Returns:      (int) usbBoard ID value.
//              if no QUSB device has been detected, return -1
//-------------------------------------------------------------------------------------------------------------------//
int UsbBoard::usbBoardId() const
{
    switch(m_quickUsbSerialNumber) {
#ifndef NEW_USBBOARD
        case 4980: return 1;
        case 4981: return 2;
        case 4982: return 3;
        case 4985: return 6;
        case 4986: return 7;
	case 4988: return 8;
        //case 4990: return 9;
	case 4990: return 15; //Telescope
	case 4991: return 10;
        //case 4991: return 12;
	case 6930: return 12;        
	case 4992: return 11;
        case 4994: return 13;
        case 4995: return 14;
	  //case 4983: return 15;//VBD box
	  //case 6929: return 15; //16;	// Added by Olivier for PEBS SiPMs test
#else
        //for new usbBoard, let's re-arrange the usbBoarrdID

        case 35442: return 1;
        case 35434: return 2;
        case 35444: return 3;
        case 35458: return 4;
#endif
        default:
             return -1;
             break;
    }
    return -1;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         readFpgaRegister
//Function:     read status of the given FPGA register
//Variables:    addr(unsigned short):  address of the register to be read
//Returns:      (unsigned short) : return the value of this register
//Note:         this is a private function for Class UsbBoard
//-------------------------------------------------------------------------------------------------------------------//
unsigned short UsbBoard::readFpgaRegister(unsigned short addr) const
{
#ifndef NO_QUSB
    bool err = false;
    unsigned short temp_short;
    unsigned char temp_array[2];

    temp_array[0] = addr & 0xFF;
    temp_array[1] = (addr>>8) & 0xFF;
    temp_short = 2;
    if (!QuickUsbWriteCommand(m_handle, 0, temp_array, temp_short)) err = true; // register address

    temp_short = 2;
    if (!QuickUsbReadCommand(m_handle, 1, temp_array, &temp_short)) err = true; // register data
    temp_short = temp_array[1];
    temp_short <<= 8;
    temp_short += temp_array[0];

    assert(!err);
    return temp_short;
#else
    return 0;
#endif
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         writeFpgaRegister
//Function:     write data to the given FPGA register
//Variables:    addr(unsigned short):  address of the register to be written
//              data(unsigned short):  data to be written into the given register
//Returns:      (bool)
//              true = write register successfully!
//              false= error exsit!
//Note:         this is a private function for Class UsbBoard
//-------------------------------------------------------------------------------------------------------------------//
bool UsbBoard::writeFpgaRegister(unsigned short addr, unsigned short data) const
{
#ifndef NO_QUSB
    unsigned short temp_short;
    unsigned char temp_array[2];
    bool err = false;
    temp_array[0] = addr & 0xFF;
    temp_array[1] = (addr>>8) & 0xFF;
    temp_short = 2;
    if (!QuickUsbWriteCommand(m_handle, 0, temp_array, temp_short)) err = true; // register address

    temp_array[0] = data & 0xFF;
    temp_array[1] = (data>>8) & 0xFF;
    temp_short = 2;
    if (!QuickUsbWriteCommand(m_handle, 1, temp_array, temp_short)) err = true; // register data

    if(err) printf("writeFpgaRegister(%#x,%#x) failed!\n",addr,data);
    assert(!err);
#endif
    return true;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         fpgaSetReset
//Function:     on the usbBoard, PortA bit0 (PA0) is to reset the FPGA. This function will set this pin PA0 to '1'
//Variables:    None
//Returns:      None
//Note:         this is a private function for Class UsbBoard
//-------------------------------------------------------------------------------------------------------------------//
//Roman: I have no Idea, what this stands for. I took the name from the original software.
//Snow:  answer as above(07.12.2010)
#ifndef NEW_USBBOARD
void UsbBoard::fpgaSetReset() 
{
#ifndef NO_QUSB
    unsigned char temp_char;
    bool err = false;
    if (!QuickUsbReadPortDir(m_handle, 0x0000, &temp_char)) err = true;
    temp_char |= 0x01;
    if (!QuickUsbWritePortDir(m_handle, 0x0000, temp_char)) err = true;
    temp_char = 0x01;
    if (!QuickUsbWritePort(m_handle, 0x0000, &temp_char, 1)) err = true;
    assert(!err);
#endif
}
#else
void UsbBoard::fpgaSetnReset()
{
#ifndef NO_QUSB
    unsigned char temp_char;
    bool err = false;
    if (!QuickUsbReadPortDir(m_handle, 0x0000, &temp_char)) err = true;
    temp_char |= 0x01;
    if (!QuickUsbWritePortDir(m_handle, 0x0000, temp_char)) err = true;

    uint16_t length = 1;
    if(!QuickUsbReadPort(m_handle,0x0000,&temp_char,&length)) err =true;
    temp_char &= 0xFE;
    if (!QuickUsbWritePort(m_handle, 0x0000, &temp_char, 1)) err = true;
    assert(!err);
#endif
}
#endif


//--------------------------------------------------------------------------------------------------------------------//
//Name:         setspiSwitch
//Function:     set switch U50 on USB board: 
//Variables:    value(int): 0 = SS0, 1 = SS1, 2 = SS2, 3 = SS3, 4 = configure FPGA
//              force(bool):true = force to set SpiSwitch with the given value, without any consideration.      false = nothing
//Returns:      (bool)
//              false = error exists
//              true  = set Spi switch successfully
//-------------------------------------------------------------------------------------------------------------------//
//setting of switch U50 on USB board: 0 = SS0, 1 = SS1, 2 = SS2, 3 = SS3, 4 = configure FPGA
#ifndef NEW_USBBOARD
bool UsbBoard::setSpiSwitch(int value, bool force)
{
#ifndef NO_QUSB
    assert(0 <= value && value <= 4);
    bool err = false;
    if (value != m_spiSwitch || value == 4 || force) {
        unsigned short length;
        unsigned char tmp = 0;

        //std::cerr << "UsbBoard:: setSpiSwitch!" << std::endl;
        //set PORTE

        // set PE2 and PE6 to output
        if (!QuickUsbReadPortDir(m_handle, 0x0004, &tmp)) err = true;
        tmp|= 0x44;
        if (!QuickUsbWritePortDir(m_handle, 0x0004, tmp)) err = true;

        // 
        length = 1;
        if (!QuickUsbReadPort(m_handle, 0x0004, &tmp, &length)) err = true;
        if(value ==4){     // For FPGA config mode, set PE2 ='1', PE6 = '1'     // changed!!!
	  tmp  = 0x44;  //should be |= but other GPIOs are not used  Guido 12.6.2011
	}else{
            tmp |= 0x04;   // For SPI mode , set PE2 ='1'   PE6 is used in SPI mode as nSS0, so the
                           // PE6 is only set if nSS0 is accessed with SPI, this is bad but still works
                           // since we only use nSS0 with SPI, Guido 12.6.2011
	}
        if (!QuickUsbWritePort(m_handle, 0x0004, &tmp, 1)) err = true;
        /////////////////////////////////////////////////////////////////

        // set PORTA
        // set port A to output, for each bit 1=output, 0 = input
        if (!QuickUsbWritePortDir(m_handle, 0x0000, 0xFF)) err = true;

        // set port A to the correct functionality
        switch (value) {

            // PA7 high in order to turn on power to the FPGA ( details refer to QUSB user guide)
	    case 0: tmp = 0xBC; break; //0xBC=10111100 SS0, pin 5 on J12       // changed!!!
            case 1: tmp = 0xBA; break; //0xBA=10111010 SS1, pin 7 on J12       // changed!!! 
            case 2: tmp = 0xB6; break; //0xB6=10110110 SS2, pin 6 on J12       // changed!!! 
            case 3: tmp = 0xAE; break; //0xAE=10101110 SS3, pin 8 on J12       // changed!!!
            case 4: tmp = 0xDF; break; //0xDF=11011111 for FPGA configuratiozn
        }
        if (!QuickUsbWritePort(m_handle, 0x00, &tmp, 1)) err = true;

        if (err) {
            m_spiSwitch = -1;
            std::cerr << "Failure: Could not set up switch U50 to mode " << value << "." << std::endl;
            assert(false);
            return false;
        } else {
            m_spiSwitch = value;
            return true;
        }
      }
#endif
    return true;
}
#else
bool UsbBoard::setSpiSwitch(int value, bool force)
{
#ifndef NO_QUSB
    assert(0 <= value && value <= 4);
    bool err = false;
    if (value != m_spiSwitch || value == 4 || force) {
        unsigned short length;
        unsigned char tmp = 0;

        //std::cerr << "UsbBoard:: setSpiSwitch!" << std::endl;
        //set PORTE

        // set PE2 and PE6 to output
//        if (!QuickUsbReadPortDir(m_handle, 0x0004, &tmp)) err = true;
//        tmp|= 0x44;
//        if (!QuickUsbWritePortDir(m_handle, 0x0004, tmp)) err = true;

//        //
//        length = 1;
//        if (!QuickUsbReadPort(m_handle, 0x0004, &tmp, &length)) err = true;
//        if(value ==4){     // For FPGA config mode, set PE2 ='1', PE6 = '1'     // changed!!!
//          tmp  = 0x44;  //should be |= but other GPIOs are not used  Guido 12.6.2011
//        }else{
//            tmp |= 0x04;   // For SPI mode , set PE2 ='1'   PE6 is used in SPI mode as nSS0, so the
//                           // PE6 is only set if nSS0 is accessed with SPI, this is bad but still works
//                           // since we only use nSS0 with SPI, Guido 12.6.2011
//        }
//        if (!QuickUsbWritePort(m_handle, 0x0004, &tmp, 1)) err = true;
        /////////////////////////////////////////////////////////////////

        // set PORTA
        // set port A to output, for each bit 1=output, 0 = input

        if(!QuickUsbReadPortDir(m_handle,0x0000,&tmp)) err = true;
        if(err) printf("read portA dir falied!\n");
        tmp |= 0x80; // PA7 to be output.
        if (!QuickUsbWritePortDir(m_handle, 0x0000, tmp)) err = true;
        if(err) printf("write portA dir falied!\n");
        // set port A to the correct functionality
        switch (value) {

            // PA7 high in order to turn on power to the FPGA ( details refer to QUSB user guide)
//            case 0: tmp = 0xBC; break; //0xBC=10111100 SS0, pin 5 on J12       // changed!!!
//            case 1: tmp = 0xBA; break; //0xBA=10111010 SS1, pin 7 on J12       // changed!!!
//            case 2: tmp = 0xB6; break; //0xB6=10110110 SS2, pin 6 on J12       // changed!!!
//            case 3: tmp = 0xAE; break; //0xAE=10101110 SS3, pin 8 on J12       // changed!!!
//            case 4: tmp = 0xDF; break; //0xDF=11011111 for FPGA configuratiozn

//            case 0: tmp = 0x3C; break; //0xBC=00111100 SS0, pin 5 on J12       // changed!!!
//            case 1: tmp = 0x3A; break; //0xBA=00111010 SS1, pin 7 on J12       // changed!!!
//            case 2: tmp = 0x36; break; //0xB6=00110110 SS2, pin 6 on J12       // changed!!!
//            case 3: tmp = 0x2E; break; //0xAE=00101110 SS3, pin 8 on J12       // changed!!!
            case 4:
                    length = 1;
                    if(!QuickUsbReadPort(m_handle,0x0000,&tmp,&length)) err = true;
                    if(err) printf("read portA falied!\n");
                    tmp &= 0x7F; //PA7 = 0, for PS mode;
                    break;

        }



        if (err) {
            m_spiSwitch = -1;
            std::cerr << "Failure: Could not set up switch U50 to mode " << value << "." << std::endl;
            assert(false);
            return false;
        } else {
            m_spiSwitch = value;
            return true;
        }
      }
#endif
    return true;
}
#endif
//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiWrite
//Function:     writes SPI data to a given slave (0-3) using the QuickUSB SPI interface
//Variables:    slave(unsigned char):   the SPI device address(nss line) to write to
//              data(unsigned char*):   the data which to be sent via SPI
//              length(unsigned short): length of 'data'
//Returns:      true on success
//-------------------------------------------------------------------------------------------------------------------//
bool UsbBoard::spiWrite(unsigned char slave, unsigned char* data, unsigned short length, int slotNo) {
#ifndef NO_QUSB
    assert(slave <= 3 && length <= 64);
    //std::cerr << "slotNo = "<<slotNo<<"  (slotNo counts from 0) << std::endl;
    assert(slotNo <= 13 && slotNo >= 0);
    bool err = false;
    unsigned char address = DEVICE_BASE_ADDRESS + slotNo;
    bool withAdapter = Settings::instance()->usbBoardConfiguration(usbBoardId()).withUplinkAdapter();
    if (!setSpiSwitch(slave)) err = true;
    if(withAdapter){
        if(!QuickUsbWriteSpi(m_handle, 0, &address, 1))
            err = true;
    }
    if (!QuickUsbWriteSpi(m_handle, 0, data, length)) err = true;
    // only device 0 can work because it sets nSS0 (of QUsb module) to 0 which is needed to access nSS0..3 
    // of the SPI on USB board 12.6.2011 Guido 
    if (err) {
        std::cout << "Failure: Could not write SPI for slave " << slave << "." << std::endl;
        assert(false);
        return false;
    }
#endif
    return true;
}




//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiRead
//Function:     reads SPI data from a given slave (0-3) using QuickUSB SPI interface
//Variables:    slave(unsigned char):   the SPI device address(nss line) to read from
//              data(unsigned char*):   the data which is read from SPI
//              length(unsigned short): length of 'data'
//Returns:      true on success
//-------------------------------------------------------------------------------------------------------------------//
bool UsbBoard::spiRead(unsigned char slave, unsigned char* data, unsigned short length, int slotNo) {
#ifndef NO_QUSB
    assert(slave <= 3 && length <= 64);
    assert(slotNo <= 13 && slotNo >= 0);
    bool err = false;
    unsigned char address = DEVICE_BASE_ADDRESS + slotNo;
    bool withAdapter = Settings::instance()->usbBoardConfiguration(usbBoardId()).withUplinkAdapter();
    if (!setSpiSwitch(slave)) err = true;
    unsigned short readLength = length;
    if(withAdapter){
        if(!QuickUsbWriteSpi(m_handle, 0, &address, 1))
            err = true;
    }
    if (!QuickUsbReadSpi(m_handle, 0, data, &readLength)) err = true;
    if (readLength != length) err = true;
    if (err) {
        std::cout << "Failure: Could not read SPI for slave " << slave << "." << std::endl;
        assert(false);
        return false;
    }
#endif
    return true;
}

bool UsbBoard::spiWriteRead(unsigned char slave, unsigned char* data, unsigned short length, int slotNo) {
#ifndef NO_QUSB
    assert(slave <= 3 && length <= 64);
    assert(slotNo <= 13 && slotNo >= 0);
    bool err = false;
    unsigned char address = DEVICE_BASE_ADDRESS + slotNo;
    bool withAdapter = Settings::instance()->usbBoardConfiguration(usbBoardId()).withUplinkAdapter();
    if (!setSpiSwitch(slave)) err = true;
    unsigned short readLength = length;
    if(withAdapter){
        if(!QuickUsbWriteSpi(m_handle, 0, &address, 1))
            err = true;
    }
    if (!QuickUsbWriteReadSpi(m_handle, 0, data, readLength)) err = true;
    //if (readLength != length) err = true; //the QuickUSB manual is inconsistent with the QuickUSB code.
    if (err) {
        std::cout << "Failure: Could not write|read SPI for slave " << slave << "." << std::endl;
        assert(false);
        return false;
    }
#endif
    return true;
}



bool UsbBoard::spiWrite2Cycles(unsigned char slave, unsigned char* data1, unsigned short length1, unsigned char* data2, unsigned short length2, int slotNo){
#ifndef NO_QUSB 
  assert(slave <= 3 && length1 <= 64 && length2 <= 64);
    assert(slotNo <= 13 && slotNo >= 0);
    bool err = false;
    unsigned char address = DEVICE_BASE_ADDRESS_TWO_CYCLES + slotNo;
    bool withAdapter = Settings::instance()->usbBoardConfiguration(usbBoardId()).withUplinkAdapter();
    if (!setSpiSwitch(slave)) err = true;
    if(withAdapter){
        if(!QuickUsbWriteSpi(m_handle, 0, &address, 1))
            err = true;
    }
    if (!QuickUsbWriteSpi(m_handle, 0, data1, length1)) err = true;
    if (!QuickUsbWriteSpi(m_handle, 0, data2, length2)) err = true;
    if (err) {
        std::cout << "Failure: Could not write SPI for slave " << slave << "." << std::endl;
        assert(false);
        return false;
    }
#endif
    return true;
}

bool UsbBoard::spiRead2Cycles(unsigned char slave, unsigned char* data1, unsigned short length1, unsigned char* data2, unsigned short length2, int slotNo){
#ifndef NO_QUSB
    assert(slave <= 3 && length1 <= 64 && length2 <= 64);
    assert(slotNo <= 13 && slotNo >= 0);
    bool err = false;
    unsigned char address = DEVICE_BASE_ADDRESS_TWO_CYCLES + slotNo;
    bool withAdapter = Settings::instance()->usbBoardConfiguration(usbBoardId()).withUplinkAdapter();
    if (!setSpiSwitch(slave)) err = true;
    unsigned short readLength1 = length1, readLength2 = length2;
    if(withAdapter){
        if(!QuickUsbWriteSpi(m_handle, 0, &address, 1))
            err = true;
    }
    if (!QuickUsbReadSpi(m_handle, 0, data1, &readLength1)) err = true;
    if (!QuickUsbReadSpi(m_handle, 0, data2, &readLength2)) err = true;
    if (readLength1 != length1 || readLength2 != length2) err = true;
    if (err) {
        std::cout << "Failure: Could not read SPI for slave " << slave << "." << std::endl;
        assert(false);
        return false;
    }
    return true;
#endif
}

bool UsbBoard::spiWriteRead2Cycles(unsigned char slave, unsigned char* data1, unsigned short length1, unsigned char* data2, unsigned short length2,int slotNo){
#ifndef NO_QUSB
    assert(slave <= 3 && length1 <= 64 && length2 <= 64);
    assert(slotNo <= 13 && slotNo >= 0);
    bool err = false;
    unsigned char address = DEVICE_BASE_ADDRESS_TWO_CYCLES + slotNo;
    bool withAdapter = Settings::instance()->usbBoardConfiguration(usbBoardId()).withUplinkAdapter();
    if (!setSpiSwitch(slave)) err = true;
    unsigned short readLength1 = length1, readLength2 = length2;
    if(withAdapter){
        if(!QuickUsbWriteSpi(m_handle, 0, &address, 1))
            err = true;
    }
    if (!QuickUsbWriteReadSpi(m_handle, 0, data1, readLength1)) err = true;
    if (!QuickUsbWriteReadSpi(m_handle, 0, data2, readLength2)) err = true;
    //if (readLength1 != length1 || readLength2 != length2) err = true; //the QuickUSB manual is inconsistent with the QuickUSB code.
    if (err) {
        std::cout << "Failure: Could not write|read SPI for slave " << slave << "." << std::endl;
        assert(false);
        return false;
    }
#endif
    return true;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         powerOnBias
//Function:     power on/off bias x/y
//Variables:    device(unsigned char): x/X : only power on X
//                                     y/Y : only power on Y
//                                     b/B : power on both X and Y
//                                     f/F : power off both X and Y
//Returns:      true on success
//-------------------------------------------------------------------------------------------------------------------//
#ifndef NEW_USBBOARD
bool UsbBoard::powerOnBias(unsigned char device){
    bool success = true;
#ifndef NO_QUSB
    bool withAdapter = Settings::instance()->usbBoardConfiguration(usbBoardId()).withUplinkAdapter();
    if(!withAdapter){
        std::cout << "There is no uplink adapter, no bias module!" << std::endl;
        return false;
    }
    unsigned char slotX = 9, slotY = 11;
    unsigned char dataX[2], dataY[2];
    switch(device) {
    case 'x' :
        ;
    case 'X' :
        dataX[0] = 0x8F;
        dataX[1] = 0xFF;
        dataY[0] = 0x80;
        dataY[1] = 0x00;
        break;
    case 'y' :
        ;
    case 'Y' :
        dataX[0] = 0x80;
        dataX[1] = 0x00;
        dataY[0] = 0x8F;
        dataY[1] = 0xFF;
        break;
    case 'b' :
        ;
    case 'B' :
        dataX[0] = 0x8F;
        dataX[1] = 0xFF;
        dataY[0] = 0x8F;
        dataY[1] = 0xFF;
        break;
    case 'f' :
        ;
    case 'F' :
        dataX[0] = 0x80;
        dataX[1] = 0x00;
        dataY[0] = 0x80;
        dataY[1] = 0x00;
    }


    success &= spiWrite(0, dataX, 2, slotX);
    success &= spiWrite(0, dataY, 2, slotY);
#endif
    return success;
}
#else
bool UsbBoard::powerOnBias(unsigned char device){
#ifndef NO_QUSB
    switch(device) {
    case 'x' :
        ;
    case 'X' :
        _isBiasAOn = true;
        if(SwitchOffBiasB()){
            _isBiasBOn = false;
            return true;
        }else{
            return false;
        }
        break;
    case 'y' :
        ;
    case 'Y' :
        _isBiasBOn = true;
        if(SwitchOffBiasA()){
            _isBiasAOn = false;
            return true;
        }else{
            return false;
        }
        break;
    case 'b' :
        ;
    case 'B' :
        _isBiasAOn = true;
        _isBiasBOn = true;
        return true;
        break;
    case 'f' :
        ;
    case 'F' :
        if(SwitchOffBiasA()){
            _isBiasAOn = false;
        }else{
            return false;
        }

        if(SwitchOffBiasB()){
            _isBiasBOn = false;
            return true;
        }else{
            return false;
        }

    }
#endif

}
#endif

bool UsbBoard::setBiasVoltage(float voltageX, float voltageY){
    bool withAdapter = Settings::instance()->usbBoardConfiguration(usbBoardId()).withUplinkAdapter();
    if(!withAdapter){
        std::cout << "There is no uplink adapter, no bias module!" << std::endl;
        return false;
    }
    bool success = true;

   // float Xcala = 0.4326, Ycala = 0.4034;
   // float Xcalb = 51.03, Ycalb = 35.501;

   // changed for Adapter cards A2 with r18,r28 removed
   // so biasX and biasY are set directly by DA(AD5322) -----by Snow 2011.12.07 @EPFL
    float Xcala = 0.8252, Ycala = 0.8244;
    float Xcalb = 14.9914, Ycalb = 14.7348;
    unsigned short daValueX = 0, daValueY = 0;
    unsigned char dataX[2], dataY[2];
    unsigned char slotX = 9, slotY = 11;
    if(voltageX<70.0 || voltageX>94.0 || voltageY<50.0 || voltageY>75.0){
        std::cout << "The required bias votage is out of range, it should be between 40.0V and 99.9V" << std::endl;
        //assert(false);
    }


    float voltageX_temp, voltageY_temp;
    voltageX_temp = (voltageX-Xcalb)/Xcala;
    voltageY_temp = (voltageY-Ycalb)/Ycala;

    float valueX = 1000*(100-voltageX_temp)/15.8;
    float valueY = 1000*(100-voltageY_temp)/15.8;

    daValueX = 1000*(100-voltageX_temp)/15.8;
    daValueY = 1000*(100-voltageY_temp)/15.8;

    daValueX = 1000*(100-voltageX)/15.8;
    daValueY = 1000*(100-voltageY)/15.8;
    printf("biasY is dirctly set by DA(AD5322),daValueX = %d, daValueY = %d\n",daValueX,daValueY);



    if(valueX-daValueX >0.5 || valueX-daValueX == 0.5) daValueX++;
    if(valueY-daValueY >0.5 || valueY-daValueY == 0.5) daValueY++;

    printf("valueX = %.4f    daValueX = %d\n",valueX,daValueX);
    printf("valueY = %.4f    daValueY = %d\n",valueY,daValueY);


    //daValueX = cala*1000*(100-voltageX)/15.8 + calb;
    //daValueY = cala*1000*(100-voltageY)/15.8 + calb;
    //float voltageX_temp, voltageY_temp;
    //voltageX_temp = (voltageX-calb)/cala;
    //voltageY_temp = (voltageY-calb)/cala;
    //daValueX = 1000*(100-voltageX)/15.8;
    //daValueY = 1000*(100-voltageY)/15.8;

    dataX[0] = (daValueX>>8)&0x0F;
    dataX[1] = (daValueX)&0xFF;
    dataY[0] = (daValueY>>8)&0x0F;
    dataY[1] = (daValueY)&0xFF;


    success &= spiWrite(0,dataX,2,slotX);


    success &= spiWrite(0,dataY,2,slotY);


    while(0) {
        dataY[0] = 0x33;
        dataY[1] = 0x33;
        spiWrite(0,dataY,2,slotY);
        //getchar();
    }
    //while(1) spiWrite(0,dataY,2,slotY);

    return success;

}

int UsbBoard::GetUplinkID(int uplinkIndex){
    const UsbBoardConfiguration& config = Settings::instance()->usbBoardConfiguration(usbBoardId());
    return config.uplinkId(uplinkIndex);
}

////////////////all the new functions defined for NEW_USBBoard
#ifdef NEW_USBBOARD

bool UsbBoard::SetPSMode(){
#ifndef NO_QUSB
    bool err = false;
    unsigned char temp_char;
    if (!QuickUsbReadPortDir(m_handle, 0x0000, &temp_char)) err = true;
    printf("read PORTA dir = %#x\n",temp_char);
    temp_char |= 0x80;
    if (!QuickUsbWritePortDir(m_handle, 0x0000, temp_char)) err = true;
    if (!QuickUsbReadPortDir(m_handle, 0x0000, &temp_char)) err = true;
    printf("PORT_A dir =%#x\n",temp_char);
    printf("set portA dir\n");
    getchar();

    uint16_t length = 1;
    if(!QuickUsbReadPort(m_handle,0x0000,&temp_char,&length)) err =true;
    printf("read PORTA = %#x\n",temp_char);
    temp_char &= 0x7F;  //PA7 = 1
    if (!QuickUsbWritePort(m_handle, 0x0000, &temp_char, 1)) err = true;
//    if(!QuickUsbReadPort(m_handle,0x0000,&temp_char,&length)) err =true;
//    printf("after set PA7 = 1, PORTA=%#x\n",temp_char);
//    getchar();
    return !err;
#else
    return true;
#endif

}

// bool UsbBoard::ClearFpganReset(){
// #ifndef NO_QUSB
//     unsigned char temp_char;
//     bool err = false;
//     if (!QuickUsbReadPortDir(m_handle, 0x0000, &temp_char)) err = true;
//     temp_char |= 0x01;
//     if (!QuickUsbWritePortDir(m_handle, 0x0000, temp_char)) err = true;

//     uint16_t length = 1;
//     if(!QuickUsbReadPort(m_handle,0x0000,&temp_char,&length)) err =true;
//     temp_char |= 0x01;
//     if (!QuickUsbWritePort(m_handle, 0x0000, &temp_char, 1)) err = true;
//     return !err;
// #else
//     return true;
// #endif
// }

bool UsbBoard::ReadQueuedSpi(uint16_t* readData, uint16_t cmdStartAdr, uint16_t length){

#ifdef DUMP_DEBUG
     printf("bool USBBoard::ReadQueuedSpi(uint16_t* readData, uint16_t cmdStartAdr, uint16_t length)\n");
#endif
     if(length>1024){
         fprintf(stdout,"USBBoard::ReadQueuedSpi(uint16_t*, uint16_t , uint16_t)---length=%d, it is out of range!! length should be less than 1024\n",length);
         return false;
     }

     bool error = false;



     /* Check whether in repeat mode */
     uint16_t  reg = 0;
     bool formerRepeat = false;
     uint16_t  startAdr_repeat = 0;
     uint16_t  endAdr_repeat = 0;
     uint32_t  repeatPeriod = 0;


     reg = readFpgaRegister(QSPI_START_ADDRESS_REG);

     if((reg&QSPI_REPEAT_MODE_ENABLE)) {
         reg &= ~QSPI_REPEAT_MODE_ENABLE; //stop repeat mode
         error |=!writeFpgaRegister(QSPI_START_ADDRESS_REG,reg);
         formerRepeat =true;
         if(error){
            fprintf(stdout,"USBBoard::ReadQueuedSpi(uint16_t*, uint16_t , uint16_t)---Stop repeat mode failed!\n");
         }
         /*save startAddress ,endAddress and repeatPeriod for repeat mode */
         startAdr_repeat = readFpgaRegister(QSPI_START_ADDRESS_REG);
         startAdr_repeat &= QSPI_START_ADDRESS_MASK;

         endAdr_repeat = readFpgaRegister(QSPI_END_ADDRESS_REG);
         endAdr_repeat &= QSPI_END_ADDRESS_MASK;

         reg = readFpgaRegister(QSPI_REPEAT_PERIOD_HIGH_REG);
         repeatPeriod = reg << 16;
         reg = readFpgaRegister(QSPI_REPEAT_PERIOD_LOW_REG);
         repeatPeriod |= reg;


         //wait for repeat mode to be finished!
         do{
             reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
         }while((reg & QSPI_ENABLE));
     }

     /* write SEND_DATA_RAM */
     for(uint16_t i=0;i<length;++i){
         error |= !writeFpgaRAM(cmdStartAdr-QSPI_BLOCK_SIZE+i,0);
         if(error){
             fprintf(stdout,"USBBoard::ReadQueuedSpi(uint16_t*, uint16_t , uint16_t)---writeFpgaRAM(cmdStartAdr-0x400+i(@%#x),0) failed!\n",cmdStartAdr-0x400+i);
         }
     }

     error |= ! writeFpgaRegister(QSPI_START_ADDRESS_REG,(cmdStartAdr-QSPI_CMD_RAM_BASE_ADR) & QSPI_START_ADDRESS_MASK);
     if(error){
         fprintf(stdout,"USBBoard::ReadQueuedSpi(uint16_t*, uint16_t , uint16_t)---writeFPGARegister(START_ADR_RAM,cmdStartAdr(@%#x)) failed!\n",cmdStartAdr & QSPI_START_ADDRESS_MASK);
     }


     error |= ! writeFpgaRegister(QSPI_END_ADDRESS_REG,(cmdStartAdr-QSPI_CMD_RAM_BASE_ADR+length-1) & QSPI_END_ADDRESS_MASK);
     if(error){

         fprintf(stdout,"USBBoard::ReadQueuedSpi(uint16_t*, uint16_t , uint16_t)---writeFPGARegister(END_ADR_RAM,cmdStartAdr+length-1(@%#x)) failed!\n",cmdStartAdr+length-1);
     }

     reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
     reg |= QSPI_ENABLE;
     error |= ! writeFpgaRegister(QSPI_START_ADDRESS_REG,reg);
     if(error){
         fprintf(stdout,"USBBoard::ReadQueuedSpi(uint16_t*, uint16_t , uint16_t)---start QueuedSPI failed!\n");
     }

     /* wait for QSPI transmission finished! */
     do{
        reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
     }while(reg & QSPI_ENABLE);  //wait until QSPI_SPE = 0

     /* readback RECEIVE_DATA_RAM */
     for(uint16_t i=0;i<length;++i){
         error |=!readFpgaRAM(cmdStartAdr+QSPI_BLOCK_SIZE+i,readData[i]);
     }
     if(error){
         fprintf(stdout,"USBBoard::ReadQueuedSpi(uint16_t*, uint16_t , uint16_t)---readFpgaRAM() failed!\n");
     }
     //if former in repeat mode, restart repeat mode
     if(formerRepeat){
        error |=!StartRepeatQueuedSpi(startAdr_repeat,endAdr_repeat,repeatPeriod);
        if(error){
            fprintf(stdout,"USBBoard::ReadQueuedSpi(uint16_t*, uint16_t , uint16_t)---StartRepeatQspi() failed!\n");
        }
     }

     return(!error);

 }

bool UsbBoard::WriteQueuedSpi(uint16_t* writeData, uint16_t cmdStartAdr, uint16_t length){
#ifdef DUMP_DEBUG
     printf("\nbool USBBoard::WriteQueuedSpi(uint16_t* writeData, uint16_t cmdStartAdr, uint16_t length)\n");
#endif
    if(length>1024){
        fprintf(stdout,"USBBoard::WriteQueuedSpi(uint16_t*, uint16_t, uint16_t)---length=%d, it is out of range!! length should be less than 1024\n",length);
        return false;
    }

    bool error = false;

    /* Check whether in repeat mode */
    uint16_t  reg = 0;
    bool formerRepeat = false;
    uint16_t  startAdr_repeat = 0;
    uint16_t  endAdr_repeat = 0;
    uint32_t  repeatPeriod = 0;


    reg = readFpgaRegister(QSPI_START_ADDRESS_REG);

    if((reg&QSPI_REPEAT_MODE_ENABLE)) {
        reg &= ~QSPI_REPEAT_MODE_ENABLE; //stop repeat mode
        error |=!writeFpgaRegister(QSPI_START_ADDRESS_REG,reg);
        formerRepeat =true;
        if(error){
           fprintf(stdout,"USBBoard::WriteQueuedSpi(uint16_t*, uint16_t, uint16_t)---stop repeat mode failed!\n");
        }
        /*save startAddress ,endAddress and repeatPeriod for repeat mode */
        startAdr_repeat= readFpgaRegister(QSPI_START_ADDRESS_REG);
        startAdr_repeat &= QSPI_START_ADDRESS_MASK;

        endAdr_repeat=readFpgaRegister(QSPI_END_ADDRESS_REG);
        endAdr_repeat &= QSPI_END_ADDRESS_MASK;

        reg = readFpgaRegister(QSPI_REPEAT_PERIOD_HIGH_REG);
        repeatPeriod = reg << 16;
        reg = readFpgaRegister(QSPI_REPEAT_PERIOD_LOW_REG);
        repeatPeriod |= reg;


        //wait for repeat mode to be finished!
        do{
            reg =readFpgaRegister(QSPI_START_ADDRESS_REG);
        }while((reg & QSPI_ENABLE));

        if(error){
           fprintf(stdout,"USBBoard::WriteQueuedSpi(uint16_t*, uint16_t, uint16_t)---wait for repeat mode to be finished failed!\n");
        }
    }

    /* write SEND_DATA_RAM */
    for(uint16_t i=0;i<length;++i){
       error |= ! writeFpgaRAM(cmdStartAdr-QSPI_BLOCK_SIZE+i,writeData[i]);
       readFpgaRAM(cmdStartAdr-QSPI_BLOCK_SIZE+i,reg);
      // printf("writeData[%d] = %#x, readback data = %#x    @RAM_adr = %#x\n",i,writeData[i],reg,cmdStartAdr-QSPI_BLOCK_SIZE+i);
      // getchar();

        if(error){
            fprintf(stdout,"USBBoard::WriteQueuedSpi(uint16_t*, uint16_t, uint16_t)---writeFpgaRAM(cmdStartAdr-0x400+i(@%#x) ,writeData[%d]) failed!\n",cmdStartAdr-QSPI_BLOCK_SIZE+i,i);
        }
    }

   error |= ! writeFpgaRegister(QSPI_START_ADDRESS_REG,cmdStartAdr-QSPI_CMD_RAM_BASE_ADR);
    if(error){
        fprintf(stdout,"USBBoard::WriteQueuedSpi(uint16_t*, uint16_t, uint16_t)---write QSPI_START_ADDRESS failed!\n");
    }


   error |= ! writeFpgaRegister(QSPI_END_ADDRESS_REG,cmdStartAdr-QSPI_CMD_RAM_BASE_ADR+length-1);
    if(error){
        fprintf(stdout,"USBBoard::WriteQueuedSpi(uint16_t*, uint16_t, uint16_t)---write QSPI_END_ADDRESS failed!\n");
    }

    /* Start QSPI transmission*/
    reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
    if(error){
        fprintf(stdout,"USBBoard::WriteQueuedSpi(uint16_t*, uint16_t, uint16_t)---read QSPI_START_ADDRESS_REG failed!\n");
    }

    reg |= QSPI_ENABLE;
    error |= ! writeFpgaRegister(QSPI_START_ADDRESS_REG,reg);
    if(error){
        fprintf(stdout,"USBBoard::WriteQueuedSpi(uint16_t*, uint16_t, uint16_t)---start Queued SPI failed\n");
    }

    /* wait for QSPI transmission finished! */
    do{
        reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
    }while(reg & QSPI_ENABLE);

    //if former in repeat mode, restart repeat mode
    if(formerRepeat){
       error |=!StartRepeatQueuedSpi(startAdr_repeat,endAdr_repeat,repeatPeriod);
       if(error){
           fprintf(stdout,"USBBoard::WriteQueuedSpi(uint16_t*, uint16_t, uint16_t)---StartRepeatQueuedSpi() failed!\n");
       }
    }

    return(!error);
}


bool UsbBoard::StartRepeatQueuedSpi(uint16_t* sendData,uint16_t cmdStartAdr, uint16_t length,uint32_t repeatPeriod){
    bool error = false;
    uint16_t reg =0 ;

    error |= !SetQueuedSpiRepeatPeriod(repeatPeriod);
    //set Repeat enable bit
    reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
    reg |= QSPI_ENABLE;
    error |= !writeFpgaRegister(QSPI_START_ADDRESS_REG,reg);

    //set QSPI_START_ADDRESS
    reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
    reg &= ~QSPI_START_ADDRESS_MASK;
    reg |= cmdStartAdr-QSPI_CMD_RAM_BASE_ADR;
    error |= !writeFpgaRegister(QSPI_START_ADDRESS_REG,reg);

    //set QSPI_END_ADDRESS
    reg = !readFpgaRegister(QSPI_END_ADDRESS_REG);
    reg &= ~QSPI_END_ADDRESS_MASK;
    reg |= cmdStartAdr+length-1-QSPI_CMD_RAM_BASE_ADR;
    error |= !writeFpgaRegister(QSPI_END_ADDRESS_REG,reg);

    //write qspi send data
    /* write SEND_DATA_RAM */
    for(uint16_t i=0;i<length;++i){
       error |= ! writeFpgaRAM(cmdStartAdr-QSPI_BLOCK_SIZE+i,sendData[i]);
        if(error){
            fprintf(stdout,"UsbBoard::StartRepeatQueuedSpi(uint16_t* ,uint16_t, uint16_t,uint32_t)---writeFpgaRAM(cmdStartAdr-0x400+i(@%#x) ,sendData[%d]) failed!\n",cmdStartAdr-0x400+i,i);
        }
    }

    //set start QSPI bit
    reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
    reg |= QSPI_ENABLE;
    error |= !writeFpgaRegister(QSPI_START_ADDRESS_REG,reg);

    if(error){
        fprintf(stdout,"UsbBoard::StartRepeatQueuedSpi(uint16_t* ,uint16_t, uint16_t,uint32_t)--- start Queued SPI failed!\n");
    }

    return(!error);
}

bool UsbBoard::StartRepeatQueuedSpi(uint16_t cmdStartAdr, uint16_t cmdEndAdr,uint32_t repeatPeriod){
    bool error = false;
    uint16_t reg =0 ;

    error |= !SetQueuedSpiRepeatPeriod(repeatPeriod);
    //set Repeat enable bit
    reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
    reg |= QSPI_REPEAT_MODE_ENABLE;
    error |= !writeFpgaRegister(QSPI_START_ADDRESS_REG,reg);

    //set QSPI_START_ADDRESS
    reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
    reg &= ~QSPI_START_ADDRESS_MASK;
    reg |= cmdStartAdr-QSPI_CMD_RAM_BASE_ADR;
    error |= !writeFpgaRegister(QSPI_START_ADDRESS_REG,reg);

    //set QSPI_END_ADDRESS
    reg = readFpgaRegister(QSPI_END_ADDRESS_REG);
    reg &= ~QSPI_END_ADDRESS_MASK;
    reg |= cmdEndAdr-QSPI_CMD_RAM_BASE_ADR;
    error |= !writeFpgaRegister(QSPI_END_ADDRESS_REG,reg);

    //set start QSPI bit
    reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
    reg |= QSPI_ENABLE;
    error |= !writeFpgaRegister(QSPI_START_ADDRESS_REG,reg);

    if(error){
        fprintf(stdout,"UsbBoard::StartRepeatQueuedSpi(uint16_t, uint16_t,uint32_t ),start Queued SPI failed!\n");
    }
    if(!error) _qspiRepeat = true;
    return(!error);
}

bool UsbBoard::StopRepeatQueuedSpi(){
    bool error = false;
    uint16_t reg = 0;
    //clear Repeat enable bit
    reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
    reg &= ~QSPI_REPEAT_PERIOD_LOW_REG;
    error |= !writeFpgaRegister(QSPI_START_ADDRESS_REG,reg);
    if(error){
       fprintf(stdout,"UsbBoard::StopRepeatQueuedSpi()---clear repeat bit failed!\n");
       return false;
    }

    //wait for repeat mode to be finished!
    do{
        reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
        printf("QSPI_START_ADDRESS_REG = %#x\n",reg);
    }while((reg & QSPI_ENABLE));

    if(error) return false;
    else{
        _qspiRepeat = false;
        return true;
    }
}

bool UsbBoard::SetQueuedSpiMode(uint16_t setQMR){
    bool error = false;

    error |= ! writeFpgaRegister(QSPI_MODE_REG,setQMR);

    uint16_t temp = readFpgaRegister(QSPI_MODE_REG);
    printf("readback QSPI_MODE_REG = %#x\n",temp);
    //getchar();
    if(error){
        fprintf(stdout,"UsbBoard::SetQueuedSpiMode(uint16_t)---writeFPGARegister(QSPI_MODE_REG,setQMR) failed!\n");
    } else{
        _qspiMode = setQMR;
    }
    return(!error);

}

bool UsbBoard::ReadWriteQueuedSpi(uint16_t* writeData, uint16_t cmdStartAdr, uint16_t* readData,uint16_t length){
#ifdef DUMP_DEBUG
    printf(" UsbBoard::ReadWriteQueuedSpi(uint16_t* writeData, uint16_t cmdStartAdr, uint16_t* readData,uint16_t length)\n");
#endif

    if(length>1024){
        fprintf(stdout,"UsbBoard::ReadWriteQueuedSpi(uint16_t*, uint16_t, uint16_t*,uint16_t)---length=%d, it is out of range!! length should be less than 1024\n",length);
        return false;
    }

    bool error = false;

    /* Check whether in repeat mode */
    uint16_t  reg = 0;
    bool formerRepeat = false;
    uint16_t  startAdr_repeat = 0;
    uint16_t  endAdr_repeat = 0;
    uint32_t  repeatPeriod = 0;


    reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
    //printf("RW_QSPI:read QSPI_START_ADDRESS_REG = %#x\n",reg);
    //getchar();

    if((reg&QSPI_REPEAT_MODE_ENABLE)) {
        reg &= ~QSPI_REPEAT_MODE_ENABLE; //stop repeat mode
        error |=!writeFpgaRegister(QSPI_START_ADDRESS_REG,reg);
        formerRepeat =true;
        if(error){
           fprintf(stdout,"UsbBoard::ReadWriteQueuedSpi(uint16_t*, uint16_t, uint16_t*,uint16_t)---stop repeat mode failed!\n");
        }
        /*save startAddress ,endAddress and repeatPeriod for repeat mode */
        startAdr_repeat = readFpgaRegister(QSPI_START_ADDRESS_REG);
        startAdr_repeat &= QSPI_START_ADDRESS_MASK;

        endAdr_repeat = readFpgaRegister(QSPI_END_ADDRESS_REG);
        endAdr_repeat &= QSPI_END_ADDRESS_MASK;

        reg = readFpgaRegister(QSPI_REPEAT_PERIOD_HIGH_REG);
        repeatPeriod = reg << 16;
        reg = readFpgaRegister(QSPI_REPEAT_PERIOD_LOW_REG);
        repeatPeriod |= reg;


        //wait for repeat mode to be finished!
        do{
            reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
        }while((reg & QSPI_ENABLE));

    }


    /* write SEND_DATA_RAM */
    for(uint16_t i=0;i<length;++i){
       error |= ! writeFpgaRAM(cmdStartAdr-QSPI_BLOCK_SIZE+i,writeData[i]);
       //readFpgaRAM(cmdStartAdr-QSPI_BLOCK_SIZE+i,reg);
       //printf("writeData[%d] = %#x, readback data = %#x    @RAM_adr = %#x\n",i,writeData[i],reg,cmdStartAdr-QSPI_BLOCK_SIZE+i);
       //getchar();
        if(error){
            fprintf(stdout,"UsbBoard::ReadWriteQueuedSpi(uint16_t*, uint16_t, uint16_t*,uint16_t)---writeFpgaRAM(%#x,writeData[%d]) failed!\n",cmdStartAdr-QSPI_BLOCK_SIZE+i,i);
        }
    }

   error |= ! writeFpgaRegister(QSPI_START_ADDRESS_REG,cmdStartAdr-QSPI_CMD_RAM_BASE_ADR);
    if(error){
        fprintf(stdout,"UsbBoard::ReadWriteQueuedSpi(uint16_t*, uint16_t, uint16_t*,uint16_t)---writeFPGARegister(START_ADR_RAM,cmdStartAdr(@%#x)) failed!\n",cmdStartAdr);
    }


   error |= ! writeFpgaRegister(QSPI_END_ADDRESS_REG,cmdStartAdr-QSPI_CMD_RAM_BASE_ADR+length-1);
    if(error){
        fprintf(stdout,"UsbBoard::ReadWriteQueuedSpi(uint16_t*, uint16_t, uint16_t*,uint16_t)---writeFPGARegister(END_ADR_RAM,cmdStartAdr+length-1(@%#x)) failed!\n",cmdStartAdr+length-1);
    }


#ifdef DUMP_DEBUG
    reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
    printf("QSPI_START_ADDRESS_REG = %#x\n",reg);
    reg = readFpgaRegister(QSPI_END_ADDRESS_REG);
    printf("QSPI_END_ADDRESS_REG =  %#x\n",reg);
    reg = readFpgaRegister(QSPI_MODE_REG);
    printf("QSPI_MODE_REG =  %#x\n",reg);
    reg = readFpgaRegister(QSPI_DELAY_REG);
    printf("QSPI_DELAY_REG = %#x\n",reg);
#endif


    /* Start QSPI transmission */
   // printf("press to start QSPI transmission.\n");
    //getchar();
    reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
    reg |= QSPI_ENABLE;
    error |= ! writeFpgaRegister(QSPI_START_ADDRESS_REG,reg);
    if(error){
        fprintf(stdout,"UsbBoard::ReadWriteQueuedSpi(uint16_t*, uint16_t, uint16_t*,uint16_t)---start QSPI failed!\n");
    }

    /* wait for QSPI transmission finished! */
    do{
        reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
    }while(reg & QSPI_ENABLE);

    /* readback RECEIVE_DATA_RAM */
    for(uint16_t i=0;i<length;++i){
       error |= ! readFpgaRAM(cmdStartAdr+QSPI_BLOCK_SIZE+i,readData[i]);
        if(error){
            fprintf(stdout,"UsbBoard::ReadWriteQueuedSpi(uint16_t*, uint16_t, uint16_t*,uint16_t)---readFPGARegister(cmdStartAdr+0x0400+i(@%#x),readData[%d]) failed!\n",cmdStartAdr+0x0400+i,i);
        }
    }

    //if former in repeat mode, restart repeat mode
    if(formerRepeat){
       error |=!StartRepeatQueuedSpi(startAdr_repeat,endAdr_repeat,repeatPeriod);
       if(error){
           fprintf(stdout,"UsbBoard::ReadWriteQueuedSpi(uint16_t*, uint16_t, uint16_t*,uint16_t)---StartRepeatQspi() failed!\n");
       }
    }
    return(!error);
}

bool UsbBoard::SetQueuedSpiDelay(uint16_t setQDLR){
    bool error = false;

    error |= ! writeFpgaRegister(QSPI_DELAY_REG,setQDLR);
    if(error){
        fprintf(stdout,"UsbBoard::SetQueuedSpiDelay(uint16_t)---writeFPGARegister(QSPI_DELAY_REG,setQDLR) failed!\n");
    }else{
        _qspiDelay = setQDLR;
    }
    return(!error);
}

bool UsbBoard::SetQueuedSpiRepeatPeriod(uint32_t setPeriod){
    bool error = false;
    uint16_t reg = setPeriod >> 16;
    error |= !writeFpgaRegister(QSPI_REPEAT_PERIOD_HIGH_REG,reg);

    reg = setPeriod;
    error |= ! writeFpgaRegister(QSPI_REPEAT_PERIOD_LOW_REG,reg);
    if(error){
        fprintf(stdout,"UsbBoard::SetQueuedSpiRepeatPeriod(uint32_t)---write QSPI_REPEAT_PERIOD_REG failed!\n");
    }else{
        _qspiRepeatPeriod = setPeriod;
    }
    return(!error);

}

bool UsbBoard::SetQueuedSpiCmdStartAdr(uint16_t cmdStart){
    uint16_t reg = readFpgaRegister(QSPI_START_ADDRESS_REG);
    reg &=0xFC00;
    reg |=(cmdStart&0x03FF);
    if(writeFpgaRegister(QSPI_START_ADDRESS_REG,reg)){
         _qspiCmdStartAdr = cmdStart;
         return true;
    }else{
         return false;
    }

}

uint16_t UsbBoard::CONT_16bits(uint8_t nss){
    return QSPI_CONT|((nss<<QSPI_CS_SHIFT)& QSPI_CS);
}
uint16_t UsbBoard::CONT_8bits(uint8_t nss){
    return QSPI_CONT|((nss<<QSPI_CS_SHIFT)& QSPI_CS)|QSPI_EIGHT_BITS;
}

bool UsbBoard::InitCMDRam(){
    bool error = false;

    //initial CMD_REG_RAM
    for(uint8_t nssx=0;nssx<8;++nssx){
        for(uint8_t i=0;i<CMD_REG_SINGLE_WIDTH_VATA64;++i){
            //printf("initCMDRam:CMD_REG_RAM i = %d\n",i);

               if(i==0){
                   error |= ! writeFpgaRAM(CMD_REG_RAM_BASE_VATA64+nssx*CMD_REG_SINGLE_WIDTH_VATA64+i,CONT_8bits(nssx));
                   if(error){
                       fprintf(stdout,"UsbBoard::InitCMDRam()---writeFpgaRAM(CMD_REG_RAM_BASE+nssx*CMD_REG_SINGLE_WIDTH+i(@%#x) ,CONT_8bits(%d)) failed!\n",CMD_REG_RAM_BASE_VATA64+nssx*CMD_REG_SINGLE_WIDTH_VATA64+i,nssx);
                   }
                   continue;
               }

               error |= ! writeFpgaRAM(CMD_REG_RAM_BASE_VATA64+nssx*CMD_REG_SINGLE_WIDTH_VATA64+i,CONT_16bits(nssx));
               if(error){
                   fprintf(stdout,"UsbBoard::InitCMDRam()---writeFpgaRAM(CMD_REG_RAM_BASE+nssx*CMD_REG_SINGLE_WIDTH+i(@%#x) ,CONT_16bits(%d)) failed!\n",CMD_REG_RAM_BASE_VATA64+nssx*CMD_REG_SINGLE_WIDTH_VATA64+i,nssx);
               }
        }
    }

    //Undo: initial CMD_ADC_RAM
    for(uint8_t nssx=0;nssx<8;++nssx){
        for(uint8_t i=0;i<CMD_ADC_SINGLE_WIDTH_VATA64;++i){
             // printf("initCMDRam:CMD_ADC_RAM i = %d\n",i);
              if(i==0){
                   error |= ! writeFpgaRAM(CMD_ADC_RAM_BASE_VATA64+nssx*CMD_ADC_SINGLE_WIDTH_VATA64+i,CONT_8bits(nssx));
                   if(error){
                        fprintf(stdout,"UsbBoard::InitCMDRam()---writeFPGARAM(CMD_ADC_RAM_BASE+nssx*CMD_ADC_SINGLE_WIDTH+i(@%#x) ,CONT_8bits(%d)) failed!\n",CMD_ADC_RAM_BASE_VATA64+nssx*CMD_ADC_SINGLE_WIDTH_VATA64+i,nssx);
                   }
                   continue;
               }
               error |= ! writeFpgaRAM(CMD_ADC_RAM_BASE_VATA64+nssx*CMD_ADC_SINGLE_WIDTH_VATA64+i,CONT_16bits(nssx));
               if(error){
                   fprintf(stdout,"UsbBoard::InitCMDRam()---writeFPGARAM(CMD_ADC_RAM_BASE+nssx*CMD_ADC_SINGLE_WIDTH+i(@%#x) ,CONT_16bits(%d)) failed!\n",CMD_ADC_RAM_BASE_VATA64+nssx*CMD_ADC_SINGLE_WIDTH_VATA64+i,nssx);
               }
        }
    }

    //inital CMD_UFM_RAM

    for(uint8_t nssx=0;nssx<8;++nssx){
        for(uint8_t i=0;i<CMD_UFM_SINGLE_WIDTH_VATA64;++i){
           // printf("initCMDRam:CMD_UFM_RAM i = %d\n",i);
            if((i==0 )|| (i==2)){
                error |= ! writeFpgaRAM(CMD_UFM_RAM_BASE_VATA64+nssx*CMD_UFM_SINGLE_WIDTH_VATA64+i,CONT_8bits(nssx));
                if(error){
                    fprintf(stdout,"UsbBoard::InitCMDRam()---writeFPGARAM(CMD_UFM_RAM_BASE+nssx*CMD_UFM_SINGLE_WIDTH+i(@%#x) ,CONT_8bits(%d)) failed!\n",CMD_UFM_RAM_BASE_VATA64+nssx*CMD_UFM_SINGLE_WIDTH_VATA64+i,nssx);
                }
                continue;
             }
            error |= ! writeFpgaRAM(CMD_UFM_RAM_BASE_VATA64+nssx*CMD_UFM_SINGLE_WIDTH_VATA64+i,CONT_16bits(nssx));
            if(error){
                fprintf(stdout,"UsbBoard::InitCMDRam()---writeFPGARAM(CMD_UFM_RAM_BASE+nssx*CMD_UFM_SINGLE_WIDTH+i(@%#x) ,CONT_16bits(%d)) failed!\n",CMD_UFM_RAM_BASE_VATA64+nssx*CMD_UFM_SINGLE_WIDTH_VATA64+i,nssx);
            }
        }
    }

    printf("finish CMD initialize\n");

    SetQueuedSpiMode(_qspiMode);
    SetQueuedSpiRepeatPeriod(_qspiRepeatPeriod);

    return(!error);
}

bool UsbBoard::InitQueuedSpi(){
    bool error = false;

    error |= !InitCMDRam();
    printf("initCMDRam()\n");
    error |= !SetQueuedSpiMode(QSPI_MSB_FIRST| BAUD_25K);   //the v1.0 usbBoard can read registers and R/W UFM correctly with this settings.
                                                            //now it also works fine for v1.1 as long as we change R33 = 300 ohm......I still don't know what happened before...  by Snow@2012.5.15
    printf("SetQueuedSpiMode()\n");
    error |= !StopRepeatQueuedSpi();
    printf("StopRepeatQueuedSpi()\n");
    printf("finish InitQueuedSpi\n");
    return(!error);
}

bool UsbBoard::SwitchOffBiasA(){
    return writeFpgaRegister(BIAS_SET_A_REG,0);
}

bool UsbBoard::SwitchOffBiasB(){
    return writeFpgaRegister(BIAS_SET_B_REG,0);
}

bool UsbBoard::SetBiasA(float biasA){
    // biasA = calA*Nadc+calB

    if(biasA>100){
        std::cout<<"BiasA is out of range, it should be less than 100V\n";
        //assert(false);
    }

    float calA = 1;
    float calB = 0;
    float Nadc_temp = (biasA-calB)/calA;
    uint16_t Nadc= (biasA-calB)/calA;
    if((Nadc_temp-Nadc)>0.5) Nadc++;

    return writeFpgaRegister(BIAS_SET_A_REG,Nadc);
}

bool UsbBoard::SetBiasB(float biasB){
    // biasB = calA*Nadc+calB

    if(biasB>100){
        std::cout<<"BiasB is out of range, it should be less than 100V\n";
        //assert(false);
    }

    float calA = 1;
    float calB = 0;
    float Nadc_temp = (biasB-calB)/calA;
    uint16_t Nadc= (biasB-calB)/calA;
    if((Nadc_temp-Nadc)>0.5) Nadc++;

    return writeFpgaRegister(BIAS_SET_B_REG,Nadc);
}

bool UsbBoard::writeFpgaRAM(uint16_t ramAddress, uint16_t data){
    return writeFpgaRegister(ramAddress,data);
}

bool UsbBoard::readFpgaRAM(uint16_t ramAddress, uint16_t &dest){
#ifndef NO_QUSB
    bool err = false;
    unsigned short temp_short;
    unsigned char temp_array[2];

    temp_array[0] = ramAddress & 0xFF;
    temp_array[1] = (ramAddress>>8) & 0xFF;
    temp_short = 2;
    if (!QuickUsbWriteCommand(m_handle, 0, temp_array, temp_short)) err = true; // register address

    temp_short = 2;
    if (!QuickUsbReadCommand(m_handle, 1, temp_array, &temp_short)) err = true; // register data
    temp_short = temp_array[1];
    temp_short <<= 8;
    temp_short += temp_array[0];

    assert(!err);
    dest = temp_short;
    return !err;
#else
    return 0;
#endif

}

uint32_t UsbBoard::getFIFOCount() const{
    uint32_t ret = 0;

    ret = readFpgaRegister(FIFO_STATUS_A_REG)& 0x00000001; //read in the highest bit of FIFO_usedw
    ret <<= 16;
//    printf("first read from FIFO_STATUS_A_REG ret = %#x\n",ret);
    ret += readFpgaRegister(FIFO_STATUS_B_REG);
//    printf("add FIFO_STATUS_B_REG,ret= %#x\n",ret);
//    getchar();
    return ret;
}

bool UsbBoard::IsFIFOFlow() const{
    uint16_t reg = readFpgaRegister(FIFO_STATUS_A_REG);
    if(reg & 0xC000) return true;
    else return false;
}

bool UsbBoard::IsFIFOOverFlow() const{
    uint16_t reg = readFpgaRegister(FIFO_STATUS_A_REG);
    printf("FIFO_STATUS_A_REG = %#x\n",reg);
    if(reg&FIFO_OVERFLOW) return true;
    else return false;
}

bool UsbBoard::IsFIFOUnderFlow() const{
    uint16_t reg = readFpgaRegister(FIFO_STATUS_A_REG);
    printf("FIFO_STATUS_A_REG = %#x\n",reg);
    if(reg&FIFO_UNDERFLOW) return true;
    else return false;
}
#endif    //end of #define NEW_USBBOARD


#ifndef NEW_USBBOARD
bool UsbBoard::SetLedInjectionOn(bool on)
{
#ifndef NO_QUSB
    bool err = false;
    unsigned short registerValue = readFpgaRegister(0x1000);
    if (on) {
        if (!writeFpgaRegister(0x1000, registerValue | 0x0200)) err = true;
    } else {
        if (!writeFpgaRegister(0x1000, registerValue & 0xFDFF)) err = true;
    }
    if (err) {
        assert(false);
        return false;
    }
#endif
    return true;
}

bool UsbBoard::SetHoldDelayTime(unsigned int holdDelayTime){
#ifndef NO_QUSB
    bool err = false;
    if (!writeFpgaRegister(0x1027, holdDelayTime & 0x01FF)) err = true;  // TODO: I don't understand why it's marked here as 0x01FF
    if (err) {
        assert(false);
        return false;
    }
#endif
    return true;

}

bool UsbBoard::SetTriggerType(TriggerMode mode) {
    bool err = false;
#ifndef NO_QUSB
    unsigned short reg0 = readFpgaRegister(0x1000);
    switch (mode) {
        case internal : reg0 = reg0 | 0x0008; break;
        case external : reg0 = reg0 | 0x0010; break;
        case calibration : reg0 = (reg0 & 0xFFE7) | 0x0100; break;
        //case led      : reg0 = (reg0 & 0xFEE7) | 0x0200;break;
        case triggerModeEnd : err = true; break;
    }
    if (!writeFpgaRegister(0x1000, reg0)) err = true;
#endif
    if (err) {
        std::cerr << "Failure: Could not configure trigger type." << std::endl;
        assert(false);
        return false;
    } else {
        return true;
    }
}

#else
bool UsbBoard::SetLedInjectionOn(bool on)
{
#ifndef NO_QUSB
    bool err = false;
    unsigned short registerValue = readFpgaRegister(FE_TYPE_TRIG_MODE_REG);
    if (on) {
        if (!writeFpgaRegister(FE_TYPE_TRIG_MODE_REG, registerValue | LED_INJECTION_MODE_ENABLE)) err = true;
    } else {
        if (!writeFpgaRegister(FE_TYPE_TRIG_MODE_REG, registerValue & LED_INJECTION_MODE_ENABLE)) err = true;
    }
    if (err) {
        assert(false);
        return false;
    }
#endif
    return true;
}

bool UsbBoard::SetHoldDelayTime(unsigned int holdDelayTime){
#ifndef NO_QUSB
    bool err = false;
    if (!writeFpgaRegister(HOLD_DELAY_TIME_REG, holdDelayTime & 0xFFFF)) err = true;
    if (err) {
        assert(false);
        return false;
    }
#endif
    return true;

}
#endif
