#ifndef UsbBoardManager_h
#define UsbBoardManager_h

#include <vector>
#include <string>

#include "Settings.h"
class UsbBoard;
class MergedEvent;
class FrontEndBoard;

class UsbBoardManager {
    public:
        UsbBoardManager();
        ~UsbBoardManager();

        unsigned long eventNumber() const;
       
        bool initUsbBoards();

        FrontEndBoard* frontEndBoard(int uplinkId);
        FrontEndBoard* frontEndBoard(int usbBoardId, int uplinkSlot);

        void resetUsbBoards();

        void resetDaq();

        bool setBusy(bool value = true) const;

        int takeEvents(MergedEvent** event, uint32_t (& TMPtriggerTimeStamp)[2]);

        void runContinuousMode(unsigned int uplinkSlot,unsigned int channel) ; 

        bool readFrontEndBoardsSensors(unsigned short *); //added by lht read sensors on va64 card
        bool readFrontEndBoardsSensors();
   
        void IncreaseLedIntense(unsigned short stepDAC);
        void DecreaseLedIntense(unsigned short stepDAC);

        void readFpgaRegister(unsigned short addr);  //dump register

        void setLedInjectionModeOn(bool on);
        void setHoldDelayTime(unsigned short holdDelayTime);
        void setLedOn(int usbBoardId,int startUplinkSlot,int endUplinkSlot,bool on);
        void setTriggerType(TriggerMode mode);

	//void spiTest();
    private:
        bool configureFpga(bool force = false);
        bool configureFrontEndBoards();
        std::vector<UsbBoard> m_usbBoards;

        unsigned long m_eventNumber;

        unsigned char* m_data;
};

#endif
