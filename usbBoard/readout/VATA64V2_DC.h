#ifndef VATA64V2_DC_h
#define VATA64V2_DC_h

#include "FrontEndBoard.h"

#include <iostream>
#include <vector>
#include <string>

#include "../TypeDefs.h"

// Define
#define  FIRMWARE_VERSION_REG_ADDR       0x0000
#define  GENERAL_CTRL_REG0_ADDR          0x0001        
//bit0:reset_cfg_reg, bit1:start_cfg_reg
#define  GENERAL_CTRL_REG1_ADDR          0x0002     
//bit4:CLK_READ_ctrl, bit5:SRIN_READ_ctrl, bit6:RESETB_READ_ctrl, bit7:HOLDB_Backup_ctrl,
#define  GENERAL_CTRL_REG2_ADDR          0x0003
#define  GENERAL_CTRL_REG3_ADDR          0x0004        
//bit0 : 1 c-code read access ; 0 USB_board read access, bit1:NPWDA, bit2:NPWDB
#define  GENERAL_CTRL_REG4_ADDR          0x0005        // unused
/////////////////////////////////////////////////////////////////////////////////////
////// in SPIROC-II we only use FIRMWARE_VERSION_REG and GENERAL_CTRL_REG0 //////////
//GENERAL_CTRL_REG0:
//bit0: reset_cfg_reg,    bit1: start_cfg_reg,
//bit2: CLK_READ_ctrl, bit3: SRIN_READ_ctrl, bit4: RESETB_READ_ctrl, bit5: HOLDB_Backup_ctrl,
//bit6: 0 c-code read access ; 1 USB_board read access, 
//bit7: NPWDA--Amplifier for Uplink disable, bit8: NPWDB--Amplifier for debug disable,
//bit9: external trigger enable,
//bit10: TESTPULSE_ctrl,
//bit14: SPIROC-II config done, bit15: SPIROC-II config error. these two bits are read only.
/////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// in SPIROC-A we still only use FIRMWARE_VERSION_REG and GENERAL_CTRL_REG0most bit definitions keep the same as SPIROC-II ///////////
///////Most bit definitions keep the same as SPIROC-II, but there are some small differences( marked with **)                  ///////////
////// Just change the name, function keep the same as before( marked with * )                                                 ///////////
/////  change the function ( marked with **)                                                                                   ///////////

//GENERAL_CTRL_REG0:
//bit0: reset_cfg_reg,     bit1: start_cfg_reg,
//bit2: CLK_READ_spi(*)    bit3: SRIN_READ_spi(*)  bit4: RESETB_READ_spi(*)  bit5: HOLDB_Backup_spi(*)
//bit6: select_for_read     0= c-code read access ; 1= USB_board read access, 
//bit7: select_sc_nPROBE(**)
//bit8&9: TRIG_SELECT(**)
//bit10: TESTPULSE_spi(*) 
//bit12: LG_EN_spi(**)
//bit13: HG_EN_spi(**)
//bit14: config done   (read only)
//bit15: config error. (read only)
/////////////////////////////////////////////////////////////////////////////////////

#define  SPIROC_CFG_RAM_BADDR            0x4000
#define  SPIROC_CFG_RB_RAM_BADDR         0x4080

// UFM CMD
#define UFM_CMD_WREN     0x60            //enable write to UFM
#define UFM_CMD_WRDI     0x04            //----disable write to UFM
#define UFM_CMD_RDSR     0x05            //----read status register
#define UFM_CMD_WRSR     0x01            //----write status register
#define UFM_CMD_READ     0x03            //----read data from UFM
#define UFM_CMD_WRITE    0x02            //----write data to UFM
#define UFM_CMD_SERASE   0x40            //----sector erase
#define UFM_CMD_BERASE   0x06            //----erase the entire UFM block(both sectors)

//QUSB SPI switch CMD
#define UFM_ERASE        0xD3
#define UFM_READ_SR      0xB5
#define UFM_WRITE        0x81
#define UFM_READ         0x42
#define UFM_READ_WORD    0x4A
#define SC_REGISTER      0x60


//
//#define  SPItoUFM             0xC3C3
//#define  SPItoREGISTER        0x3C3C

//#define  SPItoADC             0x5050 //by lht July 20, 13:57. For new parallel spi interface, ADC select
#define  SPItoUFM0             0xC3C3 //by cpj for daisy chain
#define  SPItoREGISTER0        0x3C3C

#define  SPItoADC0             0x5050    
#define  SPItoUFM1             0xDBDB
#define  SPItoREGISTER1        0xBDBD

#define  SPItoADC1             0x0505 
struct VATA64V2_DC_CFG_TYPE
{
  int DAC_on;
  int Lg;
  int Rfp_bp;
  int Rfs0_b;
  int Rfs1;
  int Rfs2_b;
  int Lgs;
  int Ssc0_b;
  int Ssc1;
  int Ssc2;
  int Tp50_dc;
  int Bypass;
  int Sel;
  int Shabi_hp1b;
  int Shabi_hp2;
  int Shabi_hp3b;
  int SHsense_en;
  int SHgen_en;
  int Buf_sel;
  int Test_on;
  int Vfp_en;
  int Vfsfclamp_en;
  int reserved;
  int Disable_register[64];
  int Disable_bit_tst_channel;
  int Threshold_alignment_DAC[64];
  int Threshold_alignment_DAC_tst_channel;
  int Preamplifier_input_potential_DAC[64];
  int Preamplifier_input_potential_DAC_tst_channel;
  int Holdbi_Bias_DAC; 
};

                                                                 //\|/ should be 00      channel select      on chip REF     power mode
const static unsigned char CMD_Array1[14] = {0x00,0x00,             //0b00              000                 0               00
                                                                //---normal mode, power on device, select channel 1 as next sample channel
                                            0x08,0x00,           //0b00 001 0 00 ---normal mode, select channel 2 as next sample channel
                                            0x10,0x00,           //0b00 010 0 00 ---normal mode, select channel 3 as next sample channel
                                            0x20,0x00,           //0b00 100 0 00 ---normal mode, select channel 5 as next sample channel
                                            0x28,0x00,           //0b00 101 0 00 ---normal mode, select channel 6 as next sample channel
                                            0x30,0x00,           //0b00 110 0 00 ---normal mode, select channel 7 as next sample channel
                                            0x00,0x00};          //0b00 000 0 10/00  ---auto shutdown/normal mode, shut down after this read out

// -----------------   VATA64V2_DC Configuration  ---------------------
class VATA64V2_DCConfiguration : public FrontEndBoardConfiguration {
    public:
        VATA64V2_DCConfiguration(int slotNo,int uplinkId);
        ~VATA64V2_DCConfiguration();

        void dump(std::ostream& stream = std::cerr) const;
        bool loadFromString(const char*);

/*         int select_nSPI_uplink() const; */
/*         void setSelect_nSPI_uplink(int select_nSPI_uplink); */

        
        int trigSelect() const;
        void setTrigSelect(int trigSelect);

        void setSlowControlCfgFile(const std::string& fileName);
        const std::string& slowControlCfgFile() const;

        unsigned short m_FEchipConfigArray[80];

        bool loadConfig(const std::string& filename,char chain); 
        bool loadFromStream(std::istream& stream);

        void convertCfgToVector(unsigned char temp[456]);

        void writeToStream(std::ostream& stream) const;

        void convertVectorTo16bitsArray(int dumpOn,unsigned char temp[456],unsigned short array[]);

        void chipConfigArray(unsigned short chipCfgArray[],int chipType);

        //  for Analogue Test
        void setPreamplifierInputPortentialDAC(int Value,unsigned short configArray[]);

    private:
        VATA64V2_DC_CFG_TYPE  m_cfgVATA64;
        int m_trigSelect;

        std::string m_slowControlCfgFile;
};


// ---------------- VATA64V2_DC ------------------------------------------------
class VATA64V2_DC : public FrontEndBoard {
    public:
        VATA64V2_DC(UsbBoard*, FrontEndBoardType, int);
        ~VATA64V2_DC();
    
        virtual bool configure(FrontEndBoardConfiguration*);

        virtual bool readSensor(unsigned short*,char chain);       //added for sensor readout on the new vata64 board --by lht

        //void spiToUfmOrRegister(unsigned char portNum,unsigned short data,bool force = false);
        void spiRegisterWrite(unsigned char portNum,unsigned short addr,unsigned short data,char chain);
        unsigned short spiRegisterRead(unsigned char portNum,unsigned short addr,char chain);
        unsigned char spiUfmStatusRead(unsigned char portNum,char chain);
        void spiUfmErase(unsigned char portNum,char chain);
        void spiUfmWrite(unsigned char portNum,unsigned short addr,unsigned short data,char chain);
        unsigned short spiUfmReadWord(unsigned char portNum,unsigned short start_addr,char chain);
        void spiUfmReadBlock(unsigned char portNum,unsigned short start_addr,unsigned short word_num,unsigned short data[512],char chain);
        void selectReadChannel(unsigned int channelIndex,char chain);

        void setCkbSpi(char setValue,char chain);
        void setShiftInbSpi(char setValue,char chain);
        void setDresetSpi(char setValue,char chain);
        void setHoldSpi(char setValue,char chain) ;
        void setSelect_nSPI_uplink(char setValue,char chain);
        void setTestpulseSpi(char setValue,char chain);
        void setNPWD(char setValue,char chain);                //change for new VA card, since the new one have just one NPWD port
        //void setNPWDA(char setValue);
        //void setNPWDB(char setValue);
        //void setNPWDC(char setValue);

        
        void dumpFirmwareVersion(char chain);
        void dumpGeneCtrlReg0(char chain);
        void resetChipConfig(char chain);     
        void setTrigSelect(char trigSelect,char chain);
        bool spiSetSelectScNprobe(char selectScNprobe,char chain); 
        bool compareUfmToConfigArray(int dumpOn,unsigned short chipConfigArray[],unsigned int compareArrayNum,char chain);      
        void writeConfigArrayToUfm(unsigned short chipConfigArray[], unsigned int writeConfigArrayNum,char chain);
        void sendStartConfigCMD(char chain);   
        bool loadConfigToVATA64(char chain);     

        bool spiConfigVATA64(int dumpOn,unsigned short chipConfigArray[],char trigSelect,char chain);

        void analogTest(char chain);
        void setScNreadDebug(char setValue);


	// const static unsigned int s_configArrayNum_spirocA = 29;
        const static unsigned int s_configArrayNum_VATA64 = 55;
        void selectContinuousMode(unsigned int channelIndex,char chain);
    
    private:
        unsigned short m_generalCtrlReg0;
        VATA64V2_DCConfiguration* m_configuration;
        unsigned short m_spiToUfmRegisterSwitch;
};

#endif
