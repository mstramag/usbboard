#ifndef UsbBoard_h
#define UsbBoard_h

#ifndef NO_QUSB
#include <QuickUSB.h>
#else
#define HANDLE void*
#endif

#include <string>
#include <iostream>
#include <stdint.h>

#include "UsbBoardConfiguration.h"
#include "../TypeDefs.h"

#define DEVICE_BASE_ADDRESS 0xC1
#define DEVICE_BASE_ADDRESS_TWO_CYCLES 0xD1




//#define NEW_USBBOARD


class FrontEndBoard;

class UsbBoard {
    public:

        //add by Snow@EPFL,2012.04.25, for new usbBoard
#ifdef NEW_USBBOARD
        enum FPGARegister{
            FE_TYPE_TRIG_MODE_REG = 0x0000,
            RESET_DAQ_REG = 0x0001,
            CLOCK_MODE_BUSY_SET_REG = 0x0002,
            FIFO_STATUS_A_REG = 0x0003,
            FIFO_STATUS_B_REG = 0x0004,
            EVENT_ID_REG = 0x0005,
            FIRMWARE_VERSION_REG = 0x0006,
            SAMPLE_DELAY_REG = 0x0007,
            DELAY_AFTER_READOUT_REG = 0x0008,
            SELF_TRIGGER_LEAD_TIME_HIGH_REG = 0x0009,
            SELF_TRIGGER_LEAD_TIME_LOW_REG = 0x000A,
            HEADER1_REG = 0x000B,
            HEADER2_REG = 0x000C,
            HEADER3_REG = 0x000D,
            HEADER4_REG = 0x000E,
            HEADER5_REG = 0x000F,
            HEADER6_REG = 0x0010,
            HEADER7_REG = 0x0011,
            HEADER8_REG = 0x0012,
            SELF_TRIGGER_PERIOD_HIGH_REG = 0x0013,
            SELF_TRIGGER_PERIOD_LOW_REG = 0x0014,
            QSPI_START_ADDRESS_REG = 0x0015,
            QSPI_END_ADDRESS_REG = 0x0016,
            QSPI_MODE_REG = 0x0017,
            QSPI_DELAY_REG = 0x0018,
            BIAS_SET_A_REG = 0x0019,
            BIAS_SET_B_REG = 0x001A,
            HOLD_DELAY_TIME_REG = 0x001B,
            HOLD_TYPE_REG = 0x001C,
            QSPI_REPEAT_PERIOD_HIGH_REG = 0x001D,
            QSPI_REPEAT_PERIOD_LOW_REG = 0x001E
        };

        enum Control_Reg0{
            SELF_TRIGGER_ENABLE = 0x0008,
            LED_INJECTION_MODE_ENABLE = 0x0200
        };

        enum Control_Reg1{
            RESET_FIFO = 0x0001
        };

        enum Control_Reg2{
            FE_CLOCK_MOD = 0x0001,
            MANUAL_SET_BUSY = 0x0002
        };

        enum FIFO_StatusA{
            FIFO_OVERFLOW = 0x8000,
            FIFO_UNDERFLOW = 0x4000,
            FIFO_ALMOST_FULL = 0x2000,
            FIFO_USED_BIT16 = 0x0001
        };

        enum FIFO_StatusB{
            FIFO_USED_LOW15BITS = 0xFFFF
        };

        enum Control_QSPI_Start_Address_Reg{
            QSPI_ENABLE = 0x8000,
            QSPI_REPEAT_MODE_ENABLE = 0x4000,
            QSPI_START_ADDRESS_MASK = 0x03FF
        };

        enum Control_QSPI_End_Address_Reg{
            QSPI_END_ADDRESS_MASK = 0x03FF
        };

        enum Control_QSPI_Mode_Reg{
            QSPI_MSB_FIRST = 0x8000,
            QSPI_CPOL = 0x4000,
            QSPI_CPHA = 0x2000,
            QSPI_BAUD_MASK = 0x0FFF
        };

        enum QSPI_RAM {
             QSPI_SEND_DATA_RAM_BASE_ADR = 0x0400,
             QSPI_CMD_RAM_BASE_ADR = 0x0800,
             QSPI_RECEIVE_DATA_RAM_BASE_ADR = 0x0C00,
             QSPI_BLOCK_SIZE = 0x0400
        };

        enum Control_QSPI_CMD_RAM{
             QSPI_CONT = 0x8000,
             QSPI_EIGHT_BITS = 0x4000,
             QSPI_DSCK = 0x2000,
             QSPI_DT = 0x1000,
             QSPI_CS = 0x0F00,
             QSPI_CS_SHIFT = 8
        };

        // CMD_RAM is devided into several parts, which is specialized for ECAL VATA64_board .


        enum QSPI_BAUD{
             BAUD_25K = 800,
             BAUD_125K = 160,
             BAUD_500K = 40,
             BAUD_1M = 20,
             BAUD_2M = 10
        };

#endif


        UsbBoard(HANDLE handle);
        ~UsbBoard();

        int usbBoardId() const;

        bool configureFpga(bool force = true);
        bool resetFpga();

        bool configureDaq();
        bool resetDaq() const;
        void runContinuousMode(unsigned int uplinkSlot, unsigned int channel);

        bool configureFrontEndBoards();
        FrontEndBoard* frontEndBoardBySlot(unsigned int slot);
        FrontEndBoard* frontEndBoardByUplinkId(int uplinkId);

        bool setBusy(bool value = true) const;
        bool setFifoWriteDelay(int delayValue);

        int readData(unsigned char* data,int usbBoardId, uint32_t (& TMPtriggerTimeStamp)[2], bool checkNumberOfAvailableEvents = true) const;
        unsigned short sampleSize() const;

        void dumpStatus(std::ostream& stream = std::cerr);

        int quickUsbSerialNumber(bool readFromBoard = false);
        
        bool spiWrite(unsigned char slave, unsigned char* data, unsigned short length, int slotNo);
        bool spiRead(unsigned char slave, unsigned char* data, unsigned short length, int slotNo);
        bool spiWriteRead(unsigned char slave, unsigned char* data, unsigned short length, int slotNo);


        bool spiWrite2Cycles(unsigned char slave, unsigned char* data1, unsigned short length1, unsigned char* data2, unsigned short length2, int slotNo);
        bool spiRead2Cycles(unsigned char slave, unsigned char* data1, unsigned short length1, unsigned char* data2, unsigned short length2, int slotNo);
        bool spiWriteRead2Cycles(unsigned char slave, unsigned char* data1, unsigned short length1, unsigned char* data2, unsigned short length2,int slotNo);

        bool powerOnBias(unsigned char device);
        bool setBiasVoltage(float voltageX, float voltageY);

        int GetUplinkID(int uplinkIndex); // uplinkIndex counted from 1, i.e. uplinkIndex =1 ---> uplink1


        unsigned short readFpgaRegister(unsigned short addr) const;

        bool SetLedInjectionOn(bool on);
        bool SetHoldDelayTime(unsigned int holdDelayTime);
        bool SetTriggerType(TriggerMode mode);

        // add for new USBBoard, since it uses Queued SPI,instead of QUSB SPI module
#ifdef NEW_USBBOARD

        /**
         *@brief generate the word for QSPI_CMD_RAM settings
         *
         *@param nss as uint8_t,counts from 0 to 7, corresponding to uplink1 to uplink8.
         *@return settings to be write into CMD_RAM
         */
        uint16_t CONT_16bits(uint8_t nss);

        uint16_t CONT_8bits(uint8_t nss);



        /**
         * @brief  read by QSPI, i.e.Quene SPI
         *
         * @param readData as uint16_t*,buffer to place the received data
         * @param cmdData as uint16_t*, buffer to place all the commands to be written into CMD_RAM
         * @param length as uint16_t, number of commands to be send
         *
         * @return bool, false if error exists, true if succeed.
         */
         bool ReadQueuedSpi(uint16_t* readData, uint16_t* cmdData, uint16_t length);

         /**
          * @brief  write by QSPI, i.e.Quene SPI
          *
          * @param writeData as uint16_t*,buffer to place the data to be sent
          * @param cmdData as uint16_t*, buffer to place all the commands to be written into CMD_RAM
          * @param length as uint16_t, number of commands to be send
          *
          * @return bool, false if error exists, true if succeed.
          */
          bool WriteQueuedSpi(uint16_t* writeData, uint16_t* cmdData, uint16_t length);

          /**
           * @brief  readWrite by QSPI, i.e.Quene SPI
           *
           * @param writeData as uint16_t*,buffer to place the data to be sent
           * @param cmdData as uint16_t*, buffer to place all the commands to be written into CMD_RAM
           * @param readData as uint16_t*,buffer to place the received data
           * @param length as uint16_t, number of commands to be send
           *
           * @return bool, false if error exists, true if succeed.
           */
           bool ReadWriteQueuedSpi(uint16_t* writeData, uint16_t* cmdData, uint16_t* readData,uint16_t length);

           /**
            * @brief  read by QSPI, i.e.Quene SPI
            *
            * @param readData as uint16_t*,buffer to place the received data
            * @param cmdStartAdr as uint16_t, head of the cmd stack
            * @param length as uint16_t, number of commands to be send
            *
            * @return bool, false if error exists, true if succeed.
            */
            bool ReadQueuedSpi(uint16_t* readData, uint16_t cmdStartAdr, uint16_t length);

            /**
             * @brief  write by QSPI, i.e.Quene SPI
             *
             * @param writeData as uint16_t*,buffer to place the data to be sent
             * @param cmdStartAdr as uint16_t, head of the cmd stack
             * @param length as uint16_t, number of commands to be send
             *
             * @return bool, false if error exists, true if succeed.
             */
            bool WriteQueuedSpi(uint16_t* writeData, uint16_t cmdStartAdr, uint16_t length);

            /**
             * @brief  readWrite by QSPI, i.e.Quene SPI
             *
             * @param writeData as uint16_t*,buffer to place the data to be sent
             * @param cmdStartAdr as uint16_t, head of the cmd stack
             * @param readData as uint16_t*,buffer to place the received data
             * @param length as uint16_t, number of commands to be send
             *
             * @return bool, false if error exists, true if succeed.
             */
            bool ReadWriteQueuedSpi(uint16_t* writeData, uint16_t cmdStartAdr, uint16_t* readData,uint16_t length);

            /**
             * @brief start a loop Qspi , cmd RAM will run from the startAdr to the endAdr and loop itself automaticaly
             *
             * @param sendData as uint16_t *: qspi send data array
             * @param cmdStartAdr as uint16_t : start address of the CMD RAM to be looped
             * @param length as uint16_t : this should not be more than the length of sendData above
             * @param repeatPeriod as uint32_t : qspi loop period

             * @return bool. false if error exists, ture if succeed.
             */
            bool StartRepeatQueuedSpi(uint16_t* sendData,uint16_t cmdStartAdr, uint16_t length, uint32_t repeatPeriod);
            bool StartRepeatQueuedSpi(uint16_t cmdStartAdr, uint16_t cmdEndAdr, uint32_t repeatPeriod);

            /**
             * @brief Stop a loop Qspi
             *
             * @return bool. false if error exists, ture if succeed.
             */
            bool StopRepeatQueuedSpi();

            /**
             * @brief read FPGA register QSPI_START_ADR_REG, check whether QSPI_RPE bit is set or not
             * In order to confirm whether QSPI is in repeat mode or not

             * @return bool. true in repeat mode; false in normal mode
             */
            bool QueuedSpiInRepeat();

            /**
             * @brief set QSPI baud rate
             *
             * @param baudRate in uint16_t: only low 12bits effective.
             * @return bool. false if error exists; true if success
            */
            bool SetQueuedSpiBaud(uint16_t baudRate);

           /**
           * @brief set QSPI Mode register and update _qspiMode
           *
           * @param setQMR as uint16_t, value to set QSPI_MODE_REG

           * @return bool. false if error exists, ture if succeed.
           */
           bool SetQueuedSpiMode(uint16_t setQMR);


           /**
           * @brief set QSPI Delay register and update _qspiDlay
           *
           * @param setQDLR as uint16_t, value to set QSPI_DELAY_REG

           * @return bool. false if error exists, ture if succeed.
           */
           bool SetQueuedSpiDelay(uint16_t setQDLR);

           /**
           * @brief set QSPI repeat period High/Low register and update _qspiRepeatPeriod
           *
           * @param setPeriod as uint32_t, value to set QSPI_REPEAT_PERIOD_HIGH/LOW registers

           * @return bool. false if error exists, ture if succeed.
           */
           bool SetQueuedSpiRepeatPeriod(uint32_t setPeriod);

           /**
           * @brief set _qspiCmdStartAdr, in ortder to set the first address to write QSPI cmd queue

           * @param cmdStart as uint16_t, the first address to write cmd queue

           */
           bool SetQueuedSpiCmdStartAdr(uint16_t cmdStart);

           /**
           * @brief set _qspiCmdStartAdr, in ortder to set the first address to write QSPI cmd queue
           * @return false if error exists, true if success.
           */
           bool InitCMDRam();

           /**
           * @brief initialize QSPI related parameters, including CMD RAM, qspiMode(i.e.baudRate) and stop repeat mode as default
           * @return false if error exists, true if success.
           */
           bool InitQueuedSpi();

           uint16_t GetQSPIMode(){return _qspiMode;}
           uint16_t GetQSPIDelay(){return _qspiDelay;}
           uint16_t GetQSPICmdStartAdr(){return _qspiCmdStartAdr;}
           bool IsQSPIInRepeatMode(){return _qspiRepeat;}
           uint16_t GetQSPIRepeatPeriod(){return _qspiRepeatPeriod;}

           // This is added for new UsbBoard, since the HV chip is controled by registers in the firmware.
           // By setting register BIAS_SET_A_REG / BIAS_SET_B_REG to 0, we switchOff the bias completely.
           /**
           * @brief By setting BIAS_SET_A_REG = 0, set the HV output to be ~40V.
           * @return false if error exists, true if success.
           */
           bool SwitchOffBiasA();

           /**
           * @brief By setting BIAS_SET_B_REG = 0, set the HV output to be ~40V.
           * @return false if error exists, true if success.
           */
           bool SwitchOffBiasB();

           /**
           * @brief set bias voltage A.For the new usbBoard, this is done by setting a corresponding register in the firmware BIAS_SET_A_REG
           * @return false if error exists, true if success.
           */
           bool SetBiasA(float biasA);

           /**
           * @brief set bias voltage B.For the new usbBoard, this is done by setting a corresponding register in the firmware BIAS_SET_B_REG
           * @return false if error exists, true if success.
           */
           bool SetBiasB(float biasB);

           /**
            *@brief set PA7 to be output pin and PA7 = 0, in order to select PS mode for FPGA configuration
            *@return false if error exists, ture if success.
            */
           bool SetPSMode();


           /**
            *@brief set PA0 to be output pin and PA0 = 0, in order to clear the RESET_FPGA.
            *@return false if error exists, ture if success.
            */
           bool ClearFpgaReset();

           //add for new usbBoard by Snow
           uint32_t getFIFOCount() const;
           bool IsFIFOUnderFlow() const;
           bool IsFIFOOverFlow() const;
           bool IsFIFOFlow()const;  //in this case , either FIFO_UNDERFLOW or FIFO_OVERFLOW




#endif

    private:

        bool writeFpgaRegister(unsigned short addr, unsigned short data) const;
        bool writeFpgaRAM(uint16_t ramAddress, uint16_t data);
        bool readFpgaRAM(uint16_t ramAddress, uint16_t& dest);
        
        bool setSpiSwitch(int value, bool force = false);
#ifndef NEW_USBBOARD
        void fpgaSetReset();
#else
        void fpgaSetnReset();
#endif




        HANDLE m_handle;

        int m_quickUsbSerialNumber;
        FrontEndBoard* m_frontEndBoards[s_uplinksPerUsbBoard];
        int m_spiSwitch;

#ifdef NEW_USBBOARD
        uint16_t _qspiMode;
        uint16_t _qspiDelay;
        uint16_t _qspiCmdStartAdr;
        bool _qspiRepeat;
        uint16_t _qspiRepeatPeriod;
        bool _isBiasAOn;
        bool _isBiasBOn;

#endif
};

#endif
