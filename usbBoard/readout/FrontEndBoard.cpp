#include "FrontEndBoard.h"
#include "UsbBoard.h"

//---------------------------     FrontEndBoardData   ------------------------------------------

FrontEndBoardData::FrontEndBoardData()
{}

FrontEndBoardData::~FrontEndBoardData()
{}
        
void FrontEndBoardData::dump(std::ostream& stream) const
{
    stream
        << "Type" << m_type
        << std::endl;
}

//---------------------------  FrontEndBoardConfiguration  ---------------------------------------

FrontEndBoardConfiguration::FrontEndBoardConfiguration(FrontEndBoardType type, int slotNo, int uplinkId)
    : m_type(type)
    , m_slotNo(slotNo)
    , m_uplinkId(uplinkId)
{}

FrontEndBoardConfiguration::~FrontEndBoardConfiguration()
{}

//---------------------------        FrontEndBoard     -------------------------------------------

FrontEndBoard::FrontEndBoard(UsbBoard* board, FrontEndBoardType type, int slotNo)
    : m_type(type)
    , m_board(board)
    , m_slotNo(slotNo)
{
    m_uplinkId = m_board->GetUplinkID(slotNo+1);
}

FrontEndBoard::~FrontEndBoard()
{}

bool FrontEndBoard::read(unsigned char slave, unsigned char* data, unsigned short length)
{
    bool noErr = true;
    //unsigned char deviceAddr;
    //deviceAddr = DEVICE_BASE_ADDRESS + m_slotNo;
    //noErr = noErr & m_board->spiWrite(slave,&deviceAddr,1);
    noErr = noErr & m_board->spiRead(slave, data, length, m_slotNo);
    //getchar();
    return noErr;
}

bool FrontEndBoard::write(unsigned char slave, unsigned char* data, unsigned short length)
{
    bool noErr = true;
    //unsigned char deviceAddr;
    //deviceAddr = DEVICE_BASE_ADDRESS + m_slotNo;
    //noErr = noErr & m_board->spiWrite(slave, &deviceAddr, 1);
    noErr = noErr & m_board->spiWrite(slave, data, length, m_slotNo);
    //getchar();
    return noErr;
}

bool FrontEndBoard::writeRead(unsigned char slave, unsigned char* data, unsigned short length)
{
    bool noErr = true;
    //unsigned char deviceAddr;
    //deviceAddr = DEVICE_BASE_ADDRESS + m_slotNo;
    //noErr = noErr & m_board->spiWrite(slave, &deviceAddr, 1);
    noErr = noErr & m_board->spiWriteRead(slave, data, length, m_slotNo);
    //getchar();
    return noErr;
}

bool FrontEndBoard::read2Cycles(unsigned char slave, unsigned char* data1, unsigned short length1, unsigned char* data2, unsigned short length2)
{
    bool noErr = true;
    //unsigned char deviceAddr;
    //deviceAddr = DEVICE_BASE_ADDRESS_TWO_CYCLES + m_slotNo;
    //noErr = noErr & m_board->spiWrite(slave, &deviceAddr, 1);
    noErr = noErr & m_board->spiRead2Cycles(slave, data1, length1, data2, length2,m_slotNo);
    //noErr = noErr & m_board->spiRead(slave, data2, length2);
    return noErr;
}

bool FrontEndBoard::write2Cycles(unsigned char slave, unsigned char* data1, unsigned short length1, unsigned char* data2, unsigned short length2)
{
    bool noErr = true;
   // unsigned char deviceAddr;
   //deviceAddr = DEVICE_BASE_ADDRESS_TWO_CYCLES + m_slotNo;
   // noErr = noErr & m_board->spiWrite(slave, &deviceAddr, 1);
    noErr = noErr & m_board->spiWrite2Cycles(slave, data1, length1, data2, length2, m_slotNo);
    //noErr = noErr & m_board->spiWrite(slave, data2, length2);
    return noErr;
}

bool FrontEndBoard::writeRead2Cycles(unsigned char slave, unsigned char* data1, unsigned short length1, unsigned char* data2, unsigned short length2)
{
    bool noErr = true;
    //unsigned char deviceAddr;
    //deviceAddr = DEVICE_BASE_ADDRESS_TWO_CYCLES + m_slotNo;
    //noErr = noErr & m_board->spiWrite(slave, &deviceAddr, 1);
    noErr = noErr & m_board->spiWriteRead2Cycles(slave, data1, length1, data2,length2, m_slotNo);
    //noErr = noErr & m_board->spiWriteRead(slave, data2, length2);
    return noErr;
}


#ifdef NEW_USBBOARD
bool FrontEndBoard::readQSPI(uint16_t* readData, uint16_t cmdStarAdr, uint16_t length){
     return m_board->ReadQueuedSpi(readData,cmdStarAdr,length);
}

bool FrontEndBoard::writeQSPI(uint16_t* writeData, uint16_t cmdStarAdr, uint16_t length){
     return m_board->WriteQueuedSpi(writeData,cmdStarAdr,length);
}

bool FrontEndBoard::readWriteQSPI(uint16_t* writeData, uint16_t cmdStarAdr, uint16_t* readData,uint16_t length){
     return m_board->ReadWriteQueuedSpi(writeData,cmdStarAdr,readData,length);

}
#endif
