#ifndef EcalSpirocII_h
#define ECalSpirocII_h

#include "FrontEndBoard.h"

#include <iostream>
#include <vector>
#include <string>
#include <unistd.h>

#include "../TypeDefs.h"

// Define
#define  FIRMWARE_VERSION_REG_ADDR       0x0000
#define  GENERAL_CTRL_REG0_ADDR          0x0001        
//bit0:reset_cfg_reg, bit1:start_cfg_reg
#define  GENERAL_CTRL_REG1_ADDR          0x0002     
//bit4:CLK_READ_ctrl, bit5:SRIN_READ_ctrl, bit6:RESETB_READ_ctrl, bit7:HOLDB_Backup_ctrl,
#define  GENERAL_CTRL_REG2_ADDR          0x0003
#define  GENERAL_CTRL_REG3_ADDR          0x0004        
//bit0 : 1 c-code read access ; 0 USB_board read access, bit1:NPWDA, bit2:NPWDB
#define  GENERAL_CTRL_REG4_ADDR          0x0005        // unused
/////////////////////////////////////////////////////////////////////////////////////
////// in SPIROC-II we only use FIRMWARE_VERSION_REG and GENERAL_CTRL_REG0 //////////
//GENERAL_CTRL_REG0:
//bit0: reset_cfg_reg,    bit1: start_cfg_reg,
//bit2: CLK_READ_ctrl, bit3: SRIN_READ_ctrl, bit4: RESETB_READ_ctrl, bit5: HOLDB_Backup_ctrl,
//bit6: 0 c-code read access ; 1 USB_board read access, 
//bit7: NPWDA--Amplifier for Uplink disable, bit8: NPWDB--Amplifier for debug disable,
//bit9: external trigger enable,
//bit10: TESTPULSE_ctrl,
//bit14: SPIROC-II config done, bit15: SPIROC-II config error. these two bits are read only.
/////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// in SPIROC-A we still only use FIRMWARE_VERSION_REG and GENERAL_CTRL_REG0most bit definitions keep the same as SPIROC-II ///////////
///////Most bit definitions keep the same as SPIROC-II, but there are some small differences( marked with **)                  ///////////
////// Just change the name, function keep the same as before( marked with * )                                                 ///////////
/////  change the function ( marked with **)                                                                                   ///////////

//GENERAL_CTRL_REG0:
//bit0: reset_cfg_reg,     bit1: start_cfg_reg,
//bit2: CLK_READ_spi(*)    bit3: SRIN_READ_spi(*)  bit4: RESETB_READ_spi(*)  bit5: HOLDB_Backup_spi(*)
//bit6: select_for_read     0= c-code read access ; 1= USB_board read access, 
//bit7: select_sc_nPROBE(**)
//bit8&9: TRIG_SELECT(**)
//bit10: TESTPULSE_spi(*) 
//bit12: LG_EN_spi(**)
//bit13: HG_EN_spi(**)
//bit14: config done   (read only)
//bit15: config error. (read only)
/////////////////////////////////////////////////////////////////////////////////////

#define  SPIROC_CFG_RAM_BADDR            0x4000
#define  SPIROC_CFG_RB_RAM_BADDR         0x4080

// UFM CMD
#define UFM_CMD_WREN     0x60            //enable write to UFM
#define UFM_CMD_WRDI     0x04            //----disable write to UFM
#define UFM_CMD_RDSR     0x05            //----read status register
#define UFM_CMD_WRSR     0x01            //----write status register
#define UFM_CMD_READ     0x03            //----read data from UFM
#define UFM_CMD_WRITE    0x02            //----write data to UFM
#define UFM_CMD_SERASE   0x40            //----sector erase
#define UFM_CMD_BERASE   0x06            //----erase the entire UFM block(both sectors)

//QUSB SPI switch CMD
#define UFM_ERASE        0xD3
#define UFM_READ_SR      0xB5
#define UFM_WRITE        0x81
#define UFM_READ         0x42
#define UFM_READ_WORD    0x4A
#define SC_REGISTER      0x60

//
#define  SPIROC_ON_PORTA                     0xBE // PA6='0' and PA5 ='1' ,   
#define  SPIROC_ON_PORTE                     0x04 // PE6='0' and pE2 ='1'
#define  SPIROC_channel_select               0xBD // 0xBD - nSS3,0xBB - nSS4,0xB7 - nSS5,0xAF - nSS6      PA0 <=> reset
#define  SPIROC_portA_dir                    0xFF
#define  SPIROC_portE_dir                    0x44

//
#define  SPItoUFM             0xC3C3
#define  SPItoREGISTER        0x3C3C


struct SPIROC_II_CFG_TYPE
{
    int sw_trig_ext;
    int sw_flag_tdc_ext;
    int sw_ramp_adc_ext;
    int sw_ramp_tdc_ext;
    int adc_res_register;
    int chip_ID_register;
    int EN_ChipSat;
    int EN_TransmitOn;
    int EN_RamFull;
    int EN_OutSerie;
    int bypass_chip;
    int resolution_adc;
    int pwr_on_adc_ramp_current_source;
    int pwr_on_adc_ramp_integrator;
    int input_dac_8bits[36];
    int input_dac_off[36];
    int sw_pa_comp_hg;
    int sw_pa_cf_hgb;
    int sw_pa_cf_lgb;
    int sw_pa_comp_lg;
    int pwr_on_pa;
    int sw_pa_disable[36];
    int sw_pa_ctest_enable[36];
    int pwr_on_suiv_ssh_lg;
    int sw_rc_lg;
    int pwr_on_ssh_lg;
    int pwr_on_suiv_ssh_hg;
    int sw_rc_hg;
    int pwr_on_ssh_hg;
    int pwr_on_suiv_fs;
    int pwr_on_fs;
    int sw_sca_backup;
    int pwr_on_sca;
    int dac1;
    int dac2;
    int pwr_on_dual_dac;
    int pwr_on_ota_dual_dac;
    int pwr_on_bandgap;
    int pwr_on_delay_start_ramp_tdc;
    int delay_start_ramp_tdc;
    int sw_ramp_slope;
    int pwr_on_tdc_ramp;
    int pwr_on_discri_adc;
    int pwr_on_discri_gs;
    int sw_auto_gain_select;
    int gain_select;
    int sw_in_adc_ext;
    int sw_tdc_on;
    int mask_discri[36];
    int cmd_hold_ext;
    int pwr_on_delay_discri;
    int delay_discri;
    int dac_4_bit_theshold_adjust[36];
    int sw_fine_adjust;
    int pwr_on_dac4b;
    int pwr_on_discri;
    int pwr_on_delay_ValidHold;
    int delay_ValidHold;
    int pwr_on_delay_RstColumn;
    int delay_RstColumn;
};


// -----------------   EcalSpirocII Configuration  ---------------------
class EcalSpirocIIConfiguration : public FrontEndBoardConfiguration {
    public:
        EcalSpirocIIConfiguration(int slotNo, int uplinkId);
        ~EcalSpirocIIConfiguration();

        void dump(std::ostream& stream = std::cerr) const;
        bool loadFromString(const char*);

	// int feChipConfigType() const;
	// void setFeChipConfigType(int fechipConfigType);

	// int switchFeChipConfig() const;
	// void setSwitchFeChipConfig(int switchFeChipConfig);

        int selectForRead() const;
        void setSelectForRead(int selectForRead);

        int selectScNprobe() const;
        void setSelectScNprobe(int selectScNprobe);
        
        int trigSelect() const;
        void setTrigSelect(int trigSelect);

        void setSlowControlCfgFile(const std::string& fileName);
        const std::string& slowControlCfgFile() const;

        unsigned short m_FEchipConfigArray[80];

        bool loadConfig(const std::string& filename); 
        bool loadFromStream(std::istream& stream);
     

        void convertCfgToVector(unsigned char temp[702]);

        void writeToStream(std::ostream& stream) const;

        void convertVectorTo16bitsArray(int dumpOn,unsigned char temp[702],unsigned short array[]);


        void chipConfigArray(unsigned short chipCfgArray[],int chipType);

        //  for Analogue Test
        void setDACandRegenerateArray(int Value,unsigned short configArray[]);

    private:
        SPIROC_II_CFG_TYPE m_cfgSpirocII;
        //unsigned short m_array[100];

        int m_selectForRead;
        int m_selectScNprobe;
        int m_trigSelect;

        std::string m_slowControlCfgFile;
};


// ---------------- EcalSpirocII ------------------------------------------------
class EcalSpirocII : public FrontEndBoard {
    public:
        EcalSpirocII(UsbBoard*, FrontEndBoardType, int);
        ~EcalSpirocII();
    
        virtual bool configure(FrontEndBoardConfiguration*);

        void spiToUfmOrRegister(unsigned char portNum,unsigned short data,bool force = false);
        void spiRegisterWrite(unsigned char portNum,unsigned short addr,unsigned short data);
        unsigned short spiRegisterRead(unsigned char portNum,unsigned short addr);
        unsigned char spiUfmStatusRead(unsigned char portNum);
        void spiUfmErase(unsigned char portNum);
        void spiUfmWrite(unsigned char portNum,unsigned short addr,unsigned short data);
        unsigned short spiUfmReadWord(unsigned char portNum,unsigned short start_addr);
        void spiUfmReadBlock(unsigned char portNum,unsigned short start_addr,unsigned short word_num,unsigned short data[512]);
        void selectReadChannel(unsigned int channelIndex);

        void setClkReadSpi(char setValue);
        void setSrinReadSpi(char setValue);
        void setResetbReadSpi(char setValue);
        void setResetbScSpi(char setValue);
        void setHoldbSpi(char setValue) ;
        void setSelectForRead(char setValue);
        void setTestpulseSpi(char setValue);
        void setLgEnSpi(char setValue);
        
        void dumpFirmwareVersion();
        void dumpGeneCtrlReg0();
        void resetChipConfig();   
        bool spiSetTrigSelect(char trigSelect);
        bool compareUfmToConfigArray(unsigned short chipConfigArray[],unsigned int compareArrayNum);      
        void writeConfigArrayToUfm(unsigned short chipConfigArray[], unsigned int writeConfigArrayNum);
        void sendStartConfigCMD();   
        bool loadConfigToSpiroc();     

        bool spiConfigSpirocII(int dumpOn,unsigned short chipConfigArray[],char trigSelect, char select_read);

        void spirocAnalogTest();
        void setScNreadDebug(char setValue);


        const static unsigned int s_configArrayNum_spirocII = 44;

        void selectContinuousMode(unsigned int channelIndex);
    
    private:
        unsigned short m_generalCtrlReg0;
        EcalSpirocIIConfiguration* m_configuration;
        unsigned short m_spiToUfmRegisterSwitch;
};

#endif
