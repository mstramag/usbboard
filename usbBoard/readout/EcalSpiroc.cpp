#include "EcalSpiroc.h"
#include "Settings.h"

#include <assert.h>
#include <string>
#include <limits.h>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <sstream>
#include <sys/stat.h> 

EcalSpirocConfiguration::EcalSpirocConfiguration(int slotNo,int uplinkId)
  : FrontEndBoardConfiguration(EcalSpirocType,slotNo,uplinkId)
    , m_selectForRead(-1)
    , m_selectScNprobe(-1)
    , m_trigSelect(-1)
    , m_slowControlCfgFile("")
{
    std::cerr
        << "EcalSpirocConfiguration::EcalSpirocConfiguration() called."
        <<  std::endl;
}

EcalSpirocConfiguration::~EcalSpirocConfiguration()
{}

void EcalSpirocConfiguration::dump(std::ostream& stream) const
{
    std::cerr
        << "EcalSpirocConfiguration::dump(std::ostream& stream) called."
        <<  std::endl;
    stream
        << "spirocA:selectForRead || " << m_selectForRead << " || " << std::endl
        << "spirocA:selectScNprobe || " << m_selectScNprobe << " || " << std::endl
        << "spirocA:trigSelect || " << m_trigSelect << " || " << std::endl
        << "spirocA:pathToSlowControlCfgFile || " << m_slowControlCfgFile << std::endl;
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadFromString
//Function:     extract useful infomation for spirocA configuration from the usbBoard config file 
//Variables:    searchSting(const char*):one line read from usbBoard config file          
//Returns:      (bool)
//              true = the given line is for spirocA configuration, read & store it successfully
//              false= the given line does't belong to spirocA configuration
//-------------------------------------------------------------------------------------------------------------------//
bool EcalSpirocConfiguration::loadFromString(const char* searchString)
{
      char cString[100];
      bool getline = false;       
      std::cerr
        << "EcalSpirocConfiguration::loadFromString(const char*) called."
        <<  std::endl;
    
      if(sscanf(searchString, "spirocA:selectForRead || %d ||", &m_selectForRead)==1) getline =true;
       
      if(sscanf(searchString, "spirocA:selectScNprobe || %d ||", &m_selectScNprobe)==1) getline =true;
      
      if(sscanf(searchString, "spirocA:trigSelect || %d ||", &m_trigSelect)==1)  getline =true;
        
      if(sscanf(searchString, "spirocA:pathToSlowControlCfgFile || %[^\n]", cString) == 1) {
            m_slowControlCfgFile = cString;
            getline = true;
      }

    return getline;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadConfig
//Function:     read spirocA slow control config file, store all the settings to struct SPIROC_A_CFG_TYPE 
//Variables:    filePath(string): path to the slow contro config file of spirocA            
//Returns:      (bool) return error value of this process.
//              true = error exist!
//              false= execute successfully!
//-------------------------------------------------------------------------------------------------------------------//  
bool EcalSpirocConfiguration::loadConfig(const std::string& filePath) {  

    std::stringstream stream;
    stream << filePath ;             //get config file name
    std::cerr << "Information: Loading configuration from file " << stream.str() << std::endl;
    std::ifstream fileStream(stream.str().c_str());      // open config file 
    // "ifstream" provides an interface to read data from files as input streams

    bool err= false;
    if (!fileStream){
          std::cerr << "Failure: coudn't open file " << stream.str()<< ".\n";
          assert(false);
     }    

    std::cerr << "Choose SpirocA, reading Config file..."<<std::endl;
    err = loadFromStream(fileStream);                           // load config file, store all the config settings in struct SPIROC_A_CFG_TYPE ( defined in TypeDefs.h )

    return(err);
}






//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadFromStream
//Function:     extract config values from stream, and store them in m_cfgSpirocA ( struct SPIROC_A_CFG_TYPE ( defined in TypeDefs.h ))
//              m_cfgSpirocA is a private variable defined in Class EcalSpirocConfiguration
//Variables:    stream(istream): path to the slow control config file of spirocA     
//                              (istream objects are stream objects used to read and interpret input from sequences of characters)      
//Returns:      (bool) return error value of this process.
//              true = error exist!
//              false= execute successfully!
//-------------------------------------------------------------------------------------------------------------------//
bool EcalSpirocConfiguration::loadFromStream(std::istream& stream) {
    unsigned int err = 1;
    unsigned int line = 1;
    std::string s;
    while (true) {
        std::getline(stream, s);
        if (stream.eof())
            break;
        const char* searchString = s.c_str();
        err = 1;      // During each loop,err will be set back to 0 ,as long as one sentence in correct format has been read.Otherwise it will stay in 1

        if (s.find("#") == 0) {
            if (s.find("# End of the config file") == 0 ){
                err = 0 ;
                break;
            }  

            err = 0 ;
            continue; // Skip commented lines
        }

        if(sscanf(searchString, "EN_input_dac || %d ||",&m_cfgSpirocA.EN_input_dac )==1){
            line++;
            err = 0;
        }

        if(sscanf(searchString, "DAC_reference || %d ||",&m_cfgSpirocA.DAC_reference )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "input_8bits_DAC    || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || ",
                    &m_cfgSpirocA.input_8bits_DAC[0],
                    &m_cfgSpirocA.input_8bits_DAC[1],
                    &m_cfgSpirocA.input_8bits_DAC[2],
                    &m_cfgSpirocA.input_8bits_DAC[3],
                    &m_cfgSpirocA.input_8bits_DAC[4],
                    &m_cfgSpirocA.input_8bits_DAC[5],
                    &m_cfgSpirocA.input_8bits_DAC[6],
                    &m_cfgSpirocA.input_8bits_DAC[7],
                    &m_cfgSpirocA.input_8bits_DAC[8],
                    &m_cfgSpirocA.input_8bits_DAC[9],
                    &m_cfgSpirocA.input_8bits_DAC[10],
                    &m_cfgSpirocA.input_8bits_DAC[11],
                    &m_cfgSpirocA.input_8bits_DAC[12],
                    &m_cfgSpirocA.input_8bits_DAC[13],
                    &m_cfgSpirocA.input_8bits_DAC[14],
                    &m_cfgSpirocA.input_8bits_DAC[15],
                    &m_cfgSpirocA.input_8bits_DAC[16],
                    &m_cfgSpirocA.input_8bits_DAC[17],
                    &m_cfgSpirocA.input_8bits_DAC[18],
                    &m_cfgSpirocA.input_8bits_DAC[19],
                    &m_cfgSpirocA.input_8bits_DAC[20],
                    &m_cfgSpirocA.input_8bits_DAC[21],
                    &m_cfgSpirocA.input_8bits_DAC[22],
                    &m_cfgSpirocA.input_8bits_DAC[23],
                    &m_cfgSpirocA.input_8bits_DAC[24],
                    &m_cfgSpirocA.input_8bits_DAC[25],
                    &m_cfgSpirocA.input_8bits_DAC[26],
                    &m_cfgSpirocA.input_8bits_DAC[27],
                    &m_cfgSpirocA.input_8bits_DAC[28],
                    &m_cfgSpirocA.input_8bits_DAC[29],
                    &m_cfgSpirocA.input_8bits_DAC[30],
                    &m_cfgSpirocA.input_8bits_DAC[31])==32){
                        line++;
                        err = 0;
                    }

        if(sscanf(searchString, "input_DAC_off      || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || ",
                    &m_cfgSpirocA.input_DAC_off[0],
                    &m_cfgSpirocA.input_DAC_off[1],
                    &m_cfgSpirocA.input_DAC_off[2],
                    &m_cfgSpirocA.input_DAC_off[3],
                    &m_cfgSpirocA.input_DAC_off[4],
                    &m_cfgSpirocA.input_DAC_off[5],
                    &m_cfgSpirocA.input_DAC_off[6],
                    &m_cfgSpirocA.input_DAC_off[7],
                    &m_cfgSpirocA.input_DAC_off[8],
                    &m_cfgSpirocA.input_DAC_off[9],
                    &m_cfgSpirocA.input_DAC_off[10],
                    &m_cfgSpirocA.input_DAC_off[11],
                    &m_cfgSpirocA.input_DAC_off[12],
                    &m_cfgSpirocA.input_DAC_off[13],
                    &m_cfgSpirocA.input_DAC_off[14],
                    &m_cfgSpirocA.input_DAC_off[15],
                    &m_cfgSpirocA.input_DAC_off[16],
                    &m_cfgSpirocA.input_DAC_off[17],
                    &m_cfgSpirocA.input_DAC_off[18],
                    &m_cfgSpirocA.input_DAC_off[19],
                    &m_cfgSpirocA.input_DAC_off[20],
                    &m_cfgSpirocA.input_DAC_off[21],
                    &m_cfgSpirocA.input_DAC_off[22],
                    &m_cfgSpirocA.input_DAC_off[23],
                    &m_cfgSpirocA.input_DAC_off[24],
                    &m_cfgSpirocA.input_DAC_off[25],
                    &m_cfgSpirocA.input_DAC_off[26],
                    &m_cfgSpirocA.input_DAC_off[27],
                    &m_cfgSpirocA.input_DAC_off[28],
                    &m_cfgSpirocA.input_DAC_off[29],
                    &m_cfgSpirocA.input_DAC_off[30],
                    &m_cfgSpirocA.input_DAC_off[31])==32){
                        line++;
                        err = 0;
                    }


        if(sscanf(searchString, "low_gain_PA_bias   || %d ||",&m_cfgSpirocA.low_gain_PA_bias )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "high_gain_preamplifier_PP || %d ||",&m_cfgSpirocA.high_gain_preamplifier_PP )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_high_gain_PA    || %d ||",&m_cfgSpirocA.EN_high_gain_PA )==1){
            line++; 
            err = 0;
        }
        if(sscanf(searchString, "low_gain_preamplifier_PP || %d ||",&m_cfgSpirocA.low_gain_preamplifier_PP )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_low_gain_PA    || %d ||",&m_cfgSpirocA.EN_low_gain_PA )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "capacitor_HG_PA_comp    || %d ||",&m_cfgSpirocA.capacitor_HG_PA_comp )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "capacitor_HG_PA_fdbck   || %d ||",&m_cfgSpirocA.capacitor_HG_PA_fdbck )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "capacitor_LG_PA_fdbck   || %d ||",&m_cfgSpirocA.capacitor_LG_PA_fdbck )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "capacitor_LG_PA_comp    || %d ||",&m_cfgSpirocA.capacitor_LG_PA_comp )==1){
            line++;
            err = 0;
        }

        if(sscanf(searchString, "preamp_in_calib_config  || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || ",
                    &m_cfgSpirocA.preamp_in_calib_cofig[0],
                    &m_cfgSpirocA.preamp_in_calib_cofig[1],
                    &m_cfgSpirocA.preamp_in_calib_cofig[2],
                    &m_cfgSpirocA.preamp_in_calib_cofig[3],
                    &m_cfgSpirocA.preamp_in_calib_cofig[4],
                    &m_cfgSpirocA.preamp_in_calib_cofig[5],
                    &m_cfgSpirocA.preamp_in_calib_cofig[6],
                    &m_cfgSpirocA.preamp_in_calib_cofig[7],
                    &m_cfgSpirocA.preamp_in_calib_cofig[8],
                    &m_cfgSpirocA.preamp_in_calib_cofig[9],
                    &m_cfgSpirocA.preamp_in_calib_cofig[10],
                    &m_cfgSpirocA.preamp_in_calib_cofig[11],
                    &m_cfgSpirocA.preamp_in_calib_cofig[12],
                    &m_cfgSpirocA.preamp_in_calib_cofig[13],
                    &m_cfgSpirocA.preamp_in_calib_cofig[14],
                    &m_cfgSpirocA.preamp_in_calib_cofig[15],
                    &m_cfgSpirocA.preamp_in_calib_cofig[16],
                    &m_cfgSpirocA.preamp_in_calib_cofig[17],
                    &m_cfgSpirocA.preamp_in_calib_cofig[18],
                    &m_cfgSpirocA.preamp_in_calib_cofig[19],
                    &m_cfgSpirocA.preamp_in_calib_cofig[20],
                    &m_cfgSpirocA.preamp_in_calib_cofig[21],
                    &m_cfgSpirocA.preamp_in_calib_cofig[22],
                    &m_cfgSpirocA.preamp_in_calib_cofig[23],
                    &m_cfgSpirocA.preamp_in_calib_cofig[24],
                    &m_cfgSpirocA.preamp_in_calib_cofig[25],
                    &m_cfgSpirocA.preamp_in_calib_cofig[26],
                    &m_cfgSpirocA.preamp_in_calib_cofig[27],
                    &m_cfgSpirocA.preamp_in_calib_cofig[28],
                    &m_cfgSpirocA.preamp_in_calib_cofig[29],
                    &m_cfgSpirocA.preamp_in_calib_cofig[30],
                    &m_cfgSpirocA.preamp_in_calib_cofig[31] )==32){
                        line++;
                        err = 0;
                    }

        if(sscanf(searchString, "low_gain_slow_shaper_PP || %d ||",&m_cfgSpirocA.low_gain_slow_shaper_PP )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_low_gain_slow_shaper || %d ||",&m_cfgSpirocA.EN_low_gain_slow_shaper )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "time_constant_LG_shaper || %d ||",&m_cfgSpirocA.time_constant_LG_shaper )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "high_gain_slow_shaper_PP || %d ||",&m_cfgSpirocA.high_gain_slow_shaper_PP )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_hign_gain_slow_shaper || %d ||",&m_cfgSpirocA.EN_high_gain_slow_shaper )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "time_constant_HG_shaper  || %d ||",&m_cfgSpirocA.time_constant_HG_shaper )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "fast_shapers_follower_PP || %d ||",&m_cfgSpirocA.fast_shapers_follower_PP )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_fast_shaper   || %d ||",&m_cfgSpirocA.EN_fast_shaper )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "fast_shaper_PP   || %d ||",&m_cfgSpirocA.fast_shaper_PP )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "T_H     || %d ||",&m_cfgSpirocA.T_H )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_T_H  || %d ||",&m_cfgSpirocA.EN_T_H )==1){
            line++;
            err = 0;
        }

        if(sscanf(searchString, "T_H_bias || %d ||",&m_cfgSpirocA.T_H_bias )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_discri || %d ||",&m_cfgSpirocA.EN_discri )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "discriminator_PP  || %d ||",&m_cfgSpirocA.discriminator_PP )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "RS_or_discri   || %d ||",&m_cfgSpirocA.RS_or_discri )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "discriminator_mask  || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || ",
                    &m_cfgSpirocA.discriminator_mask[0],
                    &m_cfgSpirocA.discriminator_mask[1],
                    &m_cfgSpirocA.discriminator_mask[2],
                    &m_cfgSpirocA.discriminator_mask[3],
                    &m_cfgSpirocA.discriminator_mask[4],
                    &m_cfgSpirocA.discriminator_mask[5],
                    &m_cfgSpirocA.discriminator_mask[6],
                    &m_cfgSpirocA.discriminator_mask[7],
                    &m_cfgSpirocA.discriminator_mask[8],
                    &m_cfgSpirocA.discriminator_mask[9],
                    &m_cfgSpirocA.discriminator_mask[10],
                    &m_cfgSpirocA.discriminator_mask[11],
                    &m_cfgSpirocA.discriminator_mask[12],
                    &m_cfgSpirocA.discriminator_mask[13],
                    &m_cfgSpirocA.discriminator_mask[14],
                    &m_cfgSpirocA.discriminator_mask[15],
                    &m_cfgSpirocA.discriminator_mask[16],
                    &m_cfgSpirocA.discriminator_mask[17],
                    &m_cfgSpirocA.discriminator_mask[18],
                    &m_cfgSpirocA.discriminator_mask[19],
                    &m_cfgSpirocA.discriminator_mask[20],
                    &m_cfgSpirocA.discriminator_mask[21],
                    &m_cfgSpirocA.discriminator_mask[22],
                    &m_cfgSpirocA.discriminator_mask[23],
                    &m_cfgSpirocA.discriminator_mask[24],
                    &m_cfgSpirocA.discriminator_mask[25],
                    &m_cfgSpirocA.discriminator_mask[26],
                    &m_cfgSpirocA.discriminator_mask[27],
                    &m_cfgSpirocA.discriminator_mask[28],
                    &m_cfgSpirocA.discriminator_mask[29],
                    &m_cfgSpirocA.discriminator_mask[30],
                    &m_cfgSpirocA.discriminator_mask[31])==32){
                        line++;
                        err = 0;
                    }

        if(sscanf(searchString, "DAC   || %d ||",&m_cfgSpirocA.DAC )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "DAC_slope  || %d ||",&m_cfgSpirocA.DAC_slope )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "DAC_PP     || %d ||",&m_cfgSpirocA.DAC_PP )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_DAC     || %d ||",&m_cfgSpirocA.EN_DAC )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "bandgap_PP || %d ||",&m_cfgSpirocA.bandgap_PP )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_bandgap || %d ||",&m_cfgSpirocA.EN_bandgap )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "high_gain_OTAq_PP || %d ||",& m_cfgSpirocA.high_gain_OTAq_PP )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "low_gain_OTAq_pp  || %d ||",& m_cfgSpirocA.low_gain_OTAq_pp )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "porbe_OTAq_PP  || %d ||",& m_cfgSpirocA.porbe_OTAq_PP )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "LVDS_rec_PP    || %d ||",& m_cfgSpirocA.LVDS_rec_PP )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_high_gain_OTAq  || %d ||",& m_cfgSpirocA.EN_high_gain_OTAq )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_low_gain_OTAq   || %d ||",& m_cfgSpirocA.EN_low_gain_OTAq )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_probe_OTAq  || %d ||",& m_cfgSpirocA.EN_probe_OTAq )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_LVDS_rec    || %d ||",& m_cfgSpirocA.EN_LVDS_rec )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_out_dig     || %d ||",& m_cfgSpirocA.EN_out_dig )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_OR32        || %d ||",& m_cfgSpirocA.EN_OR32 )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_32_trigger  || %d ||",& m_cfgSpirocA.EN_32_trigger )==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "NC             || %d ||",& m_cfgSpirocA.NC )==1){
            line++;
            err = 0;
        }
        if (err) {
            std:: cerr << " Error@FE chip Config file:  Format incorrect in LINE= "<<line<<"! (#comments are not included in LINE NUM)Please check!" << std:: endl;
            assert(false);
            break;
        }  
    }

    if (err) {
        std:: cerr << " Error: Front End Configuration failed!" << std:: endl;
        assert(false);
        return false;
    }    
    return true;
}



//--------------------------------------------------------------------------------------------------------------------//
//Name:         convertCfgToVector
//Function:     convert m_cfgSpirocA ( struct SPIROC_A_CFG_TYPE ( defined in TypeDefs.h ))into Vector.
//              m_cfgSpirocA is a private variable defined in Class EcalSpirocConfiguration,which stores all the settings reading from the slow control config file
//Variables:    temp(unsigned char[]): point to an array to place the conversion result. Each member of this array corresponds to one bit of the slow control register
//              i.e.temp[index] corresponds to BIT(index) of the slow control register      
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------// 
void  EcalSpirocConfiguration::convertCfgToVector(unsigned char temp[456]) { 

    int  bit_total =0;
    int  bit,ch;


    temp[bit_total++] = (m_cfgSpirocA.EN_input_dac                          )&0x01;   
    temp[bit_total++] = (m_cfgSpirocA.DAC_reference                         )&0x01;

    for(ch=0;ch<32;ch++) {  //from ch0 to ch31 
        for(bit=0;bit<8;bit++)//from LSB to MSB
            temp[bit_total++] = (m_cfgSpirocA.input_8bits_DAC[ch]>>bit       )&0x01;
        temp[bit_total++] = (m_cfgSpirocA.input_DAC_off[ch]                 )&0x01;
    }

    temp[bit_total++] = (m_cfgSpirocA.low_gain_PA_bias                      )&0x01;   
    temp[bit_total++] = (m_cfgSpirocA.high_gain_preamplifier_PP             )&0x01;
    temp[bit_total++] = (m_cfgSpirocA.EN_high_gain_PA                       )&0x01;   
    temp[bit_total++] = (m_cfgSpirocA.low_gain_preamplifier_PP              )&0x01;
    temp[bit_total++] = (m_cfgSpirocA.EN_low_gain_PA                        )&0x01;   

    for(bit=0;bit<4;bit++) //from LSB to MSB
        temp[bit_total++] = (m_cfgSpirocA.capacitor_HG_PA_comp>>bit         )&0x01;

    for(bit=0;bit<4;bit++) //from LSB to MSB
        temp[bit_total++] = (m_cfgSpirocA.capacitor_HG_PA_fdbck>>bit        )&0x01;

    for(bit=0;bit<4;bit++) //from MSB to LSB
        temp[bit_total++] = (m_cfgSpirocA.capacitor_LG_PA_fdbck>>(3-bit)        )&0x01; 

    for(bit=0;bit<4;bit++) //from MSB to LSB
        temp[bit_total++] = (m_cfgSpirocA.capacitor_LG_PA_comp>>(3-bit)         )&0x01; 

    for(ch=0;ch<32;ch++) {  //from ch0 to ch31 
        for(bit=0;bit<2;bit++)//from  LSB to MSB
            temp[bit_total++] = (m_cfgSpirocA.preamp_in_calib_cofig[ch]>>(1-bit) )&0x01;
    }

    temp[bit_total++] = (m_cfgSpirocA.low_gain_slow_shaper_PP               )&0x01;
    temp[bit_total++] = (m_cfgSpirocA.EN_low_gain_slow_shaper               )&0x01;   

    for(bit=0;bit<3;bit++) //from MSB to LSB
        temp[bit_total++] = (m_cfgSpirocA.time_constant_LG_shaper>>(2-bit)      )&0x01; 

    temp[bit_total++] = (m_cfgSpirocA.high_gain_slow_shaper_PP              )&0x01;
    temp[bit_total++] = (m_cfgSpirocA.EN_high_gain_slow_shaper              )&0x01;   

    for(bit=0;bit<3;bit++) //from MSB to LSB
        temp[bit_total++] = (m_cfgSpirocA.time_constant_HG_shaper>>(2-bit)  )&0x01; 

    temp[bit_total++] = (m_cfgSpirocA.fast_shapers_follower_PP              )&0x01;
    temp[bit_total++] = (m_cfgSpirocA.EN_fast_shaper                        )&0x01;   

    temp[bit_total++] = (m_cfgSpirocA.fast_shaper_PP                        )&0x01;
    temp[bit_total++] = (m_cfgSpirocA.T_H                                   )&0x01;  

    temp[bit_total++] = (m_cfgSpirocA.EN_T_H                                )&0x01;
    temp[bit_total++] = (m_cfgSpirocA.T_H_bias                              )&0x01;   

    temp[bit_total++] = (m_cfgSpirocA.EN_discri                             )&0x01;
    temp[bit_total++] = (m_cfgSpirocA.discriminator_PP                      )&0x01;   

    temp[bit_total++] = (m_cfgSpirocA.RS_or_discri                          )&0x01;  

    for(ch=0;ch<32;ch++)  //from ch0 to ch31 
        temp[bit_total++] = (m_cfgSpirocA.discriminator_mask[ch]            )&0x01;

    for(bit=0;bit<10;bit++) //from LSB to MSB
        temp[bit_total++] = (m_cfgSpirocA.DAC>>bit                          )&0x01;

    temp[bit_total++] = (m_cfgSpirocA.DAC_slope                             )&0x01;
    temp[bit_total++] = (m_cfgSpirocA.DAC_PP                                )&0x01;   

    temp[bit_total++] = (m_cfgSpirocA.EN_DAC                                )&0x01;
    temp[bit_total++] = (m_cfgSpirocA.bandgap_PP                            )&0x01;   

    temp[bit_total++] = (m_cfgSpirocA.EN_bandgap                            )&0x01;
    temp[bit_total++] = (m_cfgSpirocA.high_gain_OTAq_PP                     )&0x01;  

    temp[bit_total++] = (m_cfgSpirocA.low_gain_OTAq_pp                      )&0x01;
    temp[bit_total++] = (m_cfgSpirocA.porbe_OTAq_PP                         )&0x01; 

    temp[bit_total++] = (m_cfgSpirocA.LVDS_rec_PP                           )&0x01;
    temp[bit_total++] = (m_cfgSpirocA.EN_high_gain_OTAq                     )&0x01;   

    temp[bit_total++] = (m_cfgSpirocA.EN_low_gain_OTAq                      )&0x01;
    temp[bit_total++] = (m_cfgSpirocA.EN_probe_OTAq                         )&0x01;  

    temp[bit_total++] = (m_cfgSpirocA.EN_LVDS_rec                           )&0x01;
    temp[bit_total++] = (m_cfgSpirocA.EN_out_dig                            )&0x01;  

    temp[bit_total++] = (m_cfgSpirocA.EN_OR32                               )&0x01;
    temp[bit_total++] = (m_cfgSpirocA.EN_32_trigger                         )&0x01;   

    for(bit=0;bit<4;bit++) //from LSB to MSB
        temp[bit_total++] = (m_cfgSpirocA.NC>>bit                           )&0x01;
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         writeToStream
//Function:     dump m_cfgSpirocA ( struct SPIROC_A_CFG_TYPE ( defined in TypeDefs.h ))
//              m_cfgSpirocA is a private variable defined in Class EcalSpirocConfiguration,which stores all the settings reading from the slow control config file
//Variables:    stream(ostream):  output stream
//                                (ostream objects are stream objects used to write and format output as sequences of characters      
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------// 
void EcalSpirocConfiguration::writeToStream(std::ostream& stream) const
{
    stream
        <<" EN_input_dac:    "<<m_cfgSpirocA.EN_input_dac << std::endl
        <<" DAC_reference:   "<<m_cfgSpirocA.DAC_reference << std::endl;

    stream
        <<" input_8bits_DAC: ";
    for(unsigned int j = 0; j<8 ; j++)
        stream<<m_cfgSpirocA.input_8bits_DAC[j] <<"  ";
    stream
        <<std::endl;

    stream
        <<" input_8bits_DAC: ";
    for(unsigned int j = 0; j<8 ; j++)
        stream<<m_cfgSpirocA.input_8bits_DAC[j+8] <<"  ";
    stream
        <<std::endl;

    stream
        <<" input_8bits_DAC: ";
    for(unsigned int j = 0; j<8 ; j++)
        stream<<m_cfgSpirocA.input_8bits_DAC[j+16] <<"  ";
    stream
        <<std::endl;

    stream
        <<" input_8bits_DAC: ";
    for(unsigned int j = 0; j<8 ; j++)
        stream<<m_cfgSpirocA.input_8bits_DAC[j+24] <<"  ";
    stream
        <<std::endl;

    stream
        <<" input_DAC_off:   ";
    for(unsigned int j = 0; j<8 ; j++)
        stream<<m_cfgSpirocA.input_DAC_off[j] <<"  ";
    stream
        <<std::endl;

    stream
        <<" input_DAC_off:   ";
    for(unsigned int j = 0; j<8 ; j++)
        stream<<m_cfgSpirocA.input_DAC_off[j+8] <<"  ";
    stream
        <<std::endl;

    stream
        <<" input_DAC_off:   ";
    for(unsigned int j = 0; j<8 ; j++)
        stream<<m_cfgSpirocA.input_DAC_off[j+16] <<"  ";
    stream
        <<std::endl;

    stream
        <<" input_DAC_off:   ";
    for(unsigned int j = 0; j<8 ; j++)
        stream<<m_cfgSpirocA.input_DAC_off[j+24] <<"  ";
    stream
        <<std::endl;

    stream
        <<" low_gain_PA_bias:"<<m_cfgSpirocA.low_gain_PA_bias << std::endl
        <<" high_gain_preamplifier_PP: "<<m_cfgSpirocA.high_gain_preamplifier_PP << std::endl
        <<" EN_high_gain_PA:           "<<m_cfgSpirocA.EN_high_gain_PA << std::endl
        <<" low_gain_preamplifier_PP:  "<<m_cfgSpirocA.low_gain_preamplifier_PP << std::endl
        <<" EN_low_gain_PA:            "<<m_cfgSpirocA.EN_low_gain_PA << std::endl
        <<" capacitor_HG_PA_comp:      "<<m_cfgSpirocA.capacitor_HG_PA_comp << std::endl
        <<" capacitor_HG_PA_fdbck:     "<<m_cfgSpirocA.capacitor_HG_PA_fdbck << std::endl
        <<" capacitor_LG_PA_fdbck:     "<<m_cfgSpirocA.capacitor_LG_PA_fdbck << std::endl
        <<" capacitor_LG_PA_comp:      "<<m_cfgSpirocA.capacitor_LG_PA_comp<< std::endl;

    stream
        <<" preamp_in_calib_cofig:     ";
    for(unsigned int j = 0; j<8 ; j++)
        stream<<m_cfgSpirocA.preamp_in_calib_cofig[j] <<"  ";
    stream
        <<std::endl;   

    stream
        <<" preamp_in_calib_cofig:     ";
    for(unsigned int j = 0; j<8 ; j++)
        stream<<m_cfgSpirocA.preamp_in_calib_cofig[j+8] <<"  ";
    stream
        <<std::endl;  

    stream
        <<" preamp_in_calib_cofig:     ";
    for(unsigned int j = 0; j<8 ; j++)
        stream<<m_cfgSpirocA.preamp_in_calib_cofig[j+16] <<"  ";
    stream
        <<std::endl;  

    stream
        <<" preamp_in_calib_cofig:     ";
    for(unsigned int j = 0; j<8 ; j++)
        stream<<m_cfgSpirocA.preamp_in_calib_cofig[j+24] <<"  ";
    stream
        <<std::endl;  

    stream
        <<" low_gain_slow_shaper_PP:   "<<m_cfgSpirocA.low_gain_slow_shaper_PP << std::endl
        <<" EN_low_gain_slow_shaper:   "<<m_cfgSpirocA.EN_low_gain_slow_shaper << std::endl
        <<" time_constant_LG_shaper:   "<<m_cfgSpirocA.time_constant_LG_shaper << std::endl
        <<" high_gain_slow_shaper_PP:  "<<m_cfgSpirocA.high_gain_slow_shaper_PP << std::endl
        <<" EN_high_gain_slow_shaper:  "<<m_cfgSpirocA.EN_high_gain_slow_shaper << std::endl
        <<" time_constant_HG_shaper:   "<<m_cfgSpirocA.time_constant_HG_shaper << std::endl
        <<" fast_shapers_follower_PP:  "<<m_cfgSpirocA.fast_shapers_follower_PP << std::endl
        <<" EN_fast_shaper:            "<<m_cfgSpirocA.EN_fast_shaper << std::endl
        <<" fast_shaper_PP:            "<<m_cfgSpirocA.fast_shaper_PP<< std::endl
        <<" T_H:                       "<<m_cfgSpirocA.T_H << std::endl
        <<" EN_T_H:                    "<<m_cfgSpirocA.EN_T_H << std::endl
        <<" T_H_bias:                  "<<m_cfgSpirocA.T_H_bias << std::endl
        <<" EN_discri:                 "<<m_cfgSpirocA.EN_discri << std::endl
        <<" discriminator_PP:          "<<m_cfgSpirocA.discriminator_PP << std::endl
        <<" RS_or_discri:              "<<m_cfgSpirocA.RS_or_discri<< std::endl;

    stream
        <<" discriminator_mask:        ";
    for(unsigned int j = 0; j<8 ; j++)
        stream<<m_cfgSpirocA.discriminator_mask[j] <<"  ";
    stream
        <<std::endl;  

    stream
        <<" discriminator_mask:        ";
    for(unsigned int j = 0; j<8 ; j++)
        stream<<m_cfgSpirocA.discriminator_mask[j+8] <<"  ";
    stream
        <<std::endl;  

    stream
        <<" discriminator_mask:        ";
    for(unsigned int j = 0; j<8 ; j++)
        stream<<m_cfgSpirocA.discriminator_mask[j+16] <<"  ";
    stream
        <<std::endl;  

    stream
        <<"discriminator_mask:         ";
    for(unsigned int j = 0; j<8 ; j++)
        stream<<m_cfgSpirocA.discriminator_mask[j+24] <<"  ";
    stream
        <<std::endl;  

    stream
        <<" DAC:                       "<<m_cfgSpirocA.DAC << std::endl
        <<" DAC_slope:                 "<<m_cfgSpirocA.DAC_slope << std::endl
        <<" DAC_PP:                    "<<m_cfgSpirocA.DAC_PP << std::endl
        <<" EN_DAC:                    "<<m_cfgSpirocA.EN_DAC << std::endl
        <<" bandgap_PP:                "<<m_cfgSpirocA.bandgap_PP << std::endl
        <<" EN_bandgap:                "<<m_cfgSpirocA.EN_bandgap << std::endl
        <<" high_gain_OTAq_PP:         "<<m_cfgSpirocA.high_gain_OTAq_PP << std::endl
        <<" low_gain_OTAq_pp:          "<<m_cfgSpirocA.low_gain_OTAq_pp << std::endl
        <<" porbe_OTAq_PP:             "<<m_cfgSpirocA.porbe_OTAq_PP<< std::endl
        <<" LVDS_rec_PP:               "<<m_cfgSpirocA.LVDS_rec_PP << std::endl
        <<" EN_high_gain_OTAq:         "<<m_cfgSpirocA.EN_high_gain_OTAq << std::endl
        <<" EN_low_gain_OTAq:          "<<m_cfgSpirocA.EN_low_gain_OTAq << std::endl
        <<" EN_probe_OTAq:             "<<m_cfgSpirocA.EN_probe_OTAq << std::endl
        <<" EN_LVDS_rec:               "<<m_cfgSpirocA.EN_LVDS_rec << std::endl
        <<" EN_out_dig:                "<<m_cfgSpirocA.EN_out_dig << std::endl
        <<" EN_OR32:                   "<<m_cfgSpirocA.EN_OR32<< std::endl
        <<" EN_32_trigger:             "<<m_cfgSpirocA.EN_32_trigger << std::endl
        <<" NC:                        "<<m_cfgSpirocA.NC << std::endl;

}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         convertVectorTo16bitsArray
//Function:     convert config vector into 16bits array, which will be written into UFM
//Variables:    temp(unsigned char[]): vectors converted from m_cfgSpirocA
//              array(unsigned short[]): point to the array to place the 16bits array written into UFM
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------// 
void  EcalSpirocConfiguration::convertVectorTo16bitsArray(int dumpOn, unsigned char temp[456],unsigned short array[])
{
    short               bit;
    short               p;
    unsigned short  pad_short;

    unsigned short  temp_debug;
    short              index;
    /*=============convert to word====== order config bit 0 first in=======*/
    //    p =0;    
    //    index = 0;
    //    temp_debug = 0;

    //    for(bit=0;bit<(456+8);bit++)
    //    {  
    //      pad_short <<=1; 

    //      if(bit <456)
    //        pad_short += temp[bit];
    //      else
    //        pad_short += 0;  // fill the last byte with 0

    //       if((bit%16)==15)  
    //       {   array[index] = pad_short;

    //     if(dumpOn==1){
    //           std::cerr<<"Info@16bitsArray:array["<<index<<"]=";
    //           for(unsigned int i = 0; i<16; i++)
    //           std::cerr<<((array[index]>>(15-i))&0x0001)<<" ";
    //           std::cerr<<std::endl;
    //     }
    //           index++;
    //       }
    //    }
    /*=============convert to word====== order config bit 0 last in=======*/
    p =0;    
    index = 0;
    temp_debug = 0;

    for(bit=455+8;bit>=0;bit--)
    {  
        pad_short <<=1; 

        if(bit >=8)
            pad_short += temp[bit-8];
        else
            pad_short += 0;  // fill the last byte with 0

        if((bit%16)==0)  
        {   array[index] = pad_short;

            if(dumpOn==1){
                std::cerr<<"Info@16bitsArray:array["<<index<<"]=";
                for(unsigned int i = 0; i<16; i++)
                    std::cerr<<((array[index]>>(15-i))&0x0001)<<" ";
                std::cerr<<std::endl;
            }
            index++;
        }
    }
}

//-------------------------------------for Analogue Test / Debug---------------------------------------------------------//

//--------------------------------------------------------------------------------------------------------------------//
//Name:         setInput8bitsDAC
//Function:     set DAC value without refering to spiroc config file, then update 16bits config arrays.
//Variables:    Value (int): DAC value to be set
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//    
void EcalSpirocConfiguration::setDACandRegenerateArray(int Value,unsigned short configArray[]){
  unsigned char temp[702];
  unsigned int dumpOn = 0;
  for(unsigned int channel=0; channel<32;channel++){
        m_cfgSpirocA.input_8bits_DAC[channel] = Value;
   // std::cerr<<"DAC value written"<<m_cfgSpirocA.input_8bits_DAC[channel];
  }
  convertCfgToVector(temp);
  convertVectorTo16bitsArray(dumpOn, temp, configArray);
}



//----------------------------------------------------------------------------//
//Name:          selectForRead
//Function:      provide info that which kind of signals are used to access "READ interface" of Spiroc to other Classes which need this value.
//               Because m_selectForRead is a private variable for Class EcalSpirocConfiguration, which only can be accessed by Class EcalSpirocConfiguration itself
//               Parameter with the identical name in Firmware corresponds to bit6 of Gene_ctrl_reg0 (Firmwares for SpirocA and SpirocII use the same bit, 01.06.2010)
//               0= signals from SPI (set by C code);    1= signals from uplink(H,HB,S,SB)
//Parameters:    None
//Returns:       (int) return 0= signals from SPI (set by C code)
//                            1= signals from uplink(H,HB,S,SB)
//---------------------------------------------------------------------------//
int EcalSpirocConfiguration::selectForRead() const
{
    return m_selectForRead;
}

//----------------------------------------------------------------------------//
//Name:          setSelectForRead
//Function:      select signals used to access "READ interface" of Spiroc without refering to the config file
//Parameters:    selectForRead(int):  Only 0 or 1 could be used, otherwise the program will break off with an error report!
//               0= signals from SPI (set by C code),  1= signals from uplink(H,HB,S,SB)
//Returns:       None
//---------------------------------------------------------------------------//
void EcalSpirocConfiguration::setSelectForRead(int selectForRead)
{
    m_selectForRead = selectForRead;
}

//----------------------------------------------------------------------------//
//Name:          selectScNprobe
//Function:      provide info whether Slow Control Register or Probe Register is accessed to other Classes which need this value.
//               Because m_selectScNprobe is a private variable for Class EcalSpirocConfiguration, which only can be accessed by Class EcalSpirocConfiguration itself
//               This function is only used for SpirocA, and parameter with the identical name in Firmware corresponds to bit7 of Gene_ctrl_reg0 (01.06.2010).
//               In firmware, the default value for bit7 has been set to 1, in order to access Slow Control Register in default status.
//Parameters:    None
//Returns:       (int) return 0= access to Probe Register of Spiroc 
//                            1= access to Slow Control Register
//---------------------------------------------------------------------------//
int EcalSpirocConfiguration::selectScNprobe() const
{
    return m_selectScNprobe;
}

//----------------------------------------------------------------------------//
//Name:          setSelectScNprobe
//Function:      select signals used to access "READ interface" of Spiroc without refering to the config file
//Parameters:    selectScNprobe(int):  Only 0 or 1 could be used, otherwise the program will break off with an error report!
//               0= signals from SPI (set by C code),  1= signals from uplink(H,HB,S,SB)
//Returns:       None
//---------------------------------------------------------------------------//
void EcalSpirocConfiguration::setSelectScNprobe(int selectScNprobe)
{
    m_selectScNprobe = selectScNprobe;
}

//----------------------------------------------------------------------------//
//Name:          trigSelect
//Function:      provide trigger type of spiroc to other Classes which need this value.
//               Because m_trigSelect is a private variable for Class EcalSpirocConfiguration, which only can be accessed by Class EcalSpirocConfiguration itself
//               Return value of this function corresponds to different bits in SpirocII firmware and SpirocA firmware.
//               For SpirocA,it corresponds to bit9&bit8(trigSelect) of Gene_ctrl_reg0 (01.06.2010)
//               For SpirocII, it corresponds to bit 9(external_trigger_enable) of Gene_ctrl_reg0  (01.06.2010)
//               In SpirocA firmware, the default value for bit9&bit8 has been set to "00".
//Parameters:    None
//Returns:       (int) return value has different meanings & ranges for spirocII and spirocA
//               for SPIROCA  0=UL_HOLD(use H,HB,S,SB as trigger signal);  1=NIM_trigger;   2=LVDS_trigger
//               for SPIROCII:0=internal trigger; 1=external trigger;
//---------------------------------------------------------------------------//
int EcalSpirocConfiguration::trigSelect() const
{
    return m_trigSelect;
}

//----------------------------------------------------------------------------//
//Name:          setTrigSelect
//Function:      set trigger type of spiroc without refering to the config file
//Parameters:    trigSelect(int): has different meanings & ranges for spirocII and spirocA 
//               For spirocA: Only 0~2 could be used, otherwise the program will break off with an error report!
//                            0=UL_HOLD(use H,HB,S,SB as trigger signal);  1=NIM_trigger;   2=LVDS_trigger
//               For spirocII:Only 0 or 1 could be used, otherwise the program will break off with an error report!
//                            0=internal trigger; 1=external trigger
//Returns:       None
//---------------------------------------------------------------------------//
void EcalSpirocConfiguration::setTrigSelect(int trigSelect)
{
    m_trigSelect = trigSelect;
}

//----------------------------------------------------------------------------//
//Name:          setSlowControlCfgFile
//Function:      set a new path to the slow control config file without refering to the UsbBoard config file
//Parameters:    fileName(string) : the format is like "/home/pebs_ecal/usbBoard/readout/readoutSettings/spirocA_EPFL.cfg"
//Returns:       None
//---------------------------------------------------------------------------//
void EcalSpirocConfiguration::setSlowControlCfgFile(const std::string& fileName)
{
    m_slowControlCfgFile = fileName;
}

//----------------------------------------------------------------------------//
//Name:          firmwareFileName     
//Function:      provide path to the slow control config file to other Classes which need this value.
//               Because m_slowControlCfgFile is a private variable for Class UsbBoardConfiguration, which only can be accessed by Class UsbBoardConfiguration itself
//Parameters:    None
//Returns:       (string) return path to the slow control config file 
//---------------------------------------------------------------------------//
const std::string& EcalSpirocConfiguration::slowControlCfgFile() const
{
    return m_slowControlCfgFile;
}

// ------------------------- EcalSpiroc --------------------------------
EcalSpiroc::EcalSpiroc(UsbBoard* board, FrontEndBoardType type, int slotNo)
    : FrontEndBoard(board, type, slotNo)
    , m_configuration(0)
    , m_spiToUfmRegisterSwitch(0)
{
}

EcalSpiroc::~EcalSpiroc()
{}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         configure
//Function:     configure front end chip, if this function has been switched on in the UsbBoard cfg file
//Variables:    None
//Returns:      (bool)
//              true = config successfully
//              false= error exist during configuration
//-------------------------------------------------------------------------------------------------------------------//
bool EcalSpiroc::configure(FrontEndBoardConfiguration* configuration)
{
    m_configuration = static_cast<EcalSpirocConfiguration*>(configuration);

    std::cerr << "EcalSpiroc::configure() called." << std::endl;
    //return true; //TODO
    bool err = false;
#ifndef NO_QUSB
    // if (m_configuration->switchFeChipConfig()) {               // if switch on(=1) FeChipConfig in usbBoard Config file, Config Front End Chip through SPI
   // EcalSpirocConfiguration chipConfig(m_slotNo);
    std::string chipConfigFilePath = m_configuration->slowControlCfgFile();        // get the path to slow control config file

    unsigned char temp[702];
    unsigned short configArray[100];

    unsigned int dumpOn = 1;                // switch on/ off the debug infomation display.

    
/*
     chipConfig.loadConfig(chipConfigFilePath);
     chipConfig.convertCfgToVector(temp);
     chipConfig.convertVectorTo16bitsArray(dumpOn, temp, configArray);
     */
    m_configuration->loadConfig(chipConfigFilePath);
    m_configuration->convertCfgToVector(temp);
    m_configuration->convertVectorTo16bitsArray(dumpOn, temp, configArray);

     // if (spiConfigSpirocA(dumpOn, configArray, m_configuration->trigSelect(), m_configuration->selectForRead(), m_configuration->selectScNprobe()))  err = true;
     if (spiConfigSpirocA(dumpOn, configArray, 0, 1, 1))  err = true;
 
     //reset the  probe register
     //std::cerr<<"config finished!Press keyboard to reset probe register"<<std::endl;
     resetProbeRegister();
     //////////////////////////////////


     // select probe bit,used for test spirocA chip. Details of each bit, refer to the datasheet of spirocA
     //////////////////////////////
     //selectProbeBit(14);      // select bit14 of the probe register
     //getchar();
     ////////////////////////////////
           
     // select channel for continuous mode operation
     //////////////////////////////
     // Add only this two lines below to use the continous mode  
     //selectContinuousMode(14);      // select channel 1
     //getchar();
     ////////////////////////////////

     // change dac value and reconfig
     /////////////////////////////////////////// 
     if (0) {
	 for(unsigned int DACValue=0; DACValue<256; DACValue+=16){
           //chipConfig.setDACandRegenerateArray(DACValue,configArray);
           m_configuration->setDACandRegenerateArray(DACValue,configArray);
           if (spiConfigSpirocA(dumpOn, configArray, m_configuration->trigSelect(), m_configuration->selectForRead(), m_configuration->selectScNprobe()))  err = true;
         }
     }
     /////////////////////////////////////////////
        
     // Choose Uplink or SPI control for the read port 
     //////////////////////////
     //setSelectForRead(0);       // use spi
     setSelectForRead(1);       // use uplink
     ////////////////////////

    // Set HG_en or LG_en
    ////////////////////////////////
    // setSelectForRead(0);// use spi 
    //setHgEnSpi(1);  
    //setLgEnSpi(1);  
    ////////////////////////////////////
    
#endif
    if (err) {
        std::cout << "Could not configure DAQ" << std::endl;
        assert(false);
        return false;
      } else {
        return true;
    }
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiToUfmOrRegister
//Function:     select QUSB SPI communication between QUSB & UFM  or  QUSB & Register in FPGA on the front end board. 
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              data(unsigned short):Only two values can be used here  0xC3C3= QUSB & UFM, 0x3C3C= QUSB & Register
//                                   Two macro definition have been defined in FrontEndChipMacroDefinition.h, use it ! ( SPItoUFM = 0xC3C3, SPItoREGISTER = 0x3C3C )
//
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::spiToUfmOrRegister(unsigned char portNum, unsigned short data,bool force) {
  if(data != m_spiToUfmRegisterSwitch || force){     // executed only when change spi between Ufm and R/W Register.
      unsigned char buf[2];
      buf[0] = data >> 8;
      buf[1] = data & 0xFF;
      write(portNum, buf, 2);
      m_spiToUfmRegisterSwitch = data;
      //if (data==0xC3C3) printf("set spi to UFM \n");
      //if (data==0x3C3C) printf("set spi to GeneCtrlReg \n");       
  }
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiRegisterWrite
//Function:     write data to a given register in FOGA on the front end board
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              addr(unsigned short): address of the register to be written, currently for spirocA and spirocII front end board , we only use two register, already defined in FrontEndChipMacroDefinition.h (01.06.2010)
//                                    FIRMWARE_VERSION_REG_ADDR = 0x0000
//                                    GENERAL_CTRL_REG0 = 0x0001      ( detail definition for each bit, refer to FrontEndMacroDefiniton.h )
//              data(unsigned short): data to be written into the given register
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::spiRegisterWrite(unsigned char portNum, unsigned short addr, unsigned short data) {
    unsigned char buf[4];  //[0]--SPI cmd, [1], [2]--address, [3], [4]--data

    spiToUfmOrRegister(0,0x3C3C);  // make sure that  spi is linked to register R/W

    buf[0] = (addr>>8)&0xFF;
    buf[1] = (addr   )&0xFF;
    buf[2] = (data>>8)&0xFF;
    buf[3] = (data   )&0xFF;
    write(portNum, buf, 4);
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiRegisterRead
//Function:     read data from a given register in FOGA on the front end board
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              addr(unsigned short): address of the register to be written, currently for spirocA and spirocII front end board , we only use two register, already defined in FrontEndChipMacroDefinition.h (01.06.2010)
//                                    FIRMWARE_VERSION_REG_ADDR = 0x0000
//                                    GENERAL_CTRL_REG0 = 0x0001      ( detail definition for each bit, refer to FrontEndMacroDefiniton.h )
//Returns:      (unsigned short): return the data value read back from the given register
//-------------------------------------------------------------------------------------------------------------------//
unsigned short EcalSpiroc::spiRegisterRead(unsigned char portNum, unsigned short addr) {
    unsigned char buf[4];  //[0]--SPI cmd, [1], [2]--address, [3], [4]--data
    unsigned short  readData;
    spiToUfmOrRegister(0,0x3C3C); // make sure that  spi is linked to register R/W

    buf[0] = ((addr>>8)&0x7F) | 0x80;
    buf[1] = (addr   )&0xFF;
    buf[2] = 0x00;
    buf[3] = 0x00;
    writeRead(portNum, buf, 4);
    readData =  buf[2];
    readData <<= 8;
    readData += buf[3];
    return(readData);
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmStatusRead
//Function:     read the staus register in UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//Returns:      (unsigned char): return current status of the UFM
//Note:         before use function, remember to switch SPI communication to QUSB & UFM first, using function "spiToUfmOrRegister(unsigned char portNum, unsigned short data)"
//-------------------------------------------------------------------------------------------------------------------//
unsigned char EcalSpiroc::spiUfmStatusRead(unsigned char portNum) {
    unsigned char buf[2];  //[0]--SPI cmd
    spiToUfmOrRegister(0,0xC3C3);  // make sure that  spi is linked to write ufm
 

    buf[0] = UFM_CMD_RDSR;
    buf[1] = 0x00;
    writeRead(portNum, buf, 2);
    return buf[1];
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmErase
//Function:     erase the content in UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::spiUfmErase(unsigned char portNum) {
    unsigned char buf[2];  //[0]--SPI cmd
    spiToUfmOrRegister(0,0xC3C3);  // make sure that  spi is linked to write ufm

    buf[0] = UFM_CMD_WREN;
    write(portNum, buf, 1);   //write enable
    sleep(1);
    buf[0] = UFM_CMD_BERASE;
    write(portNum, buf, 1);   //erase
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmWrite
//Function:     write one word into UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              addr(unsigned short): the address in UFM to be written into
//              data(unsigned short): the data to be written into the UFM at the given address
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::spiUfmWrite(unsigned char portNum, unsigned short addr, unsigned short data) {
    unsigned char buf[5];
    spiToUfmOrRegister(0,0xC3C3);  // make sure that  spi is linked to write ufm

    buf[0] = UFM_CMD_WRITE;
    buf[1] = (addr>>8)&0xFF;
    buf[2] = (addr   )&0xFF;
    buf[3] = (data>>8)&0xFF;
    buf[4] = (data   )&0xFF;
    write(portNum, buf, 5);
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmReadWord
//Function:     read one word from UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              start_addr(unsigned short): the first address in UFM to be read
//Returns:      (unsigned char) return data (16bits) read from UFM
//-------------------------------------------------------------------------------------------------------------------//
unsigned short EcalSpiroc::spiUfmReadWord(unsigned char portNum, unsigned short start_addr) {
    unsigned char buf[6];
    unsigned short data;
    spiToUfmOrRegister(0,0xC3C3);  // make sure that  spi is linked to write ufm

    buf[0] =  UFM_CMD_READ;
    buf[1] = (start_addr>>8)&0xFF;
    buf[2] = (start_addr   )&0xFF;
    writeRead(portNum, buf, 5);
    data=(buf[3] << 8)+buf[4];
    return data;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmReadBlock
//Function:     read a sequence of words from UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              word_num(unsigned short): the number of words to be read
//              data(unsigned short[]): a pointer to the buffer that store the rceive data
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::spiUfmReadBlock(unsigned char portNum, unsigned short start_addr, unsigned short word_num, unsigned short data[512]) {
    unsigned char buf[64];
    unsigned short num, j;

    spiToUfmOrRegister(0,0xC3C3);  // make sure that  spi is linked to write ufm

    for (j=0; j<(word_num/30 + 1); j++)
    {
        if ((word_num-30*j) <= 30)
            num = word_num - 30*j;
        else
            num = 30;
        start_addr += 30*j;
        buf[0] =  UFM_CMD_READ;
        buf[1] = (start_addr>>8)&0xFF;
        buf[2] = (start_addr   )&0xFF;
        writeRead(portNum, buf, num*2+3);
        for(unsigned short i=0; i<num; i++) {
            data[i+j*30]=(buf[2*i+3] << 8)+buf[2*i+4];
            //                      std::cerr << "Info@SPIUfmReadBlock:readUFMfifo    readData[" << i+j*30 << "]=";
            //                         for(unsigned int bit=0; bit<16; bit++)
            //               std::cerr << ((data[i+j*30]>>(15-bit))&0x0001) << " ";
            //             std::cerr << std::endl;
        }
    }
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         selectReadChannel
//Function:     using signals send by SPI to access READ interface of spiroc, select a certain channel to be readout
//Variables:    channelIndex(unsigned int): channel index ( begin from 0)
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::selectReadChannel(unsigned int channelIndex) {

    setSelectForRead(0);      // use spi to access READ interface of spirocA 
 
    setClkReadSpi(0);         //initial CLK_READ_SPI

    setResetbReadSpi(0);      // reset Read Register low active
    setResetbReadSpi(1);      // remove reset

    setSrinReadSpi(1);        // set '1' to SRIN_READ_SPI

    setClkReadSpi(1);        // clock 1
    setClkReadSpi(0);        // clock 0
    std::cerr << "select READ Register Channel 0" << std::endl;

    setSrinReadSpi(0);       //  set '0' to SRIN_READ_SPI

    for(unsigned int i=0; i<channelIndex; i++) {
        setClkReadSpi(1);      //clock in
        setClkReadSpi(0);
        std::cerr << "select READ Register Channel " << (i+1) << std::endl;
    }
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         selectProbeBit
//Function:     using signals send by SPI to access Slow control/probe interface of spiroc, select a certain bit to be accessed
//Variables:    bitIndex(unsigned int): bit index ( begin from 0)                                                   
//Returns:      None  
//-------------------------------------------------------------------------------------------------------------------//  

void EcalSpiroc::selectProbeBit(unsigned int bitIndex){	
	
        setClkScProbe(0);         //initial CLK_SC_probe
        
        resetProbeRegister();          //reset probe register

        setSrinScProbe(1);        // set '1' to SRIN_SC_probe

        setClkScProbe(1);        // clock 1
        setClkScProbe(0);        // clock 0
	std::cerr<<"select Probe Register Bit0"<<std::endl;   

        setSrinScProbe(0);       //  set '0' to SRIN_SC_probe

        for(unsigned int i=0; i<bitIndex; i++){
	  setClkScProbe(1);      //clock in
          setClkScProbe(0);
 	  std::cerr<<"select Probe Register Bit"<<(i+1)<<std::endl;      
	}
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         setClkReadSpi
//Function:     using signals send by SPI to access READ interface of spiroc, set CLK_READ_spi to be hign/low
//Variables:    setValue(char) :  0=set CLK_READ_spi to LOW    1=set CLK_READ_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::setClkReadSpi(char setValue) {
    spiSetSelectScNprobe(1);  // switch Gene_ctrl_Reg0(2) to CLK_READ_spi
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);  // read register value
    m_generalCtrlReg0 &= 0xFFFB;                                 // mask the bit
    m_generalCtrlReg0 |= ((setValue & 0x1) << 2);                   // set bit to new setValue
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0); // write register
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         setClkScProbe
//Function:     using signals send by SPI to access probe register of spiroc, set CLk_SC_probe to be hign/low
//Variables:    setValue(char) :  0=set CLK_SC_probe to LOW    1=set CLK_SC_probe to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::setClkScProbe(char setValue) {
    spiSetSelectScNprobe(0);  // switch Gene_ctrl_Reg0(2) to CLK_SC_probe
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);  // read register value
    m_generalCtrlReg0 &= 0xFFFB;                                 // mask the bit
    m_generalCtrlReg0 |= ((setValue & 0x1) << 2);                   // set bit to new setValue
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0); // write register
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         setSrinReadSpi
//Function:     using signals send by SPI to access READ interface of spiroc, set SRIN_READ_spi to be hign/low
//Variables:    setValue(char):  0=set SRIN_READ_spi to LOW    1=set SRIN_READ_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::setSrinReadSpi(char setValue) {
    spiSetSelectScNprobe(1);  // switch Gene_ctrl_Reg0(3) to SRIN_READ_spi
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFF7) | ((setValue & 0x01) << 3);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         setSrinScProbe
//Function:     using signals send by SPI to access probe register of spiroc, set SRIN_SC_probe to be hign/low
//Variables:    setValue(char):  0=set SRIN_SC_probe to LOW    1=set SRIN_SC_probe to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::setSrinScProbe(char setValue) {
    spiSetSelectScNprobe(0);  // switch Gene_ctrl_Reg0(3) to SRIN_SC_probe
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFF7) | ((setValue & 0x01) << 3);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}



//-------------------------------------------------------------------------------------------------------------------//
//Name:         setResetbReadSpi
//Function:     using signals send by SPI to access READ interface of spiroc, set RESETB_READ_spi to be hign/low
//Variables:    setValue(char):  0=set RESETB_READ_spi to LOW    1=set RESETB_READ_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::setResetbReadSpi(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFEF) | ((setValue & 0x01) << 4);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setResetbScSpi
//Function:     using signals send by SPI to access READ interface of spiroc(actually here we access the Slow Control interface,2010.06.14 snow@EPFL), set RESETB_SC_spi to be hign/low 
//Variables:    setValue(char):  0=set RESETB_SC_spi to LOW    1=set RESETB_SC_spi to HIGH                                          
//Returns:      None  
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::setResetbScSpi(char setValue) {
        m_generalCtrlReg0 = spiRegisterRead(0,GENERAL_CTRL_REG0_ADDR);
	m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xF7FF) | ((setValue & 0x01)<<11);
	spiRegisterWrite(0,GENERAL_CTRL_REG0_ADDR,m_generalCtrlReg0);
} 

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setHoldbSpi
//Function:     using signals send by SPI to access READ interface of spiroc, set HOLDB_spi to be hign/low
//Variables:    setValue(char):  0=set Holdb_spi to LOW    1=set Holdb_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::setHoldbSpi(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFDF) | ((setValue & 0x01) << 5);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setSelectForRead
//Function:     select signals to access READ interface of the  FrontEnd chip: 0= spi(set by C code); 1 = uplink(H, HB, S, SB);
//Variables:    setValue(char):  0=use signals from SPI(programed by C code) to access READ register of spiroc
//                               1=use signals from uplink(H, HB, S, SB) to access READ register of spiroc
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::setSelectForRead(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFBF) | ((setValue & 0x01) << 6);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         seTestpulseSpi
//Function:     set Testpulse to be high/Low, when using SPI to access READ interface of the  FrontEnd chip
//Variables:    setValue(char):  0=set Testpulse_spi to LOW    1=set Testpulse_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::setTestpulseSpi(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFBFF) | ((setValue & 0x01) << 10);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setLgEnSpi
//Function:     set lg_EN to be high/Low, when using SPI to access READ interface of the  FrontEnd chip
//Variables:    setValue(char): 0=set LG_EN_spi to be LOW,
//                              1=set LG_EN_spi to be HIGH , select Low gain output from spirocA
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::setLgEnSpi(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xEFFF) | ((setValue & 0x01) << 12);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setHgEnSpi
//Function:     set hg_EN to be high/Low, when using SPI to access READ interface of the  FrontEnd chip
//Variables:    setValue(char): 0=set HG_EN_spi to be LOW,
//                              1=set HG_EN_spi to be HIGH , select high gain output from spirocA
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::setHgEnSpi(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xDFFF) | ((setValue & 0x01) << 13);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         dumpFirmwareVersion
//Function:     Read and dump SPIROCA/SPIROCII card FPGA firmware version
//Variables:    
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
// Read and dump SPIROCA/SPIROCII card FPGA firmware version
void EcalSpiroc::dumpFirmwareVersion() {
    m_generalCtrlReg0 = spiRegisterRead(0, FIRMWARE_VERSION_REG_ADDR);
    std::cerr << "Firmware version v" << m_generalCtrlReg0 << ".0 " << std::endl;
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         dumpFirmwareVersion
//Function:     Read and dump the current status of Gene_ctrl_reg0 in spirocA/II firmware
//Variables:    
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
// Read and dump Gene_ctrl_reg0 status
void EcalSpiroc::dumpGeneCtrlReg0() {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    std::cerr << "Info@Config_spirocA: GENERAL_REG0=";
    for(unsigned int i = 0; i<16; i++)
        std::cerr << ((m_generalCtrlReg0>>(15-i))&0x0001);
    std::cerr << std::endl;
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         resetChipConfig
//Function:     Reset the Slow Control interface by setting bit0 of Reg0(reset_cfg)in spiroc firmware
//Variables:    
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::resetChipConfig() {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);

    m_generalCtrlReg0 |= 0x0001;  // set reset
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);

    m_generalCtrlReg0 &= 0xFFFE; // remove reset
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);

    std::cerr << "resetChipConfig finished!" << std::endl;
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         spiSetTrigSelect
//Function:     set bit8&9 of Reg0(reset_cfg) in spirocA firmware
//Variables:    trigSelect(char):0=set bit9&8 to "00" =>UL_HOLD(use H, HB, S, SB as trigger signal);
//                               1=set bit9&8 to "01" =>NIM_trigger;
//                               2=set bit9&8 to "10" =>LVDS_trigger
//Returns:      bool
//              true = successful
//              false= Error!
//-------------------------------------------------------------------------------------------------------------------//
bool EcalSpiroc::spiSetTrigSelect(char trigSelect) {
    bool err=false;
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);

    switch(trigSelect) {
        case 0: m_generalCtrlReg0 &= 0xFCFF;
                break;
        case 1: m_generalCtrlReg0 &= 0xFDFF;
                m_generalCtrlReg0 |= 0x0100;
                break;
        case 2: m_generalCtrlReg0 &= 0xFEFF;
                m_generalCtrlReg0 |= 0x0200;
                break;
        default: std::cerr << "Info@spiSetTrigSelect:TRIG_TYPE out of range! For spirocA, this could only be select between 0 ~ 2" << std::endl;
                 err = true;
                 assert(false);
    }

    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    return(err);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         spiSetSelectScNprobe
//Function:     set bit7 of Reg0 in spirocA firmware
//Variables:    selectScNprobe(char):0=set bit7 to '0'   => access the Probe register in SpirocA
//                                   1=set bit7 to '1'   => access the Slow Control register in SpirocA
//Returns:      bool
//              true = successful
//              false= Error!
//-------------------------------------------------------------------------------------------------------------------//
bool EcalSpiroc::spiSetSelectScNprobe(char selectScNprobe) {
    bool err=false;
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);

    switch(selectScNprobe) {
        case 0: m_generalCtrlReg0 &= 0xFF7F;
                std::cerr << "select Probe!" << std::endl;
                break;
        case 1: m_generalCtrlReg0 |= 0x0080;
                std::cerr << "select SlowControl!" << std::endl;
                std::cerr << "selectScNprobe: GENERAL_REG0=";
                for(unsigned int i = 0; i<16; i++)
                    std::cerr << ((m_generalCtrlReg0>>(15-i))&0x0001);
                std::cerr << std::endl;
                break;
        default: std::cerr << "Infomation:selectScNprobe out of range! For spirocA, this could only be select as 0 or 1" << std::endl;
                 err = true;
                 assert(false);
    }

    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    return(err);
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        compareUfmToConfigArray
// Function:    compare UFM content to chipConfigArray
// Parameters:  chipConfigArray = 16bits Word Array converted from FrontEndChip config file
//              compareArrayNum = Number of the Array you want to compare
// Returns:     (bool)
//              false = UFM content is identical to chipConfigArray
//              true = UFM content is different to chipConfigArray
//--------------------------------------------------------------------------------------------------------------------//
bool EcalSpiroc::compareUfmToConfigArray(unsigned short chipConfigArray[], unsigned int compareArrayNum) {

    unsigned short chipConfigArray_readBack[50];
    bool err=false;

    spiUfmReadBlock(0, 0x0000, compareArrayNum, chipConfigArray_readBack);

    for(unsigned int i=0; i<compareArrayNum; i++) {
        if (chipConfigArray_readBack[i] != chipConfigArray[i])
        {
            err = true;
            std::cerr << "Infomation:Content in UFM is diffenert from Config file" << std::endl;
            break;
        }
    }
    return(err);
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        writeConfigArrayToUfm
// Function:    write chip Config Array into UFM
// Parameters:  chipConfigArray = 16bits Word Array converted from FrontEndChip config file
//              writeConfigArrayNum = Number of the Array you want to write into UFM
// Return:      None
//---------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::writeConfigArrayToUfm(unsigned short chipConfigArray[], unsigned int writeConfigArrayNum) {
    std::cerr << "Info:Write Config Arrays to UFM....." << std::endl;
    unsigned short addr = 0x0000;
    addr = 0x0000;

    std::cerr << "Erase UFM 1st!" << std::endl;
    spiUfmErase(0);
    sleep(1);   // wait 1s, !!Don't delete it!!  after spiUfmErase(0), we have to wait! Otherwise, ufmerase will not be successful! 

    std::cerr << "Erase UFM 2nd!" << std::endl;
    spiUfmErase(0);
    sleep(1);  // wait 1s, !! Don't delete it!!  after spiUfmErase(0), we have to wait! Otherwise, ufmerase will not be successful!
    
    // std::cerr << "Begin to Write config into UFM!" << std::endl;
    for(unsigned int i=0; i<writeConfigArrayNum; i++, addr++) {
        spiUfmWrite(0, addr, chipConfigArray[i]);
    }
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        sendStartConfigCMD
// Function:    set "start_cfg" in SpirocA/spirocII firmware, the front end board will begin to write slow contro configuration from UFM to Slow Control Register
// Parameters:
// Returns:      None
//---------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::sendStartConfigCMD() {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 |= 0x0002;
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);  // set start_cfg = '1'
    m_generalCtrlReg0 &= 0xFFFD;
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);  // set start_cfg = '0'
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        loadConfigToSpiroc
// Function:    begin to configure Slow Control Register of SpirocA/spirocII by setting "start_cfg". The configuration will be executed twice.compare UFM content with output of SROUT_SC at the end of the second one, in order to check the writing process is correct or not.
// Parameters:
// Returns:(bool)
//              false = configuration succeed
//              true  = configuration error
//--------------------------------------------------------------------------------------------------------------------//
bool EcalSpiroc::loadConfigToSpiroc() {
    bool err = false;
    unsigned short generalCtrlReg0;


    // -------------------1st time config------------------------------
    spiSetSelectScNprobe(1);  // select slow control register

    //resetChipConfig();        // reset
    std::cerr << "resetChipConfig!" << std::endl;

    sendStartConfigCMD();
    std::cerr << "send Start_cfg finished!" << std::endl;

    while (1)
    {
        generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);

        if (generalCtrlReg0 & 0x4000) {//config finished
            std::cerr << "Infomation: 1st load  Config from UFM to Spiroc,   Finished!" << std::endl;
            if (generalCtrlReg0 & 0x8000)    {   //config error
                std::cerr << "ERROR:config error = 1, There is something wrong in SPIROC configuration! Please check!" << std::endl;
            }
            break;
        }
    }

    // --------------------2nd time ------------------------------

    sendStartConfigCMD();
    std::cerr << "Infomation: 2nd Load Config from UFM to Spiroc!...." << std::endl;
    while (1)
    {
        generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);

        if (generalCtrlReg0 & 0x4000)  {   //config finished
            if (generalCtrlReg0 & 0x8000)    {   //config error
                std::cerr << "ERROR:config error = 1, There is something wrong in SPIROC configuration!please check!" << std::endl;
                err = true;
                //assert(false);
            }else{
                std::cerr << "Infomation:Config Spiroc Successfully!" << std::endl;
            }
            break;

        }
    }
    return(err);

}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        spiConfigSpirocA
// Function:    configure Both Gene_Ctrl_Reg0 in spirocA firmware and slow control register in spirocA chip
// Parameters:
// Returns:    (bool)
//              false = configuration succeed
//              true  = configuration error
//--------------------------------------------------------------------------------------------------------------------//
//config the spirocA with the slow control setting
bool EcalSpiroc::spiConfigSpirocA(int dumpOn, unsigned short chipConfigArray[], char trigSelect, char select_read, char selectScNprobe)
{

    bool  err = false;

    std::cerr << "Begin to config SpirocA..." << std::endl;

    if (dumpOn==1) {
        std::cerr << "Info@spiConfigSpirocA:Array write into UFM:" << std::endl;
        for(unsigned int index=0;index<s_configArrayNum_spirocA;index++) {
            std::cerr << "chipConfigArray[" << index << "]=";
            for(unsigned int i = 0; i<16; i++)
                std::cerr << ((chipConfigArray[index]>>(15-i))&0x0001) << " ";
            std::cerr << std::endl;
        }
    }

    // getchar();

    // ---------------------Begin to Config Reg0----------------------------------


    // Read and dump SPIROC card FPGA firmware version
    // Important! In order to confirm the version currently used in Spiroc card firmware
    // Always run this function before other further more commands.
    dumpFirmwareVersion();
    
    // reset the SPIROCA config
    // resetChipConfig();


    //select trigger type (this part is different from SPIROC_II)
    err |= spiSetTrigSelect(trigSelect);

    // set to access Slow Control Register or Probe Register (this part is different from SPIROC_II)
    err |= spiSetSelectScNprobe(selectScNprobe);

    if (dumpOn==1) {                              // if switch on the dump function, then dump debug info
        dumpGeneCtrlReg0();
    }
    // std::cerr << " change debug mode to Slow control" << std::endl;

    // select debug signals : 0=signals about Read interface;  1= signals about Slow Control interface
    // in order to switch to different signals without changing & downloading new Firmware version
    //setScNreadDebug(0);

    // Dump the current status of GeneCtrlReg0
    if (dumpOn==1) {
        dumpGeneCtrlReg0();
    }

    setSelectForRead(select_read); // select Up-Link or direct SPI access of the read interface

    std::cerr << "Info@Config_spirocA:after register config:" << std::endl;
    dumpGeneCtrlReg0();


    //--------------End of Reg0 Config--------------------------------------


    //--------------Begin to write config arrays into UFM ------------------

    // set SPI communication to UsbBoard <-> UFM

    // compare UFM content to chipConfigArray
    // 0 = UFM content is identical to chipConfigArray, skip unnecessary writing config arrays into UFM
    // 1 = UFM content is different to chipConfigArray, write config array again into UFM

     if (compareUfmToConfigArray(chipConfigArray, s_configArrayNum_spirocA) == 1) {
        do{
            writeConfigArrayToUfm(chipConfigArray, s_configArrayNum_spirocA);
        }while(compareUfmToConfigArray(chipConfigArray, s_configArrayNum_spirocA));
      }// continue writing config arrays in UFM until UFM content is identical to chipConfigArray
    std::cerr << "Info@Config_spirocA:Write Config to UFM successfully! " << std::endl;

    // load config file from UFM into SPIROCA
    bool loadConfigErr = false;
    do {
        loadConfigErr=loadConfigToSpiroc();
    }while(loadConfigErr == true);

    //select the read access
    setSelectForRead(select_read);// select Up-Link or direct SPI access of the read interface
    return(err);
}

//-------------------------for Analogue Test of the chip-----------------------------------------
//---------------------------------------------------------------------------------------------------------------------//
// Name:        spirocAnalogTest
// Function:    Use SPI (C code programed) signals to access spiroc READ register, in order to do analog test for the chip
// Parameters:
// Returns:None
//--------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::spirocAnalogTest() {
    setHoldbSpi(1) ;        // set Hold signal to Analogue memory Hold signal

    setResetbReadSpi(0);    // set Resetb_Read
    setResetbReadSpi(1);    // move Resetb_Read

    selectReadChannel(2);
    while(0) {
        std::cerr << "set pulse" << std::endl;
        setTestpulseSpi(1);          // set Testpulse =1
        setTestpulseSpi(0);          // set Testpulse =0
    }

    //       setHoldbSpi(1) ;        // set Hold signal to Analogue memory Hold signal
    //  selectReadChannel(18);
    setHgEnSpi(1);          // select Hign Gain Value
    getchar();
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        setScNreadDebug
// Function:    select signals to be observed from debug pins in SpirocA firmware (only for debug, before use this function, make sure that Gene_Ctrl_Reg0 bit11 has been assaigned as switch between READ interface and Slow Control interface )
// Parameters:
//              setValue(char): 0 = observe signals about READ interface,
//                              1 = observe signals about Slow Control interface
// Returns:     None
//--------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::setScNreadDebug(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);  // read register value
    m_generalCtrlReg0 &= 0xF7FF;                                 // mask the bit
    m_generalCtrlReg0 |= ((setValue & 0x1) << 11);                   // set bit to new setValue
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0); // write register
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        selectContinousMode
// Function:    Use SPI (C code programed) signals to select a certain channel to be readout 
// Parameters:  channelIndex (unsigned int): the selected channel index (0,1....)         
// Returns:None
//--------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::selectContinuousMode(unsigned int channelIndex){
     setSelectForRead(0); // switch to use SPI signals to access spirocA READ register

     setResetbReadSpi(0);    // set Resetb_Read, in order to clear read register
     setResetbReadSpi(1);    // move Resetb_Read
     selectReadChannel(channelIndex);    //selec readout channel
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         resetProbeRegister
//Function:     resetProbeRegister of SpirocA. After configuring spiroc slow control register, this must be done once. Otherwise, the chip will not work.
//Variables:    None                            
//Returns:      None 
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpiroc::resetProbeRegister(){
       spiSetSelectScNprobe(0);   // switch to probe resiger
       dumpGeneCtrlReg0();         // dump the current status of GeneCtrlReg0, in order to check whether bit7=0 or not, only when bit7=0,select probe register is successful!  
       setResetbScSpi(0);
       setResetbScSpi(1);
       std::cerr<<"reset probe register finished!"<<std::endl;
}
