#ifndef VATA64V2_h
#define VATA64V2_h

#include "FrontEndBoard.h"

#include <iostream>
#include <vector>
#include <string>
#include <unistd.h>

#include "../TypeDefs.h"

//#define NEW_USBBOARD

// Define
#define  FIRMWARE_VERSION_REG_ADDR       0x0000
#define  GENERAL_CTRL_REG0_ADDR          0x0001        
//bit0:reset_cfg_reg, bit1:start_cfg_reg
#define  GENERAL_CTRL_REG1_ADDR          0x0002     
//bit4:CLK_READ_ctrl, bit5:SRIN_READ_ctrl, bit6:RESETB_READ_ctrl, bit7:HOLDB_Backup_ctrl,
#define  GENERAL_CTRL_REG2_ADDR          0x0003
#define  GENERAL_CTRL_REG3_ADDR          0x0004        
//bit0 : 1 c-code read access ; 0 USB_board read access, bit1:NPWDA, bit2:NPWDB
#define  GENERAL_CTRL_REG4_ADDR          0x0005        // unused
/////////////////////////////////////////////////////////////////////////////////////
////// in SPIROC-II we only use FIRMWARE_VERSION_REG and GENERAL_CTRL_REG0 //////////
//GENERAL_CTRL_REG0:
//bit0: reset_cfg_reg,    bit1: start_cfg_reg,
//bit2: CLK_READ_ctrl, bit3: SRIN_READ_ctrl, bit4: RESETB_READ_ctrl, bit5: HOLDB_Backup_ctrl,
//bit6: 0 c-code read access ; 1 USB_board read access, 
//bit7: NPWDA--Amplifier for Uplink disable, bit8: NPWDB--Amplifier for debug disable,
//bit9: external trigger enable,
//bit10: TESTPULSE_ctrl,
//bit14: SPIROC-II config done, bit15: SPIROC-II config error. these two bits are read only.
/////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// in SPIROC-A we still only use FIRMWARE_VERSION_REG and GENERAL_CTRL_REG0most bit definitions keep the same as SPIROC-II ///////////
///////Most bit definitions keep the same as SPIROC-II, but there are some small differences( marked with **)                  ///////////
////// Just change the name, function keep the same as before( marked with * )                                                 ///////////
/////  change the function ( marked with **)                                                                                   ///////////

//GENERAL_CTRL_REG0:
//bit0: reset_cfg_reg,     bit1: start_cfg_reg,
//bit2: CLK_READ_spi(*)    bit3: SRIN_READ_spi(*)  bit4: RESETB_READ_spi(*)  bit5: HOLDB_Backup_spi(*)
//bit6: select_for_read     0= c-code read access ; 1= USB_board read access, 
//bit7: select_sc_nPROBE(**)
//bit8&9: TRIG_SELECT(**)
//bit10: TESTPULSE_spi(*) 
//bit12: LG_EN_spi(**)
//bit13: HG_EN_spi(**)
//bit14: config done   (read only)
//bit15: config error. (read only)
/////////////////////////////////////////////////////////////////////////////////////

#define  SPIROC_CFG_RAM_BADDR            0x4000
#define  SPIROC_CFG_RB_RAM_BADDR         0x4080

// UFM CMD
#define UFM_CMD_WREN     0x60            //enable write to UFM
#define UFM_CMD_WRDI     0x04            //----disable write to UFM
#define UFM_CMD_RDSR     0x05            //----read status register
#define UFM_CMD_WRSR     0x01            //----write status register
#define UFM_CMD_READ     0x03            //----read data from UFM
#define UFM_CMD_WRITE    0x02            //----write data to UFM
#define UFM_CMD_SERASE   0x40            //----sector erase
#define UFM_CMD_BERASE   0x06            //----erase the entire UFM block(both sectors)

//QUSB SPI switch CMD
#define UFM_ERASE        0xD3
#define UFM_READ_SR      0xB5
#define UFM_WRITE        0x81
#define UFM_READ         0x42
#define UFM_READ_WORD    0x4A
#define SC_REGISTER      0x60


//
#define  SPItoUFM             0xC3C3
#define  SPItoREGISTER        0x3C3C

#define  SPItoADC             0x5050    //by lht July 20, 13:57. For new parallel spi interface, ADC select
#define  SPItoLED             0xA0A0

struct VATA64V2_CFG_TYPE
{
  int DAC_on;
  int Lg;
  int Rfp_bp;
  int Rfs0_b;
  int Rfs1;
  int Rfs2_b;
  int Lgs;
  int Ssc0_b;
  int Ssc1;
  int Ssc2;
  int Tp50_dc;
  int Bypass;
  int Sel;
  int Shabi_hp1b;
  int Shabi_hp2;
  int Shabi_hp3b;
  int SHsense_en;
  int SHgen_en;
  int Buf_sel;
  int Test_on;
  int Vfp_en;
  int Vfsfclamp_en;
  int reserved;
  int Disable_register[64];
  int Disable_bit_tst_channel;
  int Threshold_alignment_DAC[64];
  int Threshold_alignment_DAC_tst_channel;
  int Preamplifier_input_potential_DAC[64];
  int Preamplifier_input_potential_DAC_tst_channel;
  int Holdbi_Bias_DAC; 
};

struct SENSOR_CAL_PARAMS {
  float cala_biasA;
  float calb_biasA;

  float cala_biasB;
  float calb_biasB;

  float cala_curr;
  float calb_curr;

  float cala_temp;
  float calb_temp;
};

struct SENSORS_INFO {
  float biasA_V;
  float biasB_V;
  float currA_nA;
  float currB_nA;
  float tempA_C;
  float tempB_C;
  int biasA_adc;
  int biasB_adc;
  int currA_adc;
  int currB_adc;
  int tempA_adc;
  int tempB_adc;
};

                                                                 //\|/ should be 00      channel select      on chip REF     power mode
const static unsigned char CMD_Array[14] = {0x00,0x00,             //0b00              000                 0               00
                                                                //---normal mode, power on device, select channel 1 as next sample channel
                                            0x08,0x00,           //0b00 001 0 00 ---normal mode, select channel 2 as next sample channel
                                            0x10,0x00,           //0b00 010 0 00 ---normal mode, select channel 3 as next sample channel
                                            0x20,0x00,           //0b00 100 0 00 ---normal mode, select channel 5 as next sample channel
                                            0x28,0x00,           //0b00 101 0 00 ---normal mode, select channel 6 as next sample channel
                                            0x30,0x00,           //0b00 110 0 00 ---normal mode, select channel 7 as next sample channel
                                            0x00,0x00};          //0b00 000 0 10/00  ---auto shutdown/normal mode, shut down after this read out

enum AD5322_CONTROL{
      SELECT_nA_B = 0x8000,
      REF_BUFFERED = 0x4000,
      NORMAL_OPERATION = 0x0000,
      POWER_DOWN_1K = 0x1000,
      POWER_DOWN_100K = 0x2000,
      POWER_DOWN_HIGH_IMPEDANCE = 0x3000,
      SELECT_nA_B_MASK = 0x8000,
      REF_BUFFERED_MASK = 0x4000,
      OPERATION_MODE_MASK = 0x3000,
      DATA_MASK = 0x0FFF
};

enum LED_SELECT{
     LED_A1 = 0x0400,
     LED_A2 = 0x0800,
     LED_B1 = 0x1000,
     LED_B2 = 0x2000
};

// -----------------   VATA64V2 Configuration  ---------------------


class VATA64V2Configuration : public FrontEndBoardConfiguration {
    public:
        VATA64V2Configuration(int slotNo,int uplinkId);
        ~VATA64V2Configuration();

        void dump(std::ostream& stream = std::cerr) const;
        bool loadFromString(const char*);

/*         int select_nSPI_uplink() const; */
/*         void setSelect_nSPI_uplink(int select_nSPI_uplink); */

        
        int trigSelect() const;
        void setTrigSelect(int trigSelect);
        unsigned short vata64BoardIndex() const;

        void setSlowControlCfgFile(const std::string& fileName);
        const std::string& slowControlCfgFile() const;

        unsigned short m_FEchipConfigArray[80];

        bool loadConfig(const std::string& filename); 
        bool loadFromStream(std::istream& stream);

        void convertCfgToVector(unsigned char temp[456]);

        void writeToStream(std::ostream& stream) const;

        void convertVectorTo16bitsArray(int dumpOn,unsigned char temp[456],unsigned short array[]);

        void chipConfigArray(unsigned short chipCfgArray[],int chipType);

        //  for Analogue Test
        void setPreamplifierInputPortentialDAC(int Value,unsigned short configArray[]); 

        // Add by Snow 2012.Feb.13
        // change m_cfgVATA64.PreamplifierInputPortentialDAC[],
        void SetPreamplifierInputPortentialDACs(int setValues[]);

        // get the currentlly used m_cfgVATA64.PreamplifierInputPortentialDAC[]
        void PreamplifierInputPortentialDACs(unsigned short* DACvalues);

        
        void SetAdjustDACMode(){m_executeAdjustDACs = true;}
        bool IsAdjustDACMode(){return m_executeAdjustDACs;}

        std::string GetPathToAdjustDACs(){return m_pathToAdjustDACs;}

        void SetLedA_DAC(unsigned short DAvalue){ m_ledA_DAC = DAvalue;}
        unsigned short GetLedA_DAC(){ return m_ledA_DAC;}

        void SetLedB_DAC(unsigned short DAvalue){ m_ledB_DAC = DAvalue;}
        unsigned short GetLedB_DAC(){ return m_ledB_DAC;}

        void SetLedA1_On(bool setOn) { m_ledA1_On = setOn;}
        bool IsLedA1_On() { return m_ledA1_On;}

        void SetLedA2_On(bool setOn) { m_ledA2_On = setOn;}
        bool IsLedA2_On() { return m_ledA2_On;}

        void SetLedB1_On(bool setOn) { m_ledB1_On = setOn;}
        bool IsLedB1_On() { return m_ledB1_On;}

        void SetLedB2_On(bool setOn) { m_ledB2_On = setOn;}
        bool IsLedB2_On() { return m_ledB2_On;}

        bool loadBoardSettings(const std::string& filePath);

    private:
        VATA64V2_CFG_TYPE  m_cfgVATA64;
        int m_trigSelect;
        unsigned short m_vata64BoardIndex;   
        std::string m_slowControlCfgFile;
        std::string m_pathToAdjustDACs;
        bool m_executeAdjustDACs;

        bool m_ledA1_On;
        bool m_ledA2_On;
        bool m_ledB1_On;
        bool m_ledB2_On;
        unsigned short m_ledA_DAC;
        unsigned short m_ledB_DAC;
};


                        
// ---------------- VATA64V2 ------------------------------------------------
class VATA64V2 : public FrontEndBoard {
    public:
        VATA64V2(UsbBoard*, FrontEndBoardType, int);
        ~VATA64V2();
    
        virtual bool configure(FrontEndBoardConfiguration*);

        virtual bool readSensor(unsigned short*);       //added for sensor readout on the new vata64 board --by lht
        bool getSensorValues(float*);          //add by snow to give the biasVoltage,current and temperature values instead of their AD values. 2012.01.02
        virtual bool readSensor();

        void DumpOctInBin_8b(uint8_t value);

        void DumpOctInBin_16b(uint16_t value);

     #ifndef NEW_USBBOARD
        void spiRegisterWrite(unsigned char portNum,unsigned short addr,unsigned short data);
        unsigned short spiRegisterRead(unsigned char portNum,unsigned short addr);
        unsigned char spiUfmStatusRead(unsigned char portNum);
        void spiUfmErase(unsigned char portNum);
        void spiUfmWrite(unsigned char portNum,unsigned short addr,unsigned short data);
        unsigned short spiUfmReadWord(unsigned char portNum,unsigned short start_addr);
        void spiUfmReadBlock(unsigned char portNum,unsigned short start_addr,unsigned short word_num,unsigned short data[512]);
#else
        void spiRegisterWrite(uint16_t addr,uint16_t data);
        uint16_t spiRegisterRead(uint16_t addr);
        uint8_t spiUfmStatusRead();
        void spiUfmErase();
        void spiUfmWrite(uint16_t addr,uint16_t data);
        uint16_t spiUfmReadWord(uint16_t start_addr);
        bool spiUfmReadBlock(uint16_t start_addr,uint16_t word_num,uint16_t* data);
#endif

        void selectReadChannel(unsigned int channelIndex);

        void setCkbSpi(char setValue);
        void setShiftInbSpi(char setValue);
        void setDresetSpi(char setValue);
        void setHoldSpi(char setValue) ;
        void setSelect_nSPI_uplink(char setValue);
        void setTestpulseSpi(char setValue);
        void setNPWD(char setValue);                //change for new VA card, since the new one have just one NPWD port
        //void setNPWDA(char setValue);
        //void setNPWDB(char setValue);
        //void setNPWDC(char setValue);

        
        void dumpFirmwareVersion();
        void dumpGeneCtrlReg0();
        void resetChipConfig();     
        void setTrigSelect(char trigSelect);
        bool spiSetSelectScNprobe(char selectScNprobe); 
        bool compareUfmToConfigArray(int dumpOn,unsigned short chipConfigArray[],unsigned int compareArrayNum);      
        void writeConfigArrayToUfm(unsigned short chipConfigArray[], unsigned int writeConfigArrayNum);
        void sendStartConfigCMD();   
        bool loadConfigToVATA64();     


        bool spiConfigVATA64(int dumpOn,unsigned short chipConfigArray[],char trigSelect);

        void analogTest();
        void setScNreadDebug(char setValue);


	// const static unsigned int s_configArrayNum_spirocA = 29;
        const static unsigned int s_configArrayNum_VATA64 = 55;
        void selectContinuousMode(unsigned int channelIndex);


        //------new code for update vata64 cards with LED injection------------------
        //------------------spiWriteAD5322-------------------------//
        //Variable: portNum: SPI device address(nss line) to write to
        //          inputShiftReg: 16bits data written to the input shift register of AD5322
        //---------------------------------------------------------//
#ifndef NEW_USBBOARD
        void spiWriteAD5322(unsigned char portNum,unsigned short inputShiftReg);
#else
        void spiWriteAD5322(uint16_t inputShiftReg);
#endif

        //--------------------setAD5322ChanA-----------------------//
        //Function: set the DA value for channelA (DACA) of AD5322, with Reference_unbuffered and normal_operation mode.
        //Variable:DAvalue: value set to channelA, only the low 12bit effect.
        //---------------------------------------------------------//
        void setAD5322ChanA(unsigned short DAvalue);

        //--------------------setAD5322ChanA-----------------------//
        //Function: set the DA value for channelA (DACB) of AD5322, with Reference_unbuffered and normal_operation mode.
        //Variable: DAvalue: value set to channelB, only the low 12bit effect.
        //---------------------------------------------------------//
        void setAD5322ChanB(unsigned short DAvalue);



        void ledA1On();
        void ledA1Off();

        void ledA2On();
        void ledA2Off();

        void ledB1On();
        void ledB1Off();

        void ledB2On();
        void ledB2Off();

        bool IsLedA1On();
        bool IsLedA2On();
        bool IsLedB1On();
        bool IsLedB2On();
        unsigned short GetLedA_DAC();
        unsigned short GetLedB_DAC();

        virtual void DecreaseLedIntense(unsigned short stepADC);
        virtual void IncreaseLedIntense(unsigned short stepADC);

        virtual void SetAllLedOn(bool on);

	 const static SENSOR_CAL_PARAMS SensorCalParasList[18];
	      
    private:
        unsigned short m_generalCtrlReg0;
        VATA64V2Configuration* m_configuration;
        unsigned short m_spiToUfmRegisterSwitch;
        SENSOR_CAL_PARAMS m_sensorCalParas;
        SENSORS_INFO m_sensorsInfo;


};

#endif
