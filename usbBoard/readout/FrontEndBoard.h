#ifndef FrontEndBoard_h
#define FrontEndBoard_h

#include <iostream>
#include <stdint.h>

//#define NEW_USBBOARD

class UsbBoard;

enum FrontEndBoardType {HpeVa256Type = 0, HpeVa128Type, HpeSpiroc128Type, EcalSpirocType, EcalSpirocIIType, VATA64Type, VATA64V2Type,VATA64V2_DCType, FrontEndBoardTypeEnd};
const char* const FrontEndBoardTypeIdentifier[] = {"HpeVa256", "HpeVa128", "HpeSpiroc128", "EcalSpiroc","EcalSpirocII","VATA64", "VATA64V2Type","VATA64V2_DCType"};

#define DEVICE_BASE_ADDRESS 0xC1
#define DEVICE_BASE_ADDRESS_TWO_CYCLES 0xD1

class FrontEndBoardData {
    public:
        FrontEndBoardData();
        virtual ~FrontEndBoardData();
        
        void setFrontEndBoardType(FrontEndBoardType type) {m_type = type;}
        FrontEndBoardType frontEndBoardType() {return m_type;}

        virtual void dump(std::ostream& stream = std::cerr) const;
    protected:
        FrontEndBoardType m_type;
};

class FrontEndBoardConfiguration {
    public:
        FrontEndBoardConfiguration(FrontEndBoardType, int slotNo, int uplinkId);
        virtual ~FrontEndBoardConfiguration();

        virtual void dump(std::ostream& = std::cerr) {}
        virtual bool loadFromString(const char*) {return false;}
        void setSlotNo(int slotNo){m_slotNo = slotNo;} // !!!m_slotNo counted from 0 （while uplink counts as 1,2,3...8), so m_slotNo = 0 corresponds to uplink1
        int getSlotNo(){return m_slotNo;}
        int m_slotNo;

        virtual const std::string& slowControlCfgFile() const {return " ";}
        virtual void SetPreamplifierInputPortentialDACs(unsigned short setValues[]){}
        virtual void PreamplifierInputPortentialDACs(unsigned short* DACvalues){}

        int GetUplinkId(){ return m_uplinkId;}
    protected:
        FrontEndBoardType m_type;
    private:
        int m_uplinkId;

};

class FrontEndBoard {
    public:
        FrontEndBoard(UsbBoard*, FrontEndBoardType, int);
        virtual ~FrontEndBoard();

        virtual bool configure(FrontEndBoardConfiguration*) {return false;}
        virtual bool readData(FrontEndBoardData&) {return false;}
        virtual void selectContinuousMode(unsigned int channel){ std::cerr<<"@Continuous Mode: channel = "<<channel<<std::endl;}

        virtual bool readSensor(unsigned short*){return false;}            //added for read new VATA64 card. implementation in other FECard is not finish yet  --by lht
        virtual bool readSensor(){return false;}

        virtual void DecreaseLedIntense(unsigned short stepDAC){return;}
        virtual void IncreaseLedIntense(unsigned short stepDAC){return;}
        virtual void SetAllLedOn(bool on){return ;}


        void setFrontEndBoardType(FrontEndBoardType type) {m_type = type;}
        FrontEndBoardType frontEndBoardType() {return m_type;}

        int uplinkId() {return m_uplinkId;}
        void setUplinkId(int id) {m_uplinkId = id;}
	
        int getSlotNo() {return m_slotNo;}
	void setSlotNo(int slot) {m_slotNo = slot;}
	//Added by lht, July 20, 12:25  Used to ID the FE for spi communication

    protected:
        bool read(unsigned char, unsigned char*, unsigned short);
        bool write(unsigned char, unsigned char*, unsigned short);
        bool writeRead(unsigned char, unsigned char*, unsigned short);

        bool read2Cycles(unsigned char, unsigned char*, unsigned short, unsigned char*, unsigned short);
        bool write2Cycles(unsigned char, unsigned char*, unsigned short, unsigned char*, unsigned short);
        bool writeRead2Cycles(unsigned char, unsigned char*, unsigned short, unsigned char*, unsigned short);
        //Add above three lines to use the spi adapter with 2-cycles sequence spi communication. By lht

        //Add for new usbboard
        bool readQSPI(uint16_t* readData, uint16_t cmdStarAdr, uint16_t length);
        bool writeQSPI(uint16_t* writeData, uint16_t cmdStarAdr, uint16_t length);
        bool readWriteQSPI(uint16_t* writeData, uint16_t cmdStarAdr, uint16_t* readData,uint16_t length);

        FrontEndBoardType m_type;
        int m_slotNo; //Added by lht, July 20, 12:25  Used to ID the FE for spi communication
    private:
        int m_uplinkId;
        UsbBoard* m_board;


};

#endif
