TEMPLATE = lib
TARGET = usbBoardReadout

HEADERS+= \
    Settings.h \
    UsbBoardConfiguration.h \
    UsbBoard.h \
    UsbBoardManager.h \
    FrontEndBoard.h \
    HpeVa256.h \
    EcalSpiroc.h \
    EcalSpirocII.h \
    VATA64.h \
    VATA64V2.h \
    VATA64V2_DC.h

SOURCES+= \
    Settings.cpp \
    UsbBoardConfiguration.cpp \
    UsbBoard.cpp \
    UsbBoardManager.cpp \
    FrontEndBoard.cpp \
    HpeVa256.cpp \
    EcalSpiroc.cpp \
    EcalSpirocII.cpp \
    VATA64.cpp \
    VATA64V2.cpp \
    VATA64V2_DC.cpp

exists($(USBBOARDPATH)) {
    include($(USBBOARDPATH)/usbBoard.pri)
    include($(USBBOARDPATH)/RootCInt.pri)
    USBBOARDPATH=$(USBBOARDPATH)
} else {
    include(../usbBoard.pri)
    include(../RootCInt.pri)
    USBBOARDPATH=..
}
DEPENDPATH+= \
    $$USBBOARDPATH \
    $$USBBOARDPATH/support \
    $$USBBOARDPATH/readout \
    $$USBBOARDPATH/event

INCLUDEPATH+= \
    $$DEPENDPATH

LIBS+=\
    -L$$USBBOARDPATH$${QMAKE_DIR_SEP}Builds -lusbBoardEvent

!NO_QUSB {
    LIBS += -L$$USBBOARDPATH$${QMAKE_DIR_SEP}support
    SOURCES += ../support/CQuickUsb.cpp

    unix {
        LIBS += -lusb -lquickusb
    }

    win* {
        LIBS += -lQuickUsb
    }
}

win* {
    DEF_FILE=readout.def
}
