#ifndef  Settings_H
#define  Settings_H

#include <string>
#include <vector>
#include <map>

#include "UsbBoardConfiguration.h"
#include "VATA64V2.h"

class RunDescription;
class MergedEvent;


class Settings
{
    public:
        virtual ~Settings();
        static Settings* instance();

        MergedEvent* createMergedEvent() const;
        RunDescription* createRunDescription(unsigned long runNumber, const std::string& comment) const;
        
        void setConfigPath(const std::string&);
        void setDumpUplink(int dumpUplink) {m_dumpUplink = dumpUplink;}
        int getDumpUplink() {return m_dumpUplink;}

	void setConfigFpgaOption(bool option) {m_configFpgaOption = option;}
	bool configFpgaOption() {return m_configFpgaOption;}

        void SetAdjustDACMode(bool option){m_adjustDACMode = option;}
        bool IsAdjustDACMode(){return m_adjustDACMode;}

        void SetDAClistDir(const std::string DirOFDAClists){m_DAClistDir = DirOFDAClists;}
        std::string GetDAClistDir(){return m_DAClistDir;}

        bool addUsbBoard(int id);

        unsigned int numberOfUsbBoards() const;
        bool usbBoardIdExists(int id) const;
        bool uplinkIdExists(int id) const;
        bool usbBoardIdToIndex(int id, unsigned int& index) const;
        unsigned int usbBoardIndex(int id) const;
        bool usbBoardIndexToId(unsigned int index, int& id) const;
        int usbBoardIdFromUplinkId(int uplinkId);
        
        int usbBoardId(unsigned int index) const;
        int usbBoardIdFromUplinkId(int uplinkId) const;
        int uplinkId(unsigned int usbBoardIndex, unsigned int uplinkSlot) const;
        const UsbBoardConfiguration& usbBoardConfiguration(int usbBoardId);
        
        void writeToStream(std::ostream& stream) const;
        bool loadFromStream(std::istream& stream);
        bool loadFromString(const std::string& str);
        bool loadFromRunDescription(const RunDescription& runDescription);

        TriggerMode triggerMode() const {return m_triggerMode;}
        int eventsPerAccess() const {return m_eventsPerAccess;}
        const std::string& firmwareFileName() const {return m_firmwareFileName;}

        std::string GetUplinkConfigPath(unsigned int usbBoardId,unsigned int uplinkSlot) const; //uplinkSlot counted from 0
        void AddSensorInfos(int uplinkId,const std::string sensorInfo);
        std::string GetSensorInfos();
        std::string GetSensorInfo(int uplinkId);

    private:
        Settings();

	std::vector <std::vector <std::string> > m_uplinkComment;
        static Settings* s_instance;
        
        void clearSettings();
        
        int m_eventsPerAccess;
        TriggerMode m_triggerMode;
        std::string m_configPath;
        std::string m_firmwareFileName;
        std::vector<UsbBoardConfiguration> m_usbBoardConfiguration;

	bool m_configFpgaOption;
        int m_dumpUplink;
        bool m_adjustDACMode;
        std::string m_DAClistDir;

        std::vector <std::vector <std::string> > m_slowControlPathPerBoard;

        void loadAllSlowControlPath();

        std::map<int,std::string> m_mapSensorInfos;


};

#endif

