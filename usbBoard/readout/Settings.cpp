#include "Settings.h"

#include <RunDescription.h>
#include <MergedEvent.h>

#include <assert.h>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstring>
#include <limits.h>

Settings* Settings::s_instance = 0;

Settings::Settings()
    : m_eventsPerAccess(-1)
    , m_uplinkComment(0)	
    , m_triggerMode(triggerModeEnd)
    , m_configPath("")
    , m_firmwareFileName("")
    , m_usbBoardConfiguration(0)
    , m_configFpgaOption(false)
    , m_DAClistDir("")
   // , m_slowControlPathPerBoard("")
{
}

Settings::~Settings()
{
}

Settings* Settings::instance()
{
    if (!s_instance)
        s_instance = new Settings;
    return s_instance;
}

MergedEvent* Settings::createMergedEvent() const
{
    MergedEvent* event = new MergedEvent();
    std::vector<UsbBoardConfiguration>::const_iterator usbBoardConfigurationIt = m_usbBoardConfiguration.begin();
    std::vector<UsbBoardConfiguration>::const_iterator usbBoardConfigurationItEnd = m_usbBoardConfiguration.end();
    for (; usbBoardConfigurationIt != usbBoardConfigurationItEnd; ++usbBoardConfigurationIt) {
        int usbBoardId = usbBoardConfigurationIt->usbBoardId();
        unsigned short sampleSize = usbBoardConfigurationIt->sampleSize();
        for (unsigned short uplinkSlot = 1; uplinkSlot <= s_uplinksPerUsbBoard; ++uplinkSlot) {
            int uplinkId = usbBoardConfigurationIt->uplinkId(uplinkSlot);
            event->addUplink(usbBoardId, uplinkId, sampleSize);
        }
    }
    return event;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         setConfigPath
//Function:     store config path to a private variable for Class Settings
//Variables:    path: the Path of the documment which keeps all the config files for UsbBoard as well as Front End chip
//Return:       None
//-------------------------------------------------------------------------------------------------------------------//
void Settings::setConfigPath(const std::string& path)      
{
    m_configPath = path;
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         addUsbBoard
//Function:     
//Variables:    
//Return:       bool            true=            false=
//-------------------------------------------------------------------------------------------------------------------//  
bool Settings::addUsbBoard(int id)
{
  UsbBoardConfiguration config;
  std::cout<<"a variable config of type UsbBoardConfiguration have been created"<<std::endl;
  std::cout<<"we call config.loadFromFile(m_configPath, id). This function create a stream, for example: board09.cfg, if id == 9. Then, it calls loadFromStream."
	<< std::endl << std::endl;
  if (config.loadFromFile(m_configPath, id)) {
    if (m_usbBoardConfiguration.size()) {
      if (config.eventsPerAccess() != m_eventsPerAccess || config.triggerMode() != m_triggerMode || config.firmwareFileName() != m_firmwareFileName) {
	std::cerr << "Failure: The software cannot handle USB board configurations with different firmware, eventsPerAccess or triggerMode." 
			<< std::endl << "Please check the config files!" << std::endl;
	assert(false);
      }
    } else {
      m_eventsPerAccess = config.eventsPerAccess();
      m_triggerMode = config.triggerMode();
      m_firmwareFileName = config.firmwareFileName();
    }
    config.writeToStream(std::cerr); // print on screen

    std::vector<UsbBoardConfiguration>::iterator configIt = m_usbBoardConfiguration.begin();
    bool configExists = false;    
    while ( (configIt != m_usbBoardConfiguration.end()) && (!configExists) ){
	configExists = ( configIt -> usbBoardId() == id );
	++configIt;
    }
    if (configExists){
	--configIt;
	m_usbBoardConfiguration.erase(configIt);
    }
    m_usbBoardConfiguration.push_back(config);     // save usbBoard cfg & FE chip cfg to the corresponding USBboard

    if (!configExists) m_uplinkComment.push_back(std::vector<std::string>(s_uplinksPerUsbBoard, ""));
    unsigned int boardIndex = usbBoardIndex(id);
    for (int i = 0 ; i!= s_uplinksPerUsbBoard ; ++i){
	unsigned int uplinkSlot=i+1;
        char tmp[40];
        sprintf(tmp,"board%i_slot%i",boardIndex,uplinkSlot);
	m_uplinkComment.at(boardIndex).at(uplinkSlot-1) = tmp;
    }

    return true;
  }
  return false;
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         numberOfUsbBoards
//Function:     Get number of UsbBoards attached 
//Variables:    None
//Returns:      return the number of UsbBoards attached  
//-------------------------------------------------------------------------------------------------------------------//   
unsigned int Settings::numberOfUsbBoards() const
{
    return m_usbBoardConfiguration.size();
}
//--------------------------------------------------------------------------------------------------------------------//
//Name:         usbBoardIdExists
//Function:     Check whether the config info of this USBBOARD exists in m_usbBoardConfiguration or not. 
//              For each attached UsbBoard which has been detected successfully, its config info will be read & stored 
//           in private variable-- vector m_usbBoardConfiguration. 
//Variables:    id:   ID of the required usbBoard ( coming from the serial number of each QUSB Device on the UsbBoard) 
//Returns:      (bool)  true = usbBoard with this ID exsits
//                      false= usbBoard with this ID does not exist 
//-------------------------------------------------------------------------------------------------------------------// 
bool Settings::usbBoardIdExists(int id) const
{
    unsigned int index = 0;
    return usbBoardIdToIndex(id, index);
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         uplinkIdExists
//Function:     Check whether a certain uplink ID exists among all the  usbBoards that has already been detected 
//Variables:    id:   uplinki ID of the required usbBoard ( set in the UsbBoard config file ) 
//Returns:      (bool)  true = this uplink ID has already  exsited
//                      false= no such uplink ID
//-------------------------------------------------------------------------------------------------------------------// 
bool Settings::uplinkIdExists(int id) const
{
    std::vector<UsbBoardConfiguration>::const_iterator usbBoardConfigurationIt = m_usbBoardConfiguration.begin();
    std::vector<UsbBoardConfiguration>::const_iterator usbBoardConfigurationItEnd = m_usbBoardConfiguration.end();
    for (unsigned int i = 0; usbBoardConfigurationIt != usbBoardConfigurationItEnd; ++usbBoardConfigurationIt, ++i) {
        if (usbBoardConfigurationIt->uplinkIdExists(id))
            return(true);
    }
    return(false);
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         usbBoardIdToIndex
//Function:     convert usbBoard ID to the corresponding index in  m_usbBoardConfiguration
//Variables:    id (int):              ID of the required usbBoard 
//                                                  ( coming from the serial number of each QUSB Device on the UsbBoard)
//              index (unsigned int&): corresponding index number of this required usbBoard
//                                                  ( only depending on the reading & storing cfg info order of usbBoards)
//Returns:      (bool)  true = usbBoard with this ID exsits
//                      false= usbBoard with this ID does not exist 
//-------------------------------------------------------------------------------------------------------------------// 
bool Settings::usbBoardIdToIndex(int id, unsigned int& index) const
{
    std::vector<UsbBoardConfiguration>::const_iterator usbBoardConfigurationIt = m_usbBoardConfiguration.begin();
    std::vector<UsbBoardConfiguration>::const_iterator usbBoardConfigurationItEnd = m_usbBoardConfiguration.end();
    for (unsigned int i = 0; usbBoardConfigurationIt != usbBoardConfigurationItEnd; ++usbBoardConfigurationIt, ++i) {
        if (usbBoardConfigurationIt->usbBoardId() == id) {
            index = i;
            return true;
        }
    }
    return false;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         usbBoardIndex
//Function:     For the  UsbBoard with given ID, get its corresponding index in  m_usbBoardConfiguration 
//Variables:    id (int):    ID of the required usbBoard ( coming from the serial number of each QUSB Device on the UsbBoard)
//Returns:      (unsigned int)  return the corresponding index of the UsbBoard with the given ID
//               This index only depends on the reading & storing cfg info order of usbBoards)
//-------------------------------------------------------------------------------------------------------------------// 
unsigned int Settings::usbBoardIndex(int id) const
{
    unsigned int index = UINT_MAX;
    assert(usbBoardIdToIndex(id, index));
    return index;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         usbBoardIndexToId
//Function:     for the member of m_usbBoardConfiguration with a given index, get its corresponding UsbBoard ID 
//              For each attached UsbBoard which has been detected successfully, its config info will be read & stored in private variable-- vector m_usbBoardConfiguration.
//Variables:    index (unsigned int):  index as the member of m_usbBoardConfiguration( only depending on the reading & storing cfg info order of usbBoards)   
//Returns:      (unsigned int)  return the corresponding index of the UsbBoard with the given ID
//               This index only depends on the reading & storing cfg info order of usbBoards)
//-------------------------------------------------------------------------------------------------------------------// 
bool Settings::usbBoardIndexToId(unsigned int index, int& id) const
{
    if (index < numberOfUsbBoards()) {
        id = usbBoardId(index);
        return true;
    }
    return false;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         usbBoardId
//Function:     for the member of m_usbBoardConfiguration with a given index, return its corresponding UsbBoard ID 
//              For each attached UsbBoard which has been detected successfully, its config info will be read & stored in private variable-- vector m_usbBoardConfiguration.
//Variables:    index (unsigned int):  index as the member of m_usbBoardConfiguration( only depending on the reading & storing cfg info order of usbBoards)   
//Returns:      (int)  return the corresponding ID number of the UsbBoard, whose config info is stored in a member of m_usbBoardConfiguration with the given index  
//               This index only depends on the reading & storing cfg info order of usbBoards)
//-------------------------------------------------------------------------------------------------------------------// 
int Settings::usbBoardId(unsigned int index) const
{
    return m_usbBoardConfiguration.at(index).usbBoardId();
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         usbBoardIdFromUplinkId
//Function:     Refering to the given uplinkId, find the corresponding usbBoard ID 
//Variables:    uplinkId(int):  uplinkId number
//Returns:      (int) if this given uplinkId exists, return its corresponding usbBoard ID,
//                    if not,return 0 
//-------------------------------------------------------------------------------------------------------------------// 
int Settings::usbBoardIdFromUplinkId(int uplinkId)
{
    std::vector<UsbBoardConfiguration>::const_iterator usbBoardConfigurationIt = m_usbBoardConfiguration.begin();
    std::vector<UsbBoardConfiguration>::const_iterator usbBoardConfigurationItEnd = m_usbBoardConfiguration.end();
    for (; usbBoardConfigurationIt != usbBoardConfigurationItEnd; ++usbBoardConfigurationIt) {
        for (unsigned short i = 1; i <= s_uplinksPerUsbBoard; ++i)
            if (usbBoardConfigurationIt->uplinkId(i) == uplinkId)
                return usbBoardConfigurationIt->usbBoardId();
    }
    assert(false);
    return 0;
}
//--------------------------------------------------------------------------------------------------------------------//
//Name:        createRunDescription
//Function:     
//Variables:   
//Returns:      
//-------------------------------------------------------------------------------------------------------------------// 
RunDescription* Settings::createRunDescription(unsigned long runNumber, const std::string& comment) const
{
    RunDescription* runDescription = new RunDescription;
    runDescription->setRunNumber(runNumber);
    runDescription->setComment(comment);
    std::stringstream settingsStream;
    writeToStream(settingsStream);
    runDescription->setSettings(settingsStream.str());
    runDescription->setTimeStamp(time(0));
    return runDescription;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         uplinkId
//Function:     
//Variables:   
//Returns:      
//-------------------------------------------------------------------------------------------------------------------// 
int Settings::uplinkId(unsigned int usbBoardId, unsigned int uplinkSlot) const
{
    return m_usbBoardConfiguration.at(usbBoardIndex(usbBoardId)).uplinkId(uplinkSlot);
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:          usbBoardConfiguration
//Function:      Refering to a given usbBoard ID, find the corresponding member of m_usbBoardConfiguration which stores all the config info of this UsbBoard
//Variables:     usbBoardId(int) : UsbBoard ID Number ( coming from the serial number of the QUSB device on this usbBoard)
//Returns:       (UsbBoardConfiguration) return the corresponding member of m_usbBoardConfiguration which stores all the config info of this UsbBoard
//-------------------------------------------------------------------------------------------------------------------//
const UsbBoardConfiguration& Settings::usbBoardConfiguration(int usbBoardId)
{
    unsigned int index = 0;
    if (!usbBoardIdToIndex(usbBoardId, index)) {
        std::cerr << "Failure: Requested UsbBoardConfiguration for a board that does not exist." << std::endl;
        assert(false);
    }
    return m_usbBoardConfiguration.at(index);
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         writeToStream
//Function:     Currently, it is used to display the config info of all the usbBoards which has been detected
//Variables:    stream(ostream)    *ostream objects are stream objects used to write and format ouput as sequences of characters 
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//  
void Settings::writeToStream(std::ostream& stream) const
{
    std::vector<UsbBoardConfiguration>::const_iterator usbBoardConfigurationIt = m_usbBoardConfiguration.begin();
    std::vector<UsbBoardConfiguration>::const_iterator usbBoardConfigurationItEnd = m_usbBoardConfiguration.end();
    for (; usbBoardConfigurationIt != usbBoardConfigurationItEnd; ++usbBoardConfigurationIt)
        usbBoardConfigurationIt->writeToStream(stream);
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadFromStream
//Function:     extract config information from the given stream, and store it in a new member of m_usbBoardConfiguration
//Variables:    stream(istream)    *istream objects are stream objects used to read and interpret input form sequences of characters
//Returns:      (bool) return 0 when operation is successful
//                     return 1 when Errors exist
//-------------------------------------------------------------------------------------------------------------------// 
bool Settings::loadFromStream(std::istream& stream)
{
    bool err = 0;
    while (1) {
        UsbBoardConfiguration config;
        if (!config.loadFromStream(stream)) err = true;
        if (stream.eof())
            break;
        m_usbBoardConfiguration.push_back(config);

        m_uplinkComment.push_back(std::vector<std::string>(s_uplinksPerUsbBoard, ""));
        unsigned int boardIndex = m_usbBoardConfiguration.size()-1;
        for (int i = 0 ; i!=s_uplinksPerUsbBoard ; ++i){
           unsigned int uplinkSlot=i+1;
           //m_usbBoardConfiguration.at(boardIndex).setUplinkId(uplinkSlot, uplinkId[i]);
           char tmp[40];
           sprintf(tmp,"board%i_slot%i",boardIndex,uplinkSlot);
  	   m_uplinkComment.at(boardIndex).at(uplinkSlot-1) = tmp;
         }

    }
    return err;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadFromString
//Function:     change a string into stringstream, then extract config information from this stream
//Variables:    str( string&)
//Returns:      (bool) return 0 when operation is successful
//                     return 1 when Errors exist
//-------------------------------------------------------------------------------------------------------------------// 
bool Settings::loadFromString(const std::string& str)
{
    std::stringstream stream(str);
    return loadFromStream(stream);
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadFromRunDescription
//Function:     
//Variables:   
//Returns:      
//-------------------------------------------------------------------------------------------------------------------// 
bool Settings::loadFromRunDescription(const RunDescription& runDescription)
{
    return loadFromString(runDescription.settings());
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         clearSettings
//Function:     
//Variables:   
//Returns:      
//-------------------------------------------------------------------------------------------------------------------// 

void Settings::clearSettings()
{
    m_eventsPerAccess = -1;
    m_usbBoardConfiguration.clear();
}


void Settings::loadAllSlowControlPath(){
    std::vector<UsbBoardConfiguration>::const_iterator usbBoardConfigurationIt = m_usbBoardConfiguration.begin();
    std::vector<UsbBoardConfiguration>::const_iterator usbBoardConfigurationItEnd = m_usbBoardConfiguration.end();
    for (;usbBoardConfigurationIt != usbBoardConfigurationItEnd; ++usbBoardConfigurationIt) m_slowControlPathPerBoard.push_back(std::vector<std::string>(s_uplinksPerUsbBoard,""));

    usbBoardConfigurationIt = m_usbBoardConfiguration.begin();
    usbBoardConfigurationItEnd = m_usbBoardConfiguration.end();
    for(int boardIndex = 0;usbBoardConfigurationIt != usbBoardConfigurationItEnd; ++usbBoardConfigurationIt,++boardIndex){
        for(int slot =0; slot<s_uplinksPerUsbBoard;slot++){
            if(usbBoardConfigurationIt->executeFrontEndBoardConfiguration(slot+1)){
                  m_slowControlPathPerBoard.at(boardIndex).at(slot) = usbBoardConfigurationIt->frontEndBoardConfiguration(slot+1)->slowControlCfgFile();
                  std::cout<<"m_slowControlPathPerBoard.at("<<boardIndex<<").at("<<slot<<")="<<m_slowControlPathPerBoard.at(boardIndex).at(slot)<<std::endl;
            }
        }
    }

}

std::string Settings::GetUplinkConfigPath(unsigned int usbBoardId, unsigned int uplinkSlot) const {
    return m_slowControlPathPerBoard.at(usbBoardIndex(usbBoardId)).at(uplinkSlot);
}


void Settings::AddSensorInfos(int uplinkId, const std::string sensorInfo){
     std::map<int,std::string>::iterator it;

     it = m_mapSensorInfos.find(uplinkId);

     if(it == m_mapSensorInfos.end()){
         //if sensors from this uplinkId has not been recorded before
         m_mapSensorInfos.insert(make_pair(uplinkId,sensorInfo));
     }else{
         //if sensors from this uplinkId has been recorded, change the values.
         it->second = sensorInfo;
     }
}

std::string Settings::GetSensorInfos(){
     std::stringstream sensorInfos;
     std::map<int,std::string>::iterator it;
     for(it=m_mapSensorInfos.begin();it!=m_mapSensorInfos.end();it++){
         sensorInfos << it->second <<"\n";
     }
     return sensorInfos.str();
}
