#include "HpeVa256.h"

#include <sstream>
#include <math.h>

const float HpeVa256::m_refVoltage = 4.096f;
const float HpeVa256::m_temperatureLSB = 0.03125f;

void HpeVa256Data::dump(std::ostream& stream) const {
    stream
        << "xx"
        << "yy"
        << std::endl;
}

HpeVa256Configuration::HpeVa256Configuration(int slotNo,int uplinkId)
    : FrontEndBoardConfiguration(HpeVa256Type, slotNo,uplinkId)
{}

HpeVa256Configuration::~HpeVa256Configuration()
{}

void HpeVa256Configuration::dump(std::ostream& stream) const {
    stream
        << "xx"
        << "yy"
        << std::endl;
};

bool HpeVa256Configuration::loadFromString(const char* string) {
    //parse stream
    return true;
}

HpeVa256::HpeVa256(UsbBoard* board, FrontEndBoardType type, int slotNo)
    : FrontEndBoard(board, type, slotNo)
{
}

HpeVa256::~HpeVa256()
{}

bool HpeVa256::configure(HpeVa256Configuration)
{
    read(1, 0, 1);
    return true;
}

void HpeVa256::readData(HpeVa256Data& data)
{
    data.setTemperature(1, -20.2);
}

bool HpeVa256::sendOpCode(unsigned char code){
    unsigned short length = 1;
    unsigned char opCodeSS = 0;
    write(opCodeSS, &code, length);
    return true;
}

bool HpeVa256::readTemperature(unsigned int sensorNumber, float& temperature){

    bool result = true;

    switch(sensorNumber){
        case UPPER_TEMPERATURE: result &= sendOpCode(0x82); break;
        case LOWER_TEMPERATURE: result &= sendOpCode(0x84); break;
    }

    unsigned char temperatureBits;
    result = read(0, &temperatureBits, m_commandByteSize);

    /* throw away trailing dont care bits */
    temperatureBits = temperatureBits << 2;

    /* copy bits to float, shift float and multiply with LSB */
    float tmpTemperature = (float) temperatureBits;
    int* temperaturePtr = (int*) &tmpTemperature;
    (*temperaturePtr) = (*temperaturePtr) << 8;

    temperature = tmpTemperature*m_temperatureLSB;

    return result;
}

bool HpeVa256::setDacVoltage(unsigned int dacNumber, float voltage){
    std::stringstream stream;

    /* leading dont care bits */
    stream << "00000000";

    /* write and update */
    stream << "0011";

    /* select channel */
    stream << std::bitset<4>(dacNumber);

    //TODO: ceil, floor, or round?
    long binaryvalue = floor(voltage*(1 << m_dacResolution) / m_refVoltage);
    stream << std::bitset<8>(binaryvalue);

    /* fill stream */
    stream << std::bitset<8>(0).to_string();

    unsigned char* data = commandBlockToData(CommandBits(stream.str()));

    return write(0, data, m_commandByteSize);
}

bool HpeVa256::initializeDac(){
    /* power down, choose external reference */

    bool result = true;
    std::stringstream stream;

    /* choose external reference */
    stream << "0111";
    /* same reference for all channels */
    stream << "1111";
    /* fill stream */
    stream << std::bitset<16>(0).to_string();

    unsigned char* data = commandBlockToData(CommandBits(stream.str()));
    result &= write(0, data, m_commandByteSize);
    /* reference source set */

    delete data; //necessary?

    stream.clear();

    /* power down all channels */
    /* leading dont care bits */
    stream << "00000000";

    /* choose all channels */
    stream << "0101";

    /* dont care */
    stream << "0000";

    /* fill up */
    stream << std::bitset<16>(0).to_string();

    data = commandBlockToData(CommandBits(stream.str()));

    result &= write(0, data, m_commandByteSize);

    return result;

}

unsigned char* HpeVa256::commandBlockToData(const CommandBits&) const
{
    //TODO
    return 0;
}
