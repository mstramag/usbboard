#include "UsbBoardConfiguration.h"
#include "EcalSpiroc.h"
#include "EcalSpirocII.h"
#include "VATA64.h"
#include "VATA64V2.h"
#include "VATA64V2_DC.h"
#include "Settings.h"

#include <assert.h>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <sys/stat.h> 
#include  <cstring>
#include "../TypeDefs.h"

UsbBoardConfiguration::UsbBoardConfiguration()
    : m_usbBoardId(-1)
    , m_withUplinkAdapter(false)
    , m_holdDelayTime(-1)
    , m_holdType(-1)
    , m_calDelay(-1)
    , m_delayAfterReadout(-1)
    , m_clkSelect(-1)
    , m_fifoWriteDelay(-1)
    , m_busyOutputSwitch(-1)
    , m_eventsPerAccess(-1)
    , m_readoutMode(readoutModeEnd)
    , m_triggerMode(triggerModeEnd)
    , m_firmwareFileName("")
    , m_biasVoltageX(0.0)
    , m_biasVoltageY(0.0)
    , m_biasSelect('F')
    , m_ledInjectionModeOn(false)//Pujiang
{
#ifdef NEW_USBBOARD
    m_selfTrigEnalbe = false;
    m_selfTrigPeriod = 0x00009C40;
    m_selfTrigLeadTime = 0x00002EE0;
#endif
    for (unsigned int i = 0; i < s_uplinksPerUsbBoard; ++i) {
        m_uplinkId[i] = -1;
        m_comment[i] = "";
        m_frontEndBoardType[i] = FrontEndBoardTypeEnd;
        m_executeFrontEndBoardConfiguration[i] = false;
        m_frontEndBoardConfiguration[i] = 0;
        m_adjustDACMode[i] = false;
    }
}
UsbBoardConfiguration::~UsbBoardConfiguration()
{
  //    for (unsigned int i = 0; i < s_uplinksPerUsbBoard; ++i) {
  //    if (m_frontEndBoardConfiguration[i]) {
  //        delete m_frontEndBoardConfiguration[i];
  //        m_frontEndBoardConfiguration[i] = 0;
  //    }
  //}
}

//----------------------------------------------------------------------------//
//Name:          usbBoardId
//Function:      provide usbBoard ID to other Classes which need this value.
//               Because m_usbBoardId is a private variable for Class UsbBoardConfiguration, which only can be accessed by Class UsbBoardConfiguration itself
//Parameters:    None
//Returns:       (int) return usbBoard ID
//---------------------------------------------------------------------------//
int UsbBoardConfiguration::usbBoardId() const
{
    return m_usbBoardId;
}

//----------------------------------------------------------------------------//
//Name:          setUsbBoardId
//Function:      set UsbBoardId without the config file
//Parameters:    id(int): ID number which will be assigned to the usbBoard 
//Returns:       None
//---------------------------------------------------------------------------//
void UsbBoardConfiguration::setUsbBoardId(int id)
{
    m_usbBoardId = id;
}

bool UsbBoardConfiguration::withUplinkAdapter() const
{
    return m_withUplinkAdapter;
}
bool UsbBoardConfiguration::ledInjectionModeOn() const
{
    return m_ledInjectionModeOn;
}
//----------------------------------------------------------------------------//
//Name:          holdDelayTime
//Function:      provide the value of holdDelayTime to other Classes which need this value.
//               Because m_holdDelayTime is a private variable for Class UsbBoardConfiguration, which only can be accessed by Class UsbBoardConfiguration itself
//Parameters:    None
//Returns:       (int) return holdDelayTime corresponding to the config of UsbBoard firmware
//               which represents the Hold signal sent by UsbBoard will be delayed by holdDelayTime*10ns clk cycles
//---------------------------------------------------------------------------//
int UsbBoardConfiguration::holdDelayTime() const   
{
    return m_holdDelayTime;
}

//----------------------------------------------------------------------------//
//Name:          setHoldDelayTime
//Function:      set holdDelayTime to the UsbBoard without refering to the config file
//Parameters:    setHoldDelayTime(int) : no more than a 16bits number!
//Returns:       None
//---------------------------------------------------------------------------//
void UsbBoardConfiguration::setHoldDelayTime(int holdDelayTime) 
{
    m_holdDelayTime = holdDelayTime;
}

//----------------------------------------------------------------------------//
//Name:          holdType
//Function:      provide the value of holdType to other Classes which need this value.
//               Because m_holdType is a private variable for Class UsbBoardConfiguration, which only can be accessed by Class UsbBoardConfiguration itself
//Parameters:    None
//Returns:       (int) return holdType corresponding to the config of UsbBoard firmware, which is a 8bits number, (LSB to MSB => uplink 1~8). 
//               for each bit, 0=fast hold time(no delay), 1= slow hold time (insert holdDelayTime)
//---------------------------------------------------------------------------//
int UsbBoardConfiguration::holdType() const   
{
    return m_holdType;
}

//----------------------------------------------------------------------------//
//Name:          setHoldType
//Function:      set holdType to the UsbBoard without refering to the config file
//Parameters:    setHoldType(int) : no more than a 8bits number!
//Returns:       None
//---------------------------------------------------------------------------//
void UsbBoardConfiguration::setHoldType(int holdType)
{
    m_holdType = holdType;
}

//----------------------------------------------------------------------------//
//Name:          calDelay
//Function:      provide the value of calDelay to other Classes which need this value.
//               Because m_calDelay is a private variable for Class UsbBoardConfiguration, which only can be accessed by Class UsbBoardConfiguration itself
//Parameters:    None
//Returns:       (int) return the value of calDelay, which is set to UsbBoard firmware.
//               calDelay is a 16bits number,representing delay in 10ns clock cycles between calibration pulse and the hold (H) signal assignment
//---------------------------------------------------------------------------//
int UsbBoardConfiguration:: calDelay() const   
{
    return m_calDelay;
}

//----------------------------------------------------------------------------//
//Name:          setCalDelay
//Function:      set calDelay to the UsbBoard without refering to the config file
//Parameters:    calDelay(int) :a 16bits number,representing delay in 10ns clock cycles between calibration pulse and the hold (H) signal assignment
//               This number should be set to 20  ?  (01.06.2010)
//Returns:       None
//---------------------------------------------------------------------------//
void UsbBoardConfiguration::setCalDelay(int calDelay) 
{
    m_calDelay = calDelay;
}

//----------------------------------------------------------------------------//
//Name:          delayAfterReadout
//Function:      provide the value of delayAfterReadout to other Classes which need this value.
//               Because m_delayAfterReadout is a private variable for Class UsbBoardConfiguration, which only can be accessed by Class UsbBoardConfiguration itself
//Parameters:    None
//Returns:       (int) return the value of delayAfterReadout, which is set to UsbBoard firmware.
//               delayAfterReadout is a 16bits number,representing number of 10ns clock cycles to wait after each event readout from the front end
//---------------------------------------------------------------------------//
int UsbBoardConfiguration::delayAfterReadout() const   
{
    return m_delayAfterReadout;
}

//----------------------------------------------------------------------------//
//Name:          setDelayAfterReadout
//Function:      set delayAfterReadout to the UsbBoard without refering to the config file
//Parameters:    delayAfterReadout(int) :a 16bits number,representing number of 10ns clock cycles to wait after each event readout from the front end
//               This number should be set to 1000  ?   (01.06.2010)
//Returns:       None
//---------------------------------------------------------------------------//
void UsbBoardConfiguration::setDelayAfterReadout(int delayAfterReadout) 
{
    m_delayAfterReadout = delayAfterReadout;
}

//----------------------------------------------------------------------------//
//Name:          clkSelect
//Function:      provide the value of clkSelect to other Classes which need this value.
//               Because m_clkSelect is a private variable for Class UsbBoardConfiguration, which only can be accessed by Class UsbBoardConfiguration itself
//Parameters:    None
//Returns:       (int) return the value of clkSelect, which is set to UsbBoard firmware.
//               0= select single clock sequence
//               1= select double clock sequence 
//---------------------------------------------------------------------------//
int UsbBoardConfiguration::clkSelect() const   
{
    return m_clkSelect;
}

//----------------------------------------------------------------------------//
//Name:          setClkSelect
//Function:      set clkSelect to the UsbBoard without refering to the config file
//Parameters:    clkSelect(int) :only 1bit ! So this value can only be set to 0 or 1. Otherwise the program will break off with an error report!
//               0= select single clock sequence
//               1= select double clock sequence 
//Returns:       None
//---------------------------------------------------------------------------//
void UsbBoardConfiguration::setClkSelect(int clkSelect) 
{
    m_clkSelect = clkSelect;
}

//----------------------------------------------------------------------------//
//Name:          fifoWriteDelay
//Function:      provide the value of fifoWriteDelay to other Classes which need this value.
//               Because m_fifoWriteDelay is a private variable for Class UsbBoardConfiguration, which only can be accessed by Class UsbBoardConfiguration itself
//Parameters:    None
//Returns:       (int) return the value of fifoWriteDelay, which is set to UsbBoard firmware.
//               The data valid at the input of the Fifos (delay to adjust with the sampling time of the ADC)will be delayed for fifoWriteDelay*10ns clock cycle
//---------------------------------------------------------------------------//
int UsbBoardConfiguration::fifoWriteDelay() const   
{
    return m_fifoWriteDelay;
}

//----------------------------------------------------------------------------//
//Name:          setFifoWriteDelay
//Function:      set fifoWriteDelay to the UsbBoard without refering to the config file
//Parameters:    fifoWriteDelay(int) :a 16bits number,representing the data valid at the input of the Fifos (delay to adjust with the sampling time of the ADC)will be delayed for fifoWriteDelay*10ns clock cycle
//                 
//Returns:       None
//---------------------------------------------------------------------------//
void UsbBoardConfiguration::setFifoWriteDelay(int fifoWriteDelay) 
{
    m_fifoWriteDelay = fifoWriteDelay;
}

//----------------------------------------------------------------------------//
//Name:          busyOutputSwitch
//Function:      provide the value of busyOutputSwitch to other Classes which need this value.
//               Because m_busyOutputSwitch is a private variable for Class UsbBoardConfiguration, which only can be accessed by Class UsbBoardConfiguration itself
//Parameters:    None
//Returns:       (int) return the value of busyOutputSwitch, which is set to UsbBoard 
//               0= no operation
//               1= impose busy output 1, in order to fake a busy state
//---------------------------------------------------------------------------//
int UsbBoardConfiguration::busyOutputSwitch() const   
{
    return m_busyOutputSwitch;
}

//----------------------------------------------------------------------------//
//Name:          setBusyOutputSwitch
//Function:      set fifoWriteDelay to the UsbBoard without refering to the config file
//Parameters:    fifoWriteDelay(int) :a 16bits number,representing the data valid at the input of the Fifos (delay to adjust with the sampling time of the ADC)will be delayed for fifoWriteDelay*10ns clock cycle
//                 
//Returns:       None
//---------------------------------------------------------------------------//
void UsbBoardConfiguration::setBusyOutputSwitch(int busyOutputSwitch) 
{
    m_busyOutputSwitch = busyOutputSwitch;
}

//----------------------------------------------------------------------------//
//Name:          readoutMode
//Function:      provide the value of readoutMode to other Classes which need this value.
//               Because m_readoutMode is a private variable for Class UsbBoardConfiguration, which only can be accessed by Class UsbBoardConfiguration itself
//Parameters:    None
//Returns:       (int) return the value of readoutMode, which is set to UsbBoard 
//               ReadoutMode is an enumerated type 
//               0= va32
//               1= spirocEpfl
//               2= spirocRwth
//               3= amsK
//               4= amsS
//               5= hpe256
//               6= spirocA
//               7= vata64
//               8= readoutModeEnd
//---------------------------------------------------------------------------//
ReadoutMode UsbBoardConfiguration::readoutMode() const
{
    return m_readoutMode;
}

//----------------------------------------------------------------------------//
//Name:          setReadoutMode
//Function:      set UsbBoard readoutMode without refering to the config file
//Parameters:    mode(ReadoutMode) :an enumerated variable. 
//               Currently, this could only be set to 0~7. Otherwise,the program will break out and give an error report!(01.06.2010)
//               0= va32
//               1= spirocEpfl
//               2= spirocRwth
//               3= amsK
//               4= amsS
//               5= hpe256
//               6= spirocA
//               7= vata64               
//Returns:       None
//---------------------------------------------------------------------------//
void UsbBoardConfiguration::setReadoutMode(ReadoutMode mode)
{
    m_readoutMode = mode;
}

//----------------------------------------------------------------------------//
//Name:          triggerMode
//Function:      provide the UsbBoard trigger type to other Classes which need this value.
//               Because m_triggerMode is a private variable for Class UsbBoardConfiguration, which only can be accessed by Class UsbBoardConfiguration itself
//Parameters:    None
//Returns:       (int) return the UsbBoard trigger type 
//               TriggerMode is an enumerated type 
//               0= internal trigger
//               1= external trigger
//               2= calibration 
//               3= triggerModeEnd
//---------------------------------------------------------------------------//
TriggerMode UsbBoardConfiguration::triggerMode() const
{
    return m_triggerMode;
}

//----------------------------------------------------------------------------//
//Name:          setTriggerMode
//Function:      set UsbBoard triggerMode without refering to the config file
//Parameters:    mode(TriggerMode) :an enumerated variable. 
//               Currently, this could only be set to 0~2. Otherwise,the program will break out and give an error report!(01.06.2010)
//               0= internal trigger
//               1= external trigger
//               2= calibration     
//Returns:       None
//---------------------------------------------------------------------------//
void UsbBoardConfiguration::setTriggerMode(TriggerMode mode)
{
    m_triggerMode = mode;
}

//----------------------------------------------------------------------------//
//Name:          uplinkId
//Function:      get uplinkId of the given uplink
//Parameters:    uplinkSlot: could only be 1~8, corresponding to uplink0~7 on UsbBoard
//Returns:       (int) return the uplinkId number
//---------------------------------------------------------------------------//
int UsbBoardConfiguration::uplinkId(unsigned short uplinkSlot) const
{
    if (1 <= uplinkSlot && uplinkSlot <= s_uplinksPerUsbBoard)
        return m_uplinkId[uplinkSlot-1];
    std::cerr << "Failure: 1 <= uplinkSlot && uplinkSlot <= s_uplinksPerUsbBoard." << std::endl;
    assert(false);
    return 0;
}

//----------------------------------------------------------------------------//
//Name:          setUplinkId
//Function:      For a given usbBoard, set uplinkId of a given uplink without refering to the config file
//Parameters:    uplinkSlot(unsigned short) : representing which uplink will be set, this value could only be 1~8, corresponding to uplink0~7 on UsbBoard
//               id(int):                     uplinkId which will be set to this uplink 
//Returns:       None
//---------------------------------------------------------------------------//
void UsbBoardConfiguration::setUplinkId(unsigned short uplinkSlot, int id)
{
    if (1 <= uplinkSlot && uplinkSlot <= s_uplinksPerUsbBoard) {
        m_uplinkId[uplinkSlot-1] = id;
        return;
    }
    std::cerr << "Failure: 1 <= uplinkSlot && uplinkSlot <= s_uplinksPerUsbBoard." << std::endl;
    assert(false);
}

//----------------------------------------------------------------------------//
//Name:          uplinkIdExists
//Function:      check whether a given uplinkId exists or not
//Parameters:    id(int):   the uplinkId value to be checked 
//Returns:       (bool)
//               true = the given uplinkId value exist
//               false= the given uplinkId value doesn't exist
//---------------------------------------------------------------------------//
bool UsbBoardConfiguration::uplinkIdExists(int id) const
{
    for (unsigned int i = 0; i < s_uplinksPerUsbBoard; ++i)
        if (m_uplinkId[i] == id)
            return true;
    return false;
}

//----------------------------------------------------------------------------//
//Name:          setFirmwareFileName
//Function:      set a new path to the usbBoard firmware without refering to the config file
//Parameters:    fileName(string) : the format is like "/home/pebs_ecal/usbBoard/readout/readoutSettings/va32_readout_top.rbf"
//Returns:       None
//---------------------------------------------------------------------------//
void UsbBoardConfiguration::setFirmwareFileName(const std::string& fileName)
{
    m_firmwareFileName = fileName;
}

//----------------------------------------------------------------------------//
//Name:          firmwareFileName     
//Function:      provide path to the UsbBoard firmware to other Classes which need this value.
//               Because m_firmwareFileName is a private variable for Class UsbBoardConfiguration, which only can be accessed by Class UsbBoardConfiguration itself
//Parameters:    None
//Returns:       (string) return path to the UsbBoard firmware 
//---------------------------------------------------------------------------//
const std::string& UsbBoardConfiguration::firmwareFileName() const
{
    return m_firmwareFileName;
}

//----------------------------------------------------------------------------//
//Name:          sampleSize    
//Function:      return the sampleSize value for a given readoutMode of the UsbBoard
//Parameters:    mode(ReadoutMode) :an enumerated variable. 
//               Currently, this could only be set to 0~7. Otherwise,the program will break out and give an error report!(01.06.2010)
//               0= va32
//               1= spirocEpfl
//               2= spirocRwth
//               3= amsK
//               4= amsS
//               5= hpe256
//               6= spirocA
//               7= vata64
//               8= vata64V2
//Returns:       (unsigned short) return UsbBoard sampleSize
//---------------------------------------------------------------------------//
unsigned short UsbBoardConfiguration::sampleSize(const ReadoutMode& mode)
{
    switch(mode) {
        case va32: return 4*32;
        case spirocEpfl: return 36;
        case spirocRwth: return 4*32;
        case amsK: return 6*64;
        case amsS: return 5*64;
        case hpe256: return 256;    
        case spirocA: return 64;
        case vata64:  return 64;
        //case vata64V2: return 64;
        default : ;
    }
    std::cerr<<"Info@Class UsbBoardConfiguration: ReadoutMode is out of range, it could only be 0~7"<<std::endl;
    assert(false);
    return 0;
}

//----------------------------------------------------------------------------//
//Name:          sampleSize    
//Function:      return the sampleSize value corresponding to the ReadoutMode set in usbBoard config File
//Parameters:    None  
//Returns:       (unsigned short) return UsbBoard sampleSize
//---------------------------------------------------------------------------//
unsigned short UsbBoardConfiguration::sampleSize() const
{
    return sampleSize(m_readoutMode);
}

//----------------------------------------------------------------------------//
//Name:          channelsPerReadoutChip    
//Function:      return number of channels corresponding to  a given readoutMode of the UsbBoard
//Parameters:    mode(ReadoutMode) :an enumerated variable. 
//               Currently, this could only be set to 0~7. Otherwise,the program will break out and give an error report!(01.06.2010)
//               0= va32
//               1= spirocEpfl
//               2= spirocRwth
//               3= amsK
//               4= amsS
//               5= hpe256
//               6= spirocA
//               7= vata64
//               8= vata64V2
//Returns:       (unsigned short) return number of channels for each front end chip
//---------------------------------------------------------------------------//
unsigned short UsbBoardConfiguration::channelsPerReadoutChip(const ReadoutMode& mode)
{
    switch(mode) {
        case va32: return 32;
        case spirocEpfl: return 36;
        case spirocRwth:  return 32;
        case amsK: return 64;
        case amsS:  return 64;
        case hpe256: return 32;  
        case spirocA:  return 32;
        case vata64: return 64;
        //case vata64V2: return 64;
        default : ;
    }
    assert(false);
    return 0;
}

//----------------------------------------------------------------------------//
//Name:          channelsPerReadoutChip    
//Function:      return number of channels corresponding to the ReadoutMode set in usbBoard config File
//Parameters:    None  
//Returns:       (unsigned short) return number of channels for each front end chip
//---------------------------------------------------------------------------//
unsigned short UsbBoardConfiguration::channelsPerReadoutChip() const
{
    return channelsPerReadoutChip(m_readoutMode);
}

//----------------------------------------------------------------------------//
//Name:          loadFromFile   
//Function:      Refering to the given path, automaticly form corresponding path to the config file of the given Usbboard, open file and extract config info, keep them as private variables for Class UsbBoardConfiguration
//Parameters:    filePath(string): Path to the fold where all the config files are stored, the form is like "/home/pebs_ecal/usbBoard/readout/readoutSettings" 
//Returns:       (bool) 
//               true = read & store UsbBoard config file successfully
//               false= errors exsit
//---------------------------------------------------------------------------//
bool UsbBoardConfiguration::loadFromFile(const std::string& filePath, int boardId)
{
    std::stringstream stream;
    stream << filePath << "/board" << std::setw(2) << std::setfill('0') <<  boardId << ".cfg";     
    return loadFromFile(stream.str());
}

bool UsbBoardConfiguration::loadFromFile(const std::string& fileName) {
    std::cerr << "Information: Loading configuration from file " << fileName << std::endl;
    std::ifstream stream(fileName.c_str());
    if (!stream){
      std::cerr << "Failure: Could not open usbBoard configure file: \"" << fileName.c_str() << "\". " << std::endl;
      std::cerr << "Please check if the path is right or the configure file exists!" << std::endl;          //---------added by lht 15:01@29-4-2011
      assert(false);
      return false;
    }
    return loadFromStream(stream);
}

void UsbBoardConfiguration::setFrontEndBoardType(unsigned short slot, FrontEndBoardType type)
{
    assert(1 <= slot && slot <= s_uplinksPerUsbBoard);
    m_frontEndBoardType[slot-1] = type;
}

FrontEndBoardType UsbBoardConfiguration::frontEndBoardType(unsigned short slot) const
{
    assert(1 <= slot && slot <= s_uplinksPerUsbBoard);
    return m_frontEndBoardType[slot-1];
}

void UsbBoardConfiguration::setExecuteFrontEndBoardConfiguration(unsigned short slot, bool value)
{
    assert(1 <= slot && slot <= s_uplinksPerUsbBoard);
    m_executeFrontEndBoardConfiguration[slot-1] = value;
}

bool UsbBoardConfiguration::executeFrontEndBoardConfiguration(unsigned short slot) const
{
    assert(1 <= slot && slot <= s_uplinksPerUsbBoard);
    return m_executeFrontEndBoardConfiguration[slot-1];
}

void UsbBoardConfiguration::setFrontEndBoardConfiguration(unsigned short slot, FrontEndBoardConfiguration* configuration)
{
    assert(1 <= slot && slot <= s_uplinksPerUsbBoard);
    m_frontEndBoardConfiguration[slot-1] = configuration;
}

FrontEndBoardConfiguration* UsbBoardConfiguration::frontEndBoardConfiguration(unsigned short slot) const
{
    assert(1 <= slot && slot <= s_uplinksPerUsbBoard);
    return m_frontEndBoardConfiguration[slot-1];
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadFromStream
//Function:     extract config information from the given stream, and store them as private variables for Class UsbBoardConfiguration
//Variables:    stream(istream)    *istream objects are stream objects used to read and interpret input form sequences of characters
//Returns:      (bool) return 0 when operation is successful
//                     return 1 when Errors exist
//-------------------------------------------------------------------------------------------------------------------// 
bool UsbBoardConfiguration::loadFromStream(std::istream& stream) {
    bool isConfigurationFile = false; //Checks if "# USBBoard config file" occurs
    unsigned int err = 1;   
    unsigned int line =1;        

    std::string s;
    while (true) {
        std::getline(stream, s);

        if (stream.eof())
            break;
        const char* searchString = s.c_str();

        err = 1;         // During each loop, err will be set back to 0, as long as one sentence in correct format has been read.Otherwise it will stay in 1             
        if (s.find("#") == 0) {
            if (s.find("# USBBoard config file") == 0 ) {isConfigurationFile = true;}
            if (s.find("# End of the config file") == 0) {err = 0;break;}
            err = 0;
            ++line;
            continue; // Skip commented lines
        }

	// char cString[s_cStringLength];
	//	char cString[100];
 
        char* cString = new char[s.size()+1];
        
        if (sscanf(searchString, "pathToUsbBoardFirmware || %[^\n]", cString) == 1) {
	  std::cout<< "cString size=" << strlen(cString) <<std::endl;
            m_firmwareFileName = cString; ++line; err = 0;
	    std::cout<< "m_firmware="<<m_firmwareFileName<<"///  size="<<m_firmwareFileName.size()<<std::endl;
        }  
        delete [] cString;

        if (sscanf(searchString, "usbBoardID || %d ||", &m_usbBoardId) == 1) {printf("usbBoardId=%d \n",m_usbBoardId); ++line; err = 0;}

        int withAdapter = 0;            //added by lht, for uplink adapter
        if (sscanf(searchString, "withUplinkAdapter || %d ||", &withAdapter) == 1) {
            printf("withUplinkAdapter=%d \n",withAdapter);
            m_withUplinkAdapter = (withAdapter == 0 ? false : true);
            ++line;
            err = 0;
        }

        if (sscanf(searchString, "eventsPerAccess || %u ||", &m_eventsPerAccess) == 1) {printf("eventsPerAccess=%d \n",m_eventsPerAccess);++line; err = 0;}

        int readoutModeNumber = -1;
        if (sscanf(searchString, "modeSel || %d ||", &readoutModeNumber) == 1) {
            printf("mode=%d \n",readoutModeNumber);
            m_readoutMode = ReadoutMode(readoutModeNumber);
            ++line; 
            err = 0;
            //if ((unsigned int)2 * m_eventsPerAccess * sampleSize() > s_physicalFifoSize) {
            if ((unsigned int)1 * m_eventsPerAccess * sampleSize() > s_physicalFifoSize) { // optimize fifo size - Olivier 31.10.2016
                std::cerr
                    << "Failure: Not enough space for m_eventsPerAccess == "
                    << m_eventsPerAccess << " events on USB board FIFO." << std::endl;
                assert(false);
                return false;
            }
        }

#ifndef NEW_USBBOARD
        int triggerModeNumber = -1;
        if (sscanf(searchString, "triggerType || %d ||", &triggerModeNumber) == 1) {
             printf("triggerType=%d \n",triggerModeNumber);
             m_triggerMode = TriggerMode(triggerModeNumber); ++line; err = 0;
        }
#else
        unsigned short selfTrigOn = 0;
        if (sscanf(searchString, "selfTrigEnable || %d ||", &selfTrigOn) == 1) {
            printf("selfTrigOn=%d \n",selfTrigOn);
            m_selfTrigEnalbe  = (selfTrigOn == 0 ? false : true);
            ++line;
            err = 0;
        }

        if (sscanf(searchString, "selfTrigPeriod || %d ||", &m_selfTrigPeriod) == 1) {
            printf("selfTrigPeriod=%d \n",m_selfTrigPeriod);
            ++line;
            err = 0;
        }

        if (sscanf(searchString, "selfTrigLeadPeriod || %d ||", &m_selfTrigLeadTime) == 1) {
            printf("selfTrigLeadPeriod=%d \n",m_selfTrigLeadTime);
            ++line;
            err = 0;
        }

#endif

        unsigned short ledInjectionModeOn = 0;
        if (sscanf(searchString, "ledInjectionModeOn || %d ||", &ledInjectionModeOn) == 1) {
            printf("ledInjectionModeOn=%d \n",ledInjectionModeOn);
            m_ledInjectionModeOn = (ledInjectionModeOn == 0 ? false : true);
            ++line;
            err = 0;
        }//Pujiang


        if (sscanf(searchString, "holdDelayTime || %d ||", &m_holdDelayTime)==1) {
             printf("holdDelayTime=%d \n",m_holdDelayTime);
             ++line; err = 0;}

        //TODO: change that, it's not readable and not type safe.
        int uplink[s_uplinksPerUsbBoard];
        if (sscanf(searchString, "holdType || %d || %d || %d || %d || %d || %d || %d || %d ||",
                    &uplink[0], &uplink[1], &uplink[2], &uplink[3], &uplink[4], &uplink[5], &uplink[6], &uplink[7]) == 8) {
            printf("holdType || %d || %d || %d || %d || %d || %d || %d || %d || \n",
		   uplink[0], uplink[1], uplink[2], uplink[3], uplink[4], uplink[5], uplink[6], uplink[7]);
            setHoldType(
                    uplink[0] | 
                    ((uplink[1]<<1) & 0x0002) | 
                    ((uplink[2]<<2) & 0x0004) | 
                    ((uplink[3]<<3) & 0x0008) |
                    ((uplink[4]<<4) & 0x0010) |
                    ((uplink[5]<<5) & 0x0020) |
                    ((uplink[6]<<6) & 0x0040) |
                    ((uplink[7]<<7) & 0x0080)
                    );
            ++line; 
            err = 0;
        }

        if (sscanf(searchString, "uplinkID || %d || %d || %d || %d || %d || %d || %d || %d ||",
            &m_uplinkId[0], &m_uplinkId[1], &m_uplinkId[2], &m_uplinkId[3], &m_uplinkId[4], &m_uplinkId[5], &m_uplinkId[6], &m_uplinkId[7])==8) {
          printf("uplinkID || %d || %d || %d || %d || %d || %d || %d || %d || \n",
		 m_uplinkId[0], m_uplinkId[1], m_uplinkId[2], m_uplinkId[3], m_uplinkId[4], m_uplinkId[5], m_uplinkId[6], m_uplinkId[7]) ;  
        ++line; err = 0;}

        //char comment[s_cStringLength][s_uplinksPerUsbBoard];    //----deleted by lht 11:41@29-4-2011
	char comment[s_uplinksPerUsbBoard][s_cStringLength];      //----added by lht 11:42@29-4-2011
        //TODO: Spaces cannot be used inside a comment so far.
        if (sscanf(searchString, "uplinkUse || %s || %s || %s || %s || %s || %s || %s || %s ||",
            comment[0], comment[1], comment[2], comment[3], comment[4], comment[5], comment[6], comment[7]) == 8) {
            printf(searchString, "uplinkUse || %s || %s || %s || %s || %s || %s || %s || %s ||\n",
		   comment[0], comment[1], comment[2], comment[3], comment[4], comment[5], comment[6], comment[7]) ;
            for (unsigned int i = 0; i < s_uplinksPerUsbBoard; ++i)
                m_comment[i] = comment[i];
            ++line;
            err = 0;
        }
        if (sscanf(searchString, "calDelay || %d ||", &m_calDelay)==1) {printf( "calDelay=%d\n", m_calDelay); ++line; err = 0;}
        if (sscanf(searchString, "delayAfterReadout || %d ||", &m_delayAfterReadout)==1) {printf("delayAfterReadout=%d \n", m_delayAfterReadout);++line; err = 0;}
        if (sscanf(searchString, "clkSelect || %d ||", &m_clkSelect)==1) {printf("clkSelect= %d \n", m_clkSelect); ++line; err = 0;}
        if (sscanf(searchString, "fifoWriteDelay || %d ||", &m_fifoWriteDelay)==1) {printf("fifoWriteDelay= %d \n", m_fifoWriteDelay);++line; err = 0;}
        if (sscanf(searchString, "busyOutputSwitch || %d ||", &m_busyOutputSwitch)==1) {printf("busyOutputSwitch= %d \n", m_busyOutputSwitch);++line; err = 0;}
        if (sscanf(searchString, "biasSelect || %c ||", &m_biasSelect)==1) {printf("biasSelect = %c \n", m_biasSelect); ++line; err = 0;}
        if (sscanf(searchString, "biasVoltage || %f || %f ||", &m_biasVoltageX, &m_biasVoltageY)==2) {printf("biasVoltageX = %5.2f, biasVoltageY = %5.2f \n", m_biasVoltageX, m_biasVoltageY); ++line; err = 0;}
        int tmp[s_uplinksPerUsbBoard];
        if (sscanf(searchString, "frontEndBoardType || %d || %d || %d || %d || %d || %d || %d || %d ||",
            &tmp[0], &tmp[1], &tmp[2], &tmp[3], &tmp[4], &tmp[5], &tmp[6], &tmp[7])) {
            printf( "frontEndBoardType || %d || %d || %d || %d || %d || %d || %d || %d ||\n",
		    tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6], tmp[7]);
            for (unsigned int uplink = 0; uplink < s_uplinksPerUsbBoard; ++uplink) {
                switch (tmp[uplink]) {
                    case 0:
                        m_frontEndBoardType[uplink] = HpeVa256Type;
                        //m_frontEndBoardConfiguration[uplink] = new HpeVa256Configuration;
                        break;
                    case 1:
                        m_frontEndBoardType[uplink] = HpeVa128Type;
                        //m_frontEndBoardConfiguration[uplink] = new HpeVa128Configuration;
                        break;
                    case 2:
                        m_frontEndBoardType[uplink] = HpeSpiroc128Type;
                        //m_frontEndBoardConfiguration[uplink] = new HpeSpiroc128Configuration;
                        break;
                    case 3:
                        m_frontEndBoardType[uplink] = EcalSpirocType;
                        m_frontEndBoardConfiguration[uplink] = new EcalSpirocConfiguration(uplink,m_uplinkId[uplink]);
                        break;

                    case 4:
                        m_frontEndBoardType[uplink] = EcalSpirocIIType;
                        m_frontEndBoardConfiguration[uplink] = new EcalSpirocIIConfiguration(uplink,m_uplinkId[uplink]);
                        break;

                    case 5:
                        m_frontEndBoardType[uplink] = VATA64Type;
                        m_frontEndBoardConfiguration[uplink] = new VATA64Configuration(uplink,m_uplinkId[uplink]);
                        break;
                    case 6:
                        m_frontEndBoardType[uplink] = VATA64V2Type;
                        m_frontEndBoardConfiguration[uplink] = new VATA64V2Configuration(uplink,m_uplinkId[uplink]);
                        break;
	            case 7:
		       m_frontEndBoardType[uplink] = VATA64V2_DCType;
                       m_frontEndBoardConfiguration[uplink] = new VATA64V2_DCConfiguration(uplink,m_uplinkId[uplink]);
                        break;

                    default:
                        m_frontEndBoardType[uplink] = FrontEndBoardTypeEnd; 
                }
            }
            ++line;
            err = 0;
        }

        if (sscanf(searchString, "executeFrontEndBoardConfiguration || %d || %d || %d || %d || %d || %d || %d || %d ||",
            &tmp[0], &tmp[1], &tmp[2], &tmp[3], &tmp[4], &tmp[5], &tmp[6], &tmp[7])) {
            printf( "executeFrontEndBoardConfiguration || %d || %d || %d || %d || %d || %d || %d || %d ||\n",
		    tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6], tmp[7]);
            for (unsigned int uplink = 0; uplink < s_uplinksPerUsbBoard; ++uplink)
                m_executeFrontEndBoardConfiguration[uplink] = (tmp[uplink] == 0 ? false : true);
            ++line;
            err = 0;
        }

        if (sscanf(searchString, "adjustDACMode || %d || %d || %d || %d || %d || %d || %d || %d ||",
            &tmp[0], &tmp[1], &tmp[2], &tmp[3], &tmp[4], &tmp[5], &tmp[6], &tmp[7])) {
            printf( "adjustDACMode || %d || %d || %d || %d || %d || %d || %d || %d ||\n",
                    tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6], tmp[7]);
            for (unsigned int uplink = 0; uplink < s_uplinksPerUsbBoard; ++uplink)
                m_adjustDACMode[uplink] = (tmp[uplink] == 0 ? false : true);
            ++line;
            err = 0;
        }
	
        for (unsigned int uplink = 0; uplink < s_uplinksPerUsbBoard; ++uplink) {            // if it is executeFrontEndBoardConfiguration for this uplink is set to 1, load more infomation on frontEnd board configuration. 
	  if (m_frontEndBoardConfiguration[uplink])  {  
            //std::cout<<"m_frontEndBoardConfiguration["<< uplink << "]" << std::endl;
                if (m_frontEndBoardConfiguration[uplink]->loadFromString(searchString)) {
                    ++line;
                    err = 0;
                }
	  }
        }

        if (err) {
            std:: cerr << "Failure: Corrupted config file, check line " << line << "!" << std:: endl;
            assert(false);
            return false; 
        }
    }

    return true;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         writeToStream
//Function:     Currently, it is used to display the config info of a single UsbBoard
//Variables:    stream(ostream)    *ostream objects are stream objects used to write and format ouput as sequences of characters 
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------// 
void UsbBoardConfiguration::writeToStream(std::ostream& stream) const
{
    stream
        << "# USBBoard config file" << std::endl
        << "usbBoardID || " << m_usbBoardId << " ||" << std::endl
        << "pathToUsbBoardFirmware || " << m_firmwareFileName << std::endl
        << "withUplinkAdapter || "<< m_withUplinkAdapter <<std::endl
        << "eventsPerAccess || " << m_eventsPerAccess << " ||" << std::endl
        << "modeSel || " << m_readoutMode << " ||" << std::endl
       #ifndef NEW_USBBOARD
        << "triggerType || " << m_triggerMode << " ||" << std::endl
       #else
        << "selfTrigEnalbe || "<< m_selfTrigEnalbe <<" ||"<<std::endl
        << "selfTrigPeriod || "<< m_selfTrigPeriod <<" ||"<<std::endl
        << "selfTrigLeadTime ||"<< m_selfTrigLeadTime <<" ||"<<std::endl
       #endif
        << "ledInjectionModeOn || " << m_ledInjectionModeOn << " || "<<std::endl
        << "holdDelayTime || " << m_holdDelayTime << " ||" << std::endl
        << "calDelay || " << m_calDelay << " ||" << std::endl
        << "delayAfterReadout || " << m_delayAfterReadout << " ||" << std::endl
        << "clkSelect || " << m_clkSelect << " ||" << std::endl
        << "fifoWriteDelay || " << m_fifoWriteDelay << " ||" << std::endl
        << "busyOutputSwitch || " << m_busyOutputSwitch << " ||" << std::endl
        << "biasSelect || " << m_biasSelect << " ||" << std::endl
        << "biasVoltage ||" << m_biasVoltageX << " || " << m_biasVoltageY << " ||" << std::endl;

    stream
        << "holdType";
    for (unsigned int j = 0; j < s_uplinksPerUsbBoard; ++j)
        stream << " || " << ((m_holdType >> j) & 0x0001); //TODO
    stream << " ||" << std::endl;

    stream
        << "uplinkID";
    for (unsigned int j = 0; j < s_uplinksPerUsbBoard; ++j) {
        stream << " || " << m_uplinkId[j];
    }
    stream << " ||" << std::endl;

    stream
        << "uplinkUse";
    for (unsigned int j = 0; j < s_uplinksPerUsbBoard; ++j) {
        stream << " || " << m_comment[j];
    }
    stream << " ||" << std::endl;

    stream << "frontEndBoardType";
    for (unsigned int j = 0; j < s_uplinksPerUsbBoard; ++j) {
        stream << " || " << (m_frontEndBoardConfiguration[j] ? m_frontEndBoardType[j] : -1);
    }
    stream << " ||" << std::endl;

    stream << "executeFrontEndBoardConfiguration";
    for (unsigned int j = 0; j < s_uplinksPerUsbBoard; ++j) {
        stream << " || " << (m_frontEndBoardConfiguration[j] && m_executeFrontEndBoardConfiguration[j] ? '1' : '0');
    }
    stream << " ||" << std::endl;

    stream << "adjustDACMode";
    for (unsigned int j = 0; j < s_uplinksPerUsbBoard; ++j) {
        stream << " || " << (m_adjustDACMode[j] ? '1' : '0');
    }
    stream << " ||" << std::endl;

    stream
        << "# End of the config file" << std::endl;
}

bool UsbBoardConfiguration::executeAdjustDACMode(unsigned short slot) const{
    assert(1 <= slot && slot <= s_uplinksPerUsbBoard);
    return m_adjustDACMode[slot-1];
}


#ifdef NEW_USBBOARD
bool UsbBoardConfiguration::IsSelfTrigEnable()const { return m_selfTrigEnalbe;}
uint32_t UsbBoardConfiguration::SelfTrigPeriod()const {return m_selfTrigPeriod;}
uint32_t UsbBoardConfiguration::SelfTrigLeadTime()const {return m_selfTrigLeadTime;}
#endif
