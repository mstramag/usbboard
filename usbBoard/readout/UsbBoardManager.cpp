#include "UsbBoardManager.h"
#include "UsbBoard.h"
#include "Settings.h"
#include "FrontEndBoard.h"

#include <MergedEvent.h>
#include <RunDescription.h>

#ifndef NO_QUSB
#include "QuickUSB.h"
#endif

#include <time.h>
#include <limits.h>
#include <iostream>
#include <assert.h>
#include <algorithm>
#include <string>
#include <sstream>
#include <iomanip>

UsbBoardManager::UsbBoardManager()
    : m_eventNumber(0)
    , m_data(0)
{}

UsbBoardManager::~UsbBoardManager()
{
    if (m_data)
        delete[] m_data;
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         setBusy
//Function:     impose busy output 1 to all the UsbBoards which have been detected.
//Variables:    value(bool)
//              true = impose busy output 1, its to fake a busy state
//              false= no operation
//Returns:      (bool)
//              true = no error
//              false= write to register error!
//-------------------------------------------------------------------------------------------------------------------//  
bool UsbBoardManager::setBusy(bool value) const
{
    std::vector<UsbBoard>::const_iterator usbBoardIt = m_usbBoards.begin();
    std::vector<UsbBoard>::const_iterator usbBoardItEnd = m_usbBoards.end();
    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt)
        if (!usbBoardIt->setBusy(value)) {
            std::cerr << "Failure: Busy signal could not be set." << std::endl;
            assert(false);
            return false;
        }
    return true;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         runContinuousMode
//Function:     select the readout interface to a certain channel.
//Variables:    uplinkSlot(unsigned int):uplinkSlot connected to spi on UsbBoard. (!!Count from 1,2...8)
//              channel(unsigned int): the channel indext to be selected (!!Count from 0,1,2...)
//Returns:      void
//-------------------------------------------------------------------------------------------------------------------// 
void UsbBoardManager::runContinuousMode(unsigned int uplinkSlot, unsigned int channel)
{
    std::vector<UsbBoard>::iterator usbBoardIt = m_usbBoards.begin();
    std::vector<UsbBoard>::iterator usbBoardItEnd = m_usbBoards.end();
    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt)
      usbBoardIt->runContinuousMode(uplinkSlot,channel);
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         eventNumber
//Function:     return eventNumber of each UsbBoard
//Variables:    None
//Returns:      (unsigned short) eventNumber of each UsbBoard
//-------------------------------------------------------------------------------------------------------------------// 
unsigned long UsbBoardManager::eventNumber() const
{
    return m_eventNumber;
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         sampleSizeCompare
//Function:     compare the sample size of two usbBoards
//Variables:    board1(UsbBoard):
//              board2(UsbBoard):
//Returns:      (bool)
//              true = the sample size of board1 is bigger than the one of board2
//              false= the sample size of board2 is smaller than the one of board2
//-------------------------------------------------------------------------------------------------------------------// 
bool sampleSizeCompare(UsbBoard board1, UsbBoard board2)
{
    return board1.sampleSize() > board2.sampleSize();
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         initUsbBoards
//Function:     1.detect all the UsbBoards attached.Once a new one is detected, configure the UsbBoard and, if switching on feChipConfig function in UsbBoard config file, configure the slow control register of spiroc as well.
//              2.resetDaq
//              3.compute the maxim size of the readout data per Access, and then build a buffer (called m_data) with the size of this "maxim size"  to instore readout data
//Variables:    None
//Returns:      (bool)
//              true = Succeed!
//              false= Error exists!
//-------------------------------------------------------------------------------------------------------------------//
bool UsbBoardManager::initUsbBoards()
{
#ifndef NO_QUSB

    char quickUsbModuleNameList[s_cStringLength];

    if (!QuickUsbFindModules(quickUsbModuleNameList, s_cStringLength)) {
        std::cerr << "Failure: No USB modules found." << std::endl;
        return(false);
    }

    // Parsing QuickUSB module list and adding them to the USB board vector.
    // Modules are separated through '\0'. The module list ends with "\0\0".
    char* namePtr = quickUsbModuleNameList;
    while (namePtr[0] != '\0' && namePtr[1] != '\0') {
        HANDLE handle = 0;
        if (QuickUsbOpen(&handle, namePtr)) {
            std::cerr << "Information: Found QuickUSB module " << namePtr << std::endl << std::endl;
            UsbBoard board(handle);
            int id = board.usbBoardId();
            if (id > 0) {
		bool exists = false;
		for ( int i = 0; (i != m_usbBoards.size()) && (!exists); ++i ){
			exists = ( id == m_usbBoards.at(i).usbBoardId());
		}
		if ( !exists ){
	      		std::cout <<"&&&&&& Begining of Settings::instance()->addUsbBoard("<<id<<")"<< std::endl << std::endl;
	      		Settings::instance()->addUsbBoard(id);    // read config file, convert it into 16 bits Arrays
	      		std::cout<<std::endl<<"&&&&&& End of Settings::instance()->addUsbBoard("<<id<<")"<<std::endl<<std::endl;
	      		m_usbBoards.push_back(board);
		}
		else std::cout<<"The board has already been added"<<std::endl;
            }
            namePtr = strchr(namePtr, '\0') + 1;
        } else {
            std::cerr << "Failure: Could not open QuickUSB module " << namePtr << std::endl;
            return false;
        }
    }
    std::sort(m_usbBoards.begin(), m_usbBoards.end(), sampleSizeCompare);
    if (!configureFpga(Settings::instance()->configFpgaOption()))
        return false;
    if (!configureFrontEndBoards())
        return false;
    if (m_data) {
        delete[] m_data;
        m_data = 0;
    }

    // depending on the readout mode, the size of the data stream can vary, we take to maximum size.
    unsigned int maximalDataStreamSize = 0;
    std::vector<UsbBoard>::iterator usbBoardIt = m_usbBoards.begin();
    std::vector<UsbBoard>::iterator usbBoardItEnd = m_usbBoards.end();
    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt) {
        // "2 +", because 2 header 16 bit words are added to each uplink.
        unsigned int dataStreamSize = (2 + usbBoardIt->sampleSize()) * s_uplinksPerUsbBoard * Settings::instance()->eventsPerAccess();
        if (dataStreamSize > maximalDataStreamSize)
            maximalDataStreamSize = dataStreamSize;

    }
    // "2 *", because we receive 8 bit words from the QuickUSB interface and dataStreamSize is measured in 16 bit words.
    m_data = new unsigned char[2 * maximalDataStreamSize];
/*#else
    return true;
#endif
    return false;
*/
   return true;
#else
   return false;
#endif


}

bool UsbBoardManager::configureFpga(bool force)
{
    std::vector<UsbBoard>::iterator usbBoardItBegin = m_usbBoards.begin();
    std::vector<UsbBoard>::iterator usbBoardItEnd = m_usbBoards.end();
    std::vector<UsbBoard>::iterator usbBoardIt = usbBoardItBegin;
    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt) {
        if (!usbBoardIt->configureFpga(force)) {
            std::cerr << "Failure: Could not configure FPGA on USB Board " << usbBoardIt->usbBoardId() << "." << std::endl;
            assert(false);
            return false;
        }
        if (!usbBoardIt->configureDaq()) {
            std::cerr << "Failure: Could not configure data aquisition mode on USB Board " << usbBoardIt->usbBoardId() << "." << std::endl;
            assert(false);
            return false;
        }
    }

    resetDaq();
    return true;
}

bool UsbBoardManager::configureFrontEndBoards()
{
    std::vector<UsbBoard>::iterator usbBoardItBegin = m_usbBoards.begin();
    std::vector<UsbBoard>::iterator usbBoardItEnd = m_usbBoards.end();
    std::vector<UsbBoard>::iterator usbBoardIt = usbBoardItBegin;
    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt) {
        if (!usbBoardIt->configureFrontEndBoards()) {
            std::cerr << "Failure: Could not configure front-end boards on USB Board " << usbBoardIt->usbBoardId() << std::endl;
            assert(false);
            return false;
        }
    }
    return true;
}

FrontEndBoard* UsbBoardManager::frontEndBoard(int usbBoardId, int uplinkSlot)
{
    std::vector<UsbBoard>::iterator usbBoardItBegin = m_usbBoards.begin();
    std::vector<UsbBoard>::iterator usbBoardItEnd = m_usbBoards.end();
    std::vector<UsbBoard>::iterator usbBoardIt = usbBoardItBegin;
    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt) {
        if (usbBoardIt->usbBoardId() == usbBoardId)
            return usbBoardIt->frontEndBoardBySlot(uplinkSlot);
    }
    std::cerr << "Failure: Could not find FrontEndBoard for board ID " << usbBoardId << " and " << uplinkSlot << "." << std::endl;
    assert(false);
    return 0;
}

FrontEndBoard* UsbBoardManager::frontEndBoard(int uplinkId)
{
    std::vector<UsbBoard>::iterator usbBoardItBegin = m_usbBoards.begin();
    std::vector<UsbBoard>::iterator usbBoardItEnd = m_usbBoards.end();
    std::vector<UsbBoard>::iterator usbBoardIt = usbBoardItBegin;
    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt) {
        FrontEndBoard* ret = usbBoardIt->frontEndBoardByUplinkId(uplinkId);
        if (ret)
            return ret;
    }
    std::cerr << "Failure: Could not find FrontEndBoard for uplink ID " << uplinkId << "." << std::endl;
    assert(false);
    return 0;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         takeEvents
//Function:     
//Variables:    event(MergedEvent**)
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------// 
int UsbBoardManager::takeEvents(MergedEvent** event,uint32_t (& TMPtriggerTimeStamp)[2])
{
    std::vector<UsbBoard>::const_iterator usbBoardItBegin = m_usbBoards.begin();
    std::vector<UsbBoard>::const_iterator usbBoardItEnd = m_usbBoards.end();
    std::vector<UsbBoard>::const_iterator usbBoardIt = usbBoardItBegin;
    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt) {
        int usbBoardId = usbBoardIt->usbBoardId();
	// usbBoardIt->readData(m_data, usbBoardIt == usbBoardItBegin);
        int stopDAQ = usbBoardIt->readData(m_data,usbBoardId,TMPtriggerTimeStamp,  true);
	if (stopDAQ != 0) return stopDAQ;
        unsigned short sampleSize = usbBoardIt->sampleSize();
        for (unsigned short uplinkSlot = 1; uplinkSlot <= s_uplinksPerUsbBoard; ++uplinkSlot) {
            int uplinkId = Settings::instance()->uplinkId(usbBoardId, uplinkSlot);
            for (int access = 0; access < Settings::instance()->eventsPerAccess(); ++access) {
                unsigned int eventStartPosition = access * 2 * (2 + sampleSize) * s_uplinksPerUsbBoard;
                unsigned int uplinkStartPosition  = eventStartPosition + 2 * (2 + sampleSize) * (uplinkSlot-1);
                UplinkData& data = event[access]->uplinkData(uplinkId);
                data.fill(&m_data[uplinkStartPosition]);
                event[access]->setEventNumber(m_eventNumber+access);
                if ((event[access]->eventNumber() % (USHRT_MAX+1)) != data.readEventNumber()) {
		  //std::cout << "eventNumber="<<event[access]->eventNumber() << ' ' << data.readEventNumber() << std::endl;
                    //    assert(false);
                }
            }
        }
    }
    m_eventNumber+= Settings::instance()->eventsPerAccess();
    return 0;
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         resetDaq
//Function:     reset all the UsbBoards which have been detected.
//Variables:    None
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------// 
void UsbBoardManager::resetDaq()
{
    std::vector<UsbBoard>::const_iterator usbBoardIt = m_usbBoards.begin();
    std::vector<UsbBoard>::const_iterator usbBoardItEnd = m_usbBoards.end();
    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt) {
        usbBoardIt->resetDaq();
    }
    m_eventNumber = 0;
    std::cerr<<"reset all the usbBoard DAQ attached to the computer, finished!"<<std::endl;
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         resetUsbBoards
//Function:     ??
//Variables:    None
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------// 
void UsbBoardManager::resetUsbBoards()
{
#if !defined(NO_QUSB) && !defined(WIN32)
    usb_init();
    usb_find_busses();
    usb_find_devices();
    for (usb_bus* bus = usb_get_busses(); bus; bus = bus->next) {
        struct usb_device *dev;

        for (dev = bus->devices; dev; dev = dev->next) {
            if (dev->descriptor.idProduct == 1 && dev->descriptor.idVendor
                    == 4027) {

                usb_dev_handle* handle = usb_open(dev);
                UsbBoard board(handle);
                if (Settings::instance()->usbBoardIdExists(board.usbBoardId())) {
                    usb_reset(handle);
                }
            }
        }
    }
#endif
}



bool UsbBoardManager::readFrontEndBoardsSensors(unsigned short * data)
{
    std::vector<UsbBoard>::iterator usbBoardIt = m_usbBoards.begin();
    std::vector<UsbBoard>::iterator usbBoardItEnd = m_usbBoards.end();
    for(int i = 0;i<6;i++)
        data[i] = 0;
    float cala_biasA = 0;
    float cala_biasB = 0;
    float calb_biasA = 0;
    float calb_biasB = 0;
    float cala_currA = 0;
    float cala_currB = 0;
    float calb_currA = 0;
    float calb_currB = 0;
    float biasA = 0;
    float biasB = 0;
    float currA = 0;
    float currB = 0;
    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt) {
        for(int slot = 1; slot <= 8; slot++){
            FrontEndBoard* frontEndBoard = usbBoardIt->frontEndBoardBySlot(slot);
            if(frontEndBoard != NULL){
                if(frontEndBoard->frontEndBoardType() == VATA64V2Type){
                    if(!frontEndBoard->readSensor(data))
                    return false;
                    switch(slot){
                      case 7:
                           cala_biasA = 0.025779;     calb_biasA = 0.290887;
                           cala_biasB = 0.025729;     calb_biasB = 0.403967;
                           cala_currA = 0.0846;      calb_currA = 0.3692;
                           cala_currB = 0.0846;      calb_currB = 0.3692;
                           printf("for slot7 (uplink7)use calibration value for vata64_board_2\n");
                           break;

                      case 6:
                           cala_biasA = 0.025713;     calb_biasA = 0.238812;
                           cala_biasB = 0.025692;     calb_biasB = 0.356303;
                           cala_currA = 0.0846;      calb_currA = 0.3692;
                           cala_currB = 0.0846;      calb_currB = 0.3692;
                           printf("use slot6 (uplink6)calibration value for vata64_board_4\n");
                           break;
                      default:
                           cala_biasA = 0.025779;     calb_biasA = 0.290887;
                           cala_biasB = 0.025729;     calb_biasB = 0.403967;
                           cala_currA = 0.0846;      calb_currA = 0.3692;
                           cala_currB = 0.0846;      calb_currB = 0.3692;
                           printf("use default calibration value, this might not be optimized for your vata64 cards \n");
                   }

                   biasA = cala_biasA*data[0]+calb_biasA-2.5;
                   currA = (data[1]-calb_currA)/cala_currA;
                   biasB = cala_biasB*data[3]+calb_biasB-2.5;
                   currB = (data[4]-calb_currB)/cala_currB;
                   std::cout << "*******************************************************" << std::endl
                              << "Sensor on VATA64 Card:" << std::endl
                              << "usbBoard ID = " << usbBoardIt->usbBoardId() << std::endl
                              << "slot NO = " << slot-1 <<" (0,1,...7)"<< std::endl
                              << "uplink = " << slot <<"(1,2,...,8)"<<std::endl
                              << "uplinkId = "<< frontEndBoard->uplinkId() << std::endl
                              << "BIAS_A_MON = " << data[0] << "        BIAS_B_MON = " << data[3] << std::endl
              //<< "CURR_A_MON = " << data[1] << "        CURR_B_MON = " << data[4] << std::endl
                              << "TEMP_A_MON = " << data[2] << "        TEMP_B_MON = " << data[5] << std::endl
                              << "Based on measured result of vata64_board1, get the following results(voltage refers to GND):"<<std::endl;
                          printf("biasA_sipm = %.2f V        biasB_sipm = %.2f V \n", biasA, biasB);
              // printf("currA_sipm = %.2f nA       currB_sipm = %.2f nA\n", currA, currB);
                    std::cout << "*******************************************************" << std::endl;
                   // printf("press any key to continue...\n");
                   // getchar();
                }
        //printf("press any key to continue...\n");
        //getchar();
            }
        }
        printf("press any key to continue...\n");
        //getchar();
    }
    return true;
}


bool UsbBoardManager::readFrontEndBoardsSensors(){
    std::vector<UsbBoard>::iterator usbBoardIt = m_usbBoards.begin();
    std::vector<UsbBoard>::iterator usbBoardItEnd = m_usbBoards.end();

    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt) {
        const UsbBoardConfiguration& config = Settings::instance()->usbBoardConfiguration(usbBoardIt->usbBoardId());
        for(int slot = 1; slot <= 8; slot++){
            FrontEndBoard* frontEndBoard = usbBoardIt->frontEndBoardBySlot(slot);
            if(frontEndBoard != NULL){
               // if(frontEndBoard->frontEndBoardType() == VATA64V2Type && config.executeFrontEndBoardConfiguration(slot)){
               if(frontEndBoard->frontEndBoardType() == VATA64V2Type){
                    if(!frontEndBoard->readSensor()){
                        return false;
                    }
                }
            }
        }
    }
    return true;
}

void UsbBoardManager::IncreaseLedIntense(unsigned short stepDAC){
    std::vector<UsbBoard>::iterator usbBoardIt = m_usbBoards.begin();
    std::vector<UsbBoard>::iterator usbBoardItEnd = m_usbBoards.end();


    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt) {
        const UsbBoardConfiguration& config = Settings::instance()->usbBoardConfiguration(usbBoardIt->usbBoardId());
        for(int slot = 1; slot <= 8; slot++){
            FrontEndBoard* frontEndBoard = usbBoardIt->frontEndBoardBySlot(slot);
            if(frontEndBoard != NULL){
                if(frontEndBoard->frontEndBoardType() == VATA64V2Type && config.executeFrontEndBoardConfiguration(slot)){
                       frontEndBoard->IncreaseLedIntense(stepDAC);
                }
            }
        }
    }
 }

void UsbBoardManager::DecreaseLedIntense(unsigned short stepDAC){
    std::vector<UsbBoard>::iterator usbBoardIt = m_usbBoards.begin();
    std::vector<UsbBoard>::iterator usbBoardItEnd = m_usbBoards.end();

    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt) {
        const UsbBoardConfiguration& config = Settings::instance()->usbBoardConfiguration(usbBoardIt->usbBoardId());
        for(int slot = 1; slot <= 8; slot++){
            FrontEndBoard* frontEndBoard = usbBoardIt->frontEndBoardBySlot(slot);
            if(frontEndBoard != NULL){
                if(frontEndBoard->frontEndBoardType() == VATA64V2Type && config.executeFrontEndBoardConfiguration(slot)){
                       frontEndBoard->DecreaseLedIntense(stepDAC);
                }
            }
        }
    }
 }


void UsbBoardManager::readFpgaRegister(unsigned short addr)
{
    unsigned short reg = 0;
    std::vector<UsbBoard>::const_iterator usbBoardIt = m_usbBoards.begin();
    std::vector<UsbBoard>::const_iterator usbBoardItEnd = m_usbBoards.end();
    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt) {
        reg = usbBoardIt->readFpgaRegister(addr);
        if(addr == 0x1003) {
            unsigned short fifo_usedw = reg & 0x07FF;
            //printf("boardID = %d, regAddr = %#x, regValue =%#x , fifo_usedw = %d\n",usbBoardIt->usbBoardId(),addr,reg,fifo_usedw);
            printf("boardID = %d, regAddr = %#x, regValue =%#x , fifo_usedw = %d\n",usbBoardIt->usbBoardId(),addr,reg,fifo_usedw*2);	// Increased FIFO size - Olivier 31.10.2016
        }else{
            printf("boardID = %d, regAddr = %#x, regValue =%#x = %d\n",usbBoardIt->usbBoardId(),addr,reg,reg);
        }
    }
}

void UsbBoardManager::setLedInjectionModeOn(bool on){
    std::vector<UsbBoard>::iterator usbBoardIt = m_usbBoards.begin();
    std::vector<UsbBoard>::iterator usbBoardItEnd = m_usbBoards.end();
    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt) {
        usbBoardIt->SetLedInjectionOn(on);
    }
}


void UsbBoardManager::setHoldDelayTime(unsigned short holdDelayTime){
    std::vector<UsbBoard>::iterator usbBoardIt = m_usbBoards.begin();
    std::vector<UsbBoard>::iterator usbBoardItEnd = m_usbBoards.end();
    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt) {
        usbBoardIt->SetHoldDelayTime(holdDelayTime);
    }
}


void UsbBoardManager::setLedOn(int usbBoardId, int startUplinkSlot, int endUplinkSlot, bool on){
    std::vector<UsbBoard>::iterator usbBoardIt = m_usbBoards.begin();
    std::vector<UsbBoard>::iterator usbBoardItEnd = m_usbBoards.end();
    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt) {
        if(usbBoardIt->usbBoardId()==usbBoardId){
            for(int slot = startUplinkSlot; slot<=endUplinkSlot;slot++){
                FrontEndBoard* frontEndBoard = usbBoardIt->frontEndBoardBySlot(slot);
                if(frontEndBoard != NULL){
                    if(frontEndBoard->frontEndBoardType() == VATA64V2Type){
                        frontEndBoard->SetAllLedOn(on);
                    }
                }
            }
        }
    }
}

void UsbBoardManager::setTriggerType(TriggerMode mode) {
	std::vector<UsbBoard>::iterator usbBoardIt = m_usbBoards.begin();
    std::vector<UsbBoard>::iterator usbBoardItEnd = m_usbBoards.end();
    for (; usbBoardIt != usbBoardItEnd; ++usbBoardIt) {
        usbBoardIt->SetTriggerType(mode);
    }
}
