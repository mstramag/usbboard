#ifndef HpeVa256_h
#define HpeVa256_h

#include "FrontEndBoard.h"

#include <bitset>

enum temperatureSensorPosition {UPPER_TEMPERATURE = 0, LOWER_TEMPERATURE = 1};

class HpeVa256Data : public FrontEndBoardData {
    public:
        static const int nTempeatureSensors = 2;
        static const int nDacVoltages = 8;

        HpeVa256Data();
        ~HpeVa256Data();

        void dump(std::ostream& stream = std::cerr) const;

        void setTemperature(int sensorNumber, double value) {m_temperature[sensorNumber] = value;}
        double temperature(int sensorNumber) const {return m_temperature[sensorNumber];}

        void setDacVoltage(int dacNumber, double value) {m_dacVoltage[dacNumber] = value;}
        double dacVoltage(int dacNumber) const {return m_dacVoltage[dacNumber];}

    private:
        double m_temperature[nTempeatureSensors];
        double m_dacVoltage[nDacVoltages];
};

class HpeVa256Configuration : public FrontEndBoardConfiguration {
    public:
        HpeVa256Configuration(int slotNo,int uplinkId);
        ~HpeVa256Configuration();

        void dump(std::ostream&) const;
        virtual bool loadFromString(const char *);
        bool loadFromStream(std::istream&);
};

class HpeVa256 : public FrontEndBoard {
    public:
        HpeVa256(UsbBoard*, FrontEndBoardType type, int);
        ~HpeVa256();

        bool configure(HpeVa256Configuration);
        void readData(HpeVa256Data&);
    private:
        FrontEndBoardType m_type;
        static const float m_refVoltage;
        static const unsigned int m_dacResolution = 8;
        static const float m_temperatureLSB; /* degree */
        static const unsigned short m_commandBitsSize = 16;
        static const unsigned short m_commandByteSize = 2;
        typedef std::bitset<m_commandBitsSize> CommandBits;

        unsigned char* commandBlockToData(const CommandBits&) const;
        bool sendOpCode(unsigned char code);
        bool readTemperature(unsigned int sensorNumber, float& temperature);
        bool setDacVoltage(unsigned int dacNumber, float voltage);
        bool initializeDac();

};

#endif
