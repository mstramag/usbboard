#include "EcalSpirocII.h"
#include "Settings.h"

#include <assert.h>
#include <string>
#include <limits.h>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <sstream>
#include <sys/stat.h> 

EcalSpirocIIConfiguration::EcalSpirocIIConfiguration(int slotNo,int uplinkId)
  : FrontEndBoardConfiguration(EcalSpirocIIType, slotNo, uplinkId)
    , m_selectForRead(-1)
    , m_selectScNprobe(-1)
    , m_trigSelect(-1)
{
    std::cerr
        << "EcalSpirocIIConfiguration::EcalSpirocIIConfiguration() called."
        <<  std::endl;
}

EcalSpirocIIConfiguration::~EcalSpirocIIConfiguration()
{}

void EcalSpirocIIConfiguration::dump(std::ostream& stream) const
{
    std::cerr
        << "EcalSpirocIIConfiguration::dump(std::ostream& stream) called."
        <<  std::endl;
    stream
        << "spirocII:selectForRead || " << m_selectForRead << " || " << std::endl
        << "spirocII:selectScNprobe || " << m_selectScNprobe << " || " << std::endl
        << "spirocII:trigSelect || " << m_trigSelect << " || " << std::endl
        << "spirocII:pathToSlowControlCfgFile || " << m_slowControlCfgFile << std::endl;
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadFromString
//Function:     extract useful infomation for spirocA configuration from the usbBoard config file 
//Variables:    searchSting(const char*):one line read from usbBoard config file          
//Returns:      (bool)
//              true = the given line is for spirocA configuration, read & store it successfully
//              false= the given line does't belong to spirocA configuration
//-------------------------------------------------------------------------------------------------------------------//
bool EcalSpirocIIConfiguration::loadFromString(const char* searchString)
{
      char cString[100];
      bool getline = false;       
      std::cerr
        << "EcalSpirocIIConfiguration::loadFromString(const char*) called."
        <<  std::endl;

      if(sscanf(searchString, "spirocII:pathToSlowControlCfgFile || %[^\n]", cString) == 1) {
            m_slowControlCfgFile = cString;
	    std::cout<<"pathToSlowControlCfgFile ||..."<<searchString<<std::endl;
            getline = true;
      }
    
      if(sscanf(searchString, "spirocII:selectForRead || %d ||", &m_selectForRead)==1) {
	std::cout<<"selectForRead ||..."<<searchString<<std::endl;
	getline =true;
      }
      if(sscanf(searchString, "spirocII:selectScNprobe || %d ||", &m_selectScNprobe)==1) {
	std::cout<<"selectScNprobe ||..."<<searchString<<std::endl;
	getline =true;
      }      
      if(sscanf(searchString, "spirocII:trigSelect || %d ||", &m_trigSelect)==1) {
	std::cout<<"trigselect ||..."<<searchString<<std::endl;
	getline =true;
      }
    return getline;
}



//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadConfig
//Function:     read spirocII slow control config file, store all the settings to struct SPIROC_II_CFG_TYPE ( defined in TypeDefs.h )
//Variables:    filePath(string): path to the slow control config file of spirocII            
//Returns:      (bool) return error value of this process.
//              true = error exist!
//              false= execute successfully!
//-------------------------------------------------------------------------------------------------------------------//
bool EcalSpirocIIConfiguration::loadConfig(const std::string& filePath) {  

    std::stringstream stream;
    stream << filePath ;             //get config file name
    std::cerr << "Information: Loading configuration from file " << stream.str() << std::endl;
    std::ifstream fileStream(stream.str().c_str());      // open config file 
    // "ifstream" provides an interface to read data from files as input streams

    bool err= false;
    if (!fileStream){
           std::cerr << "Failure: coudn't open file " << stream.str()<< ".\n";
           assert(false);
     }      


    std::cerr << "Choose SpirocII, reading Config file..."<<std::endl;
    err = loadFromStream(fileStream);                           // load config file, store all the config settings in struct SPIROC_II_CFG_TYPE( defined in TypeDefs.h)

    return(err);
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         loadFromStream
//Function:     extract config values from stream, and store them in m_cfgSpirocII ( struct SPIROC_II_CFG_TYPE ( defined in TypeDefs.h ))
//              m_cfgSpirocII is a private variable defined in Class EcalSpirocIIConfiguration
//Variables:    stream(istream): path to the slow control config file of spirocII     
//                              (istream objects are stream objects used to read and interpret input from sequences of characters)      
//Returns:      (bool) return error value of this process.
//              true = error exist!
//              false= execute successfully!
//-------------------------------------------------------------------------------------------------------------------//
bool EcalSpirocIIConfiguration::loadFromStream(std::istream& stream) {

    unsigned int err = 1;
    unsigned int line = 1;

    std::string s;
    while (true) {
        std::getline(stream, s);
        if (stream.eof())
            break;
        const char* searchString = s.c_str();

        err = 1;      // During each loop,err will be set back to 0 ,as long as one sentence in correct format has been read.Otherwise it will stay in 1
        if (s.find("#") == 0) {
            if (s.find("# End of the config file") == 0 ){
                err = 0 ;
                break;
            }  

            err = 0 ;
            continue; // Skip commented lines
        }

        if(sscanf(searchString, "sw_trig_ext || %d ||",&m_cfgSpirocII.sw_trig_ext)==1){
            line++;
            err = 0;
        }

        if(sscanf(searchString, "sw_flag_tdc_ext || %d ||",&m_cfgSpirocII.sw_flag_tdc_ext)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "sw_ramp_adc_ext || %d ||",&m_cfgSpirocII.sw_ramp_adc_ext)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "sw_ramp_tdc_ext || %d ||",&m_cfgSpirocII.sw_ramp_tdc_ext)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "adc_res_register|| %d ||",&m_cfgSpirocII.adc_res_register)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "chip_ID_registe || %d ||",&m_cfgSpirocII.chip_ID_register)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_ChipSat || %d ||",&m_cfgSpirocII.EN_ChipSat)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_TransmitOn || %d ||",&m_cfgSpirocII.EN_TransmitOn)==1){
            line++; 
            err = 0;
        }
        if(sscanf(searchString, "EN_RamFull    || %d ||",&m_cfgSpirocII.EN_RamFull)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "EN_OutSerie   || %d ||",&m_cfgSpirocII.EN_OutSerie)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "bypass_chip   || %d ||",&m_cfgSpirocII.bypass_chip)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "resolutionadc  || %d ||",&m_cfgSpirocII.resolution_adc)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_adc_ramp_current_source  || %d ||",&m_cfgSpirocII.pwr_on_adc_ramp_current_source)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_adc_ramp_integrator  || %d ||",&m_cfgSpirocII.pwr_on_adc_ramp_integrator)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "input_dac_8bits  || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || ",&m_cfgSpirocII.input_dac_8bits[0],
                    &m_cfgSpirocII.input_dac_8bits[1],
                    &m_cfgSpirocII.input_dac_8bits[2],
                    &m_cfgSpirocII.input_dac_8bits[3],
                    &m_cfgSpirocII.input_dac_8bits[4],
                    &m_cfgSpirocII.input_dac_8bits[5],
                    &m_cfgSpirocII.input_dac_8bits[6],
                    &m_cfgSpirocII.input_dac_8bits[7],
                    &m_cfgSpirocII.input_dac_8bits[8],
                    &m_cfgSpirocII.input_dac_8bits[9],
                    &m_cfgSpirocII.input_dac_8bits[10],
                    &m_cfgSpirocII.input_dac_8bits[11],
                    &m_cfgSpirocII.input_dac_8bits[12],
                    &m_cfgSpirocII.input_dac_8bits[13],
                    &m_cfgSpirocII.input_dac_8bits[14],
                    &m_cfgSpirocII.input_dac_8bits[15],
                    &m_cfgSpirocII.input_dac_8bits[16],
                    &m_cfgSpirocII.input_dac_8bits[17],
                    &m_cfgSpirocII.input_dac_8bits[18],
                    &m_cfgSpirocII.input_dac_8bits[19],
                    &m_cfgSpirocII.input_dac_8bits[20],
                    &m_cfgSpirocII.input_dac_8bits[21],
                    &m_cfgSpirocII.input_dac_8bits[22],
                    &m_cfgSpirocII.input_dac_8bits[23],
                    &m_cfgSpirocII.input_dac_8bits[24],
                    &m_cfgSpirocII.input_dac_8bits[25],
                    &m_cfgSpirocII.input_dac_8bits[26],
                    &m_cfgSpirocII.input_dac_8bits[27],
                    &m_cfgSpirocII.input_dac_8bits[28],
                    &m_cfgSpirocII.input_dac_8bits[29],
                    &m_cfgSpirocII.input_dac_8bits[30],
                    &m_cfgSpirocII.input_dac_8bits[31],
                    &m_cfgSpirocII.input_dac_8bits[32],
                    &m_cfgSpirocII.input_dac_8bits[33],
                    &m_cfgSpirocII.input_dac_8bits[34],
                    &m_cfgSpirocII.input_dac_8bits[35])==36){
                        line++;
                        err = 0;
                    }
        if(sscanf(searchString, "input_dac_off  || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || ",&m_cfgSpirocII.input_dac_off[0],
                    &m_cfgSpirocII.input_dac_off[1],
                    &m_cfgSpirocII.input_dac_off[2],
                    &m_cfgSpirocII.input_dac_off[3],
                    &m_cfgSpirocII.input_dac_off[4],
                    &m_cfgSpirocII.input_dac_off[5],
                    &m_cfgSpirocII.input_dac_off[6],
                    &m_cfgSpirocII.input_dac_off[7],
                    &m_cfgSpirocII.input_dac_off[8],
                    &m_cfgSpirocII.input_dac_off[9],
                    &m_cfgSpirocII.input_dac_off[10],
                    &m_cfgSpirocII.input_dac_off[11],
                    &m_cfgSpirocII.input_dac_off[12],
                    &m_cfgSpirocII.input_dac_off[13],
                    &m_cfgSpirocII.input_dac_off[14],
                    &m_cfgSpirocII.input_dac_off[15],
                    &m_cfgSpirocII.input_dac_off[16],
                    &m_cfgSpirocII.input_dac_off[17],
                    &m_cfgSpirocII.input_dac_off[18],
                    &m_cfgSpirocII.input_dac_off[19],
                    &m_cfgSpirocII.input_dac_off[20],
                    &m_cfgSpirocII.input_dac_off[21],
                    &m_cfgSpirocII.input_dac_off[22],
                    &m_cfgSpirocII.input_dac_off[23],
                    &m_cfgSpirocII.input_dac_off[24],
                    &m_cfgSpirocII.input_dac_off[25],
                    &m_cfgSpirocII.input_dac_off[26],
                    &m_cfgSpirocII.input_dac_off[27],
                    &m_cfgSpirocII.input_dac_off[28],
                    &m_cfgSpirocII.input_dac_off[29],
                    &m_cfgSpirocII.input_dac_off[30],
                    &m_cfgSpirocII.input_dac_off[31],
                    &m_cfgSpirocII.input_dac_off[32],
                    &m_cfgSpirocII.input_dac_off[33],
                    &m_cfgSpirocII.input_dac_off[34],
                    &m_cfgSpirocII.input_dac_off[35])==36){
                        line++; 
                        err = 0;
                    }
        if(sscanf(searchString, "sw_pa_comp_hg  || %d ||",&m_cfgSpirocII.sw_pa_comp_hg)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "sw_pa_cf_hgb   || %d ||",&m_cfgSpirocII.sw_pa_cf_hgb)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "sw_pa_cf_lgb   || %d ||",&m_cfgSpirocII.sw_pa_cf_lgb)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "sw_pa_comp_lg  || %d ||",&m_cfgSpirocII.sw_pa_comp_lg)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_pa  || %d ||",&m_cfgSpirocII.pwr_on_pa)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "sw_pa_disable  || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d ||  ",&m_cfgSpirocII.sw_pa_disable[0],
                    &m_cfgSpirocII.sw_pa_disable[1],
                    &m_cfgSpirocII.sw_pa_disable[2],
                    &m_cfgSpirocII.sw_pa_disable[3],
                    &m_cfgSpirocII.sw_pa_disable[4],
                    &m_cfgSpirocII.sw_pa_disable[5],
                    &m_cfgSpirocII.sw_pa_disable[6],
                    &m_cfgSpirocII.sw_pa_disable[7],
                    &m_cfgSpirocII.sw_pa_disable[8],
                    &m_cfgSpirocII.sw_pa_disable[9],
                    &m_cfgSpirocII.sw_pa_disable[10],
                    &m_cfgSpirocII.sw_pa_disable[11],
                    &m_cfgSpirocII.sw_pa_disable[12],
                    &m_cfgSpirocII.sw_pa_disable[13],
                    &m_cfgSpirocII.sw_pa_disable[14],
                    &m_cfgSpirocII.sw_pa_disable[15],
                    &m_cfgSpirocII.sw_pa_disable[16],
                    &m_cfgSpirocII.sw_pa_disable[17],
                    &m_cfgSpirocII.sw_pa_disable[18],
                    &m_cfgSpirocII.sw_pa_disable[19],
                    &m_cfgSpirocII.sw_pa_disable[20],
                    &m_cfgSpirocII.sw_pa_disable[21],
                    &m_cfgSpirocII.sw_pa_disable[22],
                    &m_cfgSpirocII.sw_pa_disable[23],
                    &m_cfgSpirocII.sw_pa_disable[24],
                    &m_cfgSpirocII.sw_pa_disable[25],
                    &m_cfgSpirocII.sw_pa_disable[26],
                    &m_cfgSpirocII.sw_pa_disable[27],
                    &m_cfgSpirocII.sw_pa_disable[28],
                    &m_cfgSpirocII.sw_pa_disable[29],
                    &m_cfgSpirocII.sw_pa_disable[30],
                    &m_cfgSpirocII.sw_pa_disable[31],
                    &m_cfgSpirocII.sw_pa_disable[32],
                    &m_cfgSpirocII.sw_pa_disable[33],
                    &m_cfgSpirocII.sw_pa_disable[34],
                    &m_cfgSpirocII.sw_pa_disable[35])==36){
                        line++;
                        err = 0;
                    }
        if(sscanf(searchString, "sw_pa_ctest_enable  || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || ",&m_cfgSpirocII.sw_pa_ctest_enable[0],
                    &m_cfgSpirocII.sw_pa_ctest_enable[1],
                    &m_cfgSpirocII.sw_pa_ctest_enable[2],
                    &m_cfgSpirocII.sw_pa_ctest_enable[3],
                    &m_cfgSpirocII.sw_pa_ctest_enable[4],
                    &m_cfgSpirocII.sw_pa_ctest_enable[5],
                    &m_cfgSpirocII.sw_pa_ctest_enable[6],
                    &m_cfgSpirocII.sw_pa_ctest_enable[7],
                    &m_cfgSpirocII.sw_pa_ctest_enable[8],
                    &m_cfgSpirocII.sw_pa_ctest_enable[9],
                    &m_cfgSpirocII.sw_pa_ctest_enable[10],
                    &m_cfgSpirocII.sw_pa_ctest_enable[11],
                    &m_cfgSpirocII.sw_pa_ctest_enable[12],
                    &m_cfgSpirocII.sw_pa_ctest_enable[13],
                    &m_cfgSpirocII.sw_pa_ctest_enable[14],
                    &m_cfgSpirocII.sw_pa_ctest_enable[15],
                    &m_cfgSpirocII.sw_pa_ctest_enable[16],
                    &m_cfgSpirocII.sw_pa_ctest_enable[17],
                    &m_cfgSpirocII.sw_pa_ctest_enable[18],
                    &m_cfgSpirocII.sw_pa_ctest_enable[19],
                    &m_cfgSpirocII.sw_pa_ctest_enable[20],
                    &m_cfgSpirocII.sw_pa_ctest_enable[21],
                    &m_cfgSpirocII.sw_pa_ctest_enable[22],
                    &m_cfgSpirocII.sw_pa_ctest_enable[23],
                    &m_cfgSpirocII.sw_pa_ctest_enable[24],
                    &m_cfgSpirocII.sw_pa_ctest_enable[25],
                    &m_cfgSpirocII.sw_pa_ctest_enable[26],
                    &m_cfgSpirocII.sw_pa_ctest_enable[27],
                    &m_cfgSpirocII.sw_pa_ctest_enable[28],
                    &m_cfgSpirocII.sw_pa_ctest_enable[29],
                    &m_cfgSpirocII.sw_pa_ctest_enable[30],
                    &m_cfgSpirocII.sw_pa_ctest_enable[31],
                    &m_cfgSpirocII.sw_pa_ctest_enable[32],
                    &m_cfgSpirocII.sw_pa_ctest_enable[33],
                    &m_cfgSpirocII.sw_pa_ctest_enable[34],
                    &m_cfgSpirocII.sw_pa_ctest_enable[35])==36){
                        line++;
                        err = 0;
                    }
        if(sscanf(searchString, "pwr_on_suiv_ssh_lg  || %d ||",&m_cfgSpirocII.pwr_on_suiv_ssh_lg)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "sw_rc_lg   || %d ||",&m_cfgSpirocII.sw_rc_lg)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_ssh_lg  || %d ||",&m_cfgSpirocII.pwr_on_ssh_lg)==1){
            line++;
            err = 0;
        } 
        if(sscanf(searchString, "pwr_on_suiv_ssh_hg  || %d ||",&m_cfgSpirocII.pwr_on_suiv_ssh_hg)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "sw_rc_hg  || %d ||",&m_cfgSpirocII.sw_rc_hg)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_ssh_hg  || %d ||",&m_cfgSpirocII.pwr_on_ssh_hg)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_suiv_fs || %d ||",&m_cfgSpirocII.pwr_on_suiv_fs)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_fs  || %d ||",&m_cfgSpirocII.pwr_on_fs)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "sw_sca_backup  || %d ||",&m_cfgSpirocII.sw_sca_backup)==1){
            line++; 
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_sca  || %d ||",&m_cfgSpirocII.pwr_on_sca)==1){
            line++;           
            err = 0;
        }
        if(sscanf(searchString, "dac1  || %d ||",&m_cfgSpirocII.dac1)==1){
            line++; 
            err = 0;
        }
        if(sscanf(searchString, "dac2  || %d ||",&m_cfgSpirocII.dac2)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_dual_dac  || %d ||",&m_cfgSpirocII.pwr_on_dual_dac)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_ota_dual_dac  || %d ||",&m_cfgSpirocII.pwr_on_ota_dual_dac)==1){
            line++;
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_bandgap   || %d ||",&m_cfgSpirocII.pwr_on_bandgap)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_delay_start_ramp_tdc  || %d ||",&m_cfgSpirocII.pwr_on_delay_start_ramp_tdc)==1){
            line++;    
            err = 0;
        }
        if(sscanf(searchString, "delay_start_ramp_tdc  || %d ||",&m_cfgSpirocII.delay_start_ramp_tdc)==1){
            line++;   
            err = 0;
        }
        if(sscanf(searchString, "sw_ramp_slope   || %d ||",&m_cfgSpirocII.sw_ramp_slope)==1){
            line++;             
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_tdc_ramp  || %d ||",&m_cfgSpirocII.pwr_on_tdc_ramp)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_discri_adc  || %d ||",&m_cfgSpirocII.pwr_on_discri_adc)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_discri_gs  || %d ||",&m_cfgSpirocII.pwr_on_discri_gs)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "sw_auto_gain_select  || %d || ",&m_cfgSpirocII.sw_auto_gain_select)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "gain_select  || %d ||",&m_cfgSpirocII.gain_select)==1){
            line++;   
            err = 0;
        }
        if(sscanf(searchString, "sw_in_adc_ext  || %d ||",&m_cfgSpirocII.sw_in_adc_ext)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "sw_tdc_on   || %d ||",&m_cfgSpirocII.sw_tdc_on)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "mask_discri || %d || || %d ||  || %d || || %d ||  || %d || || %d ||  || %d || || %d ||  || %d || || %d ||  || %d || || %d ||  || %d || || %d ||  || %d || || %d ||  || %d || || %d ||  || %d || || %d ||  || %d || || %d ||  || %d || || %d ||  || %d || || %d ||  || %d || || %d ||  || %d || || %d ||  || %d || || %d ||  || %d || || %d ||  || %d || || %d ||  ",&m_cfgSpirocII.mask_discri[0],
                    &m_cfgSpirocII.mask_discri[1],
                    &m_cfgSpirocII.mask_discri[2],
                    &m_cfgSpirocII.mask_discri[3],
                    &m_cfgSpirocII.mask_discri[4],
                    &m_cfgSpirocII.mask_discri[5],
                    &m_cfgSpirocII.mask_discri[6],
                    &m_cfgSpirocII.mask_discri[7],
                    &m_cfgSpirocII.mask_discri[8],
                    &m_cfgSpirocII.mask_discri[9],
                    &m_cfgSpirocII.mask_discri[10],
                    &m_cfgSpirocII.mask_discri[11],
                    &m_cfgSpirocII.mask_discri[12],
                    &m_cfgSpirocII.mask_discri[13],
                    &m_cfgSpirocII.mask_discri[14],
                    &m_cfgSpirocII.mask_discri[15],
                    &m_cfgSpirocII.mask_discri[16],
                    &m_cfgSpirocII.mask_discri[17],
                    &m_cfgSpirocII.mask_discri[18],
                    &m_cfgSpirocII.mask_discri[19],
                    &m_cfgSpirocII.mask_discri[20],
                    &m_cfgSpirocII.mask_discri[21],
                    &m_cfgSpirocII.mask_discri[22],
                    &m_cfgSpirocII.mask_discri[23],
                    &m_cfgSpirocII.mask_discri[24],
                    &m_cfgSpirocII.mask_discri[25],
                    &m_cfgSpirocII.mask_discri[26],
                    &m_cfgSpirocII.mask_discri[27],
                    &m_cfgSpirocII.mask_discri[28],
                    &m_cfgSpirocII.mask_discri[29],
                    &m_cfgSpirocII.mask_discri[30],
                    &m_cfgSpirocII.mask_discri[31],
                    &m_cfgSpirocII.mask_discri[32],
                    &m_cfgSpirocII.mask_discri[33],
                    &m_cfgSpirocII.mask_discri[34],
                    &m_cfgSpirocII.mask_discri[35])==36){
                        line++;  
                        err = 0;
                    }
        if(sscanf(searchString, "cmd_hold_ext  || %d ||",&m_cfgSpirocII.cmd_hold_ext)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_delay_discri  || %d ||",&m_cfgSpirocII.pwr_on_delay_discri)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "delay_discri  || %d ||",&m_cfgSpirocII.delay_discri)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "dac_4_bit_theshold_adjust || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || || %d || ",&m_cfgSpirocII.dac_4_bit_theshold_adjust[0],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[1],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[2],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[3],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[4],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[5],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[6],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[7],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[8],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[9],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[10],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[11],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[12],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[13],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[14],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[15],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[16],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[17],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[18],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[19],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[20],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[21],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[22],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[23],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[24],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[25],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[26],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[27],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[28],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[29],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[30],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[31],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[32],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[33],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[34],
                    &m_cfgSpirocII.dac_4_bit_theshold_adjust[35])==36){
                        line++;  
                        err = 0;
                    }

        if(sscanf(searchString, "sw_fine_adjust || %d ||",&m_cfgSpirocII.sw_fine_adjust)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_dac4b   || %d ||",&m_cfgSpirocII.pwr_on_dac4b)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_discri  || %d ||",&m_cfgSpirocII.pwr_on_discri)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_delay_ValidHold  || %d ||",&m_cfgSpirocII.pwr_on_delay_ValidHold)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "delay_ValidHold  || %d ||",&m_cfgSpirocII.delay_ValidHold)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "pwr_on_delay_RstColumn || %d ||",&m_cfgSpirocII.pwr_on_delay_RstColumn)==1){
            line++;  
            err = 0;
        }
        if(sscanf(searchString, "delay_RstColumn  || %d ||",&m_cfgSpirocII.delay_RstColumn)==1){ 
            line++;  
            err = 0;
        }
        if (err) {
            std:: cerr << " Error@Front End Config file: Format in LINE="<<line<<" Error!(#comments are not included in LINE number" << std:: endl;
            assert(false);
            break;
        }
    }

    if (err) {
        std:: cerr << " Error: Front End Chip configuration  Failed!" << std:: endl;
        assert(false);
        return false;
    }    
    return true;
}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         convertCfgToVector
//Function:     convert m_cfgSpirocII ( struct SPIROC_II_CFG_TYPE ( defined in TypeDefs.h ))into Vector.
//              m_cfgSpirocII is a private variable defined in Class EcalSpirocIIConfiguration,which stores all the settings reading from the slow control config file
//Variables:    temp(unsigned char[]): point to an array to place the conversion result. Each member of this array corresponds to one bit of the slow control register
//              i.e.temp[index] corresponds to BIT(index) of the slow control register      
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//  
void  EcalSpirocIIConfiguration::convertCfgToVector(unsigned char temp[702]) { 

    int  bit_total =0;
    int  bit,ch;

    temp[bit_total++] = (m_cfgSpirocII.sw_trig_ext                          )&0x01;   //bug fix of chip
    temp[bit_total++] = (m_cfgSpirocII.sw_flag_tdc_ext                      )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.sw_ramp_adc_ext                      )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.sw_ramp_tdc_ext                      )&0x01;

    for(bit=0;bit<12;bit++)//from LSB to MSB
        temp[bit_total++] = (m_cfgSpirocII.adc_res_register>>bit                )&0x01;

    for(bit=0;bit< 8;bit++)//from LSB to MSB
        temp[bit_total++] = (m_cfgSpirocII.chip_ID_register>>bit                )&0x01;

    temp[bit_total++] = (m_cfgSpirocII.EN_ChipSat                           )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.EN_TransmitOn                        )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.EN_RamFull                           )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.EN_OutSerie                          )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.bypass_chip                          )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.resolution_adc                       )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.pwr_on_adc_ramp_current_source       )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.pwr_on_adc_ramp_integrator           )&0x01;

    for(ch=0;ch<36;ch++) {                                    //from ch0 to ch35 
        for(bit=0;bit<8;bit++)//from LSB to MSB
            temp[bit_total++] = (m_cfgSpirocII.input_dac_8bits[ch]>>bit              )&0x01;
        temp[bit_total++] = (m_cfgSpirocII.input_dac_off[ch]                     )&0x01;
    }

    for(bit=0;bit<4;bit++)//from LSB to MSB
        temp[bit_total++] = (m_cfgSpirocII.sw_pa_comp_hg>>bit                   )&0x01;

    for(bit=0;bit<4;bit++)//from LSB to MSB
        temp[bit_total++] = (m_cfgSpirocII.sw_pa_cf_hgb>>bit                    )&0x01;

    for(bit=0;bit<4;bit++)//from LSB to MSB
        temp[bit_total++] = (m_cfgSpirocII.sw_pa_cf_lgb>>bit                    )&0x01;

    for(bit=0;bit<4;bit++)//from LSB to MSB
        temp[bit_total++] = (m_cfgSpirocII.sw_pa_comp_lg>>bit                   )&0x01;

    temp[bit_total++] = (m_cfgSpirocII.pwr_on_pa                            )&0x01;

    for(ch=0;ch<36;ch++){                                   //from ch0 to ch35
        temp[bit_total++] = (m_cfgSpirocII.sw_pa_disable[ch]                      )&0x01;
        temp[bit_total++] = (m_cfgSpirocII.sw_pa_ctest_enable[ch]                )&0x01;
    }

    temp[bit_total++] = (m_cfgSpirocII.pwr_on_suiv_ssh_lg                   )&0x01;

    for(bit=0;bit<3;bit++)//from LSB to MSB
        temp[bit_total++] = (m_cfgSpirocII.sw_rc_lg>>bit                        )&0x01;

    temp[bit_total++] = (m_cfgSpirocII.pwr_on_ssh_lg                        )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.pwr_on_suiv_ssh_hg                   )&0x01;

    for(bit=0;bit<3;bit++)//from LSB to MSB
        temp[bit_total++] = (m_cfgSpirocII.sw_rc_hg>>bit                        )&0x01;

    temp[bit_total++] = (m_cfgSpirocII.pwr_on_ssh_hg                        )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.pwr_on_suiv_fs                       )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.pwr_on_fs                            )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.sw_sca_backup                        )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.pwr_on_sca                           )&0x01;

    for(bit=0;bit<10;bit++)//from LSB to MSB
        temp[bit_total++] = (m_cfgSpirocII.dac1>>bit                            )&0x01;

    for(bit=0;bit<10;bit++) //from LSB to MSB
        temp[bit_total++] = (m_cfgSpirocII.dac2>>bit                            )&0x01;

    temp[bit_total++] = (m_cfgSpirocII.pwr_on_dual_dac                      )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.pwr_on_ota_dual_dac                  )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.pwr_on_bandgap                       )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.pwr_on_delay_start_ramp_tdc          )&0x01;

    for(bit=0;bit<6;bit++) //MSB to LSB
        temp[bit_total++] = (m_cfgSpirocII.delay_start_ramp_tdc>>(5-bit)        )&0x01;

    temp[bit_total++] = (m_cfgSpirocII.sw_ramp_slope                        )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.pwr_on_tdc_ramp                      )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.pwr_on_discri_adc                    )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.pwr_on_discri_gs                     )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.sw_auto_gain_select                  )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.gain_select                          )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.sw_in_adc_ext                        )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.sw_tdc_on                            )&0x01;

    for(ch=0;ch<36;ch++)   //from ch35 to ch0
        temp[bit_total++] = (m_cfgSpirocII.mask_discri[35-ch]                   )&0x01;

    temp[bit_total++] = (m_cfgSpirocII.cmd_hold_ext                         )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.pwr_on_delay_discri                  )&0x01;

    for(bit=0;bit<6;bit++)//from MSB to LSB
        temp[bit_total++] = (m_cfgSpirocII.delay_discri>>(5-bit)                )&0x01;

    for(ch=0;ch<36;ch++){                                  //from ch35 to ch0
        for(bit=0;bit<4;bit++)
            temp[bit_total++] = (m_cfgSpirocII.dac_4_bit_theshold_adjust[35-ch]>>bit)&0x01;
    }
    temp[bit_total++] = (m_cfgSpirocII.sw_fine_adjust                       )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.pwr_on_dac4b                         )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.pwr_on_discri                        )&0x01;
    temp[bit_total++] = (m_cfgSpirocII.pwr_on_delay_ValidHold               )&0x01;

    for(bit=0;bit<6;bit++)//from MSB to LSB
        temp[bit_total++] = (m_cfgSpirocII.delay_ValidHold>>(5-bit)             )&0x01;

    temp[bit_total++] = (m_cfgSpirocII.pwr_on_delay_RstColumn               )&0x01;

    for(bit=0;bit<6;bit++)//from MSB to LSB
        temp[bit_total++] = (m_cfgSpirocII.delay_RstColumn>>(5-bit)             )&0x01;


}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         writeToStream
//Function:     dump m_cfgSpirocII ( struct SPIROC_II_CFG_TYPE ( defined in TypeDefs.h ))
//              m_cfgSpirocII is a private variable defined in Class EcalSpirocIIConfiguration,which stores all the settings reading from the slow control config file
//Variables:    stream(ostream):  output stream
//                                (ostream objects are stream objects used to write and format output as sequences of characters      
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//  
void EcalSpirocIIConfiguration::writeToStream(std::ostream& stream) const
{
    stream
        <<" sw_trig_ext:      "<<m_cfgSpirocII.sw_trig_ext << std::endl
        <<" sw_flag_tdc_ext:  "<<m_cfgSpirocII.sw_flag_tdc_ext << std::endl
        <<" sw_ramp_adc_ext:  "<<m_cfgSpirocII.sw_ramp_adc_ext << std::endl
        <<" sw_ramp_tdc_ext:  "<<m_cfgSpirocII.sw_ramp_tdc_ext << std::endl
        <<" adc_res_register: "<<m_cfgSpirocII.adc_res_register << std::endl
        <<" chip_ID_register: "<<m_cfgSpirocII.chip_ID_register << std::endl
        <<" EN_ChipSat:       "<<m_cfgSpirocII.EN_ChipSat << std::endl
        <<" EN_TransmitOn:    "<<m_cfgSpirocII.EN_TransmitOn << std::endl
        <<" EN_RamFull:       "<<m_cfgSpirocII.EN_RamFull << std::endl     
        <<" EN_OutSerie:      "<<m_cfgSpirocII.EN_OutSerie << std::endl
        <<" bypass_chip:      "<<m_cfgSpirocII.bypass_chip << std::endl
        <<" resolution_adc:   "<<m_cfgSpirocII.resolution_adc << std::endl
        <<" pwr_on_adc_ramp_current_source: "<<m_cfgSpirocII.pwr_on_adc_ramp_current_source << std::endl
        <<" pwr_on_adc_ramp_integrator:     "<<m_cfgSpirocII.pwr_on_adc_ramp_integrator << std::endl;

    stream
        <<" input_dac_8bit:   ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.input_dac_8bits[j] <<"  ";
    stream
        <<std::endl;

    stream
        <<" input_dac_8bit:   ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.input_dac_8bits[j+9] <<"  ";
    stream
        <<std::endl;

    stream
        <<" input_dac_8bit:   ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.input_dac_8bits[j+18] <<"  ";
    stream
        <<std::endl;

    stream
        <<" input_dac_8bit:   ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.input_dac_8bits[j+27] <<"  ";
    stream
        <<std::endl;


    stream
        <<" input_dac_off:    ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.input_dac_off[j] <<"  ";
    stream
        <<std::endl;

    stream
        <<" input_dac_off:    ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.input_dac_off[j+9] <<"  ";
    stream
        <<std::endl;

    stream
        <<" input_dac_off:    ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.input_dac_off[j+18] <<"  ";
    stream
        <<std::endl;

    stream
        <<" input_dac_off:    ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.input_dac_off[j+27] <<"  ";
    stream
        <<std::endl;

    stream
        <<" sw_pa_comp_hg:    "<<m_cfgSpirocII.sw_pa_comp_hg << std::endl
        <<" sw_pa_cf_hgb:     "<<m_cfgSpirocII.sw_pa_cf_hgb << std::endl
        <<" sw_pa_cf_lgb:     "<<m_cfgSpirocII.sw_pa_cf_lgb << std::endl
        <<" sw_pa_comp_lg:    "<<m_cfgSpirocII.sw_pa_comp_lg << std::endl
        <<" pwr_on_pa:        "<<m_cfgSpirocII.pwr_on_pa << std::endl;

    stream
        <<" sw_pa_disable:    ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.sw_pa_disable[j] <<"  ";
    stream
        <<std::endl;

    stream
        <<" sw_pa_disable:    ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.sw_pa_disable[j+9] <<"  ";
    stream
        <<std::endl;

    stream
        <<" sw_pa_disable:    ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.sw_pa_disable[j+18] <<"  ";
    stream
        <<std::endl;

    stream
        <<" sw_pa_disable:    ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.sw_pa_disable[j+27] <<"  ";
    stream
        <<std::endl;

    stream
        <<" sw_pa_ctest_enable: ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.sw_pa_ctest_enable[j] <<"  ";
    stream
        <<std::endl;

    stream
        <<" sw_pa_ctest_enable: ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.sw_pa_ctest_enable[j+9] <<"  ";
    stream
        <<std::endl;

    stream
        <<" sw_pa_ctest_enable: ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.sw_pa_ctest_enable[j+18] <<"  ";
    stream
        <<std::endl;

    stream
        <<" sw_pa_ctest_enable: ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.sw_pa_ctest_enable[j+27] <<"  ";
    stream
        <<std::endl;

    stream
        <<" pwr_on_suiv_ssh_lg: "<<m_cfgSpirocII.pwr_on_suiv_ssh_lg << std::endl
        <<" sw_rc_lg:           "<<m_cfgSpirocII.sw_rc_lg << std::endl
        <<" pwr_on_ssh_lg:      "<<m_cfgSpirocII.pwr_on_ssh_lg << std::endl
        <<" pwr_on_suiv_ssh_hg: "<<m_cfgSpirocII.pwr_on_suiv_ssh_hg << std::endl
        <<" sw_rc_hg:           "<<m_cfgSpirocII.sw_rc_hg << std::endl
        <<" pwr_on_ssh_hg:      "<<m_cfgSpirocII.pwr_on_ssh_hg << std::endl
        <<" pwr_on_suiv_fs:     "<<m_cfgSpirocII.pwr_on_suiv_fs << std::endl
        <<" pwr_on_fs:          "<<m_cfgSpirocII.pwr_on_fs << std::endl
        <<" sw_sca_backup:      "<<m_cfgSpirocII.sw_sca_backup << std::endl     
        <<" pwr_on_sca:         "<<m_cfgSpirocII.pwr_on_sca << std::endl
        <<" dac1:               "<<m_cfgSpirocII.dac1 << std::endl
        <<" dac2:               "<<m_cfgSpirocII.dac2 << std::endl
        <<" pwr_on_dual_dac:    "<<m_cfgSpirocII.pwr_on_dual_dac << std::endl
        <<" pwr_on_ota_dual_dac:"<<m_cfgSpirocII.pwr_on_ota_dual_dac << std::endl
        <<" pwr_on_bandgap:     "<<m_cfgSpirocII.pwr_on_bandgap << std::endl
        <<" pwr_on_delay_start_ramp_tdc: "<<m_cfgSpirocII.pwr_on_delay_start_ramp_tdc << std::endl
        <<" delay_start_ramp_tdc:        "<<m_cfgSpirocII.delay_start_ramp_tdc << std::endl     
        <<" sw_ramp_slope:               "<<m_cfgSpirocII.sw_ramp_slope << std::endl
        <<" pwr_on_tdc_ramp:             "<<m_cfgSpirocII.pwr_on_tdc_ramp << std::endl
        <<" pwr_on_discri_adc:           "<<m_cfgSpirocII.pwr_on_discri_adc << std::endl
        <<" pwr_on_discri_gs:            "<<m_cfgSpirocII.pwr_on_discri_gs << std::endl
        <<" sw_auto_gain_select:         "<<m_cfgSpirocII.sw_auto_gain_select << std::endl
        <<" gain_select:                 "<<m_cfgSpirocII.gain_select << std::endl
        <<" sw_in_adc_ext:               "<<m_cfgSpirocII.sw_in_adc_ext << std::endl
        <<" sw_tdc_on:                   "<<m_cfgSpirocII.sw_tdc_on << std::endl;

    stream
        <<" mask_discri:                 ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.mask_discri[j] <<"  ";
    stream
        <<std::endl;

    stream 
        <<" mask_discri:                 ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.mask_discri[j+9] <<"  ";
    stream
        <<std::endl;

    stream
        <<" mask_discri:                 ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.mask_discri[j+18] <<"  ";
    stream
        <<std::endl;

    stream
        <<" mask_discri:                 ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.mask_discri[j+27] <<"  ";
    stream
        <<std::endl;

    stream
        <<" cmd_hold_ext:                "<<m_cfgSpirocII.cmd_hold_ext << std::endl
        <<" pwr_on_delay_discri:         "<<m_cfgSpirocII.pwr_on_delay_discri << std::endl
        <<" delay_discri:                "<<m_cfgSpirocII.delay_discri << std::endl;

    stream
        <<" dac_4_bit_theshold_adjust:   ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.dac_4_bit_theshold_adjust[j] <<"  ";
    stream
        <<std::endl;

    stream
        <<" dac_4_bit_theshold_adjust:   ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.dac_4_bit_theshold_adjust[j+9] <<"  ";
    stream
        <<std::endl;

    stream
        <<" dac_4_bit_theshold_adjust:   ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.dac_4_bit_theshold_adjust[j+18] <<"  ";
    stream
        <<std::endl;

    stream
        <<" dac_4_bit_theshold_adjust:   ";
    for(unsigned int j = 0; j<9 ; j++)
        stream<<m_cfgSpirocII.dac_4_bit_theshold_adjust[j+27] <<"  ";
    stream
        <<std::endl;

    stream
        <<" sw_fine_adjust:              "<<m_cfgSpirocII.sw_fine_adjust << std::endl
        <<" pwr_on_dac4b:                "<<m_cfgSpirocII.pwr_on_dac4b << std::endl
        <<" pwr_on_discri:               "<<m_cfgSpirocII.pwr_on_discri << std::endl
        <<" pwr_on_delay_ValidHold:      "<<m_cfgSpirocII.pwr_on_delay_ValidHold << std::endl
        <<" delay_ValidHold:             "<<m_cfgSpirocII.delay_ValidHold << std::endl
        <<" pwr_on_delay_RstColumn:      "<<m_cfgSpirocII.pwr_on_delay_RstColumn << std::endl
        <<" delay_RstColumn:             "<<m_cfgSpirocII.delay_RstColumn << std::endl;

}


//--------------------------------------------------------------------------------------------------------------------//
//Name:         convertVectorTo16bitsArray
//Function:     convert config vector into 16bits array, which will be written into UFM
//Variables:    dumpOn(int): switch on/off displaying  the debug infomation  
//                           0= don't display the debug infomation;   1 = display the debug infomation
//              temp(unsigned char[]): vectors converted from m_cfgSpirocII
//              array(unsigned short[]): point to the array to place the 16bits array written into UFM
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------// 
void EcalSpirocIIConfiguration::convertVectorTo16bitsArray(int dumpOn,unsigned char temp[702],unsigned short array[])
{
    short               bit;
    short               t,p;
    unsigned short  pad_short;
    short              index;
    /*=============convert to word=============*/
    pad_short = 0;
    t = 0; p =0;
    index = 0;
    // first word      .  2 dummy bits in the bit 15 and 14 are  '0'
    for(bit=701;bit>=688;bit--)
    {
        pad_short <<=1; 
        pad_short += temp[bit];
    }
    array[index]=pad_short;

    if(dumpOn==1){
        std::cerr<<"Info@16bitsArray:array["<<index<<"]=";
        for(unsigned int i = 0; i<16; i++)
            std::cerr<<((array[index]>>(15-i))&0x0001)<<" ";
        std::cerr<<std::endl;
    }
    index++;
    pad_short=0;

    for(bit=687;bit>=0;bit--)
    {  
        pad_short <<=1; 
        pad_short += temp[bit];
        t++;
        if((t%16)==0)  
        {  array[index] = pad_short;
            if(dumpOn==1){
                std::cerr<<"Info@16bitsArray:array["<<index<<"]=";
                for(unsigned int i = 0; i<16; i++)
                    std::cerr<<((array[index]>>(15-i))&0x0001)<<" ";
                std::cerr<<std::endl;
            }
            index++;     
            t = 0;
            pad_short = 0;
        }
    }
}



//----------------------------------------------------------------------------//
//Name:          selectForRead
//Function:      provide info that which kind of signals are used to access "READ interface" of Spiroc to other Classes which need this value.
//               Because m_selectForRead is a private variable for Class EcalSpirocIIConfiguration, which only can be accessed by Class EcalSpirocIIConfiguration itself
//               Parameter with the identical name in Firmware corresponds to bit6 of Gene_ctrl_reg0 (Firmwares for SpirocA and SpirocII use the same bit, 01.06.2010)
//               0= signals from SPI (set by C code);    1= signals from uplink(H,HB,S,SB)
//Parameters:    None
//Returns:       (int) return 0= signals from SPI (set by C code)
//                            1= signals from uplink(H,HB,S,SB)
//---------------------------------------------------------------------------//
int EcalSpirocIIConfiguration::selectForRead() const
{
    return m_selectForRead;
}

//----------------------------------------------------------------------------//
//Name:          setSelectForRead
//Function:      select signals used to access "READ interface" of Spiroc without refering to the config file
//Parameters:    selectForRead(int):  Only 0 or 1 could be used, otherwise the program will break off with an error report!
//               0= signals from SPI (set by C code),  1= signals from uplink(H,HB,S,SB)
//Returns:       None
//---------------------------------------------------------------------------//
void EcalSpirocIIConfiguration::setSelectForRead(int selectForRead)
{
    m_selectForRead = selectForRead;
}



//----------------------------------------------------------------------------//
//Name:          trigSelect
//Function:      provide trigger type of spiroc to other Classes which need this value.
//               Because m_trigSelect is a private variable for Class EcalSpirocIIConfiguration, which only can be accessed by Class EcalSpirocIIConfiguration itself
//               Return value of this function corresponds to different bits in SpirocII firmware and SpirocA firmware.
//               For SpirocA,it corresponds to bit9&bit8(trigSelect) of Gene_ctrl_reg0 (01.06.2010)
//               For SpirocII, it corresponds to bit 9(external_trigger_enable) of Gene_ctrl_reg0  (01.06.2010)
//               In SpirocA firmware, the default value for bit9&bit8 has been set to "00".
//Parameters:    None
//Returns:       (int) return value has different meanings & ranges for spirocII and spirocA
//               for SPIROCA  0=UL_HOLD(use H,HB,S,SB as trigger signal);  1=NIM_trigger;   2=LVDS_trigger
//               for SPIROCII:0=internal trigger; 1=external trigger;
//---------------------------------------------------------------------------//
int EcalSpirocIIConfiguration::trigSelect() const
{
    return m_trigSelect;
}

//----------------------------------------------------------------------------//
//Name:          setTrigSelect
//Function:      set trigger type of spiroc without refering to the config file
//Parameters:    trigSelect(int): has different meanings & ranges for spirocII and spirocA 
//               For spirocA: Only 0~2 could be used, otherwise the program will break off with an error report!
//                            0=UL_HOLD(use H,HB,S,SB as trigger signal);  1=NIM_trigger;   2=LVDS_trigger
//               For spirocII:Only 0 or 1 could be used, otherwise the program will break off with an error report!
//                            0=internal trigger; 1=external trigger
//Returns:       None
//---------------------------------------------------------------------------//
void EcalSpirocIIConfiguration::setTrigSelect(int trigSelect)
{
    m_trigSelect = trigSelect;
}

//----------------------------------------------------------------------------//
//Name:          setSlowControlCfgFile
//Function:      set a new path to the slow control config file without refering to the UsbBoard config file
//Parameters:    fileName(string) : the format is like "/home/pebs_ecal/usbBoard/readout/readoutSettings/spirocA_EPFL.cfg"
//Returns:       None
//---------------------------------------------------------------------------//
void EcalSpirocIIConfiguration::setSlowControlCfgFile(const std::string& fileName)
{
    m_slowControlCfgFile = fileName;
}

//----------------------------------------------------------------------------//
//Name:          firmwareFileName     
//Function:      provide path to the slow control config file to other Classes which need this value.
//               Because m_slowControlCfgFile is a private variable for Class UsbBoardConfiguration, which only can be accessed by Class UsbBoardConfiguration itself
//Parameters:    None
//Returns:       (string) return path to the slow control config file 
//---------------------------------------------------------------------------//
const std::string& EcalSpirocIIConfiguration::slowControlCfgFile() const
{
    return m_slowControlCfgFile;
}

// ------------------------- EcalSpirocII --------------------------------
EcalSpirocII::EcalSpirocII(UsbBoard* board, FrontEndBoardType type, int slotNo)
    : FrontEndBoard(board, type, slotNo)
    , m_configuration(0)
    , m_spiToUfmRegisterSwitch(0)
{
}

EcalSpirocII::~EcalSpirocII()
{}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         configure
//Function:     configure front end chip, if this function has been switched on in the UsbBoard cfg file
//Variables:    None
//Returns:      (bool)
//              true = config successfully
//              false= error exist during configuration
//-------------------------------------------------------------------------------------------------------------------//
bool EcalSpirocII::configure(FrontEndBoardConfiguration* configuration)
{
    m_configuration = static_cast<EcalSpirocIIConfiguration*>(configuration);

    std::cerr << "EcalSpirocII::configure() called." << std::endl;
    //return true; //TODO
    bool err = false;
#ifndef NO_QUSB
    // if (m_configuration->switchFeChipConfig()) {               // if switch on(=1) FeChipConfig in usbBoard Config file, Config Front End Chip through SPI
    //EcalSpirocIIConfiguration chipConfig(m_slotNo);
    std::string chipConfigFilePath = m_configuration->slowControlCfgFile();        // get the path to slow control config file

    unsigned char temp[702];
    unsigned short configArray[100];

    unsigned int dumpOn = 1;                // switch on/ off the debug infomation display.

    /*
    chipConfig.loadConfig(chipConfigFilePath);
    chipConfig.convertCfgToVector(temp);
    chipConfig.convertVectorTo16bitsArray(dumpOn, temp, configArray);
    */
    m_configuration->loadConfig(chipConfigFilePath);
    m_configuration->convertCfgToVector(temp);
    m_configuration->convertVectorTo16bitsArray(dumpOn, temp, configArray);

    if (spiConfigSpirocII(dumpOn, configArray, m_configuration->trigSelect(), m_configuration->selectForRead())) err = true;

           
     // select channel for continuous mode operation
     //////////////////////////////
     // Add only this two lines below to use the continous mode  
     //selectContinuousMode(1);      // select channel 1
     // getchar();
     ////////////////////////////////

        
     // Choose Uplink or SPI control for the read port 
     //////////////////////////
     //setSelectForRead(0);       // use spi
     setSelectForRead(1);       // use uplink
     ////////////////////////

    
#endif
    if (err) {
        std::cout << "Could not configure DAQ" << std::endl;
        assert(false);
        return false;
      } else {
        return true;
    }
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiToUfmOrRegister
//Function:     select QUSB SPI communication between QUSB & UFM  or  QUSB & Register in FPGA on the front end board
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              data(unsigned short):Only two values can be used here  0xC3C3= QUSB & UFM, 0x3C3C= QUSB & Register
//                                   Two macro definition have been defined in FrontEndChipMacroDefinition.h, use it ! ( SPItoUFM = 0xC3C3, SPItoREGISTER = 0x3C3C )
//
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::spiToUfmOrRegister(unsigned char portNum, unsigned short data,bool force) {
 if(data != m_spiToUfmRegisterSwitch || force){     // executed only when change spi between Ufm and R/W Register.
    unsigned char buf[2];
    buf[0] = data >> 8;
    buf[1] = data & 0xFF;
    write(portNum, buf, 2);
    //if (data==0xC3C3) printf("set spi to UFM \n");
    //if (data==0x3C3C) printf("set spi to GeneCtrlReg \n");       
  }
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiRegisterWrite
//Function:     write data to a given register in FOGA on the front end board
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              addr(unsigned short): address of the register to be written, currently for spirocA and spirocII front end board , we only use two register, already defined in FrontEndChipMacroDefinition.h (01.06.2010)
//                                    FIRMWARE_VERSION_REG_ADDR = 0x0000
//                                    GENERAL_CTRL_REG0 = 0x0001      ( detail definition for each bit, refer to FrontEndMacroDefiniton.h )
//              data(unsigned short): data to be written into the given register
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::spiRegisterWrite(unsigned char portNum, unsigned short addr, unsigned short data) {
    unsigned char buf[4];  //[0]--SPI cmd, [1], [2]--address, [3], [4]--data

    spiToUfmOrRegister(0,0x3C3C);  // make sure that  spi is linked to register R/W

    buf[0] = (addr>>8)&0xFF;
    buf[1] = (addr   )&0xFF;
    buf[2] = (data>>8)&0xFF;
    buf[3] = (data   )&0xFF;
    write(portNum, buf, 4);
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiRegisterRead
//Function:     read data from a given register in FOGA on the front end board
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              addr(unsigned short): address of the register to be written, currently for spirocA and spirocII front end board , we only use two register, already defined in FrontEndChipMacroDefinition.h (01.06.2010)
//                                    FIRMWARE_VERSION_REG_ADDR = 0x0000
//                                    GENERAL_CTRL_REG0 = 0x0001      ( detail definition for each bit, refer to FrontEndMacroDefiniton.h )
//Returns:      (unsigned short): return the data value read back from the given register
//-------------------------------------------------------------------------------------------------------------------//
unsigned short EcalSpirocII::spiRegisterRead(unsigned char portNum, unsigned short addr) {
    unsigned char buf[4];  //[0]--SPI cmd, [1], [2]--address, [3], [4]--data
    unsigned short  readData;
    spiToUfmOrRegister(0,0x3C3C); // make sure that  spi is linked to register R/W

    buf[0] = ((addr>>8)&0x7F) | 0x80;
    buf[1] = (addr   )&0xFF;
    buf[2] = 0x00;
    buf[3] = 0x00;
    writeRead(portNum, buf, 4);
    readData =  buf[2];
    readData <<= 8;
    readData += buf[3];
    return(readData);
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmStatusRead
//Function:     read the staus register in UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//Returns:      (unsigned char): return current status of the UFM
//Note:         before use function, remember to switch SPI communication to QUSB & UFM first, using function "spiToUfmOrRegister(unsigned char portNum, unsigned short data)"
//-------------------------------------------------------------------------------------------------------------------//
unsigned char EcalSpirocII::spiUfmStatusRead(unsigned char portNum) {
    unsigned char buf[2];  //[0]--SPI cmd
    spiToUfmOrRegister(0,0xC3C3);  // make sure that  spi is linked to write ufm

    buf[0] = UFM_CMD_RDSR;
    buf[1] = 0x00;
    writeRead(portNum, buf, 2);
    return buf[1];
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmErase
//Function:     erase the content in UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::spiUfmErase(unsigned char portNum) {
    unsigned char buf[2];  //[0]--SPI cmd
    spiToUfmOrRegister(0,0xC3C3);  // make sure that  spi is linked to write ufm

    buf[0] = UFM_CMD_WREN;
    write(portNum, buf, 1);   //write enable
    sleep(1);
    buf[0] = UFM_CMD_BERASE;
    write(portNum, buf, 1);   //erase
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmWrite
//Function:     write one word into UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              addr(unsigned short): the address in UFM to be written into
//              data(unsigned short): the data to be written into the UFM at the given address
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::spiUfmWrite(unsigned char portNum, unsigned short addr, unsigned short data) {
    unsigned char buf[5];
    spiToUfmOrRegister(0,0xC3C3);  // make sure that  spi is linked to write ufm

    buf[0] = UFM_CMD_WRITE;
    buf[1] = (addr>>8)&0xFF;
    buf[2] = (addr   )&0xFF;
    buf[3] = (data>>8)&0xFF;
    buf[4] = (data   )&0xFF;
    write(portNum, buf, 5);
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmReadWord
//Function:     read one word from UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              start_addr(unsigned short): the first address in UFM to be read
//Returns:      (unsigned char) return data (16bits) read from UFM
//-------------------------------------------------------------------------------------------------------------------//
unsigned short EcalSpirocII::spiUfmReadWord(unsigned char portNum, unsigned short start_addr) {
    unsigned char buf[6];
    unsigned short data;
    spiToUfmOrRegister(0,0xC3C3);  // make sure that  spi is linked to write ufm

    buf[0] =  UFM_CMD_READ;
    buf[1] = (start_addr>>8)&0xFF;
    buf[2] = (start_addr   )&0xFF;
    writeRead(portNum, buf, 5);
    data=(buf[3] << 8)+buf[4];
    return data;
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         spiUfmReadBlock
//Function:     read a sequence of words from UFM
//Variables:    portNum(unsigned char):the SPI device address(nss line) to write to
//              word_num(unsigned short): the number of words to be read
//              data(unsigned short[]): a pointer to the buffer that store the rceive data
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::spiUfmReadBlock(unsigned char portNum, unsigned short start_addr, unsigned short word_num, unsigned short data[512]) {
    unsigned char buf[64];
    unsigned short num, j;

    for (j=0; j<(word_num/30 + 1); j++)
    {
        if ((word_num-30*j) <= 30)
            num = word_num - 30*j;
        else
            num = 30;
        start_addr += 30*j;
        buf[0] =  UFM_CMD_READ;
        buf[1] = (start_addr>>8)&0xFF;
        buf[2] = (start_addr   )&0xFF;
        writeRead(portNum, buf, num*2+3);
        for(unsigned short i=0; i<num; i++) {
            data[i+j*30]=(buf[2*i+3] << 8)+buf[2*i+4];
            //                      std::cerr << "Info@SPIUfmReadBlock:readUFMfifo    readData[" << i+j*30 << "]=";
            //                         for(unsigned int bit=0; bit<16; bit++)
            //               std::cerr << ((data[i+j*30]>>(15-bit))&0x0001) << " ";
            //             std::cerr << std::endl;
        }
    }
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         selectReadChannel
//Function:     using signals send by SPI to access READ interface of spiroc, select a certain channel to be readout
//Variables:    channelIndex(unsigned int): channel index ( begin from 0)
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::selectReadChannel(unsigned int channelIndex) {

    setClkReadSpi(0);         //initial CLK_READ_SPI

    setResetbReadSpi(0);      // reset Read Register low active
    setResetbReadSpi(1);      // remove reset

    setSrinReadSpi(1);        // set '1' to SRIN_READ_SPI

    setClkReadSpi(1);        // clock 1
    setClkReadSpi(0);        // clock 0
    std::cerr << "select READ Register Channel 0" << std::endl;

    setSrinReadSpi(0);       //  set '0' to SRIN_READ_SPI

    for(unsigned int i=0; i<channelIndex; i++) {
        setClkReadSpi(1);      //clock in
        setClkReadSpi(0);
        std::cerr << "select READ Register Channel " << (i+1) << std::endl;
    }
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         setClkReadSpi
//Function:     using signals send by SPI to access READ interface of spiroc, set CLK_READ_spi to be hign/low
//Variables:    setValue(char) :  0=set CLK_READ_spi to LOW    1=set CLK_READ_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::setClkReadSpi(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);  // read register value
    m_generalCtrlReg0 &= 0xFFFB;                                 // mask the bit
    m_generalCtrlReg0 |= ((setValue & 0x1) << 2);                   // set bit to new setValue
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0); // write register
}

//--------------------------------------------------------------------------------------------------------------------//
//Name:         setSrinReadSpi
//Function:     using signals send by SPI to access READ interface of spiroc, set SRIN_READ_spi to be hign/low
//Variables:    setValue(char):  0=set SRIN_READ_spi to LOW    1=set SRIN_READ_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::setSrinReadSpi(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFF7) | ((setValue & 0x01) << 3);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setResetbReadSpi
//Function:     using signals send by SPI to access READ interface of spiroc, set RESETB_READ_spi to be hign/low
//Variables:    setValue(char):  0=set RESETB_READ_spi to LOW    1=set RESETB_READ_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::setResetbReadSpi(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFEF) | ((setValue & 0x01) << 4);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setResetbScSpi
//Function:     using signals send by SPI to access READ interface of spiroc(actually here we access the Slow Control interface,2010.06.14 snow@EPFL), set RESETB_SC_spi to be hign/low 
//Variables:    setValue(char):  0=set RESETB_SC_spi to LOW    1=set RESETB_SC_spi to HIGH                                          
//Returns:      None  
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::setResetbScSpi(char setValue) {
        m_generalCtrlReg0 = spiRegisterRead(0,GENERAL_CTRL_REG0_ADDR);
	m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xF7FF) | ((setValue & 0x01)<<11);
	spiRegisterWrite(0,GENERAL_CTRL_REG0_ADDR,m_generalCtrlReg0);
} 

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setHoldbSpi
//Function:     using signals send by SPI to access READ interface of spiroc, set HOLDB_spi to be hign/low
//Variables:    setValue(char):  0=set Holdb_spi to LOW    1=set Holdb_spi to HIGH
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::setHoldbSpi(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFDF) | ((setValue & 0x01) << 5);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setSelectForRead
//Function:     select signals to access READ interface of the  FrontEnd chip: 0= spi(set by C code); 1 = uplink(H, HB, S, SB);
//Variables:    setValue(char):  0=use signals from SPI(programed by C code) to access READ register of spiroc
//                               1=use signals from uplink(H, HB, S, SB) to access READ register of spiroc
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::setSelectForRead(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFFBF) | ((setValue & 0x01) << 6);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         seTestpulse
//Function:     set Testpulse to be high/Low, when using SPI to access READ interface of the  FrontEnd chip
//Variables:    setValue(char):  0=use signals from SPI(programed by C code) to access READ register of spiroc
//                               1=use signals from uplink(H, HB, S, SB) to access READ register of spiroc
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::setTestpulseSpi(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xFBFF) | ((setValue & 0x01) << 10);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         setLgEnSpi
//Function:     set lg_EN to be high/Low, when using SPI to access READ interface of the  FrontEnd chip
//Variables:    setValue(char): 0=set LG_EN_spi to be LOW,
//                              1=set LG_EN_spi to be HIGH , select Low gain output from spirocA
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::setLgEnSpi(char setValue) {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 = (m_generalCtrlReg0 & 0xEFFF) | ((setValue & 0x01) << 12);
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
}


//-------------------------------------------------------------------------------------------------------------------//
//Name:         dumpFirmwareVersion
//Function:     Read and dump SPIROCA/SPIROCII card FPGA firmware version
//Variables:    
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
// Read and dump SPIROCA/SPIROCII card FPGA firmware version
void EcalSpirocII::dumpFirmwareVersion() {
    m_generalCtrlReg0 = spiRegisterRead(0, FIRMWARE_VERSION_REG_ADDR);
    std::cerr << "Firmware version v" << m_generalCtrlReg0 << ".0 " << std::endl;
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         dumpFirmwareVersion
//Function:     Read and dump the current status of Gene_ctrl_reg0 in spirocA/II firmware
//Variables:    
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
// Read and dump Gene_ctrl_reg0 status
void EcalSpirocII::dumpGeneCtrlReg0() {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    std::cerr << "Info@Config_spirocA: GENERAL_REG0=";
    for(unsigned int i = 0; i<16; i++)
        std::cerr << ((m_generalCtrlReg0>>(15-i))&0x0001);
    std::cerr << std::endl;
}

//-------------------------------------------------------------------------------------------------------------------//
//Name:         resetChipConfig
//Function:     Reset the Slow Control interface by setting bit0 of Reg0(reset_cfg)in spiroc firmware
//Variables:    
//Returns:      None
//-------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::resetChipConfig() {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);

    m_generalCtrlReg0 |= 0x0001;  // set reset
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);

    m_generalCtrlReg0 &= 0xFFFE; // remove reset
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);

    std::cerr << "resetChipConfig finished!" << std::endl;
}


//-------------------------------------------------------------------------------------------------------------------//
//Name:         spiSetTrigSelect
//Function:     set bit9 of Reg0 in spirocII firmware
//Variables:    trigSelect(char):0=set bit9 to '0'   => use internal trigger
//                               1=set bit9 to '1'   => use external trigger
//Returns:      bool
//              true = successful
//              false= Error!
//-------------------------------------------------------------------------------------------------------------------//
bool EcalSpirocII::spiSetTrigSelect(char trigSelect) {
    bool err=false;
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);

    switch(trigSelect) {
        case 0 : m_generalCtrlReg0 &= 0xFDFF;
                 break;
        case 1 : m_generalCtrlReg0 |= 0x0200;
                 break;
        default: std::cerr << "Infomation:TRIG_TYPE out of range! For spirocII, this could only be select between 0 ~ 1" << std::endl;
                 err = true;
                 assert(false);
    }

    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);
    return(err);
}


//---------------------------------------------------------------------------------------------------------------------//
// Name:        compareUfmToConfigArray
// Function:    compare UFM content to chipConfigArray
// Parameters:  chipConfigArray = 16bits Word Array converted from FrontEndChip config file
//              compareArrayNum = Number of the Array you want to compare
// Returns:     (bool)
//              false = UFM content is identical to chipConfigArray
//              true = UFM content is different to chipConfigArray
//--------------------------------------------------------------------------------------------------------------------//
bool EcalSpirocII::compareUfmToConfigArray(unsigned short chipConfigArray[], unsigned int compareArrayNum) {

    unsigned short chipConfigArray_readBack[50];
    bool err=false;

    spiToUfmOrRegister(0, 0xC3C3);   // set SPI communication to UsbBoard <-> UFM

    spiUfmReadBlock(0, 0x0000, compareArrayNum, chipConfigArray_readBack);

    for(unsigned int i=0; i<compareArrayNum; i++) {
        if (chipConfigArray_readBack[i] != chipConfigArray[i])
        {
            err = true;
            std::cerr << "Infomation:Content in UFM is diffenert from Config file" << std::endl;
            break;
        }
    }
    return(err);
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        writeConfigArrayToUfm
// Function:    write chip Config Array into UFM
// Parameters:  chipConfigArray = 16bits Word Array converted from FrontEndChip config file
//              writeConfigArrayNum = Number of the Array you want to write into UFM
// Return:      None
//---------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::writeConfigArrayToUfm(unsigned short chipConfigArray[], unsigned int writeConfigArrayNum) {
    std::cerr << "Info:Write Config Arrays to UFM....." << std::endl;
    unsigned short addr = 0x0000;
    addr = 0x0000;

    spiToUfmOrRegister(0, 0xC3C3);   // set SPI communication to UsbBoard <-> UFM
    std::cerr << "Erase UFM 1st!" << std::endl;
    spiUfmErase(0);
    sleep(1);   // wait 1s, !!Don't delete it!!  after spiUfmErase(0), we have to wait! Otherwise, ufmerase will not be successful! 

    std::cerr << "Erase UFM 2nd!" << std::endl;
    spiUfmErase(0);
    sleep(1);   // wait 1s, !!Don't delete it!!  after spiUfmErase(0), we have to wait! Otherwise, ufmerase will not be successful! 

    for(unsigned int i=0; i<writeConfigArrayNum; i++, addr++) {
        spiUfmWrite(0, addr, chipConfigArray[i]);
    }
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        sendStartConfigCMD
// Function:    set "start_cfg" in SpirocA/spirocII firmware, the front end board will begin to write slow contro configuration from UFM to Slow Control Register
// Parameters:
// Returns:      None
//---------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::sendStartConfigCMD() {
    m_generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
    m_generalCtrlReg0 |= 0x0002;
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);  // send config from UFM to SPIROCA
    m_generalCtrlReg0 &= 0xFFFD;
    spiRegisterWrite(0, GENERAL_CTRL_REG0_ADDR, m_generalCtrlReg0);  // load config
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        loadConfigToSpiroc
// Function:    begin to configure Slow Control Register of SpirocA/spirocII by setting "start_cfg". The configuration will be executed twice.compare UFM content with output of SROUT_SC at the end of the second one, in order to check the writing process is correct or not.
// Parameters:
// Returns:(bool)
//              false = configuration succeed
//              true  = configuration error
//--------------------------------------------------------------------------------------------------------------------//
bool EcalSpirocII::loadConfigToSpiroc() {
    bool err = false;
    unsigned short generalCtrlReg0;

    //getchar();   // just for Debug
    spiToUfmOrRegister(0, 0x3C3C); // set SPI communication to UFM <-> spiroc chip
    // -------------------1st time config------------------------------

    resetChipConfig();        // reset
    sendStartConfigCMD();
    std::cerr << "send Start_cfg finished!" << std::endl;
    while (1)
    {
        generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
        if (generalCtrlReg0 & 0x4000) {//config finished
            std::cerr << "Infomation: 1st load  Config from UFM to Spiroc,   Finished!" << std::endl;
            if (generalCtrlReg0 & 0x8000)    {   //config error
                std::cerr << "ERROR:config error = 1, There is something wrong in SPIROC configuration! Please check!" << std::endl;
            }
            break;
        }
    }

    // --------------------2nd time ------------------------------

    sendStartConfigCMD();
    std::cerr << "Infomation: 2nd Load Config from UFM to Spiroc!...." << std::endl;
    while (1)
    {
        generalCtrlReg0 = spiRegisterRead(0, GENERAL_CTRL_REG0_ADDR);
        if (generalCtrlReg0 & 0x4000)  {   //config finished
            if (generalCtrlReg0 & 0x8000)    {   //config error
                std::cerr << "ERROR:config error = 1, There is something wrong in SPIROC configuration!please check!" << std::endl;
                err = true;
                //assert(false);
            }else{
                std::cerr << "Infomation:Config Spiroc Successfully!" << std::endl;
            }
            break;

        }
    }
    return(err);

}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        spiConfigSpirocII
// Function:    configure Both Gene_Ctrl_Reg0 in spirocII firmware and slow control register in spirocII chip
// Parameters:
// Returns:(bool)
//              false = configuration succeed
//              true  = configuration error
//--------------------------------------------------------------------------------------------------------------------//
//config the spirocii with the slow control setting
bool EcalSpirocII::spiConfigSpirocII(int dumpOn, unsigned short chipConfigArray[], char trigSelect, char select_read)
{

    bool err = false;


    if (dumpOn==1) {
        std::cerr << "Info@spiConfigSpirocII:Array write into UFM:" << std::endl;
        for(unsigned int index=0;index<s_configArrayNum_spirocII;index++) {
            std::cerr << "chipConfigArray[" << index << "]=";
            for(unsigned int i = 0; i<16; i++)
                std::cerr << ((chipConfigArray[index]>>(15-i))&0x0001) << " ";
            std::cerr << std::endl;
        }
    }

    std::cerr << "Begin to config SpirocII..." << std::endl;
    // ---------------------Begin to Config Reg0----------------------------------


    // Read and dump SPIROC card FPGA firmware version
    // Important! In order to confirm the version currently used in Spiroc card firmware
    // Always run this function before other further more commands.
    dumpFirmwareVersion();


    // reset the SPIROCA config
    resetChipConfig();


    //     //set to external trigger
    //     if (select_trigger == 1)
    //         m_generalCtrlReg0 |= 0x0200;
    //     else
    //         m_generalCtrlReg0 &= 0xFDFF;

    err |= spiSetTrigSelect(trigSelect);

    //---------------------End of Reg0 config---------------------------------------

    //---------------------Begin to write config arrays into UFM--------------------
    // set SPI communication to UsbBoard <-> UFM
    spiToUfmOrRegister(0, 0xC3C3);   // ext Ufm

    // compare UFM content to chipConfigArray
    // 0 = UFM content is identical to chipConfigArray, skip unnecessary writing config arrays into UFM
    // 1 = UFM content is different to chipConfigArray, write config array again into UFM
    if (compareUfmToConfigArray(chipConfigArray, s_configArrayNum_spirocII) == 1) {
        do{
            writeConfigArrayToUfm(chipConfigArray, s_configArrayNum_spirocII);
        }while(compareUfmToConfigArray(chipConfigArray, s_configArrayNum_spirocII));
    }// continue writing config arrays in UFM until UFM content is identical to chipConfigArray
    std::cerr << "Info@Config_spirocII:Write Config to UFM successfully! " << std::endl;

    spiToUfmOrRegister(0, 0x3C3C);  // set SPI communication to UFM <-> Front End Chip (spiroc)

    // load config file from UFM into SPIROCA
    bool loadConfigErr = false;
    do {
        loadConfigErr=loadConfigToSpiroc();
    }while(loadConfigErr == true);

    //select the read access
    setSelectForRead(select_read);
    return(err);
}


//-------------------------for Analogue Test of the chip-----------------------------------------
//---------------------------------------------------------------------------------------------------------------------//
// Name:        spirocAnalogTest
// Function:    Use SPI (C code programed) signals to access spiroc READ register, in order to do analog test for the chip
// Parameters:
// Returns:None
//--------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::spirocAnalogTest() {
    setHoldbSpi(1) ;        // set Hold signal to Analogue memory Hold signal

    setResetbReadSpi(0);    // set Resetb_Read
    setResetbReadSpi(1);    // move Resetb_Read

    selectReadChannel(2);
    while(0) {
        std::cerr << "set pulse" << std::endl;
        setTestpulseSpi(1);          // set Testpulse =1
        setTestpulseSpi(0);          // set Testpulse =0
    }

    //       setHoldbSpi(1) ;        // set Hold signal to Analogue memory Hold signal
    //  selectReadChannel(18);
    getchar();
}

//---------------------------------------------------------------------------------------------------------------------//
// Name:        selectContinousMode
// Function:    Use SPI (C code programed) signals to select a certain channel to be readout 
// Parameters:  channelIndex (unsigned int): the selected channel index (0,1....)         
// Returns:None
//--------------------------------------------------------------------------------------------------------------------//
void EcalSpirocII::selectContinuousMode(unsigned int channelIndex){
     setSelectForRead(0); // switch to use SPI signals to access spirocA READ register

     setResetbReadSpi(0);    // set Resetb_Read, in order to clear read register
     setResetbReadSpi(1);    // move Resetb_Read
     selectReadChannel(channelIndex);    //selec readout channel
}

