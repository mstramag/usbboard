#ifndef RunDescription_h
#define RunDescription_h

#include <ctime>
#include <iostream>
#include <string>
#include <map>

#ifndef NO_ROOT
#include <TObject.h>
#endif


class RunDescription
#ifndef NO_ROOT
    : public TObject
#endif
{
    public:
        RunDescription();

        ~RunDescription();

        unsigned long runNumber() const;
        void setRunNumber(unsigned long runNumber);

        const std::string& comment() const;
        void setComment(const std::string& comment);

        const std::string& settings() const;
        void setSettings (const std::string& settings);

        time_t timeStamp() const;
        void setTimeStamp(time_t time);

        void setMonitors(unsigned short *);         //added by lht, templorally for recording sensor value on new vata64 board

        void dump(std::ostream& stream) const;

        void addSensorInfos(int uplinkId,const std::string sensorInfo);

        void setSensorInfos(const std::string& sensorInfos) { m_sensorInfos=sensorInfos;}
        const std::string& sensorInfos(){return m_sensorInfos;}
    private:
        unsigned long m_runNumber;
        std::string m_comment;
        std::string m_settings;
        time_t m_timeStamp;

        unsigned short m_BIAS_A_MON;
        unsigned short m_CURR_A_MON;
        unsigned short m_TEMP_A_MON;
        unsigned short m_BIAS_B_MON;
        unsigned short m_CURR_B_MON;
        unsigned short m_TEMP_B_MON;
        std::string m_sensorInfos;

        std::map<int,std::string> m_mapSensorInfos;

#ifndef NO_ROOT
    ClassDef(RunDescription, 1)
#endif
};

#endif
