#include "UplinkData.h"

#include <assert.h>

#ifndef NO_ROOT
ClassImp(UplinkData)
#endif

#include <iomanip>

#ifndef NO_ROOT
UplinkData::UplinkData()
    : TObject()
{}
#endif

UplinkData::UplinkData(int usbBoardId, int uplinkId, unsigned short sampleSize)
    :
#ifndef NO_ROOT
      TObject(),
#endif
      m_usbBoardId(usbBoardId)
    , m_uplinkId(uplinkId)
    , m_headerWord1(0)
    , m_headerWord2(0)
    , m_adcValue(sampleSize, 0)
{}

UplinkData::~UplinkData()
{
}

int UplinkData::usbBoardId() const
{
    return m_usbBoardId;
}

int UplinkData::uplinkId() const
{
    return m_uplinkId;
}

unsigned short UplinkData::readEventNumber() const
{
    return m_headerWord1 & 0xFFFF;
}

int UplinkData::readUsbBoardId() const
{
    return (m_headerWord2 & 0xF000) >> 12;
}

int UplinkData::readUplinkId() const
{
    return (m_headerWord2 & 0x0FF0) >> 4;
}

bool UplinkData::readReservedBit() const
{
    return ((m_headerWord2 & 0x0008) >> 3) == 1 ? true : false;
}

ReadoutMode UplinkData::readReadoutMode() const
{
    unsigned short mode = m_headerWord2 & 0x0007;
    switch (mode) {
        case 0: return va32;
        case 1: return spirocEpfl;
        case 2: return spirocRwth;
        case 3: return amsK;
        case 4: return amsS;
        case 5: return hpe256;
        case 6: return spirocA;
        case 7: return vata64;
        default: ;
    }
    assert(false);
    return readoutModeEnd;
}

unsigned short UplinkData::sampleSize() const
{
    return m_adcValue.size();
}

void UplinkData::fill(const unsigned char* dataStream)
{
    m_headerWord1 = dataStream[0] | (dataStream[1] << 8);
    m_headerWord2 = dataStream[2] | (dataStream[3] << 8);

    std::vector<unsigned short>::iterator adcValueIt = m_adcValue.begin();
    std::vector<unsigned short>::iterator adcValueItEnd = m_adcValue.end();
    for (unsigned int i = 0; adcValueIt != adcValueItEnd; ++adcValueIt, ++i) {
        *adcValueIt = dataStream[4 + 2*i] | (dataStream[5 + 2*i] << 8);
    }
}

unsigned short UplinkData::adcValue(unsigned short i) const
{
    return m_adcValue.at(i);
}

void UplinkData::setAdcValue(unsigned short i, unsigned short val)
{
    m_adcValue.at(i) = val;
}

void UplinkData::clear()
{
    m_headerWord1 = 0;
    m_headerWord2 = 0;
    std::vector<unsigned short>::iterator adcValueIt = m_adcValue.begin();
    std::vector<unsigned short>::iterator adcValueItEnd = m_adcValue.end();
    for (; adcValueIt != adcValueItEnd; ++adcValueIt) {
        *adcValueIt = 0;
    }
}

void UplinkData::dump(std::ostream& stream)
{
    unsigned short lineSkip = 8;

    stream
        << "usbBoardId:       \t" << m_usbBoardId  << std::endl
        << "uplinkId:         \t" << m_uplinkId    << std::endl
        << "headerWord1:      \t" << m_headerWord1 << std::endl
        << "  readEventNumber:\t" << readEventNumber() << std::endl
        << "headerWord2:      \t" << m_headerWord2 << std::endl
        << "  readUsbBoardId  \t" << readUsbBoardId() << std::endl
        << "  readUplinkId    \t" << readUplinkId() << std::endl
        << "  readReservedBit \t" << (readReservedBit() ? "1" : "0") << std::endl
        << "  readReadoutMode \t" << ReadoutModeIdentifier[readReadoutMode()];

    std::vector<unsigned short>::iterator adcValueIt = m_adcValue.begin();
    std::vector<unsigned short>::iterator adcValueItEnd = m_adcValue.end();
    for (unsigned short i = 0; adcValueIt != adcValueItEnd; ++adcValueIt, ++i) {
        if (i % lineSkip == 0) {
            stream
                << std::endl << std::setw(3) << std::setfill('0') << i << '-'
                << std::setw(3) << std::setfill('0') << i+lineSkip << ":\t";
        }
        stream << *adcValueIt << '\t';
    }
    stream << std::endl << std::endl;
}

