#ifndef UplinkData_h
#define UplinkData_h

#include <vector>
#include <iostream>

#include <TypeDefs.h>

#ifndef NO_ROOT
#include <TObject.h>
#endif

class UplinkData
#ifndef NO_ROOT
    : public TObject
#endif
{
    public:
#ifndef NO_ROOT
        UplinkData();
#endif
        UplinkData(int usbBoardId, int uplinkId, unsigned short sampleSize);

        ~UplinkData();

        int usbBoardId() const;

        int uplinkId() const;

        unsigned short readEventNumber() const;
        
        int readUsbBoardId() const;
        
        int readUplinkId() const;
       
        bool readReservedBit() const;

        ReadoutMode readReadoutMode() const;

        unsigned short sampleSize() const;

        void fill(const unsigned char* dataStream);

        unsigned short adcValue(unsigned short i) const;
        
        void setAdcValue(unsigned short i, unsigned short val);

        void clear();
        
        void dump(std::ostream& stream = std::cout);

        unsigned short headerWord1() const { return m_headerWord1; }
        unsigned short headerWord2() const { return m_headerWord2; }

    private:
        int m_usbBoardId;
        int m_uplinkId;
        
        unsigned short m_headerWord1;
        unsigned short m_headerWord2;
        std::vector<unsigned short> m_adcValue;

#ifndef NO_ROOT
        ClassDef(UplinkData, 1)
#endif
};

#endif
