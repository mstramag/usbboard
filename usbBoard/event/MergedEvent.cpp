#include "MergedEvent.h"

#include <assert.h>

#ifndef NO_ROOT
ClassImp( MergedEvent)
#endif

MergedEvent::MergedEvent() :
#ifndef NO_ROOT
    TObject(),
#endif
    m_runNumber(0),
    m_eventNumber(0),
    m_uplinkData()
{
}

MergedEvent::~MergedEvent() {
}

void MergedEvent::addUplink(int usbBoardId, int uplinkId, unsigned short sampleSize)
{
    m_uplinkData.push_back(UplinkData(usbBoardId, uplinkId, sampleSize));
}

const UplinkData& MergedEvent::uplinkData(int uplinkId) const
{
    std::vector<UplinkData>::const_iterator uplinkIt = m_uplinkData.begin();
    std::vector<UplinkData>::const_iterator uplinkItEnd = m_uplinkData.end();
    for (; uplinkIt != uplinkItEnd; ++uplinkIt) {
        if (uplinkIt->uplinkId() == uplinkId)
            return *uplinkIt;
    }
    std::cerr << "Failure: Cannot find uplinkId " << uplinkId << std::endl;
    assert(false);
    return *uplinkItEnd;
}

UplinkData& MergedEvent::uplinkData(int uplinkId)
{
    std::vector<UplinkData>::iterator uplinkIt = m_uplinkData.begin();
    std::vector<UplinkData>::iterator uplinkItEnd = m_uplinkData.end();
    for (; uplinkIt != uplinkItEnd; ++uplinkIt) {
        if (uplinkIt->uplinkId() == uplinkId)
            return *uplinkIt;
    }
    std::cerr << "Failure: Cannot find uplinkId " << uplinkId << std::endl;
    assert(false);
    return *uplinkItEnd;
}

const UplinkData& MergedEvent::uplinkDataByIndex(int index) const
{
    if (index < 0 || (unsigned int)index >= m_uplinkData.size()) {
        std::cerr << "Failure: UplinkData index out of bounds" << std::endl;
        assert(false);
    }
    return m_uplinkData[index];
}

UplinkData& MergedEvent::uplinkDataByIndex(int index)
{
    if (index < 0 || (unsigned int)index >= m_uplinkData.size()) {
        std::cerr << "Failure: UplinkData index out of bounds" << std::endl;
        assert(false);
    }
    return m_uplinkData[index];
}

void MergedEvent::clear()
{
    m_eventNumber = 0;
    std::vector<UplinkData>::iterator uplinkIt = m_uplinkData.begin();
    std::vector<UplinkData>::iterator uplinkItEnd = m_uplinkData.end();
    for (; uplinkIt != uplinkItEnd; ++uplinkIt)
        uplinkIt->clear();
}

void MergedEvent::dump(std::ostream& stream)
{
    stream
        << "+++ MergedEvent +++" << std::endl
        << "run number:  \t" << m_runNumber << std::endl
        << "event number:\t" << m_eventNumber << std::endl << std::endl;
    std::vector<UplinkData>::iterator uplinkIt = m_uplinkData.begin();
    std::vector<UplinkData>::iterator uplinkItEnd = m_uplinkData.end();
    for (; uplinkIt != uplinkItEnd; ++uplinkIt)
        uplinkIt->dump(stream);
    stream << "--- MergedEvent ---" << std::endl;
}

