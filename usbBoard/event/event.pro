TEMPLATE = lib

TARGET = usbBoardEvent

#CONFIG += release
CONFIG += debug

CONFIG -= qt

HEADERS+= \
    UplinkData.h \
    RunDescription.h \
    MergedEvent.h

SOURCES+= \
    UplinkData.cpp \
    RunDescription.cpp \
    MergedEvent.cpp

CREATE_ROOT_DICT_FOR_CLASSES+= \
    UplinkData.h \
    RunDescription.h \
    MergedEvent.h

INCLUDEPATH += $${PWD}

exists($(USBBOARDPATH)) {
    include($(USBBOARDPATH)/usbBoard.pri)
    include($(USBBOARDPATH)/RootCInt.pri)
} else {
    include(../usbBoard.pri)
    include(../RootCInt.pri)
}

win* {
    !NO_ROOT {
        DEF_FILE=event.def
    } else {
        DEF_FILE=event.no_root.def
    }
}
