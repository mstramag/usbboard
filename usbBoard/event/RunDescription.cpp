#include "RunDescription.h"

#include <assert.h>
#include <ctime>
#include <iostream>
#include <sstream>
#include <iomanip>

#ifndef NO_ROOT
ClassImp(RunDescription)
#endif

RunDescription::RunDescription()
    : 
#ifndef NO_ROOT
      TObject(),
#endif
      m_runNumber(0)
    , m_comment()
    , m_settings()
    , m_timeStamp(0)
    , m_sensorInfos()
{}

RunDescription::~RunDescription()
{}

unsigned long RunDescription::runNumber() const
{
    return m_runNumber;
}

void RunDescription::setRunNumber(unsigned long runNumber)
{
    m_runNumber = runNumber;
}

const std::string& RunDescription::comment() const
{
    return m_comment;
}

void RunDescription::setComment(const std::string& comment)
{
    m_comment = comment;
}

const std::string& RunDescription::settings() const
{
    return m_settings;
}

void RunDescription::setSettings(const std::string& settings)
{
    m_settings = settings;
}

time_t RunDescription::timeStamp() const
{
    return m_timeStamp;
}

void RunDescription::setTimeStamp(time_t time)
{
    m_timeStamp = time;
}

void RunDescription::setMonitors(unsigned short *data)
{
    m_BIAS_A_MON = data[0];
    m_CURR_A_MON = data[1];
    m_TEMP_A_MON = data[2];
    m_BIAS_B_MON = data[3];
    m_CURR_B_MON = data[4];
    m_TEMP_B_MON = data[5];
}

void RunDescription::dump(std::ostream& stream) const
{
    stream
        << "+++ RunDescription +++" << std::endl
        << "run number:\t" << m_runNumber << std::endl
        << "comment:   \t" << m_comment << std::endl
        //<< "time stamp:\t" << m_timeStamp << std::endl
        << "time stamp:\t" << ctime(&m_timeStamp) << std::endl      //by lht, convert time stamp to string "Www Mmm dd hh:mm:ss yyyy" like Sat May 20 15:21:51 2000
        << "settings:  \t" << std::endl << m_settings
        << "BISA_A_MON = " << m_BIAS_A_MON << std::endl
        << "CURR_A_MON = " << m_CURR_A_MON << std::endl
        << "TEMP_A_MON = " << m_TEMP_A_MON << std::endl
        << "BISA_B_MON = " << m_BIAS_B_MON << std::endl
        << "CURR_B_MON = " << m_CURR_B_MON << std::endl
        << "TEMP_B_MON = " << m_TEMP_B_MON << std::endl
        << "sensors Infos:"<<std::endl
        << m_sensorInfos <<std::endl
        << "--- RunDescription ---" << std::endl;
}

void RunDescription::addSensorInfos(int uplinkId, const std::string sensorInfo){
     std::map<int,std::string>::iterator it;

     it = m_mapSensorInfos.find(uplinkId);

     if(it == m_mapSensorInfos.end()){
         //if sensors from this uplinkId has not been recorded before
         m_mapSensorInfos.insert(make_pair(uplinkId,sensorInfo));
     }else{
         //if sensors from this uplinkId has been recorded, change the values.
         it->second = sensorInfo;
     }
}
