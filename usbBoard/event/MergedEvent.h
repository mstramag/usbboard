#ifndef MergedEvent_h
#define MergedEvent_h

#ifndef NO_ROOT
#include <TObject.h>
#endif

#include <vector>
#include <iostream>

#include "UplinkData.h"

class MergedEvent
#ifndef NO_ROOT
: public TObject
#endif
{
public:
    MergedEvent();

    virtual ~MergedEvent();

    void addUplink(int usbBoardId, int uplinkId, unsigned short sampleSize);

    void setRunNumber(unsigned long runNumber) {m_runNumber = runNumber;}
    unsigned long runNumber() const {return m_runNumber;}

    void setEventNumber(unsigned long eventNumber) {m_eventNumber = eventNumber;}
    unsigned long eventNumber() const {return m_eventNumber;}

    unsigned int numberOfUplinks() const {return m_uplinkData.size();}

    const UplinkData& uplinkData(int uplinkId) const;
    UplinkData& uplinkData(int uplinkId);

    const UplinkData& uplinkDataByIndex(int index) const;
    UplinkData& uplinkDataByIndex(int index);

    virtual void clear();

    virtual void dump(std::ostream& stream = std::cout);

protected:
    unsigned long m_runNumber;
    unsigned long m_eventNumber;

    std::vector<UplinkData> m_uplinkData;
#ifndef NO_ROOT  
ClassDef(MergedEvent, 1)
#endif
};

#endif
