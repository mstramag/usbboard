ROOT_DATADIR="/home/production1/Prod_data"
SENSOR_DATADIR=$1
CONFDIR="/home/production1/Data/cfg_prod/"
ROOT_DATADIR="/home/production1/Prod_data/"
SETUP_DIR="/home/production1/usbboard/usbBoard"
CONFIGFILE="setupSiPM_all.txt"



cd $SETUP_DIR
source SetupUsbBoard.sh
cd Builds

echo "Acquiring pedestal files ..."
python control.py 0
PCOMMAND="./ecalTest  -d 9 -n 1000 -p $CONFDIR -o $ROOT_DATADIR/Dead_Channel/deadpedestal.root"
echo $PCOMMAND
$PCOMMAND
echo "Pedestal DAQ Done !"

vref = 57.0
echo "BIAS voltage set = $vref V"
python control.py 57.0
echo "Please check the Keithley 2400 display"
sleep 1
COMMAND="./ecalTest  -d 9 -n 10000 -p $CONFDIR -o $ROOT_DATADIR/Dead_Channel/deadlight.root"
echo $COMMAND
$COMMAND
echo "DAQ Done !"
python control.py 0
echo "Display"
COMMAND="./lect_sipm -p $ROOT_DATADIR/Dead_Channel/deadpedestal.root -s $ROOT_DATADIR/Dead_Channel/deadlight.root -S $CONFDIR$CONFIGFILE -F"
echo $COMMAND
$COMMAND
