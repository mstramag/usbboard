import time
import serial
import sys

if len(sys.argv) == 1:
	print "usage: control.py xx.x (xx.x = volts or 0 to turn off the output"
	sys.exit()

voltage = sys.argv[1]

ser = serial.Serial(port='/dev/ttyUSB0')

ser.isOpen()

print 'Serial port opened ...'

# Reset and clear register of KY2400 
ser.write("*RST" + '\r')
ser.write("*CLS" + '\r')
ser.write("*IDN?" + '\r')

out = '' 
time.sleep(1)

while ser.inWaiting() > 0:
	out += ser.read(1)
if out != '':
	print out

if voltage == '0' :
	print "OUTPUT off"
	ser.write("OUTP OFF" + '\r')
else :
	ser.write("FORM:ELEM VOLT,CURR" + '\r')
	ser.write("SOUR:VOLT:RANG:AUTO 1" + '\r')

	vcommand = "SOUR:VOLT:LEV:IMM:AMPL " + voltage
	# print vcommand
	ser.write(vcommand + '\r')

	ser.write("OUTP ON" + '\r')
	ser.write("INIT" + '\r')

	ser.write("READ?" + '\r')

	out = '' 
	time.sleep(1)
	while ser.inWaiting() > 0:
		out += ser.read(1)
	if out != '':
		print out

	time.sleep(3)
	ser.write("SYST:LOC" + '\r')

ser.close()
print 'Serial port closed ...'
