#ifndef TypeDefs_h
#define TypeDefs_h

const static unsigned int s_cStringLength = 1 << 11;
const static unsigned int s_uplinksPerUsbBoard = 8;

enum ReadoutMode {va32 = 0, spirocEpfl = 1, spirocRwth = 2, amsK = 3, amsS = 4, hpe256 = 5, spirocA = 6, vata64 = 7, vata64V2 = 8,vata64V2_DC = 9, readoutModeEnd = 10};
const char* const ReadoutModeIdentifier[] = {"va32", "spirocEpfl", "spirocRwth", "amsK", "amsS", "hpe256", "spirocA", "vata64", "vata64V2", "vata64V2_DC"};

enum TriggerMode {internal = 0, external = 1, calibration = 2, led = 3, triggerModeEnd = 4};
const char* const TriggerModeIdentifier[] = {"internal", "external", "calibration"};

enum QSPI_CMD_RAM_FOR_VATA64{
     CMD_REG_RAM_BASE_VATA64 = 0x0800,
     CMD_ADC_RAM_BASE_VATA64 = 0x0880,
     CMD_UFM_RAM_BASE_VATA64 = 0x0900,
     CMD_REG_SINGLE_WIDTH_VATA64 = 0x10,
     CMD_ADC_SINGLE_WIDTH_VATA64 = 0x10,
     CMD_UFM_SINGLE_WIDTH_VATA64 = 0x60
};
#endif
